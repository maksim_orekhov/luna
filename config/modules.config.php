<?php
return [
    'Zend\Mvc\Plugin\FlashMessenger',
    'Zend\Cache',
    'Zend\Paginator',
    'Zend\Navigation',
    'Zend\Serializer',
    'Zend\ServiceManager\Di',
    'Zend\Mvc\Plugin\FilePrg',
    'Zend\Mvc\Plugin\Identity',
    'Zend\Mvc\Plugin\Prg',
    'Zend\Session',
    'Zend\Mvc\I18n',
    'Zend\Mvc\Console',
    'Zend\Log',
    'Zend\Form',
    'Zend\InputFilter',
    'Zend\Filter',
    'Zend\Hydrator',
    'Zend\I18n',
    'DoctrineModule',
    'Zend\Mail',
    'Zend\Db',
    'Zend\Router',
    'Zend\Validator',
    'DoctrineORMModule',
    'ZendTwig',
    'Application',
    'Api',
    'Core',
    'ModuleAuthV2',
    'ModuleCode',
    'ModuleRbac',
    'ModuleFileManager',
    'ModuleImageResize'
];
