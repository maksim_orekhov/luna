<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

use Doctrine\DBAL\Driver\PDOMySql\Driver as PDOMySqlDriver;
use Zend\Session;
use Zend\Cache\Storage\Adapter\Filesystem;

return [
    'recaptcha_v2' => [
        'site_key' => '6Ldbo3sUAAAAAE_aqWbTz27HH6gaPsm0AaWPSxeK',
        'secret_key' => '6Ldbo3sUAAAAAKkVB5Yc5_ZmNy4gdiVnJqVyG8qN',
        'bypass' => false
    ],
    'server_document_root' => '',
    // Admin email
    'admin_email' => 'test@simple-technology.ru',
    'dependencies' => [
        'factories' => [
            ModuleCode\Api\CodeController::class => ModuleCode\Api\Factory\CodeControllerFactory::class
        ],
    ],
    'service_manager' => [
        'abstract_factories' => [
            Zend\Navigation\Service\NavigationAbstractServiceFactory::class,
        ],
    ],
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'driverClass' => PDOMySqlDriver::class,
            ],
        ],
        'configuration' => [
            'orm_default' => array(
                'datetime_functions' => [
                    'strtodate' => 'DoctrineExtensions\Query\Mysql\StrToDate',
                ],
            )
        ],
        // migrations configuration
        'migrations_configuration' => [
            'orm_default' => [
                'directory' => 'data/Migrations',
                'name'      => 'Doctrine Database Migrations',
                'namespace' => 'Migrations',
                'table'     => 'migrations',
            ],
        ],
    ],
    // Sessions
    'session_config' => [
        'name' => 'luna',
        // Default cookie session lifetime (Срок действия cookie сессии истечет через 1 час)
        'cookie_lifetime' => 60*60,
        // Данные сессии будут храниться на сервере до 30 дней.
        'gc_maxlifetime'     => 60*60*24*30,
    ],
    // Настройка менеджера сессий.
    'session_manager' => [
        // Валидаторы сессии (используются для безопасности).
        'validators' => [
            Session\Validator\RemoteAddr::class,
            #Session\Validator\HttpUserAgent::class //@TODO закоментил согласно GP-1721
        ]
    ],
    'session_containers' => [
        'ContainerNamespace'
    ],
    // Настройка хранилища сессий.
    'session_storage' => [
        'type' => Session\Storage\SessionArrayStorage::class
    ],
    // Cookie session lifetime with "remember me"
    'cookie_lifetime_with_remember_me' => 60*60*24*30,

    // Tokens length and validity config // Время только в секундах!
    'tokens' => [ // 3600 = 60 минут
        // Encryption Key
        'token_encryption_key'                      => '0yV8fvEaG43fA', // Key for JWToken encryption
        // Phone confirmation code
        'max_number_of_specific_code_for_user'      => 5, // Лимит кодов для одного типа операции
        'confirmation_code_expiration_time'         => 60*15, // 15 минут - время действия кода
        'confirmation_code_request_period'          => 60, // 60 секунд - максимальная частота запроса кода
        // Email confirmation
        'email_confirmation_token_length'           => 8, // 8 символов
        'email_confirmation_token_request_period'   => 60, // 60 секунд - максимальная частота смены email
        'email_confirmation_token_expiration_time'  => 3600*24, // 24 часа - время действия токена
        // Password resetting
        'password_resetting_token_length'           => 8, // 8 символов
        'password_resetting_token_request_period'   => 60, // 60 секунд - максимальная частота смены password
        'password_resetting_token_expiration_time'  => 3600*24, // 24 часа - время действия токена

        'login_cookie_lifetime'         => 3600*24, // 24 часа
    ],
    'mail_options' => [
        'name'              => 'smtp.guarantpay.ru',
        'host'              => 'smtp.guarantpay.ru',
        'port'              => 25025, // (альтернативный 25)
        'connection_class'  => 'login',
        'connection_config' => [
            'username'      => 'noreply@guarantpay.ru',
            'password'      => 'j3A4y29We5',
        ],
        'from_email'    => 'noreply@guarantpay.ru',
        'from_name'     => 'Guarant Pay'
    ],
    // Cache config
    'caches' => [
        'FilesystemCache' => [
            'adapter' => [
                'name'    => Filesystem::class,
                'options' => [
                    // Store cached data in this directory.
                    'cache_dir' => './data/cache',
                    // Store cached data for 1 hour.
                    'ttl' => 60*60*1
                ],
            ],
            'plugins' => [
                [
                    'name' => 'serializer',
                    'options' => [],
                ],
            ],
        ],
    ],
    // File management
    'file-management' => [
        'main_upload_folder' => 'upload',
    ],
    // Pagination
    'default_pagination_preferences' => [
        'start'             => 0,       // начальная точка
        'number_per_page'   => 20,      // кол-во на странице
        'sorting_order'     => 'DESC'   // порядок
    ],
];
