<?php
return [
    'zend_twig' => [
        'force_standalone' => true,
        'force_twig_strategy' => false,
        'invoke_zend_helpers' => true,
        'environment' => [
            'debug' => true,
        ],
        'extensions' => [
            \Core\Extension\TwigExtension::class,
            \Twig_Extension_Debug::class,
        ],
    ],
];