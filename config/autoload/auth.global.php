<?php

return [
    'auth_service' => [
        'provider' => 'ModuleAuthV2',
    ],
    'service_manager' => [
        'aliases' => [
            'ModuleAuthV2' => ModuleAuthV2\Provider\AuthServiceProvider::class,
        ],
    ],
];