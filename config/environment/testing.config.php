<?php
return [
    ///////////////////////////////////
    ////// common testing config //////
    ///////////////////////////////////
    'recaptcha_v2' => [
        'bypass' => true
    ],
    'module_listener_options' => [
        'config_cache_enabled' => false,
        'module_map_cache_enabled' => false,
    ],
    'view_manager' => [
        'display_exceptions' => false,
    ],
    'sms_providers' => [
        'message_send_simulation' => true,
    ],
    'email_providers' => [
        'message_send_simulation' => true,
    ],
    'auth_service' => \getenv('AUTH_PROVIDER_ENV') ? ['provider' => \getenv('AUTH_PROVIDER_ENV')] : [],
];