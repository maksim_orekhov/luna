<?php
return [
    //////////////////////////////////////
    ////// common production config //////
    //////////////////////////////////////
    'view_manager' => [
        'display_exceptions' => false,
    ],
    // Mail configurations
    'email_providers' => [
        'message_send_simulation' => false,
    ],
    'sms_providers' => [
        'message_send_simulation' => false,
    ],
];
