<?php

return [
    ///////////////////////////////////////
    ////// common development config //////
    ///////////////////////////////////////
    'view_manager' => [
        'display_exceptions' => true,
    ],
    'sms_providers' => [
        'message_send_simulation' => true, // Turns on the simulation mode in dev
    ],
    'email_providers' => [
        'message_send_simulation' => false, // Turns on the simulation mode in dev
    ],
];