import {applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import ConfigMiddleware from './ConfigMiddleware';
import SearchMiddleware from './SearchFormMiddleware';
import reduxCatch from 'redux-catch';

function errorHandler(error, getState, lastAction, dispatch) {
    console.error('Debug:', error);
    console.log('Debug: current state', getState());
    console.log('Debug: last action was', lastAction);

    // optionally dispatch an action due to the error using the dispatch parameter
}

const appMiddleware = applyMiddleware(
    thunk,
    ConfigMiddleware,
    SearchMiddleware,
    reduxCatch(errorHandler)
);

export default appMiddleware;
