import ACTION from '../constants/actions';
import URL from '../constants/urls';
import { getInlineJSON, customFetch } from '../Helpers';
import { openUserProfileForm } from '../actions';

function loadUserInfo(next, action) {
    next({
        ...action,
        type: ACTION.USER_LOAD_START
    });

    const data = getInlineJSON('profile-json');
    if (!data || !data.user) {
        return next({
            type: ACTION.USER_LOAD_FAIL,
        });
    }

    next({
        type: ACTION.USER_LOAD_SUCCESS,
        payload: {
            data: data.user
        }
    });

}

function changeUserAvatar(next, action) {
    next({ type: ACTION.USER_DATA_CHANGE_START, payload: { fieldName: 'avatar' } });

    customFetch(URL.USER_AVATAR_CHANGE, {
        method: 'POST',
        body: action.payload.formData,
        processData: false,
        contentType: false
    }).then((response) => {
        if (response.status === 'SUCCESS') {
            next({
                type: ACTION.USER_AVATAR_CHANGE_SUCCESS,
                payload: { avatar: `${response.data.user.avatar}` },
            });
        } else {
            next({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'avatar' } });
        }
    }).catch((error) => {
        next({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'avatar' } });
        console.error(error);
    });

}

function changeUserName(next, action) {
    next({ type: ACTION.USER_DATA_CHANGE_START, payload: { fieldName: 'name' } });


    customFetch(URL.PROFILE, {
        method: 'POST',
        body: action.payload.formData
    }).then((response) => {
        if (response.status === 'SUCCESS') {
            next({
                type: ACTION.USER_NAME_CHANGE_SUCCESS,
                payload: {
                    name: response.data.user.name
                }
            });
        } else {
            next({
                type: ACTION.USER_NAME_CHANGE_FAIL,
                payload: {
                    errors: response
                }
            });
        }
    }).catch((error) => {
        next({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'name' } });
        console.error(error);
    });

}

function changeUserPhone(next, action) {
    const { payload: { formData, formType } } = action;
    next({ type: ACTION.USER_DATA_CHANGE_START, payload: { fieldName: 'phone' } });

    customFetch(formType === 'add' ? URL.PHONE.CREATE : URL.PHONE.EDIT, {
        method: 'POST',
        body: formData,
    }).then((response) => {
        if (response.status.toLowerCase() === 'error') {
            next({
                type: ACTION.USER_PHONE_CHANGE_FAIL,
                payload: {
                    fieldName: 'phone',
                    errors: response
                }
            });
        } else {
            next({
                type: ACTION.USER_PHONE_CHANGE_SUCCESS,
                payload: {
                    phone: JSON.parse(formData).new_phone.replace('+', '')
                }
            });
            next(openUserProfileForm('phoneConfirmToken'));
        }
    }).catch((error) => {
        next({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'phone' } });
        console.error(error);
    });

}

function confirmPhoneToken(next, action) {
    const { payload: { formData } } = action;
    next({ type: ACTION.USER_DATA_CHANGE_START, payload: { fieldName: 'phoneConfirmToken' } });

    customFetch(URL.PHONE.CONFIRM_TOKEN, {
        method: 'POST',
        body: formData,
    }).then((response) => {
        if (response.status.toLowerCase() === 'success') {
            next({ type: ACTION.USER_PHONE_CONFIRM_TOKEN_SUCCESS });
            next(openUserProfileForm('phoneConfirmCode'));
        } else {
            next({
                type: ACTION.USER_PHONE_CONFIRM_TOKEN_FAIL,
                payload: { errors: response }
            });
        }
    }).catch((error) => {
        next({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'phoneConfirmToken' } });
        console.error(error);
    });
}

function confirmPhoneCode(next, action) {
    const { payload: { formData } } = action;
    next({ type: ACTION.USER_DATA_CHANGE_START, payload: { fieldName: 'phoneConfirmCode' } });
    customFetch(URL.PHONE.CONFIRM_CODE, {
        method: 'POST',
        body: formData
    }).then((response) => {
        if (response.status.toLowerCase() === 'success') {
            next({ type: ACTION.USER_PHONE_CONFIRM_CODE_SUCCESS });
            next(openUserProfileForm('phoneChangeSuccess'));
        } else {
            next({
                type: ACTION.USER_PHONE_CONFIRM_CODE_FAIL,
                payload: { errors: response }
            });
        }
    }).catch((error) => {
        next({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'phoneConfirmCode' } });
        console.error(error);
    });
}

function sendPhoneCode(next, action) {
    const { payload: { formData } } = action;
    next(openUserProfileForm('phoneConfirmCode'));
    next({ type: ACTION.USER_DATA_CHANGE_START, payload: { fieldName: 'phoneConfirmCode' } });


    customFetch(URL.PHONE.RESEND_CODE, {
        method: 'POST',
        body: formData
    }).then((response) => {
        if (response.status.toLowerCase() === 'success') {
            next({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'phoneConfirmCode' } });
        } else {
            next({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'phoneConfirmCode' } });
        }
    }).catch((error) => {
        next({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'phoneConfirmCode' } });
        console.error(error);
    });

}

function sendPhoneToken(next, action) {
    const { payload: { formData } } = action;
    next(openUserProfileForm('phoneConfirmToken'));
    next({ type: ACTION.USER_DATA_CHANGE_START, payload: { fieldName: 'phoneConfirmToken' } });

    customFetch(URL.PHONE.RESEND_TOKEN, {
        method: 'POST',
        body: formData
    }).then((response) => {
        if (response.status.toLowerCase() === 'success') {
            next({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'phoneConfirmToken' } });
        } else {
            next({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'phoneConfirmToken' } });
        }
    }).catch((error) => {
        next({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'phoneConfirmToken' } });
        console.error(error);
    });
}

function changeUserEmail(next, action) {
    const { payload: { formData, newEmail } } = action;
    next({ type: ACTION.USER_DATA_CHANGE_START, payload: { fieldName: 'email' } });

    customFetch(URL.EMAIL.EDIT, {
        method: 'POST',
        body: formData
    }).then((response) => {
        if (response.status.toLowerCase() === 'success') {
            next({
                type: ACTION.USER_EMAIL_CHANGE_SUCCESS,
                payload: {
                    email: newEmail,
                },
            });
            next(openUserProfileForm('emailChangeAccept'));
        } else {
            next({
                type: ACTION.USER_EMAIL_CHANGE_FAIL,
                payload: {
                    errors: response
                }
            });
        }
    }).catch((error) => {
        next({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'email' } });
        console.error(error);
    });

}

function sendConfirmEmail(next, action) {
    const { payload: { formData } } = action;
    next(openUserProfileForm('emailChangeAccept'));
    next({ type: ACTION.USER_DATA_CHANGE_START, payload: { fieldName: 'emailChangeAccept' } });
    customFetch(URL.EMAIL.RESEND, {
        method: 'POST',
        body: formData
    }).then((response) => {
        if (response.status.toLowerCase() === 'success') {
            next({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'emailChangeAccept' } });
        } else {
            next({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'emailChangeAccept' } });
        }
    }).catch((error) => {
        next({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'emailChangeAccept' } });
        console.error(error);
    });
}

export default () => (next) => (action) => {
    const { type } = action;

    switch (type) {
        case ACTION.USER_LOAD:
            return loadUserInfo(next, action);
        case ACTION.USER_AVATAR_CHANGE:
            return changeUserAvatar(next, action);
        case ACTION.USER_NAME_CHANGE:
            return changeUserName(next, action);
        case ACTION.USER_PHONE_CHANGE:
            return changeUserPhone(next, action);
        case ACTION.USER_PHONE_CONFIRM_TOKEN:
            return confirmPhoneToken(next, action);
        case ACTION.USER_PHONE_CONFIRM_CODE:
            return confirmPhoneCode(next, action);
        case ACTION.USER_SEND_PHONE_CODE:
            return sendPhoneCode(next, action);
        case ACTION.USER_SEND_PHONE_TOKEN:
            return sendPhoneToken(next, action);
        case ACTION.USER_EMAIL_CHANGE:
            return changeUserEmail(next, action);
        case ACTION.USER_SEND_CONFIRM_EMAIL:
            return sendConfirmEmail(next, action);
        default:
            return next(action);
    }

};
