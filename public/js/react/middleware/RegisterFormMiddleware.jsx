import ACTION from '../constants/actions';
import { customFetch, getUrlParameter } from '../Helpers';
import URLS from '../constants/urls';

export default () => next => action => {
    const { type, ...rest } = action;
    if(type !== ACTION.REGISTER_FORM_SENDING){
        return next(action);
    }

    next({
        ...rest,
        type: ACTION.REGISTER_FORM_SENDING_START
    });

    const { data } = action.payload;

    // customFetch(URLS.REGISTER, {
    //     method: 'POST',
    //     body: JSON.stringify(data),
    // }).then(response => {
    //     console.log('---', response);
    //     if(response.status === 'ERROR'){
    //         next({
    //             type: ACTION.REGISTER_FORM_SENDING_FAIL,
    //             payload: {
    //                 ...response
    //             }
    //         });
    //     } else {
    //         next({
    //             type: ACTION.REGISTER_FORM_SENDING_SUCCESS,
    //         });
    //
    //         let redirectUrlParams = getUrlParameter(window.location.search, 'redirectUrl');
    //         let loginUrl = '/login';
    //         if (redirectUrlParams !== '') {
    //             loginUrl = loginUrl + '?redirectUrl=' + redirectUrlParams;
    //         }
    //         window.location = loginUrl;
    //
    //     }
    // }).catch(error => {
    //     next({
    //         ...rest,
    //         type: ACTION.REGISTER_FORM_SENDING_SERVER_ERROR,
    //     });
    //     console.error('---', error);
    // });

};