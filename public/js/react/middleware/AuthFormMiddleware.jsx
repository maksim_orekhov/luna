import ACTION from '../constants/actions';
import { customFetch, getUrlParameter } from '../Helpers';
import URLS from '../constants/urls';

export default () => next => action => {
    const { type, ...rest } = action;
    if(type !== ACTION.AUTH_FORM_SENDING){
        return next(action);
    }

    next({
        ...rest,
        type: ACTION.AUTH_FORM_SENDING_START
    });

    const { data } = action.payload;



};