import ACTION from '../constants/actions';
import {customFetch, getInlineJSON} from '../Helpers';
import URLS from '../constants/urls';

function initForm(next, action) {
    next({
        ...action,
        type: ACTION.PURCHASE_FORM_INIT_START
    });
    const defaultValues = getInlineJSON('purchase-data-json');
    console.log('defaultValues---', defaultValues);
    return next({
        type: ACTION.PURCHASE_FORM_INIT_SUCCESS,
        payload: {
            defaultValues: {...defaultValues},
            hasDefaultValues: !Array.isArray(defaultValues)
        }
    });
}

export default () => next => action => {
    const { type, ...rest } = action;

    if(type === ACTION.PURCHASE_FORM_INIT){
        return initForm(next, action);
    }

    if(type !== ACTION.PURCHASE_FORM_SENDING){
        return next(action);
    }

    next({
        ...rest,
        type: ACTION.PURCHASE_FORM_SENDING_START
    });

    const { formData } = action.payload;

    customFetch(URLS.FORM.PURCHASE_CREATE, {
        method: 'POST',
        body: formData,
        processData: false,
        contentType: false,
    }).then(response => {
        console.log('---', response);
        if(response.status === 'ERROR'){
            next({
                type: ACTION.PURCHASE_FORM_SENDING_FAIL,
                payload: {
                    ...response.data
                }
            });
        } else {
            next({
                type: ACTION.PURCHASE_FORM_SENDING_SUCCESS,
            });
        }
    }).catch(error => {
        next({
            ...rest,
            type: ACTION.PURCHASE_FORM_SENDING_SERVER_ERROR,
        });
        console.error('---', error);
    });

};