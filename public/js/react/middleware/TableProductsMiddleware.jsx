import ACTION from '../constants/actions';

export default () => next => action => {
    const { type, ...rest } = action;

    if(type !== ACTION.TABLE_PRODUCTS_INIT){
        return next(action);
    }

};

