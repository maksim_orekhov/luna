import ACTION from '../constants/actions';
import URL from '../constants/urls';
import { getInlineJSON, getMetaCSRF, customFetch } from '../Helpers';

function initCSRF(next, action) {
    next({
        ...action,
        type: ACTION.CSRF_INIT_START
    });
    const CSRFValue = getMetaCSRF();
    if (CSRFValue && CSRFValue.length) {
        return next({
            ...action,
            type: ACTION.CSRF_INIT_SUCCESS,
            payload: { csrf: CSRFValue },
        });
    }

    return next({
        ...action,
        type: ACTION.CSRF_INIT_FAIL,
    });
}

function initCategories(next, action) {
    next({
        ...action,
        type: ACTION.CATEGORIES_INIT_START
    });
    const categories = getInlineJSON('categories-json');
    if (categories && categories.length) {
        next({
            ...action,
            type: ACTION.CATEGORIES_INIT_SUCCESS,
            payload: { categories }
        });
    }
}


export default () => (next) => (action) => {
    const { type } = action;
    switch (type) {
        case ACTION.CSRF_INIT:
            return initCSRF(next, action);
        case ACTION.CATEGORIES_INIT:
            return initCategories(next, action);
        default:
            return next(action);
    }
};
