import ACTION from '../constants/actions';
import {customFetch, setUrlParameter} from '../Helpers';

function searchStart(next, action) {
    const { payload: { url, query } } = action;
    let search_url = url;

    next({
        ...action,
        type: ACTION.SEARCH_FETCH_START
    });

    if (query.length) {
        search_url = setUrlParameter(search_url, 'q', query);
    }

    customFetch(search_url)
        .then((response) => {
            next({
                type: ACTION.SEARCH_FETCH_END,
                payload: {
                    query,
                    result: response.data,
                }
            });
        })
        .catch((error) => {
            console.error(error);
            next({
                type: ACTION.SEARCH_FETCH_END,
                payload: {
                    query,
                    result: [],
                }
            });
        });

}

export default () => (next) => (action) => {
    const { type } = action;
    switch (type) {
        case ACTION.SEARCH_FETCH:
            return searchStart(next, action);
        default:
            return next(action);
    }
};
