import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Timer from './Timer';
import ShowError from './ShowError';
import {SERVER_ERRORS, SERVER_ERRORS_MESSAGES} from './constants/server_errors';
import LoaderSpinner from './components/LoaderSpinner';

export default class EmailChangeAccept extends Component {

    state = {
        isUserAllowedEmailRequest: false,
        // Server_errors
        server_errors_messages: SERVER_ERRORS_MESSAGES,
        server_errors: SERVER_ERRORS,
    };

    componentWillReceiveProps(nextProps) {
        if (Object.keys(nextProps.serverErrors).length > 0) {
            this.TakeApartErrorFromServer(nextProps.serverErrors);
        }
    }

    toggleSmsTimer = () => {
        this.setState({
            isUserAllowedEmailRequest: true
        });
    };

    handleResendCode = () => {
        const { resendConfirmEmail, csrf } = this.props;
        this.setState({
            isUserAllowedEmailRequest: false,
        }, () => {
            resendConfirmEmail(JSON.stringify({
                csrf: csrf.value,
            })); 
        });
    };

    handlePrev = (ev) => {
        const { openEditEmailForm } = this.props;
        ev.preventDefault();
        openEditEmailForm();
    };

    render() {
        const { isUserAllowedEmailRequest, server_errors_messages, server_errors } = this.state;
        const { email, serverErrors, isSending } = this.props;
        return (
            <div className={`popup phone-accept-code-popup`}>
                <div className="form-popup">
                    <h4 className="popup-title">Подтверждение почты</h4>
                    <div className="control-form">
                        <input type="text" name="" value={email} disabled />
                    </div>
                    <div className="form-popup__info-text">
                        На вашу почту отправлено письмо. Следуйте инструкциям в письме.
                    </div>
                    <div className="form-popup__info-text">
                        <b>Не получили</b> письмо в течении десяти минут?<br />
                        Проверьте, пожалуйста, папку «Спам» и корректность введенной электронной почты.
                    </div>
                    {
                        isUserAllowedEmailRequest ?
                            <div className="form-popup__notice">
                                <button
                                    className="button-purple"
                                    onClick={this.handleResendCode}
                                    type="button"
                                >
                                        Получить письмо повторно
                                </button>
                            </div>
                            :
                            <div className="form-popup__notice">
                                    Запросить письмо повторно через <span className="form-popup__repeat-time-left">
                                    <Timer
                                        start={10}
                                        expired={this.toggleSmsTimer}
                                        isShowTime={true}
                                    />
                                </span> или <a href="#" className="form-popup__notice-link" onClick={this.handlePrev}>изменить почту</a>.
                            </div>
                    }
                    {
                        isSending &&
                        <LoaderSpinner isAbsolute={true} />
                    }
                    <ShowError messages={server_errors_messages} existing_errors={server_errors} />
                </div>
            </div>
        );
    }
}

EmailChangeAccept.propTypes = {
    serverErrors: PropTypes.object.isRequired,
    csrf: PropTypes.object.isRequired,
    email: PropTypes.string.isRequired,
    resendConfirmEmail: PropTypes.func.isRequired,
    openEditEmailForm: PropTypes.func.isRequired,
    isSending: PropTypes.bool.isRequired
};
