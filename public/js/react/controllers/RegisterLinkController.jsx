import React from 'react';

import FormRegister from '../containers/FormRegisterContainer';
import FormAuth from '../containers/FormAuthContainer';


export default class RegisterLinkController extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            current_form: 'login',
        };

        this.handleNext = this.handleNext.bind(this);
    }

    componentWillMount() {
        const { default_form } = this.props;

        if (default_form) {
            this.setState({
                current_form: default_form
            });
        }
    }

    handleNext(next_form) {
        this.setState({
            current_form: next_form
        });
    }

    render() {
        const { current_form } = this.state;
        return (
            <div>
                {{
                    login:
                        <FormAuth
                            handleNext={this.handleNext}
                        />,
                    register:
                        <FormRegister
                            handleNext={this.handleNext}
                        />
                }[current_form]}
            </div>
        );
    }

}

