import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import FormPhoneManagement from '../forms/FormPhoneManagement.jsx';
import FormPhoneConfirmCode from '../forms/FormPhoneConfirmCode.jsx';
import FormPhoneConfirmToken from '../forms/FormPhoneConfirmToken.jsx';
import PhoneChangeSuccess from '../PhoneChangeSuccess.jsx';
import {
    changePhone, loadCSRF, openUserProfileForm, confirmPhoneToken, confirmPhoneCode, sendPhoneCode, sendPhoneToken, confirmPrevPhoneToken
} from '../actions';


class PhoneChangeController extends React.Component {

    componentDidMount() {
        const { csrf, loadCSRF } = this.props;
        if (!csrf.isLoaded && !csrf.isLoading) {
            loadCSRF();
        }
    }

    openEditPhoneForm = () => {
        const { openForm } = this.props;
        openForm('phoneChange');
    };

    render() {
        const {
            nextPhone, csrf, currentPhone, serverErrorsPhone, phoneIsChanging, changePhone, currentForm, confirmPhoneCode,
            resendPhoneCode,  serverErrorsPhoneToken, serverErrorsPhoneCode, confirmPhoneToken, confirmPrevPhoneToken, phoneConfirmTokenIsChanging,
            phoneConfirmCodeIsChanging, resendPhoneToken, is_phone_verified
        } = this.props;
        console.log(this.props);

        if (csrf.isFailed || csrf.isLoading) {
            return null;
        }
        const form_type = currentPhone !== null ? 'edit' : 'add';
        return (
            <div>
                {
                    currentForm.phoneChange &&
                        <FormPhoneManagement
                            form_type={form_type}
                            form_label={currentPhone !== null ? 'Изменение телефона' : 'Добавление телефона'}
                            button_label={currentPhone !== null ? 'Изменить' : 'Добавить'}
                            serverErrors={serverErrorsPhone}
                            csrf={csrf}
                            currentPhone={currentPhone}
                            phoneIsChanging={phoneIsChanging}
                            changePhoneHandler={changePhone}
                        />
                }
                {
                    currentForm.phoneConfirmToken &&
                    <FormPhoneConfirmToken
                        form_type={form_type}
                        is_phone_verified={is_phone_verified}
                        phoneNumber={nextPhone}
                        serverErrors={serverErrorsPhoneToken}
                        csrf={csrf}
                        openPrevForm={this.openEditPhoneForm}
                        confirmTokenHandler={confirmPhoneToken}
                        confirmTokenSMSHandler={confirmPrevPhoneToken}
                        tokenIsSending={phoneConfirmTokenIsChanging}
                        resendPhoneToken={resendPhoneToken}
                    />
                }
                {
                    currentForm.phoneConfirmCode &&
                    <FormPhoneConfirmCode
                        phoneNumber={nextPhone}
                        is_phone_verified={is_phone_verified}
                        csrf={csrf}
                        serverErrors={serverErrorsPhoneCode}
                        confirmCodeHandler={confirmPhoneCode}
                        codeIsSending={phoneConfirmCodeIsChanging}
                        resendPhoneCode={resendPhoneCode}
                        openEditPhoneForm={this.openEditPhoneForm}
                    />
                }
                {
                    currentForm.phoneChangeSuccess &&
                    <PhoneChangeSuccess
                        form_type={form_type}
                        is_phone_verified={is_phone_verified}
                        currentPhone={nextPhone ? nextPhone : currentPhone}
                    />
                }
            </div>
        );
    }

}

PhoneChangeController.propTypes = {
    handleClosePopup: PropTypes.func,
    // data from redux store
    csrf: PropTypes.object.isRequired,
    currentPhone: PropTypes.string,
    nextPhone: PropTypes.string,
    is_phone_verified: PropTypes.bool,
    phoneIsChanging: PropTypes.bool.isRequired,
    phoneConfirmTokenIsChanging: PropTypes.bool.isRequired,
    phoneConfirmCodeIsChanging: PropTypes.bool.isRequired,
    serverErrorsPhone: PropTypes.object.isRequired,
    serverErrorsPhoneToken: PropTypes.object.isRequired,
    serverErrorsPhoneCode: PropTypes.object.isRequired,
    currentForm: PropTypes.object.isRequired,
    // redux dispatch
    loadCSRF: PropTypes.func.isRequired,
    changePhone: PropTypes.func.isRequired,
    openForm: PropTypes.func.isRequired,
    confirmPhoneToken: PropTypes.func.isRequired,
    confirmPhoneCode: PropTypes.func.isRequired,
    confirmPrevPhoneToken: PropTypes.func.isRequired,
    resendPhoneCode: PropTypes.func.isRequired,
    resendPhoneToken: PropTypes.func.isRequired,
};

PhoneChangeController.defaultProps = {
    is_phone_verified: false,
    handleClosePopup: () => {},
};

const mapStateToProps = (state) => {
    return {
        csrf: state.config.csrf,
        currentPhone: state.user.data.phone,
        nextPhone: state.user.data.nextPhone,
        phoneIsChanging: state.user.isChanging.phone,
        phoneConfirmTokenIsChanging: state.user.isChanging.phoneConfirmToken,
        phoneConfirmCodeIsChanging: state.user.isChanging.phoneConfirmCode,
        serverErrorsPhone: state.user.errors.phone,
        serverErrorsPhoneToken: state.user.errors.phoneConfirmToken,
        serverErrorsPhoneCode: state.user.errors.phoneConfirmCode,
        currentForm: state.user.isOpenForm,
        is_phone_verified: state.user.data.is_phone_verified,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        changePhone: bindActionCreators(changePhone, dispatch),
        loadCSRF: bindActionCreators(loadCSRF, dispatch),
        openForm: bindActionCreators(openUserProfileForm, dispatch),
        confirmPhoneToken: bindActionCreators(confirmPhoneToken, dispatch),
        confirmPrevPhoneToken: bindActionCreators(confirmPrevPhoneToken, dispatch),
        confirmPhoneCode: bindActionCreators(confirmPhoneCode, dispatch),
        resendPhoneCode: bindActionCreators(sendPhoneCode, dispatch),
        resendPhoneToken: bindActionCreators(sendPhoneToken, dispatch),
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(PhoneChangeController);