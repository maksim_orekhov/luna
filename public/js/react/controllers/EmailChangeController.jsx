import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import FormEmailChange from '../forms/FormEmailChange.jsx';
import EmailChangeAccept from '../EmailChangeAccept';
import { loadCSRF, changeUserEmail, sendConfirmEmail, openUserProfileForm } from '../actions';


class EmailChangeController extends React.Component {

    componentDidMount() {
        const { csrf, loadCSRF } = this.props;
        if (!csrf.isLoaded && !csrf.isLoading) {
            loadCSRF();
        }
    }

    openEmailChangeForm = () => {
        const { openUserProfileForm } = this.props;
        openUserProfileForm('emailChange');
    };

    render() {
        const {
            currentForm, csrf, currentEmail, errorsOnChanging, emailIsChanging, changeUserEmail,
            errorsOnAccepting, sendConfirmEmail, emailIsAccepting
        } = this.props;

        if (csrf.isFailed || csrf.isLoading) {
            return null;
        }

        return (
            <div>
                {
                    currentForm.emailChange &&
                    <FormEmailChange
                        csrf={csrf}
                        currentEmail={currentEmail}
                        serverErrors={errorsOnChanging}
                        isChanging={emailIsChanging}
                        submitHandler={changeUserEmail}
                    />
                }
                {
                    currentForm.emailChangeAccept &&
                    <EmailChangeAccept
                        email={currentEmail}
                        serverErrors={errorsOnAccepting}
                        csrf={csrf}
                        openEditEmailForm={this.openEmailChangeForm}
                        resendConfirmEmail={sendConfirmEmail}
                        isSending={emailIsAccepting}
                    />
                }
            </div>
        );
    }
}

EmailChangeController.propTypes = {
    // redux store data
    csrf: PropTypes.object.isRequired,
    currentForm: PropTypes.object.isRequired,
    currentEmail: PropTypes.string.isRequired,
    emailIsChanging: PropTypes.bool.isRequired,
    emailIsAccepting: PropTypes.bool.isRequired,
    errorsOnChanging: PropTypes.object.isRequired,
    errorsOnAccepting: PropTypes.object.isRequired,
    // redux dispatch
    loadCSRF: PropTypes.func.isRequired,
    changeUserEmail: PropTypes.func.isRequired,
    sendConfirmEmail: PropTypes.func.isRequired,
    openUserProfileForm: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
    return {
        csrf: state.config.csrf,
        currentForm: state.user.isOpenForm,
        currentEmail: state.user.data.email,
        emailIsChanging: state.user.isChanging.email,
        emailIsAccepting: state.user.isChanging.emailChangeAccept,
        errorsOnChanging: state.user.errors.emailChange,
        errorsOnAccepting: state.user.errors.emailChangeAccept,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        loadCSRF: bindActionCreators(loadCSRF, dispatch),
        changeUserEmail: bindActionCreators(changeUserEmail, dispatch),
        sendConfirmEmail: bindActionCreators(sendConfirmEmail, dispatch),
        openUserProfileForm: bindActionCreators(openUserProfileForm, dispatch),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(EmailChangeController);
