import ACTION from '../constants/actions';
import { getInlineJSON, customFetch } from '../Helpers';
import { USER_PURCHASES_STATUS_NAMES } from '../constants/names';
import URLS from '../constants/urls';


export const initUserPurchases = (user_purchases_status, query) => {
    return (dispatch) => {
        dispatch({ type: ACTION.USER_PURCHASES_INIT_START, payload: { user_purchases_status } });

        /**
         * @TODO: Если форма будет использоваться независимо расписать, сейчас за инит отвечает верхний компонент,
         * @TODO: который отвечает за инициализацию
         */
    };
};

export const getUserPurchasesItems = (user_purchases_status, url, query) => {
    // @TODO
    return (dispatch) => {
        dispatch({ type: ACTION.USER_PURCHASES_GET_ITEMS_START, payload: {user_purchases_status} });

        return customFetch(url)
            .then(data => {
                console.log('data', data);
                if (data.status === 'SUCCESS') {
                    const entities = data.data.purchases;
                    const paginator = data.data.purchasesPaginator;
                    dispatch({
                        type: ACTION.USER_PURCHASES_GET_ITEMS_SUCCESS,
                        payload: {
                            entities,
                            paginator,
                            user_purchases_status
                        }
                    });
                } else if (data.status === 'ERROR') {
                    dispatch({ type: ACTION.USER_PURCHASES_GET_ITEMS_FAIL, payload: {user_purchases_status} });
                }
            })
            .catch((error) => {
                console.log(error);
                dispatch({ type: ACTION.USER_PURCHASES_GET_ITEMS_FAIL, payload: {user_purchases_status} });
            });
    };
};

export const getUserPurchaseItem = (id, user_purchases_status) => {
    return (dispatch) => {
        dispatch({ type: ACTION.USER_PURCHASES_GET_ITEM_START, payload: { user_purchases_status } });
        customFetch(`${URLS.PURCHASES.BASE}/${id}`)
            .then((response) => {
                if (response.status === 'SUCCESS') {
                    const { purchase } = response.data;
                    dispatch({
                        type: ACTION.USER_PURCHASES_GET_ITEM_SUCCESS,
                        payload: {
                            id,
                            user_purchases_status,
                            purchase
                        }
                    });
                } else {
                    dispatch({ type: ACTION.USER_PURCHASES_GET_ITEM_FAIL, payload: { errors: response, user_purchases_status } });
                }
            }).catch((error) => {
                dispatch({ type: ACTION.USER_PURCHASES_GET_ITEM_FAIL, payload: { errors: error, user_purchases_status } });
                console.error(error);
            });
    };
};

export const getMoreUserPurchasesItems = (user_purchases_status, url, query) => {
    return (dispatch) => {
        dispatch({ type: ACTION.USER_PURCHASES_GET_MORE_ITEMS_START, payload: {user_purchases_status} });

        return customFetch(url)
            .then(data => {
                console.log('data', data);
                if (data.status === 'SUCCESS') {
                    const entities = data.data.purchases;
                    const paginator = data.data.purchasesPaginator;
                    dispatch({
                        type: ACTION.USER_PURCHASES_GET_MORE_ITEMS_SUCCESS,
                        payload: {
                            entities,
                            paginator,
                            user_purchases_status
                        }
                    });
                } else if (data.status === 'ERROR') {
                    dispatch({ type: ACTION.USER_PURCHASES_GET_MORE_ITEMS_FAIL, payload: {user_purchases_status} });
                }
            })
            .catch((error) => {
                console.log(error);
                dispatch({ type: ACTION.USER_PURCHASES_GET_MORE_ITEMS_FAIL, payload: {user_purchases_status} });
            });
    };
};