import ACTION from '../constants/actions';
import {customFetch, getCookie, getInlineJSON} from '../Helpers';


export const initTableUsersProducts = () => {
    return (dispatch) => {
        dispatch({ type: ACTION.TABLE_USERS_PRODUCTS_INIT_START });

        const products = getInlineJSON('products-json');
        const purchases_url = getInlineJSON('products_urls').user_purchases;
        const products_url = getInlineJSON('products_urls').user_products;
        const user_code = getInlineJSON('owner-code-json');
        const paginator = getInlineJSON('productsPaginator-json');
        const view_mode = getCookie('view_mode');

        if(products || products_url){
            dispatch({
                type: ACTION.TABLE_USERS_PRODUCTS_INIT_SUCCESS,
                payload: {
                    products,
                    view_mode,
                    user_code,
                    paginator,
                    purchases_url,
                    products_url
                }
            });
        } else {
            dispatch({
                type: ACTION.TABLE_USERS_PRODUCTS_INIT_FAIL
            });
        }
    };
};

export const getMoreUserProducts = (url, query) => {
    return (dispatch) => {
        dispatch({ type: ACTION.USERS_PRODUCTS_GET_MORE_ITEMS_START });

        return customFetch(url)
            .then(data => {
                console.log('data', data);
                if (data.status === 'SUCCESS') {
                    const products = data.data.products;
                    const paginator = data.data.productsPaginator;
                    dispatch({
                        type: ACTION.USERS_PRODUCTS_GET_MORE_ITEMS_SUCCESS,
                        payload: {
                            products,
                            paginator
                        }
                    });
                } else if (data.status === 'ERROR') {
                    dispatch({ type: ACTION.USERS_PRODUCTS_GET_MORE_ITEMS_FAIL });
                }
            })
            .catch((error) => {
                console.log(error);
                dispatch({ type: ACTION.USER_PRODUCTS_GET_MORE_ITEMS_FAIL });
            });
    };
};

export const getUsersProductsItemsBySearch = (url) => {
    return (dispatch) => {
        dispatch({ type: ACTION.USERS_PRODUCTS_GET_ITEMS_BY_SEARCH_START });

        return customFetch(url)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    const entities = data.data.products;
                    const paginator = data.data.productsPaginator;
                    dispatch({
                        type: ACTION.USERS_PRODUCTS_GET_ITEMS_BY_SEARCH_SUCCESS,
                        payload: {
                            entities,
                            paginator
                        }
                    });
                } else if (data.status === 'ERROR') {
                    dispatch({ type: ACTION.USERS_PRODUCTS_GET_ITEMS_BY_SEARCH_FAIL });
                }
            })
            .catch((error) => {
                console.log(error);
                dispatch({ type: ACTION.USERS_PRODUCTS_GET_ITEMS_BY_SEARCH_FAIL });
            });
    };
};