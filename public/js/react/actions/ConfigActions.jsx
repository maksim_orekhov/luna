import ACTION from '../constants/actions';
import URL from '../constants/urls';
import {customFetch, getInlineJSON} from '../Helpers';


export const loadCategories = () => {
    return {
        type: ACTION.CATEGORIES_INIT,
    };
};

export const fetchSubcategories = (categoryId) => {
    return (dispatch, getState) => {
        const parentCategory = getState().config.categories.entities.find(category => category.id === categoryId);
        if (!parentCategory || Array.isArray(parentCategory.subcategories) && parentCategory.subcategories.length > 0) {
            return false;
        }
        
        dispatch({ type: ACTION.SUBCATEGORIES_FETCH_START });
        customFetch(`${URL.CATEGORIES_LIST}?parent=${categoryId}`)
            .then((response) => {
                dispatch({
                    type: ACTION.SUBCATEGORIES_FETCH_SUCCESS,
                    payload: {
                        categoryId,
                        subcategories: response.data
                    }
                });
            })
            .catch((error) => {
                dispatch({ type: ACTION.SUBCATEGORIES_FETCH_FAIL });
                console.error(error);
            });
    };
};

export const loadCSRF = () => {
    return {
        type: ACTION.CSRF_INIT
    };
};

export const getProductsItemsBySearch = (url, is_query_empty) => {
    return (dispatch) => {
        dispatch({ type: ACTION.PRODUCTS_GET_ITEMS_BY_SEARCH_START });
        return customFetch(url)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    const entities = data.data.products;
                    const paginator = data.data.paginator;
                    dispatch({
                        type: ACTION.PRODUCTS_GET_ITEMS_BY_SEARCH_SUCCESS,
                        payload: {
                            entities,
                            paginator,
                            is_query_empty
                        }
                    });
                } else if (data.status === 'ERROR') {
                    dispatch({ type: ACTION.PRODUCTS_GET_ITEMS_BY_SEARCH_FAIL });
                }
            })
            .catch((error) => {
                console.log(error);
                dispatch({ type: ACTION.PRODUCTS_GET_ITEMS_BY_SEARCH_FAIL });
            });
    };
};

export const getPurchasesItemsBySearch = (url) => {
    return (dispatch) => {
        dispatch({ type: ACTION.PURCHASES_GET_ITEMS_BY_SEARCH_START });
        return customFetch(url)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    const entities = data.data.purchases;
                    const paginator = data.data.paginator;
                    dispatch({
                        type: ACTION.PURCHASES_GET_ITEMS_BY_SEARCH_SUCCESS,
                        payload: {
                            entities,
                            paginator
                        }
                    });
                } else if (data.status === 'ERROR') {
                    dispatch({ type: ACTION.PURCHASES_GET_ITEMS_BY_SEARCH_FAIL });
                }
            })
            .catch((error) => {
                console.log(error);
                dispatch({ type: ACTION.PURCHASES_GET_ITEMS_BY_SEARCH_FAIL });
            });
    };
};

export const initMobileMenu = () => {
    return (dispatch) => {
        dispatch({ type: ACTION.MOBILE_MENU_INIT_START });

        const data = getInlineJSON('init-menu-json');

        if (data) {
            const { catalog_name, catalog_root_url, categories, product_create_url, product_root_url, purchase_create_url, purchase_root_url } = data;
            dispatch({
                type: ACTION.MOBILE_MENU_INIT_SUCCESS,
                payload: {
                    catalog_name,
                    catalog_root_url,
                    categories,
                    product_create_url,
                    product_root_url,
                    purchase_create_url,
                    purchase_root_url
                }
            });
        } else {
            console.error('Данные для мобильного меню не получены');
            dispatch({ type: ACTION.MOBILE_MENU_INIT_FAIL, payload: {errors: 'Ошибка инициализации'} });
        }
    };
};