import ACTION from '../constants/actions';
import { getInlineJSON, customFetch } from '../Helpers';


export const initOperatorPurchasesTabs = () => {
    return (dispatch) => {
        dispatch({ type: ACTION.OPERATOR_PURCHASES_TABS_INIT_START });

        const data = getInlineJSON('purchases_collections_json');

        let moderation_waiting_entities = [];
        let moderation_waiting_paginator = {};
        if (data.hasOwnProperty('waiting_moderation') && data.waiting_moderation) {
            if (data.waiting_moderation.hasOwnProperty('purchases') && data.waiting_moderation.purchases) {
                moderation_waiting_entities = data.waiting_moderation.purchases;
            }
            if (data.waiting_moderation.hasOwnProperty('purchasesPaginator') && data.waiting_moderation.purchasesPaginator) {
                moderation_waiting_paginator = data.waiting_moderation.purchasesPaginator;
            }
        }

        let moderation_declined_entities = [];
        let moderation_declined_paginator = {};
        if (data.hasOwnProperty('declined') && data.declined) {
            if (data.declined.hasOwnProperty('purchases') && data.declined.purchases) {
                moderation_declined_entities = data.declined.purchases;
            }
            if (data.declined.hasOwnProperty('purchasesPaginator') && data.declined.purchasesPaginator) {
                moderation_declined_paginator = data.declined.purchasesPaginator;
            }
        }

        let moderation_published_entities = [];
        let moderation_published_paginator = {};
        if (data.hasOwnProperty('public') && data.public) {
            if (data.public.hasOwnProperty('purchases') && data.public.purchases) {
                moderation_published_entities = data.public.purchases;
            }
            if (data.public.hasOwnProperty('purchasesPaginator') && data.public.purchasesPaginator) {
                moderation_published_paginator = data.public.purchasesPaginator;
            }
        }

        let moderation_archive_entities = [];
        let moderation_archive_paginator = {};
        if (data.hasOwnProperty('archival') && data.archival) {
            if (data.archival.hasOwnProperty('purchases') && data.archival.purchases) {
                moderation_archive_entities = data.archival.purchases;
            }
            if (data.archival.hasOwnProperty('purchasesPaginator') && data.archival.purchasesPaginator) {
                moderation_archive_paginator = data.archival.purchasesPaginator;
            }
        }

        console.log(data);
        if (data) {
            dispatch({
                type: ACTION.OPERATOR_PURCHASES_TABS_INIT_SUCCESS,
                payload: {
                    moderation_waiting_entities,
                    moderation_waiting_paginator,
                    moderation_declined_entities,
                    moderation_declined_paginator,
                    moderation_published_entities,
                    moderation_published_paginator,
                    moderation_archive_entities,
                    moderation_archive_paginator
                }
            });
        } else {
            dispatch({ type: ACTION.OPERATOR_PURCHASES_TABS_INIT_FAIL, payload: {errors: 'Ошибка инициализации'} });
        }
    };
};