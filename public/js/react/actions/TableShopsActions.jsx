import ACTION from '../constants/actions';
import {customFetch, getCookie, getInlineJSON} from '../Helpers';


export const initTableShops = () => {
    return (dispatch) => {
        dispatch({ type: ACTION.TABLE_SHOPS_INIT_START });

        const shops = getInlineJSON('shops-json');

        if(shops){
            dispatch({
                type: ACTION.TABLE_SHOPS_INIT_SUCCESS,
                payload: {
                    shops
                }
            });
        } else {
            dispatch({
                type: ACTION.TABLE_SHOPS_INIT_FAIL
            });
        }
    };
};

export const getShopsPaginatorData = () => {
    return (dispatch) => {
        dispatch({ type: ACTION.TABLE_SHOPS_PAGINATOR_INIT_START });

        return customFetch(window.location)
            .then((response) => {
                const { data, status } = response;
                if (status === 'SUCCESS') {
                    const paginator = Array.isArray(data.paginator) ? {} : data.paginator;
                    dispatch({
                        type: ACTION.TABLE_SHOPS_PAGINATOR_INIT_SUCCESS,
                        payload: {
                            paginator
                        }
                    });
                } else {
                    dispatch({
                        type: ACTION.TABLE_SHOPS_PAGINATOR_INIT_FAIL
                    });
                }
            })
            .catch(error => console.log(error));
    };
};

export const showMoreShops = (url) => {
    return (dispatch) => {
        dispatch({ type: ACTION.SHOW_MORE_SHOPS_START });

        return customFetch(url)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    const collection = data.data.shops;
                    const paginator = data.data.paginator;
                    dispatch({
                        type: ACTION.SHOW_MORE_SHOPS_SUCCESS,
                        payload: {
                            collection,
                            paginator
                        }
                    });
                }
            })
            .catch((error) => console.log(error));
    };
};

export const getSearchResult = ({url, query}) => {
    return {
        type: ACTION.SEARCH_FETCH,
        payload: {
            url,
            query
        }
    };
};