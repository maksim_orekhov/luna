import ACTION from '../constants/actions';
import { customFetch, getInlineJSON, removeFromSessionStorage, getFromSessionStorage } from '../Helpers';
import URLS from '../constants/urls';

export const sendFormPurchase = ({ formData, formType = 'create', purchaseId }) => {
    return (dispatch) => {
        dispatch({ type: ACTION.PURCHASE_FORM_SENDING_START });

        return customFetch(formType === 'create' ? URLS.FORM.PURCHASE_CREATE : URLS.FORM.PURCHASE_EDIT_PREVIEW(purchaseId), {
            method: 'POST',
            body: formData,
            processData: false,
            contentType: false,
        }).then((response) => {
            const { status, data } = response;
            if(status === 'ERROR'){
                dispatch({
                    type: ACTION.PURCHASE_FORM_SENDING_FAIL,
                    payload: {
                        ...response
                    }
                });
            } else {
                dispatch({ type: ACTION.PURCHASE_FORM_SENDING_SUCCESS, });
                dispatch({
                    type: ACTION.PURCHASE_FORM_SHOW_PREVIEW,
                    payload: {
                        data: {
                            ...data['purchase'],
                            user: data['user'],
                        }
                    }
                });
            }
        }).catch(error => {
            dispatch({
                type: ACTION.PURCHASE_FORM_SENDING_SERVER_ERROR,
            });
            console.error('---', error);
        });
    };
};

export const sendConfirmationPurchaseForm = ({ formData, confirmUrl, form_name = null })  => {
    return (dispatch) => {
        dispatch({ type: ACTION.PURCHASE_PREVIEW_CONFIRM_SENDING_START });
        customFetch(confirmUrl, {
            method: 'POST',
            body: formData,
            processData: false,
            contentType: false,
        }).then((response) => {
            if (response.status.toLowerCase() === 'error') {
                if(response.code === 'ERROR_NOT_AUTHORIZED') {
                    window.location = '/login?redirectUrl=/purchase/create';
                }
                dispatch({ type: ACTION.PURCHASE_PREVIEW_CONFIRM_SENDING_FAIL, payload: { errors: response } });
            } else {
                window.sessionStorage.setItem('profileMessage', 'purchase_moderation');
                if (form_name) removeFromSessionStorage(form_name);
                window.location = URLS.PROFILE_PURCHASE;
            }
        }).catch((error) => {
            dispatch({ type: ACTION.PURCHASE_PREVIEW_CONFIRM_SENDING_FAIL, payload: { errors: error } });
            console.error('---', error);
        });
    };
};

export const initFormPurchase = (form_name) => {
    return (dispatch) => {
        dispatch({ type: ACTION.PURCHASE_FORM_INIT_START });

        let defaultValues = getInlineJSON('purchase-data-json');

        if (!defaultValues || (Array.isArray(defaultValues) && !defaultValues.length)) {
            const session_data = getFromSessionStorage(form_name);
            if (session_data) defaultValues = JSON.parse(session_data);
        }

        console.log('init', defaultValues);

        return dispatch({
            type: ACTION.PURCHASE_FORM_INIT_SUCCESS,
            payload: {
                defaultValues: {...defaultValues},
                hasDefaultValues: !Array.isArray(defaultValues)
            }
        });
    };
};

export const closePurchasePreview = () => {
    return { type: ACTION.PURCHASE_FORM_HIDE_PREVIEW, };
};
