import ACTION from '../constants/actions';
import {customFetch, getCookie, getInlineJSON} from '../Helpers';


export const initTableUsersPurchases = () => {
    return (dispatch) => {
        dispatch({ type: ACTION.TABLE_USERS_PURCHASES_INIT_START });

        const purchases = getInlineJSON('purchases_data_json').purchases;
        const url = getInlineJSON('purchases_data_json').public_purchase_collection_url ? getInlineJSON('purchases_data_json').public_purchase_collection_url : '';
        const paginator = getInlineJSON('purchasesPaginator-json');
        const products_url = getInlineJSON('purchases_data_json').user_public_products_url ? getInlineJSON('purchases_data_json').user_public_products_url : '';

        if(purchases || url){
            dispatch({
                type: ACTION.TABLE_USERS_PURCHASES_INIT_SUCCESS,
                payload: {
                    purchases,
                    paginator,
                    url,
                    products_url
                }
            });
        } else {
            dispatch({
                type: ACTION.TABLE_USERS_PURCHASES_INIT_FAIL
            });
        }
    };
};

export const getMoreUserPurchases = (url, query) => {
    return (dispatch) => {
        dispatch({ type: ACTION.USERS_PURCHASES_GET_MORE_ITEMS_START });

        return customFetch(url)
            .then(data => {
                console.log('data', data);
                if (data.status === 'SUCCESS') {
                    const purchases = data.data.purchases;
                    const paginator = data.data.purchasesPaginator;
                    dispatch({
                        type: ACTION.USERS_PURCHASES_GET_MORE_ITEMS_SUCCESS,
                        payload: {
                            purchases,
                            paginator
                        }
                    });
                } else if (data.status === 'ERROR') {
                    dispatch({ type: ACTION.USERS_PURCHASES_GET_MORE_ITEMS_FAIL });
                }
            })
            .catch((error) => {
                console.log(error);
                dispatch({ type: ACTION.USER_PURCHASES_GET_MORE_ITEMS_FAIL });
            });
    };
};

export const getUsersPurchasesItemsBySearch = (url) => {
    return (dispatch) => {
        dispatch({ type: ACTION.USERS_PURCHASES_GET_ITEMS_BY_SEARCH_START });

        return customFetch(url)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    const entities = data.data.purchases;
                    const paginator = data.data.purchasesPaginator;
                    dispatch({
                        type: ACTION.USERS_PURCHASES_GET_ITEMS_BY_SEARCH_SUCCESS,
                        payload: {
                            entities,
                            paginator
                        }
                    });
                } else if (data.status === 'ERROR') {
                    dispatch({ type: ACTION.USERS_PURCHASES_GET_ITEMS_BY_SEARCH_FAIL });
                }
            })
            .catch((error) => {
                console.log(error);
                dispatch({ type: ACTION.USERS_PURCHASES_GET_ITEMS_BY_SEARCH_FAIL });
            });
    };
};