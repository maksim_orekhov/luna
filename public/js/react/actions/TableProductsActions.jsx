import ACTION from '../constants/actions';
import {customFetch, getCookie, getInlineJSON} from '../Helpers';


export const initTableProducts = (is_landing) => {
    return (dispatch) => {
        dispatch({ type: ACTION.TABLE_PRODUCTS_INIT_START });

        const products = getInlineJSON('products-json');
        const view_mode = getCookie('view_mode');
        const breadcrumbs = getInlineJSON('breadcrumbs-json');

        if(products){
            dispatch({
                type: ACTION.TABLE_PRODUCTS_INIT_SUCCESS,
                payload: {
                    products,
                    view_mode,
                    breadcrumbs
                }
            });
        } else {
            if (is_landing) {
                const products = [];
                dispatch({
                    type: ACTION.TABLE_PRODUCTS_INIT_SUCCESS,
                    payload: {
                        products,
                        view_mode,
                        breadcrumbs
                    }
                });
            }
            dispatch({
                type: ACTION.TABLE_PRODUCTS_INIT_FAIL
            });
        }
    };
};

export const getPaginatorData = (is_landing) => {
    return (dispatch) => {
        dispatch({ type: ACTION.TABLE_PRODUCTS_PAGINATOR_INIT_START });
        const url = is_landing ? '/product/rossiia' : window.location;

        return customFetch(url)
            .then((response) => {
                const { data, status } = response;
                if (status === 'SUCCESS') {
                    const paginator = Array.isArray(data.paginator) ? {} : data.paginator;
                    dispatch({
                        type: ACTION.TABLE_PRODUCTS_PAGINATOR_INIT_SUCCESS,
                        payload: {
                            paginator
                        }
                    });
                } else {
                    dispatch({
                        type: ACTION.TABLE_PRODUCTS_PAGINATOR_INIT_FAIL
                    });
                }
            })
            .catch(error => console.log(error));
    };
};

export const showMoreProducts = (url) => {
    return (dispatch) => {
        dispatch({ type: ACTION.SHOW_MORE_PRODUCTS_START });

        return customFetch(url)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    const collection = data.data.products;
                    const paginator = data.data.paginator;
                    dispatch({
                        type: ACTION.SHOW_MORE_PRODUCTS_SUCCESS,
                        payload: {
                            collection,
                            paginator
                        }
                    });
                }
            })
            .catch((error) => console.log(error));
    };
};

export const getSearchResult = ({url, query}) => {
    return {
        type: ACTION.SEARCH_FETCH,
        payload: {
            url,
            query
        }
    };
};