import ACTION from '../constants/actions';
import {customFetch, getCookie, getInlineJSON} from '../Helpers';


export const initTablePurchases = () => {
    return (dispatch) => {
        dispatch({ type: ACTION.TABLE_PURCHASES_INIT_START });

        const purchases = getInlineJSON('purchases-json');
        const breadcrumbs = getInlineJSON('breadcrumbs-json');

        if(purchases){
            dispatch({
                type: ACTION.TABLE_PURCHASES_INIT_SUCCESS,
                payload: {
                    purchases,
                    breadcrumbs
                }
            });
        } else {
            dispatch({
                type: ACTION.TABLE_PURCHASES_INIT_FAIL
            });
        }
    };
};

export const getPurchasesPaginatorData = () => {
    return (dispatch) => {
        dispatch({ type: ACTION.TABLE_PURCHASES_PAGINATOR_INIT_START });

        return customFetch(window.location)
            .then((response) => {
                const { data, status } = response;
                if (status === 'SUCCESS') {
                    const paginator = Array.isArray(data.paginator) ? {} : data.paginator;
                    dispatch({
                        type: ACTION.TABLE_PURCHASES_PAGINATOR_INIT_SUCCESS,
                        payload: {
                            paginator
                        }
                    });
                } else {
                    dispatch({
                        type: ACTION.TABLE_PURCHASES_PAGINATOR_INIT_FAIL
                    });
                }
            })
            .catch(error => console.log(error));
    };
};

export const showMorePurchases = (url) => {
    return (dispatch) => {
        dispatch({ type: ACTION.SHOW_MORE_PURCHASES_START });

        return customFetch(url)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    const collection = data.data.purchases;
                    const paginator = data.data.paginator;
                    dispatch({
                        type: ACTION.SHOW_MORE_PURCHASES_SUCCESS,
                        payload: {
                            collection,
                            paginator
                        }
                    });
                }
            })
            .catch((error) => console.log(error));
    };
};

export const getSearchResult = ({url, query}) => {
    return {
        type: ACTION.SEARCH_FETCH,
        payload: {
            url,
            query
        }
    };
};