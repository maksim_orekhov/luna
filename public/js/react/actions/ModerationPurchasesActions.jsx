import ACTION from '../constants/actions';
import { getInlineJSON, customFetch } from '../Helpers';
import { MODERATION_PURCHASES_STATUS_NAMES } from '../constants/names';
import URLS from '../constants/urls';


export const initModerationPurchases = (moderation_purchases_status, query) => {
    return (dispatch) => {
        dispatch({ type: ACTION.MODERATION_PURCHASES_INIT_START, payload: { moderation_purchases_status } });

        /**
         * @TODO: Если форма будет использоваться независимо расписать, сейчас за инит отвечает верхний компонент,
         * @TODO: который отвечает за инициализацию
         */

        /* const entities = getInlineJSON('products-collections-json');
        if (entities && entities.length > 0) {
            dispatch({
                type: ACTION.MODERATION_PURCHASES_INIT_SUCCESS,
                payload: {
                    moderation_purchases_status,
                    paginator: {},
                    entities
                }
            });
        } else {
            dispatch({ type: ACTION.MODERATION_PURCHASES_INIT_FAIL, payload: { moderation_purchases_status} });
        }*/
    };
};

export const getModerationPurhasesItems = (moderation_purchases_status, page, per_page, query) => {
    return (dispatch) => {
        dispatch({ type: ACTION.MODERATION_PURCHASES_GET_ITEMS_START, payload: { moderation_purchases_status } });

        const urls = {
            [MODERATION_PURCHASES_STATUS_NAMES.MODERATION_PURCHASES_ARE_WAITING]: URLS.MODERATION.MODERATION_PURCHASES_ARE_WAITING,
            [MODERATION_PURCHASES_STATUS_NAMES.MODERATION_PURCHASES_ARE_DECLINED]: URLS.MODERATION.MODERATION_PURCHASES_ARE_DECLINED,
            [MODERATION_PURCHASES_STATUS_NAMES.MODERATION_PURCHASES_ARE_PUBLISHED]: URLS.PURCHASES.PURCHASES_ARE_PUBLISHED,
            [MODERATION_PURCHASES_STATUS_NAMES.MODERATION_PURCHASES_ARCHIVE]: URLS.PURCHASES.PURCHASES_ARCHIVE,
        };

        customFetch(`${urls[moderation_purchases_status]}?page=${page}&per_page=${per_page}`)
            .then((response) => {
                if (response.status === 'SUCCESS') {
                    const { purchases, purchasesPaginator } = response.data;

                    dispatch({
                        type: ACTION.MODERATION_PURCHASES_GET_ITEMS_SUCCESS,
                        payload: {
                            entities: purchases,
                            paginator: purchasesPaginator,
                            moderation_purchases_status
                        }
                    });
                } else {
                    dispatch({ type: ACTION.MODERATION_PURCHASES_GET_ITEMS_FAIL, payload: { errors: response, moderation_purchases_status } });
                }
            }).catch((error) => {
                dispatch({ type: ACTION.MODERATION_PURCHASES_GET_ITEMS_FAIL, payload: { errors: error, moderation_purchases_status } });
                console.error(error);
            });
    };
};

export const getModerationPurchaseItem = (id, moderation_purchases_status) => {
    return (dispatch) => {
        dispatch({ type: ACTION.MODERATION_PURCHASES_GET_ITEM_START, payload: { moderation_purchases_status } });
        customFetch(`/purchase/moderation/${id}`)
            .then((response) => {
                if (response.status === 'SUCCESS') {
                    const { purchase } = response.data;
                    dispatch({
                        type: ACTION.MODERATION_PURCHASES_GET_ITEM_SUCCESS,
                        payload: {
                            id,
                            moderation_purchases_status,
                            purchase
                        }
                    });
                } else {
                    dispatch({ type: ACTION.MODERATION_PURCHASES_GET_ITEM_FAIL, payload: { errors: response, moderation_purchases_status } });
                }
            }).catch((error) => {
                dispatch({ type: ACTION.MODERATION_PURCHASES_GET_ITEM_FAIL, payload: { errors: error, moderation_purchases_status } });
                console.error(error);
            });
    };
};