import ACTION from '../constants/actions';
import {customFetch, getInlineJSON} from '../Helpers';
import URL from '../constants/urls';

export const loadUserInformation = () => {
    return (dispatch) => {
        dispatch({ type: ACTION.USER_LOAD_START });

        const user = getInlineJSON('user-json');
        if (!user) {
            return dispatch({ type: ACTION.USER_LOAD_FAIL });
        }

        dispatch({
            type: ACTION.USER_LOAD_SUCCESS,
            payload: {
                data: user
            }
        });
    };
};

export const changeAvatar = (formData) => {
    return (dispatch) => {
        dispatch({ type: ACTION.USER_DATA_CHANGE_START, payload: { fieldName: 'avatar' } });

        return customFetch(URL.USER_AVATAR_CHANGE, {
            method: 'POST',
            body: formData,
            processData: false,
            contentType: false
        }).then((response) => {
            if (response.status === 'SUCCESS') {
                dispatch({
                    type: ACTION.USER_AVATAR_CHANGE_SUCCESS,
                    payload: { avatar: `${response.data.user.avatar}` },
                });
            } else {
                dispatch({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'avatar' } });
            }
        }).catch((error) => {
            dispatch({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'avatar' } });
            console.error(error);
        });
    };
};

export const openUserProfileForm = (formName) => {
    return {
        type: ACTION.USER_PROFILE_FORM_OPEN,
        payload: { formName }
    };
};
export const closeUserProfileForm = (formName) => {
    return {
        type: ACTION.USER_PROFILE_FORM_CLOSE,
        payload: { formName }
    };
};

export const changeUserName = (formData) => {
    return (dispatch) => {
        dispatch({ type: ACTION.USER_DATA_CHANGE_START, payload: { fieldName: 'name' } });

        return customFetch(URL.PROFILE, {
            method: 'POST',
            body: formData
        }).then((response) => {
            if (response.status === 'SUCCESS') {
                dispatch({
                    type: ACTION.USER_NAME_CHANGE_SUCCESS,
                    payload: {
                        name: response.data.user.name
                    }
                });
                $('.js-user-name-changed').text(response.data.user.name);
            } else {
                dispatch({
                    type: ACTION.USER_NAME_CHANGE_FAIL,
                    payload: {
                        errors: response
                    }
                });
            }
        }).catch((error) => {
            dispatch({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'name' } });
            console.error(error);
        });
    };
};

export const changePhone = (formData, formType = 'add', is_email_confirmed = true) => {
    return (dispatch) => {
        dispatch({ type: ACTION.USER_DATA_CHANGE_START, payload: { fieldName: 'phone' } });

        return customFetch(formType === 'add' ? URL.PHONE.CREATE : URL.PHONE.EDIT, {
            method: 'POST',
            body: formData,
        }).then((response) => {
            if (response.status.toLowerCase() === 'error') {
                dispatch({
                    type: ACTION.USER_PHONE_CHANGE_FAIL,
                    payload: {
                        fieldName: 'phone',
                        errors: response
                    }
                });
            } else {
                dispatch({
                    type: ACTION.USER_PHONE_CHANGE_SUCCESS,
                    payload: {
                        phone: JSON.parse(formData).new_phone.replace('+', ''),
                    }
                });
                dispatch(openUserProfileForm(is_email_confirmed ? 'phoneConfirmToken' : 'phoneConfirmCode'));
            }
        }).catch((error) => {
            dispatch({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'phone' } });
            console.error(error);
        });
    };
};

export const confirmPhoneToken = (formData, formType = 'add') => {
    return (dispatch) => {
        dispatch({ type: ACTION.USER_PHONE_CONFIRM_TOKEN });
        dispatch({ type: ACTION.USER_DATA_CHANGE_START, payload: { fieldName: 'phoneConfirmToken' } });

        return customFetch(formType === 'add' ? URL.PHONE.CONFIRM_CODE : URL.PHONE.CONFIRM_TOKEN, {
            method: 'POST',
            body: formData,
        }).then((response) => {
            if (response.status.toLowerCase() === 'success') {
                dispatch({ type: ACTION.USER_PHONE_CONFIRM_TOKEN_SUCCESS, payload: { formType: formType } });
                dispatch(openUserProfileForm( formType === 'add' ? 'phoneChangeSuccess' : 'phoneConfirmCode'));
            } else {
                dispatch({
                    type: ACTION.USER_PHONE_CONFIRM_TOKEN_FAIL,
                    payload: { errors: response }
                });
            }
        }).catch((error) => {
            dispatch({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'phoneConfirmToken' } });
            console.error(error);
        });
    };
};

export const confirmPrevPhoneToken = (formData, formType = 'add') => {
    console.log('aaaa');
    return (dispatch) => {
        // dispatch({ type: ACTION.USER_PHONE_CONFIRM_TOKEN });
        // dispatch({ type: ACTION.USER_DATA_CHANGE_START, payload: { fieldName: 'phoneConfirmToken' } });
        //
        // return customFetch(formType === 'add' ? URL.PHONE.CONFIRM_CODE : URL.PHONE.CONFIRM_TOKEN, {
        //     method: 'POST',
        //     body: formData,
        // }).then((response) => {
        //     if (response.status.toLowerCase() === 'success') {
        //         dispatch({ type: ACTION.USER_PHONE_CONFIRM_TOKEN_SUCCESS, payload: { formType: formType } });
        //         dispatch(openUserProfileForm( formType === 'add' ? 'phoneChangeSuccess' : 'phoneConfirmCode'));
        //     } else {
        //         dispatch({
        //             type: ACTION.USER_PHONE_CONFIRM_TOKEN_FAIL,
        //             payload: { errors: response }
        //         });
        //     }
        // }).catch((error) => {
        //     dispatch({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'phoneConfirmToken' } });
        //     console.error(error);
        // });
    };
};

export const confirmPhoneCode = (formData) => {
    return (dispatch) => {
        dispatch({ type: ACTION.USER_PHONE_CONFIRM_CODE });
        dispatch({ type: ACTION.USER_DATA_CHANGE_START, payload: { fieldName: 'phoneConfirmCode' } });
        return customFetch(URL.PHONE.CONFIRM_CODE, {
            method: 'POST',
            body: formData
        }).then((response) => {
            if (response.status.toLowerCase() === 'success') {
                dispatch({ type: ACTION.USER_PHONE_CONFIRM_CODE_SUCCESS });
                dispatch(openUserProfileForm('phoneChangeSuccess'));
            } else {
                dispatch({
                    type: ACTION.USER_PHONE_CONFIRM_CODE_FAIL,
                    payload: { errors: response }
                });
            }
        }).catch((error) => {
            dispatch({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'phoneConfirmCode' } });
            console.error(error);
        });
    };
};

export const sendPhoneCode = (formData) => {
    return (dispatch) => {
        dispatch({ type: ACTION.USER_SEND_PHONE_CODE });
        dispatch(openUserProfileForm('phoneConfirmCode'));
        dispatch({ type: ACTION.USER_DATA_CHANGE_START, payload: { fieldName: 'phoneConfirmCode' } });

        return customFetch(URL.PHONE.RESEND_CODE, {
            method: 'POST',
            body: formData
        }).then((response) => {
            if (response.status.toLowerCase() === 'success') {
                dispatch({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'phoneConfirmCode' } });
            } else {
                dispatch({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'phoneConfirmCode' } });
            }
        }).catch((error) => {
            dispatch({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'phoneConfirmCode' } });
            console.error(error);
        });
    };
};

export const sendPhoneToken = (formData) => {
    return (dispatch) => {
        dispatch({ type: ACTION.USER_SEND_PHONE_TOKEN });
        dispatch(openUserProfileForm('phoneConfirmToken'));
        dispatch({ type: ACTION.USER_DATA_CHANGE_START, payload: { fieldName: 'phoneConfirmToken' } });

        return customFetch(URL.PHONE.RESEND_TOKEN, {
            method: 'POST',
            body: formData
        }).then((response) => {
            if (response.status.toLowerCase() === 'success') {
                dispatch({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'phoneConfirmToken' } });
            } else {
                dispatch({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'phoneConfirmToken' } });
            }
        }).catch((error) => {
            dispatch({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'phoneConfirmToken' } });
            console.error(error);
        });
    };
};

export const changeUserEmail = (formData, newEmail) => {
    return (dispatch) => {
        dispatch({ type: ACTION.USER_EMAIL_CHANGE });
        dispatch({ type: ACTION.USER_DATA_CHANGE_START, payload: { fieldName: 'email' } });

        return customFetch(URL.EMAIL.EDIT, {
            method: 'POST',
            body: formData
        }).then((response) => {
            if (response.status.toLowerCase() === 'success') {
                dispatch({
                    type: ACTION.USER_EMAIL_CHANGE_SUCCESS,
                    payload: {
                        email: newEmail,
                    },
                });
                dispatch(openUserProfileForm('emailChangeAccept'));
            } else {
                dispatch({
                    type: ACTION.USER_EMAIL_CHANGE_FAIL,
                    payload: {
                        errors: response
                    }
                });
            }
        }).catch((error) => {
            dispatch({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'email' } });
            console.error(error);
        });
    };
};

export const sendConfirmEmail = ( formData ) => {
    return (dispatch) => {
        dispatch({ type: ACTION.USER_SEND_CONFIRM_EMAIL });
        dispatch(openUserProfileForm('emailChangeAccept'));
        dispatch({ type: ACTION.USER_DATA_CHANGE_START, payload: { fieldName: 'emailChangeAccept' } });
        customFetch(URL.EMAIL.RESEND, {
            method: 'POST',
            body: formData
        }).then((response) => {
            if (response.status.toLowerCase() === 'success') {
                dispatch({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'emailChangeAccept' } });
            } else {
                dispatch({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'emailChangeAccept' } });
            }
        }).catch((error) => {
            dispatch({ type: ACTION.USER_DATA_CHANGE_END, payload: { fieldName: 'emailChangeAccept' } });
            console.error(error);
        });
    };
};
