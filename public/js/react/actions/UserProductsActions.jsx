import ACTION from '../constants/actions';
import { getInlineJSON, customFetch } from '../Helpers';
import { USER_PRODUCTS_STATUS_NAMES } from '../constants/names';
import URLS from '../constants/urls';


export const initUserProducts = (user_products_status, query) => {
    return (dispatch) => {
        dispatch({ type: ACTION.USER_PRODUCTS_INIT_START, payload: { user_products_status } });

        /**
         * @TODO: Если форма будет использоваться независимо расписать, сейчас за инит отвечает верхний компонент,
         * @TODO: который отвечает за инициализацию
         */

        /* const entities = getInlineJSON('products-collections-json');
        if (entities && entities.length > 0) {
            dispatch({
                type: ACTION.MODERATION_PRODUCTS_INIT_SUCCESS,
                payload: {
                    moderation_products_status,
                    paginator: {},
                    entities
                }
            });
        } else {
            dispatch({ type: ACTION.MODERATION_PRODUCTS_INIT_FAIL, payload: { moderation_products_status} });
        }*/
    };
};

export const getUserProductsItems = (user_products_status, url, query) => {
    // @TODO
    return (dispatch) => {
        dispatch({ type: ACTION.USER_PRODUCTS_GET_ITEMS_START, payload: {user_products_status} });

        return customFetch(url)
            .then(data => {
                console.log('data', data);
                if (data.status === 'SUCCESS') {
                    const entities = data.data.products;
                    const paginator = data.data.productsPaginator;
                    dispatch({
                        type: ACTION.USER_PRODUCTS_GET_ITEMS_SUCCESS,
                        payload: {
                            entities,
                            paginator,
                            user_products_status
                        }
                    });
                } else if (data.status === 'ERROR') {
                    dispatch({ type: ACTION.USER_PRODUCTS_GET_ITEMS_FAIL, payload: {user_products_status} });
                }
            })
            .catch((error) => {
                console.log(error);
                dispatch({ type: ACTION.USER_PRODUCTS_GET_ITEMS_FAIL, payload: {user_products_status} });
            });
    };
};

export const getUserProductItem = (id, user_products_status) => {
    return (dispatch) => {
        dispatch({ type: ACTION.USER_PRODUCTS_GET_ITEM_START, payload: { user_products_status } });
        customFetch(`${URLS.PRODUCTS.BASE}/${id}`)
            .then((response) => {
                if (response.status === 'SUCCESS') {
                    const { product } = response.data;
                    dispatch({
                        type: ACTION.USER_PRODUCTS_GET_ITEM_SUCCESS,
                        payload: {
                            id,
                            user_products_status,
                            product
                        }
                    });
                } else {
                    dispatch({ type: ACTION.USER_PRODUCTS_GET_ITEM_FAIL, payload: { errors: response, user_products_status } });
                }
            }).catch((error) => {
                dispatch({ type: ACTION.USER_PRODUCTS_GET_ITEM_FAIL, payload: { errors: error, user_products_status } });
                console.error(error);
            });
    };
};

export const getMoreUserProductsItems = (user_products_status, url, query) => {
    return (dispatch) => {
        dispatch({ type: ACTION.USER_PRODUCTS_GET_MORE_ITEMS_START, payload: {user_products_status} });

        return customFetch(url)
            .then(data => {
                console.log('data', data);
                if (data.status === 'SUCCESS') {
                    const entities = data.data.products;
                    const paginator = data.data.productsPaginator;
                    dispatch({
                        type: ACTION.USER_PRODUCTS_GET_MORE_ITEMS_SUCCESS,
                        payload: {
                            entities,
                            paginator,
                            user_products_status
                        }
                    });
                } else if (data.status === 'ERROR') {
                    dispatch({ type: ACTION.USER_PRODUCTS_GET_MORE_ITEMS_FAIL, payload: {user_products_status} });
                }
            })
            .catch((error) => {
                console.log(error);
                dispatch({ type: ACTION.USER_PRODUCTS_GET_MORE_ITEMS_FAIL, payload: {user_products_status} });
            });
    };
};