import ACTION from '../constants/actions';
import { getInlineJSON, customFetch } from '../Helpers';


export const initOperatorProductsTabs = () => {
    return (dispatch) => {
        dispatch({ type: ACTION.OPERATOR_PRODUCTS_TABS_INIT_START });

        const data = getInlineJSON('products-collections-json');

        let moderation_waiting_entities = [];
        let moderation_waiting_paginator = {};
        if (data.hasOwnProperty('waiting_moderation') && data.waiting_moderation) {
            if (data.waiting_moderation.hasOwnProperty('products') && data.waiting_moderation.products) {
                moderation_waiting_entities = data.waiting_moderation.products;
            }
            if (data.waiting_moderation.hasOwnProperty('productsPaginator') && data.waiting_moderation.productsPaginator) {
                moderation_waiting_paginator = data.waiting_moderation.productsPaginator;
            }
        }

        let moderation_declined_entities = [];
        let moderation_declined_paginator = {};
        if (data.hasOwnProperty('declined') && data.declined) {
            if (data.declined.hasOwnProperty('products') && data.declined.products) {
                moderation_declined_entities = data.declined.products;
            }
            if (data.declined.hasOwnProperty('productsPaginator') && data.declined.productsPaginator) {
                moderation_declined_paginator = data.declined.productsPaginator;
            }
        }

        let moderation_published_entities = [];
        let moderation_published_paginator = {};
        if (data.hasOwnProperty('public') && data.public) {
            if (data.public.hasOwnProperty('products') && data.public.products) {
                moderation_published_entities = data.public.products;
            }
            if (data.public.hasOwnProperty('productsPaginator') && data.public.productsPaginator) {
                moderation_published_paginator = data.public.productsPaginator;
            }
        }

        let moderation_archive_entities = [];
        let moderation_archive_paginator = {};
        if (data.hasOwnProperty('archival') && data.archival) {
            if (data.archival.hasOwnProperty('products') && data.archival.products) {
                moderation_archive_entities = data.archival.products;
            }
            if (data.archival.hasOwnProperty('productsPaginator') && data.archival.productsPaginator) {
                moderation_archive_paginator = data.archival.productsPaginator;
            }
        }

        if (data) {
            dispatch({
                type: ACTION.OPERATOR_PRODUCTS_TABS_INIT_SUCCESS,
                payload: {
                    moderation_waiting_entities,
                    moderation_waiting_paginator,
                    moderation_declined_entities,
                    moderation_declined_paginator,
                    moderation_published_entities,
                    moderation_published_paginator,
                    moderation_archive_entities,
                    moderation_archive_paginator
                }
            });
        } else {
            dispatch({ type: ACTION.OPERATOR_PRODUCTS_TABS_INIT_FAIL, payload: {errors: 'Ошибка инициализации'} });
        }
    };
};