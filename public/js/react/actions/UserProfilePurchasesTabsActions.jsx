import ACTION from '../constants/actions';
import { getInlineJSON, customFetch } from '../Helpers';


export const initUserProfilePurchasesTabs = () => {
    return (dispatch) => {
        dispatch({ type: ACTION.USER_PROFILE_PURCHASES_TABS_INIT_START });

        const data = getInlineJSON('purchases_collections_json');

        let user_purchases_are_active_entities = [];
        let user_purchases_are_active_paginator = {};
        let user_purchases_are_active_collection_url = null;
        if (data.hasOwnProperty('public') && data.public) {
            if (data.public.hasOwnProperty('purchases') && data.public.purchases) {
                user_purchases_are_active_entities = data.public.purchases;
            }
            if (data.public.hasOwnProperty('purchasesPaginator') && data.public.purchasesPaginator) {
                user_purchases_are_active_paginator = data.public.purchasesPaginator;
            }
            if (data.public.hasOwnProperty('collection_url') && data.public.collection_url) {
                user_purchases_are_active_collection_url = data.public.collection_url;
            }
        }

        let user_purchases_are_inactive_entities = [];
        let user_purchases_are_inactive_paginator = {};
        let user_purchases_are_inactive_collection_url = null;
        if (data.hasOwnProperty('inactive') && data.inactive) {
            if (data.inactive.hasOwnProperty('purchases') && data.inactive.purchases) {
                user_purchases_are_inactive_entities = data.inactive.purchases;
            }
            if (data.inactive.hasOwnProperty('purchasesPaginator') && data.inactive.purchasesPaginator) {
                user_purchases_are_inactive_paginator = data.inactive.purchasesPaginator;
            }
            if (data.inactive.hasOwnProperty('collection_url') && data.inactive.collection_url) {
                user_purchases_are_inactive_collection_url = data.inactive.collection_url;
            }
        }

        let user_purchases_archive_entities = [];
        let user_purchases_archive_paginator = {};
        let user_purchases_archive_collection_url = null;
        if (data.hasOwnProperty('archival') && data.archival) {
            if (data.archival.hasOwnProperty('purchases') && data.archival.purchases) {
                user_purchases_archive_entities = data.archival.purchases;
            }
            if (data.archival.hasOwnProperty('purchasesPaginator') && data.archival.purchasesPaginator) {
                user_purchases_archive_paginator = data.archival.purchasesPaginator;
            }
            if (data.archival.hasOwnProperty('collection_url') && data.archival.collection_url) {
                user_purchases_archive_collection_url = data.archival.collection_url;
            }
        }

        console.log(data);

        if (data) {
            dispatch({
                type: ACTION.USER_PROFILE_PURCHASES_TABS_INIT_SUCCESS,
                payload: {
                    user_purchases_are_active_entities,
                    user_purchases_are_active_paginator,
                    user_purchases_are_active_collection_url,

                    user_purchases_are_inactive_entities,
                    user_purchases_are_inactive_paginator,
                    user_purchases_are_inactive_collection_url,

                    user_purchases_archive_entities,
                    user_purchases_archive_paginator,
                    user_purchases_archive_collection_url
                }
            });
        } else {
            dispatch({ type: ACTION.USER_PROFILE_PURCHASES_TABS_INIT_FAIL });
        }
    };
};