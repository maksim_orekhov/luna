import ACTION from '../constants/actions';
import {customFetch, getUrlParameter} from '../Helpers';
import URLS from '../constants/urls';

export const sendFormAuth = (data) => {
    return (dispatch) => {
        dispatch({ type: ACTION.AUTH_FORM_SENDING_START });
        return customFetch(URLS.LOGIN, {
            method: 'POST',
            body: JSON.stringify(data),
        }).then(response => {
            console.log('---', response);
            if(response.status === 'ERROR'){
                dispatch({
                    type: ACTION.AUTH_FORM_SENDING_FAIL,
                    payload: {
                        ...response
                    }
                });
            } else {
                dispatch({ type: ACTION.AUTH_FORM_SENDING_SUCCESS });

                let redirectUrlParams = getUrlParameter(window.location.search, 'redirectUrl');
                let loginUrl = '/login';
                if (redirectUrlParams !== '') {
                    loginUrl = loginUrl + '?redirectUrl=' + redirectUrlParams;
                }
                window.location = loginUrl;
            }
        }).catch(error => {
            dispatch({ type: ACTION.AUTH_FORM_SENDING_SERVER_ERROR, });
            console.error('---', error);
        });
    };
};