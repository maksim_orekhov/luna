import ACTION from '../constants/actions';
import { getInlineJSON, customFetch } from '../Helpers';
import { MODERATION_PRODUCTS_STATUS_NAMES } from '../constants/names';
import URLS from '../constants/urls';


export const initModerationProducts = (moderation_products_status, query) => {
    return (dispatch) => {
        dispatch({ type: ACTION.MODERATION_PRODUCTS_INIT_START, payload: { moderation_products_status } });

        /**
         * @TODO: Если форма будет использоваться независимо расписать, сейчас за инит отвечает верхний компонент,
         * @TODO: который отвечает за инициализацию
         */

        /* const entities = getInlineJSON('products-collections-json');
        if (entities && entities.length > 0) {
            dispatch({
                type: ACTION.MODERATION_PRODUCTS_INIT_SUCCESS,
                payload: {
                    moderation_products_status,
                    paginator: {},
                    entities
                }
            });
        } else {
            dispatch({ type: ACTION.MODERATION_PRODUCTS_INIT_FAIL, payload: { moderation_products_status} });
        }*/
    };
};

export const getModerationProductsItems = (moderation_products_status, page, per_page, query) => {
    return (dispatch) => {
        dispatch({ type: ACTION.MODERATION_PRODUCTS_GET_ITEMS_START, payload: { moderation_products_status } });

        const urls = {
            [MODERATION_PRODUCTS_STATUS_NAMES.MODERATION_PRODUCTS_ARE_WAITING]: URLS.MODERATION.MODERATION_PRODUCTS_ARE_WAITING,
            [MODERATION_PRODUCTS_STATUS_NAMES.MODERATION_PRODUCTS_ARE_DECLINED]: URLS.MODERATION.MODERATION_PRODUCTS_ARE_DECLINED,
            [MODERATION_PRODUCTS_STATUS_NAMES.MODERATION_PRODUCTS_ARE_PUBLISHED]: URLS.PRODUCTS.PRODUCTS_ARE_PUBLISHED,
            [MODERATION_PRODUCTS_STATUS_NAMES.MODERATION_PRODUCTS_ARCHIVE]: URLS.PRODUCTS.PRODUCTS_ARCHIVE,
        };

        customFetch(`${urls[moderation_products_status]}?page=${page}&per_page=${per_page}`)
            .then((response) => {
                if (response.status === 'SUCCESS') {
                    const { products, productsPaginator } = response.data;

                    dispatch({
                        type: ACTION.MODERATION_PRODUCTS_GET_ITEMS_SUCCESS,
                        payload: {
                            entities: products,
                            paginator: productsPaginator,
                            moderation_products_status
                        }
                    });
                } else {
                    dispatch({ type: ACTION.MODERATION_PRODUCTS_GET_ITEMS_FAIL, payload: { errors: response, moderation_products_status } });
                }
            }).catch((error) => {
                dispatch({ type: ACTION.MODERATION_PRODUCTS_GET_ITEMS_FAIL, payload: { errors: error, moderation_products_status } });
                console.error(error);
            });
    };
};

export const getModerationProductItem = (id, moderation_products_status) => {
    return (dispatch) => {
        dispatch({ type: ACTION.MODERATION_PRODUCTS_GET_ITEM_START, payload: { moderation_products_status } });
        customFetch(`/product/moderation/${id}`)
            .then((response) => {
                if (response.status === 'SUCCESS') {
                    const { product } = response.data;
                    dispatch({
                        type: ACTION.MODERATION_PRODUCTS_GET_ITEM_SUCCESS,
                        payload: {
                            id,
                            moderation_products_status,
                            product
                        }
                    });
                } else {
                    dispatch({ type: ACTION.MODERATION_PRODUCTS_GET_ITEM_FAIL, payload: { errors: response, moderation_products_status } });
                }
            }).catch((error) => {
                dispatch({ type: ACTION.MODERATION_PRODUCTS_GET_ITEM_FAIL, payload: { errors: error, moderation_products_status } });
                console.error(error);
            });
    };
};