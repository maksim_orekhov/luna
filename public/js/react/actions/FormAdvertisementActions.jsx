import ACTION from '../constants/actions';
import {customFetch, getInlineJSON, getFromSessionStorage, removeFromSessionStorage} from '../Helpers';
import URLS from '../constants/urls';

export const sendFormAdvertisement = ({ formData, formType = 'create', productId }) => {
    return (dispatch) => {
        dispatch({ type: ACTION.ADVERTISE_FORM_SENDING_START });

        return customFetch(formType === 'create' ? URLS.FORM.ADVERT_CREATE : URLS.FORM.PRODUCT_EDIT_PREVIEW(productId), {
            method: 'POST',
            body: formData,
            processData: false,
            contentType: false,
        }).then((response) => {
            const { status, data } = response;
            if(status === 'ERROR'){
                dispatch({
                    type: ACTION.ADVERTISE_FORM_SENDING_FAIL,
                    payload: {
                        ...response
                    }
                });
            } else {
                dispatch({ type: ACTION.ADVERTISE_FORM_SENDING_SUCCESS, });
                dispatch({
                    type: ACTION.ADVERTISE_FORM_SHOW_PREVIEW,
                    payload: {
                        data: {
                            ...data['product'],
                            user: data['user'],
                        }
                    }
                });
            }
        }).catch(error => {
            dispatch({
                type: ACTION.ADVERTISE_FORM_SENDING_SERVER_ERROR,
            });
            console.error('---', error);
        });
    };
};

export const sendConfirmationProductForm = ({ formData, confirmUrl, form_name = null })  => {
    return (dispatch) => {
        dispatch({ type: ACTION.PRODUCT_PREVIEW_CONFIRM_SENDING_START });
        customFetch(confirmUrl, {
            method: 'POST',
            body: formData,
            processData: false,
            contentType: false,
        }).then((response) => {
            if (response.status.toLowerCase() === 'error') {
                if(response.code === 'ERROR_NOT_AUTHORIZED') {
                    window.location = '/login?redirectUrl=/product/create';
                }
                dispatch({ type: ACTION.PRODUCT_PREVIEW_CONFIRM_SENDING_FAIL, payload: { errors: response } });
            } else {
                window.sessionStorage.setItem('profileMessage', 'moderation');
                if (form_name) removeFromSessionStorage(form_name);
                window.location = URLS.PROFILE;
            }
        }).catch((error) => {
            dispatch({ type: ACTION.PRODUCT_PREVIEW_CONFIRM_SENDING_FAIL, payload: { errors: error } });
            console.error('---', error);
        });
    };
};

export const initFormAdvertisement = (form_name) => {
    return (dispatch) => {
        dispatch({ type: ACTION.ADVERTISE_FORM_INIT_START });

        let defaultValues = getInlineJSON('product-data-json');

        if (!defaultValues || (Array.isArray(defaultValues) && !defaultValues.length)) {
            const session_data = getFromSessionStorage(form_name);
            if (session_data) defaultValues = JSON.parse(session_data);
        }

        console.log('init', defaultValues);

        return dispatch({
            type: ACTION.ADVERTISE_FORM_INIT_SUCCESS,
            payload: {
                defaultValues: {...defaultValues},
                hasDefaultValues: !Array.isArray(defaultValues)
            }
        });
    };
};

export const uploadImage = (formData, imageNumber) => {
    return (dispatch) => {
        dispatch({ type: ACTION.ADVERTISE_FORM_IMAGE_UPLOAD_START, payload: { imageNumber } });

        return customFetch(URLS.FORM.UPLOAD_PRODUCT_IMAGE, {
            method: 'POST',
            body: formData,
            processData: false,
            contentType: false,
        }).then((response) => {
            if (response.status.toLowerCase() === 'error') {
                return dispatch({ type: ACTION.ADVERTISE_FORM_IMAGE_UPLOAD_FAIL, payload: { imageNumber } });
            }
            dispatch({
                type: ACTION.ADVERTISE_FORM_IMAGE_UPLOAD_SUCCESS,
                payload: {
                    imageNumber,
                    image: {
                        ...response.data,
                        angle: 0,
                        position: 0,
                    }
                }
            });
        }).catch((error) => {
            dispatch({ type: ACTION.ADVERTISE_FORM_IMAGE_UPLOAD_FAIL });
            console.error('---', error);
        });
    };
};

export const removeImage = (imageNumber) => {
    return { type: ACTION.ADVERTISE_FORM_IMAGE_REMOVE, payload: { imageNumber } };
};

export const rotateImage = (imageNumber, angle) => {
    return { type: ACTION.ADVERTISE_FORM_IMAGE_ROTATE, payload: { imageNumber, angle } };
};

export const closeProductPreview = () => {
    return { type: ACTION.ADVERTISE_FORM_HIDE_PREVIEW, };
};
