import ACTION from '../constants/actions';
import { getInlineJSON, customFetch } from '../Helpers';


export const initUserProfileProductsTabs = () => {
    return (dispatch) => {
        dispatch({ type: ACTION.USER_PROFILE_PRODUCTS_TABS_INIT_START });

        const data = getInlineJSON('products-collections-json');

        let user_products_are_active_entities = [];
        let user_products_are_active_paginator = {};
        if (data.hasOwnProperty('public') && data.public) {
            if (data.public.hasOwnProperty('products') && data.public.products) {
                user_products_are_active_entities = data.public.products;
            }
            if (data.public.hasOwnProperty('productsPaginator') && data.public.productsPaginator) {
                user_products_are_active_paginator = data.public.productsPaginator;
            }
        }

        let user_products_are_inactive_entities = [];
        let user_products_are_inactive_paginator = {};
        if (data.hasOwnProperty('inactive') && data.inactive) {
            if (data.inactive.hasOwnProperty('products') && data.inactive.products) {
                user_products_are_inactive_entities = data.inactive.products;
            }
            if (data.inactive.hasOwnProperty('productsPaginator') && data.inactive.productsPaginator) {
                user_products_are_inactive_paginator = data.inactive.productsPaginator;
            }
        }

        let user_products_archive_entities = [];
        let user_products_archive_paginator = {};
        if (data.hasOwnProperty('archival') && data.archival) {
            if (data.archival.hasOwnProperty('products') && data.archival.products) {
                user_products_archive_entities = data.archival.products;
            }
            if (data.archival.hasOwnProperty('productsPaginator') && data.archival.productsPaginator) {
                user_products_archive_paginator = data.archival.productsPaginator;
            }
        }

        if (data) {
            dispatch({
                type: ACTION.USER_PROFILE_PRODUCTS_TABS_INIT_SUCCESS,
                payload: {
                    user_products_are_active_entities,
                    user_products_are_active_paginator,
                    user_products_are_inactive_entities,
                    user_products_are_inactive_paginator,
                    user_products_archive_entities,
                    user_products_archive_paginator
                }
            });
        } else {
            dispatch({ type: ACTION.USER_PROFILE_PRODUCTS_TABS_INIT_FAIL });
        }
    };
};