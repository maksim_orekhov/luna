import React from 'react';
import PropTypes from 'prop-types';
import { prettifyPhoneNumber } from './Helpers';

export default function PhoneChangeSuccess ({ currentPhone, form_type, is_phone_verified }) {
    return (
        <div className="popup phone-change-success-popup">
            <div className="form-popup">
                <h4 className="popup-title">{is_phone_verified ? 'Подтверждение телефона' : 'Добавление телефона'}</h4>
                <div className="control-form">
                    <input type="text" name="" value={prettifyPhoneNumber(currentPhone)} disabled className={`info-field-text ${is_phone_verified ? 'is-approved' : 'is-not-approved'}`} />
                </div>
                {
                    is_phone_verified ?
                        <div className="form-popup__info-text">
                            Ваш номер телефона успешно подтвержден. Используйте его для входа на сайт.
                        </div>
                        :
                        <div className="form-popup__info-text">
                            Вы добавили номер телефона. Подтвердите его для дальнейшей работы.
                        </div>
                }
                <div className="form-popup__info-text">
                    Телефон будет указан как контактный в объявлениях.
                </div>
            </div>
        </div>
    );
}

PhoneChangeSuccess.propTypes = {
    currentPhone: PropTypes.string.isRequired,
    form_type: PropTypes.string.isRequired,
};
