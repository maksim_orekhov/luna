import React from 'react';
import PropTypes from 'prop-types';
import { checkControlErrors } from './Helpers';

export default function ShowError (props) {
    if (!checkControlErrors(props.existing_errors)) {            // если в объекте props.existing_errors только ключи false или null - не рендерить
        return null;
    }
    let rules = props.messages[checkControlErrors(props.existing_errors)];   // вычислить какие правила именно нарушены и присвоить в переменную
    if (!rules || !Object.keys(rules).length) {
        return null;
    }

    return (
        <p className="note-red">
            {rules}
        </p>
    );
}

ShowError.propTypes = {
    messages: PropTypes.object,
    existing_errors: PropTypes.object,
};

ShowError.defaultProps = {
    messages: '',
    existing_errors: '',
};
