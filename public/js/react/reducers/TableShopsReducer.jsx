import ACTION from '../constants/actions';
const initialState = {
    tableInitError: false,
    tableIsInited: false,
    tableIsInitialising: false,
    is_loading_show_more: false
};
export default (state = initialState, action) => {
    const { type, payload } = action;

    switch (type) {
        case ACTION.TABLE_SHOPS_INIT_START:
            return {
                ...state,
                tableIsInitialising: true,
            };
        case ACTION.TABLE_SHOPS_INIT_SUCCESS:
            return {
                ...state,
                tableIsInitialising: false,
                tableIsInited: true,
                shops: payload.shops
            };
        case ACTION.TABLE_SHOPS_INIT_FAIL:
            return {
                ...state,
                tableIsInitialising: false,
                tableInitError: true,
            };
        case ACTION.TABLE_SHOPS_PAGINATOR_INIT_SUCCESS:
            return {
                ...state,
                paginator: payload.paginator
            };
        case ACTION.SHOW_MORE_SHOPS_START:
            return {
                ...state,
                is_loading_show_more: true,
            };
        case ACTION.SHOW_MORE_SHOPS_SUCCESS:
            return {
                ...state,
                shops: state.shops.concat(payload.collection),
                is_loading_show_more: false,
                paginator: payload.paginator
            };
        case ACTION.SHOPS_GET_ITEMS_BY_SEARCH_START:
            return {
                ...state,
                isLoading: true
            };
        case ACTION.SHOPS_GET_ITEMS_BY_SEARCH_SUCCESS:
            return {
                ...state,
                isLoading: false,
                shops: payload.entities,
                paginator: payload.paginator,
                errors: null
            };
        case ACTION.SHOPS_GET_ITEMS_BY_SEARCH_FAIL:
            return {
                ...state,
                isLoading: false,
                errors: payload.errors,
            };
        default:
            return state;
    }
};
