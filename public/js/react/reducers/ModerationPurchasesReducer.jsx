import ACTION from '../constants/actions';
import { MODERATION_PURCHASES_STATUS_NAMES } from '../constants/names';
const {
    MODERATION_PURCHASES_ARE_WAITING,
    MODERATION_PURCHASES_ARE_DECLINED,
    MODERATION_PURCHASES_ARE_PUBLISHED,
    MODERATION_PURCHASES_ARCHIVE
} = MODERATION_PURCHASES_STATUS_NAMES;


const initialState = {
    [MODERATION_PURCHASES_ARE_WAITING]: {
        isInitialising: false,
        isInited: false,
        isLoading: false,
        isPreviewLoading: false,
        isPreviewLoaded: false,
        errors: null,
        entities: [],
        paginator: {}
    },
    [MODERATION_PURCHASES_ARE_DECLINED]: {
        isInitialising: false,
        isInited: false,
        isLoading: false,
        isPreviewLoading: false,
        isPreviewLoaded: false,
        errors: null,
        entities: [],
        paginator: {}
    },
    [MODERATION_PURCHASES_ARE_PUBLISHED]: {
        isInitialising: false,
        isInited: false,
        isLoading: false,
        isPreviewLoading: false,
        isPreviewLoaded: false,
        errors: null,
        entities: [],
        paginator: {}
    },
    [MODERATION_PURCHASES_ARCHIVE]: {
        isInitialising: false,
        isInited: false,
        isLoading: false,
        isPreviewLoading: false,
        isPreviewLoaded: false,
        errors: null,
        entities: [],
        paginator: {}
    }
};


export default (state = initialState, action) => {
    const { type, payload = {} } = action;
    const { moderation_purchases_status } = payload;

    switch (type) {
        case ACTION.MODERATION_PURCHASES_INIT_START:
            return {
                ...state,
                [moderation_purchases_status]: {
                    ...state[moderation_purchases_status],
                    isInitialising: true
                }
            };
        case ACTION.MODERATION_PURCHASES_INIT_SUCCESS:
            return {
                ...state,
                [moderation_purchases_status]: {
                    ...state[moderation_purchases_status],
                    isInitialising: false,
                    isInited: true,
                    entities: payload.entities,
                    paginator: payload.paginator,
                    errors: null,
                }
            };
        case ACTION.MODERATION_PURCHASES_INIT_FAIL:
            return {
                ...state,
                [moderation_purchases_status]: {
                    ...state[moderation_purchases_status],
                    isInitialising: false,
                    errors: payload.errors,
                }
            };
        case ACTION.MODERATION_PURCHASES_GET_ITEMS_START:
            return {
                ...state,
                [moderation_purchases_status]: {
                    ...state[moderation_purchases_status],
                    isLoading: true,
                }
            };
        case ACTION.MODERATION_PURCHASES_GET_ITEMS_SUCCESS:
            return {
                ...state,
                [moderation_purchases_status]: {
                    ...state[moderation_purchases_status],
                    isLoading: false,
                    entities: payload.entities,
                    paginator: payload.paginator,
                    errors: null,
                }
            };
        case ACTION.MODERATION_PURCHASES_GET_ITEMS_FAIL:
            return {
                ...state,
                [moderation_purchases_status]: {
                    ...state[moderation_purchases_status],
                    isLoading: false,
                    errors: payload.errors,
                }
            };
        case ACTION.MODERATION_PURCHASES_GET_ITEM_START:
            return {
                ...state,
                [moderation_purchases_status]: {
                    ...state[moderation_purchases_status],
                    isPreviewLoading: true,
                    isPreviewLoaded: false
                }
            };
        case ACTION.MODERATION_PURCHASES_GET_ITEM_SUCCESS:
            const entity_id = state[moderation_purchases_status].entities.findIndex(purchase => purchase.id === payload.id);
            const new_entities = [...state[moderation_purchases_status].entities];
            new_entities[entity_id] = {...new_entities[entity_id],
                owner: {...payload.purchase.owner},
                files: Array.isArray(payload.purchase.files) ? [...payload.purchase.files] : [],
                address: {...payload.purchase.address},
                description: payload.purchase.description
            };

            return {
                ...state,
                [moderation_purchases_status]: {
                    ...state[moderation_purchases_status],
                    isPreviewLoading: false,
                    isPreviewLoaded: true,
                    entities: new_entities
                }
            };
        case ACTION.MODERATION_PURCHASES_GET_ITEM_FAIL:
            return {
                ...state,
                [moderation_purchases_status]: {
                    ...state[moderation_purchases_status],
                    isPreviewLoading: false,
                    isPreviewLoaded: false,
                    errors: payload.errors
                }
            };
        case ACTION.OPERATOR_PURCHASES_TABS_INIT_START:
            return {
                ...state,
                [MODERATION_PURCHASES_ARE_WAITING]: {
                    ...state[MODERATION_PURCHASES_ARE_WAITING],
                    isInitialising: true,
                },
                [MODERATION_PURCHASES_ARE_DECLINED]: {
                    ...state[MODERATION_PURCHASES_ARE_DECLINED],
                    isInitialising: true,
                },
                [MODERATION_PURCHASES_ARE_PUBLISHED]: {
                    ...state[MODERATION_PURCHASES_ARE_PUBLISHED],
                    isInitialising: true,
                },
                [MODERATION_PURCHASES_ARCHIVE]: {
                    ...state[MODERATION_PURCHASES_ARCHIVE],
                    isInitialising: true,
                }
            };
        case ACTION.OPERATOR_PURCHASES_TABS_INIT_SUCCESS:
            return {
                ...state,
                [MODERATION_PURCHASES_ARE_WAITING]: {
                    ...state[MODERATION_PURCHASES_ARE_WAITING],
                    isInitialising: false,
                    isInited: true,
                    entities: payload.moderation_waiting_entities,
                    paginator: payload.moderation_waiting_paginator,
                    errors: null,
                },
                [MODERATION_PURCHASES_ARE_DECLINED]: {
                    ...state[MODERATION_PURCHASES_ARE_DECLINED],
                    isInitialising: false,
                    isInited: true,
                    entities: payload.moderation_declined_entities,
                    paginator: payload.moderation_declined_paginator,
                    errors: null,
                },
                [MODERATION_PURCHASES_ARE_PUBLISHED]: {
                    ...state[MODERATION_PURCHASES_ARE_PUBLISHED],
                    isInitialising: false,
                    isInited: true,
                    entities: payload.moderation_published_entities,
                    paginator: payload.moderation_published_paginator,
                    errors: null,
                },
                [MODERATION_PURCHASES_ARCHIVE]: {
                    ...state[MODERATION_PURCHASES_ARCHIVE],
                    isInitialising: false,
                    isInited: true,
                    entities: payload.moderation_archive_entities,
                    paginator: payload.moderation_archive_paginator,
                    errors: null,
                }
            };
        case ACTION.OPERATOR_PURCHASES_TABS_INIT_FAIL:
            return {
                ...state,
                [MODERATION_PURCHASES_ARE_WAITING]: {
                    ...state[MODERATION_PURCHASES_ARE_WAITING],
                    isInitialising: false,
                    errors: payload.errors,
                },
                [MODERATION_PURCHASES_ARE_DECLINED]: {
                    ...state[MODERATION_PURCHASES_ARE_DECLINED],
                    isInitialising: false,
                    errors: payload.errors,
                },
                [MODERATION_PURCHASES_ARE_PUBLISHED]: {
                    ...state[MODERATION_PURCHASES_ARE_PUBLISHED],
                    isInitialising: false,
                    errors: payload.errors,
                },
                [MODERATION_PURCHASES_ARCHIVE]: {
                    ...state[MODERATION_PURCHASES_ARCHIVE],
                    isInitialising: false,
                    errors: payload.errors,
                }
            };
        default:
            return state;
    }
};