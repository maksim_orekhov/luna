import ACTION from '../constants/actions';

const initialState = {
    formSendingError: false,
    formIsSending: false,
    formIsInited: false,
    formIsIniting: false,
    formConfirming: false,
    purchasePreview: {
        isVisible: false,
        data: {}
    },
    sendResponse: {},
    serverErrors: {},
    confirmationErrors: {},
    defaultValues: {},
    uploadedFiles: [],
    hasDefaultValues: false,
};
export default (state = initialState, action) => {
    const { type, payload } = action;
    switch (type) {
        case ACTION.PURCHASE_FORM_INIT_START:
            return {
                ...state,
                formIsIniting: true
            };
        case ACTION.PURCHASE_FORM_INIT_SUCCESS:
            return {
                ...state,
                uploadedFiles: payload.defaultValues.files,
                formIsIniting: false,
                formIsInited: true,
                defaultValues: payload.defaultValues,
                hasDefaultValues: payload.hasDefaultValues,
            };
        case ACTION.PURCHASE_FORM_SENDING_START:
            return {
                ...state,
                formIsSending: true,
            };
        case ACTION.PURCHASE_FORM_SENDING_SUCCESS:
            return {
                ...state,
                formIsSending: false,
                sendResponse: {...payload}

            };
        case ACTION.PURCHASE_FORM_SENDING_FAIL:
            return {
                ...state,
                formIsSending: false,
                serverErrors: {...payload}
            };
        case ACTION.PURCHASE_FORM_SENDING_SERVER_ERROR:
            return {
                ...state,
                formIsSending: false,
                formSendingError: true,
                serverErrors: {...payload}
            };
        case ACTION.PURCHASE_PREVIEW_CONFIRM_SENDING_START:
            return {
                ...state,
                formConfirming: true,
                confirmationErrors: {},
            };
        case ACTION.PURCHASE_PREVIEW_CONFIRM_SENDING_FAIL:
            return {
                ...state,
                formConfirming: false,
                confirmationErrors: payload.errors,
            };
        case ACTION.PURCHASE_FORM_SHOW_PREVIEW:
            return {
                ...state,
                purchasePreview: {
                    isVisible: true,
                    data: {...payload.data}
                }
            };
        case ACTION.PURCHASE_FORM_HIDE_PREVIEW:
            return {
                ...state,
                purchasePreview: {
                    isVisible: false,
                    data: {}
                }
            };
        default:
            return state;
    }
};
