import ACTION from '../constants/actions';

const initialState = {
    formSendingError: false,
    formIsSending: false,
    formIsInited: false,
    formIsIniting: false,
    formConfirming: false,
    productPreview: {
        isVisible: false,
        data: {}
    },
    sendResponse: {},
    serverErrors: {},
    confirmationErrors: {},
    defaultValues: {},
    uploadedImages: [],
    uploadedFiles: [],
    hasDefaultValues: false,
};
export default (state = initialState, action) => {
    const { type, payload } = action;
    let uploadedImages = [...state.uploadedImages];
    switch (type) {
        case ACTION.ADVERTISE_FORM_INIT_START:
            return {
                ...state,
                formIsIniting: true
            };
        case ACTION.ADVERTISE_FORM_INIT_SUCCESS:
            if (Array.isArray(payload.defaultValues.gallery)) {
                uploadedImages = payload.defaultValues.gallery.map((file) => {
                    return {
                        image: {
                            ...file,
                            angle: file.rotate || 0,
                            position: 0,
                        },
                        isUploading: false,
                    };
                });
            }
            return {
                ...state,
                uploadedImages,
                uploadedFiles: payload.defaultValues.files,
                formIsIniting: false,
                formIsInited: true,
                defaultValues: payload.defaultValues,
                hasDefaultValues: payload.hasDefaultValues,
            };
        case ACTION.ADVERTISE_FORM_SENDING_START:
            return {
                ...state,
                formIsSending: true,
            };
        case ACTION.ADVERTISE_FORM_SENDING_SUCCESS:
            uploadedImages = uploadedImages.map(uploadedImage => {
                if (uploadedImage !== undefined) {
                    uploadedImage.image.position = 0;
                }
                return uploadedImage;
            });
            return {
                ...state,
                formIsSending: false,
                sendResponse: {...payload},
                uploadedImages,

            };
        case ACTION.ADVERTISE_FORM_SENDING_FAIL:
            return {
                ...state,
                formIsSending: false,
                serverErrors: {...payload}
            };
        case ACTION.ADVERTISE_FORM_SENDING_SERVER_ERROR:
            return {
                ...state,
                formIsSending: false,
                formSendingError: true,
                serverErrors: {...payload}
            };
        case ACTION.PRODUCT_PREVIEW_CONFIRM_SENDING_START:
            return {
                ...state,
                formConfirming: true,
                confirmationErrors: {},
            };
        case ACTION.PRODUCT_PREVIEW_CONFIRM_SENDING_FAIL:
            return {
                ...state,
                formConfirming: false,
                confirmationErrors: payload.errors,
            };
        case ACTION.ADVERTISE_FORM_IMAGE_UPLOAD_START:
            uploadedImages[payload.imageNumber] = {
                isUploading: true,
            };
            return {
                ...state,
                uploadedImages
            };
        case ACTION.ADVERTISE_FORM_IMAGE_UPLOAD_SUCCESS:
            uploadedImages[payload.imageNumber] = {
                isUploading: false,
                image: {...payload.image}
            };
            return {
                ...state,
                uploadedImages
            };
        case ACTION.ADVERTISE_FORM_IMAGE_UPLOAD_FAIL:
            delete uploadedImages[payload.imageNumber];
            return {
                ...state,
                uploadedImages
            };
        case ACTION.ADVERTISE_FORM_IMAGE_REMOVE:
            delete uploadedImages[payload.imageNumber];
            return {
                ...state,
                uploadedImages,
            };
        case ACTION.ADVERTISE_FORM_IMAGE_ROTATE:
            // currentAngle + 90 < 360 ? currentAngle + 90 : 0
            const currentPosition = uploadedImages[payload.imageNumber]['image']['position'];
            const currentAngle = uploadedImages[payload.imageNumber]['image']['angle'];
            uploadedImages[payload.imageNumber]['image']['angle'] = currentAngle + payload.angle < 360 ? currentAngle + payload.angle : 0;
            uploadedImages[payload.imageNumber]['image']['position'] = currentPosition + payload.angle < 360 ? currentPosition + payload.angle : 0;
            return {
                ...state,
                uploadedImages,
            };
        case ACTION.ADVERTISE_FORM_SHOW_PREVIEW:
            return {
                ...state,
                productPreview: {
                    isVisible: true,
                    data: {...payload.data}
                }
            };
        case ACTION.ADVERTISE_FORM_HIDE_PREVIEW:
            return {
                ...state,
                productPreview: {
                    isVisible: false,
                    data: {}
                }
            };
        default:
            return state;
    }
};
