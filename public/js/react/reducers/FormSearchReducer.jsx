import ACTION from '../constants/actions';

const initialState = {
    searchIsFetching: false,
    results: {}
};

export default (state = initialState, action) => {
    const { type, payload } = action;
    const { results } = state;
    switch (type) {
        case ACTION.SEARCH_FETCH_START:
            return {
                ...state,
                searchIsFetching: true,
            };
        case ACTION.SEARCH_FETCH_END:
            return {
                searchIsFetching: false,
                results: {
                    ...results,
                    [payload.query]: payload.result,
                },
            };
        default:
            return state;
    }
};
