import ACTION from '../constants/actions';

const initialState = {
    categories: {
        entities: [],
        isLoading: false,
        isLoaded: false,
        isFetching: false,
    },
    csrf: {
        value: '',
        isLoading: false,
        isLoaded: false,
        isFailed: false,
    },
    mobile_menu: {
        isLoading: false,
        isLoaded: false,
        isFailed: false,
        catalog_name: '',
        catalog_root_url: '',
        categories: [],
        product_create_url: '',
        product_root_url: '',
        purchase_create_url: '',
        purchase_root_url: ''
    }
};

export default (state = initialState, action) => {
    const { categories, csrf, mobile_menu } = state;
    const { type, payload } = action;
    switch (type) {
        case ACTION.CATEGORIES_INIT_START:
            return {
                ...state,
                categories: {
                    ...categories,
                    isLoading: true,
                },
            };
        case ACTION.SUBCATEGORIES_FETCH_START:
            return {
                ...state,
                categories: {
                    ...categories,
                    isFetching: true,
                },
            };
        case ACTION.CATEGORIES_INIT_SUCCESS:
            return {
                ...state,
                categories: {
                    ...categories,
                    isLoading: false,
                    isLoaded: true,
                    entities: payload.categories,
                },
            };
        case ACTION.SUBCATEGORIES_FETCH_SUCCESS:
            const entities = [...categories.entities];
            entities[entities.findIndex((category) => category.id === payload.categoryId)].subcategories = payload.subcategories;
            return {
                ...state,
                categories: {
                    ...categories,
                    entities,
                    isFetching: false
                },
            };
        case ACTION.CSRF_INIT_START:
            return {
                ...state,
                csrf: {
                    ...csrf,
                    isLoading: true,
                    isLoaded: false,
                    isFailed: false,
                },
            };
        case ACTION.CSRF_INIT_SUCCESS:
            return {
                ...state,
                csrf: {
                    ...csrf,
                    value: payload.csrf,
                    isLoading: false,
                    isLoaded: true,
                    isFailed: false,
                },
            };
        case ACTION.CSRF_INIT_FAIL:
            return {
                ...state,
                csrf: {
                    ...csrf,
                    value: '',
                    isLoading: false,
                    isLoaded: true,
                    isFailed: true,
                },
            };
        case ACTION.MOBILE_MENU_INIT_START:
            return {
                ...state,
                mobile_menu: {
                    ...mobile_menu,
                    isLoading: true,
                    isLoaded: false,
                    isFailed: false,
                },
            };
        case ACTION.MOBILE_MENU_INIT_SUCCESS:
            return {
                ...state,
                mobile_menu: {
                    ...mobile_menu,
                    catalog_name: payload.catalog_name || 'product',
                    catalog_root_url: payload.catalog_root_url || payload.product_root_url || '',
                    categories: payload.categories || [],
                    product_create_url: payload.product_create_url || '',
                    product_root_url: payload.product_root_url || '',
                    purchase_create_url: payload.purchase_create_url || '',
                    purchase_root_url: payload.purchase_root_url || '',
                    isLoading: false,
                    isLoaded: true,
                    isFailed: false,
                },
            };
        case ACTION.MOBILE_MENU_INIT_FAIL:
            return {
                ...state,
                mobile_menu: {
                    ...mobile_menu,
                    isLoading: false,
                    isLoaded: true,
                    isFailed: true,
                },
            };
        default:
            return state;
    }
};