import ACTION from '../constants/actions';
const initialState = {
    tableInitError: false,
    tableIsInited: false,
    tableIsInitialising: false,
    is_loading_show_more: false
};
export default (state = initialState, action) => {
    const { type, payload } = action;
    const is_landing = document.getElementById('landing_static') || document.getElementById('landing_static');

    switch (type) {
        case ACTION.TABLE_PRODUCTS_INIT_START:
            return {
                ...state,
                tableIsInitialising: true,
            };
        case ACTION.TABLE_PRODUCTS_INIT_SUCCESS:
            return {
                ...state,
                tableIsInitialising: false,
                tableIsInited: true,
                products: payload.products,
                view_mode: payload.view_mode,
                breadcrumbs: payload.breadcrumbs
            };
        case ACTION.TABLE_PRODUCTS_INIT_FAIL:
            return {
                ...state,
                tableIsInitialising: false,
                tableInitError: true,
            };
        case ACTION.TABLE_PRODUCTS_PAGINATOR_INIT_SUCCESS:
            return {
                ...state,
                paginator: payload.paginator
            };
        case ACTION.SHOW_MORE_PRODUCTS_START:
            return {
                ...state,
                is_loading_show_more: true,
            };
        case ACTION.SHOW_MORE_PRODUCTS_SUCCESS:
            return {
                ...state,
                products: state.products.concat(payload.collection),
                is_loading_show_more: false,
                paginator: payload.paginator
            };
        case ACTION.PRODUCTS_GET_ITEMS_BY_SEARCH_START:
            if (is_landing) {
                document.getElementById('landing_static_spinner').style.display = 'block';
            }
            return {
                ...state,
                isLoading: true
            };
        case ACTION.PRODUCTS_GET_ITEMS_BY_SEARCH_SUCCESS:
            if (is_landing && !payload.is_query_empty) {
                document.getElementById('landing_static').style.display = 'none';
                document.getElementById('landing_dynamic').style.display = 'block';
            } else if (is_landing && payload.is_query_empty) {
                document.getElementById('landing_static').style.display = 'block';
                document.getElementById('landing_dynamic').style.display = 'none';
                document.getElementsByClassName('list_container')[0].classList.remove('Preloader');
                document.getElementById('landing_static_spinner').style.display = 'none';
            }
            return {
                ...state,
                isLoading: false,
                products: payload.entities,
                paginator: payload.paginator,
                errors: null
            };
        case ACTION.PRODUCTS_GET_ITEMS_BY_SEARCH_FAIL:
            if (is_landing) {
                document.getElementsByClassName('list_container')[0].classList.remove('Preloader');
                document.getElementById('landing_dynamic').style.display = 'none';
                document.getElementById('landing_static_spinner').style.display = 'none';
            }
            return {
                ...state,
                isLoading: false,
                errors: payload.errors,
            };
        default:
            return state;
    }
};
