import ACTION from '../constants/actions';
const initialState = {
    tableInitError: false,
    tableIsInited: false,
    tableIsInitialising: false,
    is_loading_show_more: false
};
export default (state = initialState, action) => {
    const { type, payload } = action;

    switch (type) {
        case ACTION.TABLE_USERS_PURCHASES_INIT_START:
            return {
                ...state,
                tableIsInitialising: true,
            };
        case ACTION.TABLE_USERS_PURCHASES_INIT_SUCCESS:
            return {
                ...state,
                tableIsInitialising: false,
                tableIsInited: true,
                purchases: payload.purchases,
                url: payload.url,
                products_url: payload.products_url,
                paginator: payload.paginator
            };
        case ACTION.TABLE_USERS_PURCHASES_INIT_FAIL:
            return {
                ...state,
                tableIsInitialising: false,
                tableInitError: true,
            };
        case ACTION.USERS_PURCHASES_GET_MORE_ITEMS_START:
            return {
                ...state,
                isLoading: true
            };
        case ACTION.USERS_PURCHASES_GET_MORE_ITEMS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                purchases: state.purchases.concat(payload.purchases),
                paginator: payload.paginator,
                errors: null
            };
        case ACTION.USERS_PURCHASES_GET_MORE_ITEMS_FAIL:
            return {
                ...state,
                isLoading: false,
                errors: payload.errors,
            };
        case ACTION.USERS_PURCHASES_GET_ITEMS_BY_SEARCH_START:
            return {
                ...state,
                isLoading: true
            };
        case ACTION.USERS_PURCHASES_GET_ITEMS_BY_SEARCH_SUCCESS:
            return {
                ...state,
                isLoading: false,
                purchases: payload.entities,
                paginator: payload.paginator,
                errors: null
            };
        case ACTION.USERS_PURCHASES_GET_ITEMS_BY_SEARCH_FAIL:
            return {
                ...state,
                isLoading: false,
                errors: payload.errors,
            };
        default:
            return state;
    }
};
