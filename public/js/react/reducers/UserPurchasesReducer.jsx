import ACTION from '../constants/actions';
import { USER_PURCHASES_STATUS_NAMES } from '../constants/names';
const {
    USER_PURCHASES_ARE_ACTIVE,
    USER_PURCHASES_ARE_INACTIVE,
    USER_PURCHASES_ARCHIVE
} = USER_PURCHASES_STATUS_NAMES;


const initialState = {
    [USER_PURCHASES_ARE_ACTIVE]: {
        isInitialising: false,
        isInited: false,
        isLoading: false,
        isPreviewLoading: false,
        errors: null,
        entities: [],
        paginator: {},
        collection_url: null
    },
    [USER_PURCHASES_ARE_INACTIVE]: {
        isInitialising: false,
        isInited: false,
        isLoading: false,
        isPreviewLoading: false,
        errors: null,
        entities: [],
        paginator: {},
        collection_url: null
    },
    [USER_PURCHASES_ARCHIVE]: {
        isInitialising: false,
        isInited: false,
        isLoading: false,
        isPreviewLoading: false,
        errors: null,
        entities: [],
        paginator: {},
        collection_url: null
    }
};


export default (state = initialState, action) => {
    const { type, payload = {} } = action;
    const { user_purchases_status } = payload;

    switch (type) {
        case ACTION.USER_PURCHASES_INIT_START:
            return {
                ...state,
                [user_purchases_status]: {
                    ...state[user_purchases_status],
                    isInitialising: true
                }
            };
        case ACTION.USER_PURCHASES_INIT_SUCCESS:
            return {
                ...state,
                [user_purchases_status]: {
                    ...state[user_purchases_status],
                    isInitialising: false,
                    isInited: true,
                    entities: payload.entities,
                    paginator: payload.paginator,
                    collection_url: payload.collection_url,
                    errors: null,
                }
            };
        case ACTION.USER_PURCHASES_INIT_FAIL:
            return {
                ...state,
                [user_purchases_status]: {
                    ...state[user_purchases_status],
                    isInitialising: false,
                    errors: payload.errors,
                }
            };
        case ACTION.USER_PURCHASES_GET_ITEMS_START:
            return {
                ...state,
                [user_purchases_status]: {
                    ...state[user_purchases_status],
                    isLoading: true,
                }
            };
        case ACTION.USER_PURCHASES_GET_ITEMS_SUCCESS:
            return {
                ...state,
                [user_purchases_status]: {
                    ...state[user_purchases_status],
                    isLoading: false,
                    entities: payload.entities,
                    paginator: payload.paginator,
                    errors: null,
                }
            };
        case ACTION.USER_PURCHASES_GET_ITEMS_FAIL:
            return {
                ...state,
                [user_purchases_status]: {
                    ...state[user_purchases_status],
                    isLoading: false,
                    errors: payload.errors,
                }
            };
        case ACTION.USER_PURCHASES_GET_ITEM_START:
            return {
                ...state,
                [user_purchases_status]: {
                    ...state[user_purchases_status],
                    isPreviewLoading: true,
                    isPreviewLoaded: false
                }
            };
        case ACTION.USER_PURCHASES_GET_ITEM_SUCCESS:
            const entity_id = state[user_purchases_status].entities.findIndex(purchase => purchase.id === payload.id);
            const new_entities = [...state[user_purchases_status].entities];
            new_entities[entity_id] = {...new_entities[entity_id],
                owner: {...payload.purchase.owner},
                files: Array.isArray(payload.purchase.files) ? [...payload.purchase.files] : [],
                address: {...payload.purchase.address},
                description: payload.purchase.description
            };

            return {
                ...state,
                [user_purchases_status]: {
                    ...state[user_purchases_status],
                    isPreviewLoading: false,
                    isPreviewLoaded: true,
                    entities: new_entities
                }
            };
        case ACTION.USER_PURCHASES_GET_ITEM_FAIL:
            return {
                ...state,
                [user_purchases_status]: {
                    ...state[user_purchases_status],
                    isPreviewLoading: false,
                    isPreviewLoaded: false,
                    errors: payload.errors
                }
            };
        case ACTION.USER_PROFILE_PURCHASES_TABS_INIT_START:
            return {
                ...state,
                [USER_PURCHASES_ARE_ACTIVE]: {
                    ...state[USER_PURCHASES_ARE_ACTIVE],
                    isInitialising: true,
                },
                [USER_PURCHASES_ARE_INACTIVE]: {
                    ...state[USER_PURCHASES_ARE_INACTIVE],
                    isInitialising: true,
                },
                [USER_PURCHASES_ARCHIVE]: {
                    ...state[USER_PURCHASES_ARCHIVE],
                    isInitialising: true,
                }
            };
        case ACTION.USER_PROFILE_PURCHASES_TABS_INIT_SUCCESS:
            return {
                ...state,
                [USER_PURCHASES_ARE_ACTIVE]: {
                    ...state[USER_PURCHASES_ARE_ACTIVE],
                    isInitialising: false,
                    isInited: true,
                    entities: payload.user_purchases_are_active_entities,
                    paginator: payload.user_purchases_are_active_paginator,
                    collection_url: payload.user_purchases_are_active_collection_url,
                    errors: null,
                },
                [USER_PURCHASES_ARE_INACTIVE]: {
                    ...state[USER_PURCHASES_ARE_INACTIVE],
                    isInitialising: false,
                    isInited: true,
                    entities: payload.user_purchases_are_inactive_entities,
                    paginator: payload.user_purchases_are_inactive_paginator,
                    collection_url: payload.user_purchases_are_inactive_collection_url,
                    errors: null,
                },
                [USER_PURCHASES_ARCHIVE]: {
                    ...state[USER_PURCHASES_ARCHIVE],
                    isInitialising: false,
                    isInited: true,
                    entities: payload.user_purchases_archive_entities,
                    paginator: payload.user_purchases_archive_paginator,
                    collection_url: payload.user_purchases_archive_collection_url,
                    errors: null,
                }
            };
        case ACTION.USER_PROFILE_PURCHASES_TABS_INIT_FAIL:
            return {
                ...state,
                [USER_PURCHASES_ARE_ACTIVE]: {
                    ...state[USER_PURCHASES_ARE_ACTIVE],
                    isInitialising: false,
                    errors: payload.errors,
                },
                [USER_PURCHASES_ARE_INACTIVE]: {
                    ...state[USER_PURCHASES_ARE_INACTIVE],
                    isInitialising: false,
                    errors: payload.errors,
                },
                [USER_PURCHASES_ARCHIVE]: {
                    ...state[USER_PURCHASES_ARCHIVE],
                    isInitialising: false,
                    errors: payload.errors,
                }
            };
        case ACTION.USER_PURCHASES_GET_MORE_ITEMS_START:
            return {
                ...state,
                [user_purchases_status]: {
                    ...state[user_purchases_status],
                    isLoading: true,
                }
            };
        case ACTION.USER_PURCHASES_GET_MORE_ITEMS_SUCCESS:
            return {
                ...state,
                [user_purchases_status]: {
                    ...state[user_purchases_status],
                    isLoading: false,
                    entities: [...state[user_purchases_status].entities, ...payload.entities],
                    paginator: payload.paginator,
                    errors: null,
                }
            };
        case ACTION.USER_PURCHASES_GET_MORE_ITEMS_FAIL:
            return {
                ...state,
                [user_purchases_status]: {
                    ...state[user_purchases_status],
                    isLoading: false,
                    errors: payload.errors,
                }
            };
        default:
            return state;
    }
};