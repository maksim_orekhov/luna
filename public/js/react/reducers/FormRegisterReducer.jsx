import ACTION from '../constants/actions';
const initialState = {
    formSendingError: false,
    formIsSending: false,
    sendResponse: {},
    serverErrors: {},
};
export default (state = initialState, action) => {
    const { type, payload } = action;
    switch (type) {
        case ACTION.REGISTER_FORM_SENDING_START:
            return {
                ...state,
                serverErrors: {},
                formIsSending: true,
            };
        case ACTION.REGISTER_FORM_SENDING_SUCCESS:
            return {
                ...state,
                formIsSending: false,
                sendResponse: {...payload}
            };
        case ACTION.REGISTER_FORM_SENDING_FAIL:
            return {
                ...state,
                formIsSending: false,
                serverErrors: {...payload}
            };
        case ACTION.REGISTER_FORM_SENDING_SERVER_ERROR:
            return {
                ...state,
                formIsSending: false,
                formSendingError: true,
                serverErrors: {...payload}
            };
        default:
            return state;
    }
};
