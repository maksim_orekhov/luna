import { combineReducers } from 'redux';
import formAdvertisement from './FormAdvertisementReducer';
import formPurchase from './FormPurchaseReducer';
import formAuth from './FormAuthReducer';
import formRegister from './FormRegisterReducer';
import tableProducts from './TableProductsReducer';
import tablePurchases from './TablePurchasesReducer';
import tableShops from './TableShopsReducer';
import tableUsersProducts from './TableUsersProductsReducer';
import tableUsersPurchases from './TableUsersPurchasesReducer';
import search from './FormSearchReducer';
import config from './ConfigReducer';
import user from './UserReducer';
import moderationProducts from './ModerationProductsReducer';
import moderationPurchases from './ModerationPurchasesReducer';
import userProducts from './UserProductsReducer';
import userPurchases from './UserPurchasesReducer';

export default combineReducers({
    config,
    formAdvertisement,
    formPurchase,
    formAuth,
    formRegister,
    tableProducts,
    tablePurchases,
    tableShops,
    tableUsersProducts,
    tableUsersPurchases,
    search,
    user,
    moderationProducts,
    moderationPurchases,
    userProducts,
    userPurchases
});
