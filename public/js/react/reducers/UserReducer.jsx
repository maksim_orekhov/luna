import ACTION from '../constants/actions';

const initialState = {
    isLoading: false,
    isLoaded: false,
    loadIsFailed: false,
    data: { nextPhone: '' },
    isChanging: {
        avatar: false,
        name: false,
        phone: false,
        email: false,
        phoneConfirmToken: false,
        phoneConfirmCode: false,
        emailChangeAccept: false,
    },
    isOpenForm: {
        nameAdd: false,
        phoneChange: false,
        phoneConfirmToken: false,
        phoneConfirmCode: false,
        phoneChangeSuccess: false,
        emailChange: false,
        emailChangeAccept: false,
    },
    errors: {
        avatar: {},
        name: {},
        phone: {},
        phoneConfirmToken: {},
        phoneConfirmCode: {},
        emailChange: {},
        emailChangeAccept: {},
    }
};

export default (state = initialState, action) => {
    const { type, payload } = action;
    let isOpenForm = {...state.isOpenForm};
    switch (type) {
        case ACTION.USER_LOAD_START:
            return {
                ...state,
                isLoading: true,
            };
        case ACTION.USER_LOAD_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                data: {
                    ...state.data,
                    ...payload.data,
                    nextPhone: !payload.data.is_phone_verified ? payload.data.phone : '',
                }
            };
        case ACTION.USER_LOAD_FAIL:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                loadIsFailed: true,
            };
        case ACTION.USER_DATA_CHANGE_START:
            return {
                ...state,
                isChanging: {
                    ...state.isChanging,
                    [payload.fieldName]: true,
                }
            };
        case ACTION.USER_DATA_CHANGE_END:
            return {
                ...state,
                isChanging: {
                    ...state.isChanging,
                    [payload.fieldName]: false,
                }
            };
        case ACTION.USER_AVATAR_CHANGE_SUCCESS:
            return {
                ...state,
                data: {
                    ...state.data,
                    avatar: payload.avatar,
                },
                isChanging: {
                    ...state.isChanging,
                    avatar: false,
                },
            };
        case ACTION.USER_NAME_CHANGE_SUCCESS:
            return {
                ...state,
                data: {
                    ...state.data,
                    name: payload.name
                },
                isChanging: {
                    ...state.isChanging,
                    name: false,
                },
                errors: {
                    ...state.errors,
                    name: {},
                }
            };
        case ACTION.USER_NAME_CHANGE_FAIL:
            return {
                ...state,
                isChanging: {
                    ...state.isChanging,
                    name: false,
                },
                errors: {
                    ...state.errors,
                    name: payload.errors,
                }
            };
        case ACTION.USER_PHONE_CHANGE_SUCCESS:
            return {
                ...state,
                data: {
                    ...state.data,
                    nextPhone: payload.phone,
                    is_phone_verified: false,
                },
                isChanging: {
                    ...state.isChanging,
                    phone: false,
                },
                errors: {
                    ...state.errors,
                    phone: {},
                }
            };
        case ACTION.USER_PHONE_CHANGE_FAIL:
            return {
                ...state,
                isChanging: {
                    ...state.isChanging,
                    phone: false,
                },
                errors: {
                    ...state.errors,
                    phone: payload.errors,
                }
            };
        case ACTION.USER_PHONE_CONFIRM_TOKEN_SUCCESS:
            return {
                ...state,
                data: {
                    ...state.data,
                    is_phone_verified: payload.formType === 'add' ? true : false,
                },
                isChanging: {
                    ...state.isChanging,
                    phoneConfirmToken: false,
                },
                errors: {
                    ...state.errors,
                    phoneConfirmToken: {},
                }
            };
        case ACTION.USER_PHONE_CONFIRM_TOKEN_FAIL:
            return {
                ...state,
                isChanging: {
                    ...state.isChanging,
                    phoneConfirmToken: false,
                },
                errors: {
                    ...state.errors,
                    phoneConfirmToken: payload.errors,
                }
            };
        case ACTION.USER_PHONE_CONFIRM_CODE_SUCCESS:
            return {
                ...state,
                data: {
                    ...state.data,
                    phone: state.data.nextPhone,
                    nextPhone: '',
                    is_phone_verified: true,
                },
                isChanging: {
                    ...state.isChanging,
                    phoneConfirmCode: false,
                },
                errors: {
                    ...state.errors,
                    phoneConfirmCode: {},
                }
            };
        case ACTION.USER_PHONE_CONFIRM_CODE_FAIL:
            return {
                ...state,
                isChanging: {
                    ...state.isChanging,
                    phoneConfirmCode: false,
                },
                errors: {
                    ...state.errors,
                    phoneConfirmCode: payload.errors,
                }
            };
        case ACTION.USER_EMAIL_CHANGE_SUCCESS:
            return {
                ...state,
                isChanging: {
                    ...state.isChanging,
                    email: false,
                },
                errors: {
                    ...state.errors,
                    email: {},
                    emailChangeAccept: {},
                },
                data: {
                    ...state.data,
                    email: payload.email,
                    is_email_verified: false,
                }
            };
        case ACTION.USER_EMAIL_CHANGE_FAIL:
            return {
                ...state,
                isChanging: {
                    ...state.isChanging,
                    email: false,
                },
                errors: {
                    ...state.errors,
                    emailChange: payload.errors,
                },
            };

        case ACTION.USER_PROFILE_FORM_OPEN:
            Object.keys(isOpenForm).map((formName) => isOpenForm[formName] = false);
            return {
                ...state,
                isOpenForm: {
                    ...isOpenForm,
                    [payload.formName]: true,
                }
            };
        case ACTION.USER_PROFILE_FORM_CLOSE:
            if (payload.formName) {
                return {
                    ...state,
                    isOpenForm: {
                        ...state.isOpenForm,
                        [payload.formName]: false,
                    }
                };
            }
            Object.keys(isOpenForm).map((formName) => isOpenForm[formName] = false);
            return {
                ...state,
                isOpenForm
            };
        default:
            return state;
    }
};
