import ACTION from '../constants/actions';
import { USER_PRODUCTS_STATUS_NAMES } from '../constants/names';
const {
    USER_PRODUCTS_ARE_ACTIVE,
    USER_PRODUCTS_ARE_INACTIVE,
    USER_PRODUCTS_ARCHIVE
} = USER_PRODUCTS_STATUS_NAMES;


const initialState = {
    [USER_PRODUCTS_ARE_ACTIVE]: {
        isInitialising: false,
        isInited: false,
        isLoading: false,
        isPreviewLoading: false,
        errors: null,
        entities: [],
        paginator: {}
    },
    [USER_PRODUCTS_ARE_INACTIVE]: {
        isInitialising: false,
        isInited: false,
        isLoading: false,
        isPreviewLoading: false,
        errors: null,
        entities: [],
        paginator: {}
    },
    [USER_PRODUCTS_ARCHIVE]: {
        isInitialising: false,
        isInited: false,
        isLoading: false,
        isPreviewLoading: false,
        errors: null,
        entities: [],
        paginator: {}
    }
};


export default (state = initialState, action) => {
    const { type, payload = {} } = action;
    const { user_products_status } = payload;

    switch (type) {
        case ACTION.USER_PRODUCTS_INIT_START:
            return {
                ...state,
                [user_products_status]: {
                    ...state[user_products_status],
                    isInitialising: true
                }
            };
        case ACTION.USER_PRODUCTS_INIT_SUCCESS:
            return {
                ...state,
                [user_products_status]: {
                    ...state[user_products_status],
                    isInitialising: false,
                    isInited: true,
                    entities: payload.entities,
                    paginator: payload.paginator,
                    errors: null,
                }
            };
        case ACTION.USER_PRODUCTS_INIT_FAIL:
            return {
                ...state,
                [user_products_status]: {
                    ...state[user_products_status],
                    isInitialising: false,
                    errors: payload.errors,
                }
            };
        case ACTION.USER_PRODUCTS_GET_ITEMS_START:
            return {
                ...state,
                [user_products_status]: {
                    ...state[user_products_status],
                    isLoading: true,
                }
            };
        case ACTION.USER_PRODUCTS_GET_ITEMS_SUCCESS:
            return {
                ...state,
                [user_products_status]: {
                    ...state[user_products_status],
                    isLoading: false,
                    entities: payload.entities,
                    paginator: payload.paginator,
                    errors: null,
                }
            };
        case ACTION.USER_PRODUCTS_GET_ITEMS_FAIL:
            return {
                ...state,
                [user_products_status]: {
                    ...state[user_products_status],
                    isLoading: false,
                    errors: payload.errors,
                }
            };
        case ACTION.USER_PRODUCTS_GET_ITEM_START:
            return {
                ...state,
                [user_products_status]: {
                    ...state[user_products_status],
                    isPreviewLoading: true,
                    isPreviewLoaded: false
                }
            };
        case ACTION.USER_PRODUCTS_GET_ITEM_SUCCESS:
            const entity_id = state[user_products_status].entities.findIndex(product => product.id === payload.id);
            const new_entities = [...state[user_products_status].entities];

            new_entities[entity_id] = {...new_entities[entity_id],
                owner: {...payload.product.owner},
                address: {...payload.product.address}
            };

            return {
                ...state,
                [user_products_status]: {
                    ...state[user_products_status],
                    isPreviewLoading: false,
                    isPreviewLoaded: true,
                    entities: new_entities
                }
            };
        case ACTION.USER_PRODUCTS_GET_ITEM_FAIL:
            return {
                ...state,
                [user_products_status]: {
                    ...state[user_products_status],
                    isPreviewLoading: false,
                    isPreviewLoaded: false,
                    errors: payload.errors
                }
            };
        case ACTION.USER_PROFILE_PRODUCTS_TABS_INIT_START:
            return {
                ...state,
                [USER_PRODUCTS_ARE_ACTIVE]: {
                    ...state[USER_PRODUCTS_ARE_ACTIVE],
                    isInitialising: true,
                },
                [USER_PRODUCTS_ARE_INACTIVE]: {
                    ...state[USER_PRODUCTS_ARE_INACTIVE],
                    isInitialising: true,
                },
                [USER_PRODUCTS_ARCHIVE]: {
                    ...state[USER_PRODUCTS_ARCHIVE],
                    isInitialising: true,
                }
            };
        case ACTION.USER_PROFILE_PRODUCTS_TABS_INIT_SUCCESS:
            return {
                ...state,
                [USER_PRODUCTS_ARE_ACTIVE]: {
                    ...state[USER_PRODUCTS_ARE_ACTIVE],
                    isInitialising: false,
                    isInited: true,
                    entities: payload.user_products_are_active_entities,
                    paginator: payload.user_products_are_active_paginator,
                    errors: null,
                },
                [USER_PRODUCTS_ARE_INACTIVE]: {
                    ...state[USER_PRODUCTS_ARE_INACTIVE],
                    isInitialising: false,
                    isInited: true,
                    entities: payload.user_products_are_inactive_entities,
                    paginator: payload.user_products_are_inactive_paginator,
                    errors: null,
                },
                [USER_PRODUCTS_ARCHIVE]: {
                    ...state[USER_PRODUCTS_ARCHIVE],
                    isInitialising: false,
                    isInited: true,
                    entities: payload.user_products_archive_entities,
                    paginator: payload.user_products_archive_paginator,
                    errors: null,
                }
            };
        case ACTION.USER_PROFILE_PRODUCTS_TABS_INIT_FAIL:
            return {
                ...state,
                [USER_PRODUCTS_ARE_ACTIVE]: {
                    ...state[USER_PRODUCTS_ARE_ACTIVE],
                    isInitialising: false,
                    errors: payload.errors,
                },
                [USER_PRODUCTS_ARE_INACTIVE]: {
                    ...state[USER_PRODUCTS_ARE_INACTIVE],
                    isInitialising: false,
                    errors: payload.errors,
                },
                [USER_PRODUCTS_ARCHIVE]: {
                    ...state[USER_PRODUCTS_ARCHIVE],
                    isInitialising: false,
                    errors: payload.errors,
                }
            };
        case ACTION.USER_PRODUCTS_GET_MORE_ITEMS_START:
            return {
                ...state,
                [user_products_status]: {
                    ...state[user_products_status],
                    isLoading: true,
                }
            };
        case ACTION.USER_PRODUCTS_GET_MORE_ITEMS_SUCCESS:
            return {
                ...state,
                [user_products_status]: {
                    ...state[user_products_status],
                    isLoading: false,
                    entities: [...state[user_products_status].entities, ...payload.entities],
                    paginator: payload.paginator,
                    errors: null,
                }
            };
        case ACTION.USER_PRODUCTS_GET_MORE_ITEMS_FAIL:
            return {
                ...state,
                [user_products_status]: {
                    ...state[user_products_status],
                    isLoading: false,
                    errors: payload.errors,
                }
            };
        default:
            return state;
    }
};