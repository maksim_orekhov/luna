import ACTION from '../constants/actions';
import { MODERATION_PRODUCTS_STATUS_NAMES } from '../constants/names';
const {
    MODERATION_PRODUCTS_ARE_WAITING,
    MODERATION_PRODUCTS_ARE_DECLINED,
    MODERATION_PRODUCTS_ARE_PUBLISHED,
    MODERATION_PRODUCTS_ARCHIVE
} = MODERATION_PRODUCTS_STATUS_NAMES;


const initialState = {
    [MODERATION_PRODUCTS_ARE_WAITING]: {
        isInitialising: false,
        isInited: false,
        isLoading: false,
        isPreviewLoading: false,
        isPreviewLoaded: false,
        errors: null,
        entities: [],
        paginator: {}
    },
    [MODERATION_PRODUCTS_ARE_DECLINED]: {
        isInitialising: false,
        isInited: false,
        isLoading: false,
        isPreviewLoading: false,
        isPreviewLoaded: false,
        errors: null,
        entities: [],
        paginator: {}
    },
    [MODERATION_PRODUCTS_ARE_PUBLISHED]: {
        isInitialising: false,
        isInited: false,
        isLoading: false,
        isPreviewLoading: false,
        isPreviewLoaded: false,
        errors: null,
        entities: [],
        paginator: {}
    },
    [MODERATION_PRODUCTS_ARCHIVE]: {
        isInitialising: false,
        isInited: false,
        isLoading: false,
        isPreviewLoading: false,
        isPreviewLoaded: false,
        errors: null,
        entities: [],
        paginator: {}
    }
};


export default (state = initialState, action) => {
    const { type, payload = {} } = action;
    const { moderation_products_status } = payload;

    switch (type) {
        case ACTION.MODERATION_PRODUCTS_INIT_START:
            return {
                ...state,
                [moderation_products_status]: {
                    ...state[moderation_products_status],
                    isInitialising: true
                }
            };
        case ACTION.MODERATION_PRODUCTS_INIT_SUCCESS:
            return {
                ...state,
                [moderation_products_status]: {
                    ...state[moderation_products_status],
                    isInitialising: false,
                    isInited: true,
                    entities: payload.entities,
                    paginator: payload.paginator,
                    errors: null,
                }
            };
        case ACTION.MODERATION_PRODUCTS_INIT_FAIL:
            return {
                ...state,
                [moderation_products_status]: {
                    ...state[moderation_products_status],
                    isInitialising: false,
                    errors: payload.errors,
                }
            };
        case ACTION.MODERATION_PRODUCTS_GET_ITEMS_START:
            return {
                ...state,
                [moderation_products_status]: {
                    ...state[moderation_products_status],
                    isLoading: true,
                }
            };
        case ACTION.MODERATION_PRODUCTS_GET_ITEMS_SUCCESS:
            return {
                ...state,
                [moderation_products_status]: {
                    ...state[moderation_products_status],
                    isLoading: false,
                    entities: payload.entities,
                    paginator: payload.paginator,
                    errors: null,
                }
            };
        case ACTION.MODERATION_PRODUCTS_GET_ITEMS_FAIL:
            return {
                ...state,
                [moderation_products_status]: {
                    ...state[moderation_products_status],
                    isLoading: false,
                    errors: payload.errors,
                }
            };
        case ACTION.MODERATION_PRODUCTS_GET_ITEM_START:
            return {
                ...state,
                [moderation_products_status]: {
                    ...state[moderation_products_status],
                    isPreviewLoading: true,
                    isPreviewLoaded: false
                }
            };
        case ACTION.MODERATION_PRODUCTS_GET_ITEM_SUCCESS:
            const entity_id = state[moderation_products_status].entities.findIndex(product => product.id === payload.id);
            const new_entities = [...state[moderation_products_status].entities];
            new_entities[entity_id] = {...new_entities[entity_id],
                owner: {...payload.product.owner},
                address: {...payload.product.address}
            };

            return {
                ...state,
                [moderation_products_status]: {
                    ...state[moderation_products_status],
                    isPreviewLoading: false,
                    isPreviewLoaded: true,
                    entities: new_entities
                }
            };
        case ACTION.MODERATION_PRODUCTS_GET_ITEM_FAIL:
            return {
                ...state,
                [moderation_products_status]: {
                    ...state[moderation_products_status],
                    isPreviewLoading: false,
                    isPreviewLoaded: false,
                    errors: payload.errors
                }
            };
        case ACTION.OPERATOR_PRODUCTS_TABS_INIT_START:
            return {
                ...state,
                [MODERATION_PRODUCTS_ARE_WAITING]: {
                    ...state[MODERATION_PRODUCTS_ARE_WAITING],
                    isInitialising: true,
                },
                [MODERATION_PRODUCTS_ARE_DECLINED]: {
                    ...state[MODERATION_PRODUCTS_ARE_DECLINED],
                    isInitialising: true,
                },
                [MODERATION_PRODUCTS_ARE_PUBLISHED]: {
                    ...state[MODERATION_PRODUCTS_ARE_PUBLISHED],
                    isInitialising: true,
                },
                [MODERATION_PRODUCTS_ARCHIVE]: {
                    ...state[MODERATION_PRODUCTS_ARCHIVE],
                    isInitialising: true,
                }
            };
        case ACTION.OPERATOR_PRODUCTS_TABS_INIT_SUCCESS:
            return {
                ...state,
                [MODERATION_PRODUCTS_ARE_WAITING]: {
                    ...state[MODERATION_PRODUCTS_ARE_WAITING],
                    isInitialising: false,
                    isInited: true,
                    entities: payload.moderation_waiting_entities,
                    paginator: payload.moderation_waiting_paginator,
                    errors: null,
                },
                [MODERATION_PRODUCTS_ARE_DECLINED]: {
                    ...state[MODERATION_PRODUCTS_ARE_DECLINED],
                    isInitialising: false,
                    isInited: true,
                    entities: payload.moderation_declined_entities,
                    paginator: payload.moderation_declined_paginator,
                    errors: null,
                },
                [MODERATION_PRODUCTS_ARE_PUBLISHED]: {
                    ...state[MODERATION_PRODUCTS_ARE_PUBLISHED],
                    isInitialising: false,
                    isInited: true,
                    entities: payload.moderation_published_entities,
                    paginator: payload.moderation_published_paginator,
                    errors: null,
                },
                [MODERATION_PRODUCTS_ARCHIVE]: {
                    ...state[MODERATION_PRODUCTS_ARCHIVE],
                    isInitialising: false,
                    isInited: true,
                    entities: payload.moderation_archive_entities,
                    paginator: payload.moderation_archive_paginator,
                    errors: null,
                }
            };
        case ACTION.OPERATOR_PRODUCTS_TABS_INIT_FAIL:
            return {
                ...state,
                [MODERATION_PRODUCTS_ARE_WAITING]: {
                    ...state[MODERATION_PRODUCTS_ARE_WAITING],
                    isInitialising: false,
                    errors: payload.errors,
                },
                [MODERATION_PRODUCTS_ARE_DECLINED]: {
                    ...state[MODERATION_PRODUCTS_ARE_DECLINED],
                    isInitialising: false,
                    errors: payload.errors,
                },
                [MODERATION_PRODUCTS_ARE_PUBLISHED]: {
                    ...state[MODERATION_PRODUCTS_ARE_PUBLISHED],
                    isInitialising: false,
                    errors: payload.errors,
                },
                [MODERATION_PRODUCTS_ARCHIVE]: {
                    ...state[MODERATION_PRODUCTS_ARCHIVE],
                    isInitialising: false,
                    errors: payload.errors,
                }
            };
        default:
            return state;
    }
};