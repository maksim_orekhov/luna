import orderBy from 'lodash/orderBy';

/**
 * Fetch запрос с дефолтными настройками
 * @param {string} url - адрес запроса
 * @param {Object} options - опции fetch запроса, аналогично обычному fetch запросу
 * @returns {Promise} - обработанный ответ от сервера по статусу
 * @example
 * Пример GET запроса
 * customFetch(URLS.CSRF.GET)
 *  .then(data => {
 *      if (data.status === 'SUCCESS') {
 *          const { csrf } = data.data;
 *          this.setState({
 *              csrf
 *          });
 *      } else if (data.status === 'ERROR') {
 *          return Promise.reject('Ошибка при получении инициализации формы. Попробуйте обновить страницу.');
 *      }
 *  })
 *  .catch(error => this.updateFormError(error));
 *
 * Пример POST запроса с параметрами
 * customFetch('home', {
 *      method: 'POST',
 *      body: JSON.stringify({
 *           csrf
 *       })
 *  })
 *    .then(data => {
 *       if (data.status === 'SUCCESS') {
 *           handleOnSubmit && handleOnSubmit();
 *       } else if (data.status === 'ERROR') {
 *           return Promise.reject(data.message);
 *       }
 *    })
 *    .catch(err => {
 *       this.updateFormError(err);
 *    });
 */
export const customFetch = (url, options = {}) => {
    return fetch(url, {
        headers: {
            'X-Requested-With': 'XMLHttpRequest'
        },
        credentials: 'include',
        ...options
    })
        .then(response => {
            const { status } = response;
            if (status === 200) {
                return response.json();
            } else {
                return Promise.reject(response);
            }
        });
};

export const getIdsFromString = (start_string) => {
    // Удаляем адрес хоста
    const pattern_host = /^(https?:\/\/)?(([\w\.]+)\.([a-z]{2,6}\.?)|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/;
    const string = start_string.replace(pattern_host, '');

    const pattern_id = /\/([1-9]\d*)\/?/g;
    const ids = string.match(pattern_id);

    if (ids) {
        return ids.map(id => id.replace(/\D/g, ''));
    }

    return ids;
};

export const getIdFromUrl = (url = window.location.href) => {
    const ids = getIdsFromString(url);

    if (ids && ids.length) {
        return ids[ids.length - 1]; // Возвращаем последний найденный id
    }

    return ids;
};

// Функция сортировки, которая использует lodash/orderBy
export const sorting = (array, sortName, sortDirection) => {
    return orderBy(array, [sortName], [sortDirection]);
};

// Функция, которая меняет направление сортировки
export const changeSortDirection = (name, sortName, sortDirection) => {
    let newSortDirection;
    if (name !== sortName) { // если кликаем по новому столбцу, то ставим сортировку по возрастанию у этого столбца, иначе просто меняем направление у текущего
        newSortDirection = 'asc';
    } else {
        switch (sortDirection) {
            case 'asc':
                newSortDirection = 'desc';
                break;
            case 'desc':
                newSortDirection = 'asc';
        }
    }
    return newSortDirection;
};

// Универсальная функция для фильтрации данных в массиве, в случае плоской структуры данных. Если необходимо фильтровать по вложенным данным, например, arrayItem.deal_status.status, то необходимо писать кастомный метод для фильтрации, т.к. arrayItem[filterName] работать уже не будет.
export const filterArray = (filterName, filterValue) => {
    const matches = new RegExp(filterValue, '\i');
    return arrayItem => !filterValue || matches.test(arrayItem[filterName]);
};

// кастомная функци для фильтрации вложенных данных в массиве, например, arrayItem.deal_status.status
export const filterCustomArray = (filterFirstArgument, filterSecondArgument, filterValue) => {
    const matches = new RegExp(filterValue, '\i');
    return arrayItem => !filterValue || matches.test(arrayItem[filterFirstArgument][filterSecondArgument]);
};

export const filterArrayStrict = (filterName, filterValue) => {
    const matches = new RegExp(`^${filterValue}$`, '\i');
    return arrayItem => !filterValue || matches.test(arrayItem[filterName]);
};

// Фильтрация по ФИО
export const filterFullName = (fullName) => {
    const matches = new RegExp(fullName, '\i');
    return arrayItem =>
        !fullName
      || matches.test(`${arrayItem.last_name} ${arrayItem.first_name} ${arrayItem.secondary_name}`)
      || matches.test(`${arrayItem.first_name} ${arrayItem.last_name} ${arrayItem.secondary_name}`)
      || matches.test(`${arrayItem.first_name} ${arrayItem.secondary_name} ${arrayItem.last_name}`);
};

// Фильтрация по дате создания. Дата ОТ
export const filterCreatedFrom = (filterName, filterValue) => {
    return arrayItem => !filterValue || arrayItem[filterName] >= filterValue;
};

// Фильтрация по дате создания.  Дата ДО
export const filterCreatedTill = (filterName, filterValue) => {
    return arrayItem => !filterValue || arrayItem[filterName] <= filterValue;
};

// Фильтрация по точной дате.
export const filterCreated = (filterName, filterValue) => {
    return arrayItem => !filterValue || (arrayItem[filterName] >= filterValue && arrayItem[filterName] <= filterValue + 84600);
};

// Фильтрация по дате создания сделки в TableDealsTrouble.
export const filterCreatedDate = (filterName, filterValue) => {
    return arrayItem => !filterValue || (arrayItem.deal[filterName] >= filterValue && arrayItem.deal[filterName] <= filterValue + 84600);
};

/**
 * Функция перевода даты в секунды
 */
export const setDateToSeconds = (date) => {
    let new_date = new Date(date.split('.').reverse().join(','));
    let dd = new_date.getDate();
    let mm = new_date.getMonth() + 1;
    let yy = new_date.getFullYear();
    let newDate = yy + ',' + mm + ',' + dd;
    return Date.parse(newDate) / 1000;
};


/**
 * Добавляет дополнительное полю элементу
 * @param {Array} array - массив состоящий из объектов, каждому из которых нужно добавить доп.поле
 * @param {string} fieldName - название поля
 * @param fieldValue - значение нового поля
 * @returns {Array}
 */
export const addKeyToObjectsInArray = (array = [], fieldName, fieldValue) => {
    /**
     * @param {Object} element
     */
    Array.isArray(array) && array.forEach(element => {
        element[fieldName] = fieldValue;
    });

    return array;
};

/**
 * @param {Object} request_types
 * @returns {Array}
 */
export const addTypeToDisputeRequests = (request_types) => {
    const array = [];
    for (let [type, requests] of Object.entries(request_types)) {
        const new_requests = addKeyToObjectsInArray(requests, 'request_type', type) || [];
        array.push(new_requests);
    }
    return array;
};

export const checkControlErrors = (obj) => {
    for (let key in obj) {
        if (obj[key] === true) return key;                      // key == true
        else if (obj[key] === null) return null;
    }
    return false;
};

export const checkControlHints = (obj) => {
    for (let key in obj) {
        if (obj[key] === true) return key;                      // key == true
        else if (obj[key] === null) return null;
    }
    return false;
};

/**
 * @param {string} string
 * @returns {string} - строка проведенная к формату Первой загловоной буквы и из которой вырезаны все символы кроме букв
 */
export const replaceToTextCapitalize = (string) => (string.charAt(0).toUpperCase() + string.slice(1)).replace(/[\d`~!@#$%^&*()_|+=?;:'",.<>\{\}\[\]\\\/№§]/g, '').replace(/[Ё]/g, 'Е').replace(/[ё]/g, 'е');

/**
 * замена буквы ё на е
 * @param {string} string
 * @returns {string} - строка, в которой все буквы Ё(ё) заменены на Е(е)
 */
export const replaceRusLetterE = (string) => (string.replace(/[Ё]/g, 'Е').replace(/[ё]/g, 'е'));

/**
 * Вырезает все символы из строки кроме цифр
 * @param {string} string
 * @returns {string} - строка не содержащая любые символы кроме цифр
 */
export const textOnlyNumbers = (string) => string.replace(/\D/g, '');

/**
 * Разбивает строку по словам и возвращает их количество
 * @param {string} string
 * @returns {number} - количество слов в строке
 */
export const wordsCountInString = (string) => {
    string = string.replace(/(^\s*)|(\s*$)/gi, ''); // exclude  start and end white-space
    string = string.replace(/[ ]{2,}/gi, ' '); // 2 or more space to 1
    string = string.replace(/\n /, '\n'); // exclude newline with a start spacing
    return string.split(' ').length;
};

// Добавляет пробелы между разрядами числа
export const addSpacesBetweenThousands = (value) => {
    return String(value).replace(/[^\d.]+|(\.\d{2})\d*$/g, '$1').replace(/\d(?=(?:\d{3})+(?!\d))/g, '$& ');
};

// Форматирование БИК - удаляет всё кроме 04 из начала значения
export const bikTextFormat = (value, prevValue) => {
    let first_symbol = value[0] || '0';
    let second_symbol = value[1] || '4';

    first_symbol = first_symbol === '0';
    second_symbol = second_symbol === '4';
    if (first_symbol && second_symbol) {
        return textOnlyNumbers(value);
    } else {
        return textOnlyNumbers(prevValue);
    }
};

/**
 * @return {string} - возвращает сегодняшнюю дату в формате dd.mm.yyyy
 */
export const getDateNow = () => {
    const today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1; // January is 0!
    const yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10){
        mm = '0' + mm;
    }
    return `${dd}.${mm}.${yyyy}`;
};

/**
 * @param {string} date - необязательный параметр даты в формате dd.mm.yyyy, если он не передан берем дату сейчас
 * @return {string} - возвращает дату сейчас в формате 21 Июня 2018
 */
export const getDateNowInPrettyFormat = (date = getDateNow()) => {
    const [day, month, year] = date.split('.');

    const months = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];

    // Получаем по ключу имя месяца, для этого вычитаем из него 1, т.к. первый индекс массива 0
    return `${day} ${months[Number(month) - 1]} ${year}`;
};

/**
 * Функция перевода даты в евро формат
 */
export const setDateToNormalFormat = (date) => {
    let new_date = new Date(date.split('.').reverse().join('.'));
    let dd = new_date.getDate();
    let mm = new_date.getMonth() + 1;
    let yy = new_date.getFullYear();
    return getDateNowInPrettyFormat(dd + '.' + mm + '.' + yy);
};

/**
 * Функция для поддержания единого стиля вывода имени физического лица
 * @param {string} first_name
 * @param {string} last_name
 * @param {string} secondary_name
 * @return {string}
 */
export const getNaturalPersonName = (first_name, last_name, secondary_name) => {
    return `${last_name} ${first_name} ${secondary_name}`;
};

/**
 * Функция для получения параметра из текущего URL по имени параметра
 * @param {string} url_params
 * @param {string} name
 * @return {string}
 */
export const getUrlParameter = (url_params, name) => {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    let regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    let results = regex.exec(url_params);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};

/**
 * Функция для установления в заданном URL параметра с заданным именем в заданное значение
 * @param {string} url
 * @param {string} name
 * @param {string} value
 * @return {string}
 */
export const setUrlParameter = (url, name, value) => {
    let regex = new RegExp('([?&])' + name + '=.*?(&|$)', 'i');
    let separator = url.indexOf('?') !== -1 ? '&' : '?';
    if (url.match(regex)) {
        return url.replace(regex, '$1' + name + '=' + value + '$2');
    }
    else {
        return url + separator + name + '=' + value;
    }
};

/**
 * Функция для установления параметров в заданном URL
 * @param {string} url
 * @param {string} name
 * @param {string} value
 * @return {string}
 */
export const setUrlWithQueryParams = (url, name) => {
    let arr = decodeURIComponent(url).slice(url.indexOf('?') + 1).split('&');
    let regex = new RegExp('page=', 'i');
    let query_param = '';
    let separator = '?';

    for (let i = 0; i < arr.length; i++) {
        if (arr[i].length) {
            separator = (query_param.length === 0) ? '?' : '&';
            if (!regex.test(arr[i])) {
                query_param = query_param + separator + arr[i];
            }
        }
    }

    return (query_param.length === 0) ? query_param + '?page=' + name : query_param +  '&page=' + name;
};

export const getCookie = (name) => {
    let matches = document.cookie.match(new RegExp(
        '(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
};

export const setCookie = (name, value, options) => {
    options = options || {};

    let expires = options.expires;

    if (typeof expires == 'number' && expires) {
        let d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    let updatedCookie = name + '=' + value;

    for (let propName in options) {
        updatedCookie += '; ' + propName;
        let propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += '=' + propValue;
        }
    }

    document.cookie = updatedCookie;
};

export const deleteCookie = (name) => {
    setCookie(name, '', {
        expires: -1
    });
};

export const readAndDeleteCookie = (name) => {
    const cookie = getCookie(name);
    if (cookie) {
        deleteCookie(name);
        return cookie;
    }
    return null;
};

export const getReCaptchaSiteKey = () => {
    const site_key_element = document.getElementById('recaptcha-site-key');
    if (site_key_element) {
        if (site_key_element.getAttribute('data-site-key')) {
            return site_key_element.getAttribute('data-site-key');
        }
    }
    console.log('reCaptcha site key is not defined');
    return null;
};

export const getReCaptchaSrc = () => {
    return 'https://www.google.com/recaptcha/api.js?hl=ru';
};

export const getInlineJSON = (param) => {
    const scriptTag = document.getElementById(param);
    if(!scriptTag){
        return false;
    }
    let data = null;

    try {
        data = JSON.parse(scriptTag.textContent);
    } catch (e) {
        console.error('error in get inline json', e);
    }

    return data;
};

export const getMetaCSRF = () => {
    return document.head.querySelector('[name="csrf_token"]').content;
};

export const getDisplayName = (WrappedComponent) => {
    return WrappedComponent.displayName || WrappedComponent.name || 'Component';
};

/**
 * Возвращает объект для селекта с категориями верхнего уровня
 * */
export const getParentCategories = (categories) => {
    return categories.map((category) => {
        return {
            title: category.name,
            value: category.id
        };
    });
};

/**
 * Возвращает объект для селекта с подкатегориями
 * */
export const getSubcategoriesByParentCategoryId = (parentCategoryId, categories = []) => {
    if(!parentCategoryId || categories.length === 0) {
        return [];
    }
    const parentCategory = categories.find(category => category.id === parentCategoryId);
    if(!parentCategory || !parentCategory.subcategories || parentCategory.subcategories.length === 0) {
        return [];
    }

    return parentCategory.subcategories.map((category) => {
        return {
            title: category.name,
            value: category.id
        };
    });
};

export const prettifyPhoneNumber = (phoneNumber) => {
    if (phoneNumber) {
        return '+' + phoneNumber.substr(0, 1) + ' ' + '(' + phoneNumber.substr(1, 3) + ')' + ' ' + phoneNumber.substr(4, 3) + ' - ' + phoneNumber.substr(7, 2) + ' - ' + phoneNumber.substr(9, 2);
    }

    return '';
};

export const checkIfKeyIsExistAndNotNull = (object, key) => {
    return (object.hasOwnProperty(key) && object.key);
};

// Пример checkThatNestedKeyOfObjectExist(product, 'category', 'name') - true
// Пример checkThatNestedKeyOfObjectExist(product, 'category', 'name', 'ops') - false
// Пример checkThatNestedKeyOfObjectExist(product, 'category', 'foo') - true
export function checkThatNestedKeyOfObjectExist(obj /* , level1, level2, ... levelN */) {
    const args = Array.prototype.slice.call(arguments, 1);

    for (let i = 0; i < args.length; i++) {
        if (!obj || !obj.hasOwnProperty(args[i])) {
            return false;
        }
        obj = obj[args[i]];
    }
    return true;
}

export const getCategoryNameForProduct = (product) => {
    if (checkThatNestedKeyOfObjectExist(product, 'category', 'name') && product.category.name) {
        return product.category.name;
    } else {
        return 'неизвестно';
    }
};

export const getSubCategoryNameForProduct = (product) => {
    if (checkThatNestedKeyOfObjectExist(product, 'sub_category', 'name') && product.sub_category.name) {
        return product.sub_category.name;
    } else {
        return 'неизвестно';
    }
};

export const getModerationResolutionTimeForProduct = (product) => {
    if (checkThatNestedKeyOfObjectExist(product, 'product_moderation', 'resolution_date') && product.product_moderation.resolution_date) {
        return product.product_moderation.resolution_date;
    } else {
        return 'неизвестно';
    }
};

export const getAuthorNameForProduct = (product) => {
    if (checkThatNestedKeyOfObjectExist(product, 'owner', 'name') && product.owner.name) {
        return product.owner.name;
    } else {
        return 'неизвестно';
    }
};

export const translateProductStatus = (status) => {
    const statuses = {
        waiting_moderation: 'Ожидает модерации',
        publish_stopped: 'Показ остановлен',
        declined: 'Отклонено модератором'
    };

    if (statuses[status]) return statuses[status];

    return status;
};

export const getProductStatus = (product) => {
    if (checkThatNestedKeyOfObjectExist(product, 'status', 'status') && product.status.status) {
        return translateProductStatus(product.status.status);
    } else {
        return 'неизвестно';
    }
};

export const getProductStatusCode = (product) => {
    if (checkThatNestedKeyOfObjectExist(product, 'status', 'code') && product.status.code) {
        return product.status.code;
    } else {
        return null;
    }
};

export const getProductStatusIconClassName = (status_code) => {
    switch (status_code) {
        case 1:
            return 'waiting-for-moderation';
        case 10:
            return 'rejected-by-moderator';
        case 20:
            return 'showing-stopped';
        default:
            return '';
    }
};

export const checkShowButtonLoadMoreItems = (paginator) => {
    return (paginator && Object.values(paginator).length && paginator.current && paginator.last && paginator.current < paginator.last);
};

export const getMessageTextByUserActionName = (actionName) => {
    switch (actionName) {
        case 'moderation':
            return {
                title: 'Объявление на модерации',
                description: 'После проверки ваше объявление перейдет в статус активные, и станет видимым для покупателей.',
            };
        case 'purchase_moderation':
            return {
                title: 'Закупка на модерации',
                description: 'После проверки ваша закупка перейдет в статус активные, и станет видимой для покупателей.',
            };
        case 'active':
            return {
                title: 'Показ возобновлён',
                description: 'Объявление активно и видно покупателям.',
            };
        case 'archive':
            return {
                title: 'Перенесено в архив',
                description: 'Ваше объявление перенесено в архив. Через 30 дней оно будет удалено безвозвратно.',
            };
        case 'archive_restore':
            return {
                title: 'Восстановлено из архива',
                description: 'Ваше объявление успешно восстановлено.',
            };
        case 'archive_deleted':
            return {
                title: 'Удаление из архива',
                description: 'Ваше объявление успешно удалено.',
            };
        case 'inactive':
            return {
                title: 'Показ приостановлен',
                description: 'Объявление перешло в статусе неактивные. Вы сможете возобновить его показ в любой момент.',
            };
        case 'popup_init_error':
            return {
                title: 'Ошибка',
                description: 'При получении данных произошла ошибка. Попробуйте обновить страницу.'
            };
        case 'not_auth':
            return {
                title: 'Пользователь не авторизован',
                description: 'Пожалуйста авторизуйтесь.'
            };
        default:
            return {
                title: '',
                description: '',
            };
    }
};

export const getCategoryNameForPurchase = (purchase) => {
    if (checkThatNestedKeyOfObjectExist(purchase, 'category', 'name') && purchase.category.name) {
        return purchase.category.name;
    } else {
        return 'неизвестно';
    }
};

export const getSubCategoryNameForPurchase = (purchase) => {
    if (checkThatNestedKeyOfObjectExist(purchase, 'sub_category', 'name') && purchase.sub_category.name) {
        return purchase.sub_category.name;
    } else {
        return 'неизвестно';
    }
};

export const getModerationResolutionTimeForPurchase = (purchase) => {
    if (checkThatNestedKeyOfObjectExist(purchase, 'moderation', 'resolution_date')
        && purchase.moderation.resolution_date)
    {
        return purchase.moderation.resolution_date;
    } else {
        return 'неизвестно';
    }
};

export const getAuthorNameForPurchase = (purchase) => {
    if (checkThatNestedKeyOfObjectExist(purchase, 'owner', 'name') && purchase.owner.name) {
        return purchase.owner.name;
    } else {
        return 'неизвестно';
    }
};

export const getPurchaseStatus = (purchase) => {
    if (checkThatNestedKeyOfObjectExist(purchase, 'status', 'status') && purchase.status.status) {
        return translateProductStatus(purchase.status.status);
    } else {
        return 'неизвестно';
    }
};

export const prepareTimestamp = (timestamp) => {
    if (!timestamp) return '';

    return +timestamp * 1000;
};

export const getFormattedDate = (date) => {
    const date_string = date.toISOString().substring(0, 10).split('-');
    return date_string[2].replace(/^0+/, '') + '.' + date_string[1] + '.' + date_string[0];
};

export const getDateDotFormatFromTimestamp = (date_timestamp) => {
    if (!date_timestamp) return '';

    const prepared_timestamp = prepareTimestamp(date_timestamp);
    let final_date = '';
    if (prepared_timestamp) {
        const date = new Date(prepared_timestamp);
        if (date) {
            final_date = getFormattedDate(date);
        }
    }

    return final_date;
};

export const getDateInPrettyFormatFromTimestamp = (date_timestamp) => {
    if (!date_timestamp) return '';

    const prepared_timestamp = prepareTimestamp(date_timestamp);

    if (!prepared_timestamp) return '';

    const date = new Date(prepared_timestamp);

    if (!date) return '';

    const dd = date.getDate();
    const mm = date.getMonth() + 1;
    const yy = date.getFullYear();
    return getDateNowInPrettyFormat(dd + '.' + mm + '.' + yy);
};

/**
 * @param object
 * @param first_level_key - string
 * @param date_key - string
 * @returns {string}
 * Функция, для получения даты в формате через точку (19.11.2018) для вложенных объектов.
 * Пример использования:
 * Если у нас такой объект
 * const product = {
 *   product_moderation: {
 *      created: 1231254515 (время в timestamp)
 *   }
 * }
 * то вызов функция для него будет вида: getDateDotFormatForNestedProperty(product, 'product_moderation', 'created');
 */
export const getDateDotFormatForNestedProperty = (object, first_level_key, date_key) => {
    if (!object) return '';

    if (object && object.hasOwnProperty(first_level_key) && object[first_level_key]) {
        return getDateDotFormatFromTimestamp(object[first_level_key][date_key]);
    }
    return '';
};

export const getProductResolutionDateDotFormat = (product) => {
    return getDateDotFormatForNestedProperty(product, 'product_moderation', 'resolution_date_timestamp');
};

export const getProductModerationCreatedDateDotFormat = (product) => {
    return getDateDotFormatForNestedProperty(product, 'product_moderation', 'created_timestamp');
};

export const getPurchaseResolutionDateDotFormat = (purchase) => {
    return getDateDotFormatForNestedProperty(purchase, 'moderation', 'resolution_date_timestamp');
};

export const getPurchaseModerationCreatedDateDotFormat = (purchase) => {
    return getDateDotFormatForNestedProperty(purchase, 'moderation', 'created_timestamp');
};

export const saveToSessionStorage = (key, value) => {
    if (!key) return;

    if (sessionStorage) {
        sessionStorage.setItem(key, value);
    }
};

export const getFromSessionStorage = (key) => {
    if (sessionStorage) {
        return sessionStorage.getItem(key);
    }
};

export const removeFromSessionStorage = (key) => {
    if (sessionStorage) {
        sessionStorage.removeItem(key);
    }
};

export const prepareDataToSaveSessionStorage = (data, categories) => {
    if (!data || typeof data !== 'object') return;

    const form_data = {...data};

    const categoryId = form_data.categoryId;
    const subcategoryId = form_data.subcategoryId;

    delete form_data.safe_deal;
    delete form_data.categoryId;
    delete form_data.subcategoryId;
    delete form_data.files;

    // -------------- Сохранение категорий ---------------
    let category;
    if (categoryId && subcategoryId) {
        const current_category = categories.find(category => category.id == categoryId);

        if (current_category) {
            category = {
                parent_id: categoryId,
                parent_name: current_category.name
            };
            if (subcategoryId && current_category.subcategories && Array.isArray(current_category.subcategories)) {
                const current_subcategory = current_category.subcategories.find(subcategory => subcategory.id == subcategoryId);
                if (current_subcategory) {
                    category.id = subcategoryId;
                    category.name = current_subcategory.name;
                }
            }
        }
    }

    if (category) form_data.category = category;

    // ------------- Сохранение адреса ------------------
    if (form_data.hasOwnProperty('address') && form_data.address && typeof form_data.address === 'object') {
        form_data.address = {...form_data.address};
        if (form_data.address.hasOwnProperty('latitude') && form_data.address.latitude) {
            form_data.address.lat = form_data.address.latitude;
            delete form_data.address.latitude;
        }

        if (form_data.address.hasOwnProperty('longitude') && form_data.address.longitude) {
            form_data.address.lng = form_data.address.longitude;
            delete form_data.address.longitude;
        }
    }

    // -------------- Картинки -----------------------
    if (form_data.hasOwnProperty('gallery') && form_data.gallery && Array.isArray(form_data.gallery) && form_data.gallery.length) {
        form_data.gallery = [...form_data.gallery];

        const filtered_gallery = form_data.gallery.filter((image) => image); // Удаляем пустые слоты массива, они возникают при удалении картинки

        form_data.gallery = filtered_gallery.map((image) => {
            if (image.hasOwnProperty('image')) {
                return {...image.image};
            }
            return image;
        });
    }

    return JSON.stringify(form_data);
};

