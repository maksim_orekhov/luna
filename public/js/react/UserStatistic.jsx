import React from 'react';
import PropTypes from 'prop-types';
import FormAvatar from './forms/FormAvatar';
import Popup from './Popup';
import FormCreateShop from './forms/FormCreateShop';
import FormBecomeAManufacturer from './forms/FormBecomeAManufacturer';

export default class UserStatistic extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpenedFormCreateShop: false,
            isOpenedFormBecomeAManufacturer: false,
            isUserManufacturer: false
        };

        this.handleClosePopup = this.handleClosePopup.bind(this);
    }

    showForm = (form_name) => () => {
        this.setState({
            [form_name]: true
        });
    };

    handleClosePopup() {
        this.setState({
            isOpenedFormCreateShop: false,
            isOpenedFormBecomeAManufacturer: false
        });
    }

    render() {
        const { isOpenedFormCreateShop, isOpenedFormBecomeAManufacturer, isUserManufacturer } = this.state;
        const {name, login, phone, email, is_email_verified, is_phone_verified, created, count_published_products, avatar} = this.props;

        return (
            <div className={`col-4 col-tablet-6 col-mobile-8 col-double-padding user-info-block_statistic`}>
                <div className="user-info-block-wrap">
                    <div className="user-bar">
                        <div className="col-mobile-8">
                            <div className="Profile-Avatar">
                                <div className="AvatarWithBorder">
                                    <div className="AvatarWithBorder-Container">
                                        <img src={avatar} />
                                        <FormAvatar />
                                    </div>
                                </div>
                                <div className="user-statistic-info">
                                    <div className="user-bar-name">{name}</div>
                                    <div className="rating-scale">
                                        <img alt="" src="/img/star_blue.svg" />
                                        <img alt="" src="/img/star.svg" />
                                        <img alt="" src="/img/star.svg" />
                                        <img alt="" src="/img/star.svg" />
                                        <img alt="" src="/img/star.svg" />
                                    </div>
                                    <div className="user-statistic">
                                        <div className="user-statistic-row">
                                            <span className="title">Регистрация:</span>
                                            <span className="value">{created}</span>
                                        </div>
                                        <div className="user-statistic-row">
                                            <span className="title">Объявлений:</span>
                                            <span className="value">{count_published_products}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {
                                isUserManufacturer ?
                                    <div>
                                        <div className="manufacturer-info">
                                            <div className="manufacturer-info-title">Вы производитель:</div>
                                            <div className="manufacturer-info-name">ООО «Брянский кирпичный завод»</div>
                                        </div>
                                        <div className="shop-info">
                                            <div className="shop-info-title">Ваш магазин:</div>
                                            <div className="shop-info-name">Мир кирпичей</div>
                                        </div>
                                        <button
                                            className="button-blue button-become-shop"
                                            type="button">
                                            Настройки «Магазина»
                                        </button>
                                    </div>
                                    :
                                    <div>
                                        <div className="manufacturer-status-link" onClick={this.showForm('isOpenedFormBecomeAManufacturer')}>
                                            Вы производитель товаров? Получите статус, который сделает вас заметнее!
                                        </div>
                                        <div className="create-shop-info">Станьте магазином! Вы сможете получить свой раздел с уникальным оформлением.</div>
                                        <button
                                            className="button-blue js-button-shop button-create-shop"
                                            onClick={this.showForm('isOpenedFormCreateShop')} type="button">Стать магазином
                                        </button>
                                    </div>
                            }
                        </div>
                    </div>
                </div>
                {
                    isOpenedFormBecomeAManufacturer &&
                    <Popup close={this.handleClosePopup} popupClassName="js-popup-container-shop">
                        <FormBecomeAManufacturer
                            name={name}
                            login={login}
                            phone={phone}
                            email={email}
                            is_phone_verified={is_phone_verified}
                            is_email_verified={is_email_verified}
                            handleClosePopup={this.handleClosePopup}
                        />
                    </Popup>
                }
                {
                    isOpenedFormCreateShop &&
                    <Popup close={this.handleClosePopup} popupClassName="js-popup-container-shop">
                        <FormCreateShop
                            name={name}
                            login={login}
                            phone={phone}
                            email={email}
                            is_phone_verified={is_phone_verified}
                            is_email_verified={is_email_verified}
                            handleClosePopup={this.handleClosePopup}
                        />
                    </Popup>
                }
            </div>
        );
    }
}

UserStatistic.propTypes = {
    name: PropTypes.string.isRequired,
    created: PropTypes.string.isRequired,
    count_published_products: PropTypes.number.isRequired,
    avatar: PropTypes.string,
};

UserStatistic.defaultProps = {
    avatar: '/img/no-avatar.png',
};