import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import LoaderSpinner from '../components/LoaderSpinner';
import FormPurchase from '../forms/FormPurchase';
import PurchasePreview from '../components/PurchasePreview';
import withProvider from '../hocs/withProvider';
import Popup from '../Popup';
import {
    loadCategories, fetchSubcategories, loadCSRF, sendFormPurchase, initFormPurchase, closePurchasePreview, sendConfirmationPurchaseForm
} from '../actions';
import URLS from '../constants/urls';


class FormPurchaseContainer extends Component {
    constructor(props) {
        super(props);

        this.form_name = props.formType === 'create' ? 'formPurchase' : null;
    }

    state = {
        isInited: false,
    };

    componentDidMount() {
        const { categories, csrf, initCategories, initCSRF, initForm, formIsInited, formIsIniting } = this.props;
        this.initForm();
        if (!formIsInited && !formIsIniting) {
            initForm(this.form_name);
        }
        if (!categories.isLoading && !categories.isLoaded) {
            initCategories();
        }
        if (!csrf.isLoading && !csrf.isLoaded) {
            initCSRF();
        }
    }

    componentDidUpdate () {
        const { categories, defaultValues, hasDefaultValues, fetchSubcategories } = this.props;
        const { isInited } = this.state;
        if (!isInited) {
            if (hasDefaultValues
                && defaultValues.category
                && defaultValues.category.parent_id
                && !categories.isFetching
                && categories.entities.find(category => category.id === defaultValues.category.parent_id)
                && !categories.entities.find(category => category.id === defaultValues.category.parent_id).subcategories
            ) {
                fetchSubcategories(defaultValues.category.parent_id);
            }
            this.initForm();
        }
    }

    initForm = () => {
        const { formIsInited, categories, csrf, defaultValues, hasDefaultValues } = this.props;
        if (
            formIsInited
            && csrf.isLoaded
            && categories.isLoaded
            && (!hasDefaultValues
                || !defaultValues.category
                || !defaultValues.category.parent_id
                || (!categories.entities.find(category => category.id === defaultValues.category.parent_id)
                    || categories.entities.find(category => category.id === defaultValues.category.parent_id).subcategories
                )
            )
        ) {
            this.setState({ isInited: true });
        }
    };

    /**
     * Возвращает объект FormData для отправки на сервер
     * @param submitData
     * @returns {FormData}
     */
    createFormData = (submitData) => {
        const { csrf, uploadedFiles } = this.props;
        const formData = new FormData();

        formData.append('csrf', csrf.value);
        Object.keys(submitData).forEach((fieldName) => {
            if(Array.isArray(submitData[fieldName])){
                let itemIndex = 0;
                submitData[fieldName].map((item) => {
                    if(item){
                        formData.append(`${fieldName}[${itemIndex}]`, item);
                        itemIndex += 1;
                    }
                });
            } else if (typeof submitData[fieldName] === 'object' && submitData[fieldName] !== null) {
                Object.keys(submitData[fieldName]).forEach((propertyName) => {
                    formData.append(`${fieldName}[${propertyName}]`, submitData[fieldName][propertyName]);
                });
            } else {
                formData.append(fieldName, submitData[fieldName]);
            }
        });

        if (uploadedFiles) {
            let uploadFileIndex = 0;
            uploadedFiles.map((file) => {
                if (file !== null && file !== undefined) {
                    if (submitData.files.find(submit_file => submit_file.id === file.id) !== undefined) {
                        formData.append(`files[${uploadFileIndex}][id]`, file.id);
                        formData.append(`files[${uploadFileIndex}][path]`, file.path);
                        formData.append(`files[${uploadFileIndex}][name]`, file.name);
                        uploadFileIndex++;
                    }
                }
            });
        }

        return formData;
    };

    sendForm = (data) => {
        const { sendForm, formType, defaultValues } = this.props;
        sendForm({
            formType,
            formData: this.createFormData(data),
            purchaseId: defaultValues ? defaultValues['id'] : null
        });
    };

    sendConfirmation = () => {
        const { csrf, sendConfirmation, formType, defaultValues } = this.props;
        const formData = new FormData();
        formData.append('preview_confirm', 1);
        formData.append('csrf', csrf.value);
        sendConfirmation({
            formData,
            confirmUrl: formType === 'create' ? URLS.FORM.PURCHASE_CREATE_SAVE : URLS.FORM.PURCHASE_EDIT_CONFIRM(defaultValues['id']),
            form_name: this.form_name
        });
    };

    handleClosePreviewPopup = (ev) => {
        const { closePurchasePreview } = this.props;
        ev.preventDefault();
        closePurchasePreview();
    };

    render(){
        const {
            csrf, categories, formIsSending, serverErrors, formSendingError, defaultValues,
            formIsInited, formIsIniting, purchasePreview, uploadedFiles, formType, formConfirming
        } = this.props;
        const { isInited } = this.state;

        if (
            !csrf.isLoading && !csrf.isLoaded
            || !categories.isLoading && !categories.isLoaded
            || !formIsIniting && !formIsInited
        ) {
            return (
                <div>При инициализации формы произошла ошибка.</div>
            );
        }

        if (!isInited) {
            return(
                <div className="form-preload">
                    <LoaderSpinner />
                </div>
            );
        }

        return  (
            <div className={formIsSending ? 'form-sending-loader' : ''}>
                <FormPurchase
                    categories={categories.entities}
                    submitHandler={this.sendForm}
                    serverErrors={serverErrors}
                    defaultValues={defaultValues}
                    fileUpload={uploadedFiles}
                    formType={formType}
                    form_name={this.form_name}
                />
                {formIsSending && <LoaderSpinner />}
                {formSendingError && <div>При отправке формы произошла ошибка. Пожалуйста попробуйте позже.</div>}
                {
                    purchasePreview.isVisible
                    && <Popup close={this.handleClosePreviewPopup} popupClassName="js-popup-container-advert-check-form" popupContentClassName="popup_size_big">
                        <PurchasePreview
                            name={purchasePreview.data['name']}
                            price={purchasePreview.data['price']}
                            quantity={purchasePreview.data['quantity']}
                            measure={purchasePreview.data['measure']}
                            description={purchasePreview.data['description']}
                            category={{ name: purchasePreview.data['category']['parent_name'] }}
                            sub_category={purchasePreview.data['category']}
                            purchase_url={purchasePreview.data['urls'] && (purchasePreview.data['urls']['single_purchase_public'] || '')}
                            address={purchasePreview.data['address']}
                            files={purchasePreview.data['files']}
                            user={purchasePreview.data['user']}
                            title="Проверьте закупку перед публикацией"
                            not_show_public_link={true}
                        >
                            <div className="buttons">
                                <a href="#" className="button-transparent" onClick={this.handleClosePreviewPopup}>Вернуться к редактированию</a>
                                <button type="submit" className="button-purple" onClick={this.sendConfirmation}>
                                    {formType === 'create' ? 'Опубликовать закупку' : 'Сохранить'}
                                </button>
                            </div>
                        </PurchasePreview>
                        {formConfirming && <LoaderSpinner isAbsolute />}
                    </Popup>
                }
            </div>
        );
    }
}

FormPurchaseContainer.propTypes = {
    formType: PropTypes.string,
    // Redux store data
    formIsInited: PropTypes.bool.isRequired,
    formIsIniting: PropTypes.bool.isRequired,
    formIsSending: PropTypes.bool.isRequired,
    hasDefaultValues: PropTypes.bool.isRequired,
    defaultValues: PropTypes.object.isRequired,
    serverErrors: PropTypes.object.isRequired,
    purchasePreview: PropTypes.object.isRequired,
    formSendingError: PropTypes.bool.isRequired,
    formConfirming: PropTypes.bool.isRequired,
    confirmationErrors: PropTypes.object.isRequired,
    uploadedFiles: PropTypes.array.isRequired,
    categories: PropTypes.shape({
        entities: PropTypes.array.isRequired,
        isLoading: PropTypes.bool.isRequired,
    }).isRequired,
    csrf: PropTypes.shape({
        value: PropTypes.string.isRequired,
        isLoading: PropTypes.bool.isRequired,
        isFailed: PropTypes.bool.isRequired,
    }).isRequired,
    // Action Creators
    initForm: PropTypes.func.isRequired,
    sendForm: PropTypes.func.isRequired,
    initCategories: PropTypes.func.isRequired,
    initCSRF: PropTypes.func.isRequired,
    fetchSubcategories: PropTypes.func.isRequired,
    closePurchasePreview: PropTypes.func.isRequired,
    sendConfirmation: PropTypes.func.isRequired,
};

FormPurchaseContainer.defaultProps = {
    formType: 'create',
};

const mapStateToProps = (state) => {
    return {
        ...state.formPurchase,
        categories: {...state.config.categories},
        csrf: {...state.config.csrf},
        serverErrors: {...state.formPurchase.serverErrors}
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        initCategories: bindActionCreators(loadCategories, dispatch),
        initCSRF: bindActionCreators(loadCSRF, dispatch),
        fetchSubcategories: bindActionCreators(fetchSubcategories, dispatch),
        sendForm: bindActionCreators(sendFormPurchase, dispatch),
        sendConfirmation: bindActionCreators(sendConfirmationPurchaseForm, dispatch),
        initForm: bindActionCreators(initFormPurchase, dispatch),
        closePurchasePreview: bindActionCreators(closePurchasePreview, dispatch),
    };
};

export default withProvider(connect(mapStateToProps, mapDispatchToProps)(FormPurchaseContainer));
