import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import LoaderSpinner from '../components/LoaderSpinner';
import TableProducts from '../tables/TableProducts';
import withProvider from '../hocs/withProvider';
import {initTableProducts, getPaginatorData, showMoreProducts, getProductsItemsBySearch} from '../actions';
import {setUrlWithQueryParams} from '../Helpers';


class TableProductsContainer extends Component {

    componentDidMount = () => {
        const { tableInitError, tableIsInited, tableIsInitialising, initTable, is_landing } = this.props;
        if (!tableInitError && !tableIsInited && !tableIsInitialising) {
            initTable(is_landing);
        }
    };

    getPaginator = () => {
        const { getPaginatorData, is_landing } = this.props;
        getPaginatorData(is_landing);
    };

    getPaginatorCurrentPage = () => {
        const { paginator } = this.props;
        return paginator.current_page ? paginator.current_page : 1;
    };

    showProducts = () => {
        const { showMoreProducts } = this.props;
        showMoreProducts(this.setUrl());
    };

    setUrl = () => {
        const { is_landing } = this.props;
        const url = window.location.search;
        let query_param = setUrlWithQueryParams(url, String(this.getPaginatorCurrentPage() + 1));

        if (is_landing) {
            return '/product/rossiia' + query_param;
        }
        return window.location.pathname + query_param;
    };

    isPaginatorEnd = () => {
        const { paginator: { current_page, total_item_count, item_count_per_page } } = this.props;
        return current_page * item_count_per_page <= total_item_count;
    };

    render() {
        const { tableIsInited, tableIsInitialising, products, view_mode, breadcrumbs, is_loading_show_more, is_landing, isLoading } = this.props;

        if (tableIsInitialising) {
            return (
                <div className="form-preload">
                    <LoaderSpinner />
                </div>
            );
        }

        if (tableIsInited) {
            return (
                <div>
                    <TableProducts
                        products={products}
                        view_mode={view_mode}
                        breadcrumbs={breadcrumbs}
                        showMore={is_loading_show_more}
                        showButton={this.isPaginatorEnd()}
                        getPaginator={this.getPaginator}
                        showProducts={this.showProducts}
                        is_landing={is_landing}
                        isLoading={isLoading}
                    />
                    {tableIsInitialising && <LoaderSpinner />}
                </div>
            );
        }

        return null;
    }
}

TableProductsContainer.propTypes = {
    // Redux store data
    tableIsInited: PropTypes.bool.isRequired,
    tableIsInitialising: PropTypes.bool.isRequired,
    products: PropTypes.array,
    breadcrumbs: PropTypes.array,
    view_mode: PropTypes.string,
    paginator: PropTypes.object,
    is_loading_show_more: PropTypes.bool.isRequired,
    tableInitError: PropTypes.bool.isRequired,
    // Action Creators
    initTable: PropTypes.func.isRequired,
    getPaginatorData: PropTypes.func.isRequired,
    showMoreProducts: PropTypes.func.isRequired,
    is_landing: PropTypes.bool,
};

TableProductsContainer.defaultProps = {
    products: [],
    breadcrumbs: [],
    view_mode: '',
    paginator: {},
    is_landing: false
};

const mapStateToProps = (state) => {
    return {
        ...state.tableProducts
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        initTable: bindActionCreators(initTableProducts, dispatch),
        getPaginatorData: bindActionCreators(getPaginatorData, dispatch),
        showMoreProducts: bindActionCreators(showMoreProducts, dispatch),
        getUserProductsBySearch: bindActionCreators(getProductsItemsBySearch, dispatch),
    };
};

export default withProvider(connect(mapStateToProps, mapDispatchToProps)(TableProductsContainer));
