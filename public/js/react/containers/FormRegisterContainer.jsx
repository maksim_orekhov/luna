import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import LoaderSpinner from '../components/LoaderSpinner';
import FormRegister from '../forms/FormRegister';
import withProvider from '../hocs/withProvider';
import { loadCSRF, sendFormRegister } from '../actions';


class FormRegisterContainer extends Component {

    state = {
        isInited: false
    };

    componentDidMount() {
        const { loadCSRF, csrf } = this.props;
        if (!csrf.isLoaded) {
            loadCSRF();
        }
        this.initForm();
    }

    componentDidUpdate () {
        this.initForm();
    }

    sendFormRegister = (data) => {
        const { sendForm } = this.props;
        sendForm(JSON.stringify(data));
    };

    initForm = () => {
        const { csrf } = this.props;
        const { isInited } = this.state;
        if (!isInited && csrf.isLoaded) {
            this.setState({ isInited: true });
        }
    };

    handleSubmitForm = (data) => {
        const { csrf, sendForm } = this.props;
        data.csrf = csrf.value;
        sendForm(data);
    };

    render(){
        const { formIsSending, serverErrors, formSendingError, handleNext } = this.props;
        const { isInited } = this.state;

        if (!isInited) {
            return(
                <div className="form-preload">
                    <LoaderSpinner />
                </div>
            );
        }
        return  (
            <div className={formIsSending ? 'form-sending-loader' : ''}>
                <FormRegister submitHandler={this.handleSubmitForm} serverErrors={serverErrors} handleNext={handleNext} />
                {formIsSending && <LoaderSpinner />}
                {formSendingError && <div>При отправке формы произошла ошибка. Пожалуйста попробуйте позже.</div>}
            </div>
        );
    }
}

FormRegisterContainer.propTypes = {
    handleNext: PropTypes.func.isRequired,
    // Redux store data
    csrf: PropTypes.object.isRequired,
    formIsSending: PropTypes.bool.isRequired,
    serverErrors: PropTypes.object.isRequired,
    formSendingError: PropTypes.bool.isRequired,
    // Action Creators
    loadCSRF: PropTypes.func.isRequired,
    sendForm: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
    return {
        ...state.formRegister,
        csrf: state.config.csrf,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        loadCSRF: bindActionCreators(loadCSRF, dispatch),
        sendForm: bindActionCreators(sendFormRegister, dispatch),
    };
};

export default withProvider(connect(mapStateToProps, mapDispatchToProps)(FormRegisterContainer));
