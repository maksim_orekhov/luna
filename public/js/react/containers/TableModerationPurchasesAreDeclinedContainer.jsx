import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import LoaderSpinner from '../components/LoaderSpinner';
import Popup from '../Popup';
import FormPurchaseDeclined from '../forms/FormPurchaseDeclined';
import Paginator from '../components/Paginator';
import TableModerationPurchasesAreDeclined from '../tables/TableModerationPurchasesAreDeclined';
import TableFilterController from '../controllers/TableFilterController';
import withProvider from '../hocs/withProvider';
import { loadCSRF } from '../actions/ConfigActions';
import {initModerationPurchases, getModerationPurhasesItems, getModerationPurchaseItem} from '../actions';
import { MODERATION_PURCHASES_STATUS_NAMES } from '../constants/names';
import {getMessageTextByUserActionName, getUrlParameter, setUrlParameter} from '../Helpers';
import PageMessage from '../components/PageMessage';
import InfoMessage from '../components/InfoMessage';

const { MODERATION_PURCHASES_ARE_DECLINED: CONTAINER_NAME } = MODERATION_PURCHASES_STATUS_NAMES;

class TableModerationPurchasesAreDeclinedContainer extends Component {
    state = {
        isOpenedFormAdvert: false,
        current_moderation_id: 0,
        page: 1,
        per_page: 10
    };

    componentDidMount() {
        const { isInited, isInitialising, init, is_nested } = this.props;

        if (isInited || isInitialising || is_nested) return;

        init && init(CONTAINER_NAME);
    }

    handleTableRowClick = (purchase) => () => {
        const { fetchPurchase } = this.props;
        fetchPurchase(purchase.id, CONTAINER_NAME);
        this.setState({
            isOpenedFormAdvert: true,
            current_moderation_id: purchase.id
        });
    };

    handleClosePopup = () => {
        this.setState({
            isOpenedFormAdvert: false,
        });
    };

    handlePageSwitch = (page) => {
        const { getPurchases } = this.props;
        const { per_page } = this.state;
        window.history.pushState({}, document.title, setUrlParameter(window.location.href, 'page', page));
        this.setState(() => {
            return {
                page: page
            };
        }, () => {
            this.applyParametersFromUrl();
            getPurchases(CONTAINER_NAME, page, per_page);
        });
    };

    applyParametersFromUrl = () => {
        const url_params = window.location.search;

        const url_page_number = url_params.length ? +getUrlParameter(url_params, 'page') || this.state.page : 1;
        const url_per_page = url_params.length ? +getUrlParameter(url_params, 'per_page') || this.state.per_page : 10;

        this.setState({
            page: url_page_number,
            per_page: url_per_page
        });
    };

    handlePerPageChange = (per_page) => {
        const { getPurchases } = this.props;
        const { page } = this.state;
        window.history.pushState({}, document.title, setUrlParameter(window.location.href, 'per_page', per_page));
        this.setState({
            per_page: per_page
        }, () => {
            this.applyParametersFromUrl();
            getPurchases(CONTAINER_NAME, page, per_page);
        });
    };

    getPopUpContent = (moderation, entities, current_moderation_id) => {
        const { errors } = this.props;
        if (!moderation.isPreviewLoading && moderation.isPreviewLoaded) {
            return (
                <FormPurchaseDeclined
                    handleClosePopup={this.handleClosePopup}
                    purchase={entities.find(purchase => +purchase.id === +current_moderation_id)}
                    is_operator={true}
                />
            );
        } else if (moderation.isPreviewLoading && !moderation.isPreviewLoaded) {
            return (
                <LoaderSpinner isAbsolute={true} />
            );
        } else if (errors.code === 'ERROR_NOT_AUTHORIZED') {
            window.location = `/login?redirectUrl=${window.location.pathname}`;
            return (
                <InfoMessage
                    handleClose={this.handleClosePopup}
                    title={getMessageTextByUserActionName('not_auth').title}
                    description={getMessageTextByUserActionName('not_auth').description}
                />
            );
        }
        else {
            return (
                <InfoMessage
                    handleClose={this.handleClosePopup}
                    title={getMessageTextByUserActionName('popup_init_error').title}
                    description={getMessageTextByUserActionName('popup_init_error').description}
                />
            );
        }
    };

    render() {
        const { isOpenedFormAdvert, current_moderation_id, per_page } = this.state;
        const {
            isInited,
            isInitialising,
            errors,
            entities,
            moderation,
            isLoading,
            paginator
        } = this.props;

        if (isInitialising || isLoading) {
            return (
                <div className="form-preload">
                    <LoaderSpinner />
                </div>
            );
        }

        if (isInited && entities.length === 0) {
            return <PageMessage message="здесь будут отображаться непрошедшие модерерацию закупки" />;
        }

        if (isInited) {
            return (
                <div className="row table-container">
                    <TableFilterController />
                    <TableModerationPurchasesAreDeclined
                        purchases={entities}
                        onTableRowClick={this.handleTableRowClick}
                    />
                    <Paginator
                        paginator={paginator}
                        handlePageSwitch={this.handlePageSwitch}
                        per_page={per_page}
                        handlePerPageChange={this.handlePerPageChange}
                    />
                    {
                        isOpenedFormAdvert &&
                        <Popup
                            close={this.handleClosePopup}
                            popupClassName="js-popup-container-advert-moderate-form"
                            popupContentClassName="popup_size_big"
                            is_preview_loaded={moderation.isPreviewLoaded}
                        >
                            {
                                this.getPopUpContent(moderation, entities, current_moderation_id)
                            }
                        </Popup>
                    }
                </div>
            );
        }

        if (errors) {
            return (
                <div>
                    <p className="error">Не удалось получить данные.</p>
                    <p className="error">{errors}</p>
                </div>
            );
        }

        return null;
    }
}

TableModerationPurchasesAreDeclinedContainer.propTypes = {
    is_nested: PropTypes.bool,
    // Redux store data
    isInited: PropTypes.bool.isRequired,
    isInitialising: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    entities: PropTypes.array,
    paginator: PropTypes.object.isRequired,
    errors: PropTypes.object,
    // Action Creators
    init: PropTypes.func.isRequired,
    getPurchases: PropTypes.func.isRequired,
    moderation: PropTypes.object.isRequired,
    fetchPurchase: PropTypes.func.isRequired
};

TableModerationPurchasesAreDeclinedContainer.defaultProps = {
    is_nested: false,
    entities: [],
    errors: {}
};


const mapStateToProps = (state) => {
    const moderation_state =  {...state.moderationPurchases[CONTAINER_NAME]};

    return {
        CSRF: {...state.config.csrf},
        isInited: moderation_state.isInited,
        isInitialising: moderation_state.isInitialising,
        isLoading: moderation_state.isLoading,
        entities: moderation_state.entities,
        paginator: moderation_state.paginator,
        errors: moderation_state.errors,
        moderation: moderation_state
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        loadCSRF: bindActionCreators(loadCSRF, dispatch),
        getPurchases: bindActionCreators(getModerationPurhasesItems, dispatch),
        init: bindActionCreators(initModerationPurchases, dispatch),
        fetchPurchase: bindActionCreators(getModerationPurchaseItem, dispatch)
    };
};


export default withProvider(connect(mapStateToProps, mapDispatchToProps)(TableModerationPurchasesAreDeclinedContainer));
