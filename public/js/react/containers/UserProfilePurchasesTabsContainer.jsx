import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Tabs, { Tab } from '../components/Tabs';
import withProvider from '../hocs/withProvider';
import LoaderSpinner from '../components/LoaderSpinner';
import TableUserPurchasesAreActiveContainer from './TableUserPurchasesAreActiveContainer';
import TableUserPurchasesAreInactiveContainer from './TableUserPurchasesAreInactiveContainer';
import TableUserPurchasesArchiveContainer from './TableUserPurchasesArchiveContainer';
import { loadCSRF, initUserProfilePurchasesTabs } from '../actions';


class UserPurchasesTabsContainer extends Component {
    componentDidMount() {
        const { isInitialising, init } = this.props;

        if (isInitialising) return;

        init && init();
    }

    render(){
        const { CSRF, moderation } = this.props;
        if (CSRF.isLoading) {
            return <LoaderSpinner />;
        }

        return(
            <div className="profile-table-wrapper">
                <Tabs tabsListClassName="row tab" tabsWrapperClassName="content-header">
                    <Tab tabName="Активные" tabClassName="tab__title col-2 col-tablet-3">
                        <TableUserPurchasesAreActiveContainer
                            is_nested={true}
                        />
                    </Tab>
                    <Tab tabName="Неактивные" tabClassName="tab__title col-2 col-tablet-3">
                        <TableUserPurchasesAreInactiveContainer
                            is_nested={true}
                        />
                    </Tab>
                    <Tab tabName="Архив" tabClassName="tab__title col-2 col-tablet-3">
                        <TableUserPurchasesArchiveContainer
                            is_nested={true}
                        />
                    </Tab>
                </Tabs>
                {moderation.isLoading && <LoaderSpinner isAbsolute />}
            </div>
        );
    }
}

UserPurchasesTabsContainer.propTypes = {
    // redux store data
    CSRF: PropTypes.object.isRequired,
    moderation: PropTypes.object.isRequired,
    // action creators
    loadCSRF: PropTypes.func.isRequired,
    init: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
    return {
        CSRF: {...state.config.csrf},
        moderation: {...state.moderationPurchases},
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        loadCSRF: bindActionCreators(loadCSRF, dispatch),
        init: bindActionCreators(initUserProfilePurchasesTabs, dispatch)
    };
};

export default withProvider(connect(mapStateToProps, mapDispatchToProps)(UserPurchasesTabsContainer));
