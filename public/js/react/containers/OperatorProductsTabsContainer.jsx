import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Tabs, { Tab } from '../components/Tabs';
import withProvider from '../hocs/withProvider';
import LoaderSpinner from '../components/LoaderSpinner';
import TableModerationProductsAreWaiting from './TableModerationProductsAreWaitingContrainer';
import TableModerationProductsAreDeclinedContainer from './TableModerationProductsAreDeclinedContainer';
import TableModerationProductsArchiveContainer from './TableModerationProductsArchiveContainer';
import TableModerationProductsArePublishedContainer from './TableModerationProductsArePublishedContainer';
import { loadCSRF, initOperatorProductsTabs } from '../actions';


class OperatorProductsTabsContainer extends Component {
    componentDidMount() {
        const { isInitialising, init } = this.props;

        if (isInitialising) return;

        init && init();
    }

    render(){
        const { CSRF, moderation } = this.props;

        if (CSRF.isLoading) {
            return <LoaderSpinner />;
        }

        return(
            <div className="profile-table-wrapper">
                <Tabs tabsListClassName="row row_full-width tab" tabsWrapperClassName="content-header">
                    <Tab tabName="На модерации" tabClassName="tab__title col-2 col-tablet-3">
                        <TableModerationProductsAreWaiting
                            is_nested={true}
                        />
                    </Tab>
                    <Tab tabName="Опубликованные" tabClassName="tab__title col-2 col-tablet-3">
                        <TableModerationProductsArePublishedContainer
                            is_nested={true}
                        />
                    </Tab>
                    <Tab tabName="Отклоненные" tabClassName="tab__title col-2 col-tablet-3">
                        <TableModerationProductsAreDeclinedContainer
                            is_nested={true}
                        />
                    </Tab>
                    <Tab tabName="Архив" tabClassName="tab__title col-2 col-tablet-3">
                        <TableModerationProductsArchiveContainer
                            is_nested={true}
                        />
                    </Tab>
                </Tabs>
                {moderation.isLoading && <LoaderSpinner isAbsolute />}
            </div>
        );
    }
}

OperatorProductsTabsContainer.propTypes = {
    // redux store data
    CSRF: PropTypes.object.isRequired,
    moderation: PropTypes.object.isRequired,
    // action creators
    loadCSRF: PropTypes.func.isRequired,
    init: PropTypes.func.isRequired
};

const mapStateToProps = (state) => {
    return {
        CSRF: {...state.config.csrf},
        moderation: {...state.moderationProducts},
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        loadCSRF: bindActionCreators(loadCSRF, dispatch),
        init: bindActionCreators(initOperatorProductsTabs, dispatch)
    };
};

export default withProvider(connect(mapStateToProps, mapDispatchToProps)(OperatorProductsTabsContainer));
