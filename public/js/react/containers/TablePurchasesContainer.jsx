import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import LoaderSpinner from '../components/LoaderSpinner';
import TablePurchases from '../tables/TablePurchases';
import withProvider from '../hocs/withProvider';
import { initTablePurchases, getPurchasesPaginatorData, showMorePurchases } from '../actions';
import {setUrlWithQueryParams} from '../Helpers';


class TablePurchasesContainer extends Component {

    componentDidMount = () => {
        const { tableInitError, tableIsInited, tableIsInitialising, initTable } = this.props;
        if (!tableInitError && !tableIsInited && !tableIsInitialising) {
            initTable();
        }
    };

    getPaginator = () => {
        const { getPurchasesPaginatorData } = this.props;
        getPurchasesPaginatorData();
    };

    getPaginatorCurrentPage = () => {
        const { paginator } = this.props;
        return paginator.current_page ? paginator.current_page : 1;
    };

    showPurchases = () => {
        const { showMorePurchases } = this.props;
        showMorePurchases(this.setUrl());
    };

    setUrl = () => {
        const url = window.location.search;
        let query_param = setUrlWithQueryParams(url, String(this.getPaginatorCurrentPage() + 1));
        return window.location.pathname + query_param;
    };

    isPaginatorEnd = () => {
        const { paginator: { current_page, total_item_count, item_count_per_page } } = this.props;
        return current_page * item_count_per_page <= total_item_count;
    };

    render() {
        const { tableIsInited, tableIsInitialising, purchases, breadcrumbs, is_loading_show_more, isLoading } = this.props;

        if (tableIsInitialising) {
            return (
                <div className="form-preload">
                    <LoaderSpinner />
                </div>
            );
        }

        if (tableIsInited) {
            return (
                <div>
                    <TablePurchases
                        purchases={purchases}
                        breadcrumbs={breadcrumbs}
                        showMore={is_loading_show_more}
                        showButton={this.isPaginatorEnd()}
                        getPaginator={this.getPaginator}
                        showPurchases={this.showPurchases}
                        isLoading={isLoading}
                    />
                    {tableIsInitialising && <LoaderSpinner />}
                </div>
            );
        }

        return null;
    }
}

TablePurchasesContainer.propTypes = {
    // Redux store data
    tableIsInited: PropTypes.bool.isRequired,
    tableIsInitialising: PropTypes.bool.isRequired,
    purchases: PropTypes.array,
    breadcrumbs: PropTypes.array,
    paginator: PropTypes.object,
    is_loading_show_more: PropTypes.bool.isRequired,
    tableInitError: PropTypes.bool.isRequired,
    // Action Creators
    initTable: PropTypes.func.isRequired,
    getPurchasesPaginatorData: PropTypes.func.isRequired,
    showMorePurchases: PropTypes.func.isRequired,
};

TablePurchasesContainer.defaultProps = {
    purchases: [],
    breadcrumbs: [],
    paginator: {},
};

const mapStateToProps = (state) => {
    return {
        ...state.tablePurchases
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        initTable: bindActionCreators(initTablePurchases, dispatch),
        getPurchasesPaginatorData: bindActionCreators(getPurchasesPaginatorData, dispatch),
        showMorePurchases: bindActionCreators(showMorePurchases, dispatch),
    };
};

export default withProvider(connect(mapStateToProps, mapDispatchToProps)(TablePurchasesContainer));
