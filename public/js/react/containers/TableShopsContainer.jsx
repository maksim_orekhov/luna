import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import LoaderSpinner from '../components/LoaderSpinner';
import TableShops from '../tables/TableShops';
import withProvider from '../hocs/withProvider';
import { initTableShops, getShopsPaginatorData, showMoreShops } from '../actions';
import {setUrlWithQueryParams} from '../Helpers';


class TableShopsContainer extends Component {

    componentDidMount = () => {
        const { tableInitError, tableIsInited, tableIsInitialising, initTable } = this.props;
        if (!tableInitError && !tableIsInited && !tableIsInitialising) {
            initTable();
        }
    };

    getPaginator = () => {
        const { getShopsPaginatorData } = this.props;
        getShopsPaginatorData();
    };

    getPaginatorCurrentPage = () => {
        const { paginator } = this.props;
        return paginator.current_page ? paginator.current_page : 1;
    };

    showShops = () => {
        const { showMoreShops } = this.props;
        showMoreShops(this.setUrl());
    };

    setUrl = () => {
        const url = window.location.search;
        let query_param = setUrlWithQueryParams(url, String(this.getPaginatorCurrentPage() + 1));
        return window.location.pathname + query_param;
    };

    isPaginatorEnd = () => {
        const { paginator: { current_page, total_item_count, item_count_per_page } } = this.props;
        return current_page * item_count_per_page <= total_item_count;
    };

    render() {
        const { tableIsInited, tableIsInitialising, shops, is_loading_show_more, isLoading } = this.props;

        if (tableIsInitialising) {
            return (
                <div className="form-preload">
                    <LoaderSpinner />
                </div>
            );
        }

        if (tableIsInited) {
            return (
                <div>
                    <TableShops
                        shops={shops}
                        showMore={is_loading_show_more}
                        showButton={this.isPaginatorEnd()}
                        getPaginator={this.getPaginator}
                        showShops={this.showShops}
                        isLoading={isLoading}
                    />
                    {tableIsInitialising && <LoaderSpinner />}
                </div>
            );
        }

        return null;
    }
}

TableShopsContainer.propTypes = {
    // Redux store data
    tableIsInited: PropTypes.bool.isRequired,
    tableIsInitialising: PropTypes.bool.isRequired,
    shops: PropTypes.array,
    paginator: PropTypes.object,
    is_loading_show_more: PropTypes.bool.isRequired,
    tableInitError: PropTypes.bool.isRequired,
    // Action Creators
    initTable: PropTypes.func.isRequired,
    getShopsPaginatorData: PropTypes.func.isRequired,
    showMoreShops: PropTypes.func.isRequired,
};

TableShopsContainer.defaultProps = {
    shops: [],
    paginator: {},
};

const mapStateToProps = (state) => {
    return {
        ...state.tableShops
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        initTable: bindActionCreators(initTableShops, dispatch),
        getShopsPaginatorData: bindActionCreators(getShopsPaginatorData, dispatch),
        showMoreShops: bindActionCreators(showMoreShops, dispatch),
    };
};

export default withProvider(connect(mapStateToProps, mapDispatchToProps)(TableShopsContainer));
