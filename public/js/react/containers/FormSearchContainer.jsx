import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {loadCategories, fetchSubcategories, getProductsItemsBySearch, getPurchasesItemsBySearch} from '../actions';
import withProvider from '../hocs/withProvider';
import LoaderSpinner from '../components/LoaderSpinner';
import FormSearch from '../forms/FormSearch';
import { getInlineJSON, getSubcategoriesByParentCategoryId, setUrlParameter } from '../Helpers';

class FormSearchContainer extends Component{
    constructor (props) {
        super(props);
        const initialData = getInlineJSON('init-search-json');

        this.state = {
            initialData,
            isInited: false,
            is_search_only_fabricator: initialData.catalog_name === 'product',
            search_value: ''
        };
    }

    componentDidMount () {
        const { categories, loadCategories } = this.props;
        if (!categories.isLoaded && !categories.isLoading) {
            loadCategories();
            this.fetchInitialSubCategories();
        }
        this.initForm();
    }

    componentDidUpdate () {
        const { isInited } = this.state;
        if (!isInited) {
            this.fetchInitialSubCategories();
            this.initForm();
        }
    }

    initForm = () => {
        const { categories } = this.props;
        const { isInited, initialData } = this.state;
        if (
            !isInited
            && categories.isLoaded
            && (
                initialData.category === null
                || !categories.entities.find(category => category.id === initialData.category)
                || getSubcategoriesByParentCategoryId(initialData.category, categories.entities).length > 0
            )
        ) {
            this.setState({ isInited: true });
        }
    };

    fetchInitialSubCategories = () => {
        const { initialData } = this.state;
        const { fetchSubcategories, categories } = this.props;
        if (
            categories.isLoaded
            && initialData.category
            && !categories.isFetching
            && categories.entities.find(category => category.id === initialData.category)
            && getSubcategoriesByParentCategoryId(initialData.category, categories.entities).length === 0
        ) {
            fetchSubcategories(initialData.category);
        }
    };

    submitHandler = (data) => {
        const { categoryId, subCategoryId, query, only_fabricator } = data;
        const { categories } = this.props;
        const { initialData } = this.state;

        let url = initialData.catalog_root_url;
        let category;
        if (categoryId) {
            category = categories.entities.find((category) => category.id === categoryId);
            url += '/' + category.slug;
        }
        if (subCategoryId) {
            url += '/' + category.subcategories.find((category) => category.id === subCategoryId).slug;
        }
        if (query.length) {
            url = setUrlParameter(url, 'q', query);
        }
        if (only_fabricator) {
            url = setUrlParameter(url, 'fabricator', only_fabricator);
        }

        window.location = url;
    };

    performSearchRequest = () => {
        const { search_value } = this.state;

        // if (!search_value || search_value.length < 3) return;

        const { getProductsBySearch, getPurchasesBySearch, categories } = this.props;
        const products_url = this.state.initialData.catalog_root_url;
        const { category, sub_category, fabricator } = this.state.initialData;
        const category_id = category;
        let url = products_url;
        const current_category = categories.entities.find(category => category.id === category_id);
        let current_category_url = '';

        if (current_category) {
            current_category_url = current_category.url;
        }

        if (sub_category) {
            const sub_category_id = sub_category;
            let current_sub_category = current_category.subcategories.find(sub_category => sub_category.id === sub_category_id);

            let current_sub_category_url = '';

            if (current_sub_category) {
                current_sub_category_url = current_sub_category.url;
            }

            url += current_sub_category_url;
        } else if (category) {
            url += current_category_url;
        }

        url += `?q=${search_value}`;

        if (fabricator) {
            url += '&fabricator=true';
        }

        const catalog_name = this.state.initialData.catalog_name;

        if (catalog_name === 'product') {
            getProductsBySearch(url, search_value === '');
        } else if (catalog_name === 'purchase') {
            getPurchasesBySearch(url);
        }
    };

    handleChangeSearch = (e) => {
        const search_value = e.target.value;
        clearTimeout(this.timeout);

        this.setState({
            search_value
        }, () => {
            this.timeout = setTimeout(() => {
                this.performSearchRequest();
            }, 400);
        });
    };

    redirect = (url) => {
        window.location = url;
    };

    render () {
        const { isInited, initialData, is_search_only_fabricator, search_value } = this.state;
        const { categories, fetchSubcategories,  } = this.props;

        // if (!isInited) {
        //     return <LoaderSpinner isSmall />;
        // }

        return <FormSearch
            catalogName={initialData.catalog_name}
            categories={categories}
            fetchSubcategories={fetchSubcategories}
            initialData={initialData}
            searchURL={initialData.catalog_search_url}
            sendForm={this.submitHandler}
            redirectHandler={this.redirect}
            is_search_only_fabricator={is_search_only_fabricator}
            search_value={search_value}
            handleChangeSearchValue={this.handleChangeSearch}
        />;
    }
}

FormSearchContainer.propTypes = {
    categories: PropTypes.object.isRequired,
    loadCategories: PropTypes.func.isRequired,
    fetchSubcategories: PropTypes.func.isRequired,
    getProductsBySearch: PropTypes.func.isRequired,
    getPurchasesBySearch: PropTypes.func.isRequired
};


const mapStateToProps = (state) => {
    return {
        categories: state.config.categories
    };
};


const mapDispatchToProps = (dispatch) => {
    return {
        loadCategories: bindActionCreators(loadCategories, dispatch),
        fetchSubcategories: bindActionCreators(fetchSubcategories, dispatch),
        getProductsBySearch: bindActionCreators(getProductsItemsBySearch, dispatch),
        getPurchasesBySearch: bindActionCreators(getPurchasesItemsBySearch, dispatch),
    };
};

export default withProvider(connect(mapStateToProps, mapDispatchToProps)(FormSearchContainer));
