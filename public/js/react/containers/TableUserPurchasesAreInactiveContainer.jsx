import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import LoaderSpinner from '../components/LoaderSpinner';
import Popup from '../Popup';
import FormPurchaseDeclined from '../forms/FormPurchaseDeclined';
import FormPurchaseModerationPending from '../forms/FormPurchaseModerationPending';
import FormPurchasePaused from '../forms/FormPurchasePaused';
import TableUserPurchasesAreInactive from '../tables/TableUserPurchasesAreInactive';
import withProvider from '../hocs/withProvider';
import { loadCSRF } from '../actions/ConfigActions';
import { initUserPurchases, getUserPurchasesItems, getUserPurchaseItem, getMoreUserPurchasesItems } from '../actions';
import { USER_PURCHASES_STATUS_NAMES } from '../constants/names';
import {setUrlWithQueryParams, checkShowButtonLoadMoreItems, getMessageTextByUserActionName} from '../Helpers';
import PageMessage from '../components/PageMessage';
import InfoMessage from '../components/InfoMessage';
const { USER_PURCHASES_ARE_INACTIVE: CONTAINER_NAME } = USER_PURCHASES_STATUS_NAMES;
import URLS from '../constants/urls';


class TableUserPurchasesAreInactiveContainer extends Component {
    state = {
        isOpenedFormAdvert: false,
        current_moderation_id: 0
    };

    componentDidMount() {
        const { isInited, isInitialising, init, is_nested } = this.props;

        if (isInited || isInitialising || is_nested) return;

        init && init(CONTAINER_NAME);
    }

    handleTableRowClick = (purchase) => {
        const { fetchPurchase } = this.props;
        fetchPurchase(purchase.id, CONTAINER_NAME);
        this.setState({
            isOpenedFormAdvert: true,
            current_moderation_id: purchase.id
        });
    };

    handleClosePopup = () => {
        this.setState({
            isOpenedFormAdvert: false,
        });
    };

    showPurchases = (e) => {
        e.preventDefault();
        const { getMoreUserPurchasesItems, paginator, collection_url } = this.props;
        const url = `${collection_url}?page=${paginator.current + 1}&per_page=${paginator.item_count_per_page}`;
        getMoreUserPurchasesItems(CONTAINER_NAME, url);
    };

    setUrl = () => {
        const { page } = this.state;
        const url = window.location.search;
        let query_param = setUrlWithQueryParams(url, String(page));
        return window.location.pathname + query_param;
    };

    getPopUpContent = (purchases, entities, current_moderation_id) => {
        const { errors } = this.props;
        if (!purchases.isPreviewLoading && purchases.isPreviewLoaded) {
            return (
                this.renderPopUpByStatus(entities.find(purchase => +purchase.id === +current_moderation_id))
            );
        } else if (purchases.isPreviewLoading && !purchases.isPreviewLoaded) {
            return (
                <LoaderSpinner isAbsolute={true} />
            );
        } else if (errors.code === 'ERROR_NOT_AUTHORIZED') {
            window.location = `/login?redirectUrl=${window.location.pathname}`;
            return (
                <InfoMessage
                    handleClose={this.handleClosePopup}
                    title={getMessageTextByUserActionName('not_auth').title}
                    description={getMessageTextByUserActionName('not_auth').description}
                />
            );
        }
        else {
            return (
                <InfoMessage
                    handleClose={this.handleClosePopup}
                    title={getMessageTextByUserActionName('popup_init_error').title}
                    description={getMessageTextByUserActionName('popup_init_error').description}
                />
            );
        }
    };

    renderPopUpByStatus = (purchase) => {
        let status = null;
        if (purchase && purchase.hasOwnProperty('status') && purchase.status && purchase.status.hasOwnProperty('status')) {
            status = purchase.status.status;
        }
        switch (status) {
            case 'declined':
                return (
                    <FormPurchaseDeclined
                        handleClosePopup={this.handleClosePopup}
                        purchase={purchase}
                    />
                );
            case 'waiting_moderation':
                return (
                    <FormPurchaseModerationPending
                        handleClosePopup={this.handleClosePopup}
                        purchase={purchase}
                    />
                );
            case 'publish_stopped':
                return (
                    <FormPurchasePaused
                        handleClosePopup={this.handleClosePopup}
                        purchase={purchase}
                    />
                );
            default:
                return (
                    <div style={{color: 'red', background: '#fff', padding: '50px'}}>ошибка получения статуса</div>
                );
        }
    };

    render() {
        const { isOpenedFormAdvert, current_moderation_id } = this.state;
        const {
            isInited,
            isInitialising,
            errors,
            entities,
            purchases,
            isLoading,
            paginator
        } = this.props;

        if (isInitialising) {
            return (
                <div className="form-preload">
                    <LoaderSpinner />
                </div>
            );
        }

        if (isInited && entities.length === 0) {
            return <PageMessage message="здесь будут отображаться закупки на модерации, не&nbsp;прошедшие модерацию, и временно приостановленные" />;
        }

        if (isInited) {
            return (
                <div>
                    <TableUserPurchasesAreInactive
                        purchases={entities}
                        onTableRowClick={this.handleTableRowClick}
                    />
                    {
                        isLoading
                            ?
                            <div className="row button-row">
                                <LoaderSpinner isTiny={true} />
                            </div>
                            : checkShowButtonLoadMoreItems(paginator)
                                ?
                                <div className="row button-row">
                                    <div className="col-12">
                                        <a href="#" className="button-transparent" onClick={this.showPurchases}>Показать больше</a>
                                    </div>
                                </div>
                                : null
                    }
                    {
                        isOpenedFormAdvert &&
                        <Popup
                            close={this.handleClosePopup}
                            popupClassName="js-popup-container-advert-moderate-form"
                            popupContentClassName="popup_size_big"
                            is_preview_loaded={purchases.isPreviewLoaded}
                        >
                            {
                                this.getPopUpContent(purchases, entities, current_moderation_id)
                            }
                        </Popup>
                    }
                </div>
            );
        }

        if (errors) {
            return (
                <div>
                    <p className="error">Не удалось получить данные.</p>
                    <p className="error">{errors}</p>
                </div>
            );
        }

        return null;
    }
}

TableUserPurchasesAreInactiveContainer.propTypes = {
    is_nested: PropTypes.bool,
    // Redux store data
    isInited: PropTypes.bool.isRequired,
    isInitialising: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    entities: PropTypes.array,
    paginator: PropTypes.object,
    errors: PropTypes.object,
    collection_url: PropTypes.string,
    // Action Creators
    init: PropTypes.func.isRequired,
    getPurchases: PropTypes.func.isRequired,
    purchases: PropTypes.object.isRequired,
    fetchPurchase: PropTypes.func.isRequired,
    getMoreUserPurchasesItems: PropTypes.func.isRequired
};

TableUserPurchasesAreInactiveContainer.defaultProps = {
    is_nested: false,
    entities: [],
    errors: {},
    paginator: {},
    collection_url: URLS.PURCHASES.PURCHASES_INACTIVE,
};

const mapStateToProps = (state) => {
    const purchases =  {...state.userPurchases[CONTAINER_NAME]};

    return {
        CSRF: {...state.config.csrf},
        isInited: purchases.isInited,
        isInitialising: purchases.isInitialising,
        isLoading: purchases.isLoading,
        entities: purchases.entities,
        paginator: purchases.paginator,
        errors: purchases.errors,
        collection_url: purchases.collection_url,
        purchases
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        loadCSRF: bindActionCreators(loadCSRF, dispatch),
        getPurchases: bindActionCreators(getUserPurchasesItems, dispatch),
        init: bindActionCreators(initUserPurchases, dispatch),
        fetchPurchase: bindActionCreators(getUserPurchaseItem, dispatch),
        getMoreUserPurchasesItems: bindActionCreators(getMoreUserPurchasesItems, dispatch),
    };
};


export default withProvider(connect(mapStateToProps, mapDispatchToProps)(TableUserPurchasesAreInactiveContainer));
