import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import LoaderSpinner from '../components/LoaderSpinner';
import Popup from '../Popup';
import FormAdvertArchive from '../forms/FormProductArchived';
import TableUserProductsArchive from '../tables/TableUserProductsArchive';
import withProvider from '../hocs/withProvider';
import { loadCSRF } from '../actions/ConfigActions';
import { initUserProducts, getUserProductsItems, getUserProductItem, getMoreUserProductsItems } from '../actions';
import { USER_PRODUCTS_STATUS_NAMES } from '../constants/names';
import {setUrlWithQueryParams, checkShowButtonLoadMoreItems, getMessageTextByUserActionName} from '../Helpers';
import PageMessage from '../components/PageMessage';
import InfoMessage from '../components/InfoMessage';
const { USER_PRODUCTS_ARCHIVE: CONTAINER_NAME } = USER_PRODUCTS_STATUS_NAMES;
import URLS from '../constants/urls';


class TableUserProductsArchiveContainer extends Component {
    state = {
        isOpenedFormAdvert: false,
        current_moderation_id: 0
    };

    componentDidMount() {
        const { isInited, isInitialising, init, is_nested } = this.props;

        if (isInited || isInitialising || is_nested) return;

        init && init(CONTAINER_NAME);
    }

    handleTableRowClick = (product) => () => {
        const { fetchProduct } = this.props;
        fetchProduct(product.id, CONTAINER_NAME);
        this.setState({
            isOpenedFormAdvert: true,
            current_moderation_id: product.id
        });
    };

    handleClosePopup = () => {
        this.setState({
            isOpenedFormAdvert: false,
        });
    };

    showProducts = (e) => {
        e.preventDefault();
        const { show, paginator, getMoreUserProductsItems } = this.props;
        const url = `${URLS.PRODUCTS.PRODUCTS_ARCHIVE}?page=${paginator.current + 1}&per_page=${paginator.item_count_per_page}`;
        getMoreUserProductsItems(CONTAINER_NAME, url);
    };

    setUrl = () => {
        const { page } = this.state;
        const url = window.location.search;
        let query_param = setUrlWithQueryParams(url, String(page));
        return window.location.pathname + query_param;
    };

    getPopUpContent = (products, entities, current_moderation_id) => {
        const { errors } = this.props;
        if (!products.isPreviewLoading && products.isPreviewLoaded) {
            return (
                <FormAdvertArchive
                    handleClosePopup={this.handleClosePopup}
                    product={entities.find(product => +product.id === +current_moderation_id)}
                />
            );
        } else if (products.isPreviewLoading && !products.isPreviewLoaded) {
            return (
                <LoaderSpinner isAbsolute={true} />
            );
        } else if (errors.code === 'ERROR_NOT_AUTHORIZED') {
            window.location = `/login?redirectUrl=${window.location.pathname}`;
            return (
                <InfoMessage
                    handleClose={this.handleClosePopup}
                    title={getMessageTextByUserActionName('not_auth').title}
                    description={getMessageTextByUserActionName('not_auth').description}
                />
            );
        }
        else {
            return (
                <InfoMessage
                    handleClose={this.handleClosePopup}
                    title={getMessageTextByUserActionName('popup_init_error').title}
                    description={getMessageTextByUserActionName('popup_init_error').description}
                />
            );
        }
    };

    render() {
        const { isOpenedFormAdvert, current_moderation_id } = this.state;
        const {
            isInited,
            isInitialising,
            errors,
            entities,
            paginator,
            products,
            isLoading
        } = this.props;

        if (isInitialising) {
            return (
                <div className="form-preload">
                    <LoaderSpinner />
                </div>
            );
        }


        if (isInited && entities.length === 0) {
            return <PageMessage message="здесь будут отображаться удаленные объявления" />;
        }

        if (isInited) {
            return (
                <div>
                    <TableUserProductsArchive
                        products={entities}
                        onTableRowClick={this.handleTableRowClick}
                    />
                    {
                        isLoading
                            ?
                            <div className="row button-row">
                                <LoaderSpinner isTiny={true} />
                            </div>
                            : checkShowButtonLoadMoreItems(paginator)
                                ?
                                <div className="row button-row">
                                    <div className="col-12">
                                        <a href="#" className="button-transparent" onClick={this.showProducts}>Показать больше</a>
                                    </div>
                                </div>
                                : null
                    }
                    {
                        isOpenedFormAdvert &&
                        <Popup close={this.handleClosePopup} popupClassName="js-popup-container-advert-moderate-form" popupContentClassName="popup_size_big" is_preview_loaded={products.isPreviewLoaded}>
                            {
                                this.getPopUpContent(products, entities, current_moderation_id)
                            }
                        </Popup>
                    }
                </div>
            );
        }

        if (errors) {
            return (
                <div>
                    <p className="error">Не удалось получить данные.</p>
                    <p className="error">{errors}</p>
                </div>
            );
        }

        return null;
    }
}

TableUserProductsArchiveContainer.propTypes = {
    is_nested: PropTypes.bool,
    // Redux store data
    isInited: PropTypes.bool.isRequired,
    isInitialising: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    entities: PropTypes.array,
    paginator: PropTypes.object,
    errors: PropTypes.object,
    // Action Creators
    init: PropTypes.func.isRequired,
    getProducts: PropTypes.func.isRequired,
    products: PropTypes.object.isRequired,
    fetchProduct: PropTypes.func.isRequired
};

TableUserProductsArchiveContainer.defaultProps = {
    is_nested: false,
    entities: [],
    errors: {},
    paginator: {}
};

const mapStateToProps = (state) => {
    const products =  {...state.userProducts[CONTAINER_NAME]};
    return {
        CSRF: {...state.config.csrf},
        isInited: products.isInited,
        isInitialising: products.isInitialising,
        isLoading: products.isLoading,
        entities: products.entities,
        paginator: products.paginator,
        errors: products.errors,
        products
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        loadCSRF: bindActionCreators(loadCSRF, dispatch),
        getProducts: bindActionCreators(getUserProductsItems, dispatch),
        init: bindActionCreators(initUserProducts, dispatch),
        fetchProduct: bindActionCreators(getUserProductItem, dispatch),
        getMoreUserProductsItems: bindActionCreators(getMoreUserProductsItems, dispatch),
    };
};


export default withProvider(connect(mapStateToProps, mapDispatchToProps)(TableUserProductsArchiveContainer));
