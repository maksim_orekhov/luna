import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Tabs, { Tab } from '../components/Tabs';
import withProvider from '../hocs/withProvider';
import LoaderSpinner from '../components/LoaderSpinner';
import TableModerationPurchasesAreWaitingContainer from './TableModerationPurchasesAreWaitingContainer';
import TableModerationPurchasesAreDeclinedContainer from './TableModerationPurchasesAreDeclinedContainer';
import TableModerationPurchasesArchiveContainer from './TableModerationPurchasesArchiveContainer';
import TableModerationPurchasesArePublishedContainer from './TableModerationPurchasesArePublishedContainer';
import { loadCSRF, initOperatorPurchasesTabs } from '../actions';


class OperatorPurchasesTabsContainer extends Component {
    componentDidMount() {
        const { isInitialising, init } = this.props;

        if (isInitialising) return;

        init && init();
    }

    render(){
        const { CSRF, moderation } = this.props;

        if (CSRF.isLoading) {
            return <LoaderSpinner />;
        }

        return(
            <div className="profile-table-wrapper">
                <Tabs tabsListClassName="row row_full-width tab" tabsWrapperClassName="content-header">
                    <Tab tabName="На модерации" tabClassName="tab__title col-2 col-tablet-3">
                        <TableModerationPurchasesAreWaitingContainer
                            is_nested={true}
                        />
                    </Tab>
                    <Tab tabName="Опубликованные" tabClassName="tab__title col-2 col-tablet-3">
                        <TableModerationPurchasesArePublishedContainer
                            is_nested={true}
                        />
                    </Tab>
                    <Tab tabName="Отклоненные" tabClassName="tab__title col-2 col-tablet-3">
                        <TableModerationPurchasesAreDeclinedContainer
                            is_nested={true}
                        />
                    </Tab>
                    <Tab tabName="Архив" tabClassName="tab__title col-2 col-tablet-3">
                        <TableModerationPurchasesArchiveContainer
                            is_nested={true}
                        />
                    </Tab>
                </Tabs>
                {moderation.isLoading && <LoaderSpinner isAbsolute />}
            </div>
        );
    }
}

OperatorPurchasesTabsContainer.propTypes = {
    // redux store data
    CSRF: PropTypes.object.isRequired,
    moderation: PropTypes.object.isRequired,
    // action creators
    loadCSRF: PropTypes.func.isRequired,
    init: PropTypes.func.isRequired
};

const mapStateToProps = (state) => {
    return {
        CSRF: {...state.config.csrf},
        moderation: {...state.moderationPurchases},
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        loadCSRF: bindActionCreators(loadCSRF, dispatch),
        init: bindActionCreators(initOperatorPurchasesTabs, dispatch)
    };
};

export default withProvider(connect(mapStateToProps, mapDispatchToProps)(OperatorPurchasesTabsContainer));
