import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import LoaderSpinner from '../components/LoaderSpinner';
import TableUsersProducts from '../tables/TableUsersProducts';
import withProvider from '../hocs/withProvider';
import {initTableUsersProducts, getPaginatorData, getMoreUserProducts, getUsersProductsItemsBySearch} from '../actions';
import {checkShowButtonLoadMoreItems, setUrlWithQueryParams} from '../Helpers';


class TableUsersProductsContainer extends Component {
    state = {
        search_value: ''
    };

    componentDidMount = () => {
        const { tableInitError, tableIsInited, tableIsInitialising, initTable } = this.props;
        if (!tableInitError && !tableIsInited && !tableIsInitialising) {
            initTable();
        }
    };

    showProducts = (e) => {
        e.preventDefault();

        const { search_value } = this.state;
        const { getMoreUserProducts, paginator, products_url } = this.props;
        const url = `${products_url}?page=${paginator.current + 1}&q=${search_value}`;
        getMoreUserProducts(url);
    };

    performSearchRequest = () => {
        const { search_value } = this.state;

        // if (!search_value || search_value.length < 3) return;

        const { getUserProductsBySearch, products_url } = this.props;
        const url = `${products_url}?q=${search_value}`;

        getUserProductsBySearch(url);
    };

    handleChangeSearch = (e) => {
        const search_value = e.target.value;
        clearTimeout(this.timeout);

        this.setState({
            search_value
        }, () => {
            this.timeout = setTimeout(() => {
                this.performSearchRequest();
            }, 400);
        });
    };

    handleFindButtonClick = () => {
        this.performSearchRequest();
    };

    handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            document.activeElement.blur && document.activeElement.blur();
        }
    };

    setUrl = () => {
        const { page } = this.state;
        const url = window.location.search;
        let query_param = setUrlWithQueryParams(url, String(page));
        return window.location.pathname + query_param;
    };

    render() {
        const { tableIsInited, tableIsInitialising, view_mode, isLoading, paginator, purchases_url, products_url } = this.props;
        const products = this.props.products || [];

        if (tableIsInitialising) {
            return (
                <div className="form-preload">
                    <LoaderSpinner />
                </div>
            );
        }

        if (tableIsInited) {
            return (
                <div>
                    <TableUsersProducts
                        purchases_url={purchases_url}
                        products_url={products_url}
                        users_products={true}
                        products={products}
                        view_mode={view_mode}
                        showButton={false}
                        showProducts={this.showProducts}
                        handleChangeSearch={this.handleChangeSearch}
                        handleFindButtonClick={this.handleFindButtonClick}
                        handleKeyPress={this.handleKeyPress}
                        isLoading={isLoading}
                    />
                    {
                        isLoading
                            ?
                            null
                            : checkShowButtonLoadMoreItems(paginator)
                                ?
                                <div className="row button-row" style={{margin: '20px auto', textAlign: 'center'}}>
                                    <div className="col-12">
                                        <a href="#" className="button-transparent button-transparent-with-stroke" onClick={this.showProducts}>Показать больше</a>
                                    </div>
                                </div>
                                : null
                    }
                    {tableIsInitialising && <LoaderSpinner />}
                </div>
            );
        }

        return null;
    }
}

TableUsersProductsContainer.propTypes = {
    // Redux store data
    tableIsInited: PropTypes.bool.isRequired,
    tableIsInitialising: PropTypes.bool.isRequired,
    products: PropTypes.array,
    view_mode: PropTypes.string,
    paginator: PropTypes.object,
    user_code: PropTypes.object,
    is_loading_show_more: PropTypes.bool.isRequired,
    tableInitError: PropTypes.bool.isRequired,
    // Action Creators
    initTable: PropTypes.func.isRequired,
    getPaginatorData: PropTypes.func.isRequired,
    getMoreUserProducts: PropTypes.func.isRequired,
    getUserProductsBySearch: PropTypes.func.isRequired,
};

TableUsersProductsContainer.defaultProps = {
    products: [],
    view_mode: '',
    paginator: {},
    user_code: {},
};

const mapStateToProps = (state) => {
    return {
        ...state.tableUsersProducts
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        initTable: bindActionCreators(initTableUsersProducts, dispatch),
        getPaginatorData: bindActionCreators(getPaginatorData, dispatch),
        getMoreUserProducts: bindActionCreators(getMoreUserProducts, dispatch),
        getUserProductsBySearch: bindActionCreators(getUsersProductsItemsBySearch, dispatch),
    };
};

export default withProvider(connect(mapStateToProps, mapDispatchToProps)(TableUsersProductsContainer));
