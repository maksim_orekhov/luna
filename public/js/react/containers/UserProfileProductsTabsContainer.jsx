import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Tabs, { Tab } from '../components/Tabs';
import withProvider from '../hocs/withProvider';
import LoaderSpinner from '../components/LoaderSpinner';
import TableUserProductsAreActiveContainer from './TableUserProductsAreActiveContainer';
import TableUserProductsAreInactiveContainer from './TableUserProductsAreInactiveContainer';
import TableUserProductsArchiveContainer from './TableUserProductsArchiveContainer';
import { loadCSRF, initUserProfileProductsTabs } from '../actions';


class UserProductsTabsContainer extends Component {
    componentDidMount() {
        const { isInitialising, init } = this.props;

        if (isInitialising) return;

        init && init();
    }

    render(){
        const { CSRF, moderation } = this.props;
        if (CSRF.isLoading) {
            return <LoaderSpinner />;
        }

        return(
            <div className="profile-table-wrapper">
                <Tabs tabsListClassName="row tab" tabsWrapperClassName="content-header">
                    <Tab tabName="Активные" tabClassName="tab__title col-2 col-tablet-3">
                        <TableUserProductsAreActiveContainer
                            is_nested={true}
                        />
                    </Tab>
                    <Tab tabName="Неактивные" tabClassName="tab__title col-2 col-tablet-3">
                        <TableUserProductsAreInactiveContainer
                            is_nested={true}
                        />
                    </Tab>
                    <Tab tabName="Архив" tabClassName="tab__title col-2 col-tablet-3">
                        <TableUserProductsArchiveContainer
                            is_nested={true}
                        />
                    </Tab>
                </Tabs>
                {moderation.isLoading && <LoaderSpinner isAbsolute />}
            </div>
        );
    }
}

UserProductsTabsContainer.propTypes = {
    // redux store data
    CSRF: PropTypes.object.isRequired,
    moderation: PropTypes.object.isRequired,
    // action creators
    loadCSRF: PropTypes.func.isRequired,
    init: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
    return {
        CSRF: {...state.config.csrf},
        moderation: {...state.moderationProducts},
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        loadCSRF: bindActionCreators(loadCSRF, dispatch),
        init: bindActionCreators(initUserProfileProductsTabs, dispatch)
    };
};

export default withProvider(connect(mapStateToProps, mapDispatchToProps)(UserProductsTabsContainer));
