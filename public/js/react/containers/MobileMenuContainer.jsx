import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import LoaderSpinner from '../components/LoaderSpinner';
import withProvider from '../hocs/withProvider';
import { initMobileMenu } from '../actions';
import MobileMenu from '../components/MobileMenu';


class MobileMenuContainer extends Component {
    state = {
        is_open_submenu: false,
        current_category: {},
        current_tab: {
            name: '',
            title: '',
            base_url: '',
            create_button_text: '',
            create_url: ''
        } // Раздел сайта (объявления либо закупки);
    };

    componentDidMount() {
        const { init, data } = this.props;

        if (!data.isLoading && !data.isLoaded && !data.isFailed) {
            init();
        }

        const self = this;
        $('body').on('click', '.js-mobile-categories-close', function () {
            $(this).closest('.js-mobile-categories-menu').removeClass('active');
            $('body').removeClass('fixed-noscroll');

            let default_catalog_name = self.props.data.catalog_name;
            self.changeCurrentTab(default_catalog_name);
        });
    }

    componentDidUpdate(prevProps) {
        if (this.props.data.catalog_name && this.props.data.catalog_name !== prevProps.data.catalog_name) {
            this.setCurrentTabData(this.props.data.catalog_name);
        }
    }

    setCurrentTabData(new_tab_name) {
        const { data } = this.props;
        const { product_root_url, purchase_root_url, product_create_url, purchase_create_url } = data;

        const tabs_data = {
            product: {
                name: 'product', // Имя для всяких проверок в коде, например на активность таба
                title: 'Объявления',
                base_url: product_root_url,
                create_button_text: 'Подать объявление',
                create_url: product_create_url
            },
            purchase: {
                name: 'purchase', // Имя для всяких проверок в коде, например на активность таба
                title: 'Закупки',
                base_url: purchase_root_url,
                create_button_text: 'Разместить закупку',
                create_url: purchase_create_url
            }
        };

        if (tabs_data[new_tab_name]) {
            this.setState({
                current_tab: {
                    ...tabs_data[new_tab_name]
                }
            });
        } else {
            console.error('Неизвестный catalog_name для мобильного меню');
        }
    }

    scrollBlockToTop() {
        this.node.closest('.js-mobile-categories-menu').scrollTop = 0;
    }

    openNestedMenu = (current_category = {}) => {
        this.scrollBlockToTop();

        this.setState({
            is_open_submenu: true,
            current_category
        });
    };

    toggleNestedMenu = (trueOrFalse) => {
        this.scrollBlockToTop();

        this.setState({
            is_open_submenu: trueOrFalse
        });
    };

    changeCurrentTab = (new_tab_name) => {
        const { is_open_submenu, current_tab } = this.state;
        if (is_open_submenu) this.toggleNestedMenu(false);

        if (new_tab_name && current_tab.name !== new_tab_name) {
            this.setCurrentTabData(new_tab_name);
        }
    };

    getDefaultMobileHeader() {
        const { current_tab } = this.state;

        return (
            <ul className="mobile_header_nav_link__container">
                <li>
                    <a
                        className={`mobile_header_nav_link ${current_tab.name === 'product' ? 'active' : ''}`}
                        onClick={() => {this.changeCurrentTab('product');}}
                    >
                        Объявления
                    </a>
                </li>
                <li>
                    <a
                        className={`mobile_header_nav_link ${current_tab.name === 'purchase' ? 'active' : ''}`}
                        onClick={() => {this.changeCurrentTab('purchase');}}
                    >
                        Закупки
                    </a>
                </li>
            </ul>
        );
    }

    getNestedMobileHeader() {
        const { current_category, current_tab } = this.state;

        if (current_tab.title) {
            return (
                <div className="mobile_header__text">{`${current_tab.title} / ${current_category.name}`}</div>
            );
        }
        return <div className="mobile_header__text">{current_category.name}</div>;
    }

    getMobileMenuHeader() {
        const { is_open_submenu } = this.state;

        return (
            <div className="header-of-element">
                <div className="row">
                    <div className="col-12 content_col">
                        {
                            is_open_submenu
                                ? <div className="nav_button" onClick={() => this.toggleNestedMenu(false)}>
                                    <div className="button-back" />
                                </div>
                                : <div className="nav_button js-mobile-categories-close">
                                    <div className="button-close" />
                                </div>
                        }
                        <div className="menu-container">
                            <div className="menu">
                                {
                                    is_open_submenu
                                        ? this.getNestedMobileHeader()
                                        : this.getDefaultMobileHeader()
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    getMenu() {
        const { data } = this.props;
        const categories = data.categories || [];
        const { is_open_submenu, current_category, current_tab } = this.state;
        const { base_url, title, create_button_text, create_url } = current_tab;

        return (
            <nav className="row mobile-menu__menu" ref={node => this.node = node}>
                <div className="col-12">
                    {
                        is_open_submenu
                            ? <div style={{width: '100%'}}>
                                <a href={base_url + current_category.url} style={{display: 'block'}} className="mobile-menu__link">
                                    Все {title.toLowerCase()} категории
                                </a>
                                {
                                    current_category &&
                                    current_category.sub_category &&
                                    current_category.sub_category.length !== 0 &&
                                    current_category.sub_category.map((category, index) => {
                                        return (
                                            <a href={base_url + category.url} key={index} style={{display: 'block'}}  className="mobile-menu__link">
                                                {category.name}
                                            </a>
                                        );
                                    })
                                }
                            </div>
                            : <div style={{width: '100%'}}>
                                <div className="button_container">
                                    <a href={create_url} className="button-blue">{create_button_text}</a>
                                </div>
                                <a href={base_url} className="mobile-menu__link">
                                    Все {title.toLowerCase()}
                                </a>
                                {
                                    categories.length !== 0 &&
                                    categories.map((category) => {
                                        return (
                                            <a key={category.slug} onClick={() => this.openNestedMenu(category)} className="mobile-menu__link">
                                                {category.name}
                                            </a>
                                        );
                                    })
                                }
                            </div>
                    }
                </div>
            </nav>
        );
    }

    render() {
        const { data } = this.props;

        const MobileMenuHeader = this.getMobileMenuHeader();
        const Menu = this.getMenu();

        if (!data.isLoading && data.isFailed) {
            return (
                <div>Ошибка инициализации.</div>
            );
        }

        if (data.isLoading) {
            return(
                <div className="form-preload">
                    <LoaderSpinner />
                </div>
            );
        }

        return  (
            <MobileMenu
                Header={MobileMenuHeader}
                Menu={Menu}
            />
        );
    }
}

MobileMenuContainer.propTypes = {
    data: PropTypes.object.isRequired,
    // actions
    init: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
    return {
        data: {...state.config.mobile_menu}
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        init: bindActionCreators(initMobileMenu, dispatch)
    };
};

export default withProvider(connect(mapStateToProps, mapDispatchToProps)(MobileMenuContainer));
