import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import LoaderSpinner from '../components/LoaderSpinner';
import TableUsersPurchases from '../tables/TableUsersPurchases';
import withProvider from '../hocs/withProvider';
import {initTableUsersPurchases, getMoreUserPurchases, getUsersPurchasesItemsBySearch} from '../actions';
import {checkShowButtonLoadMoreItems} from '../Helpers';


class TableUsersPurchasesContainer extends Component {
    state = {
        search_value: ''
    };

    componentDidMount = () => {
        const { tableInitError, tableIsInited, tableIsInitialising, initTable } = this.props;
        if (!tableInitError && !tableIsInited && !tableIsInitialising) {
            initTable();
        }
    };

    showPurchases = (e) => {
        e.preventDefault();
        const { getMoreUserPurchases, paginator, url } = this.props;
        const { search_value = '' } = this.state;
        const new_url = `${url}?page=${paginator.current + 1}&q=${search_value}`;
        getMoreUserPurchases(new_url);
    };

    performSearchRequest = () => {
        const { search_value } = this.state;

        // if (!search_value || search_value.length < 3) return;

        const { getUserPurchasesBySearch } = this.props;
        let purchases_url = this.props.url;
        const url = `${purchases_url}?q=${search_value}`;

        getUserPurchasesBySearch(url);
    };

    handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            document.activeElement.blur && document.activeElement.blur();
        }
    };

    handleChangeSearch = (e) => {
        const search_value = e.target.value;
        clearTimeout(this.timeout);

        this.setState({
            search_value
        }, () => {
            this.timeout = setTimeout(() => {
                this.performSearchRequest();
            }, 400);
        });
    };

    handleFindButtonClick = () => {
        this.performSearchRequest();
    };

    render() {
        const { tableIsInited, tableIsInitialising, isLoading, paginator, products_url, purchases_url } = this.props;
        const purchases = this.props.purchases || [];

        if (tableIsInitialising) {
            return (
                <div className="form-preload">
                    <LoaderSpinner />
                </div>
            );
        }

        if (tableIsInited) {
            return (
                <div>
                    <TableUsersPurchases
                        products_url={products_url}
                        purchases_url={purchases_url}
                        purchases={purchases}
                        showButton={false}
                        handleChangeSearch={this.handleChangeSearch}
                        handleFindButtonClick={this.handleFindButtonClick}
                        handleKeyPress={this.handleKeyPress}
                        isLoading={isLoading}
                    />
                    {
                        isLoading
                            ?
                            null
                            : checkShowButtonLoadMoreItems(paginator)
                                ?
                                <div className="row button-row" style={{margin: '20px auto', textAlign: 'center', maxWidth: '170px'}}>
                                    <div className="col-12">
                                        <a href="#" className="button-transparent button-transparent-with-stroke" onClick={this.showPurchases}>Показать больше</a>
                                    </div>
                                </div>
                                : null
                    }
                    {tableIsInitialising && <LoaderSpinner />}
                </div>
            );
        }

        return null;
    }
}

TableUsersPurchasesContainer.propTypes = {
    // Redux store data
    tableIsInited: PropTypes.bool.isRequired,
    tableIsInitialising: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool,
    purchases: PropTypes.array,
    paginator: PropTypes.object,
    is_loading_show_more: PropTypes.bool.isRequired,
    tableInitError: PropTypes.bool.isRequired,
    // Action Creators
    initTable: PropTypes.func.isRequired,
    getMoreUserPurchases: PropTypes.func.isRequired,
    getUserPurchasesBySearch: PropTypes.func.isRequired,
};

TableUsersPurchasesContainer.defaultProps = {
    purchases: [],
    paginator: {},
    isLoading: false
};

const mapStateToProps = (state) => {
    return {
        ...state.tableUsersPurchases
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        initTable: bindActionCreators(initTableUsersPurchases, dispatch),
        getMoreUserPurchases: bindActionCreators(getMoreUserPurchases, dispatch),
        getUserPurchasesBySearch: bindActionCreators(getUsersPurchasesItemsBySearch, dispatch),
    };
};

export default withProvider(connect(mapStateToProps, mapDispatchToProps)(TableUsersPurchasesContainer));
