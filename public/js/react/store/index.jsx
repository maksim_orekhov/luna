import { createStore } from 'redux';
import AppReducer from '../reducers';
import AppMiddleware from '../middleware';
import { composeWithDevTools } from 'redux-devtools-extension';

const store = createStore(AppReducer, composeWithDevTools(AppMiddleware));
export default store;
