import React from 'react';
import PropTypes from 'prop-types';
import { prettifyPhoneNumber, addSpacesBetweenThousands } from '../Helpers';

function ProductPreview(props) {
    const { price, description, address, galleryFiles, files, name, title, category, sub_category, user, children, product_url, not_show_public_link, show_full_size_photo } = props;

    return(
        <div className="product-preview product-preview-popup">
            <span className="preview-title-container">{ title.length > 0 && <h2 className="page-title">{title}</h2> }</span>

            <div className="product-preview__inner">
                {
                    !not_show_public_link &&
                    <div className="purchase_preview_link_row">
                        <a className="link-inline" href={product_url}>Посмотреть как продавец</a>
                    </div>
                }
                <h2 className="product-name">{name}</h2>
                <div className="product-options-wrapper">
                    <div className="product-options-block">
                        <div className="product-option">
                            <div className="product-option__name">Цена:</div>
                            <div className="product-option__value">{addSpacesBetweenThousands(price)} Руб.</div>
                        </div>
                        <div className="product-option">
                            <div className="product-option__name">Категория:</div>
                            <div className="product-option__value product-option__value_size_big">
                                {category['name']}
                            </div>
                        </div>
                        <div className="product-option">
                            <div className="product-option__name">Подкатегория:</div>
                            <div className="product-option__value product-option__value_size_big">
                                {sub_category['name']}
                            </div>
                        </div>
                        <div className="product-option">
                            <div className="product-option__name">Описание:</div>
                            <div className="product-option__value description product-option__value_size_big">{description}</div>
                        </div>
                        <div className="product-option">
                            <div className="product-option__name">Местоположение:</div>
                            <div className="product-option__value product-option__value_size_big">
                                {
                                    address['city']
                                    + (address['street'] ? ', ' + address['street'] : '')
                                    + (address['house'] ? ', ' + address['house'] : '')
                                }
                            </div>
                        </div>
                        <div className="product-option">
                            <div className="product-option__name">Имя в объявлениях:</div>
                            <div className="product-option__value product-option__value_size_big">{user['name']}</div>
                        </div>
                        <div className="product-option">
                            <div className="product-option__name">Телефон:</div>
                            <div className="product-option__value product-option__value_size_big">{prettifyPhoneNumber(user['phone'])}</div>
                        </div>
                        <div className="product-option">
                            <div className="product-option__name">E-mail:</div>
                            <div className="product-option__value product-option__value_size_big">{user['email']}</div>
                        </div>
                        {
                            galleryFiles && galleryFiles.length !== 0 &&
                                <div className="product-images-previews">
                                    {
                                        show_full_size_photo ?
                                            galleryFiles.map((galleryFile) => {
                                                return <a href={`${galleryFile['path']}`} key={galleryFile['id']} target="_blank"><div className="product-image-preview" style={{backgroundImage: `url(${galleryFile['path']})`}} /></a>;
                                            })
                                            :
                                            galleryFiles.map((galleryFile) => {
                                                return <div className="product-image-preview" style={{backgroundImage: `url(${galleryFile['path']})`}} key={galleryFile['id']} />;
                                            })
                                    }
                                </div>
                        }
                        {
                            files && files.length !== 0 &&
                            <div className="files-list">
                                {files.map((file) => {
                                    return (
                                        <div className="file-item" key={file['id']}>
                                            <a href={file['path']} target="_blank">{file['name']}</a>
                                        </div>
                                    );
                                })}
                            </div>
                        }
                    </div>
                </div>
            </div>
            {children}
        </div>
    );
}

ProductPreview.propTypes = {
    name: PropTypes.string.isRequired,
    price: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]).isRequired,
    description: PropTypes.string.isRequired,
    category: PropTypes.object.isRequired,
    sub_category: PropTypes.object.isRequired,
    address: PropTypes.object.isRequired,
    galleryFiles: PropTypes.array,
    user: PropTypes.object.isRequired,
    files: PropTypes.array,
    onClickBack: PropTypes.func,
    children: PropTypes.any,
    title: PropTypes.string,
    product_url: PropTypes.string,
    not_show_public_link: PropTypes.bool,
    show_full_size_photo: PropTypes.bool
};

ProductPreview.defaultProps = {
    files: [],
    onClickBack: () => {},
    title: '',
    children: null,
    galleryFiles: [],
    product_url: '#',
    not_show_public_link: false,
    show_full_size_photo: false
};

export default ProductPreview;
