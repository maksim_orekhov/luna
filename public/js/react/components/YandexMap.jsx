import React, { Component } from 'react';
import PropTypes from 'prop-types';
import LoaderSpinner from './LoaderSpinner';

// Тут будет промисс загрузки карты
let loadYMap = null;

class YandexMap extends Component {

    state = {
        map: null,
        isDragging: false,
    };

    componentDidMount () {
        this.initMap();
    }

    componentDidUpdate (prevProps) {
        const { latitude, longitude } = this.props;
        const { map } = this.state;
        if (map && (prevProps.latitude !== latitude || prevProps.longitude !== longitude)) {
            this.updateMapPosition();
        }
    }

    componentWillUnmount () {
        this.map && this.map.destroy();
    }

    handleMapActionEnd = (ev) => {
        const { isDragging } = this.state;
        if (isDragging) {
            return false;
        }
        const { onSetPosition } = this.props;
        const [latitude, longitude] = ev.originalEvent.map.getCenter();
        onSetPosition({ latitude, longitude });
    };

    setMapRef = (node) => this.mapRef = node;

    initMap = () => {
        const { latitude, longitude, zoom, controls, onMapLoad, geoLocation } = this.props;
        let initialCenter = [latitude, longitude];

        if (!loadYMap) {
            loadYMap = new Promise(resolve => {
                const script = document.createElement('script');
                script.type = 'text/javascript';
                script.src = 'https://api-maps.yandex.ru/2.1/?lang=ru_RU';
                script.onload = () => resolve(window.ymaps);
                document.getElementsByTagName('head')[0].appendChild(script);
            });
        }
        loadYMap.then(ymaps => {
            ymaps.ready(() => {
                if (geoLocation) {
                    ymaps.geolocation.get({
                        provider: 'yandex',
                        mapStateAutoApply: true
                    }).then((response) => {
                        initialCenter = response.geoObjects.position;
                        this.renderMap(ymaps, {
                            center: initialCenter,
                            zoom,
                            controls
                        });
                        onMapLoad();
                    });
                } else {
                    this.renderMap(ymaps, {
                        center: initialCenter,
                        zoom,
                        controls
                    });
                    onMapLoad();
                }
            });
        });
    };

    updateMapPosition = () => {
        const { latitude, longitude } = this.props;
        const { map } = this.state;
        if (window.ymaps.util.math.areEqual([latitude, longitude], map.getCenter())) {
            return false;
        }
        this.setState({ isDragging: true }, () => {
            map.panTo([latitude, longitude], {
                safe: true,
                flying: false,
            }).then(() => {
                this.setState({ isDragging: false });
            }).catch((error) => {
                console.error('error---', error);
            });
        });
    };

    renderMap = (ymaps, options) => {
        const { onSetPosition } = this.props;
        const map  = new ymaps.Map(this.mapRef, options);
        map.events.add('actionend', this.handleMapActionEnd);
        this.setState({ map }, () => {
            onSetPosition({ latitude: options.center[0], longitude: options.center[1] });
        });
    };

    render () {
        const { map } = this.state;
        const { placeMarkOnCenter } = this.props;
        return(
            <div className="map-outer">
                <div className="map" ref={this.setMapRef} />
                {!map && <LoaderSpinner isSolid isSmall isAbsolute />}
                {placeMarkOnCenter && !!map &&  <div className="map-placemark" />}
            </div>
        );
    }

}

YandexMap.propTypes = {
    latitude: PropTypes.number.isRequired,
    longitude: PropTypes.number.isRequired,
    zoom: PropTypes.number,
    isDisabled: PropTypes.bool,
    placeMarkOnCenter: PropTypes.bool,
    // https://tech.yandex.ru/maps/doc/jsapi/2.1/dg/concepts/controls-docpage/
    controls: PropTypes.array,
    onMapLoad: PropTypes.func,
    onSetPosition: PropTypes.func,
    geoLocation: PropTypes.bool,
};

YandexMap.defaultProps = {
    zoom: 10,
    onSetPosition: () => {},
    isDisabled: true,
    placeMarkOnCenter: false,
    controls: [],
    onMapLoad: () => {},
    geoLocation: true,
};

export default YandexMap;
