import React from 'react';
import PropTypes from 'prop-types';
import GarantDeal from './GarantDeal';
import { getDateInPrettyFormatFromTimestamp, addSpacesBetweenThousands, getProductStatusIconClassName } from '../Helpers';

function UserProduct (props) {
    const {
        urls,
        gallery,
        category,
        subCategory,
        name,
        description,
        price,
        created,
        created_timestamp,
        onClick,
        is_show_status,
        status,
        status_show_format,
        fabricator
    } = props;
    const product_status_icon_class_name = getProductStatusIconClassName(status);

    return (
        <div className="col-product col-3 col-tablet-4 col-small-4">
            <div className="catalog-item no-delivery" onClick={(e) => {e.stopPropagation(); onClick();}}>
                <a className="catalog-item-photo" onClick={(e) => {e.stopPropagation(); onClick();}}>
                    {
                        is_show_status &&
                        <span className={`on-check-badge ${product_status_icon_class_name}`}>
                            <span className="on-check-badge__text">{status_show_format}</span>
                        </span>
                    }
                    <span className="photo-count">{gallery ? gallery.length : '0'} фото</span>
                    {
                        gallery && gallery[0] && gallery[0].id
                            ? <img
                                src={`/resize/img/${gallery[0].id}.resize_270x240.${gallery[0]['extension']}${gallery[0]['version']}`}
                                alt={name}
                            />
                            : <img src={`/resize/img/no-photo.resize_270x240.png`} alt={name} />
                    }

                </a>
                <div className="wrapper">
                    <div className="catalog-item-description">
                        <div className="catalog-item-description__main">
                            <div className="catalog-item-category">
                                <a href={category.url ? category.url : '#'} className="catalog-item-category__value" onClick={(e) => {e.stopPropagation();}}>
                                    {category.name}
                                </a>
                                <span> / </span>
                                <a href={subCategory.url ? subCategory.url : '#'} className="catalog-item-category__value" onClick={(e) => {e.stopPropagation();}}>
                                    {subCategory.name}
                                </a>
                            </div>
                            <div className="wrapper">
                                <h2 className="catalog-item-name">
                                    <a onClick={(e) => {e.stopPropagation(); onClick();}}>{name}</a>
                                </h2>
                                <div className="catalog-item-price">{addSpacesBetweenThousands(price)}</div>
                            </div>
                            <div className="summary">{description}</div>
                        </div>
                        <div className="catalog-item-info">
                            <div className="catalog-item-date">{getDateInPrettyFormatFromTimestamp(created_timestamp)}</div>
                            <div className="catalog-item-about-deal">
                                <GarantDeal

                                />
                                {
                                    fabricator &&
                                    <div className="status-manufacturer" title="От производителя" />
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

UserProduct.propTypes = {
    urls: PropTypes.object.isRequired,
    category: PropTypes.object,
    subCategory: PropTypes.object,
    gallery: PropTypes.array,
    name: PropTypes.string.isRequired,
    created: PropTypes.string,
    created_timestamp: PropTypes.any,
    description: PropTypes.string,
    price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    onClick: PropTypes.func,
    fabricator: PropTypes.bool
};

UserProduct.defaultProps = {
    description: '',
    created: '',
    created_timestamp: '',
    price: 0,
    gallery: [],
    onClick: () => {},
    category: {},
    subCategory: {},
    fabricator: false
};

export default UserProduct;
