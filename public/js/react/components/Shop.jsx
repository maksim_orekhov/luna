import React from 'react';
import PropTypes from 'prop-types';

function Shop (props) {
    const {
        urls,
        subCategories,
        name,
        description,
        logo
    } = props;

    return (
        <div className="row shops-item">
            <div className="col-2 col-desk-2 col-tablet-2 col-mobile-8 shops-item-logo">
                <div className="AvatarWithBorder">
                    <div className="AvatarWithBorder-Container">
                        {
                            logo ?
                                <img src={`/resize/img/${logo.id}.resize_140x140.${logo.extension}${logo.version}`} alt="Логотип магазина" />
                                :
                                <img src={`/resize/img/no-photo.resize_140x140.png`} alt="Логотип магазина" />
                        }
                    </div>
                </div>
                <div className="shops-item-name mobile">
                    <a href={`${urls.single_shop_public}`} title={name}>{name}</a>
                </div>
            </div>
            <div className="col-10 col-desk-10 col-tablet-6 col-mobile-8">
                <div className="shops-item-content">
                    <div className="shops-item-name">
                        <a href={`${urls.single_shop_public}`} title={name}>{name}</a>
                    </div>
                    {
                        subCategories.length > 0 &&
                        <ul className="categories-list">
                            {
                                subCategories.map((subCategory, i) => {
                                    return (
                                        <li className="subcategory-item" key={i}>
                                            <a href={`${subCategory.url}`}>{subCategory.name}</a>
                                        </li>
                                    );
                                })
                            }
                        </ul>
                    }
                    <div className="shops-item-description">{description}</div>
                </div>
            </div>
        </div>
    );
}

Shop.propTypes = {
    urls: PropTypes.object.isRequired,
    logo: PropTypes.object.isRequired,
    subCategories: PropTypes.array.isRequired,
    name: PropTypes.string.isRequired,
    created: PropTypes.string,
    created_timestamp: PropTypes.any,
    description: PropTypes.string,
};

Shop.defaultProps = {
    description: '',
    created: '',
    created_timestamp: '',
};

export default Shop;