import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';


function LoaderSpinner ({ isSolid, isSmall, isTiny, isAbsolute, cssClassName }){
    return(
        <div className={classNames({
            Preloader: true,
            Preloader_solid: isSolid,
            Preloader_small: isSmall,
            Preloader_tiny: isTiny,
            Preloader_absolute: isAbsolute,
            [cssClassName]: true,
        })}>
            <div className="Preloader__container">
                <div className="Preloader__spinner" />
            </div>
        </div>
    );
}

LoaderSpinner.propTypes = {
    isSolid: PropTypes.bool,
    isSmall: PropTypes.bool,
    isTiny: PropTypes.bool,
    isAbsolute: PropTypes.bool,
    cssClassName: PropTypes.string,
};

LoaderSpinner.defaultProps = {
    isSolid: false,
    isSmall: false,
    isTiny: false,
    isAbsolute: false,
    cssClassName: '',
};

export default LoaderSpinner;
