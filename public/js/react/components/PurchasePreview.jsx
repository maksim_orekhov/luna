import React from 'react';
import PropTypes from 'prop-types';
import { prettifyPhoneNumber, addSpacesBetweenThousands } from '../Helpers';

function PurchasePreview(props) {
    const {
        price, quantity, measure, description, address, files, name, title, category, sub_category, user, children, purchase_url, not_show_public_link
    } = props;

    const measure_string = +quantity ? `${quantity} ${measure || ''}` : 'не указано';
    const price_string = +price ? `${addSpacesBetweenThousands(price)} Руб.` : 'не указан';

    return(
        <div className="product-preview product-preview-popup">
            <span className="preview-title-container">{ title.length > 0 && <h2 className="page-title">{title}</h2> }</span>

            <div className="product-preview__inner purchase-preview__inner">
                {
                    !not_show_public_link &&
                        <div className="purchase_preview_link_row">
                            <a className="link-inline" href={purchase_url}>Посмотреть как продавец</a>
                        </div>
                }
                <h2 className="product-name">{name}</h2>
                <div className="product-options-wrapper">
                    <div className="product-options-block">
                        <div className="product-option">
                            <div className="product-option__name">Бюджет:</div>
                            <div className="product-option__value">{price_string}</div>
                        </div>
                        <div className="product-option">
                            <div className="product-option__name">Количество:</div>
                            <div className="product-option__value">{measure_string}</div>
                        </div>
                        <div className="product-option">
                            <div className="product-option__name">Категория:</div>
                            <div className="product-option__value product-option__value_size_big">
                                {`${category['name']} — ${sub_category['name']}`}
                            </div>
                        </div>
                        <div className="product-option">
                            <div className="product-option__name">Описание:</div>
                            <div className="product-option__value description product-option__value_size_big">{description}</div>
                        </div>
                        <div className="product-option">
                            <div className="product-option__name">Местоположение:</div>
                            <div className="product-option__value product-option__value_size_big">
                                {
                                    address && Object.values(address).length !== 0
                                        ? address['city']
                                            + (address['street'] ? ', ' + address['street'] : '')
                                            + (address['house'] ? ', ' + address['house'] : '')
                                        : 'не указано'
                                }
                            </div>
                        </div>
                        <div className="product-option">
                            <div className="product-option__name">Имя в объявлениях:</div>
                            <div className="product-option__value product-option__value_size_big">{user['name']}</div>
                        </div>
                        <div className="product-option">
                            <div className="product-option__name">Телефон:</div>
                            <div className="product-option__value product-option__value_size_big">{prettifyPhoneNumber(user['phone'])}</div>
                        </div>
                        <div className="product-option">
                            <div className="product-option__name">E-mail:</div>
                            <div className="product-option__value product-option__value_size_big">{user['email']}</div>
                        </div>
                        <div className="files-list">
                            {files.map((file) => {
                                return (
                                    <div className="file-item" key={file['id']}>
                                        <a href={file['path']} target="_blank">{file['name']}</a>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </div>
            </div>
            {children}
        </div>
    );
}

PurchasePreview.propTypes = {
    name: PropTypes.string.isRequired,
    price: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]).isRequired,
    quantity: PropTypes.number.isRequired,
    measure: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    category: PropTypes.object.isRequired,
    sub_category: PropTypes.object.isRequired,
    address: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    files: PropTypes.array,
    onClickBack: PropTypes.func,
    children: PropTypes.any,
    title: PropTypes.string,
    not_show_public_link: PropTypes.bool,
    purchase_url: PropTypes.string
};

PurchasePreview.defaultProps = {
    files: [],
    onClickBack: () => {},
    title: '',
    children: null,
    purchase_url: '#',
    not_show_public_link: false,
};

export default PurchasePreview;
