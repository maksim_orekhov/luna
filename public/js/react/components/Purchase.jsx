import React from 'react';
import PropTypes from 'prop-types';
import { getDateInPrettyFormatFromTimestamp, addSpacesBetweenThousands } from '../Helpers';

function getStatusLabel(status) {
    if (!status || !status.hasOwnProperty('status') || !status.status) return null;

    const statuses = {
        published: {
            name: 'Опубликовано',
            icon_class_name: 'published'
        },
        waiting_moderation: {
            name: 'Ожидает модерации',
            icon_class_name: 'moderation_pending'
        },
        publish_stopped: {
            name: 'Показ остановлен',
            icon_class_name: 'paused'
        },
        declined: {
            name: 'Отклонено модератором',
            icon_class_name:'declined',
        },
        archive: {
            name: 'Архив',
            icon_class_name: 'archive'
        }
    };

    const current_status = statuses[status.status];

    if (!current_status) return null;

    return (
        <div className="catalog-item-purchase-status">
            <div className="catalog-item-purchase-status__status">
                <div className={`catalog-item-purchase-status__text ${current_status.icon_class_name}`}>{current_status.name}</div>
            </div>
        </div>
    );
}

function Purchase (props) {
    const {
        urls,
        category,
        subCategory,
        name,
        description,
        price,
        created,
        created_timestamp,
        quantity,
        owner,
        measure,
        onClick,
        is_profile,
        status
    } = props;

    return (
        <div
            className="col-12 col-tablet-8 col-mobile-8 col-small-4"
            // style={{marginBottom: '10px'}}
            onClick={is_profile ? onClick : () => {window.location = urls.single_purchase_public;}}
        >
            <div className="catalog-item catalog-item-purchase no-delivery">
                <div className="row nested-row wrapper wrapper-purchase">

                    <div className="col-9 col-different-grid col-mobile-4 col-small-4">
                        <div className="row row_full-width">
                            <div className="col-6 col-tablet-8 col-mobile-8 col-overflow" style={{margin: '0'}}>
                                <div className="catalog-item-description__main">
                                    <div className="catalog-item-category">
                                        { category &&
                                        <div className="catalog-item-breadcrumbs">
                                            <a
                                                href={category.url ? category.url : '#'}
                                                className="catalog-item-category__value"
                                                onClick={(e) => {e.stopPropagation();}}
                                            >
                                                {category.name}
                                            </a>
                                            <span> / </span>
                                            <a
                                                href={subCategory.url ? subCategory.url : '#'}
                                                className="catalog-item-category__value"
                                                onClick={(e) => {e.stopPropagation();}}
                                            >
                                                {subCategory.name}
                                            </a>
                                        </div>
                                        }
                                        <h2 className="catalog-item-name catalog-item-name-purchase">
                                            { urls &&
                                            <a
                                                href={urls.single_purchase_public}
                                                onClick={(e) => {!is_profile && e.stopPropagation();}}
                                            >
                                                {name}
                                            </a>
                                            }
                                        </h2>
                                        <div className="catalog-item-date catalog-item-date-purchase">{getDateInPrettyFormatFromTimestamp(created_timestamp)}</div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-3 col-tablet-4 col-mobile-4 col-budget col-overflow" style={{margin: '0'}}>
                                <div className="catalog-item-purchase-wrap">
                                    <div className="catalog-item-price-title">Бюджет:</div>
                                    { (price || price !== 0) ?
                                        <div className="catalog-item-price catalog-item-price-purchase">{addSpacesBetweenThousands(price)}</div>
                                        :
                                        <div className="catalog-item-price no-price catalog-item-price-purchase">Не указан</div>
                                    }
                                </div>
                            </div>
                            <div className="col-3 col-tablet-8 col-mobile-4 purchase-quantity" style={{margin: '0'}}>
                                <div className="catalog-item-purchase-wrap">
                                    <div className="catalog-item-quantity-title">Количество:</div>
                                    {quantity ?
                                        <div className="catalog-item-price catalog-item-quantity-purchase no-price">{quantity} {measure}</div>
                                        :
                                        <div className="catalog-item-price catalog-item-quantity-purchase no-price">Не указано</div>
                                    }
                                </div>
                            </div>
                            <div className="col-mobile-8 col-item-date-purchase__mobile">
                                <div className="catalog-item-date catalog-item-date-purchase__mobile">
                                    {getDateInPrettyFormatFromTimestamp(created_timestamp)}
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="col-3 col-mobile-4 col-small-4 col-different-content" style={{margin: '0'}}>
                        {
                            is_profile
                                ? getStatusLabel(status)
                                : <div className="user-bar user-bar-purchase">
                                    <div className="avatar" style={{backgroundImage: `url(${owner.avatar})`}} />
                                    <div className="user-bar__info user-bar__info-purchase">
                                        <span className="user-bar-name">{owner.name}</span>
                                        <span className="rating-scale rating-scale-purchase">
                                            <img alt="" src="/img/star_blue.svg" />
                                            <img alt="" src="/img/star.svg" />
                                            <img alt="" src="/img/star.svg" />
                                            <img alt="" src="/img/star.svg" />
                                            <img alt="" src="/img/star.svg" />
                                        </span>
                                        <div className="user-bar__from-registration">{owner.time_from_create} на ЛУНЕ</div>
                                    </div>
                                </div>
                        }
                    </div>
                </div>
            </div>
        </div>
    );
}

Purchase.propTypes = {
    urls: PropTypes.object.isRequired,
    category: PropTypes.object.isRequired,
    subCategory: PropTypes.object.isRequired,
    owner: PropTypes.object,
    name: PropTypes.string.isRequired,
    created: PropTypes.string,
    created_timestamp: PropTypes.any,
    description: PropTypes.string,
    quantity: PropTypes.number,
    measure: PropTypes.string,
    price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    onClick: PropTypes.func,
    status: PropTypes.object,
    is_profile: PropTypes.bool
};

Purchase.defaultProps = {
    description: '',
    quantity: 0,
    measure: '',
    created: '',
    created_timestamp: '',
    price: 0,
    is_profile: false,
    owner: {},
    status: {},
};

export default Purchase;
