import React from 'react';
import PropTypes from 'prop-types';
import GarantDeal from './GarantDeal';
import { getDateInPrettyFormatFromTimestamp, addSpacesBetweenThousands } from '../Helpers';

function Product (props) {
    const {
        urls,
        gallery,
        category,
        subCategory,
        name,
        description,
        price,
        created,
        is_show_status,
        status,
        status_show_format,
        created_timestamp,
        fabricator
    } = props;

    return (
        <div className="col-3 col-product col-tablet-4 col-small-4">
            <div
                className="catalog-item no-delivery"
                onClick={() => {if (urls.single_product_public) window.location = urls.single_product_public;}}
            >
                <a className="catalog-item-photo" href={`${urls.single_product_public}`} onClick={(e) => {e.stopPropagation();}}>
                    {
                        is_show_status &&
                        <span className="on-check-badge">
                            <span className="on-check-badge__text">{status_show_format}</span>
                        </span>
                    }
                    <span className="photo-count">{gallery ? gallery.length : '0'} фото</span>
                    {
                        gallery && gallery[0] && gallery[0].id
                            ? <img
                                src={`/resize/img/${gallery[0].id}.resize_270x240.${gallery[0]['extension']}${gallery[0]['version']}`}
                                alt={name}
                            />
                            : <img src={`/resize/img/no-photo.resize_270x240.png`} alt={name} />
                    }
                </a>
                <div className="wrapper">
                    <div className="catalog-item-description">
                        <div className="catalog-item-description__main">
                            <div className="catalog-item-category">
                                <a href={category.url ? category.url : '#'} className="catalog-item-category__value" onClick={(e) => {e.stopPropagation();}}>
                                    {category.name}
                                </a>
                                <span> / </span>
                                <a
                                    href={subCategory.url ? subCategory.url : '#'}
                                    className="catalog-item-category__value"
                                    onClick={(e) => {e.stopPropagation();}}
                                >
                                    {subCategory.name}
                                </a>
                            </div>
                            <div className="wrapper">
                                <h2 className="catalog-item-name">
                                    <a href={`${urls.single_product_public}`}  onClick={(e) => {e.stopPropagation();}}>{name}</a>
                                </h2>
                                <div className="catalog-item-price">{addSpacesBetweenThousands(price)}</div>
                            </div>
                            <div className="summary">{description}</div>
                        </div>
                        <div className="catalog-item-info">
                            <div className="catalog-item-date">{getDateInPrettyFormatFromTimestamp(created_timestamp)}</div>
                            <div className="catalog-item-about-deal">
                                <GarantDeal

                                />
                                {
                                    fabricator &&
                                    <div className="status-manufacturer" title="От производителя" />
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

Product.propTypes = {
    urls: PropTypes.object.isRequired,
    category: PropTypes.object.isRequired,
    subCategory: PropTypes.object.isRequired,
    gallery: PropTypes.array,
    name: PropTypes.string.isRequired,
    created: PropTypes.string,
    created_timestamp: PropTypes.any,
    description: PropTypes.string,
    status: PropTypes.string,
    price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    is_show_status: PropTypes.bool,
    status_show_format: PropTypes.string,
    fabricator: PropTypes.bool
};

Product.defaultProps = {
    description: '',
    created: '',
    created_timestamp: '',
    price: 0,
    gallery: [],
    is_show_status: false,
    fabricator: false,
    status: '',
    status_show_format: ''
};

export default Product;