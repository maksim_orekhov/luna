import React from 'react';
import PropTypes from 'prop-types';

function ContentHeader ({ breadcrumbs, viewMode, changeViewModeList, changeViewModeBlock, users_products }) {
    return(
        <section className="content-header">
            <div className="container">
                <div className="row wrap-flex">
                    <div className="col-8">
                        {
                            users_products ?
                                <ul className="row tab">
                                    <li className="tab tab__title col-2 col-tablet-3 active">Объявления</li>
                                </ul>
                                :
                                <div className="content-header-navigation">
                                    <h2 className="content-header-navigation__title">Все товары</h2>
                                    <ul className="navigation-chain">
                                        {
                                            breadcrumbs.map((item, i, breadcrumbs) => {
                                                return (
                                                    <li key={item.name}>
                                                        <a href={item.url}>{item.name}</a> {breadcrumbs.length - 1 === i ? '' : '/'}&nbsp;
                                                    </li>
                                                );
                                            })
                                        }
                                    </ul>
                                </div>
                        }

                    </div>
                    <div className="col-3" />
                    <div className="col-1">
                        <div className="mode-view-block">
                            <span className={`mode-item mode-item_type_list ${viewMode === 'list' ? 'active' : ''}`} onClick={changeViewModeList}>&nbsp;</span>
                            <span className={`mode-item mode-item_type_block ${viewMode === 'block' ? 'active' : ''}`} onClick={changeViewModeBlock}>&nbsp;</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

ContentHeader.propTypes = {
    viewMode: PropTypes.string,
    breadcrumbs: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string,
        url: PropTypes.string,
    })),
    changeViewModeList: PropTypes.func,
    changeViewModeBlock: PropTypes.func,
};

ContentHeader.defaultProps = {
    viewMode: 'block',
    breadcrumbs: [],
    changeViewModeList: () => {},
    changeViewModeBlock: () => {},
};

export default ContentHeader;
