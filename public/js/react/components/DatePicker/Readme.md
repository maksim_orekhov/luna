#Компонент DatePicker

## Использование
```javascript
import DatePicker from 'path-to-folder/DatePicker';

....

<DatePicker />

```
Для подключения стилей компонента необходимо выполнить 
```less
@import "../application/molecules/date-picker";
```
В качестве Props может принимать следующие опции (все необязательны):

Имя | Тип | Описание
:---: | :---: | :---:
`multipleDates` | *boolean* | множественный выбор дат, по умолчанию *false*
`selectedDate` | *instaceof Date, instaceof Date[]* | объект даты выбранного значения или массив из выбранных дат, если задан параметр multipleDates, по умолчанию *null*
`currentView` | *'days', 'months', 'years'* | начальное представление календаря (дни, месяцы, годы), по умолчанию *'days'*
`minDate` | *instaceof Date* | минимальная дата, меньше которой нельзя выбрать день, по умолчанию *null*
`maxDate` | *instaceof Date* | максимальная дата, после которой нельзя выбрать день, по умолчанию *null*
`disabledDates` | *(instaceof Date | [instaceof Date, instaceof Date])[]* | массив с заблокированными датами. Можно передать в качестве аргументов массива либо дату конкретного дня, либо массив из двух дат - начало и конец заблокированного интервала. По умолчанию *[]*
`disabledDays` | *number[]* | номера заблокированных дней недели, где понедельник - 0, а воскресенье - 6. По умолчанию *[]*
`onSelectDate(selectedDate)` | *function* | функция callback для выбора даты. При multipleDates возвращает массив дат. `selectedDate` - *Date, Date[], null* - выбранная дата` 
`onChangeYear({ selectedDate, visibleDate, currentView })` | *function* | функция callback для изменения года. При multipleDates возвращает массив дат. `selectedDate` - Date, Date[], null - выбранная дата, `visibleDate` - *Date* - видимая дата в активном представлении календаря, `currentView` - *string* - активное представление календаря  
`onChangeMonth({ selectedDate, visibleDate, currentView })` | *function* | функция callback для изменения месяца. При multipleDates возвращает массив дат. `selectedDate` - Date, Date[], null - выбранная дата, `visibleDate` - *Date* - видимая дата в активном представлении календаря, `currentView` - *string* - активное представление календаря  
`onChangeView({ selectedDate, visibleDate, currentView })` | *function* | функция callback для изменения представления календаря. `selectedDate` - Date, Date[], null - выбранная дата, `visibleDate` - *Date* - видимая дата в активном представлении календаря, `currentView` - *string* - активное представление календаря  


##Примеры
Например, если необходимо заблокировать 17.10.2018 и 2.10.2018 - 10.10.2018:
```javascript
<DatePicker
    disabledDates={[
        new Date(2018, 9, 17),
        [ new Date(2018, 9, 2), new Date(2018, 9, 10) ],
    ]}
/>
```
Например, если необходимо заблокировать воскресенья и субботы:
```javascript
<DatePicker
    disabledDays={[5, 6]}
/>
```
