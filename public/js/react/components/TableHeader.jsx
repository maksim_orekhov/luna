import React from 'react';
import PropTypes from 'prop-types';

function TableHeader ({ type, purchases_url, products_url, viewMode, search_value, changeViewMode, handleChangeSearch, handleFindButtonClick, handleKeyPress, no_burger }) {
    const is_products = type === 'products';
    const is_purchases = type === 'purchases';
    const active_tab_title = is_products ? 'Объявления' : 'Закупки';
    const inactive_tab_title = is_purchases ? 'Объявления' : 'Закупки';
    const inactive_tab_url = is_purchases ? products_url : purchases_url;
    const placeholder = is_purchases ? 'Поиск по закупкам покупателя' : 'Поиск по объявлениям продавца';

    return(
        <section className="content-header">
            <div className="container">
                <div className="row wrap-flex">
                    <div className="tabs-container col-4 col-desk-4 col-tablet-8 col-mobile-8">
                        <div className="tab col-6 col-desk-6 col-mobile-4">
                            {
                                is_products ?
                                    <div className={`tab__title active`}>
                                        <span>
                                            {active_tab_title}
                                        </span>
                                    </div>
                                    :
                                    <div className={`tab__title`}>
                                        <a href={inactive_tab_url}>
                                            <span>
                                                {inactive_tab_title}
                                            </span>
                                        </a>
                                    </div>
                            }
                        </div>
                        <div className="tab col-6 col-desk-6 col-mobile-4">
                            {
                                is_purchases ?
                                    <div className={`tab__title active`}>
                                        <span>
                                            {active_tab_title}
                                        </span>
                                    </div>
                                    :
                                    <div className={`tab__title`}>
                                        <a href={inactive_tab_url}>
                                            <span>
                                                {inactive_tab_title}
                                            </span>
                                        </a>
                                    </div>
                            }
                        </div>
                    </div>

                    <div className="col-empty col-3 col-desk-4" />
                    {
                        type === 'purchases' &&
                        <div className="col-empty col-1 col-desk-2" />
                    }
                    <div className="col-4 col-desk-5 col-tablet-8 col-small-4 col-xs-small-4">
                        <div className="control-form">
                            <input type="search" className={`input-search ${no_burger ? 'no_burger' : ''}`} onChange={handleChangeSearch} onKeyPress={handleKeyPress} placeholder={placeholder} />
                        </div>
                    </div>
                    {
                        type === 'products' &&
                        <div className="mode-view-block col-1 col-desk-1">
                            <span className={`mode-item mode-item_type_list ${viewMode === 'list' ? 'active' : ''}`} onClick={changeViewMode('list')}>&nbsp;</span>
                            <span className={`mode-item mode-item_type_block ${viewMode === 'block' ? 'active' : ''}`} onClick={changeViewMode('block')}>&nbsp;</span>
                        </div>
                    }
                </div>
            </div>
        </section>
    );
}

// TableHeader.propTypes = {
//     viewMode: PropTypes.string,
//     breadcrumbs: PropTypes.arrayOf(PropTypes.shape({
//         name: PropTypes.string,
//         url: PropTypes.string,
//     })),
//     changeViewModeList: PropTypes.func,
//     changeViewModeBlock: PropTypes.func,
// };
//
// TableHeader.defaultProps = {
//     viewMode: 'block',
//     breadcrumbs: [],
//     changeViewModeList: () => {},
//     changeViewModeBlock: () => {},
// };

export default TableHeader;
