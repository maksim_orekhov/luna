import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Tabs extends Component {
    constructor(props){
        super(props);
        this.state = {
            activeTabIndex: this.props.defaultActiveTabIndex
        };
    }

    componentDidUpdate(prevProps, prevState){
        const { activeTabIndex } = this.state;
        const { onTabChange } = this.props;
        if(prevState.activeTabIndex !== activeTabIndex){
            onTabChange(activeTabIndex);
        }
    }

    handleTabClick = tabIndex => {
        this.setState({ activeTabIndex: tabIndex });
    };

    renderTabs = () => {
        const { children } = this.props;
        return React.Children.map(children, (child, index) => {
            return React.cloneElement(child, {
                clickHandler: this.handleTabClick,
                tabIndex: index,
                isActive: this.state.activeTabIndex === index
            });
        });
    };

    renderActiveTabContent = () => {
        if(this.props.children[this.state.activeTabIndex]){
            if(typeof this.props.children[this.state.activeTabIndex].props.children === 'function'){
                return this.props.children[this.state.activeTabIndex].props.children();
            }
            return this.props.children[this.state.activeTabIndex].props.children;
        }
        return null;
    };

    render(){
        const { tabsListClassName, tabsWrapperClassName } = this.props;
        return(
            <div className="tabs">
                <div className={tabsWrapperClassName}>
                    <div className="container">
                        <ul className={tabsListClassName}>
                            {this.renderTabs()}
                        </ul>
                    </div>
                </div>
                <div className="tabs__content">
                    {this.renderActiveTabContent()}
                </div>
            </div>
        );
    }
}

Tabs.propTypes = {
    defaultActiveTabIndex: PropTypes.number,
    children: PropTypes.oneOfType([
        PropTypes.array,
        PropTypes.element,
    ]).isRequired,
    onTabChange: PropTypes.func,
    tabsListClassName: PropTypes.string,
    tabsWrapperClassName: PropTypes.string,
};

Tabs.defaultProps = {
    defaultActiveTabIndex: 0,
    onTabChange: () => {},
    tabsListClassName: '',
    tabsWrapperClassName: '',
};

export default Tabs;
