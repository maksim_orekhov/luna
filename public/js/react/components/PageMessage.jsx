import React from 'react';
import PropTypes from 'prop-types';

function PageMessage({ message }) {
    return(
        <div className="page-message-container">
            <div>
                <img src="/img/union.svg" alt="" className="page-message-image" />
                <h4 className="page-message">{message}</h4>
            </div>
        </div>
    );
}

PageMessage.propTypes = {
    message: PropTypes.string.isRequired
};

export default PageMessage;
