import React, { Component } from 'react';
import Popup from '../Popup';
import InfoConnectionSecuryDeal from './InfoConnectionSecuryDeal';

export default class GarantDeal extends Component {
    constructor(props){
        super(props);
        this.state = {
            isOpenedInfoConnection: false,
        };

        this.handleClosePopup = this.handleClosePopup.bind(this);
    }

    showForm = (form_name) => () => {
        this.setState({
            [form_name]: true
        });
    };

    handleClosePopup() {
        this.setState({
            isOpenedInfoConnection: false
        });
    }

    render() {
        const {isOpenedInfoConnection} = this.state;
        return (
            <div onClick={(e) => {e.stopPropagation();}}>
                <div className="garant-deal">
                    <div className="tooltip">
                        <div className="tooltip-inner">
                            Для этого товара доступна безопасная сделка. Подробнее о <a className="link-inline" onClick={this.showForm('isOpenedInfoConnection')}>безопасной сделке</a>.
                        </div>
                    </div>
                </div>
                {
                    isOpenedInfoConnection &&
                    <Popup close={this.handleClosePopup} popupClassName="form-connection-secury-deal info-message">
                        <InfoConnectionSecuryDeal
                            handleClosePopup={this.handleClosePopup}
                        />
                    </Popup>
                }
            </div>
        );
    }
}
