import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class SelectImitation extends Component {
    constructor(props){
        super(props);
        const activeOption = this.getOptionByValue(this.props.defaultValue);

        this.state = {
            dropDownIsOpen: false,
            value: (typeof activeOption === 'object' && typeof activeOption.value !== 'undefined') ? activeOption.value : null
        };
    }

    componentDidMount(){
        document.addEventListener('click', this.handleClickOutside);
    }

    componentDidUpdate(prevProps, prevState){
        const { value } = this.state;
        const { name, defaultValue } = this.props;
   
        if(prevState.dropDownIsOpen !== this.state.dropDownIsOpen){
            this.triggerToggleHandlers();
        }
        if(prevState.value !== value){
            this.props.onChangeHandler({ value, name });
        }
        if(value && !this.getOptionByValue(value)){
            this.setState({ value: null });
        }

        if (defaultValue && prevProps.defaultValue !== defaultValue) {
            this.handleOptionClick(defaultValue)();
        }
    }

    componentWillUnmount(){
        document.removeEventListener('click', this.handleClickOutside);
    }

    handleClickOutside = (ev) => {
        if(this.state.dropDownIsOpen && !this.selectImitationRef.contains(ev.target)){
            this.setState({ dropDownIsOpen: false });
        }
    };

    handleOptionClick = optionValue => () => {
        if(!this.props.isDisabled) {
            this.setState({dropDownIsOpen: false, value: optionValue});
        }
    };

    toggleDropDown = () => {
        const {dropDownIsOpen} = this.state;
        if(!this.props.isDisabled){
            this.setState({ dropDownIsOpen: !dropDownIsOpen });
        }
    };

    /**
     * Вызывает срабатывание обработчиков на открытие/закрытие селекта
     */
    triggerToggleHandlers = () => {
        const {onOpenHandler, onCloseHandler} = this.props;
        if(this.state.dropDownIsOpen){
            onOpenHandler({...this.state});
        } else {
            onCloseHandler({...this.state});
        }
    };

    getOptionByValue = (value) => {
        const {options} = this.props;
        return options.find(option => option.value == value);
    };

    setSelectImitationRef = node => this.selectImitationRef = node;

    /**
     * Рендерит элементы селекта
     * @returns {array}
     */
    renderOptions = () => {
        // Если поле не обязательно к заполнению, то добавляем в список опций плейсхолодер
        const placeholderOption = this.props.isRequired ? [] : [{ title: this.props.placeholder, value: null }];

        return placeholderOption.concat(this.props.options).map(option => {
            if(option.value === this.state.value){
                return null;
            }
            return (
                <li key={option.value} onClick={this.handleOptionClick(option.value)}>
                    {option.title}
                </li>
            );
        });
    };

    render() {
        const {dropDownIsOpen, value} = this.state;
        const {label, placeholder, isDisabled} = this.props;
        const activeOption = this.getOptionByValue(value);

        const displayedValue = !value
            ? placeholder
            : activeOption
                ? activeOption.title
                : placeholder;

        return (
            <div>
                <div
                    className={`SelectImitation${isDisabled ? ' disabled' : ''}`}
                    ref={this.setSelectImitationRef}
                >
                    <div className="SelectImitation-Container">
                        <label className="label-text">
                            {label}
                        </label>
                        <div
                            className={`SelectImitation-Text ${dropDownIsOpen ? 'active' : ''}`}
                            title={displayedValue}
                            onClick={this.toggleDropDown}
                        >
                            {displayedValue}
                        </div>
                        {dropDownIsOpen &&
                            <ul className="SelectImitation-Menu">
                                {this.renderOptions()}
                            </ul>
                        }
                    </div>
                </div>
            </div>
        );
    }

}

SelectImitation.propTypes = {
    options: PropTypes.arrayOf(PropTypes.shape({
        title: PropTypes.string.isRequired,
        value: PropTypes.any.isRequired,
    }).isRequired).isRequired,
    defaultValue: PropTypes.any,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    name: PropTypes.string,
    // Если false, то placeholder можно будет выбрать в списке опций
    isRequired: PropTypes.bool,
    isDisabled: PropTypes.bool,
    onChangeHandler: PropTypes.func,
    onOpenHandler: PropTypes.func,
    onCloseHandler: PropTypes.func,
};
SelectImitation.defaultProps = {
    defaultValue: null,
    label: '',
    placeholder: '',
    name: '',
    isRequired: false,
    isDisabled: false,
    onChangeHandler: () => {},
    onOpenHandler: () => {},
    onCloseHandler: () => {},
};