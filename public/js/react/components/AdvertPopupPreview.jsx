import React from 'react';
import PropTypes from 'prop-types';
import { customFetch } from '../Helpers';

export default class AdvertPopupPreview extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            price: '',
            category: '',
            sub_category: '',
            description: '',
            city: '',
            owner_name: '',
            owner_phone: '',
            owner_email: '',
            gallery: []
        };

        this.init = this.init.bind(this);
        this.setDataToState = this.setDataToState.bind(this);
    }

    componentWillMount() {
        this.init();
    }

    init() {
        const { moderation_id } = this.props;
        customFetch('/product-moderation/' + moderation_id)
            .then((data) => {
                if (data.data.productModeration) {
                    this.setDataToState(data.data.productModeration.product);
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }

    setDataToState(data) {
        const { address, category, sub_category, name, price, description, owner, gallery } = data;

        this.setState({
            name,
            price,
            description,
            category: category.name,
            sub_category: sub_category.name,
            city: address.city,
            owner_name: owner.name,
            owner_phone: owner.phone,
            owner_email: owner.email,
            gallery
        });
    }

    render() {
        const {
            name,
            price,
            category,
            sub_category,
            description,
            city,
            owner_name,
            owner_phone,
            owner_email,
            gallery
        } = this.state;

        return(
            <div className="advert_actions_popup_ad_preview">
                <div className="advert_title">
                    <div className="guarant_pay_logo">&nbsp;</div>
                    <h4>{name}</h4>
                </div>
                <div className="advert_properties">
                    <table>
                        <tbody>
                            <tr>
                                <td>Цена:</td>
                                <td className="value">{price} Руб.</td>
                            </tr>
                            <tr>
                                <td>Категория:</td>
                                <td className="value">{category} — {sub_category}</td>
                            </tr>
                            <tr>
                                <td>Описание:</td>
                                <td className="value">{description}</td>
                            </tr>
                            <tr>
                                <td>Местоположение:</td>
                                <td className="value">{city}</td>
                            </tr>
                            <tr>
                                <td>Имя в объявлениях:</td>
                                <td className="value">{owner_name}</td>
                            </tr>
                            <tr>
                                <td>Телефон:</td>
                                <td className="value">+{owner_phone}</td>
                            </tr>
                            <tr>
                                <td>Email:</td>
                                <td className="value">{owner_email}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div className="advert_actions_popup_images">
                    {gallery.map((image, index) => {
                        return (<img src={`/resize/img/${image && image.id}.resize_168x168.jpg`} alt="" key={index} />);
                    })}
                </div>
            </div>
        );
    }
}