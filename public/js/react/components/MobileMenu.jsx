import React, { Component, PureComponent } from 'react';
import PropTypes from 'prop-types';


export default class MobileMenu extends PureComponent {
    render() {
        const { Header, Menu } = this.props;

        return  (
            <div>
                <div className="row mobile-menu js-mobile-categories-menu">
                    <div className="mobile-menu-container mobile_menu_new">
                        { Header }
                        { Menu }
                    </div>
                </div>
            </div>
        );
    }
}
