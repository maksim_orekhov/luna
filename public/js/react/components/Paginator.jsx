import React from 'react';

export default class Paginator extends React.Component {
    constructor(props) {
        super(props);
    }

    sendUpNewPageNumber(value) {
        const { first, last } = this.props.paginator;

        if (value >= first && value <= last) {
            this.props.handlePageSwitch(value);
        }
    }

    handlePrev = (e) => {
        e.preventDefault();
        const { first, current } = this.props.paginator;

        if (current > first) {
            this.props.handlePageSwitch(current - 1);
        }
    };

    handleNext = (e) => {
        e.preventDefault();
        const { last, current } = this.props.paginator;

        if (current < last) {
            this.props.handlePageSwitch(current + 1);
        }
    };

    changePerPageCount = (e) => {
        const { value } = e.target;
        this.props.handlePerPageChange(value);
    };

    render() {
        const { per_page, paginator } = this.props;

        return (
            <div className="pagination">
                <div className="pagination-wrap">
                    <div className="pagination-item">
                        Показывать по:
                    </div>
                    <div className="pagination-item">
                        <select name="" id="" onChange={this.changePerPageCount} value={per_page}>
                            <option defaultValue="10">10</option>
                            <option value="30">30</option>
                            <option value="50">50</option>
                        </select>
                    </div>
                    {
                        paginator.first_item_number && paginator.last_item_number &&
                        <div className="pagination-item">
                            <span>{paginator.first_item_number}-{paginator.last_item_number}&nbsp;</span>
                            <span>из&nbsp;</span>
                            <span>{paginator.total_item_count}</span>
                        </div>
                    }

                    <div className="pagination-item ">
                        <a href="#" className="arrow" title="Назад" onClick={this.handlePrev} />
                        <a href="#" className="arrow" title="Вперёд" onClick={this.handleNext} />
                    </div>
                </div>
            </div>
        );
    }
}
