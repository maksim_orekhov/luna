import React from 'react';
import PropTypes from 'prop-types';
import Popup from '../Popup';
import Timer from '../Timer';

export default class InfoMessage extends React.PureComponent {
    state = {
        stop_auto_close: false,
        is_paused: false,
    };

    setTimer = (trueOrFalse) => {
        if (this.props.is_auto_close) {
            if (this.state.is_paused === trueOrFalse) return;

            this.setState({
                is_paused: trueOrFalse
            });
        }
    };

    render() {
        const { stop_auto_close, is_paused } = this.state;
        const { handleClose, title, description, auto_close_timeout, is_auto_close } = this.props;

        return (
            <Popup close={handleClose} onMouseEnterPopup={() => this.setTimer(true)} onMouseLeavePopup={() => this.setTimer(false)}>
                <div className="create-shop-form">
                    <div className="form-popup">
                        <h4 className="popup-title">{title}</h4>
                        <div className="form-popup__info-text">{description}</div>
                        {
                            !stop_auto_close &&
                            is_auto_close &&
                            <p className="popup_timer">
                                Автоматически закроется через <Timer
                                    start={auto_close_timeout}
                                    isShowTime={true}
                                    expired={handleClose}
                                    is_paused={is_paused}
                                />
                            </p>
                        }
                    </div>
                </div>
            </Popup>
        );
    }
}

InfoMessage.propTypes = {
    handleClose: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string,
    auto_close_timeout: PropTypes.number,
    is_auto_close: PropTypes.bool,
};

InfoMessage.defaultProps = {
    description: '',
    auto_close_timeout: 3,
    is_auto_close: false
};
