import React from 'react';
import PropTypes from 'prop-types';

function InfoConnectionSecuryDeal () {
    return(
        <div className="form-popup">
            <h4 className="popup-title">О безопасной сделке</h4>
            <p className="secury-deal-thesis">
                - Возврат денег при невыполнении заказа.<br />
                - Исключает подлог и мошенничество.
            </p>
            <p className="secury-deal-thesis">
                Мы не храним ваши данные. Их обработка полностью защищена процессинговой системой, прошедшей международную сертификацию.
            </p>
            <p className="secury-deal-thesis">
                Арбитражная команда Луны поможет урегулировать спор, проведя независимое расследование.
            </p>

        </div>
    );
}

export default InfoConnectionSecuryDeal;
