import React from 'react';
import Tabs, { Tab } from './Tabs';

export default function ProfileTabs() {
    return (
        <section className="content-header">
            <div className="container">
                <Tabs tabsListClassName="row tab">
                    <Tab
                        tabName="Опубликованные"
                        tabClassName="tab__title col-2 col-tablet-3"
                    >
                        <div className="container">
                            <section className="content-main">
                                <div className="catalog">
                                    <div className="row">
                                        <div className="col-3 col-small-2 col-mobile-2">
                                            <div className="catalog-item no-delivery">
                                                <a className="catalog-item-photo" href="#">
                                                    <span className="photo-count">2 фото</span>
                                                    <img src="/img/catalog-image-example.png" alt="Название товара" />
                                                </a>
                                                <div className="wrapper">
                                                    <div>
                                                        <h2 className="catalog-item-name">Платье</h2>
                                                        <div className="catalog-item-price">9 350</div>
                                                        <div className="catalog-item-date">22 мая 2018</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </Tab>
                    <Tab
                        tabName="Непубликованные"
                        tabClassName="tab__title col-2 col-tablet-3"
                    >
                        <div className="container">
                            <section className="content-main">
                                <div className="catalog">
                                    <div className="row">
                                        <div className="col-3 col-small-2 col-mobile-2">
                                            <div className="catalog-item no-delivery">
                                                <a className="catalog-item-photo" href="#">
                                                    <span className="on-check-badge">
                                                        <span className="on-check-badge__text hd">На проверке</span>
                                                    </span>
                                                    <span className="photo-count">2 фото</span>
                                                    <img src="/img/catalog-image-example.png" alt="Название товара" />
                                                </a>
                                                <div className="wrapper">
                                                    <div>
                                                        <h2 className="catalog-item-name">Телевизор</h2>
                                                        <div className="catalog-item-price">10 000</div>
                                                        <div className="catalog-item-date">22 мая 2018</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-3 col-small-2 col-mobile-2">
                                            <div className="catalog-item">
                                                <a className="catalog-item-photo" href="#">
                                                    <span className="photo-count">2 фото</span>
                                                    <img src="/img/basket-big.png" alt="Название товара" />
                                                </a>
                                                <div className="wrapper">
                                                    <div>
                                                        <h2 className="catalog-item-name">Черновик</h2>
                                                        <div className="catalog-item-price no-price"><span>Не указана</span></div>
                                                        <div className="catalog-item-date">22 мая 2018</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </Tab>
                    <Tab
                        tabName="Архив"
                        tabClassName="tab__title col-2 col-tablet-3"
                    >
                        <div className="container">
                            <section className="content-main">
                                <div className="catalog">
                                    <div className="row">
                                        <div className="col-3 col-small-2 col-mobile-2">
                                            <div className="catalog-item no-delivery">
                                                <a className="catalog-item-photo" href="#">
                                                    <span className="photo-count">2 фото</span>
                                                    <img src="/img/catalog-image-example.png" alt="Название товара" />
                                                </a>
                                                <div className="wrapper">
                                                    <div>
                                                        <h2 className="catalog-item-name">Кирпич</h2>
                                                        <div className="catalog-item-price">11 500</div>
                                                        <div className="catalog-item-date">22 мая 2018</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </Tab>
                </Tabs>
            </div>
        </section>
    );
}
