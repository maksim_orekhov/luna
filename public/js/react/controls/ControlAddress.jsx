import React from 'react';
import PropTypes from 'prop-types';
import ControlSuggest from './ControlSuggest';

function ControlAddress (props) {
    const { cssClassName, isDisabled, onSelectLocation, suggestLoading, suggestAddressList, onChangeHandler, value } = props;

    return(
        <div className={`address-api-control ${suggestLoading ? 'is-loading' : ''}`}>
            <ControlSuggest
                onInputChangeHandler={onChangeHandler}
                onItemSelectHandler={onSelectLocation}
                suggestItems={suggestAddressList.map((addressItem) => addressItem.displayName)}
                isDisabled={isDisabled}
                value={value}
                cssClassName={cssClassName}
                suggestLoading={suggestLoading}
            />
        </div>
    );
}

ControlAddress.propTypes = {
    value: PropTypes.string.isRequired,
    suggestAddressList: PropTypes.array.isRequired,
    cssClassName: PropTypes.string,
    onChangeHandler: PropTypes.func,
    onSelectLocation: PropTypes.func,
    isDisabled: PropTypes.bool,
    mapIsLoaded: PropTypes.bool,
    suggestLoading: PropTypes.bool,
    emptySuggestText: PropTypes.string,
};

ControlAddress.defaultProps = {
    cssClassName: '',
    onChangeHandler: () => {},
    onSelectLocation: () => {},
    isDisabled: false,
    mapIsLoaded: false,
    suggestLoading: false,
    emptySuggestText: 'Ничего не найдено'
};

export default ControlAddress;
