import React from 'react';
import PropTypes from 'prop-types';
import ControlBase from './ControlBase';
import MESSAGES from '../constants/messages';
import VALIDATION_RULES from '../constants/validation_rules';
import ShowError from '../ShowError';
import { textOnlyNumbers } from '../Helpers';

export default class ControlInn extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                min_chars: null,
                max_chars: null,
                is_inn_valid: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                min_chars: `${MESSAGES.VALIDATION_ERRORS.min_chars} - ${VALIDATION_RULES.INN.min_chars}`,
                max_chars: `${MESSAGES.VALIDATION_ERRORS.max_chars} - ${VALIDATION_RULES.INN.max_chars}`,
                is_inn_valid: MESSAGES.VALIDATION_ERRORS.invalid_inn,
                undefined_error: ''
            },
            validation_rules: {
                min_chars: VALIDATION_RULES.INN.min_chars,
                max_chars: VALIDATION_RULES.INN.max_chars
            }
        };
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    handleChangeEvent = (e) => {
        const value = textOnlyNumbers(e.target.value);
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    };

    updateErrors = (value) => {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value),
                min_chars: !this.validateMinCharsLength(value),
                max_chars: !this.validateMaxCharsLength(value),
                is_inn_valid: !this.validateInn(value)
            }
        });
    };

    // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
    checkAllValidations = (value) => {
        this.updateErrors(value);
        return (
            this.validateIsEmpty(value)
            && this.validateMinCharsLength(value)
            && this.validateMaxCharsLength(value)
            && this.validateInn(value)
        );
    };

    render() {
        const { name, label, placeholder, value_prop } = this.props;
        const { error_messages, validation_errors } = this.state;
        return (
            <div className="control-form">
                <label className="label-text">
                    {label}
                </label>
                <input
                    name={name}
                    type="text"
                    onChange={this.handleChangeEvent}
                    onBlur={this.handleChangeEvent}
                    placeholder={placeholder}
                    value={value_prop}
                    maxLength={12}
                    className={this.colourInputField()}
                />
                <ShowError messages={error_messages} existing_errors={validation_errors} />
            </div>
        );
    }
}

ControlInn.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    value_prop: PropTypes.string,
};

ControlInn.defaultProps = {
    placeholder: '',
    value_prop: ''
};