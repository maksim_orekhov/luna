import React from 'react';
import PropTypes from 'prop-types';
import ShowError from '../ShowError.jsx';
import ControlBase from './ControlBase';
import VALIDATION_RULES from '../constants/validation_rules';
import MESSAGES from '../constants/messages';

export default class Login extends ControlBase {
    constructor() {
        super();
        this.state = {
            is_blur: false,
            validation_errors: {
                server_error: false,
                min_chars: null,
                max_words: null,
                email: null
            },
            validation_rules: {
                min_chars: VALIDATION_RULES.LOGIN.min_chars,
                max_words: VALIDATION_RULES.LOGIN.max_words
            },
            error_messages: {
                min_chars: `${MESSAGES.VALIDATION_ERRORS.min_chars} - ${VALIDATION_RULES.LOGIN.min_chars}`,
                max_words: `${MESSAGES.VALIDATION_ERRORS.max_words} - ${VALIDATION_RULES.LOGIN.max_words}`,
                email: MESSAGES.VALIDATION_ERRORS.email_reg_exp_invalid,
                undefined_error: ''
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const { value_prop } = nextProps;

        value_prop && this.validateControl(nextProps);
    }

    handleChangeEvent = (e) => {
        const value = e.target.value;
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;
        handleComponentChange && handleComponentChange(name, value);
        console.log(this.checkAllValidations(value));
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    };

    updateErrors = (value) => {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                min_chars: !this.validateMinCharsLength(value),
                max_words: !this.validateMaxWordsLength(value),
                email: (value.indexOf('@') !== -1) && !this.validateEmail(value)
            }
        });
    };

    // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
    checkAllValidations = (value) => {
        const is_check_email = (value.indexOf('@') !== -1);

        this.updateErrors(value);
        return (
            this.validateMinCharsLength(value)
            && this.validateMaxWordsLength(value)
            && (is_check_email ? this.validateEmail(value) : true)
        );
    };

    handleBlur = () => {
        this.setState({
            is_blur: true
        });
    };

    render() {
        const { value_prop, name } = this.props;
        const { validation_errors, error_messages, is_blur } = this.state;

        return (
            <div className="control-form">
                <input
                    type="text"
                    className={this.colourInputField()}
                    onChange={this.handleChangeEvent}
                    onBlur={this.handleBlur}
                    value={value_prop}
                    name={name}
                    placeholder="Введите email или номер телефона"
                />
                {
                    is_blur &&
                    <ShowError
                        existing_errors={validation_errors}
                        messages={error_messages}
                    />
                }
            </div>
        );
    }
}

Login.propTypes = {
    value_prop: PropTypes.string,
    name: PropTypes.string,
};
