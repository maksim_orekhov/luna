import React, { Component } from 'react';
import PropTypes from 'prop-types';
import LoaderSpinner from '../components/LoaderSpinner';
import classNames from 'classnames';

class ControlSuggest extends Component {
    
    state = {
        isExpanded: false,
        hoveredSuggestIndex: null,
    };

    componentDidMount () {
        document.addEventListener('click', this.handleDocumentClick);
    }

    componentWillReceiveProps (nextProps) {
        const { isExpanded } = this.state;
        const { suggestItems } = this.props;
        if(nextProps.suggestItems === suggestItems && nextProps.suggestItems && isExpanded) {
            this.setState({ hoveredSuggestIndex: null });
        }
    }

    componentWillUnmount () {
        document.removeEventListener('click', this.handleDocumentClick);
    }


    setControlSuggestRef = (node) => {
        this.controlSuggestRef = node;
    };

    handleSuggestMouseEnter = (index) => () => {
        const { hoveredSuggestIndex } = this.state;
        if (hoveredSuggestIndex !== index) {
            this.setState({ hoveredSuggestIndex: index });
        }
    };

    /**
     * Метод для выбора подходящего значения стрелками и нажатием enter
     * @param ev
     * @returns {boolean}
     */
    handleKeyDown = (ev) => {
        const { isExpanded, hoveredSuggestIndex } = this.state;
        const { suggestItems } = this.props;
        if (!isExpanded || suggestItems.length === 0) {
            return true;
        }
        switch (ev.keyCode) {
            // up
            case 38:
                ev.stopPropagation();
                ev.preventDefault();
                this.decrementHoveredSuggestIndex();
                break;
            // down
            case 40:
                ev.stopPropagation();
                ev.preventDefault();
                this.incrementHoveredSuggestIndex();
                break;
            // enter
            case 13:
                if (hoveredSuggestIndex !== null) {
                    this.handleSuggestItemSelect(suggestItems[hoveredSuggestIndex])();
                }
                ev.stopPropagation();
                ev.preventDefault();
                return false;
        }
    };

    handleSuggestInputChange = (ev) => {
        const { onInputChangeHandler } = this.props;
        onInputChangeHandler(ev.target.value);
        this.setState({ isExpanded: true });
    };

    handleSuggestItemSelect = (suggestItem) => () => {
        const { onItemSelectHandler } = this.props;
        onItemSelectHandler(suggestItem);
        this.setState({ isExpanded: false });
    };

    handleDocumentClick = (ev) => {
        const { isExpanded } = this.state;
        if (isExpanded && !this.controlSuggestRef.contains(ev.target)) {
            this.setState({ isExpanded: false });
        }
    };

    incrementHoveredSuggestIndex = () => {
        const { hoveredSuggestIndex } = this.state;
        const { suggestItems } = this.props;
        let newIndex = 0;
        if (hoveredSuggestIndex !== null) {
            if (hoveredSuggestIndex + 1 < suggestItems.length) {
                newIndex = hoveredSuggestIndex + 1;
            }
        }
        this.setState({ hoveredSuggestIndex: newIndex });
    };

    decrementHoveredSuggestIndex = () => {
        const { hoveredSuggestIndex } = this.state;
        const { suggestItems } = this.props;
        let newIndex = suggestItems.length - 1;
        if (hoveredSuggestIndex !== null) {
            if (hoveredSuggestIndex - 1 >= 0) {
                newIndex = hoveredSuggestIndex - 1;
            }
        }
        this.setState({ hoveredSuggestIndex: newIndex });
    };
    
    renderSuggestList = () => {
        const { isExpanded, hoveredSuggestIndex } = this.state;
        const { suggestLoading, suggestItems, emptySuggestText } = this.props;
        if (!isExpanded || suggestLoading) {
            return null;
        }

        if (suggestItems.length === 0) {
            return (
                <div className="suggest-list">
                    <div className="suggest-item">{emptySuggestText}</div>
                </div>
            );
        }

        return (
            <div className="suggest-list">
                {suggestItems.map((suggestItem, index) => {
                    return (
                        <div
                            onMouseEnter={this.handleSuggestMouseEnter(index)}
                            onClick={this.handleSuggestItemSelect(suggestItem)}
                            key={index}
                            className={classNames({
                                'suggest-item': true,
                                'is-hovered': hoveredSuggestIndex === index,
                            })}
                        >
                            {typeof suggestItem === 'object' ? suggestItem.text : suggestItem}
                        </div>
                    );
                })}
            </div>
        );
    };
    
    render(){
        const { isExpanded } = this.state;
        const { value, isDisabled, placeholder, cssClassName, suggestLoading } = this.props;
        return(
            <div ref={this.setControlSuggestRef} className="suggest-field">
                <input
                    type="text"
                    className={`suggest-input ${cssClassName}`}
                    value={value}
                    disabled={isDisabled}
                    placeholder={placeholder}
                    onChange={this.handleSuggestInputChange}
                    onKeyDown={this.handleKeyDown}
                />
                {isExpanded && suggestLoading && <LoaderSpinner cssClassName="input-loader" />}
                {this.renderSuggestList()}
            </div>        
        );    
    }
}

ControlSuggest.propTypes = {
    value: PropTypes.string.isRequired,
    onInputChangeHandler: PropTypes.func.isRequired,
    onItemSelectHandler: PropTypes.func.isRequired,
    suggestItems: PropTypes.arrayOf(PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.shape({
            text: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
        })
    ])).isRequired,
    emptySuggestText: PropTypes.string,
    suggestLoading: PropTypes.bool,
    isDisabled: PropTypes.bool,
    cssClassName: PropTypes.string,
    placeholder: PropTypes.string,
};

ControlSuggest.defaultProps = {
    emptySuggestText: 'Ничего не найдено',
    cssClassName: '',
    placeholder: '',
    suggestLoading: false,
    isDisabled: false,
};

export default ControlSuggest;
