import React from 'react';
import PropTypes from 'prop-types';
import ControlBase from './ControlBase';
import { replaceToTextCapitalize } from '../Helpers';
import MESSAGES from '../constants/messages';
import VALIDATION_RULES from '../constants/validation_rules';
import ShowError from '../ShowError';

export default class ControlLastName extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                cyrillic_only: null,
                min_chars: null,
                max_chars: null,
                min_words: null,
                max_words: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                cyrillic_only: MESSAGES.VALIDATION_ERRORS.cyrillic_only,
                min_chars: `${MESSAGES.VALIDATION_ERRORS.min_chars} - ${VALIDATION_RULES.LAST_NAME.min_chars}`,
                max_chars: `${MESSAGES.VALIDATION_ERRORS.max_chars} - ${VALIDATION_RULES.LAST_NAME.max_chars}`,
                min_words: `${MESSAGES.VALIDATION_ERRORS.min_words} - ${VALIDATION_RULES.LAST_NAME.min_words}`,
                max_words: `${MESSAGES.VALIDATION_ERRORS.max_words} - ${VALIDATION_RULES.LAST_NAME.max_words}`,
                undefined_error: ''
            },
            validation_rules: {
                min_chars: VALIDATION_RULES.LAST_NAME.min_chars,
                max_chars: VALIDATION_RULES.LAST_NAME.max_chars,
                min_words: VALIDATION_RULES.LAST_NAME.min_words,
                max_words: VALIDATION_RULES.LAST_NAME.max_words
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps = (nextProps) => {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    };

    handleChangeEvent = (e) => {
        const value = replaceToTextCapitalize(e.target.value);
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    };

    updateErrors = (value) => {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value),
                min_chars: !this.validateMinCharsLength(value),
                max_chars: !this.validateMaxCharsLength(value),
                min_words: !this.validateMinWordsLength(value),
                max_words: !this.validateMaxWordsLength(value),
                cyrillic_only: !this.validateRusCharacters(value)
            }
        });
    };

    // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
    checkAllValidations = (value) => {
        this.updateErrors(value);
        return (
            this.validateIsEmpty(value)
            && this.validateMinCharsLength(value)
            && this.validateMaxCharsLength(value)
            && this.validateMinWordsLength(value)
            && this.validateMaxWordsLength(value)
            && this.validateRusCharacters(value)
        );
    };

    render() {
        const { name, label, placeholder, value_prop } = this.props;
        const { validation_errors, error_messages } = this.state;

        return (
            <div className="control-form">
                <label className="label-text">
                    {label}
                </label>
                <input
                    name={name}
                    type="text"
                    onChange={this.handleChangeEvent}
                    onBlur={this.handleChangeEvent}
                    placeholder={placeholder}
                    value={value_prop}
                    className={this.colourInputField()}
                />
                <ShowError messages={error_messages} existing_errors={validation_errors} />
            </div>
        );
    }
}

ControlLastName.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    value_prop: PropTypes.string,
};

ControlLastName.defaultProps = {
    placeholder: '',
    value_prop: ''
};