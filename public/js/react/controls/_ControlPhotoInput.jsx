import React from 'react';
import ControlBase from './ControlBase';

export default class _ControlPhotoInput extends ControlBase {
    constructor(props) {
        super(props);
        this.state = {
            file: '',
            imagePreviewUrl: '',
            showOverlay: false,
            lastTarget: null
        };

        this.addPhoto = this.addPhoto.bind(this);
        this.removePhoto = this.removePhoto.bind(this);
    }

    componentDidMount() {
        window.addEventListener('dragenter', this.onDragEnter);
        window.addEventListener('dragleave', this.onDragLeave);
        window.addEventListener('dragover', this.onDragOver);
        window.addEventListener('drop', this.onDrop);
    }

    componentWillUnmount() {
        window.removeEventListener('dragenter', this.onDragEnter);
        window.removeEventListener('dragleave', this.onDragLeave);
        window.removeEventListener('dragover', this.onDragOver);
        window.removeEventListener('drop', this.onDrop);
    }

    addPhoto(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];
        const input_files = e.target.files;

        reader.onload = () => {
            this.setState({
                file: file,
                imagePreviewUrl: reader.result
            });
        };

        reader.readAsDataURL(file);

        if (input_files.length) {
            const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

            handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(input_files));
            handleComponentChange && handleComponentChange(file);
        }
    }

    removePhoto(e) {
        e.preventDefault();
        this.props.removePhoto();
        this.input.value = '';
        this.setState({
            file: '',
            imagePreviewUrl: ''
        });
    }

    onDragEnter = (e) => {
        e.preventDefault();
        this.setState({
            lastTarget: e.target,
            showOverlay: true
        });
    };

    onDragOver = (e) => {
        e.preventDefault();
    };

    onDragLeave = (e) => {
        e.preventDefault();
        if (e.target === this.state.lastTarget) {
            this.setState({
                showOverlay: false
            });
        }
    };

    onDrop = (e) => {
        e.preventDefault();
        const files = e.dataTransfer.files;

        if (this.node.contains(e.target)) {
            let reader = new FileReader();
            let file = e.dataTransfer.files[0];

            reader.onload = () => {
                this.setState({
                    imagePreviewUrl: reader.result
                });
            };

            reader.readAsDataURL(file);
            this.props.handleComponentChange(files[0]);
            this.setState({
                showOverlay: false,
                file: [...this.state.file, ...files]
            });
        }
    };

    render() {
        let { imagePreviewUrl, file } = this.state;
        let imagePreview = null;
        if (imagePreviewUrl) {
            imagePreview = (<img src={imagePreviewUrl} />);
        }

        return (
            <label className="photo-input" ref={node => this.node = node}>
                <input
                    type="file"
                    accept="image/x-png,image/jpeg"
                    className="photo-input__field"
                    onChange={this.addPhoto}
                    ref={input => this.input = input}
                />
                <span className="photo-input__placeholder">
                    <span className="photo-input__placeholder-image">
                        <img src="../../../img/photo-input-bg.svg" alt="" />
                    </span>
                    <span className="photo-input__placeholder-title">Добавить фотографию</span>
                </span>
                {
                    file &&
                    <span className="photo-input__preview is-visible">
                        {imagePreview}
                        <button type="button" className="photo-input__control-button photo-input__control-button_type_remove" onClick={this.removePhoto} />
                        <button type="button" className="photo-input__control-button photo-input__control-button_type_util" />
                    </span>
                }
            </label>
        );
    }
}
