import React from 'react';
import PropTypes from 'prop-types';
import ControlBase from './ControlBase';
import SelectImitation from '../components/SelectImitation';
import MESSAGES from '../constants/messages';

export default class ControlSubCategorySelect extends ControlBase {
    constructor(props) {
        super(props);
        this.state = {
            validation_errors: {
                isEmpty: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                undefined_error: ''
            }
        };

        this.handleChangeEventDataSet = this.handleChangeEventDataSet.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    componentDidUpdate(prevProps) {
        const { subcategories, value_prop, name } = this.props;
        if (!value_prop && subcategories && Array.isArray(subcategories) && subcategories.length) {
            const value = subcategories[0].value;

            if (value) this.handleChangeSelect({value});
        }
    }

    handleChangeSelect = (e) => {
        const { value } = e;
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    };

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value)
            }
        });
    }

    checkAllValidations(value) {
        this.updateErrors(value);
        return this.validateIsEmpty(value);
    }

    render() {
        const {subcategories, label, placeholder, value_prop} = this.props;

        return (
            <div className={`control-form ${this.colourInputField()}`}>
                <SelectImitation
                    options={subcategories}
                    label={label}
                    placeholder={placeholder}
                    isRequired
                    onChangeHandler={this.handleChangeSelect}
                    defaultValue={value_prop}
                    isDisabled={subcategories.length === 0}
                    className={this.colourInputField()}
                />
            </div>
        );
    }
}

ControlSubCategorySelect.propTypes = {
    subcategories: PropTypes.array.isRequired,
    name: PropTypes.string.isRequired,
    handleComponentChange: PropTypes.func.isRequired,
    handleComponentValidation: PropTypes.func,
    value_prop: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]),
    label: PropTypes.string,
    placeholder: PropTypes.string,
};

ControlSubCategorySelect.defaultProps = {
    activeCategoryId: 0,
    label: '',
    placeholder: '',
    handleComponentValidation: () => {},
};