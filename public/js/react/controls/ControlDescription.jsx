import React from 'react';
import ControlBase from './ControlBase';
import ShowError from '../ShowError';
import {replaceRusLetterE} from '../Helpers';
import PropTypes from 'prop-types';
import VALIDATION_RULES from '../constants/validation_rules';
import MESSAGES from '../constants/messages';

export default class ControlDescription extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            description: props.value_prop,
            validation_errors: {
                isEmpty: null,
                max_chars: null,
                min_chars: null,
                // undefined_error: null,
            },
            validation_rules: {
                max_chars: VALIDATION_RULES.ADV_DESCRIPTION.max_chars,
                min_chars: VALIDATION_RULES.ADV_DESCRIPTION.min_chars,
            },
            error_messages: {
                // undefined_error: '',
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                max_chars: `${MESSAGES.VALIDATION_ERRORS.max_chars} - ${VALIDATION_RULES.ADV_DESCRIPTION.max_chars}`,
                min_chars: `${MESSAGES.VALIDATION_ERRORS.min_chars} - ${VALIDATION_RULES.ADV_DESCRIPTION.min_chars}`,
            }
        };
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    handleChangeEvent = (e) => {
        const value = replaceRusLetterE(e.target.value);
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
        this.setState({
            description: value
        });
    };

    updateErrors = (value) => {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value),
                max_chars: !this.validateMaxCharsLength(value),
                min_chars: !this.validateMinCharsLength(value),
            }
        });
    };

    checkAllValidations = (value) => {
        this.updateErrors(value);
        return (
            this.validateIsEmpty(value)
            && this.validateMaxCharsLength(value)
            && this.validateMinCharsLength(value)
        );
    };

    adjustHeight = () => {
        this.textareaRef.style.height = (this.textareaRef.scrollHeight) + 2 + 'px';
    };

    render() {
        const { name, label, placeholder, value_prop } = this.props;
        const { error_messages, validation_errors, description } = this.state;

        return (
            <div className="flexible-row">
                <div className={`control-form`}>
                    <label className="label-text">
                        {label}
                    </label>
                    <textarea
                        className={`new-deal-from__description-field ${this.colourInputField()}`}
                        maxLength={VALIDATION_RULES.ADV_DESCRIPTION.max_chars}
                        name={name}
                        value={value_prop}
                        placeholder={placeholder}
                        onChange={this.handleChangeEvent}
                        onBlur={this.handleChangeEvent}
                        onInput={this.adjustHeight}
                        ref={node => this.textareaRef = node}
                    />
                    <div className="control-form__help-message description-field-help-message">
                        <div>До 3000 символов</div>
                        <div className="control-form__help-message symbols-count">
                            <span className="symbols-count__value">{description.length}</span>/3000
                        </div>
                    </div>
                    <ShowError messages={error_messages} existing_errors={validation_errors} />
                </div>
            </div>
        );
    }
}

ControlDescription.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    value_prop: PropTypes.string,
};

ControlDescription.defaultProps = {
    placeholder: '',
    value_prop: ''
};
