import React from 'react';
import PropTypes from 'prop-types';

export default class ControlSearchHint extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeHint: -1
        };

        this.handleClick = this.handleClick.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        // console.log(nextProps);
        this.updateHints(nextProps);
    }

    handleClick(e) {
        this.props.handleChose(e.target.dataset.value);
    }

    handleKeyPressFromParent(props) {
        let { activeHint } = this.state;

        const { keyPressed, handleChose, hintValueKey, data, hintsCount } = props;
        let maxCount = data.length;

        if (data.length > hintsCount) {
            maxCount = hintsCount;
        }

        switch (keyPressed) {
            case 40: // key down
            // maxCount - 1, т.к. индексация элементов массива начинается с 0
                if (activeHint < maxCount - 1) activeHint++;
                break;
            case 38: // key up
                if (activeHint > -1) activeHint--;
                break;
            case 13: // key enter
                if (activeHint > -1 && activeHint < maxCount) {
                    handleChose(data[activeHint][hintValueKey]);
                }
                break;
            default:
                activeHint = -1;
        }

        this.setState({
            activeHint
        });
    }

    updateHints(props) {
        const { keyPressed } = props;
        if (!(keyPressed === this.props.keyPressed && keyPressed === 13)) { // Чтобы не было рекурсии когда передается enter и функция много раз посылает данные вверх
            this.handleKeyPressFromParent(props);
        }
    }

    render() {
        const { activeHint } = this.state;
        const { hintTitleValue, hintValueKey, data, hintsCount } = this.props;

        return (
            <nav className="SearchHints">
                {
                    data.length ?
                        <ul>
                            {
                                data.map((hint, i) => {
                                    return (
                                        <li
                                            className={i === activeHint ? 'active' : ''}
                                            data-value={hint[hintValueKey]}
                                            key={hint[hintValueKey]}
                                            onMouseDown={this.handleClick}
                                            tabIndex={1}
                                        >
                                            {hint[hintTitleValue]}
                                        </li>
                                    );
                                }).slice(0, hintsCount)
                            }
                        </ul>
                        :
                        void(0)
                }
            </nav>
        );
    }
}

ControlSearchHint.propTypes = {
    hintTitleValue: PropTypes.string.isRequired,
    hintValueKey: PropTypes.string.isRequired,
    keyPressed: PropTypes.number.isRequired,
    handleChose: PropTypes.func.isRequired,
    hintsCount: PropTypes.number,
    data: PropTypes.array,
};

// Определение значений по умолчанию для props:
ControlSearchHint.defaultProps = {
    hintsCount: 5,
    data: []
};
