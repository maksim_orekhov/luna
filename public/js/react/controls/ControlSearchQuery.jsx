import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ControlSuggest from './ControlSuggest';
import { getSearchResult } from '../actions';
import { setUrlParameter } from '../Helpers';

class ControlSearchQuery extends Component {

    state = {
        isLoading: false
    };

    componentWillReceiveProps (nextProps) {
        const { isLoading } = this.state;
        if (isLoading && !nextProps.search.searchIsFetching && Array.isArray(nextProps.search.results[nextProps.value])) {
            this.setState({ isLoading: false });
        }
    }

    componentWillUnmount () {
        this.suggestRequestTimeOut && clearTimeout(this.suggestRequestTimeOut);
    }

    handleSuggestChange = (value) => {
        const { onChange, name, sendQuery, search, only_fabricator } = this.props;
        let searchURL = this.props.searchURL;

        clearTimeout(this.suggestRequestTimeOut);
        onChange({ target: {name, value} });
        if (value.length === 0 || typeof search.results[value] !== 'undefined') {
            return true;
        }
        this.setState({ isLoading: true });
        // запускаем с таймаутом, чтобы на каждое нажатие не слалось много запросов подряд
        this.suggestRequestTimeOut = setTimeout(() => {
            // Отправка на сервер
            if (only_fabricator) {
                searchURL = setUrlParameter(searchURL, 'fabricator', only_fabricator);
            }

            sendQuery({ url: searchURL, query: value });
        }, 400);
    };

    handleSuggestSelect = (data) => {
        const { redirectHandler, only_fabricator } = this.props;
        let  url = `${data.url}?q=${data.arr_phrase.phrase}`;

        if (only_fabricator) {
            url = setUrlParameter(url, 'fabricator', only_fabricator);
        }

        redirectHandler(url);
    };

    getSuggestItems = () => {
        const { search, value } = this.props;
        if (value.length === 0 || typeof search.results[value] === 'undefined') {
            return [];
        }

        return search.results[value].map((searchResult) => {
            const { arr_phrase, type, url, section} = searchResult;
            let suugestQuery =  [];
            if (arr_phrase.pre_word !== null) {
                suugestQuery.push(<span className="suggest__not-matched" key={arr_phrase.pre_word}>{arr_phrase.pre_word}</span>);
            }
            suugestQuery.push(<span className="suggest__matched" key={arr_phrase.found_word}>{arr_phrase.found_word}</span>);
            if (arr_phrase.post_word !== null) {
                suugestQuery.push(<span className="suggest__not-matched" key={arr_phrase.post_word}>{arr_phrase.post_word}</span>);
            }
            const text = (
                <div className="suggest-item__big">
                    <span>{suugestQuery}</span>
                    { type.toLowerCase() === 'section' && <span className="suggest-hint">{section}</span> }
                </div>
            );
            return {
                arr_phrase,
                type,
                url,
                section,
                text,
            };
        });
    };

    render () {
        const { value, placeholder } = this.props;
        const { isLoading } = this.state;
        return(
            <ControlSuggest
                value={value}
                onInputChangeHandler={this.handleSuggestChange}
                onItemSelectHandler={this.handleSuggestSelect}
                suggestItems={this.getSuggestItems()}
                suggestLoading={isLoading}
                cssClassName="input-search"
                placeholder={placeholder}
            />
        );
    }
}

ControlSearchQuery.propTypes = {
    placeholder: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    redirectHandler: PropTypes.func,
    searchURL: PropTypes.string.isRequired,
    value: PropTypes.string,
    // From redux store
    search: PropTypes.object.isRequired,
    // Action Creator
    sendQuery: PropTypes.func.isRequired,
    only_fabricator: PropTypes.bool,
};

ControlSearchQuery.defaultProps = {
    value: '',
    redirectHandler: () => {},
    only_fabricator: false,
};

const mapStateToProps = (state) => {
    return {
        search: state.search
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        sendQuery: bindActionCreators(getSearchResult, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ControlSearchQuery);
