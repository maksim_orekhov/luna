import React from 'react';
import PropTypes from 'prop-types';
import ControlBase from './ControlBase';
import SelectImitation from '../components/SelectImitation';
import MESSAGES from '../constants/messages';

export default class ControlCategorySelect extends ControlBase {
    constructor(props) {
        super(props);
        this.state = {
            validation_errors: {
                isEmpty: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                undefined_error: ''
            }
        };

        this.handleChangeEventDataSet = this.handleChangeEventDataSet.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    handleChangeSelect = (e) => {
        const { value } = e;
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    };

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value)
            }
        });
    }

    checkAllValidations(value) {
        this.updateErrors(value);
        return this.validateIsEmpty(value);
    }

    render() {
        const {categories, label, placeholder, value_prop} = this.props;
        return (
            <div className={`control-form ${this.colourInputField()}`}>
                <SelectImitation
                    options={categories}
                    label={label}
                    placeholder={placeholder}
                    isRequired
                    onChangeHandler={this.handleChangeSelect}
                    defaultValue={value_prop}
                    isDisabled={categories.length === 0}
                    className={this.colourInputField()}
                />
            </div>
        );
    }
}

ControlCategorySelect.propTypes = {
    categories: PropTypes.array.isRequired,
    name: PropTypes.string.isRequired,
    handleComponentChange: PropTypes.func.isRequired,
    handleComponentValidation: PropTypes.func,
    value_prop: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]),
    label: PropTypes.string,
    placeholder: PropTypes.string,
};

ControlCategorySelect.defaultProps = {
    activeCategoryId: 0,
    label: '',
    placeholder: '',
    handleComponentValidation: () => {},
};