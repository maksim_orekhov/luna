import React from 'react';
import PropTypes from 'prop-types';
import ControlBase from './ControlBase';
import PhotoInput from './ControlPhotoInput';

export default class ControlPhotoInputsList extends ControlBase {
    constructor(props) {
        super(props);
        this.state = {
            photos_number: [],
            photos: []
        };

        this.addNewPhoto = this.addNewPhoto.bind(this);
    }

    componentDidMount() {
        const photos_number = new Array(this.props.photos_number).fill(0);
        this.setState({
            photos_number
        });
    }

    // добавление нового блока с фото, если понадобится добавлять больше 4х изображений
    addNewPhoto(e) {
        e.preventDefault();
        let newPhoto = this.state.photos_number + 1;
        this.setState({
            photos_number: [...this.state.photos_number, newPhoto]
        });
    }

    // @todo валидация размера фото
    handleChangePhotos = (index) => (value) => {
        const { handleFileSelect } = this.props;
        const photos = [...this.state.photos];
        photos[index] = value;
        handleFileSelect(value, index);
        this.setState({ photos });
    };

    handleRotateClick = (imageNumber) => () => {
        const { handleImageRotate } = this.props;
        handleImageRotate(imageNumber, 90);
    };

    render() {
        const { photos_number } = this.state;
        const { name, images } = this.props;
        return (
            <div className="flexible-row">
                <div className="control-form">
                    <label className="label-text">
                        Фотографии:
                    </label>
                    {/* <a href="#" className="new-deal-from__add-image-link"><span>Добавить изображение</span></a> */}
                    <div className="photo-inputs-wrapper">
                        <div className="photo-inputs">
                            {photos_number.map((photo, i) =>
                                <PhotoInput
                                    name={name}
                                    handleComponentChange={this.handleChangePhotos(i)}
                                    handleImageRotate={this.handleRotateClick(i)}
                                    key={i}
                                    photo={images[i]}
                                />
                            )}
                        </div>
                    </div>
                    <div className="control-form__help-message">
                        Выберите <span className="hm">или перетащите</span> до 4 фотографий в формате JPG или PNG.
                    </div>
                    <div className="control-form__help-message">
                        Максимальный размер фото — 20MB.
                    </div>
                </div>
            </div>
        );
    }
}

ControlPhotoInputsList.propTypes = {
    images: PropTypes.array,
    handleFileSelect: PropTypes.func,
    handleImageRotate: PropTypes.func,
};
ControlPhotoInputsList.defaultProps = {
    images: [],
    handleFileSelect: () => {},
    handleImageRotate: () => {},
};
