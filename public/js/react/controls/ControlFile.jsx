import React from 'react';
import PropTypes from 'prop-types';
import ControlBase from './ControlBase';

export default class ControlFile extends ControlBase {
    constructor(props) {
        super(props);
        this.state = {
            files: [],
            showOverlay: false,
            lastTarget: null
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
        this.deleteFile = this.deleteFile.bind(this);
    }

    componentWillMount() {
        const { files } = this.props;

        if (files.length) {
            this.setState({
                files
            });
        }
    }

    componentDidMount() {
        window.addEventListener('dragenter', this.onDragEnter);
        window.addEventListener('dragleave', this.onDragLeave);
        window.addEventListener('dragover', this.onDragOver);
        window.addEventListener('drop', this.onDrop);
    }

    componentWillUnmount() {
        window.removeEventListener('dragenter', this.onDragEnter);
        window.removeEventListener('dragleave', this.onDragLeave);
        window.removeEventListener('dragover', this.onDragOver);
        window.removeEventListener('drop', this.onDrop);
    }

    validateIsEmpty(value) {
        return !!value.length;
    }

    handleChangeEvent(e) {
        const file_api = !!(window.File && window.FileReader && window.FileList && window.Blob);
        if (file_api) {
            const input_files = e.target.files;

            if (input_files.length) {
                const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

                const files = [...this.state.files, ...input_files];

                handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(input_files));
                handleComponentChange && handleComponentChange(name, files);

                this.setState({
                    files
                });
            }
        }
    }

    deleteFile(e) {
        const file_index = e.target.dataset.value;
        const files = [...this.state.files];

        files.splice(file_index, 1);

        const { name, handleComponentChange } = this.props;

        handleComponentChange && handleComponentChange(name, files);

        this.setState({
            files
        });
    }

    onDragEnter = (e) => {
        e.preventDefault();
        if (this.node.contains(e.target)) {
            this.setState({
                lastTarget: e.target,
                showOverlay: true
            });
        }
    };

    onDragOver = (e) => {
        e.preventDefault();
    };

    onDragLeave = (e) => {
        e.preventDefault();
        if (e.target === this.state.lastTarget) {
            this.setState({
                showOverlay: false
            });
        }
    };

    onDrop = (e) => {
        e.preventDefault();
        const files = e.dataTransfer.files;

        if (this.node.contains(e.target)) {
            this.setState({
                showOverlay: false,
                files: [...this.state.files, ...files]
            }, () => this.props.handleComponentChange('files', this.state.files));
        }
    };

    render() {
        const { name, label, hint } = this.props;
        const { files, showOverlay } = this.state;
        return (
            <div className="control-form file-input-wrapper" ref={node => this.node = node}>
                <div className="label-text">
                    {label}
                </div>
                <label className={`file-input ${showOverlay ? 'file-input-active' : ''}`}>
                    <input
                        name={name}
                        id="control-touch-file"
                        type="file"
                        onChange={this.handleChangeEvent}
                        ref={input => this.input = input}
                        multiple={true}
                    />
                    <span className="file-input__clip-icon" />
                    <span className="file-input__label">Прикрепить файл</span>
                </label>
                {
                    files.length > 0 &&
                    <div className="files-list">
                        {
                            files.map((file, i) => {
                                return (
                                    <div className="file-item" key={i}>
                                        <a href={file.path} target="_blank">{file.name}</a>
                                        <span style={{float: 'right'}} onClick={this.deleteFile} data-value={i} className="trash-icon" />
                                    </div>
                                );
                            })
                        }
                    </div>
                }
                {
                    hint &&
                        <div>
                            <div className="control-form__help-message mobile">
                                Выберите или перетащите файл.
                            </div>
                            <div className="control-form__help-message">
                                Максимальный размер файла — 50MB.
                            </div>
                        </div>
                }
            </div>
        );
    }
}

ControlFile.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    hint: PropTypes.bool
};