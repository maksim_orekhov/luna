import React from 'react';
import PropTypes from 'prop-types';
import ControlBase from './ControlBase';
import ShowError from '../ShowError';
import MESSAGES from '../constants/messages';
import { textOnlyNumbers } from '../Helpers';

export default class ControlQuantity extends ControlBase {

    constructor(props) {
        super(props);
        this.state = {
            value: props.defaultValue,
            validation_errors: {
                isEmpty: null,
                max_value: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                max_value: MESSAGES.VALIDATION_ERRORS.amount_max_value,
                undefined_error: ''
            },
            hints: {
                base_hint: true
            },
            hint_messages: {
                base_hint: MESSAGES.HINTS.amount
            }
        };
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    // componentWillReceiveProps. В случае, если мы прокидываем значение value через props (свойство valueProps), то необходимо выставить в state нужные состояния
    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    updateErrors = (value) => {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                max_value: !this.validateIsLesserOrEqual(value, this.props.max_quantity),
                isEmpty: !this.validateIsEmpty(value)
            }
        });
    };

    // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
    checkAllValidations = (value) => {
        this.updateErrors(value);
        return (
            this.validateIsEmpty(value) &&
            this.validateIsLesserOrEqual(value, this.props.max_quantity)
        );
    };

    handleChangeEvent = (e) => {
        const value = textOnlyNumbers(e.target.value);
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
        this.setState({
            value
        });
    };

    handleSafeDeal = (e) => {
        const value = e.target.checked ? 1 : 0;
        this.props.handleSafeDeal(value);
    };

    render() {
        const { name, label } = this.props;
        const { error_messages, validation_errors, value } = this.state;

        return(
            <div className="control-form flexible-row__child">
                <label className="label-text">
                    {label}
                </label>
                <div className="quantity-field">
                    <input
                        type="text"
                        className={`input-quantity ${this.colourInputField()}`}
                        name={name}
                        value={value}
                        onChange={this.handleChangeEvent}
                        onBlur={this.handleChangeEvent}
                        ref={input => this.input = input}
                    />
                </div>
                <ShowError messages={error_messages} existing_errors={validation_errors} />
            </div>
        );
    }
}

ControlQuantity.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    defaultValue: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ])
};
