import React from 'react';
import PropTypes from 'prop-types';
import MESSAGES from '../constants/messages';
import ShowError from '../ShowError';
import YandexMap from '../components/YandexMap';
import ControlAddress from './ControlAddress';

export default class ControlLocation extends React.Component {
    constructor(props) {
        super(props);

        this.suggestRequestTimeOut = null;

        this.state = {
            mapIsLoaded: false,
            validation_errors: {
                isEmpty: null,
                undefined_error: false,
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                undefined_error: ''
            },
            formattedAddress: '',
            latitude: props.defaultLatitude,
            longitude: props.defaultLongitude,
            addressFieldIsDisabled: true,
            suggestAddressList: [],
            suggestIsLoading: false,
        };
    }

    componentDidUpdate (prevProps, prevState) {
        const { longitude, latitude, mapIsLoaded } = this.state;
        if (prevState.longitude !== longitude || prevState.latitude !== latitude || prevState.mapIsLoaded !== mapIsLoaded) {
            this.geocodeCoordinatesToAddress();
        }
    }

    componentWillUnmount () {
        this.suggestRequestTimeOut && clearTimeout(this.suggestRequestTimeOut);
    }

    handleMapReady = () => {
        this.setState({ mapIsLoaded: true });
    };

    handleMapActionEnd = ({ latitude, longitude }) => {
        this.setState({ latitude, longitude });
    };

    /**
     * Обработчик изменения значения в инпуте адреса
     * @param address
     */
    handleLocationChange = (address) => {
        clearTimeout(this.suggestRequestTimeOut);
        this.setState({
            formattedAddress: address,
            suggestIsLoading: true
        });
        // запускаем с таймаутом, чтобы на каждое нажатие не слалось много запросов подряд
        this.suggestRequestTimeOut = setTimeout(() => {
            this.getSuggestAddressList(address);
        }, 500);
    };

    /**
     * Выбор города из выпадабщей подсказки
     * @param address
     */
    handleSelectLocation = (address) => {
        this.setState({
            formattedAddress: address
        });
        this.getGeocoder({ address }).then((response) => {
            const geoObject = response.GeoObjectCollection.featureMember[0];
            if (!geoObject) {
                return false;
            }
            const [longitude, latitude] = geoObject.GeoObject.metaDataProperty.GeocoderMetaData
                .InternalToponymInfo.Point.coordinates;
            this.setState({ latitude, longitude });
        });
    };

    /**
     * Получает список подходящих исправленых яндексом адресов
     * @param address
     * @returns {boolean}
     */
    getSuggestAddressList = (address) => {
        const { mapIsLoaded } = this.state;
        if (!mapIsLoaded) {
            return false;
        }
        window.ymaps.suggest(address).then((response) => {
            this.setState({
                suggestAddressList: response,
                suggestIsLoading: false,
            });
        });
    };

    /**
     * Геокодирование по ширине/долготе, устанавливает адрес
     * @returns {boolean}
     */
    geocodeCoordinatesToAddress = () => {
        const { latitude, longitude, mapIsLoaded } = this.state;
        const { handleComponentChange } = this.props;
        if (!mapIsLoaded) {
            return false;
        }
        const geocoder = this.getGeocoder({ latitude, longitude });
        geocoder.then((response) => {
            if (!Array.isArray(response.GeoObjectCollection.featureMember) || response.GeoObjectCollection.featureMember.length === 0) {
                return false;
            }
            const geocoderMetaData = response.GeoObjectCollection.featureMember[0].GeoObject.metaDataProperty.GeocoderMetaData;
            const address = this.getFullAddressFromGeocoderMetaData(geocoderMetaData);
            this.setState({
                formattedAddress: geocoderMetaData.Address.formatted,
                addressFieldIsDisabled: false,
                suggestAddressList: [],
            }, () => {
                handleComponentChange({ ...address, longitude, latitude });
            });
        });
    };

    /**
     * Возвращает промис геокодирования яндекса по координатам или по адресу
     * @param latitude - широта
     * @param longitude - долгота
     * @param address - адрес
     * @returns {*}
     */
    getGeocoder = ({ latitude, longitude, address }) => {
        return window.ymaps.geocode(address && address.length ? address : [latitude, longitude], { json: true });
    };

    getFullAddressFromGeocoderMetaData = (geocoderMetaData) => {
        const fullAddress = {};
        const _ = geocoderMetaData.AddressDetails.Country;
        try {
            fullAddress.country = _.CountryName;
            fullAddress.region = _.AdministrativeArea.AdministrativeAreaName;
            if (_.AdministrativeArea.SubAdministrativeArea) {
                fullAddress.city = _.AdministrativeArea.SubAdministrativeArea.Locality.LocalityName;
                if (_.AdministrativeArea.SubAdministrativeArea.Locality.Thoroughfare) {
                    fullAddress.street = _.AdministrativeArea.SubAdministrativeArea.Locality.Thoroughfare.ThoroughfareName;
                    if (_.AdministrativeArea.SubAdministrativeArea.Locality.Thoroughfare.Premise) {
                        fullAddress.house = _.AdministrativeArea.SubAdministrativeArea.Locality.Thoroughfare.Premise.PremiseNumber;
                        if (_.AdministrativeArea.SubAdministrativeArea.Locality.Thoroughfare.Premise.PostalCode) {
                            fullAddress.index = _.AdministrativeArea.SubAdministrativeArea.Locality.Thoroughfare.Premise.PostalCode.PostalCodeNumber;
                        }
                    }
                }
            } else {
                fullAddress.city = _.AdministrativeArea.Locality.LocalityName;
                if (_.AdministrativeArea.Locality.Thoroughfare) {
                    fullAddress.street = _.AdministrativeArea.Locality.Thoroughfare.ThoroughfareName;
                    if (_.AdministrativeArea.Locality.Thoroughfare.Premise) {
                        fullAddress.house = _.AdministrativeArea.Locality.Thoroughfare.Premise.PremiseNumber;
                        if (_.AdministrativeArea.Locality.Thoroughfare.Premise.PostalCode) {
                            fullAddress.index = _.AdministrativeArea.Locality.Thoroughfare.Premise.PostalCode.PostalCodeNumber;
                        }
                    }
                }
            }
        } catch (error){
            console.error(error);
        }

        return fullAddress;
    };

    render() {
        const { label, withGeoLocation } = this.props;
        const {
            error_messages, validation_errors, latitude, longitude,
            formattedAddress, suggestAddressList, addressFieldIsDisabled,
            suggestIsLoading
        } = this.state;
        return (
            <div className="control-form">
                <div className="label-text">
                    {label}
                </div>
                <ControlAddress
                    isDisabled={addressFieldIsDisabled}
                    onChangeHandler={this.handleLocationChange}
                    onSelectLocation={this.handleSelectLocation}
                    value={formattedAddress}
                    suggestAddressList={suggestAddressList}
                    suggestLoading={suggestIsLoading}
                />
                <ShowError messages={error_messages} existing_errors={validation_errors} />
                <YandexMap
                    placeMarkOnCenter
                    controls={['geolocationControl', 'zoomControl']}
                    onMapLoad={this.handleMapReady}
                    onSetPosition={this.handleMapActionEnd}
                    latitude={latitude}
                    longitude={longitude}
                    zoom={16}
                    geoLocation={withGeoLocation}
                />
            </div>
        );
    }
}

ControlLocation.propTypes = {
    label: PropTypes.string,
    defaultLatitude: PropTypes.number,
    defaultLongitude: PropTypes.number,
    withGeoLocation: PropTypes.bool,
    handleComponentChange: PropTypes.func,
};
ControlLocation.defaultProps = {
    label: '',
    defaultLatitude: 55.753215,
    defaultLongitude: 37.622504,
    withGeoLocation: true,
    handleComponentChange: () => {},
};
