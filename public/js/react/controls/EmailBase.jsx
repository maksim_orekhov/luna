import ControlBase from './ControlBase';


export default class EmailBase extends ControlBase {

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);
        value_prop && this.validateControl(nextProps);
    }

    handleChangeEvent(e) {
        const value = e.target.value;
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
        this.hideHint();
        this.checkValidationErrors();
    }

    setHint() {
        this.setState({
            hints: {
                email_rules: true
            }
        });
    }

    hideHint() {
        this.setState({
            hints: {
                email_rules: false
            }
        });
    }

    fetchConnectionSuccess(data) {
        console.log(data);
        if(data.status === 'SUCCESS'){
            this.setState({
                validation_errors: {
                    ...this.state.validation_errors,
                    email_unavailable: false
                }
            });
        }
        else if(data.status === 'ERROR'){
            this.setState({
                validation_errors: {
                    ...this.state.validation_errors,
                    email_unavailable: true
                }
            });
        }
        else{
            this.phoneIsValid(false);
        }
        return data;
    }

    handleFocus() {
        this.setState({
            onFocus : 'has-focus'
        });
        this.showHint();
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value),
                email_reg_exp_invalid: !this.validateEmail(value)
            }
        });
    }

    // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
    checkAllValidations(value) {
        this.updateErrors(value);
        return (
            this.validateIsEmpty(value)
            && this.validateEmail(value)
        );
    }

    checkValidationErrors() {
        Object.values(this.state.validation_errors).some(() => {
            this.setState({
                show_hint: false
            }, () => {
                Object.values(this.state.validation_errors).every((error) => {
                    error === false &&
                    this.setState({
                        show_hint: true
                    });
                });
            });
        });
    }

    handleBlur(e) {
        let ajaxCheckUniqueOptions = {
            'url': '/register/check/email',
            'field': 'email'
        };

        if (!this.props.not_check_unique) {
            this.fetchValidation(e.target.value, ajaxCheckUniqueOptions)
                .then(
                    data => this.fetchConnectionSuccess(data)
                );
        }
        this.checkAllValidations(e.target.value);
        this.checkValidationErrors();
    }
}
