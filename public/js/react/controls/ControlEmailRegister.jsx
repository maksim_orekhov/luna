import React from 'react';
import PropTypes from 'prop-types';
import ShowErrorRegistration from '../ShowError.jsx';
// import ShowHint from '../../messages/ShowHint.jsx';
import EmailBase from './EmailBase';
import MESSAGES from '../constants/messages';


export default class ControlEmailRegister extends EmailBase {
    constructor() {
        super();
        this.state = {
            disable: false,
            is_blur: false,
            validation_errors:{
                email_reg_exp_invalid:      null,
                email_unavailable:    false,
                connection_is_lost:   false,
                email:                false,
                undefined_error:      false,
            },
            onFocus: ' ',
            hints:{
                email_rules: false
            },
            messages:{
                errors:{
                    email_reg_exp_invalid: MESSAGES.VALIDATION_ERRORS.email_reg_exp_invalid,
                    email_unavailable: MESSAGES.VALIDATION_ERRORS.email_unavailable,
                    undefined_error: ''
                },
                hints: {
                    email_rules:{
                        header: 'Формат ввода E-mail:',
                        text:   'mail@example.com'
                    }
                }
            }
        };

        this.handleFocus = this.handleFocus.bind(this);
    }

    componentWillMount() {
        let invitationEmail = this.props.invitationEmail;
        let formEmail = this.props.value_prop;
        // invitation email
        if (invitationEmail && invitationEmail !== null && invitationEmail.length > 0){
            this.setState({
                disable: true,
                validation_errors: {
                    ...this.state.validation_errors,
                    email_reg_exp_invalid: false
                }
            });
        }
        if(formEmail && formEmail !== null && formEmail.length > 0){
            const { name, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;
            handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(this.props.value_prop));
        }
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors } = nextProps;
        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);
    }

    handleBlur = (e) => {
        this.setState({ is_blur: true});
        this.hideHint();
        let ajaxCheckUniqueOptions = {
            'url': '/register/check/email',
            'field': 'email'
        };

        if (!this.props.not_check_unique) {
            this.fetchValidation(e.target.value, ajaxCheckUniqueOptions)
                .then(
                    data => this.fetchConnectionSuccess(data)
                );
        }
    };

    handleChangeEvent = (e) => {
        const value = e.target.value;
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;
        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
        this.hideHint();
    };

    render() {
        const { validation_errors, messages, disable, is_blur } = this.state;
        const { value_prop, name } = this.props;
        return (
            <div className="control-form">
                <input
                    className={this.colourInputField()}
                    id="login-email"
                    value={value_prop}
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                    onChange={this.handleChangeEvent}
                    disabled={disable}
                    name={name}
                    placeholder="Введите email"
                />
                {
                    is_blur &&
                    <ShowErrorRegistration
                        existing_errors={validation_errors}
                        messages={messages.errors}
                    />
                }
                {/* <ShowHint*/}
                {/* hints={hints}*/}
                {/* messages={messages.hints}*/}
                {/* />*/}
            </div>
        );
    }
}

ControlEmailRegister.propTypes = {
    name: PropTypes.string.isRequired,
    value_prop: PropTypes.string,
};

ControlEmailRegister.defaultProps = {
    value_prop: ''
};
