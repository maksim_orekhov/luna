import React from 'react';
import PropTypes from 'prop-types';
import ControlBase from './ControlBase';
import MESSAGES from '../constants/messages';
import ShowError from '../ShowError';
import VALIDATION_RULES from '../constants/validation_rules';

export default class ControlName extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                undefined_error: false,
                min_chars: false,
                max_chars: false,
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                min_chars: `${MESSAGES.VALIDATION_ERRORS.min_chars} - ${VALIDATION_RULES.ADVERT_USER_NAME.min_chars}`,
                max_chars: `${MESSAGES.VALIDATION_ERRORS.max_chars} - ${VALIDATION_RULES.ADVERT_USER_NAME.max_chars}`,
                undefined_error: ''
            },
            validation_rules: {
                min_chars: VALIDATION_RULES.ADVERT_USER_NAME.min_chars,
                max_chars: VALIDATION_RULES.ADVERT_USER_NAME.max_chars
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    handleChangeEvent = (e) => {
        const value = e.target.value;
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    };

    updateErrors = (value) => {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value),
                min_chars: !this.validateMinCharsLength(value),
                max_chars: !this.validateMaxCharsLength(value),
            }
        });
    };

    checkAllValidations = (value) => {
        this.updateErrors(value);
        return (
            this.validateIsEmpty(value) && this.validateMinCharsLength(value)  && this.validateMaxCharsLength(value)
        );
    };

    render() {
        const { name, label, placeholder, value_prop, is_disable } = this.props;
        const { error_messages, validation_errors } = this.state;
        return (
            <div className="control-form">
                <label className="label-text">
                    {label}
                </label>
                <input
                    name={name}
                    type="text"
                    onChange={this.handleChangeEvent}
                    placeholder={placeholder}
                    value={value_prop}
                    onBlur={this.handleChangeEvent}
                    disabled={is_disable}
                    className={this.colourInputField()}
                />
                <ShowError messages={error_messages} existing_errors={validation_errors} />
            </div>
        );
    }
}

ControlName.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    value_prop: PropTypes.string,
};

ControlName.defaultProps = {
    label: '',
    placeholder: '',
    value_prop: ''
};