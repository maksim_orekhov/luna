import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getReCaptchaSiteKey, getReCaptchaSrc } from '../Helpers';

let loadCaptcha;

export default class ControlCaptcha extends Component {

    componentDidMount() {
        this.initCaptcha();
    }

    setCaptchaRef = node => this.captchaRef = node;

    initCaptcha = () => {
        const { onChange, onExpired, setWidgetId } = this.props;
        if (!loadCaptcha) {
            loadCaptcha = new Promise(resolve => {
                const captchaScript = document.createElement('script');
                captchaScript.src = getReCaptchaSrc();
                document.getElementsByTagName('head')[0].appendChild(captchaScript);
                captchaScript.onload = () => window.grecaptcha.ready(() => {
                    resolve(window.grecaptcha);
                });
            });
        }
        loadCaptcha.then((grecaptcha) => {
            const widget = grecaptcha.render(this.captchaRef, {
                'sitekey' : getReCaptchaSiteKey(),
                'callback': onChange,
                'expired-callback': onExpired,
            });
            setWidgetId(widget);
        });
    };

    render () {
        return(
            <div className="captcha-wrapper">
                <div ref={this.setCaptchaRef} />
                <div className="captcha-overlay captcha-overlay_position-top" />
                <div className="captcha-overlay captcha-overlay_position-right" />
                <div className="captcha-overlay captcha-overlay_position-bottom" />
                <div className="captcha-overlay captcha-overlay_position-left" />
                <div className="captcha-image" />
                <div className="captcha-links">
                    <a href="https://www.google.com/intl/ru/policies/privacy/" target="_blank">Конфиденциальность</a>
                    <span aria-hidden="true" role="presentation"> - </span>
                    <a href="https://www.google.com/intl/ru/policies/terms/" target="_blank">
                        Условия использования
                    </a>
                </div>
            </div>
        );
    }
}

ControlCaptcha.propTypes = {
    onChange: PropTypes.func.isRequired,
    onExpired: PropTypes.func,
    setWidgetId: PropTypes.func,
};
ControlCaptcha.defaultProps = {
    onExpired: () => {},
    setWidgetId: () => {},
};