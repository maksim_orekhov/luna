import React from 'react';
import PropTypes from 'prop-types';
import InputDateMask from './InputDateMask';
import MESSAGES from '../constants/messages';
import VALIDATION_RULES from '../constants/validation_rules';

export default class ControlBirthDate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            min_year_old: VALIDATION_RULES.BIRTH_DATE.min_year_old, // минимальный возраст
            max_birthday_year: null // минимальный возраст рождения, год формата YYYY, например 1994, получаем в getMinYearBirthday
        };
    }

    componentWillMount() {
        this.getMaxYearBirthday();
    }

    getMaxYearBirthday = () => {
        this.setState({
            max_birthday_year: (new Date()).getFullYear() - this.state.min_year_old
        });
    };

    render() {
        const { max_birthday_year, min_year_old } = this.state;
        const { handleComponentChange, birth_date, handleComponentValidation, form_control_server_errors, name, label, placeholder } = this.props;
        return (
            <InputDateMask
                date_format="dd.mm.yyyy"
                name={name}
                label={label}
                placeholder={placeholder}
                handleComponentChange={handleComponentChange}
                handleComponentValidation={handleComponentValidation}
                validation={{
                    isEmpty: true,
                    max_year: max_birthday_year,
                    max_year_error_message: `${MESSAGES.VALIDATION_ERRORS.min_year_old} ${min_year_old}`
                }}
                value_prop={birth_date}
                form_control_server_errors={form_control_server_errors}
            />
        );
    }
}

ControlBirthDate.propTypes = {
    handleComponentChange: PropTypes.func,
    birth_date: PropTypes.string,
    handleComponentValidation: PropTypes.func,
    form_control_server_errors: PropTypes.object,
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
};

ControlBirthDate.defaultProps = {
    handleComponentChange: () => {},
    birth_date: '',
    handleComponentValidation: () => {},
    form_control_server_errors: {},
    placeholder: ''
};