import React from 'react';
import PropTypes from 'prop-types';
import ShowError from '../ShowError.jsx';
import VALIDATION_RULES from '../constants/validation_rules';
import ControlBase from './ControlBase';
import MESSAGES from '../constants/messages';

export default class PasswordInput extends ControlBase {
    constructor() {
        super();
        this.state = {
            is_blur: false,
            validation_errors: {
                min_chars: null,
                server_error: false
            },
            validation_rules: {
                min_chars: VALIDATION_RULES.PASSWORD.min_chars
            },
            error_messages: {
                min_chars: `${MESSAGES.VALIDATION_ERRORS.min_chars} - ${VALIDATION_RULES.PASSWORD.min_chars}`,
                undefined_error: ''
            },
            type_input: 'password',
            isShowPassword: false
        };

        this.showPassword = this.showPassword.bind(this);
        this.handleChangeEvent = this.handleChangeEvent.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
    }

    handleChangeEvent = (e) => {
        const value = e.target.value;
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;
        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    };

    updateErrors = (value) => {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                min_chars: !this.validateMinCharsLength(value)
            }
        });
    };

    // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
    checkAllValidations = (value) => {
        this.updateErrors(value);
        return (
            this.validateMinCharsLength(value)
        );
    };

    showPassword = () => {
        this.setState({
            isShowPassword: !this.state.isShowPassword,
            type_input: !this.state.isShowPassword ? 'text' : 'password'
        });
    };

    handleBlur = () => {
        this.setState({
            is_blur: true
        });
    };

    render() {
        const { validation_errors, error_messages, is_blur, type_input } = this.state;
        const { value_prop, name } = this.props;

        return (
            <div>
                <div className="control-form">
                    <input
                        type={type_input}
                        placeholder="Введите пароль"
                        className={this.colourInputField()}
                        onChange={this.handleChangeEvent}
                        onBlur={this.handleBlur}
                        value={value_prop}
                        name={name}
                    />
                    <label>
                        <input
                            type="checkbox"
                            className="toggle-password-input-check"
                        />
                        <span
                            className="toggle-password"
                            onClick={this.showPassword}>
                            &nbsp;
                        </span>
                    </label>
                </div>
                {
                    is_blur &&
                    <ShowError
                        existing_errors={validation_errors}
                        messages={error_messages}
                    />
                }
            </div>
        );
    }
}

PasswordInput.propTypes = {
    name: PropTypes.string.isRequired,
    value_prop: PropTypes.string,
};

PasswordInput.defaultProps = {
    value_prop: ''
};