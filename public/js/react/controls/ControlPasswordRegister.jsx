import React from 'react';
import ShowError from '../ShowError.jsx';
import ControlBase from './ControlBase';
import MESSAGES from '../constants/messages';

export default class PasswordInput extends ControlBase {
    constructor() {
        super();
        this.state = {
            is_blur: false,
            validation_errors: {
                password_invalid: null,
                not_confirmed: null
            },
            onFocus: '',
            hints: {
                password_rules: false
            },
            warnings: {
                capslock: false,
                cyrillic: false
            },
            type_input: 'password',
            isShowPassword: false,
            counter: 0,
            password: '',
            second_password: '',
            messages: {
                errors: {
                    password_invalid: MESSAGES.VALIDATION_ERRORS.password_invalid,
                    not_confirmed: MESSAGES.VALIDATION_ERRORS.not_confirmed,
                    undefined_error: ''
                },
                hints: {
                    password_rules: {
                        header: 'Правила введения пароля',
                        text: 'не менее 6 символов и не более 64 (A-Z,a-z,0-9), без пробелов.'
                    }
                },
                warnings: {
                    capslock: {
                        header: 'Внимание!',
                        text: 'Вы печатаете со включенным CapsLock!'
                    },
                    cyrillic: {
                        header: 'Внимание!',
                        text: 'Вы печатаете кириллицей!'
                    }
                }
            }
        };

        this.handleChangeFirstPassword = this.handleChangeFirstPassword.bind(this);
        this.handleBlurFirstPassword = this.handleBlurFirstPassword.bind(this);
        this.showHint = this.showHint.bind(this);
        this.handleBlurSecondPassword = this.handleBlurSecondPassword.bind(this);
        this.handleChangeSecondPassword = this.handleChangeSecondPassword.bind(this);
        this.showPassword = this.showPassword.bind(this);
        this.isPasswordsEqual = this.isPasswordsEqual.bind(this);
        this.capslockHandler = this.capslockHandler.bind(this);
    }

    componentDidMount(){
        this.props.onSubmit(this.isPasswordsEqual);
    }

    setUpstatePasswordsAreEqual(trueOrFalse) {                             // setUpState - префикс передачи состояния родительскому компоненту
        this.props.areEqual('passwordsAreEqual', trueOrFalse);            // отправка состояния компонента глобальному компоненту
        this.props.componentValue('password', this.state.password);
    }

    handleChangeEvent(e) {
        const value = e.target.value;
        const { name, handleComponentChange } = this.props;

        handleComponentChange && handleComponentChange(name, value);
    }

    capslockHandler(e) {        // отслеживает индикацию капс-лока
        this.hideHint();
        let caps = e.getModifierState && e.getModifierState('CapsLock');
        if (caps) {
            this.setState({
                warnings: {
                    ...this.state.warnings,
                    capslock: true
                }
            });
        } else {
            // this.setHint();
            this.setState({
                warnings: {
                    ...this.state.warnings,
                    capslock: false
                }
            });
        }
    }

    cyrillicHandler(e) {                                  // отслеживает ввод кириллицы
        if (/[А-Яа-яё]/i.test(e.target.value)) {
            this.hideHint();
            this.setState({
                warnings: {
                    ...this.state.warnings,
                    cyrillic: true
                }
            });
        }
        else {
            this.setState({
                warnings: {
                    ...this.state.warnings,
                    cyrillic: false
                }
            });
        }
    }

    isPasswordsEqual() {

        if (this.state.counter) {
            if (this.testPasswordsAreEqual(this.state.second_password, this.state.password)) {
                this.setUpstatePasswordsAreEqual(true);
                this.passwordConfirmation(true);
            }
            else {
                this.setUpstatePasswordsAreEqual(false);
                this.passwordConfirmation(false);
            }
        }
    }

    testPasswordsAreEqual(value1, value2) {
        if (value1 === value2 && this.state.validation_errors.password_invalid === false) {
            this.setState({
                validation_errors: {
                    ...this.state.validation_errors,
                    not_confirmed: false
                }
            });
            return true;
        } else {
            this.setState({
                validation_errors: {
                    ...this.state.validation_errors,
                    not_confirmed: true
                }
            });
            return false;
        }
    }

    showPassword() {
        this.setState({
            isShowPassword: !this.state.isShowPassword,
            type_input: !this.state.isShowPassword ? 'text' : 'password'
        });
    }

    handleChangeFirstPassword(e) {
        this.handleChangeEvent(e);
        this.setState({
            password: e.target.value
        });
        // this.cyrillicHandler(e);
    }

    handleChangeSecondPassword(e) {
        this.handleChangeEvent(e);
        this.setState({
            second_password: e.target.value
        });
        // this.cyrillicHandler(e);
    }

    handleBlurFirstPassword(e) {
        this.hideHint();
        const value = e.target.value;
        const { name, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    }

    handleBlurSecondPassword() {
        this.setState({
            is_blur: true
        });
        this.hideHint();
        this.isPasswordsEqual();
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value),
                password_invalid: !this.validatePassword(value)
            }
        });
    }

    setHint() {
        this.setState({
            hints: {
                password_rules: true
            }
        });
    }

    hideHint() {
        this.setState({
            hints: {
                password_rules: false
            }
        });
    }

    // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
    checkAllValidations(value) {
        this.increaseCounter();
        this.updateErrors(value);
        return (
            this.validateIsEmpty(value)
            && this.validatePassword(value)
        );
    }

    increaseCounter(){
        this.setState({counter:1});
    }

    render() {
        const { type_input, validation_errors, messages, is_blur } = this.state;

        return (
            <div>
                <div className="control-form">
                    <input
                        placeholder="Введите пароль"
                        className={this.colourInputField()}
                        id="password_field_1"
                        type={type_input}
                        onChange={this.handleChangeFirstPassword}
                        onBlur={this.handleBlurFirstPassword}
                        onFocus={this.showHint}
                        onKeyDown={this.capslockHandler}
                        name="password"
                    />
                    <label>
                        <input type="checkbox" className="toggle-password-input-check" value="on" />
                        <span
                            className="toggle-password"
                            onClick={this.showPassword}
                        />
                    </label>
                </div>
                <div className="control-form">
                    <input
                        placeholder="Подтвердите пароль"
                        className={this.colourInputField()}
                        id="password_field_2"
                        name="confirm_password"
                        type={type_input}
                        onChange={this.handleChangeSecondPassword}
                        onBlur={this.handleBlurSecondPassword}
                        onKeyDown={this.capslockHandler}
                    />
                    <label>
                        <input type="checkbox" className="toggle-password-input-check" value="on" />
                        <span
                            className="toggle-password"
                            onClick={this.showPassword}
                        />
                    </label>
                </div>
                {
                    (is_blur || this.state.validation_errors) &&
                    <ShowError
                        existing_errors={validation_errors}
                        messages={messages.errors}
                    />
                }
                {/* <ShowHint hints={hints} messages={messages.hints}/>*/}
                {/* <ShowWarning warnings={warnings} messages={messages.warnings}/>*/}
            </div>
        );
    }
}



