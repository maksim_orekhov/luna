import React from 'react';
import MESSAGES from '../constants/messages';
import ShowError from '../ShowError';
import ControlBase from './ControlBase';

class InputDateMask extends ControlBase {

    constructor(props) {
        super(props);
        this.state = {
            validation_errors: {
                isEmpty: null,
                is_valid: null,
                max_year: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                is_valid: MESSAGES.VALIDATION_ERRORS.invalid_date,
                max_year: props.validation.max_year_error_message || `${MESSAGES.VALIDATION_ERRORS.max_date} ${props.validation.max_year}`,
                undefined_error: ''
            },
            name: '',
            user_input: props.value_prop || '',
            year_start_pos: '',
            year_end_pos: '',
            year_length: '',
            day_start_pos: '',
            day_end_pos: '',
            day_length: '',
            month_start_pos: '',
            month_end_pos: '',
            month_length: '',
            day_string: '',
            month_string: '',
            year_string: '',
            char_code: '',
            current_day: this.getCurrentDay(),
            current_month: this.getCurrentMonth(),
            current_year: this.getCurrentYear(),
            caret_pos: '',
            day_first_num: true,
            day_second_num: true,
            key_value: '',
            is_android: false,
            char_android: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.getCursorPosition = this.getCursorPosition.bind(this);
        this.getCharCode = this.getCharCode.bind(this);
        this.handleOnBlur = this.handleOnBlur.bind(this);
        this.getCharOnAndroid = this.getCharOnAndroid.bind(this);
    }

    handleChange(e) {
        const { day_start_pos, day_end_pos, month_start_pos, month_end_pos, year_start_pos, year_end_pos, user_input, is_android } = this.state;
        const value = e.target.value;
        let new_value = user_input;
        const input_length = this.getInputLength();
        let day_string = value.slice(day_start_pos, day_end_pos + 1);
        let month_string = value.slice(month_start_pos, month_end_pos + 1);
        let year_string = value.slice(year_start_pos, year_end_pos + 1);

        if (value.length <= input_length) {
            new_value = value;
        }

        this.setState({
            day_string,
            month_string,
            year_string
        });

        if (this.state.key_value !== 'Backspace') {
            if (value.search(/[!@#$%^:;?`&*(){}/\-,+А-Яа-яёA-Za-z]/g) === -1) {
                if (this.validateDay(day_string)) {
                    if (this.validateMonth(month_string)) {
                        if (this.validateYear(year_string)) {

                            this.setState({
                                user_input: new_value
                            }, () => this.handleOnBlur());

                            if (new_value.length >= this.getInputLength()) {
                                this.replaceChar();
                            }
                        }
                    }
                }
            }
        }
        if (is_android) {
            this.getCursorPosition();
        }

        this.checkForBackspaceKey(new_value);
        this.validateBeforeReplace();
        this.getCharOnAndroid();
    }

    getCharOnAndroid() {
        const inputField = this.input;

        inputField.addEventListener('textInput', (e) => {
            let char = e.data;
            this.setState({
                char_android: char
            });
        });

    }

    checkForBackspaceKey(new_value) {
        this.input.addEventListener('keydown', (e) => {
            this.setState({
                key_value: e.key
            });
        });
        !this.state.is_android && this.getCursorPosition();

        if ((this.state.key_value === 'Backspace' || this.state.char_code === '8') && this.state.user_input.length >= this.getInputLength()) {
            this.replaceToUnderscore();
        } else if (this.state.key_value === 'Backspace' || this.state.char_code === '8') {
            this.setState({
                user_input: new_value
            });
        }
    }

    handleOnBlur() {
        const { handleComponentValidation, handleComponentChange, name, validate_prop = `${name}_is_valid` } = this.props;
        const { user_input, day_string, month_string } = this.state;

        this.validateDaysAndMonths(user_input, day_string, month_string);

        handleComponentChange && handleComponentChange(name, user_input);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations());
    }

    updateErrors() {
        const { user_input } = this.state;

        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: (this.props.validation.isEmpty ? !this.validateIsEmpty(user_input) : false),
                is_valid: !this.validateFullDate(user_input),
                max_year: (this.props.validation.max_year ? !this.validateMaxYear() : false)
            }
        });
    }

    // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
    checkAllValidations() {
        this.updateErrors();
        const { user_input } = this.state;

        return (
            (!this.props.validation.isEmpty || this.validateIsEmpty(user_input))
            && this.validateFullDate(user_input)
            && (!this.props.validation.max_year || this.validateMaxYear())
            && user_input.length === this.getInputLength()
        );
    }

    validateMaxYear() {
        const { year_string, user_input } = this.state;
        // Если инпут заполнен полностью то проверяем макс дату
        return user_input.length === this.getInputLength() && Number(year_string) < this.props.validation.max_year;
    }

    validateBeforeReplace() {
        this.validateDayString(this.state.day_string);
        this.validateMonthString(this.state.month_string, this.state.user_input, this.state.day_string);
        this.validateYearString(this.state.year_string);
    }

    componentDidMount() {
        this.parseDateFormat();
        this.getMobileOperatingSystem();
    }

    componentWillMount() {
        const { day_start_pos, day_end_pos, month_start_pos, month_end_pos, year_start_pos, year_end_pos, user_input } = this.state;

        let day_string = user_input.slice(day_start_pos, day_end_pos + 1);
        let month_string = user_input.slice(month_start_pos, month_end_pos + 1);
        let year_string = user_input.slice(year_start_pos, year_end_pos + 1);

        this.setState({
            day_string,
            month_string,
            year_string
        }, () => this.checkValidateOnMount());

    }

    componentWillReceiveProps(nextProps) {
        const { value_prop, form_control_server_errors } = nextProps;

        if (value_prop) {
            this.setState({
                user_input: value_prop
            }, () => {
                this.validateControl(nextProps);
            });
        }

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);
    }

    replaceChar() {
        const { is_android, char_code, char_android } = this.state;
        let character = String.fromCharCode(+char_code);
        let cursor_pos = this.getCursorPosition();
        let replace = is_android ? this.replaceAt(cursor_pos - 1, char_android) : this.replaceAt(cursor_pos - 1, character);
        if(cursor_pos <= this.getInputLength()) {
            this.setState({
                user_input: replace
            }, () => this.setCursorPosition(cursor_pos));
        }
    }

    replaceToUnderscore() {
        let cursor_pos = this.getCursorPosition();
        let replaceToUnderscore = this.replaceAt(cursor_pos, '_');

        if (cursor_pos === this.state.day_start_pos - 1
            || cursor_pos === this.state.month_start_pos - 1
            || cursor_pos === this.state.year_start_pos - 1) {
            replaceToUnderscore = this.replaceAt(cursor_pos, this.getDateFormatSeparator());
        }

        this.setState({
            user_input: replaceToUnderscore
        }, () => this.handleOnBlur());
    }

    getCursorPosition() {
        let caret_pos = 0;
        let val = this.input.value;
        caret_pos = val.slice(0, this.input.selectionStart).length;

        this.setState({
            caret_pos: caret_pos
        }, () => {this.setCursorPosition(caret_pos);});
        return caret_pos;
    }

    setCursorPosition(caret_pos) {
        this.input.focus();
        this.input.setSelectionRange(caret_pos, caret_pos);

        if (caret_pos === this.state.day_end_pos + 1
            || caret_pos === this.state.month_end_pos + 1
            || caret_pos === this.state.year_end_pos + 1) {
            this.input.setSelectionRange(caret_pos + 1, caret_pos + 1);
        }
        if ((this.state.key_value === 'Backspace' || this.state.char_code === '8') && this.state.user_input.length >= this.getInputLength()) {
            if (caret_pos === this.state.day_start_pos - 1
                || caret_pos === this.state.month_start_pos - 1
                || caret_pos === this.state.year_start_pos - 1) {
                this.input.setSelectionRange(caret_pos, caret_pos);
            }
        }

        if (caret_pos === 0) {
            this.input.setSelectionRange(0, 0);
        }
    }

    getCharCode(e) {
        const { is_android, key_value } = this.state;
        let char_code = is_android ? e.keyCode : e.charCode;
        let value = this.state.user_input;
        this.setState({
            char_code
        });
        if (key_value !== 'Backspace' || this.state.char_code === '8') {
            this.addSeparator(value);
        }

        this.getCursorPosition();
    }

    addSeparator(value) {
        const { day_end_pos, day_start_pos, day_length, month_start_pos, month_end_pos, month_length, year_start_pos, year_end_pos, year_length } = this.state;

        if (
            ((value.length === (day_start_pos + day_length) && day_end_pos !== this.getInputLength() - 1)
                || (value.length === (month_start_pos + month_length) && month_end_pos !== this.getInputLength() - 1)
                || (value.length === (year_start_pos + year_length) && year_end_pos !== this.getInputLength() - 1))
            && (this.state.key_value !== this.getDateFormatSeparator())
        ) {
            this.setState({
                user_input: value + this.getDateFormatSeparator()
            });
        } else if (year_start_pos === 0 && value.length === (year_start_pos + year_length)) {
            this.setState({
                user_input: value + this.getDateFormatSeparator()
            });
        }
    }

    replaceAt(index, replacement) {
        let value = this.state.user_input;
        return value.slice(0, index) + replacement + value.slice(index + replacement.length);
    }

    validateDay(day_string) {
        return (this.validateDayFirstNum(day_string) && this.validateDaySecondNum(day_string));
    }

    validateMonth(month_string) {
        return (this.validateMonthFirstNum(month_string) && this.validateMonthSecondNum(month_string));
    }

    validateYear(year_string) {
        return (this.validateYearFirstNum(year_string));
    }

    validateDayFirstNum(day_string) {
        if (day_string.charAt(0) > 3) {
            this.setCursorPosition(this.state.day_start_pos);
            return false;
        } else {
            return true;
        }
    }

    validateDaySecondNum(day_string) {
        if (day_string.charAt(0) == 3 && (day_string.charAt(1) > 1)) {
            this.setCursorPosition(this.state.day_start_pos + 1);
            return false;
        } else {
            return true;
        }
    }

    validateMonthFirstNum(month_string) {
        if (month_string.charAt(0) > 1) {
            this.setCursorPosition(this.state.month_start_pos);
            return false;
        } else {
            return true;
        }
    }

    validateMonthSecondNum(month_string) {
        if (month_string.charAt(0) == 1
            && (month_string.charAt(1) > 2)) {
            this.setCursorPosition(this.state.month_start_pos + 1);
            return false;
        } else {
            return true;
        }
    }

    validateYearFirstNum(year_string) {
        if (this.state.year_length === 4 && year_string.charAt(0) > 2) {
            this.setCursorPosition(this.state.year_start_pos);
            return false;
        } else {
            return true;
        }
    }

    validateDaysAndMonths(value, day_string, month_string) {
        // проверка на месяцы, в которых 30 дней
        if (month_string.match(/(0[0469]|11)/) && day_string > 30) {
            this.setState({
                user_input: value.replace(day_string, '30')
            });
            // return false
        }
        // проверка на февраль
        if (month_string.match(/(02)/) && day_string > 28) {
            this.setState({
                user_input: value.replace(day_string, '28')
            });
            // return false
        }
    }

    validateDayString(day_string) {
        if (day_string.length === 2 && day_string.match(/^(0?[1-9]|[12][0-9]|3[01])$/)) {
            return true;
        } else {
            return false;
        }
    }

    validateMonthString(month_string) {
        if (month_string.length === 2 && month_string.match(/^(0?[1-9]|1[012])$/)) {
            return true;
        } else {
            return false;
        }
    }

    validateYearString(year_string) {
        if (year_string.length === 4 && year_string <= this.getCurrentYear() && year_string.match(/^\d{4}$/) && Number(year_string) > 1900) {
            return true;
        } else if (year_string.length === 2 && year_string > 0 && year_string <= this.getCurrentYear().slice(-2) && year_string.match(/^\d{2}$/)) {
            return true;
        } else {
            return false;
        }
    }

    validateFullDate(value) {
        if (value.match(/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:19|20)\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:19|20)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:19|20)\d{2})$/)) {
            return true;
        } else {
            return false;
        }
    }

    getValue() {
        return this.state.user_input;
    }

    getDateFormatDay() {
        let date_format = this.props.date_format;
        let day_start_pos = date_format.indexOf('d');
        let day_end_pos = date_format.lastIndexOf('d');
        let day_length = date_format.match(/d/g).length;

        this.setState({
            day_start_pos,
            day_end_pos,
            day_length
        });
    }

    getDateFormatMonth(){
        let date_format = this.props.date_format;
        let month_start_pos = date_format.indexOf('m');
        let month_end_pos = date_format.lastIndexOf('m');
        let month_length = date_format.match(/m/g).length;

        this.setState({
            month_start_pos,
            month_end_pos,
            month_length
        });
    }

    getDateFormatYear(){
        let date_format = this.props.date_format;
        let year_start_pos = date_format.indexOf('y');
        let year_end_pos = date_format.lastIndexOf('y');
        let year_length = date_format.match(/y/g).length;

        this.setState({
            year_start_pos,
            year_end_pos,
            year_length
        });
    }

    getDateFormatSeparator() {
        let date_format = this.props.date_format;
        let date_format_separator = date_format.replace(/[^_:,.\-\/]/g, '');
        return date_format_separator.charAt(0);
    }

    parseDateFormat() {
        this.getDateFormatDay();
        this.getDateFormatMonth();
        this.getDateFormatYear();
        this.getDateFormatSeparator();
    }

    getInputLength() {
        return this.state.day_length + this.state.month_length + this.state.year_length + 2;
    }

    getCurrentDay() {
        let current_date = new Date();

        return ('0' + current_date.getDate()).slice(-2);
    }

    getCurrentMonth() {
        let current_date = new Date();
        let current_month = current_date.getMonth() + 1;

        return '0' + current_month.toString().slice(-2);
    }

    getCurrentYear() {
        let current_date = new Date();

        return current_date.getFullYear().toString();
    }

    getPlaceholder() {
        let mask = this.props.date_format;
        let split_mask = mask.split(this.getDateFormatSeparator());

        for (let i = 0; i < split_mask.length; i++) {
            if (split_mask[i] === 'dd') {
                split_mask[i] = split_mask[i].replace('dd', this.getCurrentDay());
            } else if (split_mask[i] === 'mm') {
                split_mask[i] = split_mask[i].replace('mm', this.getCurrentMonth());
            } else if (split_mask[i] === 'yyyy') {
                split_mask[i] = split_mask[i].replace('yyyy', this.getCurrentYear());
            } else if (split_mask[i] === 'yy') {
                split_mask[i] = split_mask[i].replace('yy', this.getCurrentYear().slice(-2));
            }
        }
        return split_mask.join(this.getDateFormatSeparator());
    }

    getMobileOperatingSystem() {
        let userAgent = navigator.userAgent || navigator.vendor || window.opera;

        if (/android/i.test(userAgent)) {
            this.setState({
                is_android: true
            });
        }
    }

    render() {
        const { name, label, placeholder } = this.props;
        const { user_input, error_messages, validation_errors, is_android } = this.state;

        return (
            <div className="control-form">
                <label className="label-text">
                    {label}
                </label>
                <input
                    value={user_input}
                    name={name}
                    onChange={this.handleChange}
                    onFocus={this.getCursorPosition}
                    onBlur={this.handleOnBlur}
                    onKeyPress={!is_android && this.getCharCode}
                    onKeyUp={is_android && this.getCharCode}
                    ref={input => this.input = input}
                    type="text"
                    className={`with-datepicker ${this.colourInputField()}`}
                    placeholder={placeholder}
                />
                <ShowError
                    messages={error_messages}
                    existing_errors={validation_errors}
                />
            </div>
        );
    }
}


export default InputDateMask;