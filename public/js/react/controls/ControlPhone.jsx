import React from 'react';
import ShowError from '../ShowError.jsx';
import InputMask from '../../libs/react-input-mask.js';
import {customFetch} from '../Helpers';
import ControlBase from './ControlBase';
import MESSAGES from '../constants/messages';

export default class PhoneInput extends ControlBase {
    constructor() {
        super();
        this.state = {
            mask: '+#(###)###-##-##',
            placeholder: '+_(___)___-__-__',
            countryPhoneIndex: [],
            mobPhoneIndex: [],
            ruPhoneIndex: [],
            validation_errors:{
                reg_exp_invalid:    null,
                phone_unavailable:  false,
                connection_is_lost: false,
                undefined_error:    false
            },
            onFocus: '',
            hints:{
                phone_rules: false
            },

            /**
             * объект messages используется для отображения ошибок в форме регистрации
             * объект error_messages используется для отображения ошибок в форме изменения номера телефона в профиле
             **/

            messages:{
                errors:{
                    reg_exp_invalid:    {
                        header: 'Неправильный формат телефона',
                        text:   'Телефон должен начинаться с кода страны (для России +7). Пример: +79051112230'
                    },
                    phone_unavailable:  {
                        header: 'Номер используется',
                        text:   'Данный номер телефона уже зарегистрирован в системе. Возможно вы регистрировались под ним ранее?'
                    },
                    connection_is_lost: {
                        header: 'Соединение с сервером потеряно',
                        text:   'Проверьте ваше соединение с интернетом и попробуйте еще раз'
                    },
                    server_error:       {
                        header: 'Ошибка обработки запроса',
                        text:   'Произошла неизвестная ошибка обработки запроса. Попробуйте обновить страницу и повторить запрос заново'
                    }
                },
                hints: {
                    phone_rules:        {
                        header: 'Формат ввода телефона',
                        text:   'Телефон должен начинаться с кода страны (для России +7). Пример: +79051112230'
                    }
                }
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                reg_exp_invalid: MESSAGES.VALIDATION_ERRORS.phone_reg_exp_invalid,
                phone_unavailable: MESSAGES.VALIDATION_ERRORS.phone_unavailable,
                undefined_error: ''
            }
        };

        this.handleFocus = this.handleFocus.bind(this);
        this.processValidation = this.processValidation.bind(this);
        this.handleChangeEvent = this.handleChangeEvent.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);
    }

    handleChangeEvent(e) {
        const value = e.target.value;
        // {props} register - флаг для определения находимся ли мы в форме регистрации или в форме редактирования
        const { name, handleComponentChange, handleComponentValidation, componentValue, register, validate_prop = `${name}_is_valid` } = this.props;

        if (!register) {
            handleComponentChange && handleComponentChange(name, value);
            handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
            this.processValidation(e);
        }

        this.checkInputMask(e);
        let phoneValue = value.replace(/(\()|(\))|(\-)/g, '');
        componentValue && componentValue('phone', phoneValue);
    }

    // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
    checkAllValidations(value) {
        const { register } = this.props;
        this.updateErrors(value);
        return (
            this.validateIsEmpty(value)
            && this.validatePhone(value, register)
        );
    }

    updateErrors(value) {
        const { register } = this.props;
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value),
                phone: !this.validatePhone(value, register)
            }
        });
    }

    getJsonPhoneInfo(url) {
        return customFetch(url, {
            method: 'POST'
        });
    }

    getDataPhoneByMob(data) {
        this.mobPhone = data;
        this.mobPhoneIndex = [];
        let mobPhoneIndex = [];
        this.mobPhone.map(function (item, index) {
            mobPhoneIndex[index] = item['mask'].replace(/(\#)|(\+)|(\()|(\))|(\-)/g, '');
        });
        this.mobPhoneIndex = mobPhoneIndex;
        this.setState({
            mobPhoneIndex: this.mobPhoneIndex
        });
    }

    getDataPhoneByCountry(data) {
        this.countryPhone = data;
        this.countryPhoneIndex = [];
        let countryPhoneIndex = [];
        this.countryPhone.map(function (item, index) {
            countryPhoneIndex[index] = item['mask'].replace(/(\#)|(\+)|(\()|(\))|(\-)/g, '');
        });
        this.countryPhoneIndex = countryPhoneIndex;
        this.setState({
            countryPhoneIndex: this.countryPhoneIndex
        });
    }

    getDataPhoneByRU(data) {
        this.ruPhone = data;
        this.ruPhoneIndex = [];
        let ruPhoneIndex = [];
        this.ruPhone.map(function (item, index) {
            ruPhoneIndex[index] = item['mask'].replace(/(\#)|(\+)|(\()|(\))|(\-)/g, '');
        });
        this.ruPhoneIndex = ruPhoneIndex;
        this.setState({
            ruPhoneIndex: this.ruPhoneIndex
        });
    }

    componentDidMount() {
        this.getCountryUrl = 'js/libs/phone-codes.json';
        this.getRUUrl = 'js/libs/phones-ru.json';
        this.getMobUrl = 'js/libs/phones-mob.json';

        this.getJsonPhoneInfo(this.getCountryUrl).then(
            data => this.getDataPhoneByCountry(data),
            error => this.fetchConnectionIsFail(error)
        );
        this.getJsonPhoneInfo(this.getRUUrl).then(
            data => this.getDataPhoneByRU(data),
            error => this.fetchConnectionIsFail(error)
        );
        this.getJsonPhoneInfo(this.getMobUrl).then(
            data => this.getDataPhoneByMob(data),
            error => this.fetchConnectionIsFail(error)
        );
    }

    phoneIsValid(trueOrFalse){
        const { register } = this.props;
        if (register) {
            this.props.isValid('phoneIsValid', trueOrFalse);         // отправка состояния компонента глобальному компоненту
        }
    }

    setHint() {
        this.setState({
            hints: {
                phone_rules: true
            }
        });
    }

    hideHint() {
        this.setState({
            hints: {
                phone_rules: false
            }
        });
    }

    setPhoneUnavailable(boolean) {
        const { validation_errors }  = this.state;
        this.setState({
            validation_errors:  {
                ...validation_errors,
                phone_unavailable: boolean
            }
        });
    }

    fetchConnectionSuccess(data) {
        if(data.status === 'SUCCESS'){
            this.phoneIsValid(true);
            this.setPhoneUnavailable(false);
        }
        else if(data['server_error']){
            this.setValidationServerError(true);
        }
        else if(data.status === 'ERROR'){
            this.phoneIsValid(false);
            this.setPhoneUnavailable(true);
        }
        return data;
    }

    fetchConnectionIsFail(err){
        this.phoneIsValid(false);
        return err;
    }

    checkInputMask(e){
        let phoneMaskValue = e.target.value.replace(/(\#)|(\_)|(\+)|(\()|(\))|(\-)/g, '');
        let curMask = '+#(###)###-##-##';
        let arrValid = [];
        for(let i = phoneMaskValue.length; i >= 0; i--){
            arrValid[i] = phoneMaskValue.substring(0,i);
        }
        function getInputMask(value) {
            if(arrValid.indexOf(value) != -1){
                return true;
            }
        }
        let countryFilterArr = this.state.countryPhoneIndex.filter(getInputMask);
        let ruFilterArr = this.state.ruPhoneIndex.filter(getInputMask);
        let mobFilterArr = this.state.mobPhoneIndex.filter(getInputMask);

        if(mobFilterArr.length > 0)
            curMask = this.mobPhone[this.mobPhoneIndex.indexOf(mobFilterArr[0])].mask.replace(/\d/g,'#');
        if(countryFilterArr.length > 0)
            curMask = this.countryPhone[this.countryPhoneIndex.indexOf(countryFilterArr[0])].mask.replace(/\d/g,'#');
        if(countryFilterArr.length > 0 && ruFilterArr.length > 0)
            curMask = this.ruPhone[this.ruPhoneIndex.indexOf(ruFilterArr[ruFilterArr.length - 1])].mask.replace(/\d/g,'#');
        this.setState({mask: curMask});
    }

    checkValueMaskAccordance(value, mask_placeholder) {
        const validation_errors = {...this.state.validation_errors};

        if (value.indexOf(mask_placeholder) === -1) {
            validation_errors.reg_exp_invalid = false;

            this.setState({validation_errors});
            return true;
        }
        else {
            validation_errors.reg_exp_invalid = true;

            this.setState({validation_errors});
            return false;
        }
    }

    handleFocus(){
        this.setState({
            onFocus : 'has-focus'
        });
        this.showHint();
    }

    processValidation(){
        this.setState({
            onFocus : ''
        });
        // let regExp = '^[^\+]?[0-9]{10,12}$';
        /* let regExp = '\\+\\d{7,13}';
        // let regExp = '^(\\s*)?(\\+)?([- _():=+]?\\d[- _():=+]?){10,14}(\\s*)?$';
        let ajaxValidationOptions = {
            'url': '/register/check/phone',
            'field': 'phone'
        };
        this.hideHint();

        const mask_placeholder = this.state.placeholder[this.state.placeholder.length - 1];
        let phoneValue = e.target.value.replace(/(\()|(\))|(\-)/g, '');
        */
        // if(this.validationRegExp(phoneValue, regExp) && this.checkValueMaskAccordance(phoneValue, mask_placeholder)){
        //
        //     this.fetchValidation(phoneValue,ajaxValidationOptions)
        //         .then(
        //             data => this.fetchConnectionSuccess(data),
        //             error => this.fetchConnectionIsFail(error)
        //         );
        // }
        // else{
        //     this.phoneIsValid(false);
        // }
    }

    render() {
        const { validation_errors, error_messages, placeholder } = this.state;
        const {label} = this.props;

        return (
            <div className="control-form">
                <label className="label-text">
                    {label}
                </label>
                <InputMask
                    mask={this.state.mask}
                    className={this.colourInputField()}
                    onFocus={this.handleFocus}
                    onBlur={this.processValidation}
                    onChange={this.handleChangeEvent}
                    placeholder={placeholder}
                />
                <ShowError
                    messages={error_messages}
                    existing_errors={validation_errors}
                />
            </div>
        );
    }
}

