import React from 'react';
import PropTypes from 'prop-types';
import ControlBase from './ControlBase';
import { replaceToTextCapitalize } from '../Helpers';
import MESSAGES from '../constants/messages';
import VALIDATION_RULES from '../constants/validation_rules';
import ShowError from '../ShowError';

export default class ControlDeliveryOrderCustomerName extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                cyrillic_only: null,
                min_chars: null,
                max_chars: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                cyrillic_only: MESSAGES.VALIDATION_ERRORS.cyrillic_only,
                min_chars: `${MESSAGES.VALIDATION_ERRORS.min_chars} - ${VALIDATION_RULES.LAST_NAME.min_chars}`,
                max_chars: `${MESSAGES.VALIDATION_ERRORS.max_chars} - ${VALIDATION_RULES.LAST_NAME.max_chars}`,
                undefined_error: ''
            },
            validation_rules: {
                min_chars: VALIDATION_RULES.LAST_NAME.min_chars,
                max_chars: VALIDATION_RULES.LAST_NAME.max_chars,
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
    }

    handleChangeEvent = (e) => {
        const value = replaceToTextCapitalize(e.target.value);
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    };
}