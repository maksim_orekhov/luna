import React from 'react';
import PropTypes from 'prop-types';
import ControlBase from './ControlBase';
import MESSAGES from '../constants/messages';
import ShowError from '../ShowError';
import VALIDATION_RULES from '../constants/validation_rules';

export default class ControlAdvName extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                max_chars: false,
                undefined_error: false
            },
            validation_rules: {
                max_chars: VALIDATION_RULES.PURCHASE_NAME.max_chars
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                max_chars: `${MESSAGES.VALIDATION_ERRORS.max_chars} - ${VALIDATION_RULES.PURCHASE_NAME.max_chars}`,
                undefined_error: ''
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    updateErrors = (value) => {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value),
                max_chars: !this.validateMaxCharsLength(value)
            }
        });
    };

    checkAllValidations = (value) => {
        this.updateErrors(value);
        return (
            this.validateIsEmpty(value) &&
            this.validateMaxCharsLength(value)
        );
    };

    render() {
        const { name, label, placeholder, value_prop, is_disable } = this.props;
        const { error_messages, validation_errors } = this.state;
        return (
            <div className="flexible-row">
                <div className="control-form">
                    <label className="label-text">
                        {label}
                    </label>
                    <input
                        name={name}
                        tabIndex="0"
                        type="text"
                        onChange={this.handleChangeEvent}
                        placeholder={placeholder}
                        value={value_prop}
                        onBlur={this.handleChangeEvent}
                        disabled={is_disable}
                        className={this.colourInputField()}
                    />
                    <div className="control-form__help-message">Название не должно превышать 200 символов</div>
                    <ShowError messages={error_messages} existing_errors={validation_errors} />
                </div>
            </div>
        );
    }
}

ControlAdvName.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    value_prop: PropTypes.string,
};

ControlAdvName.defaultProps = {
    placeholder: '',
    value_prop: ''
};