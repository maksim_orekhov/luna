import React from 'react';
import PropTypes from 'prop-types';
import ShowError from '../ShowError.jsx';
import EmailBase from './EmailBase';
import MESSAGES from '../constants/messages';

export default class ControlEmail extends EmailBase {
    constructor() {
        super();
        this.state = {
            disable: false,
            show_hint: true,
            validation_errors:{
                reg_exp_invalid:      null,
                email_unavailable:    false,
                connection_is_lost:   false,
                email:                false,
                undefined_error:      false,
            },
            onFocus: ' ',
            hints:{
                email_rules: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                reg_exp_invalid: MESSAGES.VALIDATION_ERRORS.email_reg_exp_invalid,
                undefined_error: ''
            }
        };

        this.handleBlur = this.handleBlur.bind(this);
        this.handleFocus = this.handleFocus.bind(this);
    }

    handleChangeEvent = (e) => {
        const value = e.target.value;
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
        this.checkValidationErrors();
    };

    updateErrors = (value) => {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value),
                reg_exp_invalid: !this.validateEmail(value)
            }
        });
    };

    render() {
        const { validation_errors, disable, error_messages } = this.state;
        const { value_prop, name, placeholder, label } = this.props;

        return (
            <div className="control-form">
                <label className="label-text">
                    {label}
                </label>
                <input
                    placeholder={placeholder}
                    type="email"
                    className={this.colourInputField()}
                    value={value_prop}
                    // onBlur={this.handleBlur}
                    onChange={this.handleChangeEvent}
                    disabled={disable}
                    name={name}
                />
                <ShowError
                    messages={error_messages}
                    existing_errors={validation_errors}
                />
            </div>
        );
    }
}

ControlEmail.propTypes = {
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    value_prop: PropTypes.string,
};

ControlEmail.defaultProps = {
    placeholder: '',
    value_prop: ''
};