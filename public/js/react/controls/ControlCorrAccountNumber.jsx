import React from 'react';
import PropTypes from 'prop-types';
import ControlBase from './ControlBase';
import MESSAGES from '../constants/messages';
import VALIDATION_RULES from '../constants/validation_rules';
import ShowError from '../ShowError';
import { textOnlyNumbers } from '../Helpers';

export default class ControlCorrAccountNumber extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                chars_count: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                chars_count: `${MESSAGES.VALIDATION_ERRORS.chars_count} - ${VALIDATION_RULES.CORR_ACCOUNT_NUMBER.chars_count}`,
                undefined_error: ''
            },
            validation_rules: {
                chars_count: VALIDATION_RULES.CORR_ACCOUNT_NUMBER.chars_count
            }
        };

        this.handleOnBlur = this.handleOnBlur.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    handleChangeEvent = (e) => {
        const value = textOnlyNumbers(e.target.value);
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    };

    updateErrors = (value) => {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value),
                chars_count: !this.validateCharsCount(value)
            }
        });
    };

    // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
    checkAllValidations = (value) => {
        this.updateErrors(value);
        return (
            this.validateIsEmpty(value)
            && this.validateCharsCount(value)
        );
    };

    render() {
        const { name, label, placeholder, value_prop } = this.props;
        const { error_messages, validation_errors } = this.state;
        return (
            <div className="control-form">
                <label className="label-text">
                    {label}
                </label>
                <input
                    name={name}
                    type="text"
                    onChange={this.handleChangeEvent}
                    onBlur={this.handleChangeEvent}
                    placeholder={placeholder}
                    value={value_prop}
                    maxLength={20}
                    className={this.colourInputField()}
                />
                <ShowError messages={error_messages} existing_errors={validation_errors} />
            </div>
        );
    }
}

ControlCorrAccountNumber.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    value_prop: PropTypes.string,
};

ControlCorrAccountNumber.defaultProps = {
    placeholder: '',
    value_prop: ''
};