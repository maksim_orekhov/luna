import React from 'react';
import PropTypes from 'prop-types';
import LoaderSpinner from '../components/LoaderSpinner';
import ControlBase from './ControlBase';

export default class ControlPhotoInput extends ControlBase {
    constructor(props) {
        super(props);
        this.state = {
            showOverlay: false
        };
    }

    componentDidMount() {
        window.addEventListener('dragenter', this.onDragEnter);
        window.addEventListener('dragleave', this.onDragLeave);
        window.addEventListener('drop', this.onDrop);
    }

    componentWillUnmount() {
        window.removeEventListener('dragenter', this.onDragEnter);
        window.removeEventListener('dragleave', this.onDragLeave);
        window.removeEventListener('drop', this.onDrop);
    }

    handleFileAdd = (e) => {
        const { handleComponentChange } = this.props;
        e.preventDefault();
        let file = e.target.files[0];
        const input_files = e.target.files;

        if (input_files.length) {
            handleComponentChange(file);
        }
    };

    handleFileRemove = (ev) => {
        const { handleComponentChange } = this.props;
        ev.preventDefault();
        handleComponentChange(null);
        this.setState({
            showOverlay: false
        });
    };

    onDragEnter = (e) => {
        e.preventDefault();
        if (this.node.contains(e.target)) {
            this.setState({
                lastTarget: e.target,
                showOverlay: true
            });
        }
    };

    onDragLeave = (e) => {
        e.preventDefault();
        if (e.target === this.state.lastTarget) {
            this.setState({
                showOverlay: false
            });
        }
    };

    onDrop = (e) => {
        e.preventDefault();
        const { handleComponentChange } = this.props;
        let file = e.dataTransfer.files[0];
        const input_files = e.dataTransfer.files;

        if (input_files.length && this.node.contains(e.target)) {
            handleComponentChange(file);
        }
    };

    render() {
        const { photo, handleImageRotate } = this.props;
        const { showOverlay } = this.state;
        return(
            <div className="photo-input" ref={node => this.node = node}>
                <label>
                    <input
                        type="file"
                        accept="image/x-png,image/jpeg"
                        className="photo-input__field"
                        onChange={this.handleFileAdd}
                    />
                    <span className={`photo-input__placeholder ${showOverlay ? 'photo-input__placeholder-active' : ''}`}>
                        <span className="photo-input__placeholder-image">
                            <img src="../../../img/photo-input-bg.svg" alt="" />
                        </span>
                        <span className="photo-input__placeholder-title">Добавить фотографию</span>
                    </span>
                    {
                        photo.image && photo.image.path
                        && <div className="photo-input__preview-wrapper">
                            <span
                                className="photo-input__preview is-visible"
                                style={{backgroundImage: `url(${photo.image.path})`, transform: `rotate(${photo.image.angle}deg)`}}
                            />
                            <button type="button" className="photo-input__control-button photo-input__control-button_type_remove" onClick={this.handleFileRemove} />
                            <button type="button" className="photo-input__control-button photo-input__control-button_type_util" onClick={handleImageRotate} />
                        </div>
                    }
                </label>
                {
                    photo.isUploading && <LoaderSpinner isAbsolute isTiny />
                }
            </div>
        );
    }

}


ControlPhotoInput.propTypes = {
    handleComponentChange: PropTypes.func,
    handleImageRotate: PropTypes.func,
    photo: PropTypes.object,
};
ControlPhotoInput.defaultProps = {
    handleComponentChange: () => {},
    handleImageRotate: () => {},
    photo: {}
};

