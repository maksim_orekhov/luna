import React from 'react';
import Popup from './Popup';
import FormCreateShop from './forms/FormCreateShop';
import FormSecureDeal from './forms/FormSecureDeal';
import Tabs, { Tab } from './components/Tabs';
import {prettifyPhoneNumber} from './Helpers';
import FormConnectionSecuryDeal from './forms/new_form_for_profile/FormConnectionSecuryDeal';
import FormDeliveryDetails from './forms/new_form_for_profile/FormDeliveryDetails';
import FormBankCard from './forms/new_form_for_profile/FormBankCard';

export default class UserSettings extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpenedFormSecureDeal: false,
            isOpenedFormCreateShop: false,
            isSafeDealOn: false,
            isOpenedFormDeliveryDetails: false,
            isOpenedFormConnectionSecuryDeal: false,
            isOpenedFormBankCard: false
        };

        this.handleClosePopup = this.handleClosePopup.bind(this);
    }

    showForm = (form_name) => () => {
        this.setState({
            // isSafeDealOn: !!this.checkbox.checked,
            [form_name]: true
        });
    };

    handleClosePopup() {
        this.setState({
            isOpenedFormSecureDeal: false,
            isOpenedFormCreateShop: false,
            isOpenedFormDeliveryDetails: false,
            isOpenedFormConnectionSecuryDeal: false,
            isOpenedFormBankCard: false
        });
    }

    render() {
        const {
            isOpenedFormCreateShop,
            isOpenedFormSecureDeal,
            isSafeDealOn,
            isOpenedFormDeliveryDetails,
            isOpenedFormConnectionSecuryDeal,
            isOpenedFormBankCard
        } = this.state;

        const { email, name, login, phone, is_email_verified, is_phone_verified } = this.props;

        return(
            <div className="col-4 col-tablet-6 col-mobile-8 col-mobile-offset-0 col-double-padding user-info-block_settings">
                <div className="user-info-block-wrap">
                    <div className="col-mobile-8">
                        <div className="container">
                            <Tabs tabsListClassName="row tab">
                                <Tab
                                    tabName="Безопасная сделка"
                                    tabClassName="tab__title col-7 col-tablet-3 tab__title-user-settings"
                                >
                                    <div className="info-fields">
                                        <div className="safe-deal-for-user col-tablet-8 col-mobile-8">
                                            <p className="safe-deal-label" style={{marginBottom: 12 + 'px'}}>Получайте оплату за свои товары на карту. Это безопасно и повышает доверие покупателей к вашим товарам.</p>
                                            <p className="safe-deal-label">Активировать безопасную сделку:</p>
                                            <div className="safe-deal-control">
                                                <label className="toggled-checkbox-field toggled-checkbox-field__type_garant-pay">
                                                    <input type="checkbox" onChange={this.showForm('isOpenedFormConnectionSecuryDeal')} checked={isOpenedFormConnectionSecuryDeal} ref={checkbox => this.checkbox = checkbox} />
                                                    <div className="toggled-checkbox-field__track">
                                                        <div className="toggled-checkbox-field__button" />
                                                    </div>
                                                </label>
                                                {
                                                    isSafeDealOn ?
                                                        <p className="safe-deal-info">Безопасная сделка включена!</p>
                                                        :
                                                        <p className="help-message">Безопасная сделка отключена!</p>
                                                }
                                            </div>
                                            <div className="safe-deal-info" onClick={this.showForm('isOpenedFormConnectionSecuryDeal')}>Находится в тестовом режиме. Будьте первыми, узнайте о преимуществах и оставьте заявку на подключение!</div>
                                        </div>
                                    </div>
                                </Tab>
                                <Tab
                                    tabName="Доставка и оплата"
                                    tabClassName="tab__title col-7 col-tablet-3 tab__title-user-settings"
                                >
                                    <div className="info-fields info-fields-requisites">
                                        <div className="col-tablet-8 col-mobile-8">
                                            <div className="requisites-title">
                                                <p className="info-field-label">Реквизиты для доставки:</p>
                                                <span className="requisites-add" onClick={this.showForm('isOpenedFormDeliveryDetails')}>Добавить</span>
                                            </div>
                                            <div className="info-field__editable-text-field">
                                                <div className="info-field-text info-field-requisites">Сохраните данные для доставки и Вам больше не придется заполнять форму повторно.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="info-fields info-fields-requisites">
                                        <div className="info-field col-tablet-8 col-mobile-8">
                                            <div className="requisites-title">
                                                <p className="info-field-label">Привязанная карта:</p>
                                                <span className="requisites-add" onClick={this.showForm('isOpenedFormBankCard')}>Добавить</span>
                                            </div>
                                            <div className="info-field__editable-text-field" onClick={this.showForm('isOpenedFormBankCard')}>
                                                <div className="info-field-text info-field-text__card">Карта не привязана</div>
                                                <span className="info-field__edit-button" />
                                            </div>
                                        </div>
                                    </div>
                                </Tab>
                            </Tabs>
                        </div>
                    </div>
                </div>
                {
                    isOpenedFormCreateShop &&
                    <Popup close={this.handleClosePopup} popupClassName="js-popup-container-shop">
                        <FormCreateShop
                            handleClosePopup={this.handleClosePopup}
                        />
                    </Popup>
                }
                {
                    isOpenedFormSecureDeal &&
                    <Popup close={this.handleClosePopup} popupClassName="js-popup-container-secure-deal-form">
                        <FormSecureDeal
                            handleClosePopup={this.handleClosePopup}
                        />
                    </Popup>
                }
                {
                    isOpenedFormDeliveryDetails &&
                    <Popup close={this.handleClosePopup} popupClassName="js-popup-container-shop">
                        <FormDeliveryDetails
                            handleClosePopup={this.handleClosePopup}
                        />
                    </Popup>
                }
                {
                    isOpenedFormConnectionSecuryDeal &&
                    <Popup close={this.handleClosePopup} popupClassName="js-popup-container-shop">
                        <FormConnectionSecuryDeal
                            handleClosePopup={this.handleClosePopup}
                            name={name}
                            login={login}
                            email={email}
                            phone={phone}
                            is_email_verified={is_email_verified}
                            is_phone_verified={is_phone_verified}
                        />
                    </Popup>
                }
                {
                    isOpenedFormBankCard &&
                    <Popup close={this.handleClosePopup} popupClassName="js-popup-container-shop">
                        <FormBankCard
                            handleClosePopup={this.handleClosePopup}
                        />
                    </Popup>
                }
            </div>
        );
    }
}