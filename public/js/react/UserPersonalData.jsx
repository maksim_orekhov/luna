import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { prettifyPhoneNumber } from './Helpers';
import { openUserProfileForm, sendPhoneCode, sendConfirmEmail } from './actions';

function UserPersonalData (props) {
    const { name, phone, email, is_email_verified, is_phone_verified, openForm, sendPhoneCode, csrf, sendConfirmEmail } = props;

    const handleOpenForm = (formName) => (ev) =>  {
        ev.preventDefault();
        openForm(formName);
    };

    const handleSendPhoneCode = (ev) => {
        ev.preventDefault();
        sendPhoneCode(JSON.stringify({
            csrf: csrf.value,
        }));
    };

    const handleSendAcceptEmail = (ev) => {
        ev.preventDefault();
        sendConfirmEmail(JSON.stringify({
            csrf: csrf.value,
        }));
    };
    
    return(
        <div className="col-4 col-tablet-6 col-mobile-8 col-double-padding user-info-block_personal-data">
            <div className="user-info-block-wrap">
                <div className="col-mobile-8">
                    <h4 className="user-info__title">Личные данные</h4>
                    <div className="info-fields">
                        <div className="info-field col-tablet-8 col-mobile-8">
                            <p className="info-field-label">Имя в объявлениях:</p>
                            <div className="info-field__editable-text-field" onClick={handleOpenForm('nameAdd')}>
                                <div className="info-field-text" title={name}><span>{name}</span></div>
                                <span className="info-field__edit-button" />
                            </div>
                            <div className="info-field__action">
                                <a href="#" className="info-field__action-link" onClick={handleSendAcceptEmail} style={{visibility: 'hidden'}}>Подтвердить email</a>
                            </div>
                        </div>
                        <div className="info-field col-tablet-8 col-mobile-8">
                            <p className="info-field-label">
                                Телефон:
                            </p>
                            <div className="info-field__editable-text-field" onClick={handleOpenForm('phoneChange')}>
                                <div
                                    className={`info-field-text ${is_phone_verified ? 'is-approved' : phone ? 'is-not-approved' : 'is-empty'}`}
                                    title={phone}
                                >
                                    {phone ? prettifyPhoneNumber(phone) : 'Введите номер телефона'}
                                </div>
                                <span className="info-field__edit-button" />
                            </div>

                            <div className="info-field__action">
                                {
                                    !is_phone_verified && phone ?
                                        <a href="#" className="info-field__action-link" onClick={handleSendPhoneCode}>Подтвердить телефон</a>
                                        :
                                        <a href="#" className="info-field__action-link" onClick={handleOpenForm('phoneChange')}>{phone ? 'Изменить телефон' : 'Добавить телефон'}</a>
                                }
                            </div>

                        </div>
                        <div className="info-field col-tablet-8 col-mobile-8">
                            <p className="info-field-label">
                                Email:
                            </p>
                            <div className="info-field__editable-text-field" onClick={handleOpenForm('emailChange')}>
                                <div className={`info-field-text ${is_email_verified ? 'is-approved' : 'is-not-approved'}`}>{email}</div>
                                <span className="info-field__edit-button" />
                            </div>
                            {
                                !is_email_verified ?
                                    <div className="info-field__action">
                                        <a href="#" className="info-field__action-link" onClick={handleSendAcceptEmail}>Подтвердить email</a>
                                    </div>
                                    :
                                    <div className="info-field__action">
                                        <a href="#" className="info-field__action-link" onClick={handleOpenForm('emailChange')}>Изменить email</a>
                                    </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

UserPersonalData.propTypes = {
    email: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    phone: PropTypes.string,
    is_email_verified: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
    is_phone_verified: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
    // redux store data
    csrf: PropTypes.object.isRequired,
    // redux dispatch
    openForm: PropTypes.func.isRequired,
    sendPhoneCode: PropTypes.func.isRequired,
    sendConfirmEmail: PropTypes.func.isRequired,
};

UserPersonalData.defaultProps = {
    phone: '',
    is_email_verified: false,
    is_phone_verified: false,
};

const mapStateToProps = (state) => {
    return {
        csrf: state.config.csrf,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        openForm: bindActionCreators(openUserProfileForm, dispatch),
        sendPhoneCode: bindActionCreators(sendPhoneCode, dispatch),
        sendConfirmEmail: bindActionCreators(sendConfirmEmail, dispatch),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserPersonalData);
