import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import appStore from '../store';
import { getDisplayName } from '../Helpers';

function withProvider (Component) {
    const withProvider = function (props) {
        return (
            <Provider store={appStore}>
                <Component {...props} />
            </Provider>
        );
    };
    withProvider.displayName = `withProvider(${getDisplayName(Component)})`;
    return withProvider;
}

withProvider.propTypes = {
    Component: PropTypes.element.isRequired
};

export default withProvider;
