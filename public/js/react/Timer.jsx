import React from 'react';
import PropTypes from 'prop-types';

export default class Timer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            time_amount: props.start || 0
        };
    }

    componentDidMount() {
        this.timer = setInterval(this.tick.bind(this), 1000);
    }

    componentWillReceiveProps(nextProps) {
        const { is_paused } = nextProps;

        if (is_paused !== this.props.is_paused) {
            if (is_paused) {
                clearInterval(this.timer);
            } else {
                this.timer = setInterval(this.tick.bind(this), 1000);
            }
        }
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    tick() {
        if (this.props.is_paused) return;

        this.setState({
            time_amount: this.state.time_amount - 1
        });
        this.counterCheck();
    }

    counterCheck() {
        const { expired } = this.props;
        if (this.state.time_amount < 1) {
            expired && expired(true);
        }
    }

    render() {
        const { shutDown, isShowTime, is_paused } = this.props;

        if (shutDown) {
            this.componentWillUnmount();
        }

        // Если есть props isShowTime то выводим обратный отсчет
        if (isShowTime) {
            if (is_paused) {
                return <span className="icon-pause" />;
            } else {
                return <span>{this.state.time_amount}</span>;
            }
        }

        return(
            <span />
        );
    }
}

Timer.propTypes = {
    start: PropTypes.number.isRequired,
    isShowTime: PropTypes.bool,
    shutDown: PropTypes.bool,
    expired: PropTypes.func,
    is_paused: PropTypes.bool,
};

Timer.defaultProps = {
    isShowTime: false,
    shutDown: false,
    expired: () => {},
    is_paused: false
};
