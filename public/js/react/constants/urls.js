const URLS = {
    CSRF: {
        GET: '/auth/csrf-token'
    },
    LOGIN: '/login',
    REGISTER: '/register',
    PHONE: {
        CREATE: '/phone/create',
        EDIT: '/phone/edit',
        CONFIRM_CODE: '/phone/confirm',
        RESEND_CODE: '/phone/confirm/code-resend',
        CONFIRM_TOKEN: '/phone/edit-confirm',
        RESEND_TOKEN: '/phone/confirm/edit-code-resend'
    },
    EMAIL: {
        EDIT: '/email/edit',
        CONFIRM: '/email/confirm',
        RESEND: '/email/resend'
    },
    PROFILE: '/profile',
    PROFILE_PURCHASE: '/profile/purchase',

    FORM: {
        ADVERT_CREATE: '/product/create',
        ADVERT_CREATE_SAVE: '/product',
        UPLOAD_PRODUCT_IMAGE: '/product/image/upload',
        PRODUCT_EDIT_PREVIEW: (productId) => `/product/${productId}/edit`,
        PRODUCT_EDIT_CONFIRM: (productId) => `/product/${productId}`,
        PURCHASE_CREATE: '/purchase/create',
        PURCHASE_CREATE_SAVE: '/purchase',
        PURCHASE_EDIT_PREVIEW: (purchaseId) => `/purchase/${purchaseId}/edit`,
        PURCHASE_EDIT_CONFIRM: (purchaseId) => `/purchase/${purchaseId}`,
    },

    CATEGORIES_LIST: '/api/categories',

    PRODUCT_SEARCH: '/product/search',

    USER_AVATAR_CHANGE: '/profile',

    PRODUCTS: {
        BASE: '/product',
        PRODUCTS_INACTIVE: '/product/inactive',
        PRODUCTS_ARE_PUBLISHED: '/published-products', // Общий для модератора и пользователя
        PRODUCTS_ARCHIVE: '/product/archive', // Общий для модератора и пользователя
    },

    PURCHASES: {
        BASE: '/purchase',
        PURCHASES_INACTIVE: '/purchase/inactive',
        PURCHASES_ARE_PUBLISHED: '/purchase/public', // Общий для модератора и пользователя
        PURCHASES_ARCHIVE: '/archive-purchase', // Общий для модератора и пользователя
    },

    MODERATION: {
        // Продукты
        MODERATION_PRODUCTS_ARE_WAITING: '/product/moderation/waiting',
        MODERATION_PRODUCTS_ARE_DECLINED: '/product/moderation/declined',
        // Закупки
        MODERATION_PURCHASES_ARE_WAITING: '/purchase/moderation/waiting',
        MODERATION_PURCHASES_ARE_DECLINED: '/purchase/moderation/declined',
        MODERATE_PURCHASE: '/purchase/moderation/',
    }
};

export default URLS;