const VALIDATION_RULES = {
    BANK_NAME: {
        min_chars: 2,
        max_chars: 64
    },
    ACCOUNT_NUMBER: {
        chars_count: 20
    },
    ADDRESS: {
        min_chars: 6,
        max_chars: 1024
    },
    BANK_TRANSFER_NAME: {
        min_chars: 2,
        max_chars: 64
    },
    BIK: {
        chars_count: 9
    },
    BIRTH_DATE: {
        min_year_old: 6
    },
    CORR_ACCOUNT_NUMBER: {
        chars_count: 20
    },
    DELIVERY_PERIOD: {
        min_days: 1
    },
    FIRST_NAME: {
        min_chars: 2,
        max_chars: 45,
        min_words: 1,
        max_words: 2
    },
    LAST_NAME: {
        min_chars: 2,
        max_chars: 45,
        min_words: 1,
        max_words: 2
    },
    SECONDARY_NAME: {
        min_chars: 2,
        min_words: 1,
        max_words: 3
    },
    SOLE_PROPRIETOR_NAME: {
        max_chars: 45,
        max_words: 5
    },
    INN: {
        min_chars: 10,
        max_chars: 12
    },
    KPP: {
        min_chars: 5,
        max_chars: 9
    },
    OGRN: {
        min_chars: 13,
        max_chars: 13
    },
    LEGAL_ENTITY_NAME: {
        min_chars: 2,
        max_chars: 150
    },
    LOGIN: {
        min_chars: 3,
        max_chars: 16,
        max_words: 1
    },
    PASSWORD: {
        min_chars: 6
    },
    ADV_NAME: {
        max_chars: 50
    },
    PURCHASE_NAME: {
        max_chars: 200
    },
    ADV_DESCRIPTION: {
        min_chars: 5,
        max_chars: 3000,
    },
    ADVERT_USER_NAME: {
        min_chars: 3,
        max_chars: 50,
    }
};

export default VALIDATION_RULES;