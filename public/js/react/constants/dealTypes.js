const DEAL_TYPES = {
    1: {id: 1, ident: 'U', name: 'Услуга'},
    2: {id: 2, ident: 'T', name: 'Товар'}
};

export default DEAL_TYPES;