export const MODERATION_PRODUCTS_STATUS_NAMES = {
    MODERATION_PRODUCTS_ARE_WAITING: 'moderationProductsAreWaiting',
    MODERATION_PRODUCTS_ARE_DECLINED: 'moderationProductsAreDeclined',
    MODERATION_PRODUCTS_ARE_PUBLISHED: 'moderationProductsArePublished',
    MODERATION_PRODUCTS_ARCHIVE: 'moderationProductsArchive',
};

export const MODERATION_PURCHASES_STATUS_NAMES = {
    MODERATION_PURCHASES_ARE_WAITING: 'moderationPurchasesAreWaiting',
    MODERATION_PURCHASES_ARE_DECLINED: 'moderationPurchasesAreDeclined',
    MODERATION_PURCHASES_ARE_PUBLISHED: 'moderationPurchasesArePublished',
    MODERATION_PURCHASES_ARCHIVE: 'moderationPurchasesArchive',
};

export const USER_PRODUCTS_STATUS_NAMES = {
    USER_PRODUCTS_ARE_ACTIVE: 'userProductsAreActive',
    USER_PRODUCTS_ARE_INACTIVE: 'userProductsAreInactive',
    USER_PRODUCTS_ARCHIVE: 'userProductsArchive'
};

export const USER_PURCHASES_STATUS_NAMES = {
    USER_PURCHASES_ARE_ACTIVE: 'userPurchasesAreActive',
    USER_PURCHASES_ARE_INACTIVE: 'userPurchasesAreInactive',
    USER_PURCHASES_ARCHIVE: 'userPurchasesArchive'
};