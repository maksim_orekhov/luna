import React from 'react';
// import PropTypes from 'prop-types';
import FormBase from './FormBase';
import URLS from '../constants/urls';
import { customFetch, getMetaCSRF, getPurchaseModerationCreatedDateDotFormat } from '../Helpers';
import PurchasePreview from '../components/PurchasePreview';
import SelectImitation from '../components/SelectImitation';
import Description from '../controls/ControlDescription';


export default class FormPurchaseModerate extends FormBase {
    constructor(props) {
        super(props);

        this.state = {
            isDeclineReasonOpened: false,
            selectImitationName: 'decline_reason',
            form: {
                csrf: '',
                decline_reason: 0,
                decline_reason_description: '',
                decline_reason_recommendations: ''
            },
            error: ''
        };

        this.updateValidationFunctions = [];

        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.getActiveTab = this.getActiveTab.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillMount() {
        const csrf = getMetaCSRF();
        if (csrf) {
            this.setState({
                form: {
                    ...this.state.form,
                    csrf
                }
            });
        } else {
            this.getCsrf();
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const { purchase } = this.props;
        const { csrf } = this.state.form;
        customFetch(URLS.MODERATION.MODERATE_PURCHASE + purchase.id, {
            method: 'POST',
            body: JSON.stringify({
                csrf,
                approved: 1
            })
        }).then(data => {
            window.location.reload();
            console.log(data);
        });
    };

    handleDecline = (e) => {
        e.preventDefault();

        const { purchase } = this.props;
        const { csrf, decline_reason, decline_reason_description } = this.state.form;
        let description = '';

        if (!decline_reason) {
            this.setState({
                error: 'Пожалуйста, выберите причину из списка'
            });
            return;
        }

        if (decline_reason_description === '') {
            description = this.getOptions()[decline_reason - 1].title;
        } else {
            description = decline_reason_description;
        }
        let recommendations = this.getOptions()[decline_reason - 1].recommendation;
        customFetch(URLS.MODERATION.MODERATE_PURCHASE + purchase.id, {
            method: 'POST',
            body: JSON.stringify({
                csrf,
                approved: 0,
                description: description,
                decline_reason_recommendations: recommendations
            })
        }).then(data => {
            window.location.reload();
        });
    };

    getOptions = () => {
        return [
            {
                title: 'Контактная информация в названии',
                recommendation: 'Почему отклонили: в названии объявления использованы контактные данные: номер телефона, почтовый адрес, ссылки, ID мессенджеров.\n' +
                '\n' +
                'Как исправить: удалите контакты из названия.',
                value: 1
            },
            {
                title: 'Контактная информация на фото или видео',
                recommendation: 'Почему отклонили: на фотографиях или превью видео в объявлении используются контакты: номер телефона, почтовый адрес, ссылки, QR-коды, ID мессенджеров.\n' +
                '\n' +
                'Как исправить: удалите фотографии с контактной информацией. Если в объявление добавлено видео, проверьте и исправьте превью. Используйте фотографии, которые сделали самостоятельно.',
                value: 2
            },
            {
                title: 'Контакты и ссылки в описании',
                recommendation: 'Почему отклонили: в описании объявления присутствует контактная информация: номер телефона, почтовый адрес, ссылки, ID мессенджеров.\n' +
                '\n' +
                'Как исправить: удалите из описания контактную информацию.',
                value: 3
            },
            {
                title: 'Неправильная категория',
                recommendation: 'Почему отклонили: для объявления выбрана неверная категория.\n' +
                '\n' +
                'Как исправить: укажите правильную категорию.',
                value: 4
            },
            {
                title: 'Нереалистичная цена',
                recommendation: 'Почему отклонили: поле Цена заполнено неправильно. Возможные ошибки:\n' +
                '\n' +
                'неполная цена (например, вместо 150000 рублей указано 150 рублей);\n' +
                'цена указана не в рублях;\n' +
                'указана цена за квадратный метр (в категории «Недвижимость»);\n' +
                'указан номер телефона вместо цены;\n' +
                'указан диапазон цен (например: 200-350 рублей);\n' +
                'указана цена сразу за несколько товаров.\n' +
                'Как исправить: исправьте цену на правильную.',
                value: 5
            },
            {
                title: 'Неверный параметр Вид объявления',
                recommendation: 'Почему отклонили: для объявления выбран неверный параметр Вид объявления. Вместо «Товар приобретён на продажу» или «Товар от производителя» вы указали «Продаю своё».\n' +
                '\n' +
                'В категории «Автомобили» вместо параметра «Автомобиль приобретён на продажу» вы указали «Продаю личный автомобиль».\n' +
                '\n' +
                'В категории «Животные» вместо параметра «Продаю как заводчик» вы указали «Отдаю даром» или «Продаю как частное лицо».\n' +
                '\n' +
                'Как исправить: укажите верное значение для параметра Вид объявления.',
                value: 6
            },
            {
                title: 'Ключевые слова в описании',
                recommendation: 'Почему отклонили: в описании объявления перечислены ключевые слова (теги). Мы считаем ключевыми словами перечисление слов, которые не имеют отношения к объявлению.\n' +
                '\n' +
                'Как исправить: удалите ключевые слова для поиска из описания объявления. Если вы продаете много схожих товаров в одном объявлении — опишите их подробно и укажите цену за каждый из них.',
                value: 7
            },
            {
                title: 'Несколько предложений (услуг / товаров / вакансий) в одном объявлении',
                recommendation: 'Почему отклонили: в вашем объявлении предлагаются несколько услуг, товаров или вакансий.\n' +
                '\n' +
                'В одном объявлении можно предлагать несколько товаров, только если они относятся к одной категории или составляют комплект. Например, можно подать объявление о продаже 3 женских платьев или комплекта для рыбалки (костюм+удочка). Однако в категориях «Недвижимость», «Транспорт» и в подкатегории «Вакансии» необходимо публиковать отдельное объявление для каждого товара или услуги.\n' +
                '\n' +
                'Как исправить: оставьте в объявлении одно предложение. Для остальных — создайте другие объявления.\n' +
                '\n' +
                'Исключение: вы можете искать сотрудника на смежные должности в одном объявлении. Например, можно подать объявление, если вы ищите шиномонтажника и автоэлектрика в одном лице',
                value: 8
            },
            {
                title: 'Другое',
                recommendation: '',
                value: 9
            }
        ];
    };

    handleChangeSelect = (e) => {
        const {value} = e;
        const { handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;
        const { selectImitationName } = this.state;

        this.setState({
            error: ''
        });

        this.handleChange && this.handleChange(selectImitationName, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    };

    getActiveTab() {
        return this.getOptions()[0].title;
    }

    toggleDeclineForm = (e) => {
        e.preventDefault();
        this.setState({
            isDeclineReasonOpened: !this.state.isDeclineReasonOpened
        });
    };

    closeDeclineForm = (e) => {
        e.preventDefault();
        this.setState({
            isDeclineReasonOpened: !this.state.isDeclineReasonOpened,
            form: {
                ...this.state.form,
                decline_reason: 0,
                decline_reason_description: '',
                decline_reason_recommendations: ''
            }
        });
    };

    render() {
        const { isDeclineReasonOpened, error } = this.state;
        const { decline_reason_description, decline_reason } = this.state.form;
        const { purchase } = this.props;

        const created_formatted = getPurchaseModerationCreatedDateDotFormat(purchase);

        return (
            <div className="popup advert_actions_popup">
                <div className="advert_actions_popup_header">
                    <h4 className="popup-title">Модерация закупки</h4>
                    <div className="advert_info">
                        <table>
                            <tbody>
                                <tr>
                                    <td className="prop_name">
                                    Создал:
                                    </td>
                                    <td className="value">
                                        <a href="">{purchase.owner.name}</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td className="prop_name">
                                    На модерации с:
                                    </td>
                                    <td className="value">
                                        {created_formatted}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="advert_actions_popup_content">
                    <PurchasePreview
                        name={purchase.name}
                        price={purchase.price}
                        quantity={purchase.quantity}
                        measure={purchase.measure}
                        description={purchase.description}
                        category={purchase.category}
                        sub_category={purchase.sub_category}
                        files={purchase.files}
                        address={purchase.address}
                        user={purchase.owner}
                        purchase_url={purchase.urls.single_purchase_public}
                        not_show_public_link={true}
                    />
                </div>
                {
                    isDeclineReasonOpened &&
                    <div className="advert_moderate_decline_reason_form">
                        <div className="row">
                            <SelectImitation
                                options={this.getOptions()}
                                defaultValue={this.getActiveTab()}
                                placeholder="Выберите причину отклонения"
                                label="Причина отклонения:"
                                onChangeHandler={this.handleChangeSelect}
                            />
                        </div>
                        {
                            decline_reason === 9 &&
                            <Description
                                label="Описание причины:"
                                name="decline_reason_description"
                                handleComponentChange={this.handleChange}
                                handleComponentValidation={this.handleComponentValid}
                                value_prop={decline_reason_description}
                            />
                        }
                        {
                            error &&
                            <div className="row" style={{paddingTop: '10px', color: 'red'}}>
                                <p className="error">{error}</p>
                            </div>
                        }
                        <div className="advert_actions_popup_buttons">
                            <button className="button-popup button-popup-white" type="button" onClick={this.closeDeclineForm}>Отмена</button>
                            <button className="button-popup button-purple" type="button" onClick={this.handleDecline}>Подтвердить отклонение</button>
                        </div>
                    </div>
                }
                <div className="advert_actions_popup_buttons">
                    <button className="button-popup button-purple" type="button" onClick={this.handleSubmit}>Подтвердить публикацию</button>
                    <button className="button-popup button-popup-white" type="button" onClick={this.toggleDeclineForm}>Отклонить публикацию</button>
                </div>
            </div>
        );
    }
}

// FormAdvertActions.propTypes = {
//     submitHandler: PropTypes.func.isRequired,
//     serverErrors: PropTypes.object,
// };