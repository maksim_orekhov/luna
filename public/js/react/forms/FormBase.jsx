import React from 'react';
import MESSAGES from '../constants/messages';
import URLS from '../constants/urls';
import REQUEST_CODES from '../constants/request_codes';
import { customFetch } from '../Helpers';

export default class FormBase extends React.Component {
    getCsrf() {
        return customFetch(URLS.CSRF.GET)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    const { csrf = '' } = data.data;
                    this.setState({
                        form: {
                            ...this.state.form,
                            csrf
                        }
                    }, () => {
                        return Promise.resolve();
                    });
                } else if (data.status === 'ERROR') {
                    return Promise.reject(data.message);
                }
            })
            .catch(error => {
                this.setFormError('form_init_fail', true);
            });
    }

    handleChange(name, value) {
        this.setState({
            form: {
                ...this.state.form,
                [name]: value
            }
        }, () => this.saveFormToLocalStorage());
    }

    saveFormToLocalStorage() {
        if (localStorage && this.form_name) {
            const form = {...this.state.form};
            delete form.csrf; // Удаляем csrf

            localStorage.setItem(this.form_name, JSON.stringify(form));
        }
    }

    getFormFromLocalStorage() {
        return new Promise((resolve, reject) => {
            if (localStorage && this.form_name && localStorage.hasOwnProperty(this.form_name)) {
                const form = JSON.parse(localStorage.getItem(this.form_name));

                this.setState({
                    form: {
                        ...this.state.form,
                        ...form
                    }
                }, () => {
                    resolve();
                });
            } else {
                resolve();
            }
        });
    }

    clearFormLocalStorage() {
        if (localStorage && this.form_name && localStorage.hasOwnProperty(this.form_name)) {
            localStorage.removeItem(this.form_name);
        }
    }

    makeValidateUpdateFunction(name, value) {
        return (() => {
            this.can_update_validation = false;

            this.setState({
                validation: {
                    ...this.state.validation,
                    [name]: value
                }
            }, () => {
                this.checkAllControlsAreValid();
                this.can_update_validation = true;
            });
        });

    }

    handleComponentValid(name, value) {
        this.updateValidationFunctions.push(this.makeValidateUpdateFunction(name, value));
    }

    checkAllControlsAreValid() {
        const validation_values = this.state.validation;

        let form_isValid = true;

        Object.keys(validation_values).forEach(value => {
            if (!validation_values[value]) {
                form_isValid = false;
            }
        });
        if (this.props.editMode) {
            const current_state = JSON.stringify(this.state.form);
            if (current_state === this.prev_state) {
                form_isValid = false;
            }
        }
        this.setState({
            form_isValid
        }, () => this.sendUpFormData());
    }

    sendUpFormData() {
        if (this.props.is_nested) {
            const { form_isValid } = this.state;
            const { sendUpData, handleComponentValidation, form_validate_name, form_data_name } = this.props;

            handleComponentValidation && handleComponentValidation(form_validate_name, form_isValid);
            if (form_isValid) {
                sendUpData && sendUpData(form_data_name, this.getDataFromState());
            }
        }
    }

    getDataFromState() {
        return this.state.form;
    }

    toggleIsLoading(name = 'form_isSubmitting') {
        this.setState({
            form_isLoading: !this.state.form_isLoading,
            [name]: !this.state[name]
        });
    }

    /**
     * При ответе бэка о невалидности формы распределяем ошибки по их контролам
     * @param {Object} errors - Объект с ошибками с бэка
     */
    TakeApartInvalidFormData(errors) {
        this.setFormError('invalid_form_data', true);

        if (errors) {
            const controls_server_errors = {...this.state.controls_server_errors};

            Object.keys(errors).forEach(control => {
                if (control === 'csrf') {
                    this.setFormError('csrf', true);
                }
                controls_server_errors[control] = errors[control];
            });

            this.setState({
                controls_server_errors
            });
        }
    }

    setUndefinedFormError(data) {
        this.setFormError('undefined_error', true);

        this.setState({
            server_errors_messages: {
                ...this.state.server_errors_messages,
                undefined_error: data.message
            }
        });
    }

    /**
     * Зажигаем флаг ошибки с бэка
     */
    setFormError(name, value) {
        const server_errors = {...this.state.server_errors}; // Очищаем флаги прошлого запроса
        Object.keys(server_errors).forEach(error => {
            server_errors[error] = false;
        });

        this.setState({
            server_errors: {
                ...server_errors,
                [name]: value
            }
        });
    }

    /**
     * Кастомная функция разбора ошибок с бэка и обновления нужного флага ошибки
     * @param {Object} data - объект ответа с бэка
     */
    TakeApartErrorFromServer(data) {
        switch (data.code) {
            case REQUEST_CODES.INVALID_FORM_DATA:
                this.TakeApartInvalidFormData(data.data);
                break;
            case REQUEST_CODES.ACCESS_DENIED:
                this.setFormError('access_denied', true);
                break;
            case REQUEST_CODES.NOT_AUTHORIZED:
                this.setFormError('not_auth', true);
                window.location.reload();
                break;
            case REQUEST_CODES.PHONE_INVALID_CODE:
                this.setFormError('invalid_code', true);
                break;
            case REQUEST_CODES.PHONE_COUNTRY_NOT_DEFINED:
                this.setFormError('country_not_defined', true);
                break;
            case REQUEST_CODES.ALREADY_AUTHORIZED:
                this.setFormError('already_auth', true);
                break;
            case REQUEST_CODES.AUTHENTICATION_FAILED:
                this.setFormError('auth_failed', true);
                break;
            case REQUEST_CODES.CLS_CONFIRMED_DEAL_REMOVAL:
                this.setFormError('cls_confirmed_deal_removal', true);
                break;
            case REQUEST_CODES.CLS_LINKED_WITH_TOKEN_REMOVAL:
                this.setFormError('cls_linked_with_token_removal', true);
                break;
            default:
                this.setUndefinedFormError(data);
        }
    }

    // Метод для контрола CivilLawSubjectAndPaymentMethod
    handleChangeCivilLawSubject(name, value) {
        this.setState({
            form: {
                ...this.state.form,
                payment_method: '',
                [name]: value
            }
        });
    }

    // Метод для контрола CivilLawSubjectAndPaymentMethod
    handleCivilLawSubjectValid(name, value) {
        this.updateValidationFunctions.push(this.makeValidateUpdateFunction(name, value));
        this.updateValidationFunctions.push(this.makeValidateUpdateFunction('payment_method_is_valid', false));
    }

    handleStateChange(name, value) {
        this.setState({
            [name]: value
        });
    }

    // В этом методе разбираем ошибки полученные из компонента сверху, если форма вложена
    SetControlsErrorFromUpperForm(currentErrors, nextErrors) {
        if (JSON.stringify(currentErrors) !== JSON.stringify(nextErrors)) {
            if (nextErrors) {
                const controls_server_errors = {...this.state.controls_server_errors};

                Object.keys(nextErrors).forEach(control => {
                    controls_server_errors[control] = nextErrors[control];
                });

                this.setState({
                    controls_server_errors
                });
            }
        }
    }

    // Проверяем очередь для обновления флагов валидация контрола в state
    tryToRunValidationFunctions() {
        if (this.updateValidationFunctions.length && this.can_update_validation) {
            (this.updateValidationFunctions.shift())();
        }
    }

    rememberState() {
        this.prev_state = JSON.stringify(this.state.form);
        this.checkAllControlsAreValid();
    }



    /**
     * Возвращает объект для селекта с категориями верхнего уровня
     * */
    getParentCategories = (categories) => {
        return categories.map((category) => {
            return {
                title: category.name,
                value: category.id
            };
        });
    };

    /**
     * Возвращает объект для селекта с подкатегориями
     * */
    getSubcategoriesByParentCategoryId = (parentCategoryId, categories = []) => {
        if(!parentCategoryId || categories.length === 0) {
            return [];
        }
        const parentCategory = categories.find(category => category.id == parentCategoryId);
        if(!parentCategory || !parentCategory.subcategories || parentCategory.subcategories.length === 0) {
            return [];
        }

        return parentCategory.subcategories.map((category) => {
            return {
                title: category.name,
                value: category.id
            };
        });
    };

    /**
     * Коллбэк прохождения капчи
     * @param {string} captchaResponse - код пройденной капчи
     */
    handleCaptchaChange = (captchaResponse) => {
        const { form } = this.state;
        this.handleComponentValid('captcha_is_valid', true);
        this.setState({
            form: {
                ...form,
                captcha: captchaResponse
            }
        });
    };

    /**
     * Коллбэк истёкшей капчи
     */
    handleCaptchaExpired = () => {
        const { form } = this.state;
        this.handleComponentValid('captcha_is_valid', false);
        this.setState({
            form: {
                ...form,
                captcha: ''
            }
        });
    };

    /**
     * Получает ID виджета капчи
     * @param widgetId
     */
    setCaptchaWidgetId = (widgetId) => {
        this.captchaWidgetId = widgetId;
    };

    /**
     * Сбрасывает капчу
     */
    resetCaptcha = () => {
        const { form } = this.state;
        window.grecaptcha.reset(this.captchaWidgetId);
        this.handleComponentValid('captcha_is_valid', false);
        this.setState({
            form_isValid: false,
            form: {
                ...form,
                captcha: '',
            }
        });
    };

    setFormErrorAnimation  = () =>  {
        const { formHasError } = this.state;
        if (!formHasError) {
            this.setState({
                formHasError: true
            }, () => {
                setTimeout(() => {
                    this.setState({
                        formHasError: false
                    });
                }, 600);
            });
        }
    };

}