import React from 'react';
import PropTypes from 'prop-types';
import FormBase from './FormBase';
import PhoneConfirmCode from '../controls/ControlPhoneConfirmCode';
import LoaderSpinner from '../components/LoaderSpinner';
import Timer from '../Timer';
import ShowError from '../ShowError';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';
import { prettifyPhoneNumber } from '../Helpers';

export default class FormPhoneConfirmToken extends FormBase {
    constructor(props) {
        super(props);

        this.state = {
            sms_timer_time: 10,
            is_user_allowed_sms_request: false,
            is_limit_sms_code: false,
            is_email_verified: false,
            controls_server_errors: {
                code: null
            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validate_props
            validation: {
                code_is_valid: false
            },
            form: {
                code: '',
            },
            form_isValid: false,
        };

        this.prev_state = null;
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.toggleSmsTimer = this.toggleSmsTimer.bind(this);
        this.handleResendCode = this.handleResendCode.bind(this);
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    componentWillReceiveProps(nextProps) {
        if (Object.keys(nextProps.serverErrors).length > 0) {
            this.TakeApartErrorFromServer(nextProps.serverErrors);
        }
    }

    handleSubmit(e) {
        e.preventDefault();
        const { form_isValid, form: { code }, is_email_verified } = this.state;
        const { confirmTokenHandler, confirmTokenSMSHandler, csrf, form_type } = this.props;

        if (form_isValid) {
            confirmTokenHandler(JSON.stringify({
                code,
                csrf: csrf.value
            }), form_type);
            // if (is_email_verified) {
            //     confirmTokenHandler(JSON.stringify({
            //         code,
            //         csrf: csrf.value
            //     }), form_type);
            // } else {
            //     confirmTokenSMSHandler(JSON.stringify({
            //         code,
            //         csrf: csrf.value
            //     }), form_type);
            // }
        }
    }

    handleResendCode() {
        const { csrf, resendPhoneToken } = this.props;
        this.setState({
            is_user_allowed_sms_request: false,
        }, () => {
            resendPhoneToken(JSON.stringify({
                csrf: csrf.value
            }));
        });
    }

    toggleSmsTimer() {
        this.setState({
            is_user_allowed_sms_request: true
        });
    }

    render() {
        const { sms_timer_time, is_user_allowed_sms_request, is_limit_sms_code, server_errors, is_email_verified } = this.state;
        const { phoneNumber, serverErrors, tokenIsSending, openPrevForm, form_type, is_phone_verified } = this.props;

        return (
            <div className="popup phone-accept-code-popup">
                <div className="form-popup">
                    <form onSubmit={this.handleSubmit}>
                        <h4 className="popup-title">{form_type === 'add' ? 'Добавление телефона' : 'Изменение телефона'}</h4>
                        <div className="control-form">
                            <input type="text" name="" value={prettifyPhoneNumber(phoneNumber)} disabled />
                        </div>
                        <div className="form-popup__info-text">
                            {`На Ваш ${is_email_verified ? 'e-mail' : 'телефон'} был отправлен код подтверждения, который необходимо ввести в форму ниже.`}
                        </div>
                        <div className="form-popup__footer form-footer-controls">
                            <PhoneConfirmCode
                                name="code"
                                placeholder="Введите код"
                                handleComponentChange={this.handleChange}
                                handleComponentValidation={this.handleComponentValid}
                                form_control_server_errors={serverErrors}
                            />
                            <button className="button-purple" type="submit">Подтвердить</button>
                        </div>
                        {
                            !is_limit_sms_code ?
                                is_user_allowed_sms_request ?
                                    <div className="form-popup__notice">
                                        <button
                                            className="button-purple"
                                            // disabled={form_isLoading || confirm_isLoading}
                                            onClick={this.handleResendCode}
                                            data-ripple-button=""
                                            type="submit"
                                        >
                                                Получить код повторно
                                        </button>
                                    </div>
                                    :
                                    <div className="form-popup__notice">
                                            Запросить код повторно через <span className="form-popup__repeat-time-left">
                                            <Timer
                                                start={sms_timer_time}
                                                expired={this.toggleSmsTimer}
                                                isShowTime={true}
                                            />
                                        </span> или <a href="#" className="form-popup__notice-link" onClick={openPrevForm}>изменить номер</a>.
                                    </div>
                                :
                                <p className="TextImportant">Вы достигли лимита получения кодов и теперь сможете получить код повторно только через 15 минут.</p>
                        }
                        <ShowError messages={this.state.server_errors_messages} existing_errors={server_errors} />
                        {
                            tokenIsSending &&
                            <LoaderSpinner isAbsolute={true} />
                        }
                    </form>
                </div>
            </div>
        );
    }
}


FormPhoneConfirmToken.propTypes = {
    serverErrors: PropTypes.object.isRequired,
    csrf: PropTypes.object.isRequired,
    tokenIsSending: PropTypes.bool.isRequired,
    phoneNumber: PropTypes.string,
    openPrevForm: PropTypes.func,
    confirmTokenHandler: PropTypes.func,
    confirmTokenSMSHandler: PropTypes.func,
    resendPhoneToken: PropTypes.func,
};
FormPhoneConfirmToken.defaultProps = {
    phoneNumber: '',
    openPrevForm: () => {},
    confirmTokenHandler: () => {},
    resendPhoneToken: () => {},
};
