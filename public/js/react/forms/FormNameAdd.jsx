import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import FormBase from './FormBase';
import Name from '../controls/ControlShopName';
import ShowError from '../ShowError';
import LoaderSpinner from '../components/LoaderSpinner';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';
import { changeUserName, loadCSRF } from '../actions';

class FormNameAdd extends FormBase {
    constructor(props) {
        super(props);

        this.state = {
            controls_server_errors: {
                new_login: null
            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validate_props
            validation: {
                new_login_is_valid: false
            },
            form: {
                new_login: '',
            },
            form_isValid: false,
            isChanged: false,
        };

        this.prev_state = null;
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleCloseForm = this.handleCloseForm.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.handleStateChange = this.handleStateChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount () {
        const { csrf, loadCSRF } = this.props;
        if (!csrf.isLoaded && !csrf.isLoading) {
            loadCSRF();
        }
    }

    componentWillReceiveProps(nextProps) {
        if (Object.keys(nextProps.serverErrors).length > 0) {
            this.TakeApartErrorFromServer(nextProps.serverErrors);
        }
    }

    componentDidUpdate(prevProps) {
        const { userName } = this.props;
        if (userName !== prevProps.userName) {
            this.setState({ isChanged: true });
        }

    }

    componentWillUpdate () {
        this.tryToRunValidationFunctions();
    }

    handleCloseForm () {
        this.props.handleClosePopup();
    }

    handleSubmit (e) {
        e.preventDefault();
        const { new_login } = this.state.form;
        const { csrf, changeUserName } = this.props;

        if (this.state.form_isValid) {
            changeUserName(JSON.stringify({
                name: new_login,
                csrf: csrf.value,
            }));
        }
    }

    render () {
        const { isChanged, server_errors_messages, form: { new_login }, server_errors } = this.state;
        const { csrf, nameIsChanging, userName, serverErrors } = this.props;

        if (csrf.isLoading || csrf.isFailed) {
            return null;
        }

        if (isChanged) {
            return (
                <div className="popup create-shop-form">
                    <div className="form-popup">
                        <h4 className="popup-title">Имя в объявлениях</h4>
                        <div className="info-field">
                            <div className="info-field__editable-text-field">
                                <div className="info-field-text" title={userName}>{userName}</div>
                            </div>
                        </div>
                        <div className="form-popup__info-text">
                            Так к вам будут обращаться продавцы или покупатели.
                        </div>
                    </div>
                </div>
            );
        }

        return (
            <div className="popup create-shop-form">
                <div className="form-popup">
                    <form onSubmit={this.handleSubmit}>
                        <h4 className="popup-title">Имя в объявлениях</h4>
                        <div className="form-popup__info-text">
                            Так к вам будут обращаться продавцы или покупатели.
                        </div>
                        <Name
                            name="new_login"
                            placeholder="Введите имя"
                            handleComponentChange={this.handleChange}
                            handleComponentValidation={this.handleComponentValid}
                            value_prop={new_login}
                        />
                        <div className="form-popup__footer">
                            <button className="button-transparent-with-stroke" type="button" onClick={this.handleCloseForm}>Отменить</button>
                            <button className="button-blue" type="submit" onClick={this.handleSubmit}>Сохранить</button>
                        </div>
                        <ShowError messages={server_errors_messages} existing_errors={server_errors} />
                        {
                            nameIsChanging &&
                            <LoaderSpinner isAbsolute={true} />
                        }
                    </form>
                </div>
            </div>
        );
    }
}

FormNameAdd.propTypes = {
    handleClosePopup: PropTypes.func,
    // redux state
    csrf: PropTypes.object.isRequired,
    serverErrors: PropTypes.object.isRequired,
    userName: PropTypes.string.isRequired,
    nameIsChanging: PropTypes.bool.isRequired,
    // redux dispatch
    loadCSRF: PropTypes.func.isRequired,
    changeUserName: PropTypes.func.isRequired,
};

FormNameAdd.propTypes = {
    handleClosePopup: () => {},
};

const mapStateToProps = (state) => {
    return {
        csrf: state.config.csrf,
        nameIsChanging: state.user.isChanging.name,
        userName: state.user.data.name,
        serverErrors: state.user.errors.name,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        changeUserName: bindActionCreators(changeUserName, dispatch),
        loadCSRF: bindActionCreators(loadCSRF, dispatch),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FormNameAdd);
