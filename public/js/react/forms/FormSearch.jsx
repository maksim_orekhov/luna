import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SelectImitation from '../components/SelectImitation';
import { getParentCategories, getSubcategoriesByParentCategoryId } from '../Helpers';
import URL from '../constants/urls';
import ControlSearchQuery from '../controls/ControlSearchQuery';

class FormSearch extends Component {
    constructor (props) {
        super(props);
        this.state = {
            form: {
                categoryId: props.initialData.category,
                subCategoryId: props.initialData.sub_category,
                query:  props.initialData.search,
                only_fabricator: !!props.initialData.fabricator || false
            },
            is_advanced_search_opened: false
        };
    }

    handleCategoryChange = ({name, value}) => {
        const { categories, fetchSubcategories } = this.props;
        const nextCategory =  categories.entities.find(category => category.id === value);
        if (value !== null && !nextCategory.subcategories) {
            fetchSubcategories(value);
        }
        this.handleSelectChange({name, value});
    };

    handleControlChange = (ev) => {
        const { form } = this.state;
        this.setState({
            form: {
                ...form,
                [ev.target.name]: ev.target.value
            }
        });
    };

    handleControlChangeCheckbox = (ev) => {
        const { form } = this.state;
        this.setState({
            form: {
                ...form,
                [ev.target.name]: ev.target.checked
            }
        });
    };

    handleSelectChange = ({ value, name }) => {
        const { form } = this.state;
        this.setState({
            form: {
                ...form,
                [name]: value
            }
        });
    };

    handleSubmit = (ev) => {
        const { sendForm } = this.props;
        const { form } = this.state;
        ev.preventDefault();
        sendForm(form);
    };

    toggleAdvancedSearch = () => {
        this.setState({
            is_advanced_search_opened: !this.state.is_advanced_search_opened
        });
    };

    handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            document.activeElement.blur && document.activeElement.blur();
        }
    };

    render(){
        const { form: { subCategoryId, categoryId, query, only_fabricator }, is_advanced_search_opened } = this.state;
        const { categories, redirectHandler, catalogName, searchURL, is_search_only_fabricator, search_value, handleChangeSearchValue } = this.props;
        const placeholder = catalogName === 'product' ? 'Поиск по объявлениям' : 'Поиск по закупкам';

        return (
            <div>
                <div className="row mobile-search-first-screen">
                    <div className="col-tablet-8">
                        <div className="control-form">
                            <input className="input-search" type="search" value={search_value} onKeyPress={this.handleKeyPress} onChange={handleChangeSearchValue} placeholder={placeholder} />
                            <a className="icon-filter js-mobile-button-filter" onClick={this.toggleAdvancedSearch} />
                        </div>
                    </div>
                </div>
                <div className={`row search-main-block js-mobile-main-filter ${is_advanced_search_opened ? 'opened-mobile' : ''}`}>
                    <div className={`row search-main-block ${is_advanced_search_opened ? 'opened-mobile' : ''}`}>
                        <div className="header-of-element">
                            <span className="button-close-white js-mobile-button-close-filter open" onClick={this.toggleAdvancedSearch}>&nbsp;</span>
                            <h1>Расширенный поиск</h1>
                        </div>
                        <div className="row search-main-block-filter-wrap search-main-block-desktop">
                            <div className="col-4 col-tablet-4 col-mobile-8 col-small-4">
                                <div className="control-form">
                                    <ControlSearchQuery
                                        placeholder={placeholder}
                                        value={query}
                                        name="query"
                                        onChange={this.handleControlChange}
                                        searchURL={searchURL}
                                        redirectHandler={redirectHandler}
                                        only_fabricator={only_fabricator}
                                    />
                                </div>
                                {
                                    is_search_only_fabricator &&
                                    <div className="col-only-manufacturer">
                                        <label className="form-checkbox">
                                            <input type="checkbox" name="only_fabricator" checked={only_fabricator} onChange={this.handleControlChangeCheckbox}  />
                                            <span className="form-checkbox__field">&nbsp;</span>
                                            <span className="form-checkbox__label">Только от производителей</span>
                                            <span className="status-manufacturer">&nbsp;</span>
                                        </label>
                                    </div>
                                }
                            </div>
                            <div className="col-3 col-tablet-4 col-mobile-8 col-small-4 search-select">
                                <div className="control-form">
                                    <SelectImitation
                                        options={getParentCategories(categories.entities)}
                                        defaultValue={getParentCategories(categories.entities).length ? categoryId : null}
                                        label="Категория:"
                                        name="categoryId"
                                        placeholder="Во всех категориях"
                                        onChangeHandler={this.handleCategoryChange}
                                    />
                                </div>
                            </div>
                            <div className="col-3 col-tablet-4 col-mobile-8 col-small-4 search-select">
                                <div className="control-form">
                                    <SelectImitation
                                        options={getSubcategoriesByParentCategoryId(categoryId, categories.entities)}
                                        defaultValue={getSubcategoriesByParentCategoryId(categoryId, categories.entities).length ? subCategoryId : null}
                                        label="Подкатегория:"
                                        name="subCategoryId"
                                        placeholder="Во всех подкатегориях"
                                        onChangeHandler={this.handleSelectChange}
                                        isDisabled={getSubcategoriesByParentCategoryId(categoryId, categories.entities).length === 0}
                                    />
                                </div>
                            </div>
                            <div className="col-2 col-tablet-4 col-mobile-8 col-small-4 search-button">
                                <button className="button-blue" type="button" onClick={this.handleSubmit}>Найти</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

FormSearch.propTypes = {
    categories: PropTypes.object.isRequired,
    fetchSubcategories: PropTypes.func.isRequired,
    sendForm: PropTypes.func.isRequired,
    redirectHandler: PropTypes.func,
    initialData: PropTypes.object.isRequired,
    catalogName: PropTypes.string.isRequired,
    is_search_only_fabricator: PropTypes.bool,
};

FormSearch.defaultProps = {
    redirectHandler: () => {},
    is_search_only_fabricator: false
};

export default FormSearch;
