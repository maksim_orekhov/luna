import React from 'react';
import FormBase from './FormBase';
import FormNaturalPerson from './FormNaturalPerson';
import File from '../controls/ControlFile';
import FormBankTransfer from './FormBankTransfer';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';

export default class FormSecureDeal extends FormBase {
    constructor(props) {
        super(props);

        this.state = {

            controls_server_errors: {
                last_name: null,
                first_name: null,
                secondary_name: null,
                birth_date: null,
                bik: null,
                bank_name: null,
                account_number: null,
                corr_account_number: null
            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validate_props
            validation: {
                last_name_is_valid: false,
                first_name_is_valid: false,
                secondary_name_is_valid: false,
                birth_date_is_valid: false,
                bik_is_valid: false,
                bank_name_is_valid: false,
                account_number_is_valid: false,
                corr_account_number_is_valid: false
            },
            form: {
                last_name: '',
                first_name: '',
                secondary_name: '',
                birth_date: '',
                bik: '',
                bank_name: '',
                account_number: '',
                corr_account_number: '',
                files: []
            },
            form_isValid: false,
            form_isLoading: false,
            form_isSubmitting: false
        };

        this.prev_state = null;
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    componentWillUpdate = () => {
        this.tryToRunValidationFunctions();
    };

    handleCloseForm = () => {
        this.props.handleClosePopup();
    };

    handleStateChange = (name, value) => {
        this.setState({
            form: {
                ...this.state.form,
                [name]: value
            }
        });
    };

    render() {

        return (
            <div className="popup create-shop-form">
                <div className="form-popup">
                    <h4 className="popup-title">Подключение безопасной&nbsp;сделки</h4>
                    <FormNaturalPerson
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                        sendUpData={this.handleStateChange}
                    />
                    <File
                        label="Документ:"
                        name="files"
                        handleComponentChange={this.handleChange}
                    />
                    <FormBankTransfer
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                        sendUpData={this.handleStateChange}
                        title="Способ получения оплаты"
                    />
                    <div className="form-popup__footer">
                        <button className="button-transparent-with-stroke" type="button" onClick={this.handleCloseForm}>Отменить</button>
                        <button className="button-blue" type="submit">Создать</button>
                    </div>
                </div>
            </div>
        );
    }
}