import React from 'react';
import PropTypes from 'prop-types';
import FormBase from './FormBase';
import { getCookie, customFetch, getDateDotFormatFromTimestamp, getProductModerationCreatedDateDotFormat } from '../Helpers';
import AdvertPreview from '../components/ProductPreview';

export default class FormAdvertCheck extends FormBase {
    constructor(props) {
        super(props);

        this.state = {
            form: {
                csrf: ''
            }
        };

        // this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    componentWillMount() {
        this.getCsrf();
    }

    handleEditButtonClick = () => {
        const { id } = this.props.product;
        window.location = `/product/${id}/edit`;
    };

    handleDeleteButtonClick = () => {
        const { id } = this.props.product;
        const { csrf } = this.state.form;

        customFetch(`/product/${id}/archive`, {
            method: 'POST',
            body: JSON.stringify({
                csrf: csrf
            })
        })
            .then((response) => {
                window.sessionStorage.setItem('profileMessage', 'archive');
                window.location.reload();
            });
    };

    render() {
        const { product, is_operator } = this.props;

        const created_formatted = getDateDotFormatFromTimestamp(product.created_timestamp);
        const moderation_created_formatted = getProductModerationCreatedDateDotFormat(product);

        return (
            <div className="popup advert_actions_popup">
                <div className="advert_actions_popup_header">
                    <div className="advert_popup_header_title">
                        <div className="advert_popup_header_icon moderation_pending">&nbsp;</div>
                        <h4 className="popup-title">Ожидает модерации</h4>
                    </div>
                    <div className="advert_info">
                        <table>
                            <tbody>
                                <tr>
                                    <td className="prop_name">
                                        Дата создания:
                                    </td>
                                    <td className="value">
                                        {created_formatted}
                                    </td>
                                </tr>
                                <tr>
                                    <td className="prop_name">
                                        На модерации с:
                                    </td>
                                    <td className="value">
                                        {moderation_created_formatted}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="advert_actions_popup_content">
                    <AdvertPreview
                        name={product.name}
                        price={product.price}
                        description={product.description}
                        category={product.category}
                        sub_category={product.sub_category}
                        address={product.address}
                        galleryFiles={product.gallery}
                        files={product.files}
                        user={product.owner}
                        not_show_public_link={true}
                        show_full_size_photo={true}
                    />
                </div>
                {
                    !is_operator &&
                    <div className="advert_actions_popup_buttons">
                        <button className="button-popup button-popup-white" onClick={this.handleEditButtonClick} type="button">Редактировать</button>
                        <button className="button-popup button-popup-white" onClick={this.handleDeleteButtonClick} type="button">Удалить</button>
                    </div>
                }
            </div>
        );
    }
}

// FormAdvertActions.propTypes = {
//     submitHandler: PropTypes.func.isRequired,
//     serverErrors: PropTypes.object,
// };