import React from 'react';
import FormBase from './../FormBase';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../../constants/server_errors';
import { customFetch } from '../../Helpers';
import LoaderSpinner from '../../components/LoaderSpinner';

export default class FormConnectionSecuryDeal extends FormBase {
    constructor(props) {
        super(props);

        this.state = {
            controls_server_errors: {

            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validate_props
            validation: {

            },

            form: {
                csrf: '',
                login: '',
                email: '',
                phone: '',
            },
            form_isValid: true,
            form_isLoading: false,
            form_isSubmitting: false,
            isSending: false,
        };

        this.prev_state = null;

        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.mapPropsToState = this.mapPropsToState.bind(this);
    }

    componentWillMount () {
        this.getCsrf();
        this.mapPropsToState();
    }

    componentWillUpdate = () => {
        this.tryToRunValidationFunctions();
    };

    mapPropsToState() {
        const { is_email_verified, is_phone_verified } = this.props;
        const login = 'Логин: ' + this.props.login;
        const email = is_email_verified ? 'E-mail: ' + this.props.email : 'E-mail: ' + this.props.email + '(не подтверждён)';
        const phone = is_phone_verified ? 'Телефон: +' + this.props.phone : 'Телефон: +' + this.props.phone + '(не подтверждён)';

        this.setState({
            form: {
                ...this.state.form,
                email,
                login,
                phone
            }
        });
    }

    handleCloseForm = () => {
        this.props.handleClosePopup();
    };

    handleSubmit (e) {
        e.preventDefault();
        const { form_isValid } = this.state;

        if (form_isValid) {
            this.toggleIsLoading();
            customFetch('/safe-deal-request', {
                method: 'POST',
                body: JSON.stringify(this.getDataFromState())
            })
                .then(data => {
                    if (data.status === 'SUCCESS') {
                        this.clearFormLocalStorage();
                        this.setState({
                            isSending: true
                        }, () => {
                            setTimeout(() => {this.handleCloseForm();}, 5000);
                        });
                    } else if (data.status === 'ERROR') {
                        this.toggleIsLoading();
                        this.TakeApartErrorFromServer(data);
                    }
                })
                .catch(() => {
                    this.setFormError('critical_error', true);
                    this.toggleIsLoading();
                });
        }
    }

    render() {
        const { isSending, form_isSubmitting} = this.state;

        if (isSending) {
            return (
                <div className="popup message-popup">
                    <div className="form-popup">
                        <h4 className="popup-title">Подключение <br />безопасной сделки</h4>
                        <div className="form-popup__info-text">
                            Ваша заявка будет рассмотрена в течение суток, вы получите дальнейшие инструкции на почту, указанную в профиле.
                        </div>
                    </div>
                </div>
            );
        }

        return (
            <div className="popup form-connection-secury-deal">
                <div className="form-popup">
                    <h4 className="popup-title">Подключение <br />безопасной сделки</h4>

                    <p className="secury-deal-thesis">
                        Сохраним средства на гарантийном счету, до момента удовлетворения как покупателей, так и продавцов.
                    </p>
                    <p className="secury-deal-thesis">
                        Товары будут выделены специальным знаком «Безопасной сделки», что повышает доверие покупателей и увеличивает шансы продать товар.
                    </p>
                    <p className="secury-deal-thesis">
                        Юридическая поддержка в суде при невозможности досудебного решения спора.
                    </p>

                    <div className="form-popup__footer">
                        <button className="button-transparent-with-stroke"  type="button" onClick={this.handleCloseForm}>Отменить</button>
                        <button className="button-blue" type="submit" onClick={this.handleSubmit}>Отправить заявку</button>
                    </div>
                </div>
                {form_isSubmitting && <LoaderSpinner isAbsolute />}
            </div>
        );
    }
}