import React from 'react';
import FormBase from './../FormBase';
import ShopName from '../../controls/ControlShopName';
import LegalEntityName from '../../controls/ControlLegalEntityName';
import Inn from '../../controls/ControlInn';
import Kpp from '../../controls/ControlKpp';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../../constants/server_errors';

export default class FormBankCard extends FormBase {
    constructor(props) {
        super(props);

        this.state = {
            controls_server_errors: {

            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validate_props
            validation: {

            },

            form: {

            },
            form_isValid: false,
            form_isLoading: false,
            form_isSubmitting: false
        };

        this.prev_state = null;

        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    componentWillUpdate = () => {
        this.tryToRunValidationFunctions();
    };

    handleCloseForm = () => {
        this.props.handleClosePopup();
    };

    render() {

        return (
            <div className="form-tie-card">
                <div className="form-popup">
                    <h4 className="popup-title">Привязать карту</h4>
                    <div className="card-info-security">
                        Вся информация о карте кроме Secrity Code будет безопасно сохранена на сервисе.
                    </div>

                    <div className="payment-systems-logo">
                        <div className="icon-payment-mir" />
                        <div className="icon-payment-mastercard" />
                        <div className="icon-payment-visa" />
                    </div>

                    <div className="row card-identification">
                        <div className="control-form">
                            <label className="label-text">Номер карты:</label>
                            <input type="text"  placeholder="Введите номер карты" />
                        </div>
                        <div className="control-form">
                            <label className="label-text">Срок действия:</label>
                            <input type="text"  placeholder="__/__" />
                        </div>
                    </div>

                    <div className="control-form">
                        <label className="label-text">Имя владельца карты:</label>
                        <input type="text"  placeholder="Введите имя владельца карты" />
                    </div>
                    <div className="form-popup__footer">
                        <button className="button-transparent-with-stroke" type="button" onClick={this.handleCloseForm}>Отменить</button>
                        <button className="button-blue" type="submit">Добавить</button>
                    </div>
                </div>
            </div>
        );
    }
}