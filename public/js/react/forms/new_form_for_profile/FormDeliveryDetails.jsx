import React from 'react';
import FormBase from './../FormBase';
import Phone from '../../controls/ControlPhone';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../../constants/server_errors';

export default class FormDeliveryDetails extends FormBase {
    constructor(props) {
        super(props);

        this.state = {
            controls_server_errors: {

            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validate_props
            validation: {

            },

            form: {

            },
            form_isValid: false,
            form_isLoading: false,
            form_isSubmitting: false
        };

        this.prev_state = null;

        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    componentWillUpdate = () => {
        this.tryToRunValidationFunctions();
    };

    handleCloseForm = () => {
        this.props.handleClosePopup();
    };

    render() {

        return (
            <div className="form-delivery-details">
                <div className="form-popup">
                    <h4 className="popup-title">Реквизиты для доставки</h4>
                    <div className="control-form">
                        <label className="label-text">ФИО получателя:</label>
                        <input type="text" name="last_name" placeholder="Введите ФИО получателя" />
                    </div>

                    <div className="control-form">
                        <label className="label-text">Контактный телефон:</label>
                        <input type="text" placeholder="+_(___)___-__-__" />
                    </div>

                    <div className="control-form">
                        <label className="label-text">Населенный пункт:</label>
                        <input type="text" name="last_name" placeholder="Введите населенный пункт" />
                    </div>

                    <div className="control-form">
                        <label className="label-text">Улица, дом, квартира:</label>
                        <input type="text" name="last_name" placeholder="" />
                    </div>
                    <div className="form-popup__footer">
                        <button className="button-transparent-with-stroke" type="button" onClick={this.handleCloseForm}>Отменить</button>
                        <button className="button-blue" type="submit">Добавить</button>
                    </div>
                </div>
            </div>
        );
    }
}


