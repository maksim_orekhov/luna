import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { loadCSRF, changeAvatar } from '../actions';
import LoaderSpinner from '../components/LoaderSpinner';

class FormAvatar extends Component {

    state = {
        form: {
            avatar: {},
        }
    };

    componentDidMount() {
        const { csrf, loadCSRF } = this.props;
        if (!csrf.isLoading && !csrf.isLoaded) {
            loadCSRF();
        }
    }

    handleChangeAvatarInput = (ev) => {
        const { uploadAvatar } = this.props;
        ev.preventDefault();
        const files = ev.target.files;

        this.setState({
            form: {
                avatar: files
            }
        }, () => {
            uploadAvatar(this.createFormData());
        });
    };

    createFormData() {
        const { csrf } = this.props;
        const { avatar } = this.state.form;
        delete this.state.form.avatar;
        const form_data = new FormData();
        form_data.append('csrf', csrf.value);

        Object.values(avatar).forEach((file) => {
            if (file) {
                form_data.append(`avatar`, file);
            }
        });

        return form_data;
    }

    render(){
        const { csrf, avatarIsChanging } = this.props;

        if (!csrf.isLoading && !csrf.isLoaded || csrf.isFailed) {
            return null;
        }

        return(
            <div className="FormAvaChange-Container">
                <form className="FormAvaChange" encType="multipart/form-data">
                    {avatarIsChanging && <LoaderSpinner isAbsolute isTiny />}
                    <label className="InputIconTouch">
                        <input
                            type="file"
                            accept="image/*"
                            name="avatar"
                            onChange={this.handleChangeAvatarInput}
                        />
                        <span className="InputIconTouch-Overlay IconEdit" />
                    </label>
                </form>
            </div>
        );
    }
}


FormAvatar.propTypes = {
    csrf: PropTypes.object.isRequired,
    loadCSRF: PropTypes.func.isRequired,
    uploadAvatar: PropTypes.func.isRequired,
    avatarIsChanging: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => {
    return {
        csrf: state.config.csrf,
        avatarIsChanging: state.user.isChanging.avatar,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        loadCSRF: bindActionCreators(loadCSRF, dispatch),
        uploadAvatar: bindActionCreators(changeAvatar, dispatch),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FormAvatar);
