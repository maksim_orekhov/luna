import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';
import FormBase from './FormBase';
import CategorySelect from '../controls/ControlCategorySelect';
import SubCategorySelect from '../controls/ControlSubCategorySelect';
import AdvName from '../controls/ControlAdvName';
import Amount from '../controls/ControlAmount';
import Description from '../controls/ControlDescription';
import PhotoInputsList from '../controls/ControlPhotoInputsList';
import File from '../controls/ControlFile';
import Location from '../controls/ControlLocation';
import ShowError from '../ShowError';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';
import { fetchSubcategories } from '../actions';
import { saveToSessionStorage, removeFromSessionStorage, getFromSessionStorage, prepareDataToSaveSessionStorage } from '../Helpers';

class FormAdvertisement extends FormBase {
    constructor(props) {
        super(props);
        const { defaultValues } = props;

        const formDefault = {
            categoryId: defaultValues.category ? +defaultValues.category.parent_id :  null,
            subcategoryId: defaultValues.category ? +defaultValues.category.id :  null,
            name: '',
            price: '',
            description: '',
            gallery: [],
            files: [],
            safe_deal: 1,
            address: defaultValues.address ? defaultValues.address : {},
            ...defaultValues,
        };

        this.state = {
            controls_server_errors: {
                categoryId: null,
                subcategoryId: null,
                name: null,
                price: null,
                description: null,
                address: null,
            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validate_props
            validation: {
                categoryId_is_valid: false,
                subcategoryId_is_valid: false,
                name_is_valid: false,
                price_is_valid: false,
                description_is_valid: false,
                address_is_valid: false,
            },
            form: formDefault,
            form_isValid: false,
            form_isLoading: false,
            form_isSubmitting: false,
            safe_deal: true
        };

        this.prev_state = null;
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state


        this.handleComponentValid = this.handleComponentValid.bind(this);
        // this.handleChange = this.handleChange.bind(this);
        this.handleSafeDeal = this.handleSafeDeal.bind(this);
        this.getDataFromState = this.getDataFromState.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const { serverErrors } = this.props;
        if (nextProps.serverErrors.code !== serverErrors.code) {
            this.TakeApartErrorFromServer(nextProps.serverErrors);
        }
        if (nextProps.serverErrors.code === 'NOT_AUTH') {
            window.location = '/login?redirectUrl=/product/create';
        }
        this.handleChange('gallery', nextProps.imageUpload);
    }

    componentDidUpdate() {
        this.tryToRunValidationFunctions();
    }

    handleSafeDeal(value) {
        this.setState({
            form: {
                ...this.state.form,
                safe_deal: value
            }
        });
    }
    
    handleChange = (name, value) => {
        const categories = this.props.categories;
        this.setState((currentState) => {
            return {
                form: {
                    ...currentState.form,
                    [name]: value
                }
            };
        }, () => {
            this.checkAllControlsAreValid();
            if (this.props.form_name) saveToSessionStorage(this.props.form_name, prepareDataToSaveSessionStorage(this.state.form, categories));
        });
    };

    handleSubmit = (ev) => {
        const { form, form_isValid } = this.state;
        const { submitHandler } = this.props;
        ev.preventDefault();
        if(form_isValid){
            const address = {
                ...form.address,
                lat: form.address.latitude,
                lng: form.address.longitude,
            };
            submitHandler({
                ...form,
                address,
                category: form.subcategoryId,
            });
        }
    };

    handleAddressChange = (address) => {
        const form = {...this.state.form, address};
        const validation = {...this.state.validation, address_is_valid: this.validateAddress(address)};
        this.setState({ form, validation }, () => {
            this.checkAllControlsAreValid();
            this.can_update_validation = true;
            if (this.props.form_name) saveToSessionStorage(this.props.form_name, prepareDataToSaveSessionStorage(this.state.form, this.props.categories));
        });
    };

    handleCategoryChange = (name, value) => {
        const { fetchSubcategories } = this.props;
        fetchSubcategories(value);
        this.handleChange(name, value);
    };

    getDataFromState() {
        const data = {...this.state.form};
        delete data.gallery;
        const gallery = this.state.form.gallery;

        return this.createFormData(data, gallery);
    }

    checkAllControlsAreValid() {
        const { validation, } = this.state;

        let form_isValid = true;

        Object.keys(validation).forEach(value => {
            if (!validation[value]) {
                form_isValid = false;
            }
        });

        this.setState({ form_isValid });
    }

    validateAddress = (address) => {
        const { country, city, region } = address;
        return !!(country && country.length > 0 && city && city.length > 0 && region && region.length > 0);
    };

    createFormData(data, gallery = []) {
        const form_data = new FormData();

        Object.entries(data).forEach(([key, value]) => {
            if (typeof value === 'object' && value) {
                Object.entries(value).forEach(([k, v]) => {
                    form_data.append(`${key}[${k}]`, v);
                });
            } else {
                form_data.append(key, value);
            }
        });

        let photo_index = 0;

        if (gallery.length) {
            Object.values(gallery).forEach((photo) => {
                if (photo) {
                    form_data.append(`files[${photo_index}]`, photo);
                    photo_index++;
                }
            });
        }

        return form_data;
    }

    render() {
        const { form: {categoryId, subcategoryId, safe_deal, name, price, description, files}, server_errors } = this.state;
        const { defaultValues, categories, imageUpload, handleImageChange, handleImageRotate, formType } = this.props;
        return (
            <div className="row row_full-width">
                <form action="" className="new-deal-form form-advertisement col-8 col-tablet-8 col-mobile-8" onSubmit={this.handleSubmit}>
                    <h1 className="page-title">{formType === 'create' ? 'Новое объявление' : 'Редактирование объявления'}</h1>
                    <div className="flexible-row">
                        <div className="flexible-row__child">
                            <CategorySelect
                                categories={this.getParentCategories(categories)}
                                value_prop={categoryId}
                                handleComponentChange={this.handleCategoryChange}
                                handleComponentValidation={this.handleComponentValid}
                                name="categoryId"
                                label="Категория:"
                                placeholder="Выберите категорию"
                            />
                        </div>
                        <div className="flexible-row__child">
                            <SubCategorySelect
                                subcategories={this.getSubcategoriesByParentCategoryId(categoryId, categories)}
                                value_prop={subcategoryId}
                                handleComponentChange={this.handleChange}
                                handleComponentValidation={this.handleComponentValid}
                                name="subcategoryId"
                                label="Подкатегория:"
                                placeholder="Выберите подкатегорию"
                            />
                        </div>
                    </div>
                    <AdvName
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                        name="name"
                        label="Название:"
                        value_prop={name}
                    />
                    <div className="flexible-row price-control">
                        <Amount
                            label="Цена:"
                            name="price"
                            max_amount="10000000"
                            handleComponentChange={this.handleChange}
                            handleComponentValidation={this.handleComponentValid}
                            handleSafeDeal={this.handleSafeDeal}
                            defaultValue={price}
                            value_prop={price}
                        />
                        {/* {
                            safe_deal ?
                                <div className="control-form flexible-row__child">
                                    <div className="control-form__help-message">Безопасная сделка включена!</div>
                                    <div className="control-form__help-message">Сдвиньте переключатель влево для отключения.</div>
                                </div>
                                :
                                <div className="control-form flexible-row__child">
                                    <div className="control-form__help-message">Безопасная сделка отключена!</div>
                                    <div className="control-form__help-message">Сдвиньте переключатель вправо для включения.</div>
                                </div>
                        }*/}
                    </div>
                    <Description
                        label="Описание:"
                        name="description"
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                        value_prop={description}
                    />
                    <PhotoInputsList
                        name="gallery"
                        handleComponentChange={this.handleChange}
                        photos_number={4}
                        images={imageUpload}
                        handleFileSelect={handleImageChange}
                        handleImageRotate={handleImageRotate}
                    />
                    <File
                        label="Файл:"
                        name="files"
                        handleComponentChange={this.handleChange}
                        hint={true}
                        files={files}
                    />
                    <Location
                        name="address"
                        label="Местоположение:"
                        handleComponentChange={this.handleAddressChange}
                        handleComponentValidation={this.handleComponentValid}
                        defaultLatitude={defaultValues.address && defaultValues.address.lat ? Number(defaultValues.address.lat) : undefined}
                        defaultLongitude={defaultValues.address && defaultValues.address.lng ? Number(defaultValues.address.lng) : undefined}
                        withGeoLocation={!(defaultValues.address && defaultValues.address.lat && defaultValues.address.lng)}
                    />
                    <div className="row messages">
                        <div className="col-xl-12">
                            <ShowError messages={this.state.server_errors_messages} existing_errors={server_errors} />
                        </div>
                    </div>
                    <div className="control-form action-buttons">
                        <a href="/profile" className="button-transparent action-button">Отмена</a>
                        <button
                            type="submit"
                            className="button-blue action-button"
                            disabled={!this.state.form_isValid}
                        >
                            Далее
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}

FormAdvertisement.propTypes = {
    categories: PropTypes.array.isRequired,
    imageUpload: PropTypes.array.isRequired,
    submitHandler: PropTypes.func.isRequired,
    handleImageChange: PropTypes.func.isRequired,
    handleImageRotate: PropTypes.func.isRequired,
    formType: PropTypes.string.isRequired,
    serverErrors: PropTypes.object,
    defaultValues: PropTypes.object,
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchSubcategories: bindActionCreators(fetchSubcategories, dispatch),
    };
};

export default connect(null, mapDispatchToProps)(FormAdvertisement);