import React from 'react';
import PropTypes from 'prop-types';
import FormBase from './FormBase';
import PurchasePreview from '../components/PurchasePreview';
import { customFetch, getMetaCSRF, getDateDotFormatFromTimestamp } from '../Helpers';


export default class FormPurchaseArchived extends FormBase {
    constructor(props) {
        super(props);

        this.state = {
            form: {
                csrf: ''
            }
        };

        // this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    componentWillMount() {
        const csrf = getMetaCSRF();
        if (csrf) {
            this.setState({
                form: {
                    ...this.state.form,
                    csrf
                }
            });
        } else {
            this.getCsrf();
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const purchase = this.props.purchase || {};
        const urls = purchase.urls || {};
        const archive_url = urls.archive_url;

        const { csrf } = this.state.form;

        if (!archive_url) {
            console.error('Не получен url для восстановления из архива');
            return;
        }

        customFetch(archive_url, {
            method: 'POST',
            body: JSON.stringify({
                csrf,
                archive: 0
            })
        })
            .then((response) => {
                window.sessionStorage.setItem('profileMessage', 'archive_restore');
                window.location.reload();
            });
    };

    handleDelete = (e) => {
        e.preventDefault();
        const purchase = this.props.purchase || {};
        const deleted_url = `/purchase/${purchase.id}`;

        customFetch(deleted_url, {
            method: 'DELETE',
        })
            .then((response) => {
                window.sessionStorage.setItem('profileMessage', 'archive_deleted');
                window.location.reload();
            });
    };

    render() {
        const { purchase, is_operator } = this.props;

        const created_formatted = getDateDotFormatFromTimestamp(purchase.created_timestamp);
        const archive_formatted = getDateDotFormatFromTimestamp(purchase.changed_timestamp);

        return (
            <div className="popup advert_actions_popup">
                <div className="advert_actions_popup_header declined">
                    <div className="advert_popup_header_title">
                        <div className="advert_popup_header_icon archive">&nbsp;</div>
                        <h4 className="popup-title">В архиве</h4>
                    </div>
                    <div className="advert_info">
                        <table>
                            <tbody>
                                <tr>
                                    <td className="prop_name">
                                        Дата создания:
                                    </td>
                                    <td className="value">
                                        {created_formatted}
                                    </td>
                                </tr>
                                <tr>
                                    <td className="prop_name">
                                        В архиве с:
                                    </td>
                                    <td className="value">
                                        {archive_formatted}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="advert_actions_popup_content">
                    <PurchasePreview
                        name={purchase.name}
                        price={purchase.price}
                        quantity={purchase.quantity}
                        measure={purchase.measure}
                        description={purchase.description}
                        category={purchase.category}
                        sub_category={purchase.sub_category}
                        address={purchase.address}
                        files={purchase.files}
                        user={purchase.owner}
                        purchase_url={purchase.urls.single_purchase_public}
                        not_show_public_link={true}
                    />
                </div>
                {
                    !is_operator &&
                    <div className="advert_actions_popup_buttons">
                        <button className="button-popup button-purple" type="button" onClick={this.handleSubmit}>Восстановить</button>
                        <button className="button-popup button-popup-white" type="button" onClick={this.handleDelete}>Удалить</button>
                    </div>
                }
            </div>
        );
    }
}

FormPurchaseArchived.propTypes = {
    is_operator: PropTypes.bool,
    purchase: PropTypes.object,
};

FormPurchaseArchived.defaultProps = {
    purchase: {},
};