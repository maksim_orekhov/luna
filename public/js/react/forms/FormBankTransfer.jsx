import React from 'react';
import BankName from '../controls/ControlBankName';
import CorrAccountNumber from '../controls/ControlCorrAccountNumber';
import AccountNumber from '../controls/ControlAccountNumber';
import Bik from '../controls/ControlBik';
import FormBase from './FormBase';
import ShowError from '../ShowError';
import URLS from '../constants/urls';
import {customFetch} from '../Helpers';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';

export default class FormBankTransfer extends FormBase {
    constructor(props) {
        super(props);
        this.state = {
            isEdit: false,
            /**
             * Ожидается объект вида {"ключ ошибки": "текст ошибки"}
             * @example
             *  amount: {
                 *    isEmpty: "Value is required and can't be empty",
                 *    regExp: "Invalid regexp"
                 *  }
             **/
            controls_server_errors: {
                csrf: null,
                bik: null,
                bank_name: null,
                account_number: null,
                corr_account_number: null
            },
            // SERVER_ERRORS
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validation_props
            validation: {
                bik_is_valid: false,
                bank_name_is_valid: false,
                account_number_is_valid: false,
                corr_account_number_is_valid: false
            },
            // -----------------------------------------
            form: {
                csrf: '',
                bik: '',
                bank_name: '',
                account_number: '',
                corr_account_number: ''
            },
            // Обьект данных о самых популярных банках России
            popularBanks: {},
            form_isValid: false,
            form_isLoading: false,
            form_isSubmitting: false,
            form_isDeleting: false,
            is_deal: false // Флажок что форма открыта из карточки сделки и нужно привязать метод оплаты к сделке
        };

        this.prev_state = null;
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.form_name = 'form_bank_transfer'; // Название формы для хранения данных в local_storage
        if (props.editMode) {
            this.form_name = null; // В режиме редактирования не сохраняем данные
        }

        this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    componentWillMount = () => {
        (this.props.data && this.props.editMode) && this.getDataFromProps();
    };

    componentDidMount = () => {
        // if (!this.props.is_nested) {
        //     this.getCsrf()
        //         .then(() => this.rememberState())
        // }

        // this.getFormInitData();
        this.getFormFromLocalStorage();
    };

    componentWillReceiveProps = (nextProps) => {
        const { form_server_errors, civil_law_subject_id, clear_local_storage } = nextProps;

        if (clear_local_storage !== this.props.clear_local_storage) {
            this.clearFormLocalStorage();
        }

        civil_law_subject_id && this.setState({
            form: {
                ...this.state.form,
                civil_law_subject_id
            }
        });

        form_server_errors && this.SetControlsErrorFromUpperForm(this.props.form_server_errors, form_server_errors);
    };

    componentWillUpdate = () => {
        this.tryToRunValidationFunctions();
    };

    getDataFromProps = () => {
        const { name, bik, bank_name, corr_account_number, account_number, id } = this.props.data.bank_transfer;

        this.setState({
            form: {
                ...this.state.form,
                name,
                bik,
                bank_name,
                corr_account_number,
                account_number,
                id
            }
        }, () => this.rememberState());
    };

    getFormInitData = () => {
        customFetch(URLS.PAYMENT_METHOD.FORM_INIT)
            .then(data => {
                console.log(data);
                if (data.status === 'SUCCESS') {
                    this.setState({
                        popularBanks: data.data.handbooks.popularBanks
                    });
                } else {
                    return Promise.reject(data.message);
                }
            })
            .catch(error => {
                this.setFormError('form_init_fail', true);
                console.log(error);
            });
    };

    handleBik = (bankInfo) => {
        this.setState({
            form: {
                ...this.state.form,
                corr_account_number: bankInfo['bank_ks'],
                bank_name: bankInfo['bank_name'],
                bik: bankInfo['bank_bik']
            }
        }, () => {
            this.checkAllControlsAreValid();
            this.saveFormToLocalStorage();
        });
    };

    handleCloseForm = () => {
        this.props.handleClose && this.props.handleClose();
    };

    handleChange = (name, value) => {
        this.setState({
            form: {
                ...this.state.form,
                [name]: value
            }
        }, () => this.saveFormToLocalStorage());
        this.props.sendUpData(name, value);
    };

    render() {
        const { title } = this.props;
        const { popularBanks } = this.state;
        const { bik, bank_name, corr_account_number, account_number } = this.state.form;

        return (
            <div>
                <h4 className="popup-subtitle">{title}</h4>
                <Bik
                    name="bik"
                    label="БИК:"
                    placeholder="Введите БИК"
                    value_prop={bik}
                    popularBanks={popularBanks}
                    handleBik={this.handleBik} // Метод принимающие данные для банка и делающий автозамену
                    handleComponentChange={this.handleChange}
                    handleComponentValidation={this.handleComponentValid}
                    form_control_server_errors={this.state.controls_server_errors.bik}
                />
                <BankName
                    label="Наименование банка:"
                    placeholder="Введите наименование банка"
                    name="bank_name"
                    value_prop={bank_name}
                    handleComponentChange={this.handleChange}
                    handleComponentValidation={this.handleComponentValid}
                    form_control_server_errors={this.state.controls_server_errors.bank_name}
                />
                <CorrAccountNumber
                    name="corr_account_number"
                    label="Номер корреспондентского счёта:"
                    placeholder="Введите корр.счёт"
                    value_prop={corr_account_number}
                    handleComponentChange={this.handleChange}
                    handleComponentValidation={this.handleComponentValid}
                    form_control_server_errors={this.state.controls_server_errors.corr_account_number}
                />
                <AccountNumber
                    name="account_number"
                    label="Номер лицевого счёта:"
                    placeholder="Введите номер лицевого счёта"
                    value_prop={account_number}
                    handleComponentChange={this.handleChange}
                    handleComponentValidation={this.handleComponentValid}
                    form_control_server_errors={this.state.controls_server_errors.account_number}
                />
                <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
            </div>
        );
    }
}