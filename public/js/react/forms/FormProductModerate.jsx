import React from 'react';
// import PropTypes from 'prop-types';
import FormBase from './FormBase';
import { customFetch, getProductModerationCreatedDateDotFormat } from '../Helpers';
import AdvertPreview from '../components/ProductPreview';
import SelectImitation from '../components/SelectImitation';
import Description from '../controls/ControlDescription';
import DECLINE_REASONS from '../constants/decline_reasons';

export default class FormProductModerate extends FormBase {
    constructor(props) {
        super(props);

        this.state = {
            isDeclineReasonOpened: false,
            selectImitationName: 'decline_reason',
            form: {
                csrf: '',
                decline_reason: 0,
                decline_reason_description: '',
                decline_reason_recommendations: ''
            },
            error: '',
        };

        this.updateValidationFunctions = [];

        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.getActiveTab = this.getActiveTab.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillMount() {
        this.getCsrf();
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const { product } = this.props;
        const { csrf } = this.state.form;
        customFetch(`/product/moderation/${product.id}`, {
            method: 'POST',
            body: JSON.stringify({
                csrf,
                approved: 1
            })
        }).then(data => {
            window.location.reload();
            console.log(data);
        });
    };

    handleDecline = (e) => {
        e.preventDefault();

        const { product } = this.props;
        const { csrf, decline_reason, decline_reason_description } = this.state.form;
        let description = '';

        if (!decline_reason) {
            this.setState({
                error: 'Пожалуйста, выберите причину из списка'
            });
            return;
        }

        if (decline_reason_description === '') {
            description = this.getOptions()[decline_reason - 1].title;
        } else {
            description = decline_reason_description;
        }
        let recommendations = this.getOptions()[decline_reason - 1].recommendation;
        customFetch(`/product/moderation/${product.id}`, {
            method: 'POST',
            body: JSON.stringify({
                csrf,
                approved: 0,
                description: description,
                decline_reason_recommendations: recommendations
            })
        }).then(data => {
            window.location.reload();
        });
    };

    getOptions = () => {
        return DECLINE_REASONS;
    };

    handleChangeSelect = (e) => {
        const {value} = e;
        const { handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;
        const { selectImitationName } = this.state;

        this.setState({
            error: ''
        });

        this.handleChange && this.handleChange(selectImitationName, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    };

    getActiveTab() {
        return this.getOptions()[0].title;
    }

    toggleDeclineForm = (e) => {
        e.preventDefault();
        this.setState({
            isDeclineReasonOpened: !this.state.isDeclineReasonOpened
        });
    };

    closeDeclineForm = (e) => {
        e.preventDefault();
        this.setState({
            isDeclineReasonOpened: !this.state.isDeclineReasonOpened,
            form: {
                ...this.state.form,
                decline_reason: 0,
                decline_reason_description: '',
                decline_reason_recommendations: ''
            }
        });
    };

    render() {
        const { isDeclineReasonOpened, error } = this.state;
        const { decline_reason_description, decline_reason } = this.state.form;
        const { product } = this.props;

        const created_formatted = getProductModerationCreatedDateDotFormat(product);

        return (
            <div className="popup advert_actions_popup">
                <div className="advert_actions_popup_header">
                    <h4 className="popup-title">Модерация объявления</h4>
                    <div className="advert_info">
                        <table>
                            <tbody>
                                <tr>
                                    <td className="prop_name">
                                        Создал:
                                    </td>
                                    <td className="value">
                                        <a href="">{product.owner.name}</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td className="prop_name">
                                        На модерации с:
                                    </td>
                                    <td className="value">
                                        {created_formatted}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="advert_actions_popup_content">
                    <AdvertPreview
                        name={product.name}
                        price={product.price}
                        description={product.description}
                        category={product.category}
                        sub_category={product.sub_category}
                        address={product.address}
                        galleryFiles={product.gallery}
                        files={product.files}
                        user={product.owner}
                        not_show_public_link={true}
                        show_full_size_photo={true}
                    />
                </div>
                {
                    isDeclineReasonOpened &&
                        <div className="advert_moderate_decline_reason_form">
                            <div className="row">
                                <SelectImitation
                                    options={this.getOptions()}
                                    defaultValue={this.getActiveTab()}
                                    placeholder="Выберите причину отклонения"
                                    label="Причина отклонения:"
                                    onChangeHandler={this.handleChangeSelect}
                                />
                            </div>
                            {
                                decline_reason === 9 &&
                                <Description
                                    label="Описание причины:"
                                    name="decline_reason_description"
                                    handleComponentChange={this.handleChange}
                                    handleComponentValidation={this.handleComponentValid}
                                    value_prop={decline_reason_description}
                                />
                            }
                            {
                                error &&
                                    <div className="row" style={{paddingTop: '10px', color: 'red'}}>
                                        <p className="error">{error}</p>
                                    </div>
                            }
                            <div className="advert_actions_popup_buttons">
                                <button className="button-popup button-popup-white" type="button" onClick={this.closeDeclineForm}>Отмена</button>
                                <button className="button-popup button-purple" type="button" onClick={this.handleDecline}>Подтвердить отклонение</button>
                            </div>
                        </div>
                }
                <div className="advert_actions_popup_buttons">
                    <button className="button-popup button-purple" type="button" onClick={this.handleSubmit}>Подтвердить публикацию</button>
                    <button className="button-popup button-popup-white" type="button" onClick={this.toggleDeclineForm}>Отклонить публикацию</button>
                </div>
            </div>
        );
    }
}

// FormAdvertActions.propTypes = {
//     submitHandler: PropTypes.func.isRequired,
//     serverErrors: PropTypes.object,
// };