import React from 'react';
import PropTypes from 'prop-types';
import FormBase from './FormBase';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';
import Login from '../controls/ControlLogin.jsx';
import Captcha from '../controls/ControlCaptcha.jsx';
import Password from '../controls/ControlPasswordAuth.jsx';
import ShowError from '../ShowError.jsx';
import { getCookie } from '../Helpers';

export default class FormAuth extends FormBase {
    constructor(props) {
        super(props);

        this.state = {
            should_activate_captcha: false,
            loginIsValid:               false,
            passwordIsValid:            false,
            form_isValid:               false,
            onLoadClass: '',
            formHasError:               false,
            controls_server_errors: {
                login: null,
                password: null
            },
            validation_errors:  {
                connection_is_lost:     false,
                error_from_server:      false,
                invalid_form_data: false,
                already_auth: false,
                undefined_error: false,
            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validation props
            validation: {
                login_is_valid:         false,
                password_is_valid:      false,
            },
            form: {
                login:      '',
                password:   '',
                captcha: '',
            },
            is_show_captcha: false,
        };

        this.prev_state = null;
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    componentWillReceiveProps = (nextProps) => {
        const { is_show_captcha } = this.state;
        if (Object.keys(nextProps.serverErrors).length > 0) {
            is_show_captcha && this.resetCaptcha();
            this.setFormErrorAnimation();
            this.TakeApartErrorFromServer(nextProps.serverErrors);
        }
    };

    componentDidUpdate = () => {
        this.tryToRunValidationFunctions();
        const { is_show_captcha, validation } = this.state;
        const shouldShowCaptcha = getCookie('login_captcha_enable');

        if (shouldShowCaptcha && !is_show_captcha) {
            this.setState({
                form_isValid: false,
                is_show_captcha: shouldShowCaptcha,
                validation: {
                    ...validation,
                    captcha_is_valid: false,
                }
            });
        }
    };

    showFormRegister = (e) => {
        e.preventDefault();
        this.props.handleNext('register');
    };

    handleChange = (name, value) => {
        this.setState({
            form: {
                ...this.state.form,
                [name]: value
            }
        }, () => this.checkAllControlsAreValid());
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const { form_isValid, form } = this.state;
        const { submitHandler } = this.props;

        if (form_isValid) {
            submitHandler({...form});
        } else {
            this.setFormErrorAnimation();
        }
        this.checkIfCaptchaActivated();
    };

    isSubmitButtonDisabled = () => {
        return !this.state.form_isValid;
    };

    checkIfCaptchaActivated = () => {
        const captcha = this.state.form.captcha;
        const captchaEnable = getCookie('count_failed_login_attempts');
        if (captcha === '' && captchaEnable === '3') {
            this.setState({
                should_activate_captcha: true
            });
        }
    };

    render() {
        const { controls_server_errors, server_errors, server_errors_messages, is_show_captcha, onLoadClass, formHasError, should_activate_captcha } = this.state;
        const { login, password } = this.state.form;
        return (
            <div className={`auth-form ${onLoadClass} ${formHasError ? 'animation-shake' : ''} react-js`}>
                <div className="row js-popup-content">
                    <div className="col-6 auth-form-main">
                        <div className="auth-form-main-container">
                            <h1>Войти в систему</h1>
                            <form action="" onSubmit={this.handleSubmit}>
                                <Login
                                    handleComponentChange={this.handleChange}
                                    handleComponentValidation={this.handleComponentValid}
                                    form_control_server_errors={controls_server_errors.login}
                                    value_prop={login}
                                    name="login"
                                />
                                <Password
                                    handleComponentChange={this.handleChange}
                                    handleComponentValidation={this.handleComponentValid}
                                    form_control_server_errors={controls_server_errors.password}
                                    value_prop={password}
                                    name="password"
                                />
                                <ShowError
                                    existing_errors={server_errors}
                                    messages={server_errors_messages}
                                    id="captcha_auth"
                                />
                                <div className="auth-form-password-field">
                                    <div className="control-form">
                                        <label className="form-checkbox">
                                            <input type="checkbox" />
                                            <span className="form-checkbox__field">&nbsp;</span>
                                            <span className="form-checkbox__label">Запомнить пароль?</span>
                                        </label>
                                    </div>
                                    <a className="link-inline" href="/forgot/password/reset">Забыли пароль?</a>
                                </div>
                                { is_show_captcha &&
                                    <Captcha
                                        onChange={this.handleCaptchaChange}
                                        onExpired={this.handleCaptchaExpired}
                                        setWidgetId={this.setCaptchaWidgetId}
                                    />
                                }
                                {
                                    should_activate_captcha &&
                                    <p className="note-red" style={{marginBottom: '15px'}}>Пожалуйста заполните капчу</p>
                                }
                                <button className="button-purple" type="submit">Войти</button>
                            </form>
                            <div className="registration-links">
                                <p>Или зарегистрируйтесь</p>
                                <a className="button-blue" onClick={this.showFormRegister} href="#">Регистрация</a>
                            </div>
                        </div>
                    </div>
                    <div className="col-6 auth-form-img" />
                </div>
            </div>
        );
    }
}

FormAuth.propTypes = {
    submitHandler: PropTypes.func.isRequired,
    serverErrors: PropTypes.object,
};