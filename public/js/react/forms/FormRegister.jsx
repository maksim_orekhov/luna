import React from 'react';
import PropTypes from 'prop-types';
import FormBase from './FormBase';
import Email from '../controls/ControlEmailRegister.jsx';
import Password from '../controls/ControlPasswordRegister.jsx';
import ShowError from '../ShowError.jsx';
import Captcha from '../controls/ControlCaptcha.jsx';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';

export default class FormRegister extends FormBase {
    constructor(props) {
        super(props);
        this.html_form_url = '/register';
        this.state = {
            should_activate_captcha:    false,
            onLoadClass:                '',
            passwordsAreEqual:          false,
            registration_is_finished:   false,
            form_isValid:               false,
            formHasError:               false,
            controls_server_errors: {
                login:      null,
                password:   null,
                email:      null,
                phone:      null
            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validation props
            validation: {
                email_is_valid:         false,
                password_is_valid:      false,
                captcha_is_valid:      false,
                // email_unavailable:      false
            },
            validation_errors:  {
                connection_is_lost:     false,
                error_from_server:      false,
                form_not_valid: false,
                undefined_error: false
            },
            form: {
                login:      '',
                phone:      '',
                email:      props.invitationEmail || '',
                password:   '',
                password_confirm: null,
                invitationToken:   props.invitationToken || null,
                captcha: '',
            },
        };

        this.prev_state = null;
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    componentWillUpdate = () => {
        this.tryToRunValidationFunctions();
    };

    componentWillReceiveProps = (nextProps) => {
        if (Object.keys(nextProps.serverErrors).length > 0) {
            this.setFormErrorAnimation();
            this.resetCaptcha();
            this.TakeApartErrorFromServer(nextProps.serverErrors);
        }
    };

    handleChange = (name, value) => {
        this.setState({
            form: {
                ...this.state.form,
                [name]: value
            }
        }, () => this.checkAllControlsAreValid());
    };

    handleValid = (name,isTrue) => {
        this.setState({[name] : isTrue});
    };

    handleValue = (name, value) => {
        if (name === 'password') {
            this.setState({
                form: {
                    ...this.state.form,
                    confirm_password: value,
                    [name]: value
                }
            });
        } else {
            this.setState({
                form: {
                    ...this.state.form,
                    [name]: value
                }
            });
        }
    };

    showFormAuth = (e) => {
        e.preventDefault();
        this.props.handleNext('login');
    };

    handleSubmit = (e) => {
        const { submitHandler } = this.props;
        const { form, form_isValid } = this.state;
        e.preventDefault();
        if (form_isValid) {
            submitHandler(form);
        } else {
            this.setFormErrorAnimation();
        }
        this.checkIfCaptchaActivated();
    };

    toFinishRegistration = (data) => {
        console.log('toFinishRegistration');

        this.setState({registration_is_finished:true});
        this.props.registerFormValues(this.state.form);

        console.log('redirect_url: ' + data.redirect_url);

        if (data.redirect_url !== null && data.redirect_url !== '/login') {
            window.location = data.redirect_url;
        }
    };

    checkIfCaptchaActivated = () => {
        const captcha = this.state.form.captcha;
        this.setState({
            should_activate_captcha: captcha === ''
        });
    };

    render() {
        const { controls_server_errors, onLoadClass, server_errors, server_errors_messages, formHasError, should_activate_captcha } = this.state;
        const { email } = this.state.form;
        return (
            <div className={`registration-form ${onLoadClass} ${formHasError ? 'animation-shake' : ''} react-js`}>
                <div className="row js-popup-content">
                    <div className="row">
                        <div className="col-6 registration-form__main">
                            <div className="registration-form__main-container">
                                <h4 className="popup-title">
                                    Регистрация
                                </h4>
                                <form onSubmit={this.handleSubmit}>
                                    <Email
                                        handleComponentChange={this.handleChange}
                                        handleComponentValidation={this.handleComponentValid}
                                        form_control_server_errors={controls_server_errors.login}
                                        value_prop={email}
                                        name="email"
                                        not_check_unique={false}
                                    />
                                    <Password
                                        name="password"
                                        handleComponentChange={this.handleChange}
                                        handleComponentValidation={this.handleComponentValid}
                                        areEqual={this.handleValid}
                                        componentValue={this.handleValue}
                                        form_control_server_errors={controls_server_errors.password}
                                        onSubmit={handleBlur => this.handleBlur = handleBlur}
                                    />
                                    <Captcha
                                        onChange={this.handleCaptchaChange}
                                        onExpired={this.handleCaptchaExpired}
                                        setWidgetId={this.setCaptchaWidgetId}
                                    />
                                    {
                                        should_activate_captcha &&
                                        <p className="note-red" style={{marginBottom: '15px'}}>Пожалуйста заполните капчу</p>
                                    }
                                    <div className="control-form">
                                        <button type="submit" className="button-blue">Регистрация</button>
                                    </div>
                                    <ShowError
                                        existing_errors={server_errors}
                                        messages={server_errors_messages}
                                    />
                                    <div className="registration-form__footer">
                                        <span className="registration-form__note">Уже зарегистрированы?</span>
                                        <a href="#" className="registration-form__enter-link" onClick={this.showFormAuth}>Войти?</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div className="col-6 registration-form__img" />
                    </div>
                </div>
            </div>
        );
    }
}

FormRegister.propTypes = {
    submitHandler: PropTypes.func.isRequired,
    serverErrors: PropTypes.object,
};