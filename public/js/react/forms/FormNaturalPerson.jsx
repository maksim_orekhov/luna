import React from 'react';
import LastName from '../controls/ControlLastName';
import FirstName from '../controls/ControlFirstName';
import SecondaryName from '../controls/ControlSecondaryName';
import BirthDate from '../controls/ControlBirthDate';
import FormBase from './FormBase';
import ShowError from '../ShowError';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';

export default class FormNaturalPerson extends FormBase {

    constructor(props) {
        super(props);

        this.state = {
            /**
             * Ожидается объект вида {"ключ ошибки": "текст ошибки"}
             * @example
             *  amount: {
                 *    isEmpty: "Value is required and can't be empty",
                 *    regExp: "Invalid regexp"
                 *  }
             **/
            controls_server_errors: {
                csrf: null,
                last_name: null,
                first_name: null,
                secondary_name: null,
                birth_date: null
            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validate_props
            validation: {
                last_name_is_valid: false,
                first_name_is_valid: false,
                secondary_name_is_valid: false,
                birth_date_is_valid: false
            },
            // --------------------------------
            form: {
                csrf: '',
                last_name: '',
                first_name: '',
                secondary_name: '',
                birth_date: '',
            },
            form_isValid: false,
            form_isLoading: false,
            form_isSubmitting: false,
            form_isDeleting: false
        };

        this.prev_state = null; // Первоначальное состояние формы для блокировки кнопки сохранить в режиме изменения, если данные не изменились от первоначальных
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.form_name = 'form_natural_person'; // Название формы для хранения данных в local_storage

        this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    componentWillMount = () => {
        this.getFormFromLocalStorage();
        (this.props.data && this.props.editMode) && this.getDataFromProps();
    };

    // componentDidMount() {
    //     if (!this.props.is_nested) {
    //         this.getCsrf()
    //             .then(() => this.rememberState());
    //     }
    // }

    componentWillReceiveProps = (nextProps) => {
        const { form_server_errors, clear_local_storage } = nextProps;

        if (clear_local_storage !== this.props.clear_local_storage) {
            this.clearFormLocalStorage();
        }

        form_server_errors && this.SetControlsErrorFromUpperForm(this.props.form_server_errors, form_server_errors);
    };

    componentWillUpdate = () => {
        this.tryToRunValidationFunctions();
    };

    getDataFromProps = () => {
        const { id, last_name, first_name, secondary_name, birth_date } = this.props.data;
        this.setState({
            id,
            form: {
                ...this.state.form,
                last_name,
                first_name,
                secondary_name,
                birth_date
            }
        }, () => this.rememberState());
    };

    handleChange = (name, value) => {
        this.setState({
            form: {
                ...this.state.form,
                [name]: value
            }
        }, () => this.saveFormToLocalStorage());
        this.props.sendUpData(name, value);
    };

    render() {
        const { last_name, first_name, secondary_name, birth_date } = this.state.form;

        return (
            <div>
                <LastName
                    label="Фамилия:"
                    placeholder="Введите фамилию"
                    name="last_name"
                    value_prop={last_name}
                    handleComponentChange={this.handleChange}
                    handleComponentValidation={this.handleComponentValid}
                    form_control_server_errors={this.state.controls_server_errors.last_name}
                />
                <FirstName
                    label="Имя:"
                    placeholder="Введите имя"
                    name="first_name"
                    value_prop={first_name}
                    handleComponentChange={this.handleChange}
                    handleComponentValidation={this.handleComponentValid}
                    form_control_server_errors={this.state.controls_server_errors.first_name}
                />
                <SecondaryName
                    label="Отчество:"
                    name="secondary_name"
                    placeholder="Введите отчество"
                    value_prop={secondary_name}
                    handleComponentChange={this.handleChange}
                    handleComponentValidation={this.handleComponentValid}
                    form_control_server_errors={this.state.controls_server_errors.secondary_name}
                />
                <BirthDate
                    label="Дата рождения:"
                    name="birth_date"
                    placeholder="дд.мм.гггг"
                    handleComponentChange={this.handleChange}
                    value_prop={birth_date}
                    handleComponentValidation={this.handleComponentValid}
                    form_control_server_errors={this.state.controls_server_errors.birth_date}
                />
                <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
            </div>
        );
    }
}
