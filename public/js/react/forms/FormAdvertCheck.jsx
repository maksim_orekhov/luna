import React from 'react';
import PropTypes from 'prop-types';
import FormBase from './FormBase';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';
import Login from '../controls/ControlLogin.jsx';
import Captcha from '../controls/ControlCaptcha.jsx';
import Password from '../controls/ControlPasswordAuth.jsx';
import ShowError from '../ShowError.jsx';
import { getCookie } from '../Helpers';
import AdvertPreview from '../components/AdvertPopupPreview';

export default class FormAdvertCheck extends FormBase {
    constructor(props) {
        super(props);

        this.state = {

        };

        // this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    handleSubmit = (e) => {
        e.preventDefault();
        // const { form_isValid, form } = this.state;
        // const { submitHandler } = this.props;
        //
        // if (form_isValid) {
        //     submitHandler({...form});
        // } else {
        //     this.setFormErrorAnimation();
        // }
    };

    render() {
        // const { controls_server_errors, server_errors, server_errors_messages, is_show_captcha, onLoadClass, formHasError } = this.state;
        // const { login, password } = this.state.form;
        return (
            <div className="popup advert_actions_popup">
                <div className="advert_actions_popup_header">
                    <h4 className="popup-title">Проверьте объявление перед публикацией</h4>
                </div>
                <div className="advert_actions_popup_content">
                    <AdvertPreview
                        not_show_public_link={true}
                    />
                </div>
                <div className="advert_actions_popup_buttons">
                    <button className="button-popup button-popup-white" type="button">Редактировать</button>
                    <button className="button-popup button-purple" type="button">Опубликовать</button>
                </div>
            </div>
        );
    }
}

// FormAdvertActions.propTypes = {
//     submitHandler: PropTypes.func.isRequired,
//     serverErrors: PropTypes.object,
// };