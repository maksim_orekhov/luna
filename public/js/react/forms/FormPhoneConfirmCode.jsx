import React from 'react';
import PropTypes from 'prop-types';
import FormBase from './FormBase';
import PhoneConfirmCode from '../controls/ControlPhoneConfirmCode';
import LoaderSpinner from '../components/LoaderSpinner';
import Timer from '../Timer';
import ShowError from '../ShowError';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';
import { prettifyPhoneNumber } from '../Helpers';

export default class FormPhoneConfirmCode extends FormBase {
    constructor(props) {
        super(props);

        this.state = {
            sms_timer_time: 10,
            is_user_allowed_sms_request: false,
            is_limit_sms_code: false,
            controls_server_errors: {
                code: null
            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validate_props
            validation: {
                code_is_valid: false
            },
            form: {
                code: '',
            },
            form_isValid: false,
        };

        this.prev_state = null;
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.toggleSmsTimer = this.toggleSmsTimer.bind(this);
        this.handleResendCode = this.handleResendCode.bind(this);
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    componentWillReceiveProps(nextProps) {
        if (Object.keys(nextProps.serverErrors).length > 0) {
            this.TakeApartErrorFromServer(nextProps.serverErrors);
        }
    }

    handleSubmit(e) {
        e.preventDefault();
        const { form_isValid, form: { code } } = this.state;
        const { csrf, confirmCodeHandler } = this.props;

        if (form_isValid) {
            confirmCodeHandler(JSON.stringify({
                code,
                csrf: csrf.value
            }));
        }
    }

    handleResendCode() {
        const { csrf, resendPhoneCode } = this.props;
        this.setState({
            is_user_allowed_sms_request: false,
        }, () => {
            resendPhoneCode(JSON.stringify(({
                csrf: csrf.value
            })));
        });

    }

    checkAllowingToSendSmsCodeTimeOut() {
        const { sms_request_period, sms_last_code_created_at } = this.state;
        const now = Date.now() / 1000; // Время сейчас в секундах
        const timer_time = sms_last_code_created_at + sms_request_period - now;

        if (timer_time > 0) {
            this.setState({
                is_user_allowed_sms_request: false,
                sms_timer_time: Math.round(timer_time),
                form_isLoading: false, // Отключаем прелоадеры
                resend_isLoading: false
            });
        }
    }

    toggleSmsTimer() {
        this.setState({
            is_user_allowed_sms_request: true
        });
    }

    handlePrev = (ev) => {
        const { openEditPhoneForm } = this.props;
        ev.preventDefault();
        openEditPhoneForm();
    };

    render() {
        const { sms_timer_time, is_user_allowed_sms_request, is_limit_sms_code, server_errors } = this.state;
        const { phoneNumber, codeIsSending, serverErrors, is_phone_verified } = this.props;

        return (
            <div className={`popup phone-accept-code-popup`}>
                <div className="form-popup">
                    <form onSubmit={this.handleSubmit}>
                        <h4 className="popup-title">{is_phone_verified ? 'Изменение телефона' : 'Подтверждение телефона'}</h4>
                        <div className="control-form">
                            <input type="text" name="" value={prettifyPhoneNumber(phoneNumber)} disabled />
                        </div>
                        <div className="form-popup__info-text">
                            На Ваш номер отправлено SMS-сообщение с кодом, который необходимо ввести в форму ниже.
                        </div>
                        <div className="form-popup__footer form-footer-controls">
                            <PhoneConfirmCode
                                name="code"
                                placeholder="Введите код"
                                handleComponentChange={this.handleChange}
                                handleComponentValidation={this.handleComponentValid}
                                form_control_server_errors={serverErrors}
                            />
                            <button className="button-purple" type="submit">Подтвердить</button>
                        </div>
                        {
                            !is_limit_sms_code ?
                                is_user_allowed_sms_request ?
                                    <div className="form-popup__notice">
                                        <button
                                            className="button-purple"
                                            // disabled={form_isLoading || confirm_isLoading}
                                            onClick={this.handleResendCode}
                                            type="button"
                                        >
                                                Получить код повторно
                                        </button>
                                    </div>
                                    :
                                    <div className="form-popup__notice">
                                            Запросить код повторно через <span className="form-popup__repeat-time-left">
                                            <Timer
                                                start={sms_timer_time}
                                                expired={this.toggleSmsTimer}
                                                isShowTime={true}
                                            />
                                        </span> или <a href="#" className="form-popup__notice-link" onClick={this.handlePrev}>изменить номер</a>.
                                    </div>
                                :
                                <p className="TextImportant">Вы достигли лимита получения sms кодов и теперь сможете получить код повторно только через 15 минут.</p>
                        }
                        <ShowError messages={this.state.server_errors_messages} existing_errors={server_errors} />
                        {
                            codeIsSending &&
                            <LoaderSpinner isAbsolute={true} />
                        }
                    </form>
                </div>
            </div>
        );
    }
}

FormPhoneConfirmCode.propTypes = {
    serverErrors: PropTypes.object.isRequired,
    csrf: PropTypes.object.isRequired,
    codeIsSending: PropTypes.bool.isRequired,
    phoneNumber: PropTypes.string,
    confirmCodeHandler: PropTypes.func,
    resendPhoneCode: PropTypes.func,
    openEditPhoneForm: PropTypes.func,
};

FormPhoneConfirmCode.defaultProps = {
    phoneNumber: '',
    confirmCodeHandler: () => {},
    resendPhoneCode: () => {},
    openEditPhoneForm: () => {},
};
