import React from 'react';
import PropTypes from 'prop-types';
import FormBase from './FormBase';
import { customFetch, getDateDotFormatFromTimestamp, getProductResolutionDateDotFormat } from '../Helpers';
import AdvertPreview from '../components/ProductPreview';

export default class FormAdvertPublished extends FormBase {
    constructor(props) {
        super(props);

        this.state = {
            form: {
                csrf: ''
            }
        };

        // this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    componentWillMount() {
        this.getCsrf();
    }

    handleEditButtonClick = () => {
        const { id } = this.props.product;
        window.location = `/product/${id}/edit`;
    };

    handleStopPublishingButtonClick = () => {
        const { id } = this.props.product;
        const { csrf } = this.state.form;

        customFetch(`/product/${id}/stop-publish`, {
            method: 'POST',
            body: JSON.stringify({
                csrf: csrf
            })
        })
            .then((response) => {
                window.sessionStorage.setItem('profileMessage', 'inactive');
                window.location.reload();
            });
    };

    handleDeleteButtonClick = () => {
        const { id } = this.props.product;
        const { csrf } = this.state.form;

        customFetch(`/product/${id}/archive`, {
            method: 'POST',
            body: JSON.stringify({
                csrf: csrf
            })
        })
            .then((response) => {
                window.sessionStorage.setItem('profileMessage', 'archive');
                window.location.reload();
            });
    };



    render() {
        const { product, is_operator } = this.props;

        const created_formatted = getDateDotFormatFromTimestamp(product.created_timestamp);
        const resolution_formatted = getProductResolutionDateDotFormat(product);

        return (
            <div className="popup advert_actions_popup">
                <div className="advert_actions_popup_header">
                    <div className="advert_popup_header_title">
                        <div className="advert_popup_header_icon published">&nbsp;</div>
                        <h4 className="popup-title">Опубликовано</h4>
                    </div>
                    <div className="advert_info">
                        <table>
                            <tbody>
                                <tr>
                                    <td className="prop_name">
                                        Дата создания:
                                    </td>
                                    <td className="value">
                                        {created_formatted}
                                    </td>
                                </tr>
                                <tr>
                                    <td className="prop_name">
                                        Дата публикации:
                                    </td>
                                    <td className="value">
                                        {resolution_formatted}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="advert_actions_popup_content">
                    <AdvertPreview
                        name={product.name}
                        price={product.price}
                        description={product.description}
                        category={product.category}
                        sub_category={product.sub_category}
                        address={product.address}
                        galleryFiles={product.gallery}
                        files={product.files}
                        user={product.owner}
                        product_url={product.urls.single_product_public}
                    />
                </div>
                {
                    !is_operator &&
                    <div className="advert_actions_popup_buttons published">
                        <button className="button-popup button-popup-white" onClick={this.handleEditButtonClick} type="button">Редактировать</button>
                        <button className="button-popup button-popup-white" onClick={this.handleStopPublishingButtonClick} type="button">Остановить показ</button>
                        <button className="button-popup button-popup-white" onClick={this.handleDeleteButtonClick} type="button">Удалить</button>
                    </div>
                }
            </div>
        );
    }
}

FormAdvertPublished.propTypes = {
    is_operator: PropTypes.bool,
    product: PropTypes.object,
};