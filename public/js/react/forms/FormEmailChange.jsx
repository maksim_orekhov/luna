import React from 'react';
import PropTypes from 'prop-types';
import FormBase from './FormBase';
import Email from '../controls/ControlEmail';
import LoaderSpinner from '../components/LoaderSpinner';
import ShowError from '../ShowError';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';

export default class FormEmailChange extends FormBase {
    constructor(props) {
        super(props);

        this.state = {
            controls_server_errors: {
                email: null
            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validate_props
            validation: {
                email_is_valid: false
            },
            form: {
                email: '',
            },
            form_isValid: false,
        };

        this.prev_state = null;
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.handleStateChange = this.handleStateChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    componentWillReceiveProps(nextProps) {
        if (Object.keys(nextProps.serverErrors).length > 0) {
            this.TakeApartErrorFromServer(nextProps.serverErrors);
        }
    }

    handleSubmit(e) {
        e.preventDefault();
        const { submitHandler, csrf, currentEmail } = this.props;
        const { form_isValid, form: { email } } = this.state;

        if (form_isValid) {
            submitHandler(JSON.stringify({
                new_email: email,
                csrf: csrf.value,
                current_email: currentEmail
            }), email);
        }
    }

    render() {
        const { server_errors_messages, form: { email }, server_errors } = this.state;
        const { serverErrors, isChanging } = this.props;
        return (
            <div className={`popup email-change-request-popup`}>
                <div className="form-popup">
                    <form onSubmit={this.handleSubmit}>
                        <h4 className="popup-title">Изменение почты</h4>
                        <Email
                            name="email"
                            placeholder="Введите новый e-mail"
                            handleComponentChange={this.handleChange}
                            handleComponentValidation={this.handleComponentValid}
                            value_prop={email}
                        />
                        <div className="form-popup__info-text">
                            Для защиты вашего профиля мы вышлем на Вашу почту письмо с инструкциями.
                        </div>
                        <button className="button-blue" type="submit">Получить письмо</button>
                        <ShowError messages={server_errors_messages} existing_errors={server_errors} />
                        {
                            isChanging &&
                            <LoaderSpinner isAbsolute={true} />
                        }
                    </form>
                </div>
            </div>
        );
    }
}

FormEmailChange.propTypes = {
    csrf: PropTypes.object.isRequired,
    currentEmail: PropTypes.string.isRequired,
    serverErrors: PropTypes.object.isRequired,
    isChanging: PropTypes.bool,
    submitHandler: PropTypes.func,
};

FormEmailChange.defaultProps = {
    isChanging: false,
    submitHandler: () => {},
};
