import React from 'react';
import Email from '../controls/ControlEmail.jsx';

import ShowError from '../ShowError.jsx';
import FormBase from '../forms/FormBase';
import { customFetch } from '../Helpers';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';

export default class FormForgotPassword extends FormBase {
    constructor() {
        super();
        this.html_form_url = '/forgot/password';
        this.state = {
            onLoadClass: '',
            emailIsValid:               false,
            validation_errors:  {
                connection_is_lost:     false,
                error_from_server:      false
            },
            controls_server_errors: {
                email:      null
            },
            form: {
                email:      null,
                csrf: ''
            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validation props
            validation: {
                connection_is_lost:     false,
                error_from_server:      false,
                email_is_valid:         false,
                undefined_error:        false
            }
        };

        this.prev_state = null;
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNext = this.handleNext.bind(this);
        this.handlePrev = this.handlePrev.bind(this);
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    componentDidMount() {
        this.getCsrf();
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({onLoadClass : 'Preloader'});
        customFetch('/forgot/password/reset', {
            method: 'POST',
            body: JSON.stringify(this.state.form)
        })
            .then(data => {
                console.log(data);
                if (data.status === 'SUCCESS') {
                    this.handleNext();
                } else if (data.status === 'ERROR') {
                    this.setState({onLoadClass : ''});
                    this.TakeApartErrorFromServer(data);
                }
            })
            .catch(() => {
                this.setFormError('critical_error', true);
                this.setState({onLoadClass : ''});
            });
    }

    handleNext() {
        this.props.handleNext('forgot_password_success_message');
    }

    handlePrev() {
        this.props.handleNext('forgot_password');
    }

    render() {
        let { server_errors, server_errors_messages, email } = this.state;

        return (
            <div className="popup phone-change-popup">
                <div className="form-popup">
                    <h4 className="popup-title">Восстановление пароля</h4>
                    <div className="form-popup__info-text">
                        Пожалуйста, укажите <b>email</b>, который Вы использовали при регистрации на сайте.
                    </div>
                    <Email
                        value_prop={email}
                        name="email"
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                        form_control_server_errors={this.state.controls_server_errors.email}
                        not_check_unique={true}
                        placeholder="Введите email"
                    />
                    <button className="button-blue" onClick={this.handleNext} type="button">Сбросить текущий пароль</button>
                    <div className="form-popup__notice">
                        <a href="#" className="form-popup__notice-link" onClick={this.handlePrev}>Вспомнили пароль?</a>
                    </div>
                    <ShowError existing_errors={server_errors} messages={server_errors_messages} />
                </div>
            </div>
        );
    }
}