import React from 'react';
import FormBase from './FormBase';
import ShopName from '../controls/ControlShopName';
import LegalEntityName from '../controls/ControlLegalEntityName';
import Inn from '../controls/ControlInn';
import Kpp from '../controls/ControlKpp';
import Ogrn from '../controls/ControlOgrn';
import ShowError from '../ShowError';
import LoaderSpinner from '../components/LoaderSpinner';
import ControlDescription from '../controls/ControlDescription';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';
import { customFetch } from '../Helpers';

export default class FormCreateShop extends FormBase {
    constructor(props) {
        super(props);

        this.state = {
            controls_server_errors: {
                shop_name: null,
                description: null,
                phones: null,
                work_schedule: null,
                web_links: null,
                legal_entity_name: null,
                legal_entity_address: null,
                legal_entity_postal_address: null,
                inn: null,
                kpp: null,
                ogrn: null
            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validate_props
            validation: {
                shop_name_is_valid: false,
                description_is_valid: false,
                phones_is_valid: false,
                work_schedule_is_valid: false,
                web_links_is_valid: false,
                legal_entity_name_is_valid: false,
                legal_entity_address_is_valid: false,
                legal_entity_postal_address_is_valid: false,
                inn_is_valid: false,
                kpp_is_valid: false,
                ogrn_is_valid: false
            },
            form: {
                csrf: '',
                shop_name: '',
                description: '',
                phones: '',
                work_schedule: '',
                web_links: '',
                legal_entity_name: '',
                legal_entity_address: '',
                legal_entity_postal_address: '',
                inn: '',
                kpp: '',
                ogrn: '',
                unique_design: false
            },
            form_isValid: false,
            form_isLoading: false,
            form_isSubmitting: false,
            form_sent: false
        };

        this.prev_state = null;
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    componentWillMount() {
        this.getCsrf();
    }

    componentWillUpdate = () => {
        this.tryToRunValidationFunctions();
    };

    getDataFromState = () => {
        let { csrf, shop_name, description, phones, work_schedule, web_links, legal_entity_name, legal_entity_address, legal_entity_postal_address, inn, kpp, ogrn, unique_design } = this.state.form;
        let { login, email, phone, is_email_verified, is_phone_verified } = this.props;

        login = 'Логин: ' + login;
        email =  email ? 'E-mail: ' + email + (is_email_verified ? '' : ' (не подтвержден)') : 'E-mail: отсутствует';
        phone =  phone ? 'Телефон: +' + phone + (is_phone_verified ? '' : ' (не подтвержден)') : 'Телефон: отсутствует';
        shop_name = 'Название магазина: ' + shop_name;
        description = 'Описание магазина: ' + description;
        phones = 'Номера телефонов: ' + this.prettyfyUserInput(phones);
        work_schedule = 'График работы: ' + this.prettyfyUserInput(work_schedule);
        web_links = 'Ссылки: ' + this.prettyfyUserInput(web_links);
        legal_entity_name = 'Название юридического лица: ' + legal_entity_name;
        legal_entity_address = 'Юридический адрес: ' + legal_entity_address;
        legal_entity_postal_address = 'Почтовый адрес: ' + legal_entity_postal_address;
        inn = 'ИНН: ' + inn;
        kpp = 'КПП: ' + kpp;
        ogrn = 'ОГРН: ' + ogrn;
        unique_design = unique_design ? 'Нужно уникальное оформление: да' : 'Нужно уникальное оформление: нет';

        return { csrf, login, email, phone, shop_name, description, phones, work_schedule, web_links, legal_entity_name, legal_entity_address, legal_entity_postal_address, inn, kpp, ogrn, unique_design };
    };

    handleChangeUniqueDesign = (e) => {
        this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.checked
            }
        }, () => this.saveFormToLocalStorage());
    };

    handleCloseForm = () => {
        this.props.handleClosePopup();
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const { form_isValid } = this.state;

        if (form_isValid) {
            this.toggleIsLoading();
            customFetch('/shop-request', {
                method: 'POST',
                body: JSON.stringify(this.getDataFromState())
            })
                .then(data => {
                    if (data.status === 'SUCCESS') {
                        this.clearFormLocalStorage();
                        this.setState({
                            form_sent: true
                        }, () => {
                            setTimeout(() => {this.handleCloseForm();}, 5000);
                        });
                    } else if (data.status === 'ERROR') {
                        this.toggleIsLoading();
                        this.TakeApartErrorFromServer(data);
                    }
                })
                .catch(() => {
                    this.setFormError('critical_error', true);
                    this.toggleIsLoading();
                });
        }
    };

    prettyfyUserInput = (input_string) => {
        const pretty_string = input_string.replace(/\r?\n|\r/g, ',').split(',');
        let result = '';
        for (let i = 0; i < pretty_string.length; i++) {
            let current_str_elem = pretty_string[i].trim();
            if (current_str_elem.length > 0) {
                result += (current_str_elem + ', ');
            }
        }
        return result.substr(0, result.length - 2);
    };

    render() {
        const { form_sent, form_isValid, form_isLoading, server_errors_messages, server_errors } = this.state;
        const { shop_name, description, phones, work_schedule, web_links, legal_entity_name, inn, kpp, ogrn, legal_entity_address, legal_entity_postal_address, unique_design } = this.state.form;

        if (form_sent) {
            return (
                <div className="message-popup message-popup-create-shop">
                    <div className="form-popup">
                        <h4 className="popup-title">Создание «Магазина»</h4>
                        <div className="form-popup__info-text">
                            Ваша заявка отправлена. В ближайшее время с вами свяжется наш оператор для обработки заявки. После чего, ваш профиль станет «Магазином».
                        </div>
                    </div>
                </div>
            );
        }

        return (
            <div className="form-create-shop">
                <div className="form-popup">
                    <h4 className="popup-title">Создание «Магазина»</h4>
                    <div className="form-popup__info-text">
                        После заполнения формы с Вами свяжется наш оператор для обработки заявки.
                    </div>
                    <ShopName
                        label="Название магазина:"
                        placeholder="Введите название магазина"
                        name="shop_name"
                        value_prop={shop_name}
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                    />
                    <ControlDescription
                        label="Описание магазина:"
                        name="description"
                        placeholder="Введите описание магазина"
                        value_prop={description}
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                    />
                    <ControlDescription
                        label="Номера телефонов:"
                        name="phones"
                        placeholder="Введите номера телефонов (по одному в строке или через запятую)"
                        value_prop={phones}
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                    />
                    <ControlDescription
                        label="График работы:"
                        name="work_schedule"
                        placeholder="Введите график работы (по одной строке или через запятую)"
                        value_prop={work_schedule}
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                    />
                    <ControlDescription
                        label="Ссылки:"
                        name="web_links"
                        placeholder="Введите ссылки на сайты или социальные сети (по одной строке или через запятую)"
                        value_prop={web_links}
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                    />
                    <LegalEntityName
                        label="Наименование юр. лица:"
                        placeholder="Введите наименование юридического лица"
                        name="legal_entity_name"
                        value_prop={legal_entity_name}
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                    />
                    <LegalEntityName
                        label="Юридический адрес:"
                        placeholder="Введите адрес юридического лица"
                        name="legal_entity_address"
                        value_prop={legal_entity_address}
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                    />
                    <LegalEntityName
                        label="Почтовый адрес:"
                        placeholder="Введите почтовый адрес"
                        name="legal_entity_postal_address"
                        value_prop={legal_entity_postal_address}
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                    />
                    <Inn
                        label="ИНН:"
                        placeholder="Введите ИНН"
                        name="inn"
                        value_prop={inn}
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                    />
                    <Kpp
                        label="КПП:"
                        placeholder="Введите КПП"
                        name="kpp"
                        value_prop={kpp}
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                    />
                    <Ogrn
                        label="ОГРН:"
                        placeholder="Введите ОГРН"
                        name="ogrn"
                        value_prop={ogrn}
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                    />
                    <label className="form-checkbox">
                        <input type="checkbox" name="unique_design" onChange={this.handleChangeUniqueDesign} checked={unique_design} value={unique_design} />
                        <span className="form-checkbox__field" />
                        <span className="form-checkbox__label">Нужно уникальное оформление</span>
                    </label>
                    <div className="form-popup__footer">
                        <button className="button-transparent-with-stroke" type="button" onClick={this.handleCloseForm}>Отменить</button>
                        <button className="button-blue" type="submit" onClick={this.handleSubmit} disabled={!form_isValid}>Отправить заявку</button>
                    </div>
                    <ShowError messages={server_errors_messages} existing_errors={server_errors} />
                </div>
                {form_isLoading && <LoaderSpinner isAbsolute />}
            </div>
        );
    }
}