import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import FormBase from './FormBase';
import Description from '../controls/ControlDescription';
import Email from '../controls/ControlEmail';
import ShowError from '../ShowError';
import LoaderSpinner from '../components/LoaderSpinner';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';
import { loadCSRF } from '../actions';
import {customFetch} from '../Helpers';
import withProvider from '../hocs/withProvider';

class FormComplaint extends FormBase {
    constructor(props) {
        super(props);

        this.state = {
            form: {
                description: '',
                email: '',
            },
            controls_server_errors: {
                description: null,
                email: null
            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validate_props
            validation: {
                description_is_valid: false,
                email_is_valid: false
            },
            form_isValid: false,
            form_isLoading: false,
            is_request_sent: false,
        };

        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.showForm = this.showForm.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.getDataFromState = this.getDataFromState.bind(this);
    }

    componentDidMount () {
        const { csrf, loadCSRF } = this.props;
        if (!csrf.isLoaded && !csrf.isLoading) {
            loadCSRF();
        }
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    showForm() {
        this.setState({
            formOpen: true
        });
    }


    getDataFromState() {
        const { form } = this.state;
        let form_to_send = {
            ...form
        };

        Object.keys(form).map(function(key) {
            let prefix = '';

            switch(key) {
                case 'email':
                    prefix = 'Контактный email: ';
                    break;
                case 'description':
                    prefix = 'Описание жалобы: ';
                    break;
                default:
                    prefix = '';
            }
            form_to_send[key] = prefix + form[key];
        });

        return form_to_send;
    }


    handleSubmit (e) {
        e.preventDefault();
        const { form_isValid } = this.state;
        const { description, email } = this.state.form;
        const { csrf } = this.props;

        if (form_isValid) {
            this.toggleIsLoading();
            customFetch('/complain-request', {
                method: 'POST',
                body: JSON.stringify({...this.getDataFromState(), csrf: csrf.value})
            })
                .then(data => {
                    if (data.status === 'SUCCESS') {
                        this.clearFormLocalStorage();
                        this.setState({
                            is_request_sent: true
                        }, () => {
                            setTimeout(() => {this.handleCloseForm();}, 5000);
                        });
                    } else if (data.status === 'ERROR') {
                        this.toggleIsLoading();
                        this.TakeApartErrorFromServer(data);
                    }
                })
                .catch(() => {
                    this.setFormError('critical_error', true);
                    this.toggleIsLoading();
                });
        }
    }

    render () {
        const {form_isValid, server_errors_messages, form_isLoading, is_request_sent} = this.state;
        const {description, email} = this.state.form;
        const { serverErrors } = this.props;

        const mesagges_after_submit = 'Спасибо! Ваше обращение принято!';

        return (
            <div className="js-popup-content">
                <form className="form-popup complaint-form" onSubmit={this.handleSubmit}>
                    <h4 className="popup-title">Описание жалобы</h4>
                    {
                        !is_request_sent ?
                            <div>
                                <div className="flexible-row">
                                    <Email
                                        label="Контактный email:"
                                        name="email"
                                        placeholder="Введите Ваш e-mail"
                                        handleComponentChange={this.handleChange}
                                        handleComponentValidation={this.handleComponentValid}
                                        value_prop={email}
                                    />
                                </div>
                                <Description
                                    label="Причина:"
                                    name="description"
                                    handleComponentChange={this.handleChange}
                                    handleComponentValidation={this.handleComponentValid}
                                    value_prop={description}
                                />
                                <div className="form-popup__footer">
                                    <button className="button-transparent-with-stroke js-close-complaint-form"  type="button">Отменить</button>
                                    <button className="button-blue" type="submit" onClick={this.handleSubmit} disabled={!form_isValid}>Отправить</button>
                                </div>
                            </div>
                            :
                            <div className="form-popup__info-text mesagges_after_submit">{mesagges_after_submit}</div>
                    }

                    <ShowError messages={server_errors_messages} existing_errors={serverErrors} />
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        csrf: state.config.csrf,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        loadCSRF: bindActionCreators(loadCSRF, dispatch),
    };
};

export default withProvider( connect(mapStateToProps, mapDispatchToProps)(FormComplaint));
