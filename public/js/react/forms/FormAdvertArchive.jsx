import React from 'react';
import PropTypes from 'prop-types';
import FormBase from './FormBase';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';
import Login from '../controls/ControlLogin.jsx';
import Captcha from '../controls/ControlCaptcha.jsx';
import Password from '../controls/ControlPasswordAuth.jsx';
import ShowError from '../ShowError.jsx';
import { getCookie } from '../Helpers';
import AdvertPreview from '../components/ProductPreview';

export default class FormAdvertPaused extends FormBase {
    constructor(props) {
        super(props);

        this.state = {

        };

        // this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    handleSubmit = (e) => {
        e.preventDefault();
        // const { form_isValid, form } = this.state;
        // const { submitHandler } = this.props;
        //
        // if (form_isValid) {
        //     submitHandler({...form});
        // } else {
        //     this.setFormErrorAnimation();
        // }
    };

    getFormattedDate(date) {
        const date_string = date.toISOString().substring(0, 10).split('-');
        return date_string[2].replace(/^0+/, '') + '.' + date_string[1] + '.' + date_string[0];
    }

    render() {
        const { product, is_operator } = this.props;
        const created_date = new Date(product.created);
        const created_formatted = this.getFormattedDate(created_date);
        const stopping_date = new Date(product.created);
        // TODO: брать настоящую дату архивации
        const stopping_formatted = this.getFormattedDate(stopping_date);

        return (
            <div className="popup advert_actions_popup">
                <div className="advert_actions_popup_header">
                    <div className="advert_popup_header_title">
                        <div className="advert_popup_header_icon paused">&nbsp;</div>
                        <h4 className="popup-title">В архиве</h4>
                    </div>
                    <div className="advert_info">
                        <table>
                            <tbody>
                                <tr>
                                    <td className="prop_name">
                                        Дата создания:
                                    </td>
                                    <td className="value">
                                        {created_formatted}
                                    </td>
                                </tr>
                                <tr>
                                    <td className="prop_name">
                                        В архиве с:
                                    </td>
                                    <td className="value">
                                        {stopping_formatted}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="advert_actions_popup_content">
                    <AdvertPreview
                        name={product.name}
                        price={product.price}
                        description={product.description}
                        category={product.category}
                        sub_category={product.sub_category}
                        address={product.address}
                        galleryFiles={product.gallery}
                        user={product.owner}
                        not_show_public_link={true}
                    />
                </div>
                {
                    !is_operator &&
                    <div className="advert_actions_popup_buttons paused">
                        <button className="button-popup button-popup-white" type="button">Восстановить</button>
                        <button className="button-popup button-popup-white" type="button">Удалить</button>
                    </div>
                }
            </div>
        );
    }
}

// FormAdvertActions.propTypes = {
//     submitHandler: PropTypes.func.isRequired,
//     serverErrors: PropTypes.object,
// };