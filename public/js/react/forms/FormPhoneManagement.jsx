import React from 'react';
import PropTypes from 'prop-types';
import FormBase from './FormBase';
import Phone from '../controls/ControlPhone';
import LoaderSpinner from '../components/LoaderSpinner';
import ShowError from '../ShowError';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';

class FormPhoneManagement extends FormBase {
    constructor(props) {
        super(props);

        this.state = {
            controls_server_errors: {
                csrf: null,
                phone: null
            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validate_props
            validation: {
                phone_is_valid: false
            },
            form: {
                phone: '',
            },
            form_isValid: false,
        };

        this.prev_state = null;
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    componentWillReceiveProps(nextProps) {
        if (Object.keys(nextProps.serverErrors).length > 0) {
            this.TakeApartErrorFromServer(nextProps.serverErrors);
        }
    }

    handleSubmit(e) {
        e.preventDefault();
        const { form_isValid, form: { phone } } = this.state;
        const { changePhoneHandler, currentPhone, csrf, form_type } = this.props;
        if (form_isValid) {
            changePhoneHandler(JSON.stringify({
                csrf: csrf.value,
                current_phone: `+${currentPhone}`,
                new_phone: phone,
                phone: phone,
            }), form_type);
        }
    }

    render() {
        const { form_label, phoneIsChanging, serverErrors, form_type } = this.props;

        return (
            <div className={`popup phone-change-popup`}>
                <div className="form-popup">
                    <form onSubmit={this.handleSubmit}>
                        <h4 className="popup-title">{form_label}</h4>
                        <Phone
                            name="phone"
                            placeholder="Введите новый номер телефона"
                            handleComponentChange={this.handleChange}
                            handleComponentValidation={this.handleComponentValid}
                            validateOnChange={true}
                            componentValue={this.handleChange}
                        />
                        <div className="form-popup__info-text">
                            После добавления телефона Вам будет необходимо его подтвердить.
                        </div>
                        <button className="button-blue" type="button" onClick={this.handleSubmit}>{form_type === 'add' ? 'Добавить' : 'Получить код'}</button>
                        <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
                        {
                            phoneIsChanging &&
                            <LoaderSpinner isAbsolute />
                        }
                    </form>
                </div>
            </div>
        );
    }
}

FormPhoneManagement.propTypes = {
    currentPhone: PropTypes.string,
    phoneIsChanging: PropTypes.bool.isRequired,
    serverErrors: PropTypes.object.isRequired,
    csrf: PropTypes.object.isRequired,
    form_label: PropTypes.string,
    form_type: PropTypes.string,
    changePhoneHandler: PropTypes.func,
};
FormPhoneManagement.defaultProps = {
    form_label: '',
    changePhoneHandler: () => {},
    form_type: 'add',
};

export default FormPhoneManagement;
