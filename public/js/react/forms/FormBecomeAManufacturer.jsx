import React from 'react';
import FormBase from './FormBase';
import CompanyName from '../controls/ControlShopName';
import LegalEntityAddress from '../controls/ControlLegalEntityName';
import Inn from '../controls/ControlInn';
import Kpp from '../controls/ControlKpp';
import ControlDescription from '../controls/ControlDescription';
import LoaderSpinner from '../components/LoaderSpinner';
import ShowError from '../ShowError';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';
import { customFetch } from '../Helpers';

export default class FormBecomeAManufacturer extends FormBase {
    constructor(props) {
        super(props);

        this.state = {
            controls_server_errors: {
                company_name: null,
                legal_entity_address: null,
                inn: null,
                kpp: null,
                categories: null
            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validate_props
            validation: {
                company_name_is_valid: false,
                legal_entity_address_is_valid: false,
                inn_is_valid: false,
                kpp_is_valid: false,
                categories_is_valid: false
            },
            form: {
                csrf: '',
                company_name: '',
                legal_entity_address: '',
                categories: '',
                inn: '',
                kpp: ''
            },
            form_isValid: false,
            form_isLoading: false,
            form_isSubmitting: false,
            form_sent: false
        };

        this.prev_state = null;
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    componentWillMount() {
        this.getCsrf();
    }

    componentWillUpdate = () => {
        this.tryToRunValidationFunctions();
    };

    handleCloseForm = () => {
        this.props.handleClosePopup();
    };

    getDataFromState = () => {
        let { csrf, company_name, inn, kpp, legal_entity_address, categories } = this.state.form;
        let { login, email, phone, is_email_verified, is_phone_verified } = this.props;

        login = 'Логин: ' + login;
        email =  email ? 'E-mail: ' + email + (is_email_verified ? '' : ' (не подтвержден)') : 'E-mail: отсутствует';
        phone =  phone ? 'Телефон: +' + phone + (is_phone_verified ? '' : ' (не подтвержден)') : 'Телефон: отсутствует';
        company_name = 'Название компании: ' + company_name;
        inn = 'ИНН: ' + inn;
        kpp = 'КПП: ' + kpp;
        legal_entity_address = 'Юридический адрес: ' + legal_entity_address;
        categories = 'Желаемые категории: ' + categories;

        return { csrf, login, email, phone, company_name, inn, kpp, legal_entity_address, categories };
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const { form_isValid } = this.state;

        if (form_isValid) {
            this.toggleIsLoading();
            customFetch('/fabricator-request', {
                method: 'POST',
                body: JSON.stringify(this.getDataFromState())
            })
                .then(data => {
                    if (data.status === 'SUCCESS') {
                        this.clearFormLocalStorage();
                        this.setState({
                            form_sent: true
                        }, () => {
                            setTimeout(() => {this.handleCloseForm();}, 5000);
                        });
                    } else if (data.status === 'ERROR') {
                        this.toggleIsLoading();
                        this.TakeApartErrorFromServer(data);
                    }
                })
                .catch(() => {
                    this.setFormError('critical_error', true);
                    this.toggleIsLoading();
                });
        }
    };

    render() {
        const { form_isValid, form_isLoading, form_sent, server_errors, server_errors_messages } = this.state;
        const { categories, legal_entity_address, inn, kpp, company_name } = this.state.form;

        if (form_sent) {
            return (
                <div className="message-popup message-popup-get-status-producer">
                    <div className="form-popup">
                        <h4 className="popup-title">Получить статус производителя</h4>

                        <div className="form-popup__info-text">
                            Ваша заявка отправлена. В ближайшее время с вами свяжется наш оператор для обработки заявки. После чего, вам будет присвоен статус производителя.
                        </div>
                    </div>
                </div>
            );
        }

        return (
            <div id="form-create-status-producer" className="form-get-status-producer">
                <div className="form-popup">
                    <h4 className="popup-title">Получить статус производителя</h4>

                    <div className="form-popup__info-text">
                        После заполнения формы с Вами свяжется наш оператор для обработки заявки.
                    </div>
                    <CompanyName
                        label="Название компании:"
                        placeholder="Введите название компании"
                        name="company_name"
                        value_prop={company_name}
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                    />
                    <Inn
                        label="ИНН:"
                        placeholder="Введите ИНН"
                        name="inn"
                        value_prop={inn}
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                    />
                    <Kpp
                        label="КПП:"
                        placeholder="Введите КПП"
                        name="kpp"
                        value_prop={kpp}
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                    />
                    <LegalEntityAddress
                        label="Юридический адрес:"
                        placeholder="Введите адрес юридического лица"
                        name="legal_entity_address"
                        value_prop={legal_entity_address}
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                    />
                    <ControlDescription
                        label="Разделы для размещения объявлений:"
                        name="categories"
                        placeholder="Например: пиломатериалы, кирпич"
                        value_prop={categories}
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                    />
                    <div className="form-popup__footer">
                        <button className="button-transparent-with-stroke" type="button" onClick={this.handleCloseForm}>Отменить</button>
                        <button className="button-blue" type="submit" disabled={!form_isValid} onClick={this.handleSubmit}>Отправить заявку</button>
                    </div>
                    <ShowError messages={server_errors_messages} existing_errors={server_errors} />
                </div>
                {form_isLoading && <LoaderSpinner isAbsolute />}
            </div>
        );
    }
}