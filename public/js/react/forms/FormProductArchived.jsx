import React from 'react';
import PropTypes from 'prop-types';
import FormBase from './FormBase';
import { customFetch, getMetaCSRF, getDateDotFormatFromTimestamp } from '../Helpers';
import AdvertPreview from '../components/ProductPreview';

export default class FormProductArchived extends FormBase {
    constructor(props) {
        super(props);

        this.state = {
            form: {
                csrf: ''
            }
        };

        // this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    componentWillMount() {
        const csrf = getMetaCSRF();
        if (csrf) {
            this.setState({
                form: {
                    ...this.state.form,
                    csrf
                }
            });
        } else {
            this.getCsrf();
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const product = this.props.product || {};
        const archive_url = `/product/${product.id}/unarchive`;

        const { csrf } = this.state.form;

        customFetch(archive_url, {
            method: 'POST',
            body: JSON.stringify({
                csrf
            })
        })
            .then((response) => {
                window.sessionStorage.setItem('profileMessage', 'archive_restore');
                window.location.reload();
            });
    };

    handleDelete = (e) => {
        e.preventDefault();
        const product = this.props.product || {};
        const deleted_url = `/product/${product.id}`;

        customFetch(deleted_url, {
            method: 'DELETE',
        })
            .then((response) => {
                window.sessionStorage.setItem('profileMessage', 'archive_deleted');
                window.location.reload();
            });
    };


    render() {
        const { product, is_operator } = this.props;

        const created_formatted = getDateDotFormatFromTimestamp(product.created_timestamp);
        const archive_formatted = getDateDotFormatFromTimestamp(product.changed_timestamp);

        return (
            <div className="popup advert_actions_popup">
                <div className="advert_actions_popup_header declined">
                    <div className="advert_popup_header_title">
                        <div className="advert_popup_header_icon archive">&nbsp;</div>
                        <h4 className="popup-title">В архиве</h4>
                    </div>
                    <div className="advert_info">
                        <table>
                            <tbody>
                                <tr>
                                    <td className="prop_name">
                                        Дата создания:
                                    </td>
                                    <td className="value">
                                        {created_formatted}
                                    </td>
                                </tr>
                                <tr>
                                    <td className="prop_name">
                                        В архиве с:
                                    </td>
                                    <td className="value">
                                        {archive_formatted}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="advert_actions_popup_content">
                    <AdvertPreview
                        name={product.name}
                        price={product.price}
                        description={product.description}
                        category={product.category}
                        sub_category={product.sub_category}
                        address={product.address}
                        galleryFiles={product.gallery}
                        files={product.files}
                        user={product.owner}
                        not_show_public_link={true}
                    />
                </div>
                {
                    !is_operator &&
                     <div className="advert_actions_popup_buttons">
                         <button className="button-popup button-purple" type="button" onClick={this.handleSubmit}>Восстановить</button>
                         <button className="button-popup button-popup-white" type="button" onClick={this.handleDelete}>Удалить</button>
                     </div>
                }
            </div>
        );
    }
}

FormProductArchived.propTypes = {
    is_operator: PropTypes.bool,
    product: PropTypes.object,
};