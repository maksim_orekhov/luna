import React from 'react';
import PropTypes from 'prop-types';
import FormBase from './FormBase';
import { customFetch, getMetaCSRF, getDateDotFormatFromTimestamp, getPurchaseResolutionDateDotFormat } from '../Helpers';
import PurchasePreview from '../components/PurchasePreview';
import DECLINE_REASONS from '../constants/decline_reasons';


export default class FormPurchaseDeclined extends FormBase {
    constructor(props) {
        super(props);

        this.state = {
            form: {
                csrf: ''
            }
        };
    }

    componentWillMount() {
        const csrf = getMetaCSRF();
        if (csrf) {
            this.setState({
                form: {
                    ...this.state.form,
                    csrf
                }
            });
        } else {
            this.getCsrf();
        }
    }

    handleEditButtonClick = () => {
        const { id } = this.props.purchase;
        window.location = `/purchase/${id}/edit`;
    };

    handleDeleteButtonClick = (e) => {
        e.preventDefault();
        const purchase = this.props.purchase || {};
        const urls = purchase.urls || {};
        const archive_url = urls.archive_url;

        const { csrf } = this.state.form;

        if (!archive_url) {
            console.error('Не получен url для восстановления из архива');
            return;
        }

        customFetch(archive_url, {
            method: 'POST',
            body: JSON.stringify({
                csrf: csrf,
                archive: 1
            })
        })
            .then((response) => {
                window.sessionStorage.setItem('profileMessage', 'archive');
                window.location.reload();
            });
    };

    render() {
        const { purchase, is_operator } = this.props;

        const created_formatted = getDateDotFormatFromTimestamp(purchase.created_timestamp);
        const resolution_formatted = getPurchaseResolutionDateDotFormat(purchase);

        const declining_reason = (purchase.moderation && purchase.moderation.description) || '';
        // const recommendations = (purchase.moderation && purchase.moderation.recommendations) || '';
        const recommendations = DECLINE_REASONS.find(reason => reason.title === declining_reason)
            ?
            DECLINE_REASONS.find(reason => reason.title === declining_reason).recommendation
            :
            '';

        return (
            <div className="popup advert_actions_popup">
                <div className="advert_actions_popup_header declined">
                    <div className="advert_popup_header_title">
                        <div className="advert_popup_header_icon declined">&nbsp;</div>
                        <h4 className="popup-title">Отклонено модератором</h4>
                    </div>
                    <div className="advert_info">
                        <table>
                            <tbody>
                                <tr>
                                    <td className="prop_name">
                                        Дата создания:
                                    </td>
                                    <td className="value">
                                        {created_formatted}
                                    </td>
                                </tr>
                                <tr>
                                    <td className="prop_name">
                                        Отклонено:
                                    </td>
                                    <td className="value">
                                        {resolution_formatted}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="declined_advert_reason">
                    Причина отклонения: { declining_reason }
                    <pre className="recommendations">{ recommendations }</pre>
                </div>
                <div className="advert_actions_popup_content">
                    <PurchasePreview
                        name={purchase.name}
                        price={purchase.price}
                        quantity={purchase.quantity}
                        measure={purchase.measure}
                        description={purchase.description}
                        category={purchase.category}
                        sub_category={purchase.sub_category}
                        address={purchase.address}
                        files={purchase.files}
                        user={purchase.owner}
                        purchase_url={purchase.urls.single_purchase_public}
                        not_show_public_link={true}
                    />
                </div>
                {
                    !is_operator &&
                    <div className="advert_actions_popup_buttons">
                        <button className="button-popup button-popup-white" onClick={this.handleEditButtonClick} type="button">Редактировать</button>
                        <button className="button-popup button-popup-white" onClick={this.handleDeleteButtonClick} type="button">Удалить</button>
                    </div>
                }
            </div>
        );
    }
}

FormPurchaseDeclined.propTypes = {
    is_operator: PropTypes.bool,
    purchase: PropTypes.object,
};

FormPurchaseDeclined.defaultProps = {
    purchase: {},
};