import React, { Component } from 'react';
import { setCookie } from '../Helpers';
import PropTypes from 'prop-types';
import LoaderSpinner from '../components/LoaderSpinner';
import Product from '../components/Product';
import ContentHeader from '../components/ContentHeader';
import TableHeader from '../components/TableHeader';

export default class TableProducts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            viewMode: props.view_mode || 'block'
        };
    }

    componentDidMount = () => {
        this.props.getPaginator();
    };

    changeViewMode = (viewMode) => () => {
        this.setState({
            viewMode
        }, () => {
            setCookie('view_mode', viewMode);
        });
    };

    showMoreProducts = (e) => {
        e.preventDefault();
        this.props.showProducts();
    };

    render() {
        const { viewMode } = this.state;
        const { showMore, showButton, products, purchases_url, users_products, handleChangeSearch, handleFindButtonClick, handleKeyPress, isLoading } = this.props;

        return(
            <section className="content">
                <TableHeader
                    purchases_url={purchases_url}
                    type="products"
                    viewMode={viewMode}
                    changeViewMode={this.changeViewMode}
                    handleChangeSearch={handleChangeSearch}
                    handleFindButtonClick={handleFindButtonClick}
                    handleKeyPress={handleKeyPress}
                    no_burger={true}
                />
                <section className="content-main">
                    <div className="container">
                        <div className={`catalog ${viewMode === 'list' ? 'is-list-catalog' : 'is-block-catalog'}`}>
                            <div className="row">
                                {
                                    products.length > 0
                                        ? products.map((product) => {
                                            return <Product
                                                key={product.id}
                                                urls={product.urls}
                                                category={product.category}
                                                subCategory={product.sub_category}
                                                gallery={product.gallery}
                                                name={product.name}
                                                created={product.created}
                                                created_timestamp={product.created_timestamp}
                                                price={product.price}
                                                description={product.description}
                                                fabricator={product.fabricator}
                                            />;
                                        })
                                        :
                                        <div className="page-message-container">
                                            <img src="/img/union.svg" alt="" className="page-message-image" />
                                            <h2 className="page-message">Объявлений не найдено</h2>
                                        </div>
                                }
                            </div>
                            {
                                showMore
                                    ?
                                    <div className="row button-row">
                                        <LoaderSpinner isTiny={true} />
                                    </div>
                                    : showButton
                                        ?
                                        <div className="row button-row">
                                            <div className="col-12">
                                                <a href="#" className="button-transparent" onClick={this.showMoreProducts}>Показать больше</a>
                                            </div>
                                        </div>
                                        : null
                            }
                        </div>
                    </div>
                    {
                        isLoading &&
                        <div className="row button-row">
                            <LoaderSpinner isTiny={true} />
                        </div>
                    }
                </section>
            </section>
        );
    }
}

TableProducts.propTypes = {
    products: PropTypes.array,
    breadcrumbs: PropTypes.array,
    view_mode: PropTypes.string,
    getPaginator: PropTypes.func,
    showProducts: PropTypes.func,
    showMore: PropTypes.bool,
    showButton: PropTypes.bool,
    purchases_url: PropTypes.string
};

TableProducts.defaultProps  = {
    products: [],
    breadcrumbs: [],
    view_mode: 'list',
    getPaginator: () => {},
    showProducts: () => {},
    showMore: false,
    showButton: true,
    purchases_url: ''
};