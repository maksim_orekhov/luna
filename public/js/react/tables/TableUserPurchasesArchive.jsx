import React from 'react';
import PropTypes from 'prop-types';
import Purchase from '../components/Purchase';

function TableUserPurchasesArchive({ purchases, onTableRowClick }) {
    return (
        <section className="content">
            <section className="content-main">
                <div className="container">
                    <div className="catalog is-list-catalog">
                        <div className="row">
                            {
                                purchases.length > 0
                                    ? purchases.map((purchase, index) => {
                                        return <Purchase
                                            key={index}
                                            urls={purchase.urls}
                                            category={purchase.category}
                                            subCategory={purchase.sub_category}
                                            name={purchase.name}
                                            created={purchase.created}
                                            created_timestamp={purchase.created_timestamp}
                                            price={purchase.price}
                                            quantity={purchase.quantity}
                                            owner={purchase.owner}
                                            measure={purchase.measure}
                                            description={purchase.description}
                                            status={purchase.status}
                                            onClick={
                                                (e) => {
                                                    e.preventDefault();
                                                    onTableRowClick(purchase);
                                                }
                                            }
                                            is_profile={true}
                                        />;
                                    })
                                    :
                                    <div className="page-message-container">
                                        <img src="/img/union.svg" alt="" className="page-message-image" />
                                        <h2 className="page-message">Закупок не найдено</h2>
                                    </div>
                            }
                        </div>
                    </div>
                </div>
            </section>
        </section>
    );
}

TableUserPurchasesArchive.propTypes = {
    purchases: PropTypes.array
};

TableUserPurchasesArchive.defaultProps  = {
    purchases: []
};

export default TableUserPurchasesArchive;