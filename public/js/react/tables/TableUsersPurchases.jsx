import React, { Component } from 'react';
import { setCookie } from '../Helpers';
import PropTypes from 'prop-types';
import LoaderSpinner from '../components/LoaderSpinner';
import Purchase from '../components/Purchase';
import TableHeader from '../components/TableHeader';

export default class TableUsersPurchases extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search_value: ''
        };
    }

    componentDidMount = () => {
        this.props.getPaginator();
    };

    showMorePurchases = (e) => {
        e.preventDefault();
        this.props.showPurchases();
    };

    handleChangeSearch = (e) => {
        this.setState({
            search_value: e.target.value
        });
    };

    render() {
        const { search_value } = this.state;
        const { showMore, showButton, purchases, products_url, purchases_url, handleChangeSearch, handleFindButtonClick, handleKeyPress, isLoading } = this.props;

        return(
            <section className="content">
                <TableHeader
                    products_url={products_url}
                    type="purchases"
                    search_value={search_value}
                    handleChangeSearch={handleChangeSearch}
                    handleFindButtonClick={handleFindButtonClick}
                    handleKeyPress={handleKeyPress}
                    no_burger={true}
                />
                <section className="content-main">
                    <div className="container">
                        <div className="catalog is-list-catalog">
                            <div className="row">
                                {
                                    purchases.length > 0
                                        ? purchases.map((purchase, index) => {

                                            return <Purchase
                                                key={index}
                                                urls={purchase.urls}
                                                category={purchase.category}
                                                subCategory={purchase.sub_category}
                                                name={purchase.name}
                                                created={purchase.created}
                                                created_timestamp={purchase.created_timestamp}
                                                price={purchase.price}
                                                quantity={purchase.quantity}
                                                owner={purchase.owner}
                                                measure={purchase.measure}
                                                description={purchase.description}
                                            />;
                                        })
                                        :
                                        <div className="page-message-container">
                                            <img src="/img/union.svg" alt="" className="page-message-image" />
                                            <h2 className="page-message">Закупок не найдено</h2>
                                        </div>
                                }
                            </div>
                            {
                                showMore
                                    ?
                                    <div className="row button-row">
                                        <LoaderSpinner isTiny={true} />
                                    </div>
                                    : showButton
                                        ?
                                        <div className="row button-row">
                                            <div className="col-12">
                                                <a href="#" className="button-transparent" onClick={this.showMorePurchases}>Показать больше</a>
                                            </div>
                                        </div>
                                        : null
                            }
                        </div>
                    </div>
                    {
                        isLoading &&
                        <div className="row button-row">
                            <LoaderSpinner isTiny={true} />
                        </div>
                    }
                </section>
            </section>
        );
    }
}

TableUsersPurchases.propTypes = {
    purchases: PropTypes.array,
    getPaginator: PropTypes.func,
    showPurchases: PropTypes.func,
    showMore: PropTypes.bool,
    showButton: PropTypes.bool,
    products_url: PropTypes.string
};

TableUsersPurchases.defaultProps  = {
    purchases: [],
    getPaginator: () => {},
    showPurchases: () => {},
    showMore: false,
    showButton: true,
    products_url: ''
};