import React from 'react';
import PropTypes from 'prop-types';
import {
    getCategoryNameForProduct,
    getSubCategoryNameForProduct,
    getAuthorNameForProduct
} from '../Helpers';

const columns = [
    {
        name: 'id',
        title: 'ID'
    },
    {
        name: 'archive_since',
        title: 'В архиве с',
    },
    {
        name: 'category',
        title: 'Категория',
    },
    {
        name: 'sub_category',
        title: 'Подкатегория',
    },
    {
        name: 'name',
        title: 'Название',
    },
    {
        name: 'author',
        title: 'Автор',
    },
];

function TableModerationProductsArchive({ products, onTableRowClick }) {
    return(
        <div className="table">
            <table>
                <thead className="th">
                    <tr className="th">
                        {columns.map((column) => {
                            return (
                                <th className="td" key={column.name}>
                                    <a className="link-sort" href="#" onClick={() => {}}>{column.title}</a>
                                </th>
                            );
                        })}
                    </tr>
                </thead>
                <tbody className="tbody">
                    { products.length !== 0 && products.map((product) => {
                        // console.log(product);
                        return (
                            <tr className="tr" key={product.id} onClick={onTableRowClick(product)}>
                                <td className="td td_size-5">
                                    <div className="td__title-mobile">ID</div>
                                    <div className="td__content">{product.id}</div>
                                </td>
                                <td className="td td_size-15">
                                    <div className="td__title-mobile">В архиве с</div>
                                    <div className="td__content">{product.changed || ''}</div>
                                </td>
                                <td className="td td_size-15">
                                    <div className="td__title-mobile">Категория</div>
                                    <div className="td__content">{getCategoryNameForProduct(product)}</div>
                                </td>
                                <td className="td td_size-20">
                                    <div className="td__title-mobile">Подкатегория</div>
                                    <div className="td__content">{getSubCategoryNameForProduct(product)}</div>
                                </td>
                                <td className="td">
                                    <div className="td__title-mobile">Название</div>
                                    <div className="td__content">{product.name}</div>
                                </td>
                                <td className="td">
                                    <div className="td__title-mobile">Автор</div>
                                    <div className="td__content">
                                        <a href="#" className="link-inline">
                                            {getAuthorNameForProduct(product)}
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        );
                    }) }
                    {
                        products.length === 0 &&
                        <tr>
                            <td colSpan="6" style={{textAlign: 'center'}}>Архивных объявлений не найдено</td>
                        </tr>
                    }
                </tbody>
            </table>
        </div>
    );
}

TableModerationProductsArchive.propTypes = {
    products: PropTypes.array,
    onOrderLinkClick: PropTypes.func,
    onModerationItemClick: PropTypes.func,
    onTableRowClick: PropTypes.func
};

TableModerationProductsArchive.defaultProps = {
    products: [],
    onOrderLinkClick: () => {},
    onModerationItemClick: () => {},
    onTableRowClick: () => {}
};

export default TableModerationProductsArchive;
