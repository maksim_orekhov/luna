import React from 'react';
import PropTypes from 'prop-types';
import {
    checkThatNestedKeyOfObjectExist,
    getModerationResolutionTimeForProduct,
    getAuthorNameForProduct
} from '../Helpers';


const columns = [
    {
        name: 'id',
        title: 'ID'
    },
    {
        name: 'declined_time',
        title: 'Отклонено',
    },
    {
        name: 'reason',
        title: 'Причина',
    },
    {
        name: 'name',
        title: 'Название',
    },
    {
        name: 'author',
        title: 'Автор',
    },
];

function TableModerationProductsAreDeclined({ products, onTableRowClick }) {
    return(
        <div className="table">
            <table>
                <thead className="th">
                    <tr className="th">
                        {columns.map((column) => {
                            return (
                                <th className="td" key={column.name}>
                                    <a className="link-sort" href="#" onClick={() => {}}>{column.title}</a>
                                </th>
                            );
                        })}
                    </tr>
                </thead>
                <tbody className="tbody">
                    { products.length !== 0 && products.map((product) => {
                        return (
                            <tr className="tr" key={product.id} onClick={onTableRowClick(product)}>
                                <td className="td td_size-5">
                                    <div className="td__title-mobile">ID</div>
                                    <div className="td__content">{product.id}</div>
                                </td>
                                <td className="td td_size-15">
                                    <div className="td__title-mobile">Отклонено</div>
                                    <div className="td__content">{getModerationResolutionTimeForProduct(product)}</div>
                                </td>
                                <td className="td">
                                    <div className="td__title-mobile">Причина</div>
                                    <div className="td__content">
                                        {
                                            checkThatNestedKeyOfObjectExist(product, 'product_moderation', 'description')
                                            && product.product_moderation.description
                                                ? product.product_moderation.description
                                                : 'неизвестно'
                                        }
                                    </div>
                                </td>
                                <td className="td">
                                    <div className="td__title-mobile">Название</div>
                                    <div className="td__content">{product.name}</div>
                                </td>
                                <td className="td">
                                    <div className="td__title-mobile">Автор</div>
                                    <div className="td__content">
                                        <a href="#" className="link-inline">
                                            {getAuthorNameForProduct(product)}
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        );
                    }) }
                    {
                        products.length === 0 &&
                        <tr>
                            <td colSpan="5" style={{textAlign: 'center'}}>Отклоненных объявлений не найдено</td>
                        </tr>
                    }
                </tbody>
            </table>
        </div>
    );
}

TableModerationProductsAreDeclined.propTypes = {
    products: PropTypes.array,
    onOrderLinkClick: PropTypes.func,
    onModerationItemClick: PropTypes.func,
    onTableRowClick: PropTypes.func
};

TableModerationProductsAreDeclined.defaultProps = {
    products: [],
    onOrderLinkClick: () => {},
    onModerationItemClick: () => {},
    onTableRowClick: () => {}
};

export default TableModerationProductsAreDeclined;
