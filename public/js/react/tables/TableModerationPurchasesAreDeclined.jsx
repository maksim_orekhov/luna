import React from 'react';
import PropTypes from 'prop-types';
import {
    checkThatNestedKeyOfObjectExist,
    getModerationResolutionTimeForPurchase,
    getAuthorNameForPurchase
} from '../Helpers';


const columns = [
    {
        name: 'id',
        title: 'ID'
    },
    {
        name: 'declined_time',
        title: 'Отклонено',
    },
    {
        name: 'reason',
        title: 'Причина',
    },
    {
        name: 'name',
        title: 'Название',
    },
    {
        name: 'author',
        title: 'Автор',
    },
];

function TableModerationPurchasesAreDeclined({ purchases, onTableRowClick }) {
    return(
        <div className="table">
            <table>
                <thead className="th">
                    <tr className="th">
                        {columns.map((column) => {
                            return (
                                <th className="td" key={column.name}>
                                    <a className="link-sort" href="#" onClick={() => {}}>{column.title}</a>
                                </th>
                            );
                        })}
                    </tr>
                </thead>
                <tbody className="tbody">
                    { purchases.length !== 0 && purchases.map((purchase) => {
                        return (
                            <tr className="tr" key={purchase.id} onClick={onTableRowClick(purchase)}>
                                <td className="td td_size-5">
                                    <div className="td__title-mobile">ID</div>
                                    <div className="td__content">{purchase.id}</div>
                                </td>
                                <td className="td td_size-15">
                                    <div className="td__title-mobile">Отклонено</div>
                                    <div className="td__content">{getModerationResolutionTimeForPurchase(purchase)}</div>
                                </td>
                                <td className="td">
                                    <div className="td__title-mobile">Причина</div>
                                    <div className="td__content">
                                        {
                                            checkThatNestedKeyOfObjectExist(purchase, 'moderation', 'description')
                                            && purchase.moderation.description
                                                ? purchase.moderation.description
                                                : 'неизвестно'
                                        }
                                    </div>
                                </td>
                                <td className="td">
                                    <div className="td__title-mobile">Название</div>
                                    <div className="td__content">{purchase.name}</div>
                                </td>
                                <td className="td">
                                    <div className="td__title-mobile">Автор</div>
                                    <div className="td__content">
                                        <a href="#" className="link-inline">
                                            {getAuthorNameForPurchase(purchase)}
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        );
                    }) }
                    {
                        purchases.length === 0 &&
                        <tr>
                            <td colSpan="5" style={{textAlign: 'center'}}>Отклоненных закупок не найдено</td>
                        </tr>
                    }
                </tbody>
            </table>
        </div>
    );
}

TableModerationPurchasesAreDeclined.propTypes = {
    purchases: PropTypes.array,
    onOrderLinkClick: PropTypes.func,
    onModerationItemClick: PropTypes.func,
    onTableRowClick: PropTypes.func
};

TableModerationPurchasesAreDeclined.defaultProps = {
    purchases: [],
    onOrderLinkClick: () => {},
    onModerationItemClick: () => {},
    onTableRowClick: () => {}
};

export default TableModerationPurchasesAreDeclined;
