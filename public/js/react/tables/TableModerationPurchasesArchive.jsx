import React from 'react';
import PropTypes from 'prop-types';
import {
    getCategoryNameForPurchase,
    getSubCategoryNameForPurchase,
    getAuthorNameForPurchase
} from '../Helpers';

const columns = [
    {
        name: 'id',
        title: 'ID'
    },
    {
        name: 'archive_since',
        title: 'В архиве с',
    },
    {
        name: 'category',
        title: 'Категория',
    },
    {
        name: 'sub_category',
        title: 'Подкатегория',
    },
    {
        name: 'name',
        title: 'Название',
    },
    {
        name: 'author',
        title: 'Автор',
    },
];

function TableModerationPurchasesArchive({ purchases, onTableRowClick }) {
    return(
        <div className="table">
            <table>
                <thead className="th">
                    <tr className="th">
                        {columns.map((column) => {
                            return (
                                <th className="td" key={column.name}>
                                    <a className="link-sort" href="#" onClick={() => {}}>{column.title}</a>
                                </th>
                            );
                        })}
                    </tr>
                </thead>
                <tbody className="tbody">
                    { purchases.length !== 0 && purchases.map((purchase) => {
                        // console.log(product);
                        return (
                            <tr className="tr" key={purchase.id} onClick={onTableRowClick(purchase)}>
                                <td className="td td_size-5">
                                    <div className="td__title-mobile">ID</div>
                                    <div className="td__content">{purchase.id}</div>
                                </td>
                                <td className="td td_size-15">
                                    <div className="td__title-mobile">В архиве с</div>
                                    <div className="td__content">{purchase.changed || ''}</div>
                                </td>
                                <td className="td td_size-15">
                                    <div className="td__title-mobile">Категория</div>
                                    <div className="td__content">{getCategoryNameForPurchase(purchase)}</div>
                                </td>
                                <td className="td td_size-20">
                                    <div className="td__title-mobile">Подкатегория</div>
                                    <div className="td__content">{getSubCategoryNameForPurchase(purchase)}</div>
                                </td>
                                <td className="td">
                                    <div className="td__title-mobile">Название</div>
                                    <div className="td__content">{purchase.name}</div>
                                </td>
                                <td className="td">
                                    <div className="td__title-mobile">Автор</div>
                                    <div className="td__content">
                                        <a href="#" className="link-inline">
                                            {getAuthorNameForPurchase(purchase)}
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        );
                    }) }
                    {
                        purchases.length === 0 &&
                        <tr>
                            <td colSpan="6" style={{textAlign: 'center'}}>Архивных закупок не найдено</td>
                        </tr>
                    }
                </tbody>
            </table>
        </div>
    );
}

TableModerationPurchasesArchive.propTypes = {
    purchases: PropTypes.array,
    onOrderLinkClick: PropTypes.func,
    onModerationItemClick: PropTypes.func,
    onTableRowClick: PropTypes.func
};

TableModerationPurchasesArchive.defaultProps = {
    purchases: [],
    onOrderLinkClick: () => {},
    onModerationItemClick: () => {},
    onTableRowClick: () => {}
};

export default TableModerationPurchasesArchive;
