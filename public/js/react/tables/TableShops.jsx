import React, { Component } from 'react';
import PropTypes from 'prop-types';
import LoaderSpinner from '../components/LoaderSpinner';
import Shop from '../components/Shop';

export default class TableShops extends Component {

    componentDidMount = () => {
        this.props.getPaginator();
    };

    showMoreShops = (e) => {
        e.preventDefault();
        this.props.showShops();
    };

    render() {
        const { showMore, showButton, shops, isLoading } = this.props;

        return(
            <section className="content">
                <section className="content-header">
                    <div className="container">
                        <div className="row wrap-flex">
                            <div className="col-9">
                                <div className="content-header-navigation">
                                    <h2 className="content-header-navigation__title">Все магазины</h2>
                                </div>
                            </div>
                            <div className="col-3 col-tablet-4 col-mobile-8 col-small-4 search-select">
                                <div className="control-form">
                                    <div className="SelectImitation">
                                        <div className="SelectImitation-Container">
                                            {/* <div className="SelectImitation-Text" title="Во всех категориях">*/}
                                            {/* Во всех категориях*/}
                                            {/* </div>*/}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div className="container list-shops">
                    {
                        shops.length > 0
                            ? shops.map((shop, index) => {
                                return <Shop
                                    key={index}
                                    urls={shop.urls}
                                    categories={shop.categories}
                                    subCategories={shop.sub_categories}
                                    name={shop.name}
                                    created={shop.created}
                                    created_timestamp={shop.created_timestamp}
                                    owner={shop.owner}
                                    logo={shop.logo}
                                    description={shop.description}
                                />;
                            })
                            : <h2>Магазинов не найдено</h2>
                    }
                    {
                        showMore
                            ?
                            <div className="row button-row">
                                <LoaderSpinner isTiny={true} />
                            </div>
                            : showButton
                                ?
                                <div className="button-row">
                                    <a href="#" className="button-transparent-with-stroke" onClick={this.showMoreShops}>Показать больше</a>
                                </div>
                                : null
                    }
                    {
                        isLoading &&
                        <div className="row button-row">
                            <LoaderSpinner isTiny={true} />
                        </div>
                    }
                </div>

            </section>
        );
    }
}

TableShops.propTypes = {
    shops: PropTypes.array,
    breadcrumbs: PropTypes.array,
    view_mode: PropTypes.string,
    getPaginator: PropTypes.func,
    showShops: PropTypes.func,
    showMore: PropTypes.bool,
    showButton: PropTypes.bool,
};

TableShops.defaultProps  = {
    shops: [],
    breadcrumbs: [],
    view_mode: 'list',
    getPaginator: () => {},
    showShops: () => {},
    showMore: false,
    showButton: true,
};