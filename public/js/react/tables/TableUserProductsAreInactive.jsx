import React from 'react';
import PropTypes from 'prop-types';
import UserProduct from '../components/UserProduct';
import { getProductStatus, getProductStatusCode } from '../Helpers';

function TableUserProductsAreInactive({ products, onTableRowClick }) {
    return(
        <section className="content">
            <section className="content-main">
                <div className="container">
                    <div className="catalog is-block-catalog">
                        <div className="row">
                            {
                                products.map((product) => {
                                    return <UserProduct
                                        key={product.id}
                                        urls={product.urls}
                                        category={product.category}
                                        subCategory={product.sub_category}
                                        gallery={product.gallery}
                                        name={product.name}
                                        created={product.created}
                                        created_timestamp={product.created_timestamp}
                                        price={product.price}
                                        description={product.description}
                                        onClick={onTableRowClick(product)}
                                        is_show_status={true}
                                        status={getProductStatusCode(product)}
                                        status_show_format={getProductStatus(product)}
                                        fabricator={product.fabricator}
                                    />;
                                })
                            }
                        </div>
                    </div>
                </div>
            </section>
        </section>
    );
}

TableUserProductsAreInactive.propTypes = {
    products: PropTypes.array
};

TableUserProductsAreInactive.defaultProps  = {
    products: [],
};

export default TableUserProductsAreInactive;