import React from 'react';
import PropTypes from 'prop-types';

const columns = [
    {
        name: 'id',
        title: 'ID'
    },
    {
        name: 'moderation_time',
        title: 'На модерации',
    },
    {
        name: 'category',
        title: 'Категория',
    },
    {
        name: 'sub_category',
        title: 'Подкатегория',
    },
    {
        name: 'name',
        title: 'Название',
    },
    {
        name: 'author',
        title: 'Автор',
    },
];

function TableModeration({ onOrderLinkClick, products, onModerationItemClick }) {
    const calculateDateDiff = (date) => {
        const currentDate = new Date();
        const hoursDiff = (+currentDate / 1000 - +date) / 3600;
        let days = '';
        let hours = '';
        let minutes = '';
        if (hoursDiff >= 24) {
            days = `${Math.floor(hoursDiff / 24)}д `;
        }
        if (hoursDiff >= 1) {
            hours = `${Math.floor(hoursDiff % 24)}ч`;
        } else {
            minutes = `${Math.floor(hoursDiff * 60)}мин`;
        }

        return days + hours + minutes;
    };

    return(
        <div className="table">
            <table>
                <thead className="th">
                    <tr className="th">
                        {columns.map((column) => {
                            return (
                                <th className="td" key={column.name}>
                                    <a className="link-sort" href="#" onClick={onOrderLinkClick(column.name)}>{column.title}</a>
                                </th>
                            );
                        })}
                    </tr>
                </thead>
                <tbody className="tbody">
                    { products.map((product) => {
                        // console.log(product);
                        return (
                            <tr className="tr" key={product.id} onClick={onModerationItemClick(product)}>
                                <td className="td">
                                    <div className="td__title-mobile">ID</div>
                                    <div className="td__content">{product.id}</div>
                                </td>
                                <td className="td">
                                    <div className="td__title-mobile">На модерации</div>
                                    <div className="td__content">{calculateDateDiff(new Date(product.created).getTime())}</div>
                                </td>
                                <td className="td">
                                    <div className="td__title-mobile">Категория</div>
                                    <div className="td__content">{product.category.name}</div>
                                </td>
                                <td className="td">
                                    <div className="td__title-mobile">Подкатегория</div>
                                    <div className="td__content">{product.sub_category.name}</div>
                                </td>
                                <td className="td">
                                    <div className="td__title-mobile">Название</div>
                                    <div className="td__content">{product.name}</div>
                                </td>
                                <td className="td">
                                    <div className="td__title-mobile">Автор</div>
                                    <div className="td__content"><a href="#" className="link-inline">{product.author}</a></div>
                                </td>
                            </tr>
                        );
                    }) }
                </tbody>
            </table>
        </div>
    );
}

TableModeration.propTypes = {
    products: PropTypes.array,
    onOrderLinkClick: PropTypes.func,
    onModerationItemClick: PropTypes.func
};

TableModeration.defaultProps = {
    products: [],
    onOrderLinkClick: () => {},
    onModerationItemClick: () => {}
};

export default TableModeration;
