import React, { Component } from 'react';
import PropTypes from 'prop-types';
import LoaderSpinner from '../components/LoaderSpinner';
import Purchase from '../components/Purchase';

export default class TablePurchases extends Component {

    componentDidMount = () => {
        this.props.getPaginator();
    };

    showMoreProducts = (e) => {
        e.preventDefault();
        this.props.showPurchases();
    };

    render() {
        const { showMore, showButton, purchases, breadcrumbs, isLoading } = this.props;

        return(
            <section className="content">
                <section className="content-header">
                    <div className="container">
                        <div className="row wrap-flex">
                            <div className="col-8">
                                <div className="content-header-navigation">
                                    <h2 className="content-header-navigation__title">Все закупки</h2>
                                    <ul className="navigation-chain">
                                        {
                                            breadcrumbs.map((item, i, breadcrumbs) => {
                                                return (
                                                    <li key={item.name}>
                                                        <a href={item.url}>{item.name}</a> {breadcrumbs.length - 1 === i ? '' : '/'}&nbsp;
                                                    </li>
                                                );
                                            })
                                        }
                                    </ul>
                                </div>
                            </div>
                            <div className="col-3" />
                        </div>
                    </div>
                </section>
                <section className="content-main content-main_mobile_pt_0">
                    <div className="container">
                        <div className="catalog is-list-catalog">
                            <div className="row">
                                {
                                    purchases.length > 0
                                        ? purchases.map((purchase, index) => {
                                            return <Purchase
                                                key={index}
                                                urls={purchase.urls}
                                                category={purchase.category}
                                                subCategory={purchase.sub_category}
                                                name={purchase.name}
                                                created={purchase.created}
                                                created_timestamp={purchase.created_timestamp}
                                                price={purchase.price}
                                                quantity={purchase.quantity}
                                                owner={purchase.owner}
                                                measure={purchase.measure}
                                                description={purchase.description}
                                            />;
                                        })
                                        :
                                        <div className="page-message-container">
                                            <img src="/img/union.svg" alt="" className="page-message-image" />
                                            <h2 className="page-message">Закупок не найдено</h2>
                                        </div>
                                }
                            </div>
                            {
                                showMore
                                    ?
                                    <div className="row button-row">
                                        <LoaderSpinner isTiny={true} />
                                    </div>
                                    : showButton
                                        ?
                                        <div className="row button-row">
                                            <div className="col-12">
                                                <a href="#" className="button-transparent" onClick={this.showMoreProducts}>Показать больше</a>
                                            </div>
                                        </div>
                                        : null
                            }
                        </div>
                    </div>
                    {
                        isLoading &&
                        <div className="row button-row">
                            <LoaderSpinner isTiny={true} />
                        </div>
                    }
                </section>
            </section>
        );
    }
}

TablePurchases.propTypes = {
    purchases: PropTypes.array,
    breadcrumbs: PropTypes.array,
    view_mode: PropTypes.string,
    getPaginator: PropTypes.func,
    showPurchases: PropTypes.func,
    showMore: PropTypes.bool,
    showButton: PropTypes.bool,
};

TablePurchases.defaultProps  = {
    purchases: [],
    breadcrumbs: [],
    view_mode: 'list',
    getPaginator: () => {},
    showPurchases: () => {},
    showMore: false,
    showButton: true,
};