import React from 'react';
import PropTypes from 'prop-types';
import UserProduct from '../components/UserProduct';

function TableUserProductsAreActive({ products, onTableRowClick }) {
    return(
        <section className="content">
            <section className="content-main">
                <div className="container">
                    <div className="catalog is-block-catalog">
                        <div className="row">
                            {
                                products.map((product) => {
                                    return <UserProduct
                                        key={product.id}
                                        urls={product.urls}
                                        category={product.category}
                                        subCategory={product.sub_category}
                                        gallery={product.gallery}
                                        name={product.name}
                                        created={product.created}
                                        created_timestamp={product.created_timestamp}
                                        price={product.price}
                                        description={product.description}
                                        fabricator={product.fabricator}
                                        onClick={onTableRowClick(product)}
                                    />;
                                })
                            }
                        </div>
                    </div>
                </div>
            </section>
        </section>
    );
}

TableUserProductsAreActive.propTypes = {
    products: PropTypes.array
};

TableUserProductsAreActive.defaultProps  = {
    products: [],
};

export default TableUserProductsAreActive;