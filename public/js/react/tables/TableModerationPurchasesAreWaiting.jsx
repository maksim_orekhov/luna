import React from 'react';
import PropTypes from 'prop-types';
import {
    getCategoryNameForPurchase,
    getSubCategoryNameForPurchase,
    checkThatNestedKeyOfObjectExist,
    getAuthorNameForPurchase
} from '../Helpers';


const columns = [
    {
        name: 'id',
        title: 'ID'
    },
    {
        name: 'moderation_time',
        title: 'На модерации',
    },
    {
        name: 'category',
        title: 'Категория',
    },
    {
        name: 'sub_category',
        title: 'Подкатегория',
    },
    {
        name: 'name',
        title: 'Название',
    },
    {
        name: 'author',
        title: 'Автор',
    },
];

function TableModerationPurchasesAreWaiting({ purchases, onTableRowClick }) {
    return (
        <div className="table">
            <table>
                <thead className="th">
                    <tr className="th">
                        {columns.map((column) => {
                            return (
                                <th className="td" key={column.name}>
                                    <a className="link-sort" href="#" onClick={() => {}}>{column.title}</a>
                                </th>
                            );
                        })}
                    </tr>
                </thead>
                <tbody className="tbody">
                    { purchases.length !== 0 && purchases.map((purchase) => {
                        return (
                            <tr className="tr" key={purchase.id} onClick={onTableRowClick(purchase)}>
                                <td className="td td_size-5">
                                    <div className="td__title-mobile">ID</div>
                                    <div className="td__content">{purchase.id}</div>
                                </td>
                                <td className="td td_size-15">
                                    <div className="td__title-mobile">На модерации</div>
                                    <div className="td__content">
                                        {
                                            checkThatNestedKeyOfObjectExist(purchase, 'moderation', 'moderation_time')
                                            && purchase.moderation.moderation_time
                                                ? purchase.moderation.moderation_time
                                                : 'неизвестно'
                                        }
                                    </div>
                                </td>
                                <td className="td td_size-15">
                                    <div className="td__title-mobile">Категория</div>
                                    <div className="td__content">{getCategoryNameForPurchase(purchase)}</div>
                                </td>
                                <td className="td td_size-20">
                                    <div className="td__title-mobile">Подкатегория</div>
                                    <div className="td__content">{getSubCategoryNameForPurchase(purchase)}</div>
                                </td>
                                <td className="td">
                                    <div className="td__title-mobile">Название</div>
                                    <div className="td__content">{purchase.name}</div>
                                </td>
                                <td className="td">
                                    <div className="td__title-mobile">Автор</div>
                                    <div className="td__content">
                                        <a href="#" className="link-inline">
                                            {getAuthorNameForPurchase(purchase)}
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        );
                    }) }
                    {
                        purchases.length === 0 &&
                        <tr>
                            <td colSpan="6" style={{textAlign: 'center'}}>Закупок, требующих модерации не найдено</td>
                        </tr>
                    }
                </tbody>
            </table>
        </div>
    );
}

TableModerationPurchasesAreWaiting.propTypes = {
    purchases: PropTypes.array,
    onOrderLinkClick: PropTypes.func,
    onTableRowClick: PropTypes.func
};

TableModerationPurchasesAreWaiting.defaultProps = {
    purchases: [],
    onOrderLinkClick: () => {},
    onTableRowClick: () => {}
};

export default TableModerationPurchasesAreWaiting;
