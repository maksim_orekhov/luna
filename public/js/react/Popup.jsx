import React from 'react';
import PropTypes from 'prop-types';

export default class Popup extends React.Component {
    // Для ситуации, когда открыто несколько попапов считаем количество открытых попапов, чтобы закрылся только верхний
    static openedPopupCount = 0;
    popupNumber = 0;

    componentDidMount() {
        this.constructor.openedPopupCount += 1;
        this.popupNumber = this.constructor.openedPopupCount;
        document.querySelector('body').classList.add('overflow-hidden__popup');
        document.addEventListener('keydown', this.handleKeyDown, false);
    }

    componentWillUnmount() {
        this.constructor.openedPopupCount -= 1;
        if (this.constructor.openedPopupCount === 0) {
            document.querySelector('body').classList.remove('overflow-hidden__popup');
        }
        document.removeEventListener('keydown', this.handlePopupContentClick, false);
    }

    handleKeyDown = (ev) => {
        const { close } = this.props;
        if (ev.keyCode !== 27) {
            return true;
        }
        if (this.popupNumber === this.constructor.openedPopupCount) {
            close();
        }
    };

    handlePopupContentClick = (e) => {
        e.stopPropagation();
    };

    render() {
        const { children, close, popupClassName, popupContentClassName, is_preview_loaded, onMouseEnterPopup, onMouseLeavePopup } = this.props;
        return (
            <div className={`popup-container ${popupClassName} open`} onClick={close}>
                <div className="popup-inner">
                    <div
                        className={`popup ${popupContentClassName}`}
                        onClick={this.handlePopupContentClick}
                        onMouseEnter={onMouseEnterPopup}
                        onMouseLeave={onMouseLeavePopup}
                    >
                        {is_preview_loaded && <button type="button" onClick={close} className="close-popup-btn" />}
                        { children }
                    </div>
                </div>
            </div>
        );
    }
}

Popup.propTypes = {
    close: PropTypes.func.isRequired,
    popupClassName: PropTypes.string,
    popupContentClassName: PropTypes.string,
    is_preview_loaded: PropTypes.bool,
    children: PropTypes.any,
    onMouseLeavePopup: PropTypes.func,
    onMouseEnterPopup: PropTypes.func,
};

Popup.defaultProps = {
    popupClassName: '',
    popupContentClassName: '',
    children: '',
    is_preview_loaded: true,
    onMouseLeavePopup: () => {},
    onMouseEnterPopup: () => {}
};
