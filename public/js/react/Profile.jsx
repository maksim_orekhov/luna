import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import UserStatistic from './UserStatistic';
import UserPersonalData from './UserPersonalData';
import UserSettings from './UserSettings';
import withProvider from './hocs/withProvider';
import { loadUserInformation, closeUserProfileForm } from './actions';
import LoaderSpinner from './components/LoaderSpinner';
import Popup from './Popup';
import InfoMessage from './components/InfoMessage';
import FormNameAdd from './forms/FormNameAdd';
import EmailChangeController from './controllers/EmailChangeController';
import PhoneChangeController from './controllers/PhoneChangeController';
import { getMessageTextByUserActionName } from './Helpers';

class Profile extends React.Component {

    state = {
        messageIsVisible: false,
    };

    messageText = {};

    componentDidMount() {
        const { loadUserInformation, user } = this.props;
        if (!user.isLoaded && !user.isLoading) {
            loadUserInformation();
        }

        if (sessionStorage.getItem('profileMessage')) {
            this.messageText = getMessageTextByUserActionName(sessionStorage.getItem('profileMessage'));
            sessionStorage.removeItem('profileMessage');
            this.setState({ messageIsVisible: true });
        }
    }

    handleCloseForm = (formName = null) => () => {
        const { closeForm } = this.props;
        closeForm(formName);
    };

    handleCloseMessage = () => {
        this.setState({ messageIsVisible: false });
    };

    render() {
        const {
            user: {
                isLoading,
                isLoaded,
                isOpenForm,
                data: {
                    name, login, avatar, nextPhone, phone, is_phone_verified, email, is_email_verified,
                    created_format, count_published_products
                }
            }
        } = this.props;
        const { messageIsVisible } = this.state;
        if (isLoading) {
            return <LoaderSpinner  />;
        }

        if (isLoaded) {
            return (
                <div className="profile-page-user-info">
                    <div className="row profile-page-user-info">
                        <UserStatistic
                            name={name}
                            login={login}
                            phone={nextPhone ? nextPhone : phone}
                            email={email}
                            is_email_verified={is_email_verified}
                            is_phone_verified={is_phone_verified}
                            created={created_format}
                            count_published_products={count_published_products}
                            avatar={avatar}
                        />
                        <UserPersonalData
                            email={email}
                            name={name}
                            phone={nextPhone ? nextPhone : phone}
                            is_email_verified={is_email_verified}
                            is_phone_verified={is_phone_verified}
                        />
                        <UserSettings
                            email={email}
                            name={name}
                            login={login}
                            phone={nextPhone ? nextPhone : phone}
                            is_email_verified={is_email_verified}
                            is_phone_verified={is_phone_verified}
                        />
                    </div>
                    {
                        isOpenForm.nameAdd &&
                        <Popup close={this.handleCloseForm('nameAdd')} popupClassName="js-popup-container-name-add">
                            <FormNameAdd
                                handleClosePopup={this.handleCloseForm('nameAdd')}
                            />
                        </Popup>
                    }
                    {
                        (isOpenForm.phoneChange || isOpenForm.phoneConfirmToken || isOpenForm.phoneConfirmCode || isOpenForm.phoneChangeSuccess) &&
                        <Popup close={this.handleCloseForm()} popupClassName="js-popup-container-name-add">
                            <PhoneChangeController
                                handleClosePopup={this.handleCloseForm()}
                                phone_number={nextPhone ? nextPhone : phone}
                            />
                        </Popup>
                    }
                    {
                        (isOpenForm.emailChange || isOpenForm.emailChangeAccept || isOpenForm.emailChangeSuccess) &&
                        <Popup close={this.handleCloseForm()} popupClassName="js-popup-container-email-change">
                            <EmailChangeController
                                handleClosePopup={this.handleCloseForm()}
                            />
                        </Popup>
                    }
                    {
                        messageIsVisible &&
                            <InfoMessage
                                handleClose={this.handleCloseMessage}
                                title={this.messageText.title}
                                description={this.messageText.description}
                                is_auto_close={true}
                            />
                    }
                </div>
            );
        }

        return <div>Ошибка загрузки данных</div>;
    }
}

Profile.propTypes = {
    // redux state
    user: PropTypes.object.isRequired,
    // redux dispatch
    loadUserInformation: PropTypes.func.isRequired,
    closeForm: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
    return {
        user: {...state.user},
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        loadUserInformation: bindActionCreators(loadUserInformation, dispatch),
        closeForm: bindActionCreators(closeUserProfileForm, dispatch),
    };
};

export default withProvider(connect(mapStateToProps, mapDispatchToProps)(Profile));
