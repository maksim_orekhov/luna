import 'air-datepicker';
import 'slick-carousel';

$('.js-burger').on('click', function () {
    $('.js-mobile-menu').toggleClass('active');
    $('.js-mobile-menu-bg').toggleClass('active');
    $('body').toggleClass('fixed-noscroll');
});
$('body').on('click', '.js-mobile-menu-close', function () {
    $('.js-mobile-menu').removeClass('active');
    $('.js-mobile-menu-bg').removeClass('active');
    $('body').toggleClass('fixed-noscroll');
});

$('.js-mobile-categories-menu').on('click', function () {
    $('.js-mobile-categories-menu').toggleClass('active');
    $('body').toggleClass('fixed-noscroll');
});

/* $(document).mouseup(function (e){ // событие клика по веб-документуpopup
    var div = $('.popup');
    console.log(div.closest('.open.popup-container').length);
    var body =  $('body');
    if (div && div.length
        && !div.is(e.target) // если клик был не по нашему блоку
        && div.has(e.target).length === 0
    ) { // и не по его дочерним элементам
        console.log('inside');
        div.removeClass('active');// скрываем его
        body.removeClass('overflow-hidden__modal');
    }
});*/

$('.js-button-auth').on('click', function () {
    $('.js-mobile-menu').removeClass('active');
    $('.js-mobile-menu-bg').removeClass('active');
});

// Выбор категории на мобильном экране
$('.js-categorie-choose-button').on('click', function () {
    $('.js-categories-popup-container').toggleClass('open');
});

$('.js-categories-container-button-close').on('click', function () {
    $('.js-categories-popup-container').removeClass('open');
});

// Показ окна с авторизацией
$('.js-button-auth').on('click', function (ev) {
    ev.preventDefault();
    $('.js-popup-container-auth').toggleClass('open');
});

// Показ окна с жалобой на объявление
$('.js-advertise-compliance').on('click', function (ev) {
    ev.preventDefault();
    $('.js-popup-container-complaint').addClass('open');
    $('body').addClass('overflow-hidden__modal');
});

// Закрытие формы "Пожаловаться на объявление
$('.js-close-inner-popup').on('click', '.js-close-complaint-form', function (ev) {
    ev.preventDefault();
    $('.js-popup-container-complaint').removeClass('open');
    $('body').removeClass('overflow-hidden__modal');
});
$('.js-close-popup-btn').on('click', function (ev) {
    ev.preventDefault();
    $('.js-popup-container-complaint, .js-popup-container-info-message-secury-deal').removeClass('open');
    $('body').removeClass('overflow-hidden__modal');
});

// Показ информации о безопасной сделке
$('.js-popup-info-message-secury-deal').on('click', function (ev) {
    ev.preventDefault();
    $('.js-popup-container-info-message-secury-deal').addClass('open');
    $('body').addClass('overflow-hidden__modal');
});


// закрытие popup по нажатию клавиши esc
$(window).on('keyup', function(event) {
    if (event.keyCode === 27) {
        $('.popup-container').removeClass('open');
    }
}); 

$(document).mousedown(function (e){
    var popup_content = $('.popup');
    var popup_container = [];
    popup_container = $('.js-popup-container-auth, .js-popup-container-complaint, .js-popup-container-info-message-secury-deal');
    if (!popup_content.is(e.target)
        && popup_content.has(e.target).length === 0) {
        popup_container.removeClass('open');
    }
});

// Показ фильтра на мобильном экране
$('.js-mobile-button-filter').on('click', function () {
    $('.js-mobile-main-filter').addClass('open');
});
$('.js-mobile-button-close-filter').on('click', function () {
    $('.js-mobile-main-filter').removeClass('open');
});

// Отображение иконки сортировки

$('.link-sort').on('click', function () {
    this.classList.toggle('link-sort__up');
});

// На мобильных экранах по умолчанию отображение каталога в виде списка

// if(window.innerWidth < 768) {
//     let catalog = $('.catalog');
//     $(catalog).addClass('is-list-catalog');
//     console.log('dffff');
// }

/**
 * Меняет тип инпута пароля password/text
 */
$('.toggle-password-input-check').on('change', function () {
    const $this = $(this),
        $passwordInput = $this.closest('.control-form').find('.password-input');
    $passwordInput.attr('type', $this.is(':checked') ? 'text' : 'password');
});

//
$('.js-show-hidden').on('click', function () {
    $('.js-show-hidden').toggleClass('show');
});

$('.with-datepicker').datepicker();
/**
 * Вписывает в поле значение range input
 */
$('.range-with-value-field').on('input change', function (){
    const $this = $(this);
    $this.siblings('.range-label').find('.range__value').text($this.val());
});
/**
 * Раскрывает/закрывает блок
 */
$('[data-slide-for]').on('click', function (e) {
    e.preventDefault();
    const $this = $(this);
    const $target = $(`[data-slide='${$this.data('slide-for')}']`);
    if(!$target){
        return null;
    }
    $this.toggleClass('is-toggled');
    $target.stop(true).slideToggle(400);
});

// При клике на строку таблицы имитируем поведение ссылки переходя на нужный адрес из атрибута data-path
(() => {
    const tables = $('.js-table-rows-are-link');

    if (tables.length > 0) {
        tables.each(function () {
            const table = $(this);

            table.find('.tr').each(function () {

                const row = $(this);

                const path = row.data('path');
                const start = table.data('start');
                const length = table.data('length');

                if (path) {
                    if (start !== undefined && start > 0 && length !== undefined && length > 0) {
                        row.find('.td').each(function (index) {
                            if (index + 1 >= start && index + 1 < length + start) {
                                $(this).addClass('active');
                                $(this).on('click', function () {
                                    window.location = path;
                                });
                            }
                        });
                    } else {
                        row.addClass('active');
                        row.on('click', function () {
                            window.location = path;
                        });
                    }
                }
            });
        });
    }
})();

const ProductCard = {
    $productPhotoSlider: null,
    $gallery: null,
    $galleryImages: null,
    galleryImageActiveIndex: 0,

    init: function () {
        this.initProductSlider();
        this.initGallery();
    },

    initProductSlider: function () {
        this.$productPhotoSlider = $('.product-photo-slider__items');
        if(!this.$productPhotoSlider){
            return null;
        }
        this.$productPhotoSlider.slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 3000,
            cssEase: 'linear',
            fade: true
        });

        $('.product-photo-slider__preview') .on('click', e => {
            e.preventDefault();
            this.$productPhotoSlider.slick('slickGoTo', $(e.currentTarget).index());
        });
    },

    initGallery: function initGallery() {
        this.$gallery = $('.product-gallery');
        if (!this.$gallery.length) {
            return null;
        }
        this.$galleryImages = this.$gallery.find('.gallery-item__image');

        this.$productPhotoSlider.add(this.$gallery).on('click', '[data-gallery-index]', (ev) => {
            ev.preventDefault();
            ev.stopPropagation();
            this.setActiveGalleryImage(ev.currentTarget.dataset.galleryIndex);
            this.openGallery();
        });

        $('.gallery-control-close').on('click', (ev) => {
            ev.preventDefault();
            this.closeGallery();
        });

        this.$gallery.find('.gallery-item').on('click', (ev) => {
            this.setActiveGalleryImage($(ev.currentTarget).index() + 1);
        });

        $(document).on('click', (ev) => {
            if (
                this.$gallery.hasClass('active')
                && !this.$gallery.find('.gallery-items, .gallery-control').has(ev.target).length
            ) {
                this.$gallery.removeClass('active');
            }
        }).on('keydown', (ev) => {
            if (!this.$gallery.hasClass('active')) {
                return true;
            }
            switch (ev.keyCode) {
                case 27:
                    this.closeGallery();
                    break;
                case 37:
                    this.setActiveGalleryImage(this.galleryImageActiveIndex - 1);
                    break;
                case 39:
                    this.setActiveGalleryImage(this.galleryImageActiveIndex + 1);
                    break;
            }
        });

    },

    openGallery: function openGallery() {
        if (!this.$gallery.hasClass('active')) {
            this.$gallery.addClass('active');
        }
    },

    closeGallery: function openGallery() {
        this.$gallery.removeClass('active');
    },

    setActiveGalleryImage: function setActiveGalleryImage(index) {
        if (this.galleryImageActiveIndex === index) {
            return false;
        }
        const imagesCount = this.$galleryImages.length;
        const nextImageIndex = index > imagesCount - 1 ? 0 : index < 0 ? imagesCount - 1 : index;

        this.$gallery.find('.gallery-item').eq(nextImageIndex).addClass('active')
            .siblings('.gallery-item').removeClass('active');
        this.galleryImageActiveIndex = nextImageIndex;
        this.setActivePreviewImage();
    },

    setActivePreviewImage: function setActivePreviewImage() {
        this.$gallery.find('.gallery-preview-item').eq(this.galleryImageActiveIndex).addClass('active')
            .siblings('.gallery-preview-item').removeClass('active');
    }

};
ProductCard.init();

const SiteMapSlider = {
    $siteMapSlider: null,

    init: function (selector) {
        this.initSiteMapSlider(selector);
    },

    initSiteMapSlider: function (selector) {
        this.$siteMapSlider = $(selector);
        if(!this.$siteMapSlider){
            return null;
        }
        this.$siteMapSlider.slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            arrows: false,
            responsive: [{
                breakpoint: 1196,
                settings: {
                    slidesToShow: 4,
                }
            }]
        });

        $('.horizontal-control-right') .on('click', e => {
            e.preventDefault();
            this.$siteMapSlider.slick('slickNext');
        });

        $('.horizontal-control-left') .on('click', e => {
            e.preventDefault();
            this.$siteMapSlider.slick('slickPrev');
        });
    }
};

const CategoriesSlider = {
    // Для слайдера
    slider_root_selector: null,
    $slider: null,
    // Для категорий
    $container: null,
    default_height_container: null,
    height_container: null,
    category_selector: null,
    are_categories_opened: false,
    $categories: null,
    $all_categories: null, // Все включая скрытые
    show_more_categories_selector: null,
    $show_all_button: null,
    padding: 38, // @TODO: MAGIC NUMBER
    animation_time: 600,

    init: function (options = {}) {
        this.initVariables(options);

        $(this.slider_root_selector).on('afterChange', (event, slick, currentSlide) => {
            if (!slick) return;

            if (this.are_categories_opened) this.deleteAllShowMoreButtons();

            this.updateSlides(slick);
        });

        $(this.slider_root_selector).on('init', (event, slick) => {
            if ($(window).width() < 1025) return;

            if (!slick) return;
            this.addShowAllCategoriesButton();

            this.updateSlides(slick);
        });

        // !!! ОБЯЗАТЕЛЬНО ДОЛЖНО ИДТИ ПОСЛЕ ПОДПИСКИ НА СОБЫТИЯ
        SiteMapSlider.init(this.slider_root_selector);
    },

    initVariables: function (options = {}) {
        const {
            slider_root_selector,
            container,
            default_height_container,
            show_more_categories_selector,
            category_selector
        } = options;

        this.slider_root_selector = slider_root_selector || '.categories-slider-wrapper';
        this.$container = $(container || '.categories-menu');
        this.default_height_container = default_height_container || this.$container.height();
        this.height_container = this.default_height_container;
        this.category_selector = category_selector || '.categories-item';
        this.show_more_categories_selector = show_more_categories_selector || '.show-more-link';
    },

    showAllCategoriesButtonOnClick: function ($show_all_button = this.$show_all_button) {
        const self = this;
        $show_all_button.on('click', function () {
            self.are_categories_opened = !self.are_categories_opened;
            self.rerender();
        });
    },

    rerender: function () {
        this.deleteAllShowMoreButtons();
        this.chooseBehaviour();
        this.changeShowAllCategoriesButtonText();
    },

    updateSlides: function (slick = this.$slider) {
        this.$slider = slick;
        const $slides = slick.$slider.find('.slick-slide');
        this.updateCategoriesByVisibility($slides);
    },

    updateCategoriesByVisibility: function ($slides) {
        const $visible_slides = $slides.filter('.slick-active');
        this.$all_categories = $slides.find(this.category_selector);
        this.$categories = $visible_slides.find(this.category_selector);
        this.chooseBehaviour();
    },

    chooseBehaviour: function () {
        if (this.are_categories_opened) {
            this.adjustContainerSize();
        } else {
            if (this.height_container !== this.default_height_container) {
                this.triggerContainerResize(this.default_height_container);
            } else {
                this.checkAllCategoriesOnHeight();
            }
        }
    },

    adjustContainerSize: function () {
        this.findTheHighestCategory();
    },

    triggerContainerResize: function (new_height, dont_check) {
        this.$container.animate({
            'height': new_height
        }, this.animation_time, () => {
            this.updateContainerHeight();
            if (!dont_check) this.checkAllCategoriesOnHeight();
        });
    },

    createShowButton: function () {
        const newButton = document.createElement('div');
        newButton.className = 'subcategory-item show-more-link';
        newButton.innerHTML = '...';

        // @TODO: MAGIC NUMBER 13!
        newButton.style.top = this.height_container - this.padding - 14 + 'px';

        return newButton;
    },

    createShowAllCategoriesButton: function () {
        const newButton = document.createElement('div');
        newButton.className = 'show-all-button button-transparent';
        newButton.innerHTML = this.are_categories_opened ? 'Скрыть' : 'Показать все';

        return newButton;
    },

    addShowAllCategoriesButton: function ($container = this.$container) {
        const $show_all_button = this.createShowAllCategoriesButton();
        this.$show_all_button = $($show_all_button);
        this.showAllCategoriesButtonOnClick(this.$show_all_button);
        $container.closest('section.top').append($show_all_button);
    },

    changeShowAllCategoriesButtonText() {
        if (this.are_categories_opened) {
            this.$show_all_button.text('Скрыть');
        } else {
            this.$show_all_button.text('Показать все');
        }
    },

    addShowMoreButtonToCategory: function ($category) {
        $category.append(this.createShowButton());
    },

    deleteShowMoreButtonFromCategory: function ($category) {
        $category.find(this.show_more_categories_selector).remove();
    },

    checkAllCategoriesOnHeight: function($categories = this.$all_categories) {
        if (!$categories || !$categories.length) return;
        const self = this;

        $categories.each(function() {
            const $category = $(this);

            const has_already_category_button = self.hasCategoryButton($category);

            if (self.isCategoryMoreThanContainer($category)) {
                if (!has_already_category_button) self.addShowMoreButtonToCategory($category);
            } else {
                if (has_already_category_button) self.deleteShowMoreButtonFromCategory($category);
            }
        });
    },

    findTheHighestCategory: function ($categories = this.$categories) {
        if (!$categories || !$categories.length) return;

        let the_highest_value =  $categories.first().outerHeight();

        $categories.each(function() {
            const $category = $(this);

            const category_height = $category.outerHeight();
            if (category_height > the_highest_value) the_highest_value = category_height;
        });

        this.triggerContainerResize(the_highest_value + this.padding, true);
    },

    isCategoryMoreThanContainer: function($category) {
        return $category.outerHeight() > this.height_container - 25;
    },

    hasCategoryButton: function ($category) {
        const show_more_button = $category.has(this.show_more_categories_selector);

        return !!show_more_button.length;
    },

    deleteAllShowMoreButtons: function($categories = this.$all_categories) {
        if (!$categories || !$categories.length) return;

        const self = this;

        $categories.each(function() {
            const $category = $(this);

            const has_category_button = self.hasCategoryButton($category);

            if (has_category_button) self.deleteShowMoreButtonFromCategory($category);
        });
    },

    updateContainerHeight: function () {
        this.height_container = this.$container.height();
    },
};


const CategoriesSliderToggle = {
    init: function () {
        if (!$('.js-slider-toggle').length) return;

        $('.js-slider-toggle-link').on('mouseenter', function () {
            const wrapper = $(this).closest('.js-slider-toggle-row');
            wrapper.find('.js-slider-toggle-sub_items').each(function () {
                $(this).stop(true);
                $(this).slideDown(800);
            });
        });

        $('.js-slider-toggle-row').on('mouseleave', function () {
            $(this).find('.js-slider-toggle-sub_items').each(function () {
                $(this).stop(true);
                $(this).slideUp(800);
            });
        });
    }
};


$(document).ready(() => {
    /**
     * @TODO: Отрефакторить логику, сделать поведение на resize для CategoriesSlider
     */
    CategoriesSlider.init();

    // НОВЫЙ СЛАЙДЕР
    CategoriesSliderToggle.init();

    // Добавляет кликабельность объявлениям на лэндинге, только там где не используется реакт для их вывода, для реакта логика в описана в компонентах
    $('.js-product-catalog-item a,.js-product-catalog-item .js-product-catalog-item_stop-propagation').on('click', function (e) {
        e.stopPropagation();
    });

    $('.js-product-catalog-item').on('click', function () {
        const new_location = $(this).data('path');
        if (new_location) {
            window.location = new_location;
        }
    });
    // ----------------------------------
});



