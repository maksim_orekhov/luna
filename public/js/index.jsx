import React from 'react';
import ReactDom from 'react-dom';

import RegisterLinkController from './react/controllers/RegisterLinkController';
import FormAdvertisement from './react/containers/FormAdvertisementContainer';
import FormCreateShop from './react/forms/FormCreateShop';
import FormSecureDeal from './react/forms/FormSecureDeal';
import InfoConnectionSecuryDeal from './react/components/InfoConnectionSecuryDeal';
import FormComplaint from './react/forms/FormComplaint';
import TableProductsContainer from './react/containers/TableProductsContainer';
import TablePurchasesContainer from './react/containers/TablePurchasesContainer';
import TableShopsContainer from './react/containers/TableShopsContainer';
import TableUsersProductsContainer from './react/containers/TableUsersProductsContainer';
import TableUsersPurchasesContainer from './react/containers/TableUsersPurchasesContainer';
import ProfileTabs from './react/components/ProfileTabs';
import Profile from './react/Profile';
import FormSearchContainer from './react/containers/FormSearchContainer';
import OperatorProductsTabsContainer from './react/containers/OperatorProductsTabsContainer';
import OperatorPurchasesTabsContainer from './react/containers/OperatorPurchasesTabsContainer';
import UserProductsTabsContainer from './react/containers/UserProfileProductsTabsContainer';
import UserPurchasesTabsContainer from './react/containers/UserProfilePurchasesTabsContainer';
import FormPurchase from './react/containers/FormPurchaseContainer';
import FormRegister from './react/forms/FormRegister';
import MobileMenu from './react/containers/MobileMenuContainer';
import LoaderSpinner from './react/components/LoaderSpinner';



if (document.getElementById('auth-form')) {
    const rootEl = document.getElementById('auth-form');
    ReactDom.render(<RegisterLinkController />, rootEl);
}

if (document.getElementById('FormAdvertisement')) {
    const rootEl = document.getElementById('FormAdvertisement');
    ReactDom.render(<FormAdvertisement />, rootEl);
}
if (document.getElementById('form-product-edit')) {
    const rootEl = document.getElementById('form-product-edit');
    ReactDom.render(<FormAdvertisement formType="edit" />, rootEl);
}

if (document.getElementById('create-shop-form')) {
    const rootEl = document.getElementById('create-shop-form');
    ReactDom.render(<FormCreateShop />, rootEl);
}

if (document.getElementById('secure-deal-form')) {
    const rootEl = document.getElementById('secure-deal-form');
    ReactDom.render(<FormSecureDeal />, rootEl);
}

if (document.getElementById('complaint-form')) {
    const rootEl = document.getElementById('complaint-form');
    ReactDom.render(<FormComplaint />, rootEl);
}

if (document.getElementById('info-message-secury-deal')) {
    const rootEl = document.getElementById('info-message-secury-deal');
    ReactDom.render(<InfoConnectionSecuryDeal />, rootEl);
}

if (document.getElementById('info-message-secury-deal-landing')) {
    const rootEl = document.getElementById('info-message-secury-deal-landing');
    ReactDom.render(<InfoConnectionSecuryDeal />, rootEl);
}

if (document.getElementById('profile-page-user-info')) {
    const rootEl = document.getElementById('profile-page-user-info');
    ReactDom.render(<Profile />, rootEl);
}
if (document.getElementById('TableProducts')) {
    const rootEl = document.getElementById('TableProducts');
    ReactDom.render(<TableProductsContainer />, rootEl);
}

if (document.getElementById('TablePurchase')) {
    const rootEl = document.getElementById('TablePurchase');
    ReactDom.render(<TablePurchasesContainer />, rootEl);
}

if (document.getElementById('TableShops')) {
    const rootEl = document.getElementById('TableShops');
    ReactDom.render(<TableShopsContainer />, rootEl);
}

if (document.getElementById('main-search-block')) {
    const rootEl = document.getElementById('main-search-block');
    ReactDom.render(<FormSearchContainer />, rootEl);
}

if (document.getElementById('ProfileTabs')) {
    const rootEl = document.getElementById('ProfileTabs');
    ReactDom.render(<ProfileTabs />, rootEl);
}

if (document.getElementById('OperatorProfileTabsProducts')) {
    const rootEl = document.getElementById('OperatorProfileTabsProducts');
    ReactDom.render(<OperatorProductsTabsContainer />, rootEl);
}

if (document.getElementById('OperatorProfileTabsPurchases')) {
    const rootEl = document.getElementById('OperatorProfileTabsPurchases');
    ReactDom.render(<OperatorPurchasesTabsContainer />, rootEl);
}

if (document.getElementById('UserProfileTabsProducts')) {
    const rootEl = document.getElementById('UserProfileTabsProducts');
    ReactDom.render(<UserProductsTabsContainer />, rootEl);
}

if (document.getElementById('UserProfileTabsPurchases')) {
    const rootEl = document.getElementById('UserProfileTabsPurchases');
    ReactDom.render(<UserPurchasesTabsContainer />, rootEl);
}

if (document.getElementById('TableUsersProducts')) {
    const rootEl = document.getElementById('TableUsersProducts');
    ReactDom.render(<TableUsersProductsContainer />, rootEl);
}

if (document.getElementById('FormPurchase')) {
    const rootEl = document.getElementById('FormPurchase');
    ReactDom.render(<FormPurchase />, rootEl);
}

if (document.getElementById('TableUsersPurchases')) {
    const rootEl = document.getElementById('TableUsersPurchases');
    ReactDom.render(<TableUsersPurchasesContainer />, rootEl);
}

if (document.getElementById('form-purchase-edit')) {
    const rootEl = document.getElementById('form-purchase-edit');
    ReactDom.render(<FormPurchase formType="edit" />, rootEl);
}

if (document.getElementById('registrationForm')) {
    const rootEl = document.getElementById('registrationForm');
    ReactDom.render(<RegisterLinkController default_form="register" />, rootEl);
}

if (document.getElementById('authForm')) {
    const rootEl = document.getElementById('authForm');
    ReactDom.render(<RegisterLinkController default_form="login" />, rootEl);
}


if (document.getElementById('react-mobile-menu')) {
    const rootEl = document.getElementById('react-mobile-menu');
    ReactDom.render(<MobileMenu />, rootEl);
}

if (document.getElementById('landing_dynamic')) {
    const rootEl = document.getElementById('landing_dynamic');
    ReactDom.render(<TableProductsContainer is_landing={true} />, rootEl);
}

if (document.getElementById('landing_static_spinner')) {
    const rootEl = document.getElementById('landing_static_spinner');
    ReactDom.render(<LoaderSpinner isTiny={true} />, rootEl);
}
