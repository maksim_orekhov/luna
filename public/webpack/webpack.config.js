const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const extractCss = new ExtractTextPlugin({
    filename: '[name].css',
    allChunks: true
});

module.exports = {
    module: {
        loaders: [
            {
                test: /\.js(x*)?$/,
                enforce: 'pre',
                use: 'eslint-loader',
            },
            {
                test: /\.js(x*)?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    cacheDirectory: true,
                }
            }, {
                test: /\.less$/,
                use: extractCss.extract({
                    use: [{
                        loader: 'css-loader',
                        options: {
                            url: false
                        }
                    }, {
                        loader: 'postcss-loader',
                        options: {
                            config: {
                                path: path.resolve(__dirname, 'postcss.config.js')
                            }
                        }
                    }, {
                        loader: 'less-loader'
                    }],
                    fallback: 'style-loader'
                })
            }
        ]
    },
    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        }),
        extractCss
    ],
    devtool: 'source-map',
    stats: { children: false },
    resolve: {
        extensions: ['.js', '.jsx', '.css', '.less']
    },
    entry: {
        landing: [
            path.resolve(__dirname, '../css/landing.less'),
            path.resolve(__dirname, '../js/index.js'),
            path.resolve(__dirname, '../js/index.jsx')
        ],
    },
    output: {
        filename: '../build/[name].js',
        path: path.resolve(__dirname, '../build'),
        // sourceMapFilename: '[name].js.map'
    }
};

