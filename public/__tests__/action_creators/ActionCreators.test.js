import React from 'react';
import { loadCategories, fetchSubcategories, loadCSRF, sendFormAdvertisement, initFormAdvertisement, initFormAuth, sendFormAuth, initFormRegister, sendFormRegister, initTableProducts, getPaginatorData, showMoreProducts, getSearchResult, loadUserInformation, changeAvatar, openUserProfileForm, closeUserProfileForm } from '../../js/react/actions/index';
import ACTION from "../../js/react/constants/actions";

describe('action creators tests', () => {
    it('loadCategories(): should create an action CATEGORIES_INIT', () => {
        const expectedAction = {
            type: ACTION.CATEGORIES_INIT,
        };
        expect(loadCategories()).toEqual(expectedAction)
    });

    it('fetchSubcategories(): should create an action to set categoryId', () => {
        const expectedAction = {
            type: ACTION.SUBCATEGORIES_FETCH,
            payload: {
                categoryId: '2'
            }
        };
        expect(fetchSubcategories('2')).toEqual(expectedAction)
    });

    it('loadCSRF(): should create an action CSRF_INIT', () => {
        const expectedAction = {
            type: ACTION.CSRF_INIT
        };
        expect(loadCSRF()).toEqual(expectedAction)
    });

    it('sendFormAdvertisement(): should create an action to set formData', () => {
        const expectedAction = {
            type: ACTION.ADVERTISE_FORM_SENDING,
            payload: {
                formData: {}
            }
        };
        expect(sendFormAdvertisement({})).toEqual(expectedAction)
    });

    it('initFormAdvertisement(): should create an action ADVERTISE_FORM_INIT', () => {
        const expectedAction = {
            type: ACTION.ADVERTISE_FORM_INIT
        };
        expect(initFormAdvertisement()).toEqual(expectedAction)
    });

    it('initFormAuth(): should create an action AUTH_FORM_INIT', () => {
        const expectedAction = {
            type: ACTION.AUTH_FORM_INIT
        };
        expect(initFormAuth()).toEqual(expectedAction)
    });

    it('sendFormAuth(): should create an action to set data', () => {
        const expectedAction = {
            type: ACTION.AUTH_FORM_SENDING,
            payload: {
                data: {
                    login: 'admin',
                    password: 'admin'
                }
            }
        };
        const data = expectedAction.payload.data;
        expect(sendFormAuth(data)).toEqual(expectedAction)
    });

    it('initFormRegister(): should create an action REGISTER_FORM_INIT', () => {
        const expectedAction = {
            type: ACTION.REGISTER_FORM_INIT
        };
        expect(initFormRegister()).toEqual(expectedAction)
    });

    it('sendFormRegister(): should create an action to set data', () => {
        const expectedAction = {
            type: ACTION.REGISTER_FORM_SENDING,
            payload: {
                data: {
                    login: 'admin',
                    password: 'admin'
                }
            }
        };
        const data = expectedAction.payload.data;
        expect(sendFormRegister(data)).toEqual(expectedAction)
    });

    it('initTableProducts(): should create an action to set formData', () => {
        const expectedAction = {
            type: ACTION.TABLE_PRODUCTS_INIT,
            payload: {
                formData: {}
            }
        };
        expect(initTableProducts({})).toEqual(expectedAction)
    });

    it('getPaginatorData(): should create an action TABLE_PRODUCTS_PAGINATOR_INIT', () => {
        const expectedAction = {
            type: ACTION.TABLE_PRODUCTS_PAGINATOR_INIT
        };
        expect(getPaginatorData()).toEqual(expectedAction)
    });

    it('showMoreProducts(): should create an action to set url', () => {
        const expectedAction = {
            type: ACTION.SHOW_MORE_PRODUCTS,
            payload: {
                url: 'luna.local/'
            }
        };
        expect(showMoreProducts('luna.local/')).toEqual(expectedAction)
    });

    it('getSearchResult(): should create an action to set url and query', () => {
        const expectedAction = {
            type: ACTION.SEARCH_FETCH,
            payload: {
                url: 'luna.local/',
                query: {}
            }
        };
        const payload = expectedAction.payload;
        expect(getSearchResult(payload)).toEqual(expectedAction)
    });

    it('loadUserInformation(): should create an action REGISTER_FORM_INIT', () => {
        const expectedAction = {
            type: ACTION.USER_LOAD
        };
        expect(loadUserInformation()).toEqual(expectedAction)
    });

    it('changeAvatar(): should create an action to set formData', () => {
        const expectedAction = {
            type: ACTION.USER_AVATAR_CHANGE,
            payload: {
                formData: {}
            }
        };
        expect(changeAvatar({})).toEqual(expectedAction)
    });

    it('openUserProfileForm(): should create an action to set formName', () => {
        const expectedAction = {
            type: ACTION.USER_PROFILE_FORM_OPEN,
            payload: {
                formName: 'phoneChange'
            }
        };
        expect(openUserProfileForm('phoneChange')).toEqual(expectedAction)
    });

    it('closeUserProfileForm(): should create an action to set formName', () => {
        const expectedAction = {
            type: ACTION.USER_PROFILE_FORM_CLOSE,
            payload: {
                formName: 'phoneChange'
            }
        };
        expect(closeUserProfileForm('phoneChange')).toEqual(expectedAction)
    });
});
