import ACTION from '../../js/react/constants/actions';
import FormAuthReducer from '../../js/react/reducers/FormAuthReducer';

const initialState = {
    formSendingError: false,
    formIsSending: false,
    sendResponse: {},
    serverErrors: {},
};

describe('Form auth reducer', function () {
    it('should return initial state', function () {
        expect(FormAuthReducer(undefined, {})).toEqual(initialState);
    });

    it('should return correct state on sending', function () {
        expect(FormAuthReducer(initialState, {
            type: ACTION.AUTH_FORM_SENDING_START
        })).toEqual({
            ...initialState,
            formIsSending: true,
        });

        expect(FormAuthReducer(initialState, {
            type: ACTION.AUTH_FORM_SENDING_SUCCESS,
            payload: { status: 'SUCCESS' }
        })).toEqual({
            ...initialState,
            formIsSending: false,
            sendResponse: { status: 'SUCCESS' }
        });

        expect(FormAuthReducer(initialState, {
            type: ACTION.AUTH_FORM_SENDING_FAIL,
            payload: { status: 'ERROR' }
        })).toEqual({
            ...initialState,
            formIsSending: false,
            serverErrors: { status: 'ERROR' }
        });

        expect(FormAuthReducer(initialState, {
            type: ACTION.AUTH_FORM_SENDING_SERVER_ERROR,
            payload: { status: 'ERROR' }
        })).toEqual({
            ...initialState,
            formIsSending: false,
            formSendingError: true,
            serverErrors: { status: 'ERROR' }
        });
    });

});
