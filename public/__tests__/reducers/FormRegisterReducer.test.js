import ACTION from '../../js/react/constants/actions';
import FormRegisterReducer from '../../js/react/reducers/FormRegisterReducer';

const initialReducerState = {
    formSendingError: false,
    formIsSending: false,
    sendResponse: {},
    serverErrors: {},
};

describe('Form register reducer', function () {
    it('should return initial state', function () {
        expect(FormRegisterReducer(undefined, {})).toEqual(initialReducerState);
    });

    it('should return state after sending', function () {
        expect(FormRegisterReducer(initialReducerState, {
            type: ACTION.REGISTER_FORM_SENDING_START
        })).toEqual({
            ...initialReducerState,
            serverErrors: {},
            formIsSending: true,
        });

        expect(FormRegisterReducer(initialReducerState, {
            type: ACTION.REGISTER_FORM_SENDING_SUCCESS,
            payload: { status: 'SUCCESS' }
        })).toEqual({
            ...initialReducerState,
            formIsSending: false,
            sendResponse: { status: 'SUCCESS' },
        });

        expect(FormRegisterReducer({
            ...initialReducerState,
            formIsSending: true,
        }, {
            type: ACTION.REGISTER_FORM_SENDING_FAIL,
            payload: { status: 'FAIL' },
        })).toEqual({
            ...initialReducerState,
            formIsSending: false,
            serverErrors: { status: 'FAIL' },
        });

        expect(FormRegisterReducer({
            ...initialReducerState,
            formIsSending: true,
        }, {
            type: ACTION.REGISTER_FORM_SENDING_SERVER_ERROR,
            payload: { status: 'FAIL' },
        })).toEqual({
            ...initialReducerState,
            formIsSending: false,
            formSendingError: true,
            serverErrors: { status: 'FAIL' },
        });

    });

});
