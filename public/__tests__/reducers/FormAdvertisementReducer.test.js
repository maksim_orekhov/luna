import FormAdvertisementReducer from '../../js/react/reducers/FormAdvertisementReducer';
import ACTION from '../../js/react/constants/actions';
import faker from 'faker';

const initialReducerState = {
    formSendingError: false,
    formIsSending: false,
    formIsInited: false,
    formIsIniting: false,
    sendResponse: {},
    serverErrors: {},
    defaultValues: {},
    hasDefaultValues: false,
};

describe('Form Advertisement reducer', function () {

    it('should return initial state', function () {
        expect(FormAdvertisementReducer(undefined, {})).toEqual(initialReducerState);
    });

    it('should return state after init', function () {
        const defaultValues = {
            name: faker.commerce.productName()
        };

        expect(FormAdvertisementReducer(initialReducerState, {
            type: ACTION.ADVERTISE_FORM_INIT_START
        })).toEqual({
            ...initialReducerState,
            formIsIniting: true
        });
        expect(FormAdvertisementReducer({
            ...initialReducerState,
            formIsIniting: true,
        }, {
            type: ACTION.ADVERTISE_FORM_INIT_SUCCESS,
            payload: {
                hasDefaultValues: true,
                defaultValues,
            }
        })).toEqual({
            ...initialReducerState,
            defaultValues,
            formIsIniting: false,
            formIsInited: true,
            hasDefaultValues: true,
        });
    });

    it('should return state after sending', function () {
        expect(FormAdvertisementReducer(initialReducerState, {
            type: ACTION.ADVERTISE_FORM_SENDING_START
        })).toEqual({
            ...initialReducerState,
            formIsSending: true,
        });

        expect(FormAdvertisementReducer(initialReducerState, {
            type: ACTION.ADVERTISE_FORM_SENDING_SUCCESS,
            payload: {
                status: 'SUCCESS',
            }
        })).toEqual({
            ...initialReducerState,
            formIsSending: false,
            sendResponse: {
                status: 'SUCCESS',
            },
        });

        expect(FormAdvertisementReducer(initialReducerState, {
            type: ACTION.ADVERTISE_FORM_SENDING_FAIL,
            payload: { message: 'error' }
        })).toEqual({
            ...initialReducerState,
            formIsSending: false,
            serverErrors: { message: 'error' }
        });

        expect(FormAdvertisementReducer(initialReducerState, {
            type: ACTION.ADVERTISE_FORM_SENDING_SERVER_ERROR,
            payload: { message: 'error' }
        })).toEqual({
            ...initialReducerState,
            formIsSending: false,
            formSendingError: true,
            serverErrors: { message: 'error' }
        });

    });

});
