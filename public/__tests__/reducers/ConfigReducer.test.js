import ConfigReducer from '../../js/react/reducers/ConfigReducer';
import ACTION from '../../js/react/constants/actions';
import faker from 'faker';

const initialReducerState = {
    categories: {
        entities: [],
        isLoading: false,
        isLoaded: false,
        isFetching: false,
    },
    csrf: {
        value: '',
        isLoading: false,
        isLoaded: false,
        isFailed: false,
    }
};

function generateCategories (categoriesCount = 10) {
    return new Array(categoriesCount).fill(undefined).map((item, index) => {
        return { id: index, name: faker.lorem.words(), slug: faker.lorem.slug(), url: faker.internet.url() }
    })
}

describe('Config reducer', () => {
    it('should return initial state', function () {
        expect(ConfigReducer(undefined, {})).toEqual(initialReducerState);
    });

    it('should return state after categories init start', function () {
        expect(ConfigReducer(initialReducerState, {
            type: ACTION.CATEGORIES_INIT_START
        })).toEqual({
            ...initialReducerState,
            categories: {
                ...initialReducerState.categories,
                isLoading: true,
            },
        })
    });

    it('should return state after categories init success', function () {
        const categories = generateCategories();
        expect(ConfigReducer(initialReducerState, {
            type: ACTION.CATEGORIES_INIT_SUCCESS,
            payload: {
                categories
            }
        })).toEqual({
            ...initialReducerState,
            categories: {
                ...initialReducerState.categories,
                entities: categories,
                isLoading: false,
                isLoaded: true,
            }
        });
    });

    it('should return state after subcategories fetch success', function () {
        const currentState = {
            ...initialReducerState,
            categories: {
                ...initialReducerState.categories,
                entities: generateCategories(),
                isFetching: true,
            }
        };
        const subCategories = generateCategories(8);
        expect(ConfigReducer(currentState, {
            type: ACTION.SUBCATEGORIES_FETCH_SUCCESS,
            payload: {
                categoryId: 0,
                subcategories: subCategories
            }
        })).toEqual({
            ...currentState,
            categories: {
                ...currentState.categories,
                entities: [
                    {
                        ...currentState.categories.entities[0],
                        subcategories: [
                            ...subCategories
                        ]
                    },
                    ...currentState.categories.entities.slice(1),
                ],
                isFetching: false
            }
        });
    });

    it('should return state after subcategories fetch start', function () {
        expect(ConfigReducer(initialReducerState, {
            type: ACTION.SUBCATEGORIES_FETCH_START,
        })).toEqual({
            ...initialReducerState,
            categories: {
                ...initialReducerState.categories,
                isFetching: true,
            },
        });
    });

    it('should return state after CSRF load start', function () {
        expect(ConfigReducer(initialReducerState, {
            type: ACTION.CSRF_INIT_START
        })).toEqual({
            ...initialReducerState,
            csrf: {
                ...initialReducerState.csrf,
                isLoading: true,
                isLoaded: false,
                isFailed: false,
            },
        });
    });

    it('should return state after CSRF load success', function () {
        const csrfValue = faker.random.uuid();
        expect(ConfigReducer(initialReducerState, {
            type: ACTION.CSRF_INIT_SUCCESS,
            payload: {
                csrf: csrfValue,
            }
        })).toEqual({
            ...initialReducerState,
            csrf: {
                value: csrfValue,
                isLoading: false,
                isLoaded: true,
                isFailed: false,
            }
        })
    });

    it('should handle CSRF load fail', function () {
        expect(ConfigReducer(initialReducerState, {
            type: ACTION.CSRF_INIT_FAIL,
        })).toEqual({
            ...initialReducerState,
            csrf: {
                ...initialReducerState.csrf,
                value: '',
                isLoading: false,
                isLoaded: true,
                isFailed: true,
            },
        })
    });

});
