import React from 'react';
import {shallow} from 'enzyme';
import {shallowToJson} from 'enzyme-to-json';
import LoaderSpinner from '../../js/react/components/LoaderSpinner';

describe('LoaderSpinner component tests', () => {
    const loaderComponent = shallow(<LoaderSpinner />);
    it('should correct snapshot render', () => {
        expect(shallowToJson(loaderComponent)).toMatchSnapshot();
    });
});
