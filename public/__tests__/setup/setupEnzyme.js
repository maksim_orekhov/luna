import Enzyme, { shallow, render, mount } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-15';
import VALIDATION_RULES from '../../js/react/constants/validation_rules';
import * as Helpers from '../../js/react/Helpers';

Enzyme.configure({ adapter: new Adapter });

const customFetch = () => new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve({
            status: 'SUCCESS',
            data: {}
        });
    }, 100);
});

Helpers.customFetch = customFetch;

global.Helpers = Helpers;

global.shallow = shallow;
global.render = render;
global.mount = mount;
global.shallowToJson = shallowToJson;
global.VALIDATION_RULES = VALIDATION_RULES;