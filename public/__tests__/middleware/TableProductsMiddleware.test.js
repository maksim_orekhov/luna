import React from 'react';
import TableProductsMiddleware from '../../js/react/middleware/TableProductsMiddleware';

describe('TableProductsMiddleware tests', () => {
    const create = () => {
        const store = {
            getState: jest.fn(() => ({})),
            dispatch: jest.fn(),
        };

        const next = jest.fn();
        const invoke = (action) => TableProductsMiddleware(store)(next)(action);
        return {store, next, invoke}
    };

    it('calls action TABLE_PRODUCTS_INIT_START', () => {
        const { next, invoke } = create();
        const action = {type: 'TABLE_PRODUCTS_INIT_START'};
        invoke(action);
        expect(next).toHaveBeenCalledWith(action)
    });

    it('calls action TABLE_PRODUCTS_PAGINATOR_INIT_START', () => {
        const { next, invoke } = create();
        const action = {type: 'TABLE_PRODUCTS_PAGINATOR_INIT_START'};
        invoke(action);
        expect(next).toHaveBeenCalledWith(action)
    });

    it('calls action SHOW_MORE_PRODUCTS_START', () => {
        const { next, invoke } = create();
        const action = {type: 'SHOW_MORE_PRODUCTS_START'};
        invoke(action);
        expect(next).toHaveBeenCalledWith(action)
    });
});
