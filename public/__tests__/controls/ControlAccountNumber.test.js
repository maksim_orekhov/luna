import React from 'react';
import ControlAccountNumber from '../../js/react/controls/ControlAccountNumber';
import { sendUpValidationOnReceivePropsTest, sendUpValidationWhenInputChangeTest, didComponentChangeWhenInputChangeTest } from './Helpers/SendUpValidation';
import { shouldCheckAllValidations, shouldCheckEmptyValue } from './Helpers/CheckAllValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';

describe('ControlAccountNumber component tests', () => {
    const props = {
        label: 'Номер лицевого счёта:',
        name: 'account_number',
        value_prop: '1'.repeat(VALIDATION_RULES.ACCOUNT_NUMBER.chars_count)
    };
    const loaderComponent = shallow(<ControlAccountNumber {...props} />);
    it('should correct snapshot render', () => {
        expect(shallowToJson(loaderComponent)).toMatchSnapshot();
    });

    it('should send up validation when receive new value from parent component', () => {
        sendUpValidationOnReceivePropsTest(<ControlAccountNumber {...props} />, props);
    });

    it('should send up value and validation when input change value', () => {
        sendUpValidationWhenInputChangeTest(<ControlAccountNumber {...props} />, props);
        didComponentChangeWhenInputChangeTest(<ControlAccountNumber {...props} />, props);
    });

    it('should correct work component checkAllValidations', () => {
        shouldCheckEmptyValue(<ControlAccountNumber {...props} />);
        shouldCheckAllValidations(<ControlAccountNumber {...props} />, props);
    });

    it('should correct set errors flags', () => {
        const flags = {
            isEmpty: true,
            chars_count: true,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlAccountNumber {...props} />, flags);
    });
});
