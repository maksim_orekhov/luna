#!/bin/bash

color_output() {
  echo -e "\033[$2m$1\033[0m"
}

title_label() {
  color_output "$1" "$START_END_SCRIPT_COLOR"
}

#Command line colors
COLOR_INTERACTION=36
COLOR_WARNING=33
COLOR_OK=32
COLOR_ERROR=31
COLOR_EXIT=35



echo -e "\e[45m\e[4mСкрипт развертывания проекта\e[0m"
echo -e "\e[45m\e[18m\e[37m            LUNA            \e[0m"
color_output "Выполнить (y/n)?" "$COLOR_INTERACTION"
read CONTINUE
if [[ "$CONTINUE" == *"yes"* ]] || [[ "$CONTINUE" == *"y"* ]]
then
	#SYS -> Check cache directory
	
	if [ -d /d/wamp64/www/master/data/cache ];
	then
		color_output "Проверка кеша - Ок " "$COLOR_OK"
	else 
		color_output "Директория cache - Не найдена " "$COLOR_ERROR"
		color_output "Создаем директорию... " "$COLOR_OK"		
		mkdir data/cache		
		
		if [ -d /d/wamp64/www/master/data/cache ];
		then		
			color_output "...директория создана." "$COLOR_OK"
		else
			color_output "Директория все равно не создана :( " "$COLOR_ERROR"
		fi
	fi
      
	
    #Back -> Install dependencies
    echo ""
    color_output "Устанавливаем зависимости..." "$COLOR_OK"
    composer install

    #Back -> Migrations
    echo ""
    color_output "Установить миграции (yes/n)?" "$COLOR_INTERACTION"
    color_output "Текущая база данных будет сброшена" "$COLOR_WARNING"
    read RUN_MIGRATIONS

    if [[ "$RUN_MIGRATIONS" == *"yes"* ]] || [[ "$RUN_MIGRATIONS" == *"y"* ]]
    then
        echo ""
        color_output "Выполняем миграции..." "$COLOR_OK"
        ./vendor/bin/doctrine-module --no-interaction migrations:migrate
        echo ""
    fi

    #Back -> Fixtures
    echo ""
    color_output "Установить фикстуры (yes/no)? " "$COLOR_INTERACTION"
    read INSTALL_FIXTURES

    if [[ "$INSTALL_FIXTURES" == *"yes"* ]] || [[ "$INSTALL_FIXTURES" == *"y"* ]]
    then
        ./vendor/bin/doctrine-module --no-interaction orm:fixtures:load
        echo ""
    fi
	  
    # Front

    # Installing front dependencies
    color_output "Вы хотите установить зависимости для Front (y/n)?" "$COLOR_INTERACTION"
    read INSTALL_FRONT_DEPENDENCIES
    cd public

    if [[ "$INSTALL_FRONT_DEPENDENCIES" == *"yes"* ]] || [[ "$INSTALL_FRONT_DEPENDENCIES" == *"y"* ]] 
    then
        echo ""
        color_output "Устанавливаем зависимости для Front..." "$COLOR_OK"
        echo ""
		npm install
    fi

    # Front construction
    color_output "Вы хотите собрать Front (y/n)?" "$COLOR_INTERACTION"
    read RUN_FRONT_BUILD

    if [[ "$RUN_FRONT_BUILD" == *"yes"* ]] || [[ "$RUN_FRONT_BUILD" == *"y"* ]];
    then
        npm run build		
        echo ""
    fi
	
	# Front Watcher
	color_output "Вы хотите запустить режим отслеживания Front (y/n)?" "$COLOR_INTERACTION"
    read RUN_FRONT_WATCHER

    if [[ "$RUN_FRONT_WATCHER" == *"yes"* ]] || [[ "$RUN_FRONT_WATCHER" == *"y"* ]];
    then
        color_output "Для выхода из режиме слежения нажмите CTRL+C" "$COLOR_WARNING"
        npm run start
        echo ""
    fi
else
    echo ""
    color_output "Exit" "$COLOR_EXIT"
fi

