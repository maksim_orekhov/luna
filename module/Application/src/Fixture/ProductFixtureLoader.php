<?php
namespace Application\Fixture;

use Application\Entity\Product;
use Application\Entity\Category;
use Application\Entity\Moderation;
use Application\Service\ProductManager;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\User;

class ProductFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{

    use ContainerAwareTrait;
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var ProductManager
     */
    private $productManager;

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        require_once __DIR__.'./products/files/DisablePhpUploadChecks.php';
        $products_data = include __DIR__.'./products/products.php';

        $this->entityManager = $this->container->get('doctrine.entitymanager.orm_default');
        $this->productManager = $this->container->get(ProductManager::class);
        /**
         * @var User $user
         */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => 'test'));
        if($user !== null) {
            //для пользователя test
            $this->createProductsToUser($user, $products_data[$user->getLogin()]);
        }
        //для магазинов
        $i = 0;
        foreach (UserFixtureLoader::USERS as $userPattern) {
            if ($userPattern['type'] === 'shop') {
                $i++;
                /**
                 * @var User $userForShop
                 */
                $userForShop = $this->entityManager->getRepository(User::class)
                    ->findOneBy(array('login' => $userPattern['login']));
                if ($userForShop) {
                    $this->createProductsToUser($userForShop, $products_data['shop'], $i);
                }
            }
        }
    }

    /**
     * @param User $user
     * @param $products_data
     * @param int $shop_idx
     * @throws \Exception
     */
    public function createProductsToUser(User $user, $products_data, $shop_idx = 0)
    {
        $csrf = new \Zend\Form\Element\Csrf('csrf');
        $i = 0;
        foreach ($products_data as $product_data) {
            /** @var Category $category */
            $category = $this->entityManager->getRepository(Category::class)
                ->findOneBy(array('slug' => $product_data['category']));
            if (!$category) {
                throw new \Exception("\n****************\nNot found category !!!".$product_data['category']);
            }
            $data = [
                'name' => $shop_idx > 0 ? 'Shop №'.$shop_idx.' '.$product_data['name'] : $product_data['name'],
                'description' => $product_data['description'],
                'price' => $product_data['price'],
                'safe_deal' => $product_data['safe_deal'] ?? '',
                'csrf' => $csrf->getValue(),
                'category' => $category->getId(),
                'address' => $product_data['address']
            ];

            $temporary_files['files'] = $this->productManager->saveProductTemporaryFiles($product_data['files'] ?? []);
            $temporary_files['gallery'] = $this->productManager->saveProductTemporaryFiles($product_data['gallery'] ?? []);
            /** @var Product $product */
            $product = $this->productManager->createProduct($user, $data);

            //привязываем файлы
            $this->productManager->attachProductFiles($product, $temporary_files);

            $current_day = new \DateTime();
            $current_day->modify('-'.$i.' hours');
            $product->setCreated($current_day);

            if (isset($product_data['removed'])) {
                $product->setRemoved($product_data['removed']);
            }

            if (isset($product_data['visible'])) {
                $product->setVisible($product_data['visible']);
            }

            if (isset($product_data['approved'])) {
                /** @var ArrayCollection|PersistentCollection $moderations */
                $moderations = $product->getModerations();
                /** @var Moderation $lastModeration */
                $lastModeration = $moderations->last();
                $lastModeration->setApproved(true);
                $lastModeration->setResolutionDate(new \DateTime());

                #$this->entityManager->persist($lastModeration);
            }

            if (isset($product_data['declined'])) {
                /** @var ArrayCollection|PersistentCollection $moderations */
                $moderations = $product->getModerations();
                /** @var Moderation $lastModeration */
                $lastModeration = $moderations->last();
                $lastModeration->setApproved(false);
                $lastModeration->setDescription('Контактная информация в заголовке объявления');
                $lastModeration->setResolutionDate(new \DateTime());

                #$this->entityManager->persist($lastModeration);
            }

            $this->entityManager->persist($product);

            $i++;
        }

        $this->entityManager->flush();
    }

    public function getOrder()
    {
        return 50;
    }
}