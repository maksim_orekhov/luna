<?php
namespace Application\Fixture;

use Application\Entity\Category;
use Application\Service\ShopManager;
use Application\Service\UserManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\User;

class ShopFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{

    use ContainerAwareTrait;
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var ShopManager
     */
    private $shopManager;
    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        require_once __DIR__.'./products/files/DisablePhpUploadChecks.php';
        $this->entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        $this->shopManager = $this->container->get(ShopManager::class);
        $this->userManager = $this->container->get(UserManager::class);

        $i = 0;
        foreach (UserFixtureLoader::USERS as $userPattern) {
            if ($userPattern['type'] === 'shop') {
                $i++;
                /**
                 * @var User $user
                 */
                $user = $this->entityManager->getRepository(User::class)
                    ->findOneBy(array('login' => $userPattern['login']));
                $data = [
                    'name'=>'М.Видео №'.$i,
                    'legal_name'=>'ПАО "М.ВИДЕО №'.$i.'"',
                    'description'=>'Интернет-магазин цифровой и бытовой техники и электроники М.Видео. В каталоге большой выбор товаров по доступным ценам с доставкой на дом и самовывоз из магазинов М.Видео по всей России. Гарантия качества!',
                    'inn'=>'7707602010',
                    'ogrn'=>'5067746789248',
                    'legal_address'=>'105066, г Москва, улица Красносельская Нижн., 40/12 20.',
                    'postal_address'=>'105066, г Москва, улица Красносельская Нижн., 40/12 20.',
                    'phones'=>'+79101000101,+79101000102',
                    'work_schedule'=>'Пн-Пт 09:00-18:00,Сб-Вс Выходной',
                    'web_links'=>'https://yandex.ru/,https://www.google.ru/',
                ];
                $shop = $this->shopManager->createCustomShop($user, $data);
                //set logo
                $data = [
                    'name' => 'mvideo.png',
                    'type' => 'image/png',
                    'tmp_name' => __DIR__.'./shops/files/mvideo.png',
                    'size' => filesize(__DIR__.'./shops/files/mvideo.png'),
                ];
                $this->shopManager->createShopLogo($shop, $data);
            }
        }
    }

    public function getOrder()
    {
        return 40;
    }
}