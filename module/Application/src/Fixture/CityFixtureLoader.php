<?php
namespace Application\Fixture;

use Application\Entity\City;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;


class CityFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');
        $ru_cities = file_get_contents(__DIR__.'/cities/ru_cities.json');
        $ru_cities = json_decode($ru_cities, true);
        foreach ($ru_cities as $country_pattern) {
            $country = new City();
            $country->setName($country_pattern['name']);
            $country->setNamePrepositional($country_pattern['name_prepositional']);
            $country->setSlug($country_pattern['slug']);
            $country->setType(City::COUNTRY_TYPE);

            $entityManager->persist($country);

            foreach ($country_pattern['regions'] as $region_pattern) {
                $region = new City();
                $region->setName($region_pattern['name']);
                $region->setNamePrepositional($region_pattern['name_prepositional']);
                $region->setSlug($region_pattern['slug']);
                $region->setType(City::REGION_TYPE);
                $region->setParent($country);

                //persist child
                $entityManager->persist($region);
                //add to collection
                $country->addChildren($region);
                //persist parent
                $entityManager->persist($country);

                // sub category
                foreach ($region_pattern['cities'] as $city_pattern) {
                    $city = new City();
                    $city->setName($city_pattern['name']);
                    $city->setNamePrepositional($city_pattern['name_prepositional']);
                    $city->setSlug($city_pattern['slug']);
                    $city->setType(City::CITY_TYPE);
                    $city->setParent($region);

                    //persist child
                    $entityManager->persist($city);
                    //add to collection
                    $region->addChildren($city);
                    //persist parent
                    $entityManager->persist($region);
                }
            }
        }

        $entityManager->flush();
    }

    public function getOrder()
    {
        return 20;
    }
}