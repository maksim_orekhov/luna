<?php
namespace Application\Fixture;

use Application\Entity\Category;
use Application\Service\FabricatorManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\User;

class FabricatorFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{

    use ContainerAwareTrait;
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var FabricatorManager
     */
    private $fabricatorManager;

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $this->entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        $this->fabricatorManager = $this->container->get(FabricatorManager::class);
        /**
         * @var User $userTest
         * @var User $userTest2
         */
        $userTest = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => 'test'));

        /** @var Category $legkovye_avtomobili */
        $legkovye_avtomobili = $this->entityManager->getRepository(Category::class)
            ->findOneBy(array('slug' => 'legkovye_avtomobili'));
        $this->fabricatorManager->createCustomFabricator(
            $userTest,
            [
                'category'=> [
                    $legkovye_avtomobili->getId()
                ],
                'name' => 'Japan Motors Corporation',
                'inn' => '7705540343',
                'kpp' => '771001001',
                'legal_address' => '33-8, Shiba 5-chome, Minato-ku, Tokyo 108-8410 Japan'
            ]
        );
    }

    public function getOrder()
    {
        return 50;
    }
}