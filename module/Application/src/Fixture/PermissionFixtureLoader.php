<?php

namespace Application\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\Permission;

class PermissionFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    const PERMISSION_NAMES =[
        'profile_own_edit' => 'profile.own.edit',
        'profile_any_view' => 'profile.any.view',
        'product_own_edit' => 'product.own.edit',
        'product_own_view' => 'product.own.view',
        'purchase_own_edit' => 'purchase.own.edit',
        'purchase_own_view' => 'purchase.own.view',
        'moderation_any_edit' => 'moderation.any.edit',
        'settings_edit' => 'settings.edit',
    ];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        foreach (self::PERMISSION_NAMES as $permission_name) {
            $permission = new Permission();
            $permission->setName($permission_name);
            $entityManager->persist($permission);
        }

        $entityManager->flush();
    }

    public function getOrder()
    {
        return 10;
    }
}