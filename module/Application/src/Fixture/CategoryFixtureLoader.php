<?php
namespace Application\Fixture;

use Application\Entity\Category;
use Behat\Transliterator\Transliterator;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;


class CategoryFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    const CATEGORIES = [
        [
            'name' => 'Транспорт',
            'slug' => 'transport',
            'child' => [
                [
                    'name' => 'Легковые автомобили',
                    'slug' => 'legkovye_avtomobili',
                ],
                [
                    'name' => 'Грузовики и спецтехника',
                    'slug' => 'gruzoviki_i_spetstekhnika',
                ],
                [
                    'name' => 'Мотоциклы и мототехника',
                    'slug' => 'mototsikly_i_mototekhnika',
                ],
                [
                    'name' => 'Водный транспорт',
                    'slug' => 'vodnyi_transport',
                ],
                [
                    'name' => 'Запчасти и аксессуары',
                    'slug' => 'zapchasti_i_aksessuary',
                ],

                [
                    'name' => 'Шины, диски и колёса',
                    'slug' => 'shiny_diski_i_koliosa',
                ],

            ],
        ],
        [
            'name' => 'Промтовары',
            'slug' => 'promtovary',
            'child' => [
                [
                    'name' => 'Пиломатериалы',
                    'slug' => 'pilomaterialy'
                ],
                [
                    'name' => 'Кирпичи',
                    'slug' => 'kirpichi'
                ],
                [
                    'name' => 'Строительные материалы',
                    'slug' => 'stroitelnye_materialy'
                ],
            ],
        ],
        [
            'name' => 'Недвижимость',
            'slug' => 'nedvizhimost',
            'child' => [
                [
                    'name' => 'Новостройки',
                    'slug' => 'novostroiki'
                ],
                [
                    'name' => 'Вторичное жилье',
                    'slug' => 'vtorichnoe_zhile'
                ],
                [
                    'name' => 'Дома, дачи, коттеджи',
                    'slug' => 'doma_dachi_kottedzhi'
                ],
                [
                    'name' => 'Земельные участки',
                    'slug' => 'zemelnye_uchastki'
                ],
                [
                    'name' => 'Гаражи и машиноместа',
                    'slug' => 'garazhi_i_mashinomesta'
                ],
                [
                    'name' => 'Коммерческая недвижимость',
                    'slug' => 'kommercheskaia_nedvizhimost'
                ],
                [
                    'name' => 'Недвижимость за рубежом',
                    'slug' => 'nedvizhimost_za_rubezhom'
                ],
            ],
        ],
        [
            'name' => 'Услуги',
            'slug' => 'uslugi',
            'child' => [
                [
                    'name' => 'Аренда спецтехники',
                    'slug' => 'arenda_spetstekhniki'
                ],
                [
                    'name' => 'Ремонт, строительство',
                    'slug' => 'remont_stroitelstvo'
                ],
            ],
        ],
        [
            'name' => 'Одежда',
            'slug' => 'odezhda',
            'child' => [
                [
                    'name' => 'Нижнее белье',
                    'slug' => 'nizhnee_bele'
                ],
                [
                    'name' => 'Верхняя одежда',
                    'slug' => 'verkhniaia_odezhda'
                ],
                [
                    'name' => 'Обувь',
                    'slug' => 'obuv',
                ],
                [
                    'name' => 'Детская одежда и обувь',
                    'slug' => 'detskaia_odezhda_i_obuv',
                ],
                [
                    'name' => 'Аксессуары',
                    'slug' => 'aksessuary',
                ],
            ],
        ],
        [
            'name' => 'Товары для дома',
            'slug' => 'tovary_dlia_doma',
            'child' => [
                [
                    'name' => 'Мебель и интерьер',
                    'slug' => 'mebel_i_interer'
                ],
                [
                    'name' => 'Посуда и товары для кухни',
                    'slug' => 'posuda_i_tovary_dlia_kukhni',
                ],
                [
                    'name' => 'Продукты питания',
                    'slug' => 'produkty_pitaniia',
                ],
                [
                    'name' => 'Растения',
                    'slug' => 'rasteniia',
                ],
                [
                    'name' => 'Бытовая химия',
                    'slug' => 'bytovaia_khimiia',
                ],
                [
                    'name' => 'Детские коляски',
                    'slug' => 'detskie_koliaski',
                ],
            ],
        ],
        [
            'name' => 'Бытовая электроника',
            'slug' => 'bytovaia_elektronika',
            'child' => [
                [
                    'name' => 'Мобильные телефоны',
                    'slug' => 'mobilnye_telefony',
                ],
                [
                    'name' => 'Ноутбуки',
                    'slug' => 'noutbuki',
                ],
                [
                    'name' => 'Настольные компьютеры',
                    'slug' => 'nastolnye_kompiutery',
                ],
                [
                    'name' => 'Телевизоры',
                    'slug' => 'televizory',
                ],
                [
                    'name' => 'Планшеты и электронные книги',
                    'slug' => 'planshety_i_elektronnye_knigi',
                ],
                [
                    'name' => 'Аудио и видео',
                    'slug' => 'audio_i_video',
                ],
                [
                    'name' => 'Стиральные машины',
                    'slug' => 'stiralnye_mashiny',
                ],
                [
                    'name' => 'Оргтехника и расходники',
                    'slug' => 'orgtekhnika_i_raskhodniki',
                ],
                [
                    'name' => 'Товары для компьютера',
                    'slug' => 'tovary_dlia_kompiutera',
                ],
                [
                    'name' => 'Фототехника',
                    'slug' => 'fototekhnika',
                ],
                [
                    'name' => 'Игры, приставки и программы',
                    'slug' => 'igry_pristavki_i_programmy',
                ],
            ],
        ],
        [
            'name' => 'Хобби и отдых',
            'slug' => 'khobbi_i_otdykh',
            'child' => [
                [
                    'name' => 'Билеты и путешествия',
                    'slug' => 'bilety_i_puteshestviia',
                ],
                [
                    'name' => 'Велосипеды',
                    'slug' => 'velosipedy',
                ],
                [
                    'name' => 'Книги и журналы',
                    'slug' => 'knigi_i_zhurnaly',
                ],
                [
                    'name' => 'Коллекционирование',
                    'slug' => 'kollektsionirovanie',
                ],
                [
                    'name' => 'Музыкальные инструменты',
                    'slug' => 'muzykalnye_instrumenty',
                ],
                [
                    'name' => 'Охота и рыбалка',
                    'slug' => 'okhota_i_rybalka',
                ],
                [
                    'name' => 'Спорт и отдых',
                    'slug' => 'sport_i_otdykh',
                ],
                [
                    'name' => 'Сделай сам (Рукоделие)',
                    'slug' => 'sdelai_sam_rukodelie',
                ],
            ],
        ],
        [
            'name' => 'Животные',
            'slug' => 'zhivotnye',
            'child' => [
                [
                    'name' => 'Собаки',
                    'slug' => 'sobaki',
                ],
                [
                    'name' => 'Кошки',
                    'slug' => 'koshki',
                ],
                [
                    'name' => 'Птицы',
                    'slug' => 'ptitsy',
                ],
                [
                    'name' => 'Аквариум',
                    'slug' => 'akvarium',
                ],
                [
                    'name' => 'Другие животные',
                    'slug' => 'drugie_zhivotnye',
                ],
                [
                    'name' => 'Товары для животных',
                    'slug' => 'tovary_dlia_zhivotnykh',
                ],
            ],
        ],
        [
            'name' => 'Для бизнеса',
            'slug' => 'dlia_biznesa',
            'child' => [
                [
                    'name' => 'Готовый бизнес',
                    'slug' => 'gotovyi_biznes',
                ],
                [
                    'name' => 'Оборудование для бизнеса',
                    'slug' => 'oborudovanie_dlia_biznesa',
                ],
            ],
        ],
    ];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        foreach (self::CATEGORIES as $ar_category) {
            $category = new Category();
            $category->setName(trim($ar_category['name']));
            $category->setSlug(
                $ar_category['slug'] ?? Transliterator::transliterate(
                    mb_strtolower(trim($ar_category['name'])),
                    '_'
                )
            );
            $category->setActive(true);

            //persist category
            $entityManager->persist($category);
            // sub category
            foreach ($ar_category['child'] as $ar_sub_category) {
                $sub_category = new Category();
                $sub_category->setName(trim($ar_sub_category['name']));
                $sub_category->setSlug(
                    $ar_sub_category['slug'] ?? Transliterator::transliterate(
                        mb_strtolower(trim($ar_sub_category['name'])),
                        '_'
                    )
                );
                $sub_category->setActive(true);
                $sub_category->setParent($category);

                //persist child category
                $entityManager->persist($sub_category);
                //add to collection
                $category->addChildren($sub_category);
                //persist parent category
                $entityManager->persist($category);
            }
        }

        $entityManager->flush();
    }

    public function getOrder()
    {
        return 10;
    }
}