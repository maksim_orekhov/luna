<?php

namespace Application\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\Role;
use Application\Entity\Permission;

class RoleFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');
        //verified
        $profile_any_view = $entityManager->getRepository(Permission::class)
            ->findOneBy(array('name' => PermissionFixtureLoader::PERMISSION_NAMES['profile_any_view']));
        $profile_own_edit = $entityManager->getRepository(Permission::class)
            ->findOneBy(array('name' => PermissionFixtureLoader::PERMISSION_NAMES['profile_own_edit']));
        $product_own_view = $entityManager->getRepository(Permission::class)
            ->findOneBy(array('name' => PermissionFixtureLoader::PERMISSION_NAMES['product_own_view']));
        $product_own_edit = $entityManager->getRepository(Permission::class)
            ->findOneBy(array('name' => PermissionFixtureLoader::PERMISSION_NAMES['product_own_edit']));
        $purchase_own_view = $entityManager->getRepository(Permission::class)
            ->findOneBy(array('name' => PermissionFixtureLoader::PERMISSION_NAMES['purchase_own_view']));
        $purchase_own_edit = $entityManager->getRepository(Permission::class)
            ->findOneBy(array('name' => PermissionFixtureLoader::PERMISSION_NAMES['purchase_own_edit']));
        // operator
        $moderation_any_edit = $entityManager->getRepository(Permission::class)
            ->findOneBy(array('name' => PermissionFixtureLoader::PERMISSION_NAMES['moderation_any_edit']));
        //admin
        $settings_edit = $entityManager->getRepository(Permission::class)
            ->findOneBy(array('name' => PermissionFixtureLoader::PERMISSION_NAMES['settings_edit']));

        //*********************  Unverified   *************************//
        $roleUnverified = new Role();
        $roleUnverified->setName('Unverified');
        $entityManager->persist($roleUnverified);

        //*********************  Verified   ****************************//
        $roleVerified = new Role();
        $roleVerified->setName('Verified');
        $roleVerified->addPermission($profile_any_view);
        $roleVerified->addPermission($profile_own_edit);
        $roleVerified->addPermission($product_own_view);
        $roleVerified->addPermission($product_own_edit);
        $roleVerified->addPermission($purchase_own_view);
        $roleVerified->addPermission($purchase_own_edit);
        $entityManager->persist($roleVerified);

        //*********************  Operator   ****************************//
        $roleOperator = new Role();
        $roleOperator->setName('Operator');
        $roleOperator->addPermission($moderation_any_edit);
        $entityManager->persist($roleOperator);

        //*********************  Administrator   ***********************//
        $roleAdministrator = new Role();
        $roleAdministrator->setName('Administrator');
        $roleAdministrator->addPermission($settings_edit);
        $entityManager->persist($roleAdministrator);

        $entityManager->flush();
    }

    public function getOrder()
    {
        return 20;
    }
}