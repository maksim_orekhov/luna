<?php

namespace Application\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\GlobalSetting;

class GlobalSettingFixtureLoader implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function load(ObjectManager $manager)
    {
//        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');
//
//        $globalSetting = new GlobalSetting();
//        $globalSetting->setName('fee_percentage');
//        $globalSetting->setDescription('Процент гонорара');
//        $globalSetting->setValue(4);
//        $entityManager->persist($globalSetting);
//
//        $globalSetting2 = new GlobalSetting();
//        $globalSetting2->setName('min_delivery_period');
//        $globalSetting2->setDescription('Минимальный срок поставки');
//        $globalSetting2->setValue(1);
//        $entityManager->persist($globalSetting2);
//
//        $entityManager->flush();
    }
}