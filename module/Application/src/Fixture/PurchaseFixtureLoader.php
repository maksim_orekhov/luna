<?php
namespace Application\Fixture;

use Application\Entity\Purchase;
use Application\Entity\Category;
use Application\Entity\Moderation;
use Application\Service\PurchaseManager;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\User;

class PurchaseFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{

    use ContainerAwareTrait;
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var PurchaseManager
     */
    private $purchaseManager;

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        require_once __DIR__.'./products/files/DisablePhpUploadChecks.php';
        $purchases_data = include __DIR__.'./purchases/purchases.php';

        $this->entityManager = $this->container->get('doctrine.entitymanager.orm_default');
        $this->purchaseManager = $this->container->get(PurchaseManager::class);
        /**
         * @var User $userTest
         * @var User $userTest2
         */
        $userTest = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => 'test'));
        $userTest2 = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => 'test2'));

        //для пользователя test
        $this->createPurchasesToUser($userTest, $purchases_data[$userTest->getLogin()]);
        //для пользователя test2
        $this->createPurchasesToUser($userTest2, $purchases_data[$userTest2->getLogin()]);
    }

    /**
     * @param User $user
     * @param $purchases_data
     * @throws \Exception
     */
    public function createPurchasesToUser(User $user, $purchases_data)
    {
        $csrf = new \Zend\Form\Element\Csrf('csrf');
        $i = 0;
        foreach ($purchases_data as $purchase_data) {
            /** @var Category $category */
            $category = $this->entityManager->getRepository(Category::class)
                ->findOneBy(array('slug' => $purchase_data['category']));
            if (!$category) {
                throw new \Exception("\n****************\nNot found category !!!".$purchase_data['category']);
            }
            $data = [
                'name' => $purchase_data['name'],
                'description' => $purchase_data['description'],
                'price' => $purchase_data['price'],
                'safe_deal' => $purchase_data['safe_deal'],
                'quantity' => $purchase_data['quantity'],
                'measure' => $purchase_data['measure'],
                'csrf' => $csrf->getValue(),
                'category' => $category->getId(),
                'address' => $purchase_data['address']
            ];

            $temporary_files['files'] = $this->purchaseManager->savePurchaseTemporaryFiles($purchase_data['files'] ?? []);
            /** @var Purchase $purchase */
            $purchase = $this->purchaseManager->createPurchase($user, $data);

            //привязываем файлы
            $this->purchaseManager->attachPurchaseFiles($purchase, $temporary_files);

            $current_day = new \DateTime();
            $current_day->modify('-'.$i.' hours');
            $purchase->setCreated($current_day);

            if (isset($purchase_data['removed'])) {
                $purchase->setRemoved($purchase_data['removed']);
            }

            if (isset($purchase_data['visible'])) {
                $purchase->setVisible($purchase_data['visible']);
            }

            if (isset($purchase_data['approved'])) {
                /** @var ArrayCollection|PersistentCollection $moderations */
                $moderations = $purchase->getModerations();
                /** @var Moderation $lastModeration */
                $lastModeration = $moderations->last();
                $lastModeration->setApproved(true);
                $lastModeration->setResolutionDate(new \DateTime());
            }

            if (isset($purchase_data['declined'])) {
                /** @var ArrayCollection|PersistentCollection $moderations */
                $moderations = $purchase->getModerations();
                /** @var Moderation $lastModeration */
                $lastModeration = $moderations->last();
                $lastModeration->setApproved(false);
                $lastModeration->setDescription('Контактная информация в заголовке закупки');
                $lastModeration->setResolutionDate(new \DateTime());
            }

            $this->entityManager->persist($purchase);

            $i++;
        }

        $this->entityManager->flush();
    }

    public function getOrder()
    {
        return 50;
    }
}