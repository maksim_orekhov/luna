<?php
namespace Application\Fixture;

use Application\Entity\Country;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\User;
use Application\Entity\Phone;
use Application\Entity\Email;
use Application\Entity\Role;
use Zend\Crypt\Password\Bcrypt;

class UserFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;
    const PASS = 'qwerty';
    const USERS = [
        'operator' => [
            'login' => 'operator',
            'name' => 'operator',
            'email' => 'operator@simple-technology.ru',
            'phone' => '79101000103',
            'type' => 'user',
        ],
        'admin' => [
            'login' => 'admin',
            'name' => 'admin',
            'email' => 'admin@simple-technology.ru',
            'phone' => '79101000104',
            'type' => 'user',
        ],
        'test' => [
            'login' => 'test',
            'name' => 'test',
            'email' => 'test@simple-technology.ru',
            'phone' => '79101000101',
            'type' => 'user',
        ],
        'test2' => [
            'login' => 'test2',
            'name' => 'test2',
            'email' => 'test2@simple-technology.ru',
            'phone' => '79101000102',
            'type' => 'shop',
        ],
        'test3' => [
            'login' => 'test3',
            'name' => 'test3',
            'email' => 'test3@simple-technology.ru',
            'phone' => '79101000103',
            'type' => 'shop',
        ],
        'test4' => [
            'login' => 'test4',
            'name' => 'test4',
            'email' => 'test4@simple-technology.ru',
            'phone' => '79101000104',
            'type' => 'shop',
        ],
        'test5' => [
            'login' => 'test5',
            'name' => 'test5',
            'email' => 'test5@simple-technology.ru',
            'phone' => '79101000105',
            'type' => 'shop',
        ],
        'test6' => [
            'login' => 'test6',
            'name' => 'test6',
            'email' => 'test6@simple-technology.ru',
            'phone' => '79101000106',
            'type' => 'shop',
        ],
        'test7' => [
            'login' => 'test7',
            'name' => 'test7',
            'email' => 'test7@simple-technology.ru',
            'phone' => '79101000107',
            'type' => 'shop',
        ],
        'test8' => [
            'login' => 'test8',
            'name' => 'test8',
            'email' => 'test8@simple-technology.ru',
            'phone' => '79101000108',
            'type' => 'shop',
        ],
        'test9' => [
            'login' => 'test9',
            'name' => 'test9',
            'email' => 'test9@simple-technology.ru',
            'phone' => '79101000109',
            'type' => 'shop',
        ],
        'test10' => [
            'login' => 'test10',
            'name' => 'test10',
            'email' => 'test10@simple-technology.ru',
            'phone' => '79101000110',
            'type' => 'shop',
        ],
    ];


    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');
        $country = $entityManager->getRepository(Country::class)
            ->findOneBy(array('code' => 'RU'));
        $roleVerified = $entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => 'Verified'));
        $roleOperator = $entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => 'Operator'));
        $roleAdministrator = $entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => 'Administrator'));

        foreach (self::USERS as $userPattern) {
            $phone = new Phone();
            $phone->setUserEnter($userPattern['phone']);
            $phone->setPhoneNumber($userPattern['phone']);
            $phone->setIsVerified(true);
            $phone->setCountry($country);
            $entityManager->persist($phone);

            $email = new Email();
            $email->setEmail($userPattern['email']);
            $email->setIsVerified(true);
            $entityManager->persist($email);

            $bcrypt = new Bcrypt();
            $passwordHash = $bcrypt->create(self::PASS);

            $user = new User();
            $user->setLogin($userPattern['login']);
            $user->setName($userPattern['name']);
            $user->setPassword($passwordHash);
            $user->setCreated(new \DateTime(date('Y-m-d H:i:s')));
            $user->setIsBanned(false);
            $user->setPhone($phone);
            $user->setEmail($email);
            $user->addRole($roleVerified);
            if ($userPattern['login'] === 'operator') {
                $user->addRole($roleOperator);
            }elseif ($userPattern['login'] === 'admin') {
                $user->addRole($roleAdministrator);
            }

            $entityManager->persist($user);
        }

        $entityManager->flush();
    }

    public function getOrder()
    {
        return 30;
    }
}