<?php
namespace Application\EventManager;

use Core\EventManager\AbstractEventProvider;

class ProductEventProvider extends AbstractEventProvider
{
    const EVENT_SUCCESS_CREATED_PRODUCT = 'EVENT_SUCCESS_CREATED_PRODUCT';
}

