<?php
namespace Application\Exception;

use Application\Exception\Code\UserExceptionCodeInterface;
use Application\Exception\Code\ShopExceptionCodeInterface;
use Application\Exception\Code\SmsExceptionCodeInterface;
use Application\Exception\Code\ProductExceptionCodeInterface;

/**
 * Interface BaseExceptionCodeInterface
 * @package Core\Exception
 */
interface ApplicationExceptionCodeInterface extends
    UserExceptionCodeInterface,
    ShopExceptionCodeInterface,
    SmsExceptionCodeInterface,
    ProductExceptionCodeInterface
{
    /**
     * Только для общих кодов, которые могут быть использованы везде!!!
     *
     * без префикса, для сообшений суфикс _MESSAGE
     */
    const INCORRECT_DATA_PROVIDED_TO_LISTENER = 'INCORRECT_DATA_PROVIDED_TO_LISTENER';
    const INCORRECT_DATA_PROVIDED_TO_LISTENER_MESSAGE = 'Incorrect data provided to listener';
}