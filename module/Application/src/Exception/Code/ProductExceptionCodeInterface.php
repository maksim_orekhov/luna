<?php
namespace Application\Exception\Code;

interface ProductExceptionCodeInterface
{
    /**
     * префикс ADVERT, для сообшений суфикс _MESSAGE
     */

    const PRODUCT_NOT_FOUND = 'PRODUCT_NOT_FOUND';
    const PRODUCT_NOT_FOUND_MESSAGE = 'Product not found.';

    const PRODUCT_ENTITY_NOT_PROVIDED = 'PRODUCT_ENTITY_NOT_PROVIDED';
    const PRODUCT_ENTITY_NOT_PROVIDED_MESSAGE = 'Product entity not provided.';

    const PRODUCT_GENERATION_UNIQUE_CODE_FAILED = 'PRODUCT_GENERATION_UNIQUE_CODE_FAILED';
    const PRODUCT_GENERATION_UNIQUE_CODE_FAILED_MESSAGE = 'Error generating unique code product.';
}
