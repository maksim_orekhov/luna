<?php
namespace Application\Exception\Code;

interface ShopExceptionCodeInterface
{
    /**
     * префикс SHOP, для сообшений суфикс _MESSAGE
     */
    const SHOP_LOGO_REMOVE_FAILED = 'SHOP_LOGO_REMOVE_FAILED';
    const SHOP_LOGO_REMOVE_FAILED_MESSAGE = 'Логотип магазина удалить не удалось.';

    const SHOP_LOGO_CREATE_FAILED = 'SHOP_LOGO_CREATE_FAILED';
    const SHOP_LOGO_CREATE_FAILED_MESSAGE = 'Логотип магазина добавить не удалось.';

    const SHOP_GENERATION_UNIQUE_SLUG_FAILED  = 'SHOP_GENERATION_UNIQUE_SLUG_FAILED';
    const SHOP_GENERATION_UNIQUE_SLUG_FAILED_MESSAGE  = 'Ошибка при создании уникального url для магазина.';
}
