<?php
namespace Application\Exception\Code;

interface UserExceptionCodeInterface
{
    /**
     * префикс USER, для сообшений суфикс _MESSAGE
     */
    const USER_BY_LOGIN_NOT_FOUND = 'USER_BY_LOGIN_NOT_FOUND';
    const USER_BY_LOGIN_NOT_FOUND_MESSAGE = 'User with such login not found';

    const USER_AVATAR_REMOVE_FAILED = 'USER_AVATAR_REMOVE_FAILED';
    const USER_AVATAR_REMOVE_FAILED_MESSAGE = 'Аватар пользователя удалить не удалось.';

    const USER_AVATAR_CREATE_FAILED = 'USER_AVATAR_CREATE_FAILED';
    const USER_AVATAR_CREATE_FAILED_MESSAGE = 'Аватар пользователя добавить не удалось.';
}
