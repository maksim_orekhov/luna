<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class UserForm
 * @package Application\Form
 */
class UserForm extends Form
{
    /**
     * @var string|null
     */
    private $current_name;

    /**
     * UserForm constructor.
     * @param string|null $current_name
     */
    public function __construct(string $current_name = null)
    {
        parent::__construct('user');

        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/profile');
        $this->setAttribute('class', 'user-change-form');
        $this->setAttribute('enctype', 'multipart/form-data');

        if (null !== $current_name) {
            $this->current_name = $current_name;
        }

        //add field
        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name' => 'name',
            'attributes' => [
                'id' => 'name',
                'placeholder'=>'Введите имя'
            ],
            'options' => [
                'label' => 'Имя',
            ],
        ]);

        $this->add([
            'type' => 'file',
            'name' => 'avatar',
            'options' => [
                'label' => 'Аватар',
                'class'=>'input-file-form',
                'accept'=>'image/*',
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Изменить',
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $login_min_length = 6;
        $login_max_length = 50;

        $name_validators = [
            [
                'name' => \Zend\Validator\StringLength::class,
                'options' => [
                    'min' => $login_min_length,
                    'max' => $login_max_length,
                    'messages' => [
                        'stringLengthTooShort' => 'Имя слишком короткое (минимум - '.$login_min_length.' симоволов).',
                        'stringLengthTooLong' => 'Имя слишком длинное (максимум - '.$login_max_length.' симоволов).'
                    ]
                ]
            ],
        ];
        if (null !== $this->current_name) {
            $name_validators[] = [
                'name' => \Application\Form\Validator\IdenticalToUserNameValidator::class,
                'options' => ['current_name' => $this->current_name],
            ];
        }

        $inputFilter->add([
            'name'     => 'name',
            'required' => false,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => $name_validators,
        ]);

        $inputFilter->add([
            'name'     => 'avatar',
            'required' => false,
            'filters' => [],
            'validators' => [
                [
                    'name' => \Zend\Validator\File\IsImage::class,
                    'options' => [],
                ],
                [
                    'name' => \Zend\Validator\File\Size::class,
                    'options' => [
                        'max' => '10MB'
                    ],
                ]
            ],
        ]);
    }
}