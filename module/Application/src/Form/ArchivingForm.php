<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;
use Zend\Validator\Digits;

/**
 * Class ArchivingForm
 * @package Application\Form
 */
class ArchivingForm extends Form
{
    /**
     * ArchiveForm constructor.
     */
    public function __construct()
    {
        parent::__construct('archive-form');

        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'archive-form');
        //add field
        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
//        $this->add([
//            'type'  => Element\Number::class,
//            'name' => 'id',
//        ]);

        $this->add([
            'type' => Element\Checkbox::class,
            'name' => 'archive',
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'сохранить',
            ],
        ]);
    }

    protected function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

//        $inputFilter->add([
//            'name'     => 'id',
//            'required' => true,
//            'filters'  => [
//                ['name' => \Zend\Filter\Digits::class]
//            ],
//            'validators' => [
//                ['name' => Digits::class]
//            ],
//        ]);

        $inputFilter->add([
            'name'     => 'archive',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\InArray::class,
                    'options' => [
                        'haystack' => [0,1],
                        'messages' => [
                            'notInArray' => 'Недопустимое значение!',
                        ],
                    ],
                ],
            ],
        ]);
    }
}