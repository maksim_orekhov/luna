<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;

/**
 * Class StartPublishForm
 * @package Application\Form
 */
class StartPublishForm extends Form
{
    /**
     * ArchiveForm constructor.
     */
    public function __construct()
    {
        parent::__construct('start-publish-form');

        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'start-publish-form');
        //add field
        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'сохранить',
            ],
        ]);
    }

    protected function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);
    }
}