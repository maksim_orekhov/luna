<?php
namespace Application\Form\FieldSet;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class AddressFieldSet extends Fieldset implements InputFilterProviderInterface
{
    public function __construct($name)
    {
        parent::__construct($name);

        $this->add([
            'type' => 'hidden',
            'name' => 'country',
            'attributes' => [
                'value' => 'Россия',
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'region',
            'attributes' => [
                'id' => 'address-region',
                'placeholder'=>'Введите регион'
            ],
            'options' => [
                'label' => 'Регион:',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'city',
            'attributes' => [
                'id' => 'address-city',
                'placeholder'=>'Введите город'
            ],
            'options' => [
                'label' => 'Город:',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'street',
            'attributes' => [
                'id' => 'address-street',
                'placeholder'=>'Введите улицу'
            ],
            'options' => [
                'label' => 'Улица:',
            ],
        ]);
        $this->add([
            'type'  => 'text',
            'name' => 'house',
            'attributes' => [
                'id' => 'address-house',
                'placeholder'=>'Введите дом'
            ],
            'options' => [
                'label' => 'Дом:',
            ],
        ]);

        $this->add([
            'type' => 'hidden',
            'name' => 'apartment',
        ]);

        $this->add([
            'type' => 'hidden',
            'name' => 'post_code',
        ]);

        $this->add([
            'type' => 'hidden',
            'name' => 'lat',
        ]);

        $this->add([
            'type' => 'hidden',
            'name' => 'lng',
        ]);
    }

    /**
     * @return array
     */
    public function getInputFilterSpecification():array
    {
        return [
            [
                'name'     => 'country',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => \Zend\Validator\InArray::class,
                        'options' => [
                            'haystack' => ['Россия'],
                            'messages' => [
                                'notInArray' => 'Недопустимое значение!',
                            ],
                        ],
                    ],
                ],
            ],
            [
                'name'     => 'region',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
                'validators' => [
                    [
                        'name' => \Zend\Validator\StringLength::class,
                        'options' => [
                            'min' => 5,
                            'max' => 128,
                            'messages' => [
                                'stringLengthInvalid' => 'Некорректное занчение для региона.',
                                'stringLengthTooShort' => 'Минимальная длина региона не должна быть менее 5 символов.',
                                'stringLengthTooLong' => 'Максимальная длина региона не должна превышать 128 символов.',
                            ],
                        ]
                    ],
                ],
            ],
            [
                'name'     => 'city',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
                'validators' => [
                    [
                        'name' => \Zend\Validator\StringLength::class,
                        'options' => [
                            'min' => 5,
                            'max' => 64,
                            'messages' => [
                                'stringLengthInvalid' => 'Некорректное занчение для города.',
                                'stringLengthTooShort' => 'Минимальная длина города не должна быть менее 5 символов.',
                                'stringLengthTooLong' => 'Максимальная длина города не должна превышать 64 символов.',
                            ],
                        ]
                    ],
                ],
            ],
            [
                'name'     => 'street',
                'required' => false,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
                'validators' => [
                    [
                        'name' => \Zend\Validator\StringLength::class,
                        'options' => [
                            'min' => 5,
                            'max' => 128,
                            'messages' => [
                                'stringLengthInvalid' => 'Некорректное занчение для улицы.',
                                'stringLengthTooShort' => 'Минимальная длина улицы не должна быть менее 5 символов.',
                                'stringLengthTooLong' => 'Максимальная длина улицы не должна превышать 128 символов.',
                            ],
                        ]
                    ],
                ],
            ],
            [
                'name'     => 'house',
                'required' => false,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
                'validators' => [
                    [
                        'name' => \Zend\Validator\StringLength::class,
                        'options' => [
                            'min' => 1,
                            'max' => 5,
                            'messages' => [
                                'stringLengthInvalid' => 'Некорректное занчение для дома.',
                                'stringLengthTooShort' => 'Минимальная длина номера дома не должна быть менее 1 символов.',
                                'stringLengthTooLong' => 'Максимальная длина номера дома не должна превышать 5 символов.',
                            ],
                        ]
                    ],
                ],
            ],
            [
                'name'     => 'apartment',
                'required' => false,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
                'validators' => [
                    [
                        'name' => \Zend\Validator\StringLength::class,
                        'options' => [
                            'min' => 1,
                            'max' => 6,
                            'messages' => [
                                'stringLengthInvalid' => 'Некорректное занчение для офиса/квартиры.',
                                'stringLengthTooShort' => 'Минимальная длина номера офиса/квартиры не должна быть менее 1 символов.',
                                'stringLengthTooLong' => 'Максимальная длина номера офиса/квартиры не должна превышать 6 символов.',
                            ],
                        ]
                    ],
                ],
            ],
            [
                'name'     => 'index',
                'required' => false,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
                'validators' => [
                    [
                        'name' => \Zend\Validator\StringLength::class,
                        'options' => [
                            'min' => 1,
                            'max' => 64,
                            'messages' => [
                                'stringLengthInvalid' => 'Некорректное занчение для индекса.',
                                'stringLengthTooShort' => 'Минимальная длина индекса не должна быть менее 1 символов.',
                                'stringLengthTooLong' => 'Максимальная длина индекса не должна превышать 64 символов.',
                            ],
                        ]
                    ],
                ],
            ],
            [
                'name'     => 'lat',
                'required' => false,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
                'validators' => [
                    [
                        'name' => \Zend\Validator\StringLength::class,
                        'options' => [
                            'min' => 1,
                            'max' => 128,
                            'messages' => [
                                'stringLengthInvalid' => 'Некорректное занчение для координат.',
                                'stringLengthTooShort' => 'Минимальная длина координат не должна быть менее 1 символов.',
                                'stringLengthTooLong' => 'Максимальная длина координат не должна превышать 128 символов.',
                            ],
                        ]
                    ],
                ],
            ],
            [
                'name'     => 'lng',
                'required' => false,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
                'validators' => [
                    [
                        'name' => \Zend\Validator\StringLength::class,
                        'options' => [
                            'min' => 1,
                            'max' => 128,
                            'messages' => [
                                'stringLengthInvalid' => 'Некорректное занчение для координат.',
                                'stringLengthTooShort' => 'Минимальная длина координат не должна быть менее 1 символов.',
                                'stringLengthTooLong' => 'Максимальная длина координат не должна превышать 128 символов.',
                            ],
                        ]
                    ],
                ],
            ]
        ];
    }
}