<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;

/**
 * Class ProductImageUploadForm
 * @package Application\Form
 */
class ProductImageUploadForm extends Form
{
    /**
     * ProductImageUploadForm constructor.
     */
    public function __construct()
    {
        parent::__construct('product-image-upload-form');

        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/product/image/upload');
        $this->setAttribute('class', 'product-image-upload-form');
        $this->setAttribute('enctype', 'multipart/form-data');
        //add field
        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $this->add([
            'type'  => Element\File::class,
            'name' => 'image',
            'options' => [
                'label' => 'фото:',
            ],
            'attributes' => [
                'id' => 'product-image',
                'multiple'=> false
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'сохранить',
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'image',
            'required' => true,
            'filters' => [],
            'validators' => [
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => [
                        'messages' => [
                            'isEmpty' => 'Пожалуйста добавьте хотя бы одно изображение для объявления.',
                        ],
                    ],
                ],
                [
                    'name' => \Zend\Validator\File\IsImage::class,
                    'options' => [
                        'mimeType' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'messages' => [
                            'fileIsImageFalseType' => 'Изображение должно быть в формате JPG или PNG.',
                            'fileIsImageNotDetected' => 'Не удалось обнаружить MIME-TYPE в файле',
                            'fileIsImageNotReadable' => 'Файл не читается или не существует.',
                        ],
                    ],
                ],
                [
                    'name' => \Zend\Validator\File\Size::class,
                    'options' => [
                        'min' => '10kB',
                        'max' => '10MB',
                        'messages' => [
                            'fileSizeTooBig' => 'Изображение не должно превышать размер 10MB',
                            'fileSizeTooSmall' => 'Изображение не должно быть менее 10kB',
                            'fileSizeNotFound' => 'Файл не читается или не существует.',
                        ],
                    ],
                ]
            ],
        ]);
    }
}