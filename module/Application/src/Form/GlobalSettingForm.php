<?php
namespace Application\Form;

use Application\Service\SettingManager;
use Zend\InputFilter\InputFilter;
use Zend\Form\Form;

class GlobalSettingForm extends Form
{
    /**
     * @var int|null|string
     */
    private $globalSettings;

    /**
     * GlobalSettingForm constructor.
     * @param array $globalSettings
     */
    public function __construct($globalSettings)
    {
        $this->globalSettings = $globalSettings;

        // Define form name
        parent::__construct('global-setting');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name' => 'fee_percentage',
            'attributes' => [
                'class'=>'form-control',
                'value' => $this->globalSettings['fee_percentage']
            ],
            'options' => [
                'label' => 'Процент гонорара',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'min_delivery_period',
            'attributes' => [
                'class'=>'form-control',
                'value' => $this->globalSettings['min_delivery_period']
            ],
            'options' => [
                'label' => 'Минимальный срок поставки',
            ],
        ]);
        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);
        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'class'=>'btn btn-primary',
                'value' => 'Сохранить',
                'id' => 'submit',
            ]
        ]);
    }

    /**
     * Этот метод создает фильтр входных данных (используется для фильтрации/валидации).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name' => 'fee_percentage',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\Digits::class,
                    'options' => [],
                ],
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => ['zero'],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'min_delivery_period',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\Digits::class,
                    'options' => [],
                ],
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => ['zero'],
                ],
            ],
        ]);

    }
}