<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;

/**
 * Class ProductImageUploadForm
 * @package Application\Form
 */
class ProductImageModifyForm extends Form
{
    /**
     * @var array
     */
    private $images;
    /**
     * @var array
     */
    private $haystack_images;

    /**
     * ProductImageUploadForm constructor.
     * @param array $saved_images
     */
    public function __construct(array $saved_images = [])
    {
        parent::__construct('product-image-upload-form');

        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/product/image/modify');
        $this->setAttribute('class', 'product-image-upload-form');
        $this->setAttribute('enctype', 'multipart/form-data');
        //add field
        $this->setHaystackImages($saved_images);
        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $this->add([
            'type' => Element\Select::class,
            'name' => 'image',
            'attributes' => [
                'id' => 'image',
                'placeholder' => 'Выберите фото',
                'multiple' => false
            ],
            'options' => [
                'label' => 'Фото:',
                'empty_option' => 'Выберите фото',
                'value_options' => $this->images,
                'disable_inarray_validator' => true,
            ]
        ]);

        $this->add([
            'type' => Element\Select::class,
            'name' => 'rotate',
            'attributes' => [
                'id' => 'rotate',
                'placeholder' => 'Поворот',
                'multiple' => false
            ],
            'options' => [
                'label' => 'Поворот:',
                'empty_option' => 'Выберите угол поворота',
                'value_options' => [
                    '0' => '0 градусов',
                    '90' => '90 градусов',
                    '180' => '180 градусов',
                    '270' => '270 градусов',
                ],
                'disable_inarray_validator' => true,
            ]
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'сохранить',
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'image',
            'required' => true,
            'filters' => [],
            'validators' => [
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => [
                        'messages' => [
                            'isEmpty' => 'Пожалуйста добавьте хотя бы одно изображение для объявления.',
                        ],
                    ],
                ],
                [
                    'name' => \Zend\Validator\File\IsImage::class,
                    'options' => [
                        'mimeType' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'messages' => [
                            'fileIsImageFalseType' => 'Изображение должно быть в формате JPG или PNG.',
                            'fileIsImageNotDetected' => 'Не удалось обнаружить MIME-TYPE в файле',
                            'fileIsImageNotReadable' => 'Файл не читается или не существует.',
                        ],
                    ],
                ],
                [
                    'name' => \Zend\Validator\File\Size::class,
                    'options' => [
                        'min' => '10kB',
                        'max' => '10MB',
                        'messages' => [
                            'fileSizeTooBig' => 'Изображение не должно превышать размер 10MB',
                            'fileSizeTooSmall' => 'Изображение не должно быть менее 10kB',
                            'fileSizeNotFound' => 'Файл не читается или не существует.',
                        ],
                    ],
                ]
            ],
        ]);

        $inputFilter->add([
            'name'     => 'rotate',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => [
                        'messages' => [
                            'isEmpty' => 'Пожалуйста выберите угол поворота.',
                        ],
                    ],
                ],
                [
                    'name' => \Zend\Validator\InArray::class,
                    'options' => [
                        'haystack' => ['0','90','180','270'],
                        'messages' => [
                            'notInArray' => 'Пожалуйста выберите угол поворота.',
                        ],
                    ],
                ],
            ],
        ]);
    }

    private function setHaystackImages(array $saved_images = [])
    {
        $this->images = [];
        $this->haystack_images = [];
        foreach ($saved_images as $saved_image)
        {
            $this->haystack_images[] = $saved_image['id'];
            $this->images[$saved_image['id']] = $saved_image['name'];
        }
    }
}