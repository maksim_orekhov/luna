<?php
namespace Application\Form;

use Application\Entity\Category;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;
use Application\Form\FieldSet\AddressFieldSet;

/**
 * Class PurchaseForm
 * @package Application\Form
 */
class PurchaseForm extends Form
{
    /**
     * @var array
     */
    private $categories;
    /**
     * @var array
     */
    private $categories_haystack;

    /**
     * AdvertForm constructor.
     * @param array $categories
     */
    public function __construct(array $categories)
    {
        parent::__construct('advert');

        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/purchase/create');
        $this->setAttribute('class', 'purchase-form');
        $this->setAttribute('enctype', 'multipart/form-data');
        //add field
        $this->prepareCategory($categories);
        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $this->add([
            'type' => Element\Select::class,
            'name' => 'category',
            'attributes' => [
                'id' => 'advert_category',
                'placeholder' => 'Выберите категорию',
                'multiple' => false
            ],
            'options' => [
                'label' => 'Категория:',
                'empty_option' => 'Выберите категорию',
                'value_options' => $this->categories,
                'disable_inarray_validator' => true,
            ]
        ]);

        $this->add([
            'type'  => Element\Text::class,
            'name' => 'name',
            'attributes' => [
                'id' => 'name',
                'placeholder'=>'Введите название'
            ],
            'options' => [
                'label' => 'Название:',
            ],
        ]);

        $this->add([
            'type'  => Element\Number::class,
            'name' => 'price',
            'attributes' => [
                'id' => 'price',
                'placeholder'=>'Укажите цену'
            ],
            'options' => [
                'label' => 'Цена:',
            ],
        ]);

        $this->add([
            'type'  => Element\Number::class,
            'name' => 'quantity',
            'attributes' => [
                'id' => 'quantity',
                'placeholder'=>'Укажите количество'
            ],
            'options' => [
                'label' => 'Количество:',
            ],
        ]);

        $this->add([
            'type'  => Element\Text::class,
            'name' => 'measure',
            'attributes' => [
                'id' => 'measure',
                'placeholder'=>'Укажите единицу измерения'
            ],
            'options' => [
                'label' => 'Единица измерения:',
            ],
        ]);

        $this->add([
            'type' => Element\Checkbox::class,
            'name' => 'safe_deal',
            'attributes' => [
                'id' => 'advert-safe-deal',
            ],
            'options' => [
                'label' => 'Безопастная сделка:',
                'use_hidden_element' => true,
                'checked_value' => 1,
                'unchecked_value' => 0
            ],
        ]);

        $this->add([
            'type'  => Element\Textarea::class,
            'name' => 'description',
            'attributes' => [
                'id' => 'advert-description',
                'placeholder'=>'Введите описание'
            ],
            'options' => [
                'label' => 'Описание:',
            ],
        ]);

        $this->add([
            'type'  => Element\File::class,
            'name' => 'files',
            'options' => [
                'label' => 'Файлы:',
            ],
            'attributes' => [
                'id' => 'advert-files',
                'multiple'=> true
            ],
        ]);

        $this->add([
            'name' => 'address',
            'type' => AddressFieldSet::class,
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'сохранить',
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'category',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => [
                        'zero',
                        'messages' => [
                            'isEmpty' => 'Пожалуйста выберите категорию объявления.',
                        ],
                    ],
                ],
                [
                    'name' => \Zend\Validator\InArray::class,
                    'options' => [
                        'haystack' => $this->categories_haystack,
                        'messages' => [
                            'notInArray' => 'Пожалуйста выберите категорию объявления.',
                        ],
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'name',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => [
                        'messages' => [
                            'isEmpty' => 'Пожалуйста введите название для объявления.',
                        ],
                    ],
                ],
                [
                    'name' => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 3,
                        'max' => 200,
                        'messages' => [
                            'stringLengthInvalid' => 'Некорректное название объявления.',
                            'stringLengthTooShort' => 'Минимальная длина названия не должна быть менее 3 символов.',
                            'stringLengthTooLong' => 'Максимальная длина названия не должна превышать 200 символов.',
                        ],
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'price',
            'required' => false,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\Digits::class,
                    'options' => [
                        'messages' => [
                            'notDigits' => 'Некорректная сумма.',
                            'digitsStringEmpty' => 'Некорректная сумма.',
                            'digitsInvalid' => 'Некорректная сумма.',
                        ],
                    ],
                ],
                [
                    'name' => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 0,
                        'max' => 8,
                        'messages' => [
                            'stringLengthInvalid' => 'Некорректная сумма.',
                            'stringLengthTooShort' => 'Некорректная сумма.',
                            'stringLengthTooLong' => 'Максимальная сумма не должна превышать 8 символов.',
                        ],
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'quantity',
            'required' => false,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\Digits::class,
                    'options' => [
                        'messages' => [
                            'notDigits' => 'Некорректное количество.',
                            'digitsStringEmpty' => 'Некорректное количество.',
                            'digitsInvalid' => 'Некорректное количество.',
                        ],
                    ],
                ],
                [
                    'name' => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 0,
                        'max' => 8,
                        'messages' => [
                            'stringLengthInvalid' => 'Некорректное количество.',
                            'stringLengthTooShort' => 'Некорректное количество.',
                            'stringLengthTooLong' => 'Количество не должно превышать 8 символов.',
                        ],
                    ]
                ],
            ],
        ]);


        $inputFilter->add([
            'name'     => 'measure',
            'required' => false,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 1,
                        'max' => 50,
                        'messages' => [
                            'stringLengthInvalid' => 'Некорректное занчение для единицы измерения.',
                            'stringLengthTooShort' => 'Минимальная длина не должна быть менее 1 символов.',
                            'stringLengthTooLong' => 'Максимальная длина не должна превышать 50 символов.',
                        ],
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'safe_deal',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\Digits::class,
                    'options' => [
                        'messages' => [
                            'notDigits' => 'Некорректное значение безопасной сделки.',
                            'digitsStringEmpty' => 'Некорректное значение безопасной сделки.',
                            'digitsInvalid' => 'Некорректное значение безопасной сделки.',
                        ],
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'description',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => [
                        'messages' => [
                            'isEmpty' => 'Пожалуйста добавьте описание для объявления.',
                        ],
                    ],
                ],
                [
                    'name' => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 5,
                        'max' => 3000,
                        'messages' => [
                            'stringLengthInvalid' => 'Некорректное занчение описания.',
                            'stringLengthTooShort' => 'Минимальная длина описания не должна быть менее 5 символов.',
                            'stringLengthTooLong' => 'Максимальная длина описания не должна превышать 3000 символов.',
                        ],
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'files',
            'required' => false,
            'filters' => [],
            'validators' => [
                [
                    'name' => \Zend\Validator\File\Size::class,
                    'options' => [
                        'min' => '10B',
                        'max' => '10MB',
                        'messages' => [
                            'fileSizeTooBig' => 'Изображение не должно превышать размер 10MB',
                            'fileSizeTooSmall' => 'Изображение не должно быть менее 10B',
                            'fileSizeNotFound' => 'Файл не читается или не существует.',
                        ],
                    ],
                ]
            ],
        ]);
    }

    /**
     * @param $categories
     */
    private function prepareCategory($categories)
    {
        $this->categories = [];
        $this->categories_haystack = [];
        /** @var Category $category */
        foreach ($categories as $category)
        {
            $sub_categories = $category->getChildren();
            $this->categories[$category->getId()]['label'] = $category->getName();
            /** @var Category $sub_category */
            foreach ($sub_categories as $sub_category)
            {
                $this->categories_haystack[] = $sub_category->getId();
                $this->categories[$category->getId()]['options'][$sub_category->getId()] = $sub_category->getName();
            }
        }
    }
}