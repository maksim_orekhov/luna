<?php
namespace Application\Form;

use Application\Entity\Category;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;
use Application\Form\FieldSet\AddressFieldSet;

/**
 * Class FabricatorCustomForm
 * @package Application\Form
 */
class FabricatorCustomForm extends Form
{
    /**
     * @var array
     */
    private $categories;
    /**
     * @var array
     */
    private $categories_haystack;

    /**
     * AdvertForm constructor.
     * @param array $categories
     */
    public function __construct(array $categories)
    {
        parent::__construct('fabricator-custom-form');

        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/fabricator-custom-create');
        $this->setAttribute('class', 'fabricator-custom-form');
        $this->setAttribute('enctype', 'multipart/form-data');
        //add field
        $this->prepareCategory($categories);
        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name' => 'ident',
            'attributes' => [
                'id' => 'ident',
                'placeholder'=>'Введите логин или email пользователя'
            ],
            'options' => [
                'label' => 'Введите логин или email пользователя:',
            ],
        ]);

        $this->add([
            'type' => Element\Select::class,
            'name' => 'category',
            'attributes' => [
                'id' => 'categories',
                'placeholder' => 'Выберите категории',
                'multiple' => true
            ],
            'options' => [
                'label' => 'Категория:',
                'value_options' => $this->categories,
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'name',
            'attributes' => [
                'id' => 'name',
                'placeholder'=>'Введите название компании'
            ],
            'options' => [
                'label' => 'Введите название компании:',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'legal_address',
            'attributes' => [
                'id'=>'adress',
                'placeholder'=>'Введите юридический адрес'
            ],
            'options' => [
                'label' => 'Введите юридический адрес:',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'inn',
            'attributes' => [
                'id'=>'number-inn',
                'placeholder'=>'Введите ИНН'
            ],
            'options' => [
                'label' => 'Введите ИНН:',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'kpp',
            'attributes' => [
                'id'=>'number-kpp',
                'placeholder'=>'Введите КПП'
            ],
            'options' => [
                'label' => 'Введите КПП:',
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'сохранить',
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'category',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => [
                        'zero',
                        'messages' => [
                            'isEmpty' => 'Пожалуйста выберите категорию объявления.',
                        ],
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'ident',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => [
                        'messages' => [
                            'isEmpty' => 'Пожалуйста введите логин или email пользователя.',
                        ],
                    ],
                ],
                [
                    'name' => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 3,
                        'max' => 50,
                        'messages' => [
                            'stringLengthInvalid' => 'Некорректный логин или email пользователя.',
                            'stringLengthTooShort' => 'Минимальная длина логин или email не должна быть менее 3 символов.',
                            'stringLengthTooLong' => 'Максимальная длина логин или email не должна превышать 50 символов.',
                        ],
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'name',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => [
                        'messages' => [
                            'isEmpty' => 'Пожалуйста введите название компании.',
                        ],
                    ],
                ],
                [
                    'name' => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 3,
                        'max' => 128,
                        'messages' => [
                            'stringLengthInvalid' => 'Некорректный название компании.',
                            'stringLengthTooShort' => 'Минимальная длина названия не должна быть менее 3 символов.',
                            'stringLengthTooLong' => 'Максимальная длина названия не должна превышать 128 символов.',
                        ],
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'legal_address',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => [
                        'messages' => [
                            'isEmpty' => 'Пожалуйста введите юридический адрес.',
                        ],
                    ],
                ],
                [
                    'name' => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 3,
                        'max' => 255,
                        'messages' => [
                            'stringLengthInvalid' => 'Некорректный юридический адрес.',
                            'stringLengthTooShort' => 'Минимальная длина юридического адреса не должна быть менее 3 символов.',
                            'stringLengthTooLong' => 'Максимальная длина юридического адреса не должна превышать 255 символов.',
                        ],
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'inn',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            //валидный инн для теста юр лиц: 4217030520
            //валидный инн для теста физ лиц: 683103646744
            'validators' => [
                ['name' => \Application\Form\Validator\InnValidator::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'kpp',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => [
                        'messages' => [
                            'isEmpty' => 'Пожалуйста введите КПП.',
                        ],
                    ],
                ],
                [
                    'name' => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 5,
                        'max' => 9,
                        'messages' => [
                            'stringLengthInvalid' => 'Некорректный КПП.',
                            'stringLengthTooShort' => 'Минимальная длина КПП не должна быть менее 5 символов.',
                            'stringLengthTooLong' => 'Максимальная длина КПП не должна превышать 9 символов.',
                        ],
                    ]
                ],
            ],
        ]);
    }

    /**
     * @param $categories
     */
    private function prepareCategory($categories)
    {
        $this->categories = [];
        $this->categories_haystack = [];
        /** @var Category $category */
        foreach ($categories as $category)
        {
            $sub_categories = $category->getChildren();
            $this->categories[$category->getId()]['label'] = $category->getName();
            /** @var Category $sub_category */
            foreach ($sub_categories as $sub_category)
            {
                $this->categories_haystack[] = $sub_category->getId();
                $this->categories[$category->getId()]['options'][$sub_category->getId()] = $sub_category->getName();
            }
        }
    }
}