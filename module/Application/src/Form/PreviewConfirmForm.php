<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class PreviewConfirmForm
 * @package Application\Form
 */
class PreviewConfirmForm extends Form
{
    /**
     * PreviewAdvertForm constructor.
     */
    public function __construct()
    {
        parent::__construct('preview-confirm-form');

        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'preview-confirm-form');

        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {

        $this->add([
            'type' => 'hidden',
            'name' => 'preview_confirm',
            'attributes' => [
                'value' => 1,
            ]
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'сохранить',
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'preview_confirm',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\InArray::class,
                    'options' => [
                        'haystack' => [1],
                        'messages' => [
                            'notInArray' => 'Недопустимое значение!',
                        ],
                    ],
                ],
            ],
        ]);
    }

}