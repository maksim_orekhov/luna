<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;

/**
 * Class ProductModerationApprovalForm
 * @package Application\Form
 */
class ProductModerationForm extends Form
{
    /**
     * @var string
     */
    private $type;

    /**
     * ProductModerationApprovalForm constructor.
     * @param string $type
     */
    public function __construct(string $type = null)
    {
        parent::__construct('product-moderation-form');

        $this->type = $type;

        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'product-moderation-form');
        //add field
        $this->addElements();
        $this->addInputFilter();

        $is_approved = $this->type === 'decline' ? 0 : 1;
        $this->get('approved')->setValue($is_approved);
    }

    protected function addElements()
    {
        if ($this->type === 'decline') {
            $this->add([
                'type' => Element\Textarea::class,
                'name' => 'description',
                'attributes' => [
                    'id' => 'description',
                    'placeholder'=>'Причина'
                ],
            ]);
        }

        $this->add([
            'type' => 'hidden',
            'name' => 'approved',
            'attributes' => [
                'id' => 'product-moderation',
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'сохранить',
            ],
        ]);
    }

    protected function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        if ($this->type === 'decline') {
            $inputFilter->add([
                'name'     => 'description',
                'required' => false,
                'filters'  => [
                    ['name' => \Zend\Filter\StringTrim::class],
                    ['name' => \Zend\Filter\StripTags::class]
                ],
                'validators' => [
                    [
                        'name' => \Zend\Validator\NotEmpty::class,
                        'options' => [
                            'messages' => [
                                'isEmpty' => 'Пожалуйста, укажите причину отклонения.',
                            ],
                        ],
                    ],
                    [
                        'name' => \Zend\Validator\StringLength::class,
                        'options' => [
                            'min' => 5,
                            'max' => 3000,
                            'messages' => [
                                'stringLengthInvalid' => 'Некорректное занчение описания.',
                                'stringLengthTooShort' => 'Минимальная длина описания не должна быть менее 5 символов.',
                                'stringLengthTooLong' => 'Максимальная длина описания не должна превышать 3000 символов.',
                            ],
                        ]
                    ],
                ],
            ]);
        }

        $inputFilter->add([
            'name'     => 'approved',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\InArray::class,
                    'options' => [
                        'haystack' => [0,1],
                        'messages' => [
                            'notInArray' => 'Недопустимое значение!',
                        ],
                    ],
                ],
            ],
        ]);
    }
}