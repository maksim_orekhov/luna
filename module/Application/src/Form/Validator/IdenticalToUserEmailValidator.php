<?php
namespace Application\Form\Validator;

use Zend\Validator\AbstractValidator;

class IdenticalToUserEmailValidator extends AbstractValidator
{
    const INVALID  = 'Invalid';

    protected $options = [
        'email' => null,
    ];

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::INVALID  => 'Identical to user email',
    ];

    public function __construct($options = [])
    {
        parent::__construct($options);
    }

    /**
     * @param mixed $email
     * @return bool
     */
    public function isValid($email)
    {
        if ( $this->options['email'] !== null && strtolower($this->options['email']) === strtolower(trim($email))){
            $this->error(self::INVALID);

            return false;
        }

        return true;
    }
}