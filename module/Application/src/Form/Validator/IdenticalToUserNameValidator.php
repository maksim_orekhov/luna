<?php
namespace Application\Form\Validator;

use Zend\Validator\AbstractValidator;

/**
 * Class IdenticalToUserNameValidator
 * @package Application\Form\Validator
 */
class IdenticalToUserNameValidator extends AbstractValidator
{
    const INVALID  = 'name';

    protected $options = [
        'current_name' => null,
    ];

    /**
     * @var array
     */
    protected $messageTemplates = [
        #self::INVALID  => 'Provided name is identical to current name',
        self::INVALID  => 'Введенное имя совпадает с именем, указанным в профайле',
    ];

    public function __construct($options = [])
    {
        parent::__construct($options);
    }

    /**
     * @param mixed $email
     * @return bool
     */
    public function isValid($name)
    {
        if ($this->options['current_name'] !== null && strtolower($this->options['current_name']) === strtolower(trim($name))){
            $this->error(self::INVALID);

            return false;
        }

        return true;
    }
}