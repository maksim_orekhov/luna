<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;

/**
 * Class ShopCustomForm
 * @package Application\Form
 */
class ShopCustomForm extends Form
{
    /**
     * ShopCustomForm constructor.
     */
    public function __construct()
    {
        parent::__construct('shop-custom-form');

        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/shop-custom-create');
        $this->setAttribute('class', 'shop-custom-form');
        //add field
        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name' => 'ident',
            'attributes' => [
                'id' => 'ident',
                'placeholder'=>'Введите логин или email пользователя'
            ],
            'options' => [
                'label' => 'Введите логин или email пользователя:',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'name',
            'attributes' => [
                'id' => 'name',
                'placeholder'=>'Введите название магазина'
            ],
            'options' => [
                'label' => 'Введите название магазина:',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'legal_name',
            'attributes' => [
                'id'=>'legal_name',
                'placeholder'=>'Введите название юридического лица'
            ],
            'options' => [
                'label' => 'Введите название юридического лица:',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'legal_address',
            'attributes' => [
                'id'=>'adress',
                'placeholder'=>'Введите юридический адрес'
            ],
            'options' => [
                'label' => 'Введите юридический адрес:',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'postal_address',
            'attributes' => [
                'id'=>'postal_address',
                'placeholder'=>'Введите почтовый адрес'
            ],
            'options' => [
                'label' => 'Введите почтовый адрес:',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'inn',
            'attributes' => [
                'id'=>'number-inn',
                'placeholder'=>'Введите ИНН'
            ],
            'options' => [
                'label' => 'Введите ИНН:',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'ogrn',
            'attributes' => [
                'id'=>'number-ogrn',
                'placeholder'=>'Введите ОГРН'
            ],
            'options' => [
                'label' => 'Введите ОГРН:',
            ],
        ]);

        $this->add([
            'type'  => Element\Textarea::class,
            'name' => 'description',
            'attributes' => [
                'id' => 'description',
                'placeholder'=>'Введите описание магазина'
            ],
            'options' => [
                'label' => 'Введите описание магазина:',
            ],
        ]);

        $this->add([
            'type'  => Element\Textarea::class,
            'name' => 'phones',
            'attributes' => [
                'id' => 'phones',
                'placeholder'=>'Введите номера телефонов (по одному в строке или через запятую)'
            ],
            'options' => [
                'label' => 'Введите номера телефонов (по одному в строке или через запятую):',
            ],
        ]);

        $this->add([
            'type'  => Element\Textarea::class,
            'name' => 'work_schedule',
            'attributes' => [
                'id' => 'work_schedule',
                'placeholder'=>'Введите график работы (по одной строке или через запятую)'
            ],
            'options' => [
                'label' => 'Введите график работы (по одной строке или через запятую):',
            ],
        ]);

        $this->add([
            'type'  => Element\Textarea::class,
            'name' => 'web_links',
            'attributes' => [
                'id' => 'web_links',
                'placeholder'=>'Введите ссылки на сайты или социальные сети (по одной строке или через запятую)'
            ],
            'options' => [
                'label' => 'Введите ссылки на сайты или социальные сети (по одной строке или через запятую):',
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'сохранить',
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'ident',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => [
                        'messages' => [
                            'isEmpty' => 'Пожалуйста введите логин или email пользователя.',
                        ],
                    ],
                ],
                [
                    'name' => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 3,
                        'max' => 50,
                        'messages' => [
                            'stringLengthInvalid' => 'Некорректный логин или email пользователя.',
                            'stringLengthTooShort' => 'Минимальная длина логин или email не должна быть менее 3 символов.',
                            'stringLengthTooLong' => 'Максимальная длина логин или email не должна превышать 50 символов.',
                        ],
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'name',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => [
                        'messages' => [
                            'isEmpty' => 'Пожалуйста введите название магазина.',
                        ],
                    ],
                ],
                [
                    'name' => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 3,
                        'max' => 128,
                        'messages' => [
                            'stringLengthInvalid' => 'Некорректный название магазина.',
                            'stringLengthTooShort' => 'Минимальная длина названия не должна быть менее 3 символов.',
                            'stringLengthTooLong' => 'Максимальная длина названия не должна превышать 128 символов.',
                        ],
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'legal_address',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => [
                        'messages' => [
                            'isEmpty' => 'Пожалуйста введите юридический адрес.',
                        ],
                    ],
                ],
                [
                    'name' => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 3,
                        'max' => 255,
                        'messages' => [
                            'stringLengthInvalid' => 'Некорректный юридический адрес.',
                            'stringLengthTooShort' => 'Минимальная длина юридического адреса не должна быть менее 3 символов.',
                            'stringLengthTooLong' => 'Максимальная длина юридического адреса не должна превышать 255 символов.',
                        ],
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'legal_address',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => [
                        'messages' => [
                            'isEmpty' => 'Пожалуйста введите почтовый адрес.',
                        ],
                    ],
                ],
                [
                    'name' => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 3,
                        'max' => 255,
                        'messages' => [
                            'stringLengthInvalid' => 'Некорректный почтовый адрес.',
                            'stringLengthTooShort' => 'Минимальная длина почтового адреса не должна быть менее 3 символов.',
                            'stringLengthTooLong' => 'Максимальная длина почтового адреса не должна превышать 255 символов.',
                        ],
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'inn',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            //валидный инн для теста юр лиц: 4217030520
            //валидный инн для теста физ лиц: 683103646744
            'validators' => [
                ['name' => \Application\Form\Validator\InnValidator::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'ogrn',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => [
                        'messages' => [
                            'isEmpty' => 'Пожалуйста введите ОГРН.',
                        ],
                    ],
                ],
                [
                    'name' => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 5,
                        'max' => 15,
                        'messages' => [
                            'stringLengthInvalid' => 'Некорректный ОГРН.',
                            'stringLengthTooShort' => 'Минимальная длина ОГРН не должна быть менее 5 символов.',
                            'stringLengthTooLong' => 'Максимальная длина ОГРН не должна превышать 15 символов.',
                        ],
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'description',
            'required' => false,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => [
                        'messages' => [
                            'isEmpty' => 'Пожалуйста введите описание магазина.',
                        ],
                    ],
                ],
                [
                    'name' => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 5,
                        'max' => 3000,
                        'messages' => [
                            'stringLengthInvalid' => 'Некорректные описание магазина.',
                            'stringLengthTooShort' => 'Минимальная длина описания не должна быть менее 5 символов.',
                            'stringLengthTooLong' => 'Максимальная длина описания не должна превышать 3000 символов.',
                        ],
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'phones',
            'required' => false,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => [
                        'messages' => [
                            'isEmpty' => 'Пожалуйста введите номера телефонов.',
                        ],
                    ],
                ],
                [
                    'name' => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 5,
                        'max' => 3000,
                        'messages' => [
                            'stringLengthInvalid' => 'Некорректные номера телефонов.',
                            'stringLengthTooShort' => 'Минимальная длина номеров не должна быть менее 5 символов.',
                            'stringLengthTooLong' => 'Максимальная длина номеров не должна превышать 3000 символов.',
                        ],
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'work_schedule',
            'required' => false,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => [
                        'messages' => [
                            'isEmpty' => 'Пожалуйста введите график работы.',
                        ],
                    ],
                ],
                [
                    'name' => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 5,
                        'max' => 3000,
                        'messages' => [
                            'stringLengthInvalid' => 'Некорректный график работы.',
                            'stringLengthTooShort' => 'Минимальная длина графика не должна быть менее 5 символов.',
                            'stringLengthTooLong' => 'Максимальная длина графика не должна превышать 3000 символов.',
                        ],
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'web_links',
            'required' => false,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => [
                        'messages' => [
                            'isEmpty' => 'Пожалуйста введите ссылки на сайты или социальные сети.',
                        ],
                    ],
                ],
                [
                    'name' => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 5,
                        'max' => 3000,
                        'messages' => [
                            'stringLengthInvalid' => 'Некорректные ссылки на сайты или социальные сети.',
                            'stringLengthTooShort' => 'Минимальная длина ссылкок не должна быть менее 5 символов.',
                            'stringLengthTooLong' => 'Максимальная длина ссылкок не должна превышать 3000 символов.',
                        ],
                    ]
                ],
            ],
        ]);
    }
}