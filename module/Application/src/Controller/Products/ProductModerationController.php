<?php
namespace Application\Controller\Products;

use Application\Entity\Product;
use Application\Entity\Moderation;
use Application\Form\ProductModerationForm;
use Application\Service\ProductManager;
use Application\Service\ProductModerationManager;
use Application\Service\UserManager;
use Core\Controller\AbstractRestfulController;
use Core\Controller\Plugin\Message\MessageJsonModelTrait;
use Core\Exception\LogicException;
use Core\Service\Base\BaseAuthManager;
use Core\Service\TwigViewModel;
use ModuleRbac\Service\RbacManager;

/**
 * Class ProductModerationController
 * @package Application\Controller\Products
 */
class ProductModerationController extends AbstractRestfulController
{
    use MessageJsonModelTrait;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * User manager
     * @var UserManager
     */
    private $userManager;

    /**
     * @var ProductModerationManager
     */
    private $productModerationManager;

    /**
     * @var ProductManager
     */
    private $productManager;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * ProductModerationApprovalController constructor.
     * @param BaseAuthManager $baseAuthManager
     * @param UserManager $userManager
     * @param ProductModerationManager $productModerationManager
     * @param ProductManager $productManager
     * @param RbacManager $rbacManager
     */
    public function __construct(BaseAuthManager $baseAuthManager, UserManager $userManager,
                                ProductModerationManager $productModerationManager, ProductManager $productManager,
                                RbacManager $rbacManager)
    {
        $this->baseAuthManager = $baseAuthManager;
        $this->userManager = $userManager;
        $this->productModerationManager = $productModerationManager;
        $this->productManager = $productManager;
        $this->rbacManager = $rbacManager;
    }

    /**
     * @return TwigViewModel|mixed|\Zend\Http\Response|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     */
    public function getWaitingModerationListAction()
    {
        $this->setOnlyAjax();
        $this->message()->setJsonStrategy(true);
        $page = (int)$this->params()->fromQuery('page', 1);
        $per_page = (int)$this->params()->fromQuery('per_page', 20);

        try {
            $user = null;
            if (null !== $this->baseAuthManager->getIdentity()) {
                // Get current user // Can throw Exception
                $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            }
            if (null === $user || !$this->rbacManager->hasRole($user, 'Operator')) {

                return $this->message()->accessDenied();
            }

            $products = $this->productManager->getModerationProductsAndPaginatorOutput(null, $page, $per_page);

            if (!$products['products']) {

                return $this->message()->error('No product found');
            }

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $data = [
            'products' => $products['products'] ?? null,
            'productsPaginator' => $products['paginator'] ?? null
        ];

        return $this->message()->success('Waiting moderation products', $data);
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     */
    public function getDeclinedListAction()
    {
        $this->setOnlyAjax();
        $this->message()->setJsonStrategy(true);
        $page = (int)$this->params()->fromQuery('page', 1);
        $per_page = (int)$this->params()->fromQuery('per_page', 20);

        try {
            $user = null;
            if (null !== $this->baseAuthManager->getIdentity()) {
                // Get current user // Can throw Exception
                $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            }
            if (null === $user || !$this->rbacManager->hasRole($user, 'Operator')) {

                return $this->message()->error('Access is denied');
            }

            $products = $this->productManager->getDeclineProductsAndPaginatorOutput(null, $page, $per_page);

            if (!$products['products']) {

                return $this->message()->error('No product found');
            }

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $data = [
            'products' => $products['products'] ?? null,
            'productsPaginator' => $products['paginator'] ?? null
        ];

        return $this->message()->success('Declined products', $data);
    }

    /**
     * @param mixed $id
     * @return TwigViewModel|mixed|\Zend\Http\Response|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     */
    public function get($id)
    {
        $this->setOnlyAjax();
        $this->message()->setJsonStrategy(true);

        try {
            $user = null;
            if (null !== $this->baseAuthManager->getIdentity()) {
                // Get current user // Can throw Exception
                $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            }
            if (null === $user || !$this->rbacManager->hasRole($user, 'Operator')) {

                return $this->message()->error('Access is denied');
            }

            /** @var Product $product */
            $product = $this->productManager->getProductById((int)$id);
            // Get Output
            $productOutput = $this->productManager->getInsideOutput($product);

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $data = [
            'product' => $productOutput
        ];

        return $this->message()->success('Product single', $data);
        // Http -
//        $view = new TwigViewModel($data);
//        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
//        $view->setTemplate("application/product-moderation/single");
//        return $view;
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return array|bool|TwigViewModel|mixed|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function update($id, $data)
    {
        $this->setOnlyPostAndAjax();
        $this->message()->setJsonStrategy(true);

        $user = null;
        if (null !== $this->baseAuthManager->getIdentity()) {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        }
        if (null === $user || !$this->rbacManager->hasRole($user, 'Operator')) {

            return $this->message()->error('Access is denied');
        }

        try {
            $formDataInit = $this->processFormDataInit();

            /** @var Product $product */
            $product = $formDataInit['product'];
            /** @var Moderation $moderation */
            $moderation = $product->getModerations()->last();
            if (null !== $moderation->getResolutionDate()) {

                return $this->message()->error('Moderation already has resolution');
            }

            if (isset($data['approved']) && $data['approved'] === 1) {
                /** @var ProductModerationForm $form */
                $form = new ProductModerationForm();
            } else {
                /** @var ProductModerationForm $form */
                $form = new ProductModerationForm('decline');
            }

            // set data to form
            $form->setData($data);
            // validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                // Save Form Data
                $moderation = $this->productModerationManager->saveFormData($moderation, $formData);
                // Get Output
                $productOutput = $this->productManager->getInsideOutput($product);

                return $this->message()->success('Product', ['product' => $productOutput]);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @return array|bool|TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function formInitAction()
    {
        $this->setOnlyAjax();
        $this->message()->setJsonStrategy(true);

        try {
            $user = null;
            if (null !== $this->baseAuthManager->getIdentity()) {
                // Get current user // Can throw Exception
                $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            }
            if (null === $user || !$this->rbacManager->hasRole($user, 'Operator')) {

                return $this->message()->error('Access is denied');
            }

            $formDataInit = $this->processFormDataInit();

            if (isset($formDataInit['product'])) {
                /** @var Product $product */
                $product = $formDataInit['product'];
                $productOutput = $this->productManager->getInsideOutput($product);

                $formDataInit['product'] = $productOutput;
            }
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        return $this->message()->success('formInit', $formDataInit);
    }

    /**
     * @return array
     * @throws LogicException
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        $product = null;
        if (isset($incomingData['id']) || isset($incomingData['idProduct'])) {
            $id = $incomingData['id'] ?? $incomingData['idProduct'];
            /** @var Product $product */
            $product = $this->productManager->getProductById((int)$id);
        }

        if (null === $product) {

            throw new LogicException(null, 'PRODUCT_NOT_FOUND');
        }

        return [
            'product' => $product
        ];
    }

    /**
     * @return array
     */
    private function setOnlyAjax()
    {
        // only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }
    }

    /**
     * @return array
     */
    private function setOnlyPostAndAjax()
    {
        // only ajax
        if (!$this->getRequest()->isXmlHttpRequest() || !$this->getRequest()->isPost()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }
    }
}
