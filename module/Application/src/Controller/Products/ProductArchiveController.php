<?php
namespace Application\Controller\Products;

use Application\Entity\Product;
use Application\Form\ProductArchiveForm;
use Application\Service\ProductArchiveManager;
use Application\Service\ProductManager;
use Application\Service\UserManager;
use Core\Controller\AbstractRestfulController;
use Core\Controller\Plugin\Message\MessageJsonModelTrait;
use Core\Exception\LogicException;
use Core\Service\Base\BaseAuthManager;
use Core\Service\TwigViewModel;
use ModuleRbac\Service\RbacManager;

/**
 * Class ProductArchiveController
 * @package Application\Controller\Products
 */
class ProductArchiveController extends AbstractRestfulController
{
    use MessageJsonModelTrait;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * User manager
     * @var UserManager
     */
    private $userManager;

    /**
     * @var ProductManager
     */
    private $productManager;

    /**
     * @var ProductArchiveManager
     */
    private $productArchiveManager;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * ProductArchiveController constructor.
     * @param BaseAuthManager $baseAuthManager
     * @param UserManager $userManager
     * @param ProductManager $productManager
     * @param ProductArchiveManager $productArchiveManager
     * @param RbacManager $rbacManager
     */
    public function __construct(BaseAuthManager $baseAuthManager,
                                UserManager $userManager,
                                ProductManager $productManager,
                                ProductArchiveManager $productArchiveManager,
                                RbacManager $rbacManager)
    {
        $this->baseAuthManager = $baseAuthManager;
        $this->userManager = $userManager;
        $this->productManager = $productManager;
        $this->productArchiveManager = $productArchiveManager;
        $this->rbacManager = $rbacManager;
    }

    /**
     * @return TwigViewModel|mixed|\Zend\Http\Response|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     */
    public function getList()
    {
        $this->setOnlyAjax();
        $this->message()->setJsonStrategy(true);
        $page = (int)$this->params()->fromQuery('page', 1);
        $per_page = (int)$this->params()->fromQuery('per_page', 20);

        try {
            $user = null;
            if (null !== $this->baseAuthManager->getIdentity()) {
                // Get current user // Can throw Exception
                $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            }
            if (null === $user || !$this->rbacManager->hasRole($user, 'Verified')) {

                return $this->message()->error('Access is denied');
            }

            $is_operator = $this->rbacManager->hasRole($user, 'Operator');

            $products = $this->productManager->getArchiveProductsAndPaginatorOutput($is_operator ? null : $user, $page, $per_page);

            if (!$products['products']) {

                return $this->message()->error('No product found');
            }
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $data = [
            'products' => $products['products'] ?? null,
            'productsPaginator' => $products['paginator'] ?? null,
        ];

        return $this->message()->success('Archival products', $data);
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return array|bool|TwigViewModel|mixed|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     *
     * // @TODO получать id из POST формы и валидировать
     */
    public function archiveAction()
    {
        $this->setOnlyPostAndAjax();
        $this->message()->setJsonStrategy(true);

        $user = null;
        if (null !== $this->baseAuthManager->getIdentity()) {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        }

        try {
            if (null === $user || !$this->rbacManager->hasRole($user, 'Verified')) {

                return $this->message()->accessDenied();
            }

            $formDataInit = $this->processFormDataInit();
            /** @var Product $product */
            $product = $formDataInit['product'];

            if ($user !== $product->getUser()) {

                return $this->message()->accessDenied();
            }

            if ($product->isRemoved()) {

                return $this->message()->error('Product is already in the archive');
            }

            $form = new ProductArchiveForm();
            $postData = $formDataInit['postData'];
            // set data to form
            $form->setData($postData);
            // validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                // Save Form Data
                $this->productArchiveManager->sendProductToArchive($product);
                // Get Output
                $productOutput = $this->productManager->getInsideOutput($product);

                return $this->message()->success('Product sent to archive', ['product' => $productOutput]);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return array|bool|TwigViewModel|mixed|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     *
     * // @TODO получать id из POST формы и валидировать
     */
    public function unarchiveAction()
    {
        $this->setOnlyPostAndAjax();
        $this->message()->setJsonStrategy(true);

        $user = null;
        if (null !== $this->baseAuthManager->getIdentity()) {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        }

        try {
            if (null === $user || !$this->rbacManager->hasRole($user, 'Verified')) {

                return $this->message()->accessDenied();
            }

            $formDataInit = $this->processFormDataInit();
            /** @var Product $product */
            $product = $formDataInit['product'];

            if ($user !== $product->getUser()) {

                return $this->message()->accessDenied();
            }

            if (false === $product->isRemoved()) {

                return $this->message()->error('The product is not in the archive');
            }

            $form = new ProductArchiveForm();
            $postData = $formDataInit['postData'];
            // set data to form
            $form->setData($postData);
            // validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                // Save Form Data
                $this->productArchiveManager->returnProductFromArchive($product);
                // Get Output
                $productOutput = $this->productManager->getInsideOutput($product);

                return $this->message()->success('The product returned from the archive', ['product' => $productOutput]);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @return array|bool|TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function formInitAction()
    {
        $this->setOnlyAjax();
        $this->message()->setJsonStrategy(true);

        try {
            $user = null;
            if (null !== $this->baseAuthManager->getIdentity()) {
                // Get current user // Can throw Exception
                $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            }
            if (null === $user || !$this->rbacManager->hasRole($user, 'Operator')) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $formDataInit = $this->processFormDataInit();

            if (isset($formDataInit['product'])) {
                /** @var Product $product */
                $product = $formDataInit['product'];
                $productOutput = $this->productManager->getInsideOutput($product);

                $formDataInit['product'] = $productOutput;
            }
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        return $this->message()->success('formInit', $formDataInit);
    }

    /**
     * @return array
     * @throws LogicException
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        $product = null;
        if (isset($incomingData['id']) || isset($incomingData['idProduct'])) {
            $id = $incomingData['id'] ?? $incomingData['idProduct'];
            /** @var Product $product */
            $product = $this->productManager->getProductById((int)$id);
        }

        if (null === $product) {

            throw new LogicException(null, 'PRODUCT_NOT_FOUND');
        }

        return [
            'product' => $product,
            'postData' => $incomingData['postData']
        ];
    }

    /**
     * @return array
     */
    private function setOnlyAjax()
    {
        // only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }
    }

    /**
     * @return array
     */
    private function setOnlyPostAndAjax()
    {
        // only ajax
        if (!$this->getRequest()->isXmlHttpRequest() || !$this->getRequest()->isPost()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }
    }
}
