<?php
namespace Application\Controller\Products;

use Application\Form\ProductImageModifyForm;
use Application\Form\ProductImageUploadForm;
use Application\Service\ProductManager;
use Core\Controller\AbstractRestfulController;
use Core\Exception\LogicException;
use ModuleFileManager\Service\FileManager;

/**
 * Class ProductImageController
 * @package Application\Controller\Products
 */
class ProductImageController extends AbstractRestfulController
{
    const SESSION_IMAGES_KEY = 'saved_images';
    /**
     * @var FileManager
     */
    private $fileManager;
    /**
     * @var ProductManager
     */
    private $advertManager;



    /**
     * AdvertFileController constructor.
     * @param FileManager $fileManager
     * @param ProductManager $productManager
     */
    public function __construct(FileManager $fileManager,
                                ProductManager $productManager)
    {
        $this->fileManager = $fileManager;
        $this->advertManager = $productManager;
    }

    /**
     * @return bool|\Core\Service\TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function productImageShowAction()
    {
        try {
            $image_id = $this->params()->fromRoute('image', null);
            $file = $this->fileManager->getFileById($image_id);

            if ( !$file || !$this->fileManager->isFileReadable($file)) {

                throw new LogicException(null, LogicException::FILE_NOT_READABLE_OR_NOT_EXIST);
            }
            if (!$this->fileManager->isFileTypeDisplayable($file->getType())) {

                throw new LogicException(null, LogicException::FILE_COULD_NOT_BE_DISPLAYED);
            }

            return $this->fileManager->fileDisplay($file);
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @return bool|\Core\Service\TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function imageUploadAction()
    {
        $this->setOnlyPostAndAjax();
        $this->message()->setJsonStrategy(true);
        try {
            $productImageUploadForm = new ProductImageUploadForm();
            $postData = null;
            $filesData = $this->collectIncomingData()['filesData'];
            if (isset($filesData['image']) && $filesData['image']['error'] === 0) {
                $postData = array_merge_recursive($this->collectIncomingData()['postData'], $filesData);
            }
            if ($postData) {
                $productImageUploadForm->setData($postData);
                if ($productImageUploadForm->isValid()) {
                    $data = $productImageUploadForm->getData();
                    $imageOutput = $this->advertManager->saveImageTemporaryFile($data['image'] ?? []);

                    return $this->message()->success('upload successful', $imageOutput);
                }

                return $this->message()->invalidFormData($productImageUploadForm->getMessages());
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        return $this->message()->error('Not defined the image data');
        // вьюшка для отладки
//         return $this->view(['productImageUploadForm' => $productImageUploadForm]);
    }

    /**
     * @return array
     */
    private function setOnlyPostAndAjax()
    {
        // only ajax
        if (!$this->getRequest()->isXmlHttpRequest() || !$this->getRequest()->isPost()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }
    }
}
