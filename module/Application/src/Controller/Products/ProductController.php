<?php
namespace Application\Controller\Products;

use Application\Entity\Product;
use Application\Entity\User;
use Application\Form\ProductForm;
use Application\Form\StartPublishForm;
use Application\Form\StopPublishForm;
use Application\Form\PreviewConfirmForm;
use Application\Service\Output\ProductOutput;
use Application\Service\ProductManager;
use Application\Service\CategoryManager;
use Application\Service\UserManager;
use Application\View\Helper\UrlHelper;
use Core\Controller\AbstractRestfulController;
use Core\Controller\Plugin\Message\MessageJsonModelTrait;
use Core\Exception\LogicException;
use Core\Service\Base\BaseAuthManager;
use Core\Service\TwigViewModel;
use Core\Service\JsonUtil;
use Doctrine\ORM\EntityManager;
use ModuleRbac\Service\RbacManager;
use Zend\Form\Element;

/**
 * Class ProductController
 * @package Application\Controller\Products
 */
class ProductController extends AbstractRestfulController
{
    use MessageJsonModelTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var ProductManager
     */
    private $productManager;

    /**
     * @var CategoryManager
     */
    private $categoryManager;

    /**
     * @var ProductOutput
     */
    private $productOutput;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * @var array
     */
    private $config;

    /**
     * LandingController constructor.
     * @param EntityManager $entityManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param ProductManager $productManager
     * @param CategoryManager $categoryManager
     * @param ProductOutput $productOutput
     * @param array $config
     */
    public function __construct(EntityManager $entityManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                ProductManager $productManager,
                                CategoryManager $categoryManager,
                                ProductOutput $productOutput,
                                RbacManager $rbacManager,
                                array $config)
    {
        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->productManager = $productManager;
        $this->categoryManager = $categoryManager;
        $this->productOutput = $productOutput;
        $this->rbacManager = $rbacManager;
        $this->config = $config;
    }

    public function getList()
    {
        $this->response->setStatusCode(404);

        return [
            'content' => 'Page not found'
        ];
    }

    /**
     * @param mixed $id
     * @return TwigViewModel|mixed|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     */
    public function get($id)
    {
        $this->setOnlyAjax();
        $this->message()->setJsonStrategy(true);

        try {
            $user = null;
            if (null !== $this->baseAuthManager->getIdentity()) {
                // Get current user // Can throw Exception
                $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            }
            if (null === $user || !$this->rbacManager->hasRole($user, 'Verified')) {

                return $this->message()->error('Access is denied');
            }

            /** @var Product $product */
            $product = $this->productManager->getProductById((int)$id);
            if ($user !== $product->getUser()) {

                return $this->message()->error('Access is denied');
            }
            // Get Output
            $productOutput = $this->productManager->getInsideOutput($product);

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $data = [
            'product' => $productOutput
        ];

        return $this->message()->success('Product single', $data);
    }

    /**
     * @param mixed $data
     * @return TwigViewModel|mixed|\Zend\Http\Response|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Core\Exception\LogicException
     * @throws \Exception
     */
    public function create($data)
    {
        $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        if ($this->rbacManager->hasRole($user, 'Operator')) {

            return $this->message()->accessDenied();
        }

        $categories = $this->categoryManager->getCategories();

        $productForm = new ProductForm($categories);
        $previewConfirmForm = new PreviewConfirmForm();
        $previewConfirmForm->setData($data);

        $product_data = $this->sessionPlugin()->getSessionVar(ProductManager::SESSION_PRODUCT_FLAG);
        $product_data['csrf'] = $previewConfirmForm->get('csrf')->getValue();
        $productForm = $this->productManager->fillProductFormWithSessionData(
            $productForm,
            $product_data
        );

        if ($previewConfirmForm->isValid() && $productForm->isValid()) {
            //отчищаем данные в сессии
            $this->sessionPlugin()->clearSessionVar(ProductManager::SESSION_PRODUCT_FLAG);
            //сохраняем объявление
            $product = $this->productManager->createProduct($user, $productForm->getData());
            //прикрепляем файлы к объявлению
            $this->productManager->attachProductFiles($product, [
                'files' => $product_data['files'] ?? null,
                'gallery' => $product_data['gallery'] ?? null,
            ]);

            return $this->message()->success('product create success', [
                'product' => $this->productManager->getInsideOutput($product)
            ]);
        }

        $previewErrors = array_merge_recursive(
            $previewConfirmForm->getMessages(),
            $productForm->getMessages()
        );

        return $this->message()->invalidFormData($previewErrors);
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return TwigViewModel|mixed|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Core\Exception\LogicException
     * @throws \Exception
     */
    public function update($id, $data)
    {
        $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        $product = $this->productManager->getProductById($id);
        // Check permissions
        if (!$product || !$this->access('product.own.edit', ['user' => $user,'product' => $product]) ) {

            return $this->message()->accessDenied();
        }
        $categories = $this->categoryManager->getCategories();
        $productForm = new ProductForm($categories);
        $previewConfirmForm = new PreviewConfirmForm();
        $previewConfirmForm->setData($data);

        $product_data = $this->sessionPlugin()->getSessionVar(ProductManager::SESSION_PRODUCT_FLAG);
        $product_data['csrf'] = $previewConfirmForm->get('csrf')->getValue();
        $productForm = $this->productManager->fillProductFormWithSessionData(
            $productForm,
            $product_data
        );

        if ($previewConfirmForm->isValid() && $productForm->isValid()) {
            //отчищаем данные в сессии
            $this->sessionPlugin()->clearSessionVar(ProductManager::SESSION_PRODUCT_FLAG);
            //сохраняем объявление
            $product = $this->productManager->editProduct($product, $productForm->getData());
            //прикрепляем файлы к объявлению
            $this->productManager->attachProductFiles($product, [
                'files' => $product_data['files'] ?? null,
                'gallery' => $product_data['gallery'] ?? null,
            ]);

            return $this->message()->success('product edit success', [
                'product' => $this->productManager->getInsideOutput($product)
            ]);
        }

        $previewErrors = array_merge_recursive(
            $previewConfirmForm->getMessages(),
            $productForm->getMessages()
        );

        return $this->message()->invalidFormData($previewErrors);
    }

    /**
     * @return bool|TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function createProductAction()
    {
        try {
            $isAjax = $this->getRequest()->isXmlHttpRequest();
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            if ($this->rbacManager->hasRole($user, 'Operator')) {

                return $this->message()->accessDenied();
            }

            $userOutput = $this->userManager->getUserOutput($user);
            $categories = $this->categoryManager->getCategories();
            $form = new ProductForm($categories);
            $form->setAttribute('action', $this->url()->fromRoute('product-create'));
            /////////////////////////////////////////////
            //если есть параметр то берем данные из сессии
            $is_need_session_data = $this->params()->fromQuery('sd', false) === 'true';
            $product_data = $this->productOutput->getProductOutputWithSessionOrEntity(
                null,
                $this->sessionPlugin(),
                $is_need_session_data
            );
            $form_data = $this->productManager->getFormDataWithProductData($product_data);
            if (!empty($form_data)) {
                $form->setData($form_data);
            }
            ////////////////////////////////////////////////
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                $filesData = $this->productManager->getFilesPostData($this->collectIncomingData()['filesData']);
                ///// step 1: разбиваем данные на предворительно сохраненные картинки и данные ProductForm /////
                $preparedData = $this->productManager->prepareDataToProduct($postData, $filesData);
                ///// step 2: валидация предворительно сохраненных картинок /////
                $preparedData = $this->productManager->getValidationSavedGalleryAndFiles($preparedData);
                ///// step 3: валидация ProductForm /////
                $preparedData = $this->productManager->getValidationProductForm($preparedData, $form);
                if ($preparedData['validation']['is_valid']) {
                    // добавить/редактировать файлы и сохранить данные в сессии
                    $product_data = $this->productManager->editFilesAndSaveDataInSession($this->sessionPlugin(), $preparedData);

                    return $this->message()->success('product create success', [
                        'product' => $product_data,
                        'user' => $userOutput
                    ]);
                }
                if ($isAjax) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($form->getMessages());
                }
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $form->prepare();
        return $this->view([
            'categories_json' => JsonUtil::mb_encode($this->categoryManager->getCategoriesOutput($categories, false)),
            'product_data_json' => JsonUtil::mb_encode($product_data),
            'productForm' => $form
        ]);
    }

    /**
     * @return bool|TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function editProductAction()
    {
        try {
            $isAjax = $this->getRequest()->isXmlHttpRequest();
            $product_id = (int)$this->params()->fromRoute('idProduct', 0);

            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            $userOutput = $this->userManager->getUserOutput($user);
            $product = $this->productManager->getProductById($product_id);
            // Check permissions
            if (!$product || !$this->access('product.own.edit', ['user' => $user,'product' => $product]) ) {

                return $this->message()->accessDenied();
            }

            $categories = $this->categoryManager->getCategories();
            $form = new ProductForm($categories);
            $form->setAttribute('action', $this->url()->fromRoute('product-edit', ['idProduct'=>$product_id]));
            ////////////////////////////////////////////////////////////////
            //если есть параметр то берем данные из сессии
            $is_need_session_data = $this->params()->fromQuery('sd', false) === 'true';
            $product_data = $this->productOutput->getProductOutputWithSessionOrEntity(
                $product,
                $this->sessionPlugin(),
                $is_need_session_data
            );
            $form_data = $this->productManager->getFormDataWithProductData($product_data);
            if (!empty($form_data)) {
                $form->setData($form_data);
            }
            $product_data['id'] = $product->getId();

            //////////////////////////////////////////////////////////////////////
            $postData = $this->collectIncomingData()['postData'];

            if ($postData) {
                $filesData = $this->productManager->getFilesPostData($this->collectIncomingData()['filesData']);
                ///// step 1: разбиваем данные на предворительно сохраненные картинки и данные ProductForm /////
                $preparedData = $this->productManager->prepareDataToProduct($postData, $filesData);
                ///// step 2: валидация предворительно сохраненных картинок /////
                $preparedData = $this->productManager->getValidationSavedGalleryAndFiles($preparedData);
                ///// step 3: валидация ProductForm /////
                $preparedData = $this->productManager->getValidationProductForm($preparedData, $form);

                if ($preparedData['validation']['is_valid']) {
                    // добавить/редактировать файлы и сохранить данные в сессии
                    $product_data = $this->productManager->editFilesAndSaveDataInSession($this->sessionPlugin(), $preparedData);
                    $product_data['id'] = $product->getId();
                    return $this->message()->success('product edit success', [
                        'product' => $product_data,
                        'user' => $userOutput
                    ]);
                }
                if ($isAjax) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($form->getMessages());
                }
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $form->prepare();
        return $this->view([
            'categories_json' => JsonUtil::mb_encode($this->categoryManager->getCategoriesOutput($categories, false)),
            'product_data_json' => JsonUtil::mb_encode($product_data),
            'productForm' => $form
        ]);
    }

    /**
     * @return bool|TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function productSearchAction()
    {
        $this->message()->setJsonStrategy(true);
        try {
            $incomingData = $this->collectIncomingData();
            $queryData = $incomingData['queryData'];

            $search = isset($queryData['q']) && !empty($queryData['q']) ? trim($queryData['q']) : null;
            $fabricator = isset($queryData['fabricator']) && !empty($queryData['fabricator']) ? trim($queryData['fabricator']) : null;

            if (empty($search)) {
                return $this->message()->error('Missing search value', null);
            }
            $catalog_root_url = $this->url()->fromRoute(UrlHelper::ROUTE_PRODUCT_LIST, ['city_slug' => 'rossiia']);
            $searchOutput = $this->productManager->productsSearchAutoComplete($search, $catalog_root_url, $fabricator);

            return $this->message()->success('product search result', $searchOutput);
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @return TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     */
    public function getPublishedListAction()
    {
        $this->setOnlyAjax();
        $this->message()->setJsonStrategy(true);
        $page = (int)$this->params()->fromQuery('page', 1);
        $per_page = (int)$this->params()->fromQuery('per_page', 8);

        try {
            $user = null;
            if (null !== $this->baseAuthManager->getIdentity()) {
                /** @var User $user */ // Current user // Can throw Exception
                $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            }

            if (null === $user) {

                return $this->message()->accessDenied();
            }

            $for_user = $user;
            if ($this->rbacManager->hasRole($user, 'Operator')) {

                $for_user = null;
            }

            $products = $this->productManager->getPublishedProductsAndPaginatorOutput($for_user, $page, $per_page);

            if (!$products['products']) {

                return $this->message()->error('No product found');
            }

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $data = [
            'products' => $products['products'] ?? null,
            'productsPaginator' => $products['paginator'] ?? null
        ];

        return $this->message()->success('Public products', $data);
    }

    /**
     * @return TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function getInactiveListAction()
    {
        $this->setOnlyAjax();
        $this->message()->setJsonStrategy(true);
        $page = (int)$this->params()->fromQuery('page', 1);
        $per_page = (int)$this->params()->fromQuery('per_page', 20);

        try {
            $user = null;
            if (null !== $this->baseAuthManager->getIdentity()) {
                // Get current user // Can throw Exception
                $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            }
            if (null === $user || !$this->rbacManager->hasRole($user, 'Verified')) {

                return $this->message()->error('Access is denied');
            }

            $products = $this->productManager->getInactiveProductsAndPaginatorOutput($user, $page, $per_page);

            if (!$products['products']) {

                return $this->message()->error('No product found');
            }

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $data = [
            'products' => $products['products'] ?? null,
            'productsPaginator' => $products['paginator'] ?? null
        ];

        return $this->message()->success('Inactive products', $data);
    }

    /**
     * @return bool|TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function stopPublishAction()
    {
        $this->setOnlyPostAndAjax();
        $this->message()->setJsonStrategy(true);

        $user = null;
        if (null !== $this->baseAuthManager->getIdentity()) {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        }

        try {
            if (null === $user || !$this->rbacManager->hasRole($user, 'Verified')) {

                return $this->message()->error('Access is denied');
            }

            $formDataInit = $this->processFormDataInit();
            /** @var Product $product */
            $product = $formDataInit['product'];

            if ($user !== $product->getUser()) {

                return $this->message()->error('Access is denied');
            }

            if (false === $product->isVisible()) {

                return $this->message()->error('Product is not published');
            }

            $form = new StopPublishForm();
            $postData = $formDataInit['postData'];
            // set data to form
            $form->setData($postData);
            // validate form
            if ($form->isValid()) {
                $this->productManager->stopPublish($product);
                // Get Output
                $productOutput = $this->productManager->getInsideOutput($product);

                return $this->message()->success('Publishing stopped', ['product' => $productOutput]);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @return bool|TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function startPublishAction()
    {
        $this->setOnlyPostAndAjax();
        $this->message()->setJsonStrategy(true);

        $user = null;
        if (null !== $this->baseAuthManager->getIdentity()) {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        }

        try {
            if (null === $user || !$this->rbacManager->hasRole($user, 'Verified')) {

                return $this->message()->error('Access is denied');
            }

            $formDataInit = $this->processFormDataInit();
            /** @var Product $product */
            $product = $formDataInit['product'];

            if ($user !== $product->getUser()) {

                return $this->message()->error('Access is denied');
            }

            if (true === $product->isVisible()) {

                return $this->message()->error('Product is already published');
            }

            $form = new StartPublishForm();
            $postData = $formDataInit['postData'];
            // set data to form
            $form->setData($postData);
            // validate form
            if ($form->isValid()) {
                $this->productManager->startPublish($product);
                // Get Output
                $productOutput = $this->productManager->getInsideOutput($product);

                return $this->message()->success('Publishing started', ['product' => $productOutput]);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * Удалить объявление навсегда
     *
     * @param mixed $id
     * @return bool|TwigViewModel|mixed|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function delete($id)
    {
        $this->message()->setJsonStrategy(true);

        try {
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            $product = $this->productManager->getProductById($id);
            // Check permissions
            if (!$product || !$this->access('product.own.edit', ['user' => $user,'product' => $product]) ) {

                return $this->message()->accessDenied();
            }
            //удаление объявления
            $this->productManager->removeProduct($product);

            return $this->message()->success('delete successfully');
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @return array
     * @throws LogicException
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        $product = null;
        if (isset($incomingData['id']) || isset($incomingData['idProduct'])) {
            $id = $incomingData['id'] ?? $incomingData['idProduct'];
            /** @var Product $product */
            $product = $this->productManager->getProductById((int)$id);
        }

        if (null === $product) {

            throw new LogicException(null, 'PRODUCT_NOT_FOUND');
        }

        return [
            'product' => $product,
            'postData' => $incomingData['postData']
        ];
    }

    /**
     * @return array
     */
    private function setOnlyAjax()
    {
        // only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }
    }

    /**
     * @return array
     */
    private function setOnlyPostAndAjax()
    {
        // only ajax
        if (!$this->getRequest()->isXmlHttpRequest() || !$this->getRequest()->isPost()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }
    }
}
