<?php
namespace Application\Controller\Products\Factory;

use Application\Controller\Products\ProductArchiveController;
use Application\Service\ProductArchiveManager;
use Application\Service\ProductManager;
use Application\Service\UserManager;
use Core\Service\Base\BaseAuthManager;
use Interop\Container\ContainerInterface;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class ProductArchiveControllerFactory
 * @package Application\Controller\Products\Factory
 */
class ProductArchiveControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ProductArchiveManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $userManager = $container->get(UserManager::class);
        $productManager = $container->get(ProductManager::class);
        $productArchiveManager = $container->get(ProductArchiveManager::class);
        $rbacManager = $container->get(RbacManager::class);

        return new ProductArchiveController(
            $baseAuthManager,
            $userManager,
            $productManager,
            $productArchiveManager,
            $rbacManager
        );
    }
}