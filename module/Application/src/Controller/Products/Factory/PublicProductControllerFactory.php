<?php
namespace Application\Controller\Products\Factory;

use Application\Controller\Products\PublicProductController;
use Application\Service\CityManager;
use Application\Service\ProductManager;
use Application\Service\CategoryManager;
use Application\Service\Status\ProductStatus\PublishedStatus;
use Application\Service\UserManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class PublicProductControllerFactory
 * @package Application\Controller\Products\Factory
 */
class PublicProductControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return \Application\Controller\Products\PublicProductController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $userManager = $container->get(UserManager::class);
        $advertManager = $container->get(ProductManager::class);
        $cityManager = $container->get(CityManager::class);
        $categoryManager = $container->get(CategoryManager::class);
        $publishedStatus = new PublishedStatus();

        return new PublicProductController(
            $userManager,
            $advertManager,
            $cityManager,
            $categoryManager,
            $publishedStatus
        );
    }
}