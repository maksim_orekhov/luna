<?php
namespace Application\Controller\Products\Factory;

use Application\Controller\Products\ProductController;
use Application\EventManager\ProductEventProvider;
use Application\Service\Output\ProductOutput;
use Application\Service\ProductManager;
use Application\Service\CategoryManager;
use Application\Service\UserManager;
use Core\Service\Base\BaseAuthManager;
use Interop\Container\ContainerInterface;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class ProductControllerFactory
 * @package Application\Controller\Products\Factory
 */
class ProductControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ProductController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $advertManager = $container->get(ProductManager::class);
        $categoryManager = $container->get(CategoryManager::class);
        $productOutput = $container->get(ProductOutput::class);
        $rbacManager = $container->get(RbacManager::class);
        $config = $container->get('config');

        return new ProductController(
            $entityManager,
            $userManager,
            $baseAuthManager,
            $advertManager,
            $categoryManager,
            $productOutput,
            $rbacManager,
            $config
        );
    }
}