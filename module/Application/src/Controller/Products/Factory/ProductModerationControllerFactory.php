<?php
namespace Application\Controller\Products\Factory;

use Application\Controller\Products\ProductModerationController;
use Application\Service\ProductManager;
use Application\Service\ProductModerationManager;
use Application\Service\UserManager;
use Core\Service\Base\BaseAuthManager;
use Interop\Container\ContainerInterface;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class ProductModerationControllerFactory
 * @package Application\Controller\Products\Factory
 */
class ProductModerationControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ProductModerationController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $userManager = $container->get(UserManager::class);
        $productModerationManager = $container->get(ProductModerationManager::class);
        $productManager = $container->get(ProductManager::class);
        $rbacManager = $container->get(RbacManager::class);

        return new ProductModerationController(
            $baseAuthManager,
            $userManager,
            $productModerationManager,
            $productManager,
            $rbacManager
        );
    }
}