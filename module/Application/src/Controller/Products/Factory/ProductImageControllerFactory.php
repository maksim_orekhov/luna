<?php
namespace Application\Controller\Products\Factory;

use Application\Controller\Products\ProductImageController;
use Application\Service\ProductManager;
use Interop\Container\ContainerInterface;
use ModuleFileManager\Service\FileManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class ProductImageControllerFactory
 * @package Application\Controller\Products\Factory
 */
class ProductImageControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return \Application\Controller\Products\ProductImageController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $fileManager = $container->get(FileManager::class);
        $advertManager = $container->get(ProductManager::class);

        return new ProductImageController(
            $fileManager,
            $advertManager
        );
    }
}