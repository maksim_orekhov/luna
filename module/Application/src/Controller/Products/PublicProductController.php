<?php
namespace Application\Controller\Products;

use Application\Entity\Category;
use Application\Entity\City;
use Application\Service\CityManager;
use Application\Service\Output\UserOutput;
use Application\Service\ProductManager;
use Application\Service\CategoryManager;
use Application\Service\Status\ProductStatus\PublishedStatus;
use Application\Service\UserManager;
use Application\View\Helper\UrlHelper;
use Core\Controller\AbstractRestfulController;
use Core\Controller\Plugin\Message\MessageJsonModelTrait;
use Core\Exception\BaseExceptionCodeInterface;
use Core\Exception\LogicException;
use Core\Service\Base\BaseAuthManager;
use Core\Service\JsonUtil;
use Zend\Paginator\Paginator;

/**
 * Class ProductShowController
 * @package Application\Controller\Products
 */
class PublicProductController extends AbstractRestfulController
{
    use MessageJsonModelTrait;
    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var ProductManager
     */
    private $productManager;
    /**
     * @var CategoryManager
     */
    private $categoryManager;
    /**
     * @var CityManager
     */
    private $cityManager;
    /**
     * @var PublishedStatus
     */
    private $publishedStatus;

    /**
     * PublicProductController constructor.
     * @param UserManager $userManager
     * @param ProductManager $productManager
     * @param CityManager $cityManager
     * @param CategoryManager $categoryManager
     * @param PublishedStatus $publishedStatus
     */
    public function __construct(UserManager $userManager,
                                ProductManager $productManager,
                                CityManager $cityManager,
                                CategoryManager $categoryManager,
                                PublishedStatus $publishedStatus)
    {
        $this->userManager = $userManager;
        $this->productManager = $productManager;
        $this->cityManager = $cityManager;
        $this->categoryManager = $categoryManager;
        $this->publishedStatus = $publishedStatus;
    }

    /**
     * @return array|bool|\Core\Service\TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function productListAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        $incomingData = $this->collectIncomingData();
        $queryData = $incomingData['queryData'];
        $view_mode = isset($queryData['view_mode']) ? trim($queryData['view_mode']) : null;
        $fabricator = isset($queryData['fabricator']) && !empty($queryData['fabricator']) ? trim($queryData['fabricator']) : null;

        try {
            $route_params = [
                'search' => isset($queryData['q']) && !empty($queryData['q']) ? trim($queryData['q']) : null,
                'city_slug' => $this->params()->fromRoute('city_slug', null),
                'category_slug' => $this->params()->fromRoute('category_slug', null),
                'sub_category_slug' => $this->params()->fromRoute('sub_category_slug', null),
                'fabricator' => $fabricator,
                'page' => isset($queryData['page']) ? trim($queryData['page']) : 1
            ];

            $city = $this->cityManager->getCityBySlug($route_params['city_slug']);
            $category = $this->categoryManager->getCategoryBySlug($route_params['category_slug']);
            $subCategory = $this->categoryManager->getCategoryBySlug($route_params['sub_category_slug']);

            if (! $this->productManager->isRouteProductCorrect($city, $category, $subCategory) ) {
                $this->response->setStatusCode(404);
                return [
                    'message' => BaseExceptionCodeInterface::PAGE_NOT_FOUND_MESSAGE
                ];
            }

            /** @var Paginator $paginatorProducts */
            $paginatorProducts = $this->productManager->getPublicProductsByRouteData($route_params);

            $categories = $this->categoryManager->getCategories();
            $sub_category = false;

            $categoriesOutput = $this->categoryManager->getCategoriesOutput($categories, $sub_category);

            $productsOutput = [];
            $paginator_data = [];
            if ($paginatorProducts && $paginatorProducts->getCurrentItemCount() > 0) {
                $productsOutput = $this->productManager->getPublicListOutput($paginatorProducts->getCurrentItems());
                $paginator_data = [
                    'current_page' => $paginatorProducts->getCurrentPageNumber(),
                    'total_item_count' => $paginatorProducts->getTotalItemCount(),
                    'item_count_per_page' => $paginatorProducts->getItemCountPerPage(),
                ];
            }

            if ($isAjax) {

                return $this->message()->success('Products', [
                    'products' => $productsOutput,
                    'paginator' => $paginator_data
                ]);
            }
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        return $this->view([
            'products' => $productsOutput,
            'paginator' => $paginator_data,
            'view_mode' => $view_mode,
            'categories' => $categoriesOutput,
            'init_search' => [
                'catalog_root_url' => $this->url()->fromRoute(UrlHelper::ROUTE_PRODUCT_LIST, ['city_slug' => 'rossiia']),
                'catalog_search_url' => $this->url()->fromRoute(UrlHelper::ROUTE_PRODUCT_SEARCH),
                'catalog_name' => UrlHelper::CATALOG_PRODUCT,
                'category' => $category ? $category->getId() : null,
                'sub_category' => $subCategory ? $subCategory->getId() : null,
                'search' => $route_params['search'] ?? '',
                'fabricator' => $fabricator
            ]
        ]);
    }

    /**
     * @return array|bool|\Core\Service\TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function productSingleAction()
    {
        try {
            $incomingData = $this->collectIncomingData();
            $queryData = $incomingData['queryData'];
            $fabricator = isset($queryData['fabricator']) && !empty($queryData['fabricator']) ? trim($queryData['fabricator']) : null;
            $route_params = [
                'city_slug' => $this->params()->fromRoute('city_slug', null),
                'category_slug' => $this->params()->fromRoute('category_slug', null),
                'sub_category_slug' => $this->params()->fromRoute('sub_category_slug', null),
                'product_slug' => $this->params()->fromRoute('product_slug', null),
            ];
            $product = $this->productManager->getProductByCategory($route_params['product_slug'], $route_params['sub_category_slug']);
            if (!$product || ($product && !$this->publishedStatus->isProductInStatus($product))) {
                $this->response->setStatusCode(404);
                return [
                    'message' => BaseExceptionCodeInterface::PAGE_NOT_FOUND_MESSAGE
                ];
            }
            $category = $this->categoryManager->getCategoryBySlug($route_params['category_slug']);
            $subCategory = $this->categoryManager->getCategoryBySlug($route_params['sub_category_slug']);

            $productOutput = $this->productManager->getPublicSingleOutput($product);
            /** @var Paginator $similarProducts */
            $similarProducts = $this->productManager->getSimilarProducts($product);
            $similarProductsOutput = [];
            if ($similarProducts && $similarProducts->getCurrentItemCount() > 0) {
                $similarProductsOutput = $this->productManager->getPublicListOutput($similarProducts->getCurrentItems());
            }

            $categories = $this->categoryManager->getCategories();
            $sub_category = false;

            $categoriesOutput = $this->categoryManager->getCategoriesOutput($categories, $sub_category);
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        return $this->view([
            'product' => $productOutput,
            'similarProductsOutput' => $similarProductsOutput,
            'categories' => $categoriesOutput,
            'init_search' => [
                'catalog_root_url' => $this->url()->fromRoute(UrlHelper::ROUTE_PRODUCT_LIST, ['city_slug' => 'rossiia']),
                'catalog_search_url' => $this->url()->fromRoute(UrlHelper::ROUTE_PRODUCT_SEARCH),
                'catalog_name' => UrlHelper::CATALOG_PRODUCT,
                'category' => $category ? $category->getId() : null,
                'sub_category' => $subCategory ? $subCategory->getId() : null,
                'search' => $route_params['search'] ?? '',
                'fabricator' => $fabricator
            ]
        ]);
    }

    /**
     * @return array|bool|\Core\Service\TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function userProductListAction()
    {
        try {
            $isAjax = $this->getRequest()->isXmlHttpRequest();

            $incomingData = $this->collectIncomingData();
            $queryData = $incomingData['queryData'];
            $routeData = $incomingData['routeData'];

            $route_params = [
                'search' => isset($queryData['q']) && !empty($queryData['q']) ? trim($queryData['q']) : null,
                'code' => $routeData['code'] ?? null,
                'per_page' => isset($queryData['per_page']) ? trim($queryData['per_page']) : 8,
                'page' => isset($queryData['page']) ? trim($queryData['page']) : 1,
            ];

            if (!$route_params['code']) {
                $this->response->setStatusCode(404);
                return [
                    'message' => BaseExceptionCodeInterface::PAGE_NOT_FOUND_MESSAGE
                ];
            }

            $user = $this->userManager->getUserBySlug($route_params['code']);
            if (!$user) {
                $this->response->setStatusCode(404);
                return [
                    'message' => BaseExceptionCodeInterface::PAGE_NOT_FOUND_MESSAGE
                ];
            }

            /** @var Paginator $products */
            $paginatedProducts = $this->productManager->getPublicProductsAndPaginatorOutput($user, $route_params);
            $userOutput = $this->userManager->getLargeOutput($user, true, true);

            if ($isAjax) {
                $data = [
                    'products' => $paginatedProducts['products'] ?? null,
                    'productsPaginator' => $paginatedProducts['paginator'] ?? null
                ];

                return $this->message()->success('Published products for user', $data);
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $products_url = [
            'user_purchases' => UserOutput::getUserPurchasesUrl($user),
            'user_products' => UserOutput::getUserProductsUrl($user),
        ];

        return $this->view([
            'owner' => $userOutput,
            'products' => $paginatedProducts['products'],
            'products_urls' => $products_url,
            'products_json' => JsonUtil::mb_encode($paginatedProducts['products']),
            'productsPaginator_json' => JsonUtil::mb_encode($paginatedProducts['paginator']),
            'owner_code_json' => JsonUtil::mb_encode(['owner_code' => $route_params['code']])
        ]);
    }
}
