<?php
namespace Application\Controller\Shops;

use Application\Entity\User;
use Application\Form\ShopCustomForm;
use Application\Service\ShopManager;
use Application\Service\UserManager;
use Core\Controller\AbstractRestfulController;
use Core\Service\Base\BaseAuthManager;
use ModuleRbac\Service\RbacManager;

class ShopController extends AbstractRestfulController
{
    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;
    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var RbacManager
     */
    private $rbacManager;
    /**
     * @var ShopManager
     */
    private $shopManager;

    /**
     * FabricatorController constructor.
     * @param BaseAuthManager $baseAuthManager
     * @param UserManager $userManager
     * @param RbacManager $rbacManager
     * @param ShopManager $shopManager
     */
    public function __construct(BaseAuthManager $baseAuthManager,
                                UserManager $userManager,
                                RbacManager $rbacManager,
                                ShopManager $shopManager
    )
    {
        $this->baseAuthManager = $baseAuthManager;
        $this->userManager = $userManager;
        $this->rbacManager = $rbacManager;
        $this->shopManager = $shopManager;
    }

    /**
     * @return bool|\Core\Service\TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function shopCustomCreateAction()
    {
        try {
            $isAjax = $this->getRequest()->isXmlHttpRequest();
            $current_user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole($current_user, 'Operator');
            if (!$isOperator) {
                return $this->message()->accessDenied();
            }
            // изменение пользователя
            $form = new ShopCustomForm();
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                $form->setData($postData);
                if ($form->isValid()) {
                    $data = $form->getData();
                    /** @var User $user */
                    $user = $this->userManager->selectUserByLoginOrEmail($data['ident']);
                    if ( $user === null) {

                        return $this->message()->error('user not found by login or email');
                    }
                    $this->shopManager->createCustomShop($user, $data);

                    return $this->message()->success('Shop created');
                }
                if ($isAjax) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($form->getMessages());
                }
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $form->prepare();
        return $this->view([
            'shopCustomForm' => $form
        ]);
    }
}