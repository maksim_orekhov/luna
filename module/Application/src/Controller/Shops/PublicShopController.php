<?php
namespace Application\Controller\Shops;

use Application\Service\CityManager;
use Application\Service\CategoryManager;
use Application\Service\ProductManager;
use Application\Service\ShopManager;
use Application\Service\Status\ShopStatus\PublishedStatus;
use Application\View\Helper\UrlHelper;
use Core\Controller\AbstractRestfulController;
use Core\Controller\Plugin\Message\MessageJsonModelTrait;
use Core\Exception\BaseExceptionCodeInterface;
use ModuleFileManager\Service\FileManager;
use Zend\Paginator\Paginator;

/**
 * Class PublicShopController
 * @package Application\Controller\Shops
 */
class PublicShopController extends AbstractRestfulController
{
    use MessageJsonModelTrait;

    /**
     * @var ShopManager
     */
    private $shopManager;
    /**
     * @var CityManager
     */
    private $cityManager;
    /**
     * @var CategoryManager
     */
    private $categoryManager;
    /**
     * @var PublishedStatus
     */
    private $publishedStatus;
    /**
     * @var ProductManager
     */
    private $productManager;
    /**
     * @var FileManager
     */
    private $fileManager;

    /**
     * PublicShopController constructor.
     * @param ShopManager $shopManager
     * @param CityManager $cityManager
     * @param CategoryManager $categoryManager
     * @param PublishedStatus $publishedStatus
     * @param ProductManager $productManager
     * @param FileManager $fileManager
     */
    public function __construct(ShopManager $shopManager,
                                CityManager $cityManager,
                                CategoryManager $categoryManager,
                                PublishedStatus $publishedStatus,
                                ProductManager $productManager,
                                FileManager $fileManager)
    {
        $this->shopManager = $shopManager;
        $this->cityManager = $cityManager;
        $this->categoryManager = $categoryManager;
        $this->publishedStatus = $publishedStatus;
        $this->productManager = $productManager;
        $this->fileManager = $fileManager;
    }

    /**
     * @return array|bool|\Core\Service\TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function shopListAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        $incomingData = $this->collectIncomingData();
        $queryData = $incomingData['queryData'];
        $routeData = $incomingData['routeData'];

        try {
            $route_params = [
                'search' => isset($queryData['q']) && !empty($queryData['q']) ? trim($queryData['q']) : null,
                'city_all' => isset($routeData['city_all']) && $routeData['city_all'] !== null,
                'city_slug' => $routeData['city_slug'] ?? null,
                'category_slug' => $routeData['category_slug'] ?? null,
                'sub_category_slug' => $routeData['sub_category_slug'] ?? null,
                'page' => isset($queryData['page']) ? trim($queryData['page']) : 1
            ];

            $city_all = $route_params['city_all'];
            $city = $this->cityManager->getCityBySlug($route_params['city_slug']);
            $category = $this->categoryManager->getCategoryBySlug($route_params['category_slug']);
            $subCategory = $this->categoryManager->getCategoryBySlug($route_params['sub_category_slug']);

            if (! $this->shopManager->isRouteShopCorrect($city_all, $city, $category, $subCategory) ) {
                $this->response->setStatusCode(404);
                return [
                    'message' => BaseExceptionCodeInterface::PAGE_NOT_FOUND_MESSAGE
                ];
            }
            /** @var Paginator $paginatorShops */
            $paginatorShops = $this->shopManager->getPublicShopsByRouteData($route_params);

            $categories = $this->categoryManager->getCategories();
            $sub_category = false;

            $categoriesOutput = $this->categoryManager->getCategoriesOutput($categories, $sub_category);

            $shopsOutput = [];
            $paginator_data = [];
            if ($paginatorShops && $paginatorShops->getCurrentItemCount() > 0) {
                $shopsOutput = $this->shopManager->getPublicListOutput($paginatorShops->getCurrentItems(), [
                    'fileManager' => $this->fileManager,
                    'city' => $city,
                ]);
                $paginator_data = [
                    'current_page' => $paginatorShops->getCurrentPageNumber(),
                    'total_item_count' => $paginatorShops->getTotalItemCount(),
                    'item_count_per_page' => $paginatorShops->getItemCountPerPage(),
                ];
            }

            if ($isAjax) {

                return $this->message()->success('Shops', [
                    'shops' => $shopsOutput,
                    'paginator' => $paginator_data
                ]);
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        return $this->view([
            'shops' => $shopsOutput,
            'paginator' => $paginator_data,
            'categories' => $categoriesOutput,
            'init_search' => [
                'catalog_root_url' => $this->url()->fromRoute(UrlHelper::ROUTE_SHOP_LIST, ['city_slug' => 'rossiia']),
                'catalog_search_url' => $this->url()->fromRoute(UrlHelper::ROUTE_PRODUCT_SEARCH),
                'catalog_name' => UrlHelper::CATALOG_PRODUCT,
                'category' => $category ? $category->getId() : null,
                'sub_category' => $subCategory ? $subCategory->getId() : null,
                'search' => $route_params['search'] ?? '',
                'fabricator' => null
            ]
        ]);
    }

    /**
     * @return array|bool|\Core\Service\TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function shopSingleAction()
    {
        try {
            $isAjax = $this->getRequest()->isXmlHttpRequest();

            $incomingData = $this->collectIncomingData();
            $queryData = $incomingData['queryData'];
            $routeData = $incomingData['routeData'];
            $view_mode = isset($queryData['view_mode']) ? trim($queryData['view_mode']) : null;
            $route_params = [
                'shop_slug' => $routeData['shop_slug'] ?? null,
                'search' => isset($queryData['q']) && !empty($queryData['q']) ? trim($queryData['q']) : null,
                'city_slug' => $routeData['city_slug'] ?? null,
                'category_slug' => $routeData['category_slug'] ?? null,
                'sub_category_slug' => $routeData['sub_category_slug'] ?? null,
                'page' => isset($queryData['page']) ? trim($queryData['page']) : 1
            ];

            $shop = $this->shopManager->getShopBySlug($route_params['shop_slug']);
            if (!$shop || ($shop && !$this->publishedStatus->isShopInStatus($shop))) {
                $this->response->setStatusCode(404);
                return [
                    'message' => BaseExceptionCodeInterface::PAGE_NOT_FOUND_MESSAGE
                ];
            }

            $city = $this->cityManager->getCityBySlug($route_params['city_slug']);
            $category = $this->categoryManager->getCategoryBySlug($route_params['category_slug']);
            $subCategory = $this->categoryManager->getCategoryBySlug($route_params['sub_category_slug']);
            //если нет города то считаем что all = true
            $city_all = !(bool) $city;
            if (! $this->shopManager->isRouteShopCorrect($city_all, $city, $category, $subCategory) ) {
                $this->response->setStatusCode(404);
                return [
                    'message' => BaseExceptionCodeInterface::PAGE_NOT_FOUND_MESSAGE
                ];
            }
            $shopOutput = $this->shopManager->getPublicSingleOutput($shop, [
                'fileManager' => $this->fileManager,
                'city' => $city,
            ]);
            /** @var Paginator $paginatorProducts */
            $paginatorProducts = $this->productManager->getPublicProductsByRouteData($route_params);
            $productsOutput = [];
            $paginator_data = [];
            if ($paginatorProducts && $paginatorProducts->getCurrentItemCount() > 0) {
                $productsOutput = $this->productManager->getPublicListOutput($paginatorProducts->getCurrentItems());
                $paginator_data = [
                    'current_page' => $paginatorProducts->getCurrentPageNumber(),
                    'total_item_count' => $paginatorProducts->getTotalItemCount(),
                    'item_count_per_page' => $paginatorProducts->getItemCountPerPage(),
                ];
            }

            if ($isAjax) {

                return $this->message()->success('Products for shop', [
                    'shop' => $shopOutput,
                    'products' => $productsOutput,
                    'paginator' => $paginator_data
                ]);
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        return $this->view([
            'shop' => $shopOutput,
            'products' => $productsOutput,
            'paginator' => $paginator_data,
            'view_mode' => $view_mode,
        ]);
    }
}
