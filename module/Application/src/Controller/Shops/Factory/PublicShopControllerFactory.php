<?php
namespace Application\Controller\Shops\Factory;

use Application\Controller\Shops\PublicShopController;
use Application\Service\CategoryManager;
use Application\Service\CityManager;
use Application\Service\ProductManager;
use Application\Service\ShopManager;
use Application\Service\Status\ShopStatus\PublishedStatus;
use Interop\Container\ContainerInterface;
use ModuleFileManager\Service\FileManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class PublicShopControllerFactory
 * @package Application\Controller\Shops\Factory
 */
class PublicShopControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return PublicShopController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $shopManager = $container->get(ShopManager::class);
        $cityManager = $container->get(CityManager::class);
        $categoryManager = $container->get(CategoryManager::class);
        $productManager = $container->get(ProductManager::class);
        $fileManager = $container->get(FileManager::class);
        $publishedStatus = new PublishedStatus();

        return new PublicShopController(
            $shopManager,
            $cityManager,
            $categoryManager,
            $publishedStatus,
            $productManager,
            $fileManager
        );
    }
}