<?php
namespace Application\Controller\Shops\Factory;

use Application\Controller\Shops\ShopController;
use Application\Service\ShopManager;
use Application\Service\UserManager;
use Core\Service\Base\BaseAuthManager;
use Interop\Container\ContainerInterface;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;

class ShopControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $userManager = $container->get(UserManager::class);
        $rbacManager = $container->get(RbacManager::class);
        $shopManager = $container->get(ShopManager::class);

        return new ShopController(
            $baseAuthManager,
            $userManager,
            $rbacManager,
            $shopManager
        );
    }
}