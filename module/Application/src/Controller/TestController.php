<?php

namespace Application\Controller;

use Application\Entity\City;
use Application\Entity\Country;
use Behat\Transliterator\Transliterator;
use Core\Controller\AbstractRestfulController;
use Core\Service\TwigRenderer;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;
use Zend\Json\Decoder;
use Zend\Math\Rand;


class TestController extends AbstractRestfulController
{
    /**
     * @var TwigRenderer $twigRenderer
     */
    private $twigRenderer;
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * TestController constructor.
     * @param EntityManager $entityManager
     * @param TwigRenderer $twigRenderer
     */
    public function __construct(EntityManager $entityManager,
                                TwigRenderer $twigRenderer)
    {
        $this->entityManager = $entityManager;
        $this->twigRenderer = $twigRenderer;
    }


    public function indexAction()
    {
//        var_dump($this->phraseProcessing($search, $r));
//        var_dump(explode('п', 'Прооессиональная', 2));
        var_dump(Transliterator::transliterate(mb_strtolower('Строительные материалы'), '_'));
        die();
    }

    /**
     * для теста верстки писем
     * @throws \Exception
     */
    public function mailTemplateAction(){
        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel();
        $twigRenderer->setTemplate('twig/test-email');
        $html = $twigRenderer->getHtml();
        print $html;
        exit;
    }
}