<?php
namespace Application\Controller\Purchases\Factory;

use Application\Controller\Purchases\PurchaseController;
use Application\Service\CategoryManager;
use Application\Service\CityManager;
use Application\Service\PurchaseManager;
use Application\Service\Status\ProductStatus\PublishedStatus;
use Application\Service\UserManager;
use Core\Service\Base\BaseAuthManager;
use Interop\Container\ContainerInterface;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;

class PurchaseControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $rbacManager = $container->get(RbacManager::class);
        $purchaseManager = $container->get(PurchaseManager::class);
        $cityManager = $container->get(CityManager::class);
        $categoryManager = $container->get(CategoryManager::class);
        $publishedStatus = new PublishedStatus();

        return new PurchaseController(
            $userManager,
            $baseAuthManager,
            $rbacManager,
            $purchaseManager,
            $cityManager,
            $categoryManager,
            $publishedStatus
        );
    }
}