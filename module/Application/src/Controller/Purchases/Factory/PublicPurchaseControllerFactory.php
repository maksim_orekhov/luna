<?php
namespace Application\Controller\Purchases\Factory;

use Application\Controller\Purchases\PublicPurchaseController;
use Application\Service\CategoryManager;
use Application\Service\CityManager;
use Application\Service\PurchaseManager;
use Application\Service\Status\ProductStatus\PublishedStatus;
use Application\Service\UserManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class PublicPurchaseControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $purchaseManager = $container->get(PurchaseManager::class);
        $cityManager = $container->get(CityManager::class);
        $categoryManager = $container->get(CategoryManager::class);
        $userManager = $container->get(UserManager::class);
        $publishedStatus = new PublishedStatus();

        return new PublicPurchaseController(
            $purchaseManager,
            $cityManager,
            $categoryManager,
            $publishedStatus,
            $userManager
        );
    }
}