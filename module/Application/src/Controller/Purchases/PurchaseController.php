<?php
namespace Application\Controller\Purchases;

use Application\Entity\Purchase;
use Application\Form\PreviewConfirmForm;
use Application\Form\PurchaseForm;
use Application\Form\StartPublishForm;
use Application\Form\StopPublishForm;
use Application\Service\CityManager;
use Application\Service\CategoryManager;
use Application\Service\Output\PurchaseOutput;
use Application\Service\PurchaseManager;
use Application\Service\Status\ProductStatus\PublishedStatus;
use Application\Service\UserManager;
use Application\View\Helper\UrlHelper;
use Core\Controller\AbstractRestfulController;
use Core\Controller\Plugin\Message\MessageJsonModelTrait;
use Core\Service\Base\BaseAuthManager;
use Core\Service\TwigViewModel;
use ModuleRbac\Service\RbacManager;

class PurchaseController extends AbstractRestfulController
{
    use MessageJsonModelTrait;
    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;
    /**
     * @var PurchaseManager
     */
    private $purchaseManager;
    /**
     * @var CityManager
     */
    private $cityManager;
    /**
     * @var CategoryManager
     */
    private $categoryManager;
    /**
     * @var PublishedStatus
     */
    private $publishedStatus;
    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * PurchaseController constructor.
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param RbacManager $rbacManager
     * @param PurchaseManager $purchaseManager
     * @param CityManager $cityManager
     * @param CategoryManager $categoryManager
     * @param PublishedStatus $publishedStatus
     */
    public function __construct(UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                RbacManager $rbacManager,
                                PurchaseManager $purchaseManager,
                                CityManager $cityManager,
                                CategoryManager $categoryManager,
                                PublishedStatus $publishedStatus)
    {

        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->purchaseManager = $purchaseManager;
        $this->cityManager = $cityManager;
        $this->categoryManager = $categoryManager;
        $this->publishedStatus = $publishedStatus;
        $this->rbacManager = $rbacManager;
    }

    /**
     * @param mixed $id
     * @return \Core\Service\TwigViewModel|mixed|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     */
    public function get($id)
    {
        $this->setOnlyAjax();
        $this->message()->setJsonStrategy(true);

        try {
            $user = null;
            if (null !== $this->baseAuthManager->getIdentity()) {
                // Get current user // Can throw Exception
                $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            }
            if (null === $user || !$this->rbacManager->hasRole($user, 'Verified')) {

                return $this->message()->accessDenied();
            }

            /** @var Purchase $purchase */
            $purchase = $this->purchaseManager->getPurchaseById((int)$id);
            if ($user !== $purchase->getUser()) {

                return $this->message()->accessDenied();
            }
            // Get Output
            $purchaseOutput = $this->purchaseManager->getInsideOutput($purchase);

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $data = [
            'purchase' => $purchaseOutput
        ];

        return $this->message()->success('Purchase single', $data);
    }

    /**
     * @param mixed $data
     * @return \Core\Service\TwigViewModel|mixed|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function create($data)
    {
        try {
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            if ($this->rbacManager->hasRole($user, 'Operator')) {

                return $this->message()->accessDenied();
            }

            $categories = $this->categoryManager->getCategories();

            $purchaseForm = new PurchaseForm($categories);
            $previewConfirmForm = new PreviewConfirmForm();
            $previewConfirmForm->setData($data);
            $purchase_data = $this->sessionPlugin()->getSessionVar(PurchaseManager::SESSION_PURCHASE_FLAG);
            $purchase_data['csrf'] = $previewConfirmForm->get('csrf')->getValue();

            $purchaseForm = $this->purchaseManager->fillPurchaseFormWithSessionData(
                $purchaseForm,
                $purchase_data
            );

            if ($previewConfirmForm->isValid() && $purchaseForm->isValid()) {
                //сохраняем закупки
                $purchase = $this->purchaseManager->createPurchase($user, $purchaseForm->getData());
                //отчищаем данные в сессии
                $this->sessionPlugin()->clearSessionVar(PurchaseManager::SESSION_PURCHASE_FLAG);
                //прикрепляем файлы к закупке
                $this->purchaseManager->attachPurchaseFiles($purchase, [
                    'files' => $purchase_data['files'] ?? null
                ]);

                return $this->message()->success('purchase create success', [
                    'purchase' => $this->purchaseManager->getInsideOutput($purchase)
                ]);
            }

            $previewErrors = array_merge_recursive(
                $previewConfirmForm->getMessages(),
                $purchaseForm->getMessages()
            );

            return $this->message()->invalidFormData($previewErrors);

        } catch (\Throwable $t) {

            return $this->message()->exception($t);
        }
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return \Core\Service\TwigViewModel|mixed|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function update($id, $data)
    {
        $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        $purchase = $this->purchaseManager->getPurchaseById($id);
        // Check permissions
        if (!$purchase || !$this->access('purchase.own.edit', ['user' => $user,'purchase' => $purchase]) ) {

            return $this->message()->accessDenied();
        }
        $categories = $this->categoryManager->getCategories();
        $purchaseForm = new PurchaseForm($categories);
        $previewConfirmForm = new PreviewConfirmForm();
        $previewConfirmForm->setData($data);

        $purchase_data = $this->sessionPlugin()->getSessionVar(PurchaseManager::SESSION_PURCHASE_FLAG);
        $purchase_data['csrf'] = $previewConfirmForm->get('csrf')->getValue();
        $purchaseForm = $this->purchaseManager->fillPurchaseFormWithSessionData(
            $purchaseForm,
            $purchase_data
        );

        if ($previewConfirmForm->isValid() && $purchaseForm->isValid()) {
            //сохраняем объявление
            $purchase = $this->purchaseManager->editPurchase($purchase, $purchaseForm->getData());
            //отчищаем данные в сессии
            $this->sessionPlugin()->clearSessionVar(PurchaseManager::SESSION_PURCHASE_FLAG);
            //прикрепляем файлы к объявлению
            $this->purchaseManager->attachPurchaseFiles($purchase, [
                'files' => $purchase_data['files'] ?? null
            ]);

            return $this->message()->success('purchase edit success', [
                'purchase' => $this->purchaseManager->getInsideOutput($purchase)
            ]);
        }

        $previewErrors = array_merge_recursive(
            $previewConfirmForm->getMessages(),
            $purchaseForm->getMessages()
        );

        return $this->message()->invalidFormData($previewErrors);
    }

    /**
     * Удалить объявление навсегда
     *
     * @param mixed $id
     * @return bool|TwigViewModel|mixed|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function delete($id)
    {
        $this->message()->setJsonStrategy(true);

        try {
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            $purchase = $this->purchaseManager->getPurchaseById($id);
            // Check permissions
            if (!$purchase || !$this->access('purchase.own.edit', ['user' => $user,'purchase' => $purchase]) ) {

                return $this->message()->accessDenied();
            }
            //удаление объявления
            $this->purchaseManager->removePurchase($purchase);

            return $this->message()->success('delete successfully');
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @return bool|\Core\Service\TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function createPurchaseAction()
    {
        try {
            $isAjax = $this->getRequest()->isXmlHttpRequest();
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            if ($this->rbacManager->hasRole($user, 'Operator')) {

                return $this->message()->accessDenied();
            }

            $userOutput = $this->userManager->getUserOutput($user);
            $categories = $this->categoryManager->getCategories();
            $form = new PurchaseForm($categories);

            $form->setAttribute('action', $this->url()->fromRoute('purchase-create'));
            /////////////////////////////////////////////
            //если есть параметр то берем данные из сессии
            $is_need_session_data = $this->params()->fromQuery('sd', false) === 'true';
            $purchase_data = $this->purchaseManager->getPurchaseOutputWithSessionOrEntity(
                null,
                $this->sessionPlugin(),
                $is_need_session_data
            );
            $form_data = $this->purchaseManager->getFormDataWithPurchaseData($purchase_data);
            if (!empty($form_data)) {
                $form->setData($form_data);
            }

            ////////////////////////////////////////////////
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                $filesData = $this->purchaseManager->getFilesPostData($this->collectIncomingData()['filesData']);
                ///// step 1: подготавливаем данные PurchaseForm /////
                $preparedData = $this->purchaseManager->prepareDataToPurchase($postData, $filesData);
                ///// step 2: валидация предворительно сохраненных файлов /////
                $preparedData = $this->purchaseManager->getValidationSavedFiles($preparedData);
                ///// step 3: валидация PurchaseForm /////
                $preparedData = $this->purchaseManager->getValidationPurchaseForm($preparedData, $form);

                if ($preparedData['validation']['is_valid']) {
                    // добавить/редактировать файлы и сохранить данные в сессии
                    $purchase_data = $this->purchaseManager->editFilesAndSaveDataInSession($this->sessionPlugin(), $preparedData);

                    return $this->message()->success('purchase create success', [
                        'purchase' => $purchase_data,
                        'user' => $userOutput
                    ]);
                }
                if ($isAjax) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($preparedData['validation']['purchase']);
                }
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $form->prepare();
        return $this->view([
            'categories' => $this->categoryManager->getCategoriesOutput($categories, false),
            'purchase' => $purchase_data,
            'purchaseForm' => $form
        ]);
    }

    /**
     * @return bool|\Core\Service\TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function editPurchaseAction()
    {
        try {
            $isAjax = $this->getRequest()->isXmlHttpRequest();
            $purchase_id = (int)$this->params()->fromRoute('idPurchase', 0);

            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            $userOutput = $this->userManager->getUserOutput($user);
            $purchase = $this->purchaseManager->getPurchaseById($purchase_id);
            // Check permissions
            if (!$purchase || !$this->access('purchase.own.edit', ['user' => $user,'purchase' => $purchase]) ) {

                return $this->message()->accessDenied();
            }

            $categories = $this->categoryManager->getCategories();
            $form = new PurchaseForm($categories);
            $form->setAttribute('action', $this->url()->fromRoute('purchase-edit', ['idPurchase'=> $purchase_id]));
            ////////////////////////////////////////////////////////////////
            //если есть параметр то берем данные из сессии
            $is_need_session_data = $this->params()->fromQuery('sd', false) === 'true';
            $purchase_data = $this->purchaseManager->getPurchaseOutputWithSessionOrEntity(
                $purchase,
                $this->sessionPlugin(),
                $is_need_session_data
            );
            $form_data = $this->purchaseManager->getFormDataWithPurchaseData($purchase_data);
            if (!empty($form_data)) {
                $form->setData($form_data);
            }
            $purchase_data['id'] = $purchase->getId();

            //////////////////////////////////////////////////////////////////////
            $postData = $this->collectIncomingData()['postData'];

            if ($postData) {
                $filesData = $this->purchaseManager->getFilesPostData($this->collectIncomingData()['filesData']);
                ///// step 1: подготавливаем данные PurchaseForm /////
                $preparedData = $this->purchaseManager->prepareDataToPurchase($postData, $filesData);
                ///// step 2: валидация предворительно сохраненных файлов /////
                $preparedData = $this->purchaseManager->getValidationSavedFiles($preparedData);
                ///// step 3: валидация PurchaseForm /////
                $preparedData = $this->purchaseManager->getValidationPurchaseForm($preparedData, $form);

                if ($preparedData['validation']['is_valid']) {
                    // добавить/редактировать файлы и сохранить данные в сессии
                    $purchase_data = $this->purchaseManager->editFilesAndSaveDataInSession($this->sessionPlugin(), $preparedData);
                    $purchase_data['id'] = $purchase->getId();
                    return $this->message()->success('purchase edit success', [
                        'purchase' => $purchase_data,
                        'user' => $userOutput
                    ]);
                }
                if ($isAjax) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($purchase_data['validation']['purchase']);
                }
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $form->prepare();
        return $this->view([
            'categories' => $this->categoryManager->getCategoriesOutput($categories, false),
            'purchase' => $purchase_data
        ]);
    }

    /**
     * @return bool|\Core\Service\TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function stopPublishAction()
    {
        $this->setOnlyPostAndAjax();
        $this->message()->setJsonStrategy(true);

        $user = null;
        if (null !== $this->baseAuthManager->getIdentity()) {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        }

        try {
            if (null === $user || !$this->rbacManager->hasRole($user, 'Verified')) {

                return $this->message()->accessDenied();
            }

            $formDataInit = $this->processFormDataInit();
            /** @var Purchase $purchase */
            $purchase = $formDataInit['purchase'];

            if (null === $purchase) {

                return $this->message()->error('No purchase provided');
            }

            if ($user !== $purchase->getUser()) {

                return $this->message()->accessDenied();
            }

            if (false === $purchase->isVisible()) {

                return $this->message()->error('Purchase is not published');
            }

            $form = new StopPublishForm();
            $postData = $formDataInit['postData'];
            // set data to form
            $form->setData($postData);
            // validate form
            if ($form->isValid()) {
                $this->purchaseManager->stopPublish($purchase);
                // Get Output
                $purchaseOutput = $this->purchaseManager->getInsideOutput($purchase);

                return $this->message()->success('Publishing stopped', ['purchase' => $purchaseOutput]);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @return bool|\Core\Service\TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function startPublishAction()
    {
        $this->setOnlyPostAndAjax();
        $this->message()->setJsonStrategy(true);

        $user = null;
        if (null !== $this->baseAuthManager->getIdentity()) {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        }

        try {
            if (null === $user || !$this->rbacManager->hasRole($user, 'Verified')) {

                return $this->message()->accessDenied();
            }

            $formDataInit = $this->processFormDataInit();
            /** @var Purchase $purchase */
            $purchase = $formDataInit['purchase'];

            if (null === $purchase) {

                return $this->message()->error('No purchase provided');
            }

            if ($user !== $purchase->getUser()) {

                return $this->message()->accessDenied();
            }

            if (true === $purchase->isVisible()) {

                return $this->message()->error('Purchase is already published');
            }

            $form = new StartPublishForm();
            $postData = $formDataInit['postData'];
            // set data to form
            $form->setData($postData);
            // validate form
            if ($form->isValid()) {
                $this->purchaseManager->startPublish($purchase);
                // Get Output
                $purchaseOutput = $this->purchaseManager->getInsideOutput($purchase);

                return $this->message()->success('Publishing started', ['purchase' => $purchaseOutput]);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @return bool|\Core\Service\TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function purchaseSearchAction()
    {
        $this->message()->setJsonStrategy(true);
        try {
            $search = $this->params()->fromQuery('q', null);
            if (empty($search)) {
                return $this->message()->error('Missing search value', null);
            }
            $catalog_root_url = $this->url()->fromRoute(UrlHelper::ROUTE_PURCHASE_LIST, ['city_slug' => 'rossiia']);
            $searchOutput = $this->purchaseManager->purchaseSearchAutoComplete($search, $catalog_root_url);

            return $this->message()->success('product search result', $searchOutput);
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @return array
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        $purchase = null;
        if (isset($incomingData['id']) || isset($incomingData['idPurchase'])) {
            $id = $incomingData['id'] ?? $incomingData['idPurchase'];
            /** @var Purchase $purchase */
            $purchase = $this->purchaseManager->getPurchaseById((int)$id);
        }

        return [
            'purchase' => $purchase,
            'postData' => $incomingData['postData']
        ];
    }

    /**
     * @return array
     */
    private function setOnlyAjax()
    {
        // only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }
    }

    /**
     * @return array
     */
    private function setOnlyPostAndAjax()
    {
        // only ajax
        if (!$this->getRequest()->isXmlHttpRequest() || !$this->getRequest()->isPost()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }
    }
}
