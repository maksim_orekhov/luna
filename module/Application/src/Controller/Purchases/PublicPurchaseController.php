<?php
namespace Application\Controller\Purchases;

use Application\Service\CityManager;
use Application\Service\CategoryManager;
use Application\Service\Output\UserOutput;
use Application\Service\PurchaseManager;
use Application\Service\Status\ProductStatus\PublishedStatus;
use Application\Service\UserManager;
use Application\View\Helper\UrlHelper;
use Core\Controller\AbstractRestfulController;
use Core\Controller\Plugin\Message\MessageJsonModelTrait;
use Core\Exception\BaseExceptionCodeInterface;
use Core\Service\JsonUtil;
use Zend\Paginator\Paginator;

class PublicPurchaseController extends AbstractRestfulController
{
    use MessageJsonModelTrait;

    /**
     * @var PurchaseManager
     */
    private $purchaseManager;
    /**
     * @var CityManager
     */
    private $cityManager;
    /**
     * @var CategoryManager
     */
    private $categoryManager;
    /**
     * @var PublishedStatus
     */
    private $publishedStatus;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * PublicPurchaseController constructor.
     * @param PurchaseManager $purchaseManager
     * @param CityManager $cityManager
     * @param CategoryManager $categoryManager
     * @param PublishedStatus $publishedStatus
     * @param UserManager $userManager
     */
    public function __construct(PurchaseManager $purchaseManager,
                                CityManager $cityManager,
                                CategoryManager $categoryManager,
                                PublishedStatus $publishedStatus,
                                UserManager $userManager)
    {
        $this->purchaseManager = $purchaseManager;
        $this->cityManager = $cityManager;
        $this->categoryManager = $categoryManager;
        $this->publishedStatus = $publishedStatus;
        $this->userManager = $userManager;
    }

    /**
     * @return array|bool|\Core\Service\TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function purchaseListAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        $incomingData = $this->collectIncomingData();
        $queryData = $incomingData['queryData'];

        try {
            $route_params = [
                'search' => $this->params()->fromQuery('q', null),
                'city_slug' => $this->params()->fromRoute('city_slug', null),
                'category_slug' => $this->params()->fromRoute('category_slug', null),
                'sub_category_slug' => $this->params()->fromRoute('sub_category_slug', null),
                'page' => isset($queryData['page']) ? trim($queryData['page']) : 1
            ];

            $city = $this->cityManager->getCityBySlug($route_params['city_slug']);
            $category = $this->categoryManager->getCategoryBySlug($route_params['category_slug']);
            $subCategory = $this->categoryManager->getCategoryBySlug($route_params['sub_category_slug']);

            if (! $this->purchaseManager->isRoutePurchaseCorrect($city, $category, $subCategory) ) {
                $this->response->setStatusCode(404);
                return [
                    'message' => BaseExceptionCodeInterface::PAGE_NOT_FOUND_MESSAGE
                ];
            }

            /** @var Paginator $paginatorPurchases */
            $paginatorPurchases = $this->purchaseManager->getPublishedPurchasesByRouteData($route_params);
            $categories = $this->categoryManager->getCategories();
            $sub_category = false;

            $categoriesOutput = $this->categoryManager->getCategoriesOutput($categories, $sub_category);

            $purchasesOutput = [];
            $paginator_data = [];
            if ($paginatorPurchases && $paginatorPurchases->getCurrentItemCount() > 0) {
                $purchasesOutput = $this->purchaseManager->getPublicListOutput($paginatorPurchases->getCurrentItems());
                $paginator_data = [
                    'current_page' => $paginatorPurchases->getCurrentPageNumber(),
                    'total_item_count' => $paginatorPurchases->getTotalItemCount(),
                    'item_count_per_page' => $paginatorPurchases->getItemCountPerPage(),
                ];
            }

            if ($isAjax) {
                return $this->message()->success('Products', [
                    'purchases' => $purchasesOutput,
                    'paginator' => $paginator_data
                ]);
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        return $this->view([
            'purchases' => $purchasesOutput,
            'paginator' => $paginator_data,
            'categories' => $categoriesOutput,
            'init_search' => [
                'catalog_root_url' => $this->url()->fromRoute(UrlHelper::ROUTE_PURCHASE_LIST, ['city_slug' => 'rossiia']),
                'catalog_search_url' => $this->url()->fromRoute(UrlHelper::ROUTE_PURCHASE_SEARCH),
                'catalog_name' => UrlHelper::CATALOG_PURCHASE,
                'category' => $category ? $category->getId() : null,
                'sub_category' => $subCategory ? $subCategory->getId() : null,
                'search' => $route_params['search'] ?? '',
                'fabricator' => null
            ]
        ]);
    }

    /**
     * @return array|bool|\Core\Service\TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function purchaseSingleAction()
    {
        try {
            $route_params = [
                'city_slug' => $this->params()->fromRoute('city_slug', null),
                'category_slug' => $this->params()->fromRoute('category_slug', null),
                'sub_category_slug' => $this->params()->fromRoute('sub_category_slug', null),
                'purchase_slug' => $this->params()->fromRoute('purchase_slug', null),
            ];
            $purchase = $this->purchaseManager->getPurchaseByCategory($route_params['purchase_slug'], $route_params['sub_category_slug']);
            if (!$purchase || ($purchase && !$this->publishedStatus->isProductInStatus($purchase))) {
                $this->response->setStatusCode(404);
                return [
                    'message' => BaseExceptionCodeInterface::PAGE_NOT_FOUND_MESSAGE
                ];
            }
            $category = $this->categoryManager->getCategoryBySlug($route_params['category_slug']);
            $subCategory = $this->categoryManager->getCategoryBySlug($route_params['sub_category_slug']);

            $purchaseOutput = $this->purchaseManager->getPublicSingleOutput($purchase);
            $categories = $this->categoryManager->getCategories();
            $sub_category = false;

            $categoriesOutput = $this->categoryManager->getCategoriesOutput($categories, $sub_category);
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        return $this->view([
            'purchase' => $purchaseOutput,
            'categories' => $categoriesOutput,
            'init_search' => [
                'catalog_root_url' => $this->url()->fromRoute(UrlHelper::ROUTE_PURCHASE_LIST, ['city_slug' => 'rossiia']),
                'catalog_search_url' => $this->url()->fromRoute(UrlHelper::ROUTE_PURCHASE_SEARCH),
                'catalog_name' => UrlHelper::CATALOG_PURCHASE,
                'category' => $category ? $category->getId() : null,
                'sub_category' => $subCategory ? $subCategory->getId() : null,
                'search' => $route_params['search'] ?? '',
                'fabricator' => null
            ]
        ]);
    }

    /**
     * @return array|bool|\Core\Service\TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function userPurchaseListAction()
    {
        try {
            $isAjax = $this->getRequest()->isXmlHttpRequest();

            $incomingData = $this->collectIncomingData();
            $queryData = $incomingData['queryData'];
            $routeData = $incomingData['routeData'];

            $route_params = [
                'search' => isset($queryData['q']) && !empty($queryData['q']) ? trim($queryData['q']) : null,
                'code' => $routeData['code'] ?? null,
                'per_page' => isset($queryData['per_page']) ? trim($queryData['per_page']) : 8,
                'page' => isset($queryData['page']) ? trim($queryData['page']) : 1
            ];

            if (!$route_params['code']) {
                $this->response->setStatusCode(404);
                return [
                    'message' => BaseExceptionCodeInterface::PAGE_NOT_FOUND_MESSAGE
                ];
            }

            $user = $this->userManager->getUserBySlug($route_params['code']);
            if (!$user) {
                $this->response->setStatusCode(404);
                return [
                    'message' => BaseExceptionCodeInterface::PAGE_NOT_FOUND_MESSAGE
                ];
            }

            /** @var Paginator $paginatedPurchases */
            $paginatedPurchases = $this->purchaseManager->getPublicPurchasesAndPaginatorOutput($user, $route_params);
            $userOutput = $this->userManager->getLargeOutput($user, true, true);
            if ($isAjax) {
                $data = [
                    'purchases' => $paginatedPurchases['purchases'] ?? null,
                    'purchasesPaginator' => $paginatedPurchases['paginator'] ?? null
                ];

                return $this->message()->success('Published purchases for user', $data);
            }
            $purchases_data = [
                'purchases' => $paginatedPurchases['purchases'],
                'public_purchase_collection_url' => UserOutput::getUserPurchasesUrl($user),
                'user_public_purchases_url' => UserOutput::getUserPurchasesUrl($user),
                'user_public_products_url' => UserOutput::getUserProductsUrl($user),
            ];
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        return $this->view([
            'owner' => $userOutput,
            'purchases' => $paginatedPurchases['purchases'],
            'purchasesPaginator' => $paginatedPurchases['paginator'],
            'purchases_data' => $purchases_data
        ]);
    }
}
