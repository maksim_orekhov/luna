<?php
namespace Application\Controller;

use Application\Form\UserForm;
use Application\Service\Output\UserOutput;
use Application\Service\ProductArchiveManager;
use Application\Service\ProductManager;
use Application\Service\ProductModerationManager;
use Application\Service\ProfileManager;
use Application\Service\PurchaseManager;
use Application\Service\UserManager;
use Core\Controller\AbstractRestfulController;
use Core\Controller\Plugin\Message\MessageJsonModelTrait;
use Core\Service\Base\BaseAuthManager;
use Core\Service\TwigViewModel;
use ModuleFileManager\Controller\FileController;
use ModuleRbac\Service\RbacManager;
use Zend\Paginator\Paginator;

class ProfileController extends AbstractRestfulController
{
    use MessageJsonModelTrait;
    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var ProfileManager
     */
    private $profileManager;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * @var ProductManager
     */
    private $productManager;

    /**
     * @var ProductModerationManager
     */
    private $productModerationManager;

    /**
     * @var ProductArchiveManager
     */
    private $productArchiveManager;

    /**
     * @var PurchaseManager
     */
    private $purchaseManager;

    /**
     * ProfileController constructor.
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param ProfileManager $profileManager
     * @param RbacManager $rbacManager
     * @param ProductManager $productManager
     * @param ProductModerationManager $productModerationManager
     * @param ProductArchiveManager $productArchiveManager
     */
    public function __construct(UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                ProfileManager $profileManager,
                                RbacManager $rbacManager,
                                ProductManager $productManager,
                                ProductModerationManager $productModerationManager,
                                ProductArchiveManager $productArchiveManager,
                                PurchaseManager $purchaseManager)
    {
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->profileManager = $profileManager;
        $this->rbacManager = $rbacManager;
        $this->productManager = $productManager;
        $this->productModerationManager = $productModerationManager;
        $this->productArchiveManager = $productArchiveManager;
        $this->purchaseManager = $purchaseManager;
    }

    /**
     * @return bool|TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function profileAction()
    {
        try {
            // набор коллекций для профиля
            $public_products = null;
            $moderation_products = null;
            $decline_products = null;
            $inactive_products = null;
            $archive_products = null;

            $isAjax = $this->getRequest()->isXmlHttpRequest();
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole($user, 'Operator');

            if ($isOperator) {
                // Products
                $moderation_products    = $this->productManager->getModerationProductsAndPaginatorOutput(null, 1, 10);
                $decline_products       = $this->productManager->getDeclineProductsAndPaginatorOutput(null, 1, 10);
                $public_products        = $this->productManager->getPublishedProductsAndPaginatorOutput(null, 1, 10);
                $archive_products       = $this->productManager->getArchiveProductsAndPaginatorOutput(null, 1, 10);
            } else {
                // Products
                $public_products        = $this->productManager->getPublishedProductsAndPaginatorOutput($user, 1, 8);
                $inactive_products      = $this->productManager->getInactiveProductsAndPaginatorOutput($user, 1, 8);
                $archive_products       = $this->productManager->getArchiveProductsAndPaginatorOutput($user, 1, 8);
            }

            /** @var Paginator $publishedProducts */
            $publishedProducts = $this->productManager->getPublishedProductsByUser($user);
            $paginator = $publishedProducts->getPages();

            $userOutput = UserOutput::getLargeOutput($user);
            $userOutput['count_published_products'] = $paginator->totalItemCount ;

            // изменение пользователя
            $userForm = new UserForm($user->getName());
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                $filesData = $this->collectIncomingData()['filesData'];
                if (\count($filesData) && isset($filesData['avatar']) && $filesData['avatar']['error'] === 4) { $filesData = null; }
                if ($filesData) {
                    $postData = array_merge_recursive($postData, $filesData);
                }

                $userForm->setData($postData);
                if ($userForm->isValid()) {
                    $data = $userForm->getData();

                    $user = $this->userManager->editUser($user, $data);
                    $userOutput = UserOutput::getLargeOutput($user);
                    $userOutput['count_published_products'] = $paginator->totalItemCount ;
                    if ($isAjax) {
                        return $this->message()->success('user edit success', ['user' => $userOutput]);
                    }
                } else if ($isAjax) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($userForm->getMessages());
                }
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $products_collections = $this->profileManager->formProductsCollections([
            'public' => $public_products,
            'waiting_moderation' => $moderation_products,
            'declined' => $decline_products,
            'inactive' => $inactive_products,
            'archival' => $archive_products
        ]);

        $userForm->prepare();
        $view = new TwigViewModel([
            'is_operator' => $isOperator,
            'user' => $userOutput,
            'userForm' => $userForm,
            'products_collections' => $products_collections,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        return $view;
    }

    /**
     * @return bool|TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function purchaseAction()
    {
        try {
            // набор коллекций для профиля
            $moderation_purchases = null;
            $decline_purchases = null;
            $public_purchases = null;
            $archive_purchases = null;
            $inactive_purchases = null;
            $purchases_collection_urls = null;

            $isAjax = $this->getRequest()->isXmlHttpRequest();
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole($user, 'Operator');

            if ($isOperator) {
                // Purchases
                $moderation_purchases   = $this->purchaseManager->getModerationPurchasesAndPaginatorOutput(null, 1, 10);
                $decline_purchases      = $this->purchaseManager->getDeclinePurchasesAndPaginatorOutput(null, 1, 10);
                $public_purchases       = $this->purchaseManager->getPublishedPurchasesAndPaginatorOutput(null, 1, 10);
                $archive_purchases      = $this->purchaseManager->getArchivePurchasesAndPaginatorOutput(null, 1, 10);
            } else {
                // Purchases
                $public_purchases       = $this->purchaseManager->getPublishedPurchasesAndPaginatorOutput($user, 1, 8);
                $inactive_purchases     = $this->purchaseManager->getInactivePurchasesAndPaginatorOutput($user, 1, 8);
                $archive_purchases      = $this->purchaseManager->getArchivePurchasesAndPaginatorOutput($user, 1, 8);
            }

            /** @var Paginator $publishedPurchases */
            $publishedPurchases = $this->purchaseManager->getPublishedPurchasesByUser($user);
            $paginator = $publishedPurchases->getPages();

            $userOutput = UserOutput::getLargeOutput($user);
            $userOutput['count_published_products'] = $paginator->totalItemCount ;

            // изменение пользователя
            $userForm = new UserForm();
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                $filesData = $this->collectIncomingData()['filesData'];

                if (\count($filesData) && isset($filesData['avatar']) && $filesData['avatar']['error'] === 4) { $filesData = null; }
                if ($filesData) {
                    $postData = array_merge_recursive($postData, $filesData);
                }

                $userForm->setData($postData);
                if ($userForm->isValid()) {
                    $data = $userForm->getData();

                    $user = $this->userManager->editUser($user, $data);
                    $userOutput = UserOutput::getLargeOutput($user);
                    $userOutput['count_published_products'] = $paginator->totalItemCount;
                    if ($isAjax) {
                        return $this->message()->success('user edit success', ['user' => $userOutput]);
                    }
                } else if ($isAjax) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($userForm->getMessages());
                }
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $purchases_collections = $this->profileManager->formPurchasesCollections([
            'public' => $public_purchases,
            'waiting_moderation' => $moderation_purchases,
            'declined' => $decline_purchases,
            'inactive' => $inactive_purchases,
            'archival' => $archive_purchases
        ]);

        $userForm->prepare();
        $view = new TwigViewModel([
            'is_operator' => $isOperator,
            'user' => $userOutput,
            'userForm' => $userForm,
            'purchases_collections' => $purchases_collections,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return array|bool|TwigViewModel|\Zend\View\Model\JsonModel
     * @throws \Exception
     */
    public function formInitAction()
    {
        //only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        try {
            // Can throw Exception
            $formDataInit = $this->processFormDataInit();
            $data = $formDataInit;
            $data['user']['created_format'] = date('d.m.y', strtotime($formDataInit['user']['created']));
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        return $this->message()->success('formInit', $data);
    }

    /**
     * Показ аватарок
     */
    public function showAvatarAction()
    {
        try{
            $idAvatar = $this->params()->fromRoute('fileId', null);

            return $this->forward()->dispatch(FileController::class, ['id' => (int) $idAvatar]);
        }
        catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }
    }

    /**
     * Подготовка данных для инициализации формы
     *
     * @return array
     * @throws \Throwable
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        $userOutput = [];
        if (null !== $this->baseAuthManager->getIdentity()) {
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            $userOutput = $this->userManager->getUserOutput($user);
        }

        return [
            'user' => $userOutput
        ];
    }
}
