<?php
namespace Application\Controller\Factory;

use Application\Controller\FabricatorController;
use Application\Service\CategoryManager;
use Application\Service\FabricatorManager;
use Application\Service\UserManager;
use Core\Service\Base\BaseAuthManager;
use Interop\Container\ContainerInterface;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;

class FabricatorControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $userManager = $container->get(UserManager::class);
        $rbacManager = $container->get(RbacManager::class);
        $categoryManager = $container->get(CategoryManager::class);
        $fabricatorManager = $container->get(FabricatorManager::class);

        return new FabricatorController(
            $baseAuthManager,
            $userManager,
            $rbacManager,
            $categoryManager,
            $fabricatorManager
        );
    }
}