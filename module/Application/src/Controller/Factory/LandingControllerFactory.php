<?php
namespace Application\Controller\Factory;

use Application\Controller\LandingController;
use Application\Service\CategoryManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class LandingControllerFactory
 * @package Application\Controller\Factory
 */
class LandingControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return LandingController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $categoryManager = $container->get(CategoryManager::class);

        return new LandingController(
            $categoryManager
        );
    }
}