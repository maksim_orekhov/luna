<?php
namespace Application\Controller\Factory;

use Interop\Container\ContainerInterface;
use Application\Controller\TestController;
use Zend\ServiceManager\Factory\FactoryInterface;
use Core\Service\TwigRenderer;

/**
 * This is the factory for TestController
 */
class TestControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $twigRenderer = $container->get(TwigRenderer::class);

        return new TestController(
            $entityManager,
            $twigRenderer
        );
    }
}