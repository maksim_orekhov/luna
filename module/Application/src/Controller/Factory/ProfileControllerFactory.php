<?php
namespace Application\Controller\Factory;

use Application\Controller\ProfileController;
use Application\Service\ProductArchiveManager;
use Application\Service\ProductManager;
use Application\Service\ProductModerationManager;
use Application\Service\ProfileManager;
use Application\Service\PurchaseManager;
use Application\Service\UserManager;
use Core\Service\Base\BaseAuthManager;
use Interop\Container\ContainerInterface;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class ProfileControllerFactory
 * @package Application\Controller\Factory
 */
class ProfileControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ProfileController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $profileManager = $container->get(ProfileManager::class);
        $rbacManager = $container->get(RbacManager::class);
        $productManager = $container->get(ProductManager::class);
        $productModerationManager = $container->get(ProductModerationManager::class);
        $productArchiveManager = $container->get(ProductArchiveManager::class);
        $purchaseManager = $container->get(PurchaseManager::class);

        return new ProfileController(
            $userManager,
            $baseAuthManager,
            $profileManager,
            $rbacManager,
            $productManager,
            $productModerationManager,
            $productArchiveManager,
            $purchaseManager
        );
    }
}