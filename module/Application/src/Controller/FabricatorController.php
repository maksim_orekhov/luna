<?php
namespace Application\Controller;

use Application\Entity\User;
use Application\Form\FabricatorCustomForm;
use Application\Service\CategoryManager;
use Application\Service\FabricatorManager;
use Application\Service\UserManager;
use Core\Controller\AbstractRestfulController;
use Core\Service\Base\BaseAuthManager;
use ModuleRbac\Service\RbacManager;

class FabricatorController extends AbstractRestfulController
{
    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;
    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var RbacManager
     */
    private $rbacManager;
    /**
     * @var CategoryManager
     */
    private $categoryManager;
    /**
     * @var FabricatorManager
     */
    private $fabricatorManager;

    /**
     * FabricatorController constructor.
     * @param BaseAuthManager $baseAuthManager
     * @param UserManager $userManager
     * @param RbacManager $rbacManager
     * @param CategoryManager $categoryManager
     * @param FabricatorManager $fabricatorManager
     */
    public function __construct(BaseAuthManager $baseAuthManager,
                                UserManager $userManager,
                                RbacManager $rbacManager,
                                CategoryManager $categoryManager,
                                FabricatorManager $fabricatorManager
    )
    {

        $this->baseAuthManager = $baseAuthManager;
        $this->userManager = $userManager;
        $this->rbacManager = $rbacManager;
        $this->categoryManager = $categoryManager;
        $this->fabricatorManager = $fabricatorManager;
    }

    /**
     * @return bool|\Core\Service\TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function fabricatorCustomCreateAction()
    {
        try {
            $isAjax = $this->getRequest()->isXmlHttpRequest();
            $current_user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole($current_user, 'Operator');
            if (!$isOperator) {
                return $this->message()->accessDenied();
            }
            $categories = $this->categoryManager->getCategories();
            // изменение пользователя
            $form = new FabricatorCustomForm($categories);
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                $form->setData($postData);
                if ($form->isValid()) {
                    $data = $form->getData();
                    /** @var User $user */
                    $user = $this->userManager->selectUserByLoginOrEmail($data['ident']);

                    if ( $user === null) {

                        return $this->message()->error('user not found by login or email');
                    }

                    $this->fabricatorManager->createCustomFabricator($user, $data);

                    return $this->message()->success('Fabricator created');
                }
                if ($isAjax) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($form->getMessages());
                }
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $form->prepare();
        return $this->view([
            'fabricatorCustomForm' => $form
        ]);
    }
}