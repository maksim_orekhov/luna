<?php

namespace Application\Controller;

use Core\Controller\AbstractRestfulController;
use Zend\Form\Element;

class FeedbackController extends AbstractRestfulController
{
    /**
     * @return array|\Zend\View\Model\JsonModel
     */
    public function complainRequestAction()
    {
        $this->message()->setJsonStrategy(true);

        try {
            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            $queryData = $this->collectIncomingData()['queryData'];
            $data = array_merge_recursive($postData ?? [], $queryData ?? []);

            $csrf = new Element\Csrf('csrf');
            $validator = $csrf->getCsrfValidator();
            if ( !$validator->isValid($data['csrf'] ?? '')) {

                return $this->message()->invalidFormData($validator->getMessages());
            }
            if (array_key_exists('csrf', $data)) {

                unset($data['csrf']);
            }
            $message = [];
            if (!empty($data)) {
                $message = implode('<br>', $data);
            }
            if (!empty($message)) {
                $this->message()->sendSpecialMail($message, null, 'Complain request sending');

                return $this->message()->success('Complain request sending success');
            }

            return $this->message()->error('Sending data is empty');
        }
        catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }
    }

    /**
     * @return array|\Zend\View\Model\JsonModel
     */
    public function safeDealRequestAction()
    {
        $this->message()->setJsonStrategy(true);

        try {
            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            $queryData = $this->collectIncomingData()['queryData'];
            $data = array_merge_recursive($postData ?? [], $queryData ?? []);

            $csrf = new Element\Csrf('csrf');
            $validator = $csrf->getCsrfValidator();
            if ( !$validator->isValid($data['csrf'] ?? '')) {

                return $this->message()->invalidFormData($validator->getMessages());
            }
            if (array_key_exists('csrf', $data)) {

                unset($data['csrf']);
            }
            $message = [];
            if (!empty($data)) {
                $message = implode('<br>', $data);
            }
            if (!empty($message)) {
                $this->message()->sendSpecialMail($message, null, 'Safe deal request sending');

                return $this->message()->success('Safe deal request sending success');
            }

            return $this->message()->error('Sending data is empty');
        }
        catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }
    }

    /**
     * @return array|\Zend\View\Model\JsonModel
     */
    public function fabricatorRequestAction()
    {
        $this->message()->setJsonStrategy(true);

        try {
            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            $queryData = $this->collectIncomingData()['queryData'];
            $data = array_merge_recursive($postData ?? [], $queryData ?? []);

            $csrf = new Element\Csrf('csrf');
            $validator = $csrf->getCsrfValidator();
            if ( !$validator->isValid($data['csrf'] ?? '')) {

                return $this->message()->invalidFormData($validator->getMessages());
            }
            if (array_key_exists('csrf', $data)) {

                unset($data['csrf']);
            }
            $message = [];
            if (!empty($data)) {
                $message = implode('<br>', $data);
            }
            if (!empty($message)) {
                $this->message()->sendSpecialMail($message, null, 'Fabricator request sending');

                return $this->message()->success('Fabricator sending success');
            }

            return $this->message()->error('Sending data is empty');
        }
        catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }
    }

    /**
     * @return array|\Zend\View\Model\JsonModel
     */
    public function shopRequestAction()
    {
        $this->message()->setJsonStrategy(true);

        try {
            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            $queryData = $this->collectIncomingData()['queryData'];
            $data = array_merge_recursive($postData ?? [], $queryData ?? []);

            $csrf = new Element\Csrf('csrf');
            $validator = $csrf->getCsrfValidator();
            if ( !$validator->isValid($data['csrf'] ?? '')) {

                return $this->message()->invalidFormData($validator->getMessages());
            }
            if (array_key_exists('csrf', $data)) {

                unset($data['csrf']);
            }
            $message = [];
            if (!empty($data)) {
                $message = implode('<br>', $data);
            }
            if (!empty($message)) {
                $this->message()->sendSpecialMail($message, null, 'Shop request sending');

                return $this->message()->success('Shop sending success');
            }

            return $this->message()->error('Sending data is empty');
        }
        catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }
    }
}