<?php
namespace Application\Controller;

use Application\Service\CategoryManager;
use Application\View\Helper\UrlHelper;
use Core\Controller\AbstractRestfulController;
use Core\Controller\Plugin\Message\MessageJsonModelTrait;
use Core\Service\JsonUtil;
use Core\Service\TwigViewModel;

class LandingController extends AbstractRestfulController
{
    use MessageJsonModelTrait;
    /**
     * @var CategoryManager
     */
    private $categoryManager;
    /**
     * LandingController constructor.
     * @param CategoryManager $categoryManager
     */
    public function __construct(CategoryManager $categoryManager)
    {
        $this->categoryManager = $categoryManager;
    }

    /**
     * @return bool|TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function landingAction()
    {
        try {
            $categories = $this->categoryManager->getCategories();
            $categoriesOutput = $this->categoryManager->getCategoriesOutput($categories, false);
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        return $this->view([
            'categories' => $categoriesOutput,
            'init_search' => [
                'catalog_root_url' => $this->url()->fromRoute(UrlHelper::ROUTE_PRODUCT_LIST, ['city_slug' => 'rossiia']),
                'catalog_search_url' => $this->url()->fromRoute(UrlHelper::ROUTE_PRODUCT_SEARCH),
                'catalog_name' => UrlHelper::CATALOG_PRODUCT,
                'category' => null,
                'sub_category' => null,
                'search' => '',
                'fabricator' => null
            ]
        ]);
    }
}
