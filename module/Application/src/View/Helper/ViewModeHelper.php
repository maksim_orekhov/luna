<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Class ViewModeHelper
 * @package Application\View\Helper
 */
class ViewModeHelper extends AbstractHelper
{
    const DEFAULT_VIEW_MODE = 'block';

    /**
     * @return $this
     */
    public function __invoke()
    {
        return $this;
    }

    /**
     * @param string|null $view_mode
     * @return string
     */
    public function getViewMode(string $view_mode = null)
    {
        if (null === $view_mode) {

            $cookie = $_COOKIE['view_mode'] ?? null;

            if (null !== $cookie) {

                return $cookie;
            }

            return self::DEFAULT_VIEW_MODE;
        }

        return $view_mode;
    }
}