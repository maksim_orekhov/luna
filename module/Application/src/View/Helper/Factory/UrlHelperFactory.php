<?php
namespace Application\View\Helper\Factory;

use Application\Service\CategoryManager;
use Application\View\Helper\UrlHelper;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\View\Helper\Url;

/**
 * Class UrlHelperFactory
 * @package Application\View\Helper\Factory
 */
class UrlHelperFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return UrlHelper|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $routeMatch = [];
        if ($container->has('Application')) {
            /** @var \Zend\Mvc\Application $application */
            $application = $container->get('Application');

            /** @var \Zend\Mvc\MvcEvent $mvcEvent */
            $mvcEvent = $application->getMvcEvent();
            /** @var \Zend\Router\Http\RouteMatch $routeMatch */
            $routeMatch = $mvcEvent->getRouteMatch() ? $mvcEvent->getRouteMatch()->getParams() : [];
        }
        $categoryManager = $container->get(CategoryManager::class);
        $viewHelperManager = $container->get('ViewHelperManager');
        $urlHelper = $viewHelperManager->get(Url::class);

        return new UrlHelper(
            $urlHelper,
            $categoryManager,
            $routeMatch
        );
    }
}