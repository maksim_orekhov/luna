<?php
namespace Application\View\Helper\Factory;

use Application\Service\ProductManager;
use Application\View\Helper\ProductsHelper;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class ProductsHelperFactory
 * @package Application\View\Helper\Factory
 */
class ProductsHelperFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ProductsHelper|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $productManager = $container->get(ProductManager::class);

        return new ProductsHelper(
            $productManager
        );
    }
}