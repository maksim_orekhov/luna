<?php
namespace Application\View\Helper\Factory;

use Application\View\Helper\SortHelper;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * This is the factory class for AuthAdapter service. The purpose of the factory
 * is to instantiate the service and pass it dependencies (inject dependencies).
 */
class SortHelperFactory implements FactoryInterface
{
    /**
     * This method creates the UserHelper service and returns its instance.
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return SortHelper|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $request = $container->get('Request');

        return new SortHelper($request);
    }
}