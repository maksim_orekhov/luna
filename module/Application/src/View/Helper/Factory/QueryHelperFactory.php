<?php
namespace Application\View\Helper\Factory;

use Application\View\Helper\QueryHelper;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * This is the factory class for AuthAdapter service. The purpose of the factory
 * is to instantiate the service and pass it dependencies (inject dependencies).
 */
class QueryHelperFactory implements FactoryInterface
{
    /**
     * This method creates the UserHelper service and returns its instance.
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $request = $container->get('Request');
        $routeMatch = [];
        if ($container->has('Application')) {
            /** @var \Zend\Mvc\Application $application */
            $application = $container->get('Application');
            /** @var \Zend\Mvc\MvcEvent $mvcEvent */
            $mvcEvent = $application->getMvcEvent();
            /** @var \Zend\Router\Http\RouteMatch $routeMatch */
            $routeMatch = $mvcEvent->getRouteMatch() ? $mvcEvent->getRouteMatch()->getParams() : [];
        }
        return new QueryHelper(
            $request,
            $routeMatch
        );
    }
}