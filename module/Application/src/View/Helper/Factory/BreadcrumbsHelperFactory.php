<?php
namespace Application\View\Helper\Factory;

use Application\Service\CategoryManager;
use Application\Service\CityManager;
use Application\View\Helper\BreadcrumbsHelper;
use Application\View\Helper\UrlHelper;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * This is the factory class for AuthAdapter service. The purpose of the factory
 * is to instantiate the service and pass it dependencies (inject dependencies).
 */
class BreadcrumbsHelperFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return BreadcrumbsHelper|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $routeMatch = [];
        if ($container->has('Application')) {
            /** @var \Zend\Mvc\Application $application */
            $application = $container->get('Application');
            /** @var \Zend\Mvc\MvcEvent $mvcEvent */
            $mvcEvent = $application->getMvcEvent();
            /** @var \Zend\Router\Http\RouteMatch $routeMatch */
            $routeMatch = $mvcEvent->getRouteMatch() ? $mvcEvent->getRouteMatch()->getParams() : [];
        }

        $viewHelperManager = $container->get('ViewHelperManager');
        $urlHelper = $viewHelperManager->get(UrlHelper::class);
        $cityManager = $container->get(CityManager::class);
        $categoryManager = $container->get(CategoryManager::class);

        return new BreadcrumbsHelper(
            $urlHelper,
            $cityManager,
            $categoryManager,
            $routeMatch
        );
    }
}