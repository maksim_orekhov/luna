<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Class VersionHelper
 * @package Application\View\Helper
 */
class VersionHelper extends AbstractHelper
{
    /**
     * @var string
     */
    private $version;

    /**
     * VersionHelper constructor.
     * @param string|null $version
     */
    public function __construct(string $version = null)
    {
        $this->version = $version;
    }

    /**
     * @return $this
     */
    public function __invoke()
    {
        return $this;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        if ($this->version) {

            return $this->version;
        }

        /** @var \DateTime $current_date */
        $current_date = new \DateTime();
        $rand = random_int(100, 999);

        return hash('sha256', (string)$current_date->getTimestamp().''.(string)$rand);
    }
}