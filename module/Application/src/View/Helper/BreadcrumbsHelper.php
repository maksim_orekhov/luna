<?php
namespace Application\View\Helper;

use Application\Entity\City;
use Application\Service\CategoryManager;
use Application\Service\CityManager;
use Core\Service\JsonUtil;
use Zend\View\Helper\AbstractHelper;

/**
 * Class BreadcrumbsHelper
 * @package Application\View\Helper
 */
class BreadcrumbsHelper extends AbstractHelper
{
    /**
     * @var UrlHelper
     */
    private $urlHelper;
    /**
     * @var CityManager
     */
    private $cityManager;
    /**
     * @var CategoryManager
     */
    private $categoryManager;
    /**
     * @var array
     */
    private $routeMatch;

    /**
     * @return $this
     */
    public function __invoke()
    {
        return $this;
    }

    /**
     * BreadcrumbsHelper constructor.
     * @param UrlHelper $urlHelper
     * @param CityManager $cityManager
     * @param CategoryManager $categoryManager
     * @param array $routeMatch
     */
    public function __construct(UrlHelper $urlHelper,
                                CityManager $cityManager,
                                CategoryManager $categoryManager,
                                array $routeMatch)
    {

        $this->urlHelper = $urlHelper;
        $this->cityManager = $cityManager;
        $this->categoryManager = $categoryManager;
        $this->routeMatch = $routeMatch;
    }

    /**
     * @return array
     */
    public function toArray():array
    {
        $catalog_root_url = $this->urlHelper->catalog_root_url();
        $catalog_city_url = $this->urlHelper->catalog_city_url();

        $city_slug = $this->routeMatch['city_slug'] ?? null;
        $category_slug = $this->routeMatch['category_slug'] ?? null;
        $sub_category_slug = $this->routeMatch['sub_category_slug'] ?? null;

        $city = $this->cityManager->getCityBySlug($city_slug);
        $category = $this->categoryManager->getCategoryBySlug($category_slug);
        $subCategory = $this->categoryManager->getCategoryBySlug($sub_category_slug);

        if ($subCategory && !$subCategory->getParent()){
            $subCategory = null;
        }


        $breadcrumbs[] = [
            'name' => 'Главная',
            'url' => '/',
        ];

        if ($city) {
            $city_slug = $city->getSlug();
            $city_name = $city->getName();
            if (!$category && !$subCategory && $city->getType() !== City::COUNTRY_TYPE) {
                $city_name = $this->getBreadcrumbsCountryName();
            } elseif ($city->getType() === City::COUNTRY_TYPE) {
                $this->urlHelper->setCountryRouteParams($city_slug);
                $catalog_root_url = $this->urlHelper->catalog_root_url();
                $city_name = $this->getBreadcrumbsCountryName();
            } else {
                $catalog_city_url = $this->urlHelper->catalog_city_url($city_slug);
            }

            $breadcrumbs[] = [
                'name' => $city_name,
                'url' => $catalog_city_url,
            ];
        }
        if ($category) {
            $breadcrumbs[] = [
                'name' => $category->getName(),
                'url' => $catalog_root_url.CategoryManager::getCategoryUrl($category),
            ];
        }
        if ($subCategory) {
            $breadcrumbs[] = [
                'name' => $subCategory->getName(),
                'url' => $catalog_root_url.CategoryManager::getCategoryUrl($subCategory),
            ];
        }

        return $breadcrumbs;
    }

    private function getBreadcrumbsCountryName()
    {
        $catalog_type = $this->urlHelper->catalog_type();
        switch ($catalog_type) {
            case UrlHelper::CATALOG_PRODUCT:
                return 'Все товары';
            case UrlHelper::CATALOG_PURCHASE:
                return 'Все закупки';
            default:
                return 'Все товары';
        }
    }

    /**
     * @return string
     */
    public function toJson()
    {
       return JsonUtil::mb_encode($this->toArray());
    }
}