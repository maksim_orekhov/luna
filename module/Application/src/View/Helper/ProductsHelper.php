<?php
namespace Application\View\Helper;

use Application\Service\ProductManager;
use Zend\View\Helper\AbstractHelper;

/**
 * Class ProductsHelper
 * @package Application\View\Helper
 */
class ProductsHelper extends AbstractHelper
{
    private $productManager;

    /**
     * @return $this
     */
    public function __invoke()
    {
        return $this;
    }

    /**
     * ProductsHelper constructor.
     * @param ProductManager $productManager
     */
    public function __construct(ProductManager $productManager)
    {
        $this->productManager = $productManager;
    }

    /**
     * @param array $criterion
     * @return array
     */
    public function getLastProductsByCriteria(array $criterion = []): array
    {
        $lastProducts = $this->productManager->getLastProductsByCriteria($criterion);

        return $this->productManager->getPublicListOutput($lastProducts) ?? [];
    }
}