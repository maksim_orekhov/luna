<?php
namespace Application\View\Helper;

use Core\Service\JsonUtil;
use Zend\View\Helper\AbstractHelper;

/**
 * Class JsonUtilHelper
 * @package Application\View\Helper
 */
class JsonUtilHelper extends AbstractHelper
{
    /**
     * @return $this
     */
    public function __invoke()
    {
        return $this;
    }

    public function mb_encode($data): string
    {
        return JsonUtil::mb_encode(\is_array($data) ? $data : []);
    }
}