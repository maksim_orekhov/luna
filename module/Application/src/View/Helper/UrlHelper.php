<?php
namespace Application\View\Helper;

use Application\Service\CategoryManager;
use Zend\View\Helper\AbstractHelper;
use Zend\View\Helper\Url;

/**
 * Class RouteHelper
 * @package Application\View\Helper
 */
class UrlHelper extends AbstractHelper
{
    const CATALOG_KEY = 'catalog_type';

    const CATALOG_PRODUCT = 'product';
    const CATALOG_PURCHASE = 'purchase';
    const CATALOG_SHOP = 'shop';
    const CATALOG_SHOPS = 'shops';

    const ROUTE_PRODUCT_LIST = 'dynamic-route-product-list';
    const ROUTE_PURCHASE_LIST = 'dynamic-route-purchase-list';
    const ROUTE_SHOP_LIST = 'dynamic-route-shop-list';

    const ROUTE_PRODUCT_SINGLE = 'dynamic-route-product-single';
    const ROUTE_PURCHASE_SINGLE = 'dynamic-route-purchase-single';
    const ROUTE_SHOP_SINGLE = 'dynamic-route-shop-single';

    const ROUTE_PRODUCT_SEARCH = 'api-product-search';
    const ROUTE_PURCHASE_SEARCH = 'api-purchase-search';
    const ROUTE_SHOP_SEARCH = 'api-shop-search';

    /**
     * @var array
     */
    private $country_route_params;
    /**
     * @var Url
     */
    private $urlHelper;
    /**
     * @var array
     */
    private $routeMatch;
    /**
     * @var CategoryManager
     */
    private $categoryManager;

    /**
     * @return $this
     */
    public function __invoke()
    {
        return $this;
    }

    /**
     * UrlHelper constructor.
     * @param Url $urlHelper
     * @param CategoryManager $categoryManager
     * @param array $routeMatch
     */
    public function __construct(Url $urlHelper,
                                CategoryManager $categoryManager,
                                array $routeMatch)
    {

        $this->urlHelper = $urlHelper;
        $this->categoryManager = $categoryManager;
        $this->routeMatch = $routeMatch;

        $this->setCountryRouteParams();
    }

    public function setCountryRouteParams($city_slug = 'rossiia')
    {
        $this->country_route_params = ['city_slug' => $city_slug];
    }

    /**
     * @param null $name
     * @param array $params
     * @param array $options
     * @param bool $reuseMatchedParams
     * @return string
     */
    public function url($name = null, array $params = [], array $options = [], $reuseMatchedParams = false): string
    {
        $url = $this->urlHelper;

        return $url($name, $params, $options, $reuseMatchedParams);
    }

    /**
     * данные для инициализации меню
     * @return array
     */
    public function init_menu(): array
    {
        return [
            'catalog_name' => $this->catalog_type(),
            'catalog_root_url' => $this->catalog_root_url(),
            'product_root_url' => $this->product_root_url(),
            'purchase_root_url' => $this->purchase_root_url(),
            'purchase_create_url' => $this->url('purchase-create'),
            'product_create_url' => $this->url('product-create'),
            'categories' => $this->categoryManager->getCategoriesOutput($this->categoryManager->getCategories(), true),
        ];
    }

    /**
     * root url объявлений
     * @return string
     */
    public function product_root_url(): string
    {
        return $this->url(self::ROUTE_PRODUCT_LIST, $this->country_route_params);
    }

    /**
     * root url закупок
     * @return string
     */
    public function purchase_root_url(): string
    {
        return $this->url(self::ROUTE_PURCHASE_LIST, $this->country_route_params);
    }

    /**
     * root url закупок
     * @return string
     */
    public function shop_root_url(): string
    {
        return $this->url(self::ROUTE_SHOP_LIST, $this->country_route_params);
    }

    /**
     * вычисляется текущий root url
     * @return string
     */
    public function catalog_root_url(): string
    {
        return $this->url($this->getCatalogRootRoute(), $this->country_route_params);
    }

    /**
     * вычисляется текущий root url оносительно заданного города
     * @param string $city_slug
     * @return string
     */
    public function catalog_city_url($city_slug = 'rossiia'): string
    {
        return $this->url($this->getCatalogRootRoute(), ['city_slug' => $city_slug]);
    }

    /**
     * вычисляется url для автокомплита относительно текущего root url
     * @return string
     */
    public function catalog_search_url(): string
    {
        return $this->url($this->getCatalogSearchRoute());
    }

    /**
     * получаем route name для текущего каталога
     * @return string
     */
    public function getCatalogRootRoute(): string
    {
        if (isset($this->routeMatch[self::CATALOG_KEY])) {
            switch ($this->routeMatch[self::CATALOG_KEY]) {
                case self::CATALOG_PRODUCT:
                    return self::ROUTE_PRODUCT_LIST;
                case self::CATALOG_PURCHASE:
                    return self::ROUTE_PURCHASE_LIST;
            }
        }
        //default route
        return self::ROUTE_PRODUCT_LIST;
    }

    /**
     * получаем тип каталого 'product|purchase|shops' и т.д
     * @return string
     */
    public function catalog_type(): string
    {
        if (isset($this->routeMatch[self::CATALOG_KEY])) {
            switch ($this->routeMatch[self::CATALOG_KEY]) {
                case self::CATALOG_PRODUCT:
                    return self::CATALOG_PRODUCT;
                case self::CATALOG_PURCHASE:
                    return self::CATALOG_PURCHASE;
                case self::CATALOG_SHOP:
                    return self::CATALOG_SHOP;
            }
        }
        //default type
        return self::CATALOG_PRODUCT;
    }

    /**
     * @return string
     */
    public function is_catalog_shop(): string
    {
        if (isset($this->routeMatch[self::CATALOG_KEY])) {
            return $this->routeMatch[self::CATALOG_KEY] === self::CATALOG_SHOP;
        }
        //default type
        return false;
    }

    /**
     * @return string
     */
    public function is_catalog_purchase(): string
    {
        if (isset($this->routeMatch[self::CATALOG_KEY])) {
            return $this->routeMatch[self::CATALOG_KEY] === self::CATALOG_PURCHASE;
        }
        //default type
        return false;
    }

    /**
     * @return string
     */
    public function is_catalog_product(): string
    {
        if (isset($this->routeMatch[self::CATALOG_KEY])) {
            return $this->routeMatch[self::CATALOG_KEY] === self::CATALOG_PRODUCT;
        }
        //default type
        return false;
    }

    /**
     * получаем route name для автокомплита по текущему каталогу
     * @return string
     */
    public function getCatalogSearchRoute(): string
    {
        if (isset($this->routeMatch[self::CATALOG_KEY])) {
            switch ($this->routeMatch[self::CATALOG_KEY]) {
                case self::CATALOG_PRODUCT:
                    return self::ROUTE_PRODUCT_SEARCH;
                case self::CATALOG_PURCHASE:
                    return self::ROUTE_PURCHASE_SEARCH;
            }
        }
        //default route
        return self::ROUTE_PRODUCT_SEARCH;
    }
}