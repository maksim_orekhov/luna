<?php

namespace Application\Listener;

use Application\Entity\Product;
use Application\Service\ProductManager;
use Application\EventManager\ProductEventProvider as ProductEvent;
use Core\Exception\LogicException;
use Core\Listener\ListenerAggregateTrait;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;

/**
 * Class ProductListenerAggregate
 * @package Application\Listener
 */
class ProductListenerAggregate implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;
    /**
     * @var ProductManager
     */
    private $productManager;

    /**
     * AdvertListenerAggregate constructor.
     * @param ProductManager $productManager
     */
    public function __construct(ProductManager $productManager)
    {
        $this->productManager = $productManager;
    }

    /**
     * @param EventManagerInterface $events
     * @param int $priority
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach(ProductEvent::EVENT_SUCCESS_CREATED_PRODUCT, [$this, 'successCreatedProduct'], $priority);
    }

    /**
     * @param EventInterface $event
     * @throws LogicException
     */
    public function successCreatedProduct(EventInterface $event)
    {
        $params = $event->getParam('success_created_product_params', null);
        $event->setParam('success_created_product_params', null);
        if (!\is_array($params) ||
            !array_key_exists('product', $params)) {

            throw new LogicException(null, LogicException::PRODUCT_ENTITY_NOT_PROVIDED);
        }
        /** @var Product $product */
        $product = $params['product'];

        // 1. прикрепиляем категории
        // 2. прикрепиляем города
    }
}