<?php

namespace Application\Listener;

use Application\Entity\User;
use Core\Entity\Interfaces\EmailInterface;
use Core\EventManager\AuthEventProvider as AuthEvent;
use Core\Exception\LogicException;
use Core\Listener\ListenerAggregateTrait;
use Core\Service\Base\BaseAuthManager;
use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BaseUserManager;
use Core\Service\SessionContainerManager;
use Core\Service\TwigViewModel;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Form\Form;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;

/**
 * Class AuthListenerAggregate
 * @package Application\Listener
 */
class AuthListenerAggregate implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;
    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;
    /**
     * @var BaseUserManager
     */
    private $baseUserManager;

    /**
     * AuthListenerAggregate constructor.
     * @param BaseAuthManager $baseAuthManager
     * @param BaseUserManager $baseUserManager
     */
    public function __construct(BaseAuthManager $baseAuthManager,
                                BaseUserManager $baseUserManager)
    {
        $this->baseAuthManager = $baseAuthManager;
        $this->baseUserManager = $baseUserManager;
    }

    /**
     * @param EventManagerInterface $events
     * @param int $priority
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        //auth
        $this->listeners[] = $events->attach(AuthEvent::EVENT_BEFORE_AUTH, [$this, 'beforeAuth'], $priority);
        $this->listeners[] = $events->attach(AuthEvent::EVENT_AFTER_AUTH, [$this, 'afterAuth'], $priority);
    }

    /**
     * @param EventInterface $event
     * @throws LogicException
     */
    public function beforeAuth(EventInterface $event)
    {
        /**
         * @var User $user
         * @var MvcEvent $mvcEvent
         */
        if ($this->baseAuthManager->getIdentity() !== null) {

            $user = $this->baseUserManager->getUserByLogin($this->baseAuthManager->getIdentity());
            if (!$user) {

                throw new LogicException(null, LogicException::USER_NOT_FOUND);
            }

            $redirect_url = $this->getRedirectUrlForLoggedUser($user, $event);

            $application = $event->getTarget();
            $application->redirect()->toUrl($redirect_url);
        }
    }

    /**
     * @param EventInterface $event
     * @throws LogicException
     */
    public function afterAuth(EventInterface $event)
    {
        /**
         * @var User $user
         * @var MvcEvent $mvcEvent
         */
        if ($this->baseAuthManager->getIdentity() !== null) {

            $user = $this->baseUserManager->getUserByLogin($this->baseAuthManager->getIdentity());
            if (!$user) {

                throw new LogicException(null, LogicException::USER_NOT_FOUND);
            }

            $redirect_url = $this->getRedirectUrlForLoggedUser($user, $event);

            $application = $event->getTarget();
            /** @var MvcEvent $mvcEvent */
            $mvcEvent = $application->getEvent();
            $mvcEvent->setParam('redirect_url', $redirect_url);
        }
    }

    /**
     * @param User $user
     * @param EventInterface $event
     * @return string
     */
    private function getRedirectUrlForLoggedUser(User $user, EventInterface $event): string
    {
        $application = $event->getTarget();
        /** @var string $redirect_url */
        $redirect_url = $application->params()->fromQuery('redirectUrl', null);

        if ( $redirect_url ) {

            return $redirect_url;
        }

        $emailIsVerified = $user->getEmail() ? $user->getEmail()->getIsVerified() : false;
        $phoneIsVerified = $user->getPhone() ? $user->getPhone()->getIsVerified() : false;

        if ( $phoneIsVerified && $emailIsVerified ) {

            return $application->url()->fromRoute('profile');
        }

        return $application->url()->fromRoute('profile');
    }
}