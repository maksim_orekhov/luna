<?php
namespace Application\Listener\Factory;

use Application\Listener\ProductListenerAggregate;
use Application\Service\ProductManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class ProductListenerAggregateFactory
 * @package Application\Listener\Factory
 */
class ProductListenerAggregateFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ProductListenerAggregate|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $advertManager = $container->get(ProductManager::class);

        return new ProductListenerAggregate(
            $advertManager
        );
    }
}