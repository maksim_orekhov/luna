<?php
namespace Application\Listener\Factory;

use Application\Listener\AuthListenerAggregate;
use Core\Service\Base\BaseAuthManager;
use Core\Service\Base\BaseUserManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class AuthListenerAggregateFactory
 * @package Application\Listener\Factory
 */
class AuthListenerAggregateFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $baseUserManager = $container->get(BaseUserManager::class);

        return new AuthListenerAggregate(
            $baseAuthManager,
            $baseUserManager
        );
    }
}