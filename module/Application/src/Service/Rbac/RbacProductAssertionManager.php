<?php
namespace Application\Service\Rbac;

use Zend\Permissions\Rbac\Rbac;

/**
 * Class RbacProductAssertionManager
 * @package Application\Service\Rbac
 */
class RbacProductAssertionManager
{
    /**
     * For dynamic assertions
     *
     * @param Rbac      $rbac
     * @param string    $permission
     * @param array     $params
     * @return bool
     */
    public function assert(Rbac $rbac, $permission, $params)
    {
        if( (!array_key_exists('user', $params) || $params['user'] === null) ||
        (!array_key_exists('product', $params) || $params['product'] === null)) {

            return false;
        }
        if($permission === 'product.own.view' || $permission === 'product.own.edit') {

            if ($params['user'] === $params['product']->getUser()) {

                return true;
            }
        }

        return false;
    }
}