<?php
namespace Application\Service\Rbac;

use Zend\Permissions\Rbac\Rbac;

/**
 * Class RbacPurchaseAssertionManager
 * @package Application\Service\Rbac
 */
class RbacPurchaseAssertionManager
{
    /**
     * For dynamic assertions
     *
     * @param Rbac      $rbac
     * @param string    $permission
     * @param array     $params
     * @return bool
     */
    public function assert(Rbac $rbac, $permission, $params)
    {
        if( (!array_key_exists('user', $params) || $params['user'] === null) ||
        (!array_key_exists('purchase', $params) || $params['purchase'] === null)) {

            return false;
        }
        if($permission === 'purchase.own.view' || $permission === 'purchase.own.edit') {

            if ($params['user'] === $params['purchase']->getUser()) {

                return true;
            }
        }

        return false;
    }
}