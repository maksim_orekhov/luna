<?php
namespace Application\Service\Rbac;

use Application\Entity\User;
use ModuleRbac\Service\Rbac;

/**
 * This service is used for invoking user-defined RBAC dynamic assertions
 */
class RbacProfileAssertionManager
{
    /**
     * For dynamic assertions
     *
     * @param Rbac      $rbac
     * @param string    $permission
     * @param array     $params
     * @return bool
     */
    public function assert(Rbac $rbac, $permission, $params)
    {
        if (!array_key_exists('user', $params)
        ) {

            return false;
        }

        // Get user object by login from Identity
        try {
            $currentUser = $rbac->getUser();
        }
        catch (\Throwable $t) {

            return false;
        }

        if ($permission === 'profile.own.edit') {

            // Проверка, что User из параметров и $currentUser один и тот же пользователь
            if (array_key_exists('user', $params) && $params['user'] instanceof User) {

                return $currentUser === $params['user'];
            }
        }

        return false;
    }
}