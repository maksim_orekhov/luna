<?php
namespace Application\Service;

use Application\Controller\Products\PublicProductController;
use Application\Entity\Category;
use Application\Entity\City;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;
use Zend\Cache\Storage\Adapter\Filesystem;
use Zend\Cache\Storage\Adapter\FilesystemOptions;
use Zend\Router\Http\Segment;

class RouteManager
{
    const CACHE_DIR = './data/cache/routes';
    const CACHE_NAMESPACE = 'route';
    private $product_routes = [
        'type' => Segment::class,
        'options' => [
            'route' => '/:product_slug',
            'constraints' => [
                'product_slug' => '[\w0-9_-]+',
            ],
            'defaults' => [
                'controller' => PublicProductController::class,
                'action' => 'productSingle',
            ],
        ],
    ];
    private $sub_category_routes = [
        'type' => Segment::class,
        'options' => [
            'route' => '/:sub_category_slug',
            'constraints' => [
                'sub_category_slug' => '[\w0-9_-]+',
            ],
            'defaults' => [
                'controller' => PublicProductController::class,
                'action' => 'productList',
            ],
        ],
    ];

    private $city_routes = [
        'type' => Segment::class,
        'options' => [
            'route' => '/:city_slug',
            'constraints' => [
                'city_slug' => '',
            ],
            'defaults' => [
                'controller' => PublicProductController::class,
                'action' => 'productList',
            ],
        ],
    ];

    private $category_routes = [
        'type' => Segment::class,
        'options' => [
            'route' => '/:category_slug',
            'constraints' => [
                'category_slug' => '',
            ],
            'defaults' => [
                'controller' => PublicProductController::class,
                'action' => 'productList',
            ],
        ]
    ];
    /**
     * @var array|ArrayCollection|PersistentCollection
     */
    private $categories;
    /**
     * @var array|ArrayCollection|PersistentCollection
     */
    private $cities;

    /**
     * @var CategoryManager
     */
    private $categoryManager;

    /**
     * @var array
     */
    private $routes;
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var CityManager
     */
    private $cityManager;
    /**
     * @var Filesystem
     */
    private $cache;
    /**
     * @var array
     */
    private $config;
    /**
     * @var string
     */
    private $cache_id;

    /**
     * ProductRouteManager constructor.
     * @param EntityManager $entityManager
     * @param CategoryManager $categoryManager
     * @param CityManager $cityManager
     * @param Filesystem $cache
     * @param array $config
     */
    public function __construct(EntityManager $entityManager,
                                CategoryManager $categoryManager,
                                CityManager $cityManager,
                                Filesystem $cache,
                                array $config)
    {
        $this->entityManager = $entityManager;
        $this->categoryManager = $categoryManager;
        $this->cityManager = $cityManager;
        $this->cache = $cache;
        $this->config = $config;
    }

    public function initCache()
    {
        if(!is_dir(self::CACHE_DIR) && !mkdir($concurrentDirectory = self::CACHE_DIR, 0755, true) && !is_dir($concurrentDirectory)) {
            throw new \RuntimeException('error create cache folder');
        }
        $optionsCache = new FilesystemOptions([
            'cache_dir' => self::CACHE_DIR,
            'namespace' => self::CACHE_NAMESPACE,
            'ttl' => $this->config['route_cache_max_age']
        ]);
        $this->cache->setOptions($optionsCache);
        $this->cache->clearExpired();
    }

    /**
     * @return mixed
     */
    public function setCacheId()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $category_id = $this->entityManager->getRepository(Category::class)
            ->getNextCategoryAutoIncrementId();
        $city_id = $this->entityManager->getRepository(City::class)
            ->getNextCityAutoIncrementId();

        return $category_id.'_'.$city_id;
    }

    /**
     * @return string
     */
    public function getCacheId(): string
    {
        if (! $this->cache_id) {
            return $this->setCacheId();
        }

        return $this->cache_id;
    }

    /**
     * @return array
     */
    public function getRoutes(): array
    {
        $this->initCache();
        $cache_id = $this->getCacheId();

        if (null !== ($result = $this->cache->getItem($cache_id))) {
            $this->routes = $result;

            return $this->routes;
        }

        $this->categories = $this->categoryManager->getCategories();
//        $this->cities = $this->cityManager->getOnlyCities();

        /** @var Category $category */
        foreach ($this->categories as $category) {
            $category_slug = $category->getSlug();

            $this->routes[$category_slug] = $this->category_routes;
            $this->routes[$category_slug]['options']['constraints']['category_slug'] = $category_slug;
            $this->routes[$category_slug]['may_terminate'] = true;
            $this->routes[$category_slug]['child_routes']['sub_category_routes'] = $this->sub_category_routes;
            $this->routes[$category_slug]['child_routes']['sub_category_routes']['may_terminate'] = true;
            $this->routes[$category_slug]['child_routes']['sub_category_routes']['child_routes']['product_routes'] = $this->product_routes;
        }

//        /** @var City $category */
//        foreach ($this->cities as $city) {
//            $city_slug = $city->getSlug();
//
//            $this->routes[$city_slug] = $this->city_routes;
//            $this->routes[$city_slug]['options']['constraints']['city_slug'] = $city_slug;
//            $this->routes[$city_slug]['may_terminate'] = true;
//            $this->routes[$city_slug]['child_routes']['sub_category_routes'] = $this->sub_category_routes;
//        }

        $this->cache->setItem($cache_id, $this->routes);

        return $this->routes;
    }
}