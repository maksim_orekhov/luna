<?php
namespace Application\Service;

use Application\Entity\Address;
use Application\Entity\Interfaces\ProductInterface;
use Application\Entity\Product;
use Application\Entity\City;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;

class CityManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var array
     */
    public  $onlyCities;

    /**
     * CityManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $id
     * @return null|object
     */
    public function getCity(int $id)
    {
        return $this->entityManager->getRepository(City::class)->find($id);
    }

    /**
     * @return array
     */
    public function getOnlyCities():array
    {
        /** @var array $onlyCities */
        $this->onlyCities = $this->entityManager->getRepository(City::class)
            ->findBy(['type' => City::CITY_TYPE]);

        return $this->onlyCities;
    }

    /**
     * @param $slug
     * @return City|null
     */
    public function getCityBySlug($slug)
    {
        /** @var City|null $city */
        $city = $this->entityManager->getRepository(City::class)
            ->findOneBy(['slug'=> $slug]);

        return $city;
    }

    /**
     * @param ProductInterface $advert
     * @param Address $address
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function linkAddressWithCities(ProductInterface $advert, Address $address)
    {
        /** @var ArrayCollection|PersistentCollection $productCities */
        $productCities = $advert->getCities();
        if ($productCities) {
            $productCities->clear();
            $this->entityManager->persist($advert);
            $this->entityManager->flush($advert);
            $this->entityManager->refresh($advert);
        }

        $cityCollection = new ArrayCollection();
        $addressCountry = $address->getCountry();
        $addressRegion = $address->getRegion();
        $addressCity = $address->getCity();
        /** @var City $country */
        $country = $this->entityManager->getRepository(City::class)
            ->findOneBy(['name' => $addressCountry, 'type' => City::COUNTRY_TYPE]);
        if (!$country) {
            $country = $this->entityManager->getRepository(City::class)
                ->findOneBy(['slug' => 'rossiia', 'type' => City::COUNTRY_TYPE]);
        }
        if ($country) {
            $cityCollection->add($country);
        }
        /** @var City $region */
        $region = $this->entityManager->getRepository(City::class)
            ->findRegionOneByNameAndCountry($country, $addressRegion);
        if ($region) {
            $cityCollection->add($region);
            /** @var City $region */
            $city = $this->entityManager->getRepository(City::class)
                ->findCityOneByNameAndRegion($region, $addressCity);
            if ($city) {
                $cityCollection->add($city);
            }
        }

        $advert->setCities($cityCollection);

        $this->entityManager->persist($advert);
        $this->entityManager->flush($advert);
    }

    /**
     * @param ProductInterface $entity
     * @param City $item
     * @return string
     */
    public static function getPartUrlCityByProduct(ProductInterface $entity, City $item = null): string
    {
        /**
         * @var ArrayCollection|PersistentCollection $cities
         * @var City $item
         */
        if (!$item instanceof City) {
            $cities = $entity->getCities();
            $item = $cities->filter(function (City $entity) {
                return $entity->getType() === $entity::CITY_TYPE;
            })->first();
            if (!$item instanceof City) {
                $item = $cities->filter(function (City $entity) {
                    return $entity->getType() === $entity::REGION_TYPE;
                })->first();
            }
            if (!$item instanceof City) {
                $item = $cities->filter(function (City $entity) {
                    return $entity->getType() === $entity::COUNTRY_TYPE;
                })->first();
            }
        }

        return self::getPartUrlByCity($item);
    }

    /**
     * @param City|null $city
     * @return string
     */
    public static function getPartUrlByCity(City $city = null): string
    {
        return $city instanceof City ? '/'.$city->getSlug() : '';
    }

    /**
     * @param ArrayCollection|PersistentCollection $collections
     * @param $name
     * @return mixed
     */
    public function filteredCollectionByName($collections, $name)
    {
        return $collections->filter(function(City $entity) use ($name) {
            return $entity->getName() === $name;
        });
    }
}