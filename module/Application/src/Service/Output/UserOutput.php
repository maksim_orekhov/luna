<?php
namespace Application\Service\Output;

use Application\Entity\Email;
use Application\Entity\Phone;
use Application\Entity\User;
use Application\Service\ProductManager;
use Zend\Paginator\Paginator;

/**
 * Class UserOutput
 * @package Application\Service\Output
 *
 * Класс для разнообразных методов Output для User
 */
class UserOutput
{
    /**
     * @param User $user
     * @return array
     */
    public static function getSmallOutput(User $user): array
    {
        $userOutput = [];

        $userOutput['name'] = $user->getName();
        $userOutput['avatar'] = $user->getAvatar() ? '/profile/avatar/'.$user->getAvatar()->getId() : '/img/no-avatar.png';
        /** @var \DateTime $created */
        $created = $user->getCreated();
        $userOutput['created'] = $created ? $created->format('Y-m-d H:i:s') : null;

        return $userOutput;
    }

    /**
     * @param User $user
     * @return array
     */
    public static function getMediumOutput(User $user): array
    {
        $userOutput = self::getSmallOutput($user);
        /** @var Phone $phone */
        $phone = $user->getPhone();
        $userOutput['is_phone_verified'] = $phone ? $phone->getIsVerified(): null;
        /** @var Email $email */
        $email = $user->getEmail();
        $userOutput['is_email_verified'] = $email ? $email->getIsVerified(): null;
        /** @var \DateTime $created */
        $created = $user->getCreated();
        $userOutput['created_format'] = $created ? $created->format('d.m.y') : null;
        $dateTo = new \DateTime();
        $dateFrom = clone $created;
        $interval = $dateTo->diff($dateFrom);
        $year = '';
        if ($interval->y >= 1) {
            $year = $interval->y . ' ' . DatePeriodsOutput::pluralize($interval->y, 'year').' ';
        }
        $month = '';
        if ($interval->m >= 1) {
            $month = $interval->m . ' ' . DatePeriodsOutput::pluralize($interval->m, 'month').' ';
        }
        $day = '';
        if (empty($year)) {
            if ($interval->d >= 1){
                $day = $interval->d . ' ' . DatePeriodsOutput::pluralize($interval->d, 'day');
            } else {
                $day = 'меньше дня';
            }
        }

        $userOutput['time_from_create'] = rtrim($year.$month.$day);

        return $userOutput;
    }

    /**
     * @param User $user
     * @return array
     */
    public static function getLargeOutput(User $user): array
    {
        $userOutput = self::getMediumOutput($user);

        $userOutput['id'] = $user->getId();
        $userOutput['login'] = $user->getLogin();
        /** @var Phone $phone */
        $phone = $user->getPhone();
        $userOutput['phone'] = $phone ? $phone->getPhoneNumber(): null;
        /** @var Email $email */
        $email = $user->getEmail();
        $userOutput['email'] = $email ? $email->getEmail(): null;

        return $userOutput;
    }

    /**
     * @param User $user
     * @return string
     */
    public static function getUserProductsUrl(User $user): string
    {
        $slug = $user->getSlug() ?? $user->getLogin();
        return '/'.$slug.'/products';
    }

    /**
     * @param User $user
     * @return string
     */
    public static function getUserPurchasesUrl(User $user): string
    {
        $slug = $user->getSlug() ?? $user->getLogin();
        return '/'.$slug.'/purchases';
    }
}