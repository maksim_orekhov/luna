<?php
namespace Application\Service\Output;

use Application\Entity\Moderation;

/**
 * Class ModerationOutput
 * @package Application\Service\Output
 */
class ModerationOutput
{
    /**
     * @param Moderation $moderation
     * @return array
     */
    public static function getBasicOutput(Moderation $moderation): array
    {
        $moderationOutput = [];
        $moderationOutput['created'] = $moderation->getCreated()->format('Y-m-d H:i:s');
        $moderationOutput['created_timestamp'] = $moderation->getCreated()->getTimestamp();
        $resolutionDate = $moderation->getResolutionDate();
        $moderationOutput['resolution_date'] = isset($resolutionDate) ? $resolutionDate->format('Y-m-d H:i:s') : null;
        $moderationOutput['resolution_date_timestamp'] = isset($resolutionDate) ? $resolutionDate->getTimestamp() : null;
        $moderationOutput['approved'] = $moderation->isApproved();
        $moderationOutput['description'] = $moderation->getDescription();

        return $moderationOutput;
    }

    /**
     * @param Moderation $moderation
     * @return array
     */
    public static function getOutputForModerationList(Moderation $moderation): array
    {
        $moderationOutput = self::getBasicOutput($moderation);
        $moderationOutput['moderation_time'] = DatePeriodsOutput::differenceOutput(
            $moderation->getCreated(),
            $moderation->getResolutionDate()
        );

        return $moderationOutput;
    }
}