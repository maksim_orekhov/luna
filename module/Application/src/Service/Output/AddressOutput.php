<?php
namespace Application\Service\Output;

use Application\Entity\Address;

/**
 * Class AddressOutput
 * @package Application\Service\Output
 */
class AddressOutput
{
    /**
     * @param Address|null $address
     * @return array
     */
    public static function getLargeOutput(Address $address = null): array
    {
        $addressOutput = [];
        if ($address) {
            $addressOutput['country'] = $address->getCountry();
            $addressOutput['region'] = $address->getRegion();
            $addressOutput['city'] = $address->getCity();
            $addressOutput['street'] = $address->getStreet();
            $addressOutput['house'] = $address->getHouse();
            $addressOutput['apartment'] = $address->getApartment();
            $addressOutput['index'] = $address->getPostcode();
            $addressOutput['lat'] = $address->getLat();
            $addressOutput['lng'] = $address->getLng();
        }

        return $addressOutput;
    }
}