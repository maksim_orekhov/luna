<?php
namespace Application\Service\Output;

use Application\Entity\Category;
use Application\Entity\City;
use Application\Entity\Product;
use Application\Entity\Shop;
use Application\Entity\User;
use Application\Service\CategoryManager;
use Application\Service\CityManager;
use Application\View\Helper\UrlHelper;
use Doctrine\Common\Collections\ArrayCollection;
use ModuleFileManager\Entity\File;

/**
 * Class ShopOutput
 * @package Application\Service\Output
 */
class ShopOutput
{
    /**
     * @param Shop $shop
     * @return array
     */
    public static function getSmallOutput(Shop $shop): array
    {
        $shopOutput = [];

        $shopOutput['id'] = $shop->getId();
        $shopOutput['name'] = $shop->getName();
        $shopOutput['description'] = $shop->getDescription();
        $shopOutput['created'] = $shop->getCreated()->format('Y-m-d H:i:s');
        $shopOutput['created_timestamp'] = $shop->getCreated()->getTimestamp();

        return $shopOutput;
    }

    /**
     * @param Shop $shop
     * @param array $params
     * @return array
     */
    public static function getMediumOutput(Shop $shop, array $params = []): array
    {
        /**
         * @var File $logo
         * @var User $owner
         * @var Product $product
         * @var Category $category
         * @var Category $subCategory
         */
        $shopOutput = self::getSmallOutput($shop);

        $shopOutput['logo'] = null;
        $logo = $shop->getLogo();
        if ($logo){
            $shopOutput['logo'] = FileOutput::getMediumOutput($logo, [
                'fileManager' => $params['fileManager'] ?? null,
                'is_need_version' => true
            ]);
        }
        $currentCity = null;
        if (isset($params['city']) || $params['city'] instanceof City) {
            $currentCity = $params['city'];
        }

        $owner = $shop->getUser();
        $shopOutput['owner'] = UserOutput::getMediumOutput($owner);
        //products and categories
        $shopCategories = new ArrayCollection();
        $shopSubCategories = new ArrayCollection();
        $products = $owner->getProducts();
        foreach ($products as $product){
            $productCategories = $product->getCategories();
            foreach ($productCategories as $category) {
                if ($category->getParent() === null
                    && !$shopCategories->contains($category)) {
                    $shopCategories->add($category);
                }
                if ($category->getParent() instanceof Category
                    && !$shopSubCategories->contains($category)) {
                    $shopSubCategories->add($category);
                }
            }
        }
        $shopOutput['categories'] = null;
        $shopOutput['sub_categories'] = null;
        // Category
        foreach ($shopCategories as $category) {
            $shopOutput['categories'][] = self::getCategoryOutputForShop($shop, $currentCity, $category);
        }
        foreach ($shopSubCategories as $subCategory) {
            $shopOutput['sub_categories'][] = self::getCategoryOutputForShop($shop, $currentCity, $subCategory);
        }

        $shopOutput['urls'] = [
            'single_shop_public' => self::getUrlForShopOrCategory($shop, $currentCity),
        ];

        return $shopOutput;
    }

    /**
     * @param Shop $shop
     * @param array $params
     * @return array
     */
    public static function getLargeOutput(Shop $shop, array $params = []): array
    {
        $shopOutput = self::getMediumOutput($shop, $params);
        $shopOutput['legal_name'] = $shop->getLegalName();
        $shopOutput['legal_address'] = $shop->getLegalAddress();
        $shopOutput['postal_address'] = $shop->getPostalAddress();
        $shopOutput['inn'] = $shop->getInn();
        $shopOutput['ogrn'] = $shop->getOgrn();
        $shopOutput['work_schedule'] = $shop->getWorkSchedule();
        $shopOutput['web_links'] = $shop->getWebLinks();
        $shopOutput['phones'] = $shop->getPhoneNumbers();
        $shopOutput['moderation'] = ModerationOutput::getOutputForModerationList($shop->getModerations()->last());

        return $shopOutput;
    }

    /**
     * @param Shop $shop
     * @param City $city
     * @param Category $category
     * @return array
     */
    public static function getCategoryOutputForShop(Shop $shop, City $city, Category $category): array
    {
        return [
            'name' => $category->getName(),
            'slug' => $category->getSlug(),
            'url' => self::getUrlForShopOrCategory($shop, $city, $category),
        ];
    }

    public static function getUrlForShopOrCategory(Shop $shop = null, City $city = null, Category $category = null): string
    {
        return '/'.
            UrlHelper::CATALOG_SHOP.
            self::getPartUrlByShop($shop).
            CityManager::getPartUrlByCity($city).
            CategoryManager::getCategoryUrl($category);
    }

    /**
     * @param Shop|null $shop
     * @return string
     */
    public static function getPartUrlByShop(Shop $shop = null): string
    {
        return $shop instanceof Shop && !empty(trim($shop->getSlug())) ? '/'.trim($shop->getSlug()) : '';
    }
}