<?php
namespace Application\Service\Output;

use Application\Entity\Category;
use Application\Entity\City;
use Application\Entity\Purchase;
use Application\Service\CategoryManager;
use Application\Service\CityManager;
use Application\View\Helper\UrlHelper;

/**
 * Class PurchaseOutput
 * @package Application\Service\Output
 */
class PurchaseOutput
{
    /**
     * @param Purchase $purchase
     * @return array
     */
    public static function getSmallOutput(Purchase $purchase): array
    {
        $purchaseOutput = [];

        $purchaseOutput['id'] = $purchase->getId();
        $purchaseOutput['name'] = $purchase->getName();
        $purchaseOutput['price'] = (int) $purchase->getPrice();
        $purchaseOutput['description'] = $purchase->getDescription();
        $purchaseOutput['quantity'] = $purchase->getQuantity();
        $purchaseOutput['measure'] = $purchase->getMeasure();
        $purchaseOutput['created'] = $purchase->getCreated()->format('Y-m-d H:i:s');
        $purchaseOutput['created_timestamp'] = $purchase->getCreated()->getTimestamp();
        // Categories
        $purchaseCategories = $purchase->getCategories();
        $categories = CategoryManager::filteredCollectionByCategory($purchaseCategories);
        $subCategories = CategoryManager::filteredCollectionBySubcategory($purchaseCategories);
        $cities = $purchase->getCities();
        $country = $cities->filter(function(City $entity){
            return $entity->getType() === $entity::COUNTRY_TYPE;
        })->first();

        $purchaseOutput['category'] = null;
        // Category
        if ($categories) {
            /** @var Category $category */
            $category = $categories->first();
            $purchaseOutput['category'] = [
                'name' => $category->getName(),
                'slug' => $category->getSlug(),
                'url' => self::getCityAndCategoryUrlByPurchase($purchase, $country, $category),
            ];
        }

        $purchaseOutput['sub_category'] = null;
        // Sub Category
        if ($subCategories) {
            /** @var Category $subCategory */
            $subCategory = $subCategories->first();
            $purchaseOutput['sub_category'] = [
                'name' => $subCategory->getName(),
                'slug' => $subCategory->getSlug(),
                'url' => self::getCityAndCategoryUrlByPurchase($purchase, $country, $subCategory),
            ];
        }

        $purchaseOutput['urls'] = [
            'single_purchase_public' => self::getSingleUrlByPurchase($purchase),
        ];

        return $purchaseOutput;
    }

    /**
     * @param Purchase $purchase
     * @return array
     */
    public static function getMediumOutput(Purchase $purchase): array
    {
        $purchaseOutput = self::getSmallOutput($purchase);
        /** @var \DateTime $changedDate */
        $changedDate = $purchase->getChanged();
        $purchaseOutput['changed'] = null !== $changedDate ? $changedDate->format('Y-m-d H:i:s') : null;
        $purchaseOutput['changed_timestamp'] = null !== $changedDate ? $changedDate->getTimestamp() : null;
        $purchaseOutput['visible'] = $purchase->isVisible();
        $purchaseOutput['archive'] = $purchase->isRemoved();

        return $purchaseOutput;
    }

    /**
     * @param Purchase $purchase
     * @return array
     */
    public static function getLargeOutput(Purchase $purchase): array
    {
        $purchaseOutput = self::getMediumOutput($purchase);
        $purchaseOutput['urls']['archive_url'] = '/archive-purchase/'.$purchase->getId();

        return $purchaseOutput;
    }

    /**
     * @param Purchase $purchase
     * @return string
     */
    private static function getSingleUrlByPurchase(Purchase $purchase): string
    {
        return self::getCityAndCategoryUrlByPurchase($purchase).'/'.$purchase->getSlug();
    }

    /**
     * @param Purchase $purchase
     * @param City|null $city
     * @param Category|null $category
     * @return string
     */
    public static function getCityAndCategoryUrlByPurchase(Purchase $purchase, City $city = null, Category $category = null): string
    {
        if (! $category instanceof Category) {
            $categories = $purchase->getCategories();
            /** @var Category $subCategory */
            $category = $categories->filter(function (Category $entity) {
                return $entity->getParent() !== null;
            })->first();
        }

        return '/'.
            UrlHelper::CATALOG_PURCHASE.
            CityManager::getPartUrlCityByProduct($purchase, $city).
            CategoryManager::getCategoryUrl($category);
    }
}