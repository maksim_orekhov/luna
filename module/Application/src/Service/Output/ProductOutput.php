<?php
namespace Application\Service\Output;

use Application\Entity\City;
use Application\Entity\Fabricator;
use Application\Entity\Moderation;
use Application\Entity\Product;
use Application\Entity\Category;
use Application\Entity\User;
use Application\Service\CategoryManager;
use Application\Service\CityManager;
use Application\Service\ProductManager;
use Application\Service\Status\ProductStatus;
use Application\View\Helper\UrlHelper;
use Core\Controller\Plugin\SessionPlugin;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;
use ModuleFileManager\Service\FileManager;

/**
 * Class ProductOutput
 * @package Application\Service\Output
 *
 * Класс для разнообразных методов Output для Product
 */
class ProductOutput
{
    /**
     * @var FileManager
     */
    private $fileManager;
    /**
     * @var ProductStatus
     */
    private $productStatus;

    /**
     * ProductOutput constructor.
     * @param FileManager $fileManager
     * @param ProductStatus $productStatus
     */
    public function __construct(FileManager $fileManager,
                                ProductStatus $productStatus)
    {
        $this->fileManager = $fileManager;
        $this->productStatus = $productStatus;
    }

    /**
     * @param Product $product
     * @return array
     */
    public static function getMediumOutput(Product $product): array
    {
        $productOutput = [];

        $productOutput['id'] = $product->getId();
        $productOutput['name'] = $product->getName();
        $productOutput['price'] = (int) $product->getPrice();
        $productOutput['description'] = $product->getDescription();
        $productOutput['created'] = $product->getCreated()->format('Y-m-d H:i:s');
        $productOutput['created_timestamp'] = $product->getCreated()->getTimestamp();
        /** @var \DateTime $changedDate */
        $changedDate = $product->getChanged();
        $productOutput['changed'] = null !== $changedDate ? $changedDate->format('Y-m-d H:i:s') : null;
        $productOutput['changed_timestamp'] = null !== $changedDate ? $changedDate->getTimestamp() : null;
        $productOutput['slug'] = $product->getSlug();

        // Categories
        $productCategories = $product->getCategories();
        $categories = CategoryManager::filteredCollectionByCategory($productCategories);
        $subCategories = CategoryManager::filteredCollectionBySubcategory($productCategories);
        $cities = $product->getCities();
        $country = $cities->filter(function(City $entity){
            return $entity->getType() === $entity::COUNTRY_TYPE;
        })->first();

        $productOutput['category'] = null;
        // Category
        if ($categories) {
            /** @var Category $category */
            $category = $categories->first();
            $productOutput['category'] = [
                'name' => $category->getName(),
                'slug' => $category->getSlug(),
                'url' => self::getUrlForProductOrCategory($product, $country, $category),
            ];
        }

        $productOutput['sub_category'] = null;
        // Sub Category
        if ($subCategories) {
            /** @var Category $subCategory */
            $subCategory = $subCategories->first();
            $productOutput['sub_category'] = [
                'name' => $subCategory->getName(),
                'slug' => $subCategory->getSlug(),
                'url' => self::getUrlForProductOrCategory($product, $country, $subCategory),
            ];
        }

        $productOutput['urls'] = [
            'single_product_public' => self::getUrlForProductOrCategory($product).'/'.$product->getSlug(),
        ];

        /** @var User $owner */
        $owner = $product->getUser();
        $productOutput['fabricator'] = false;
        $productOutput['owner'] = UserOutput::getMediumOutput($owner);
        /** @var Fabricator $fabricator */
        $fabricator = $owner->getFabricator();
        if ($fabricator) {
            /** @var Moderation $fabricatorModeration */
            $fabricatorModeration = $fabricator->getModerations()->last();
            if ($fabricatorModeration && $fabricatorModeration->isApproved()) {
                $fabricatorCategories = $fabricator->getCategories();
                $productOutput['fabricator'] = self::intersectionCollection($productCategories, $fabricatorCategories);
            }
        }

        return $productOutput;
    }

    /**
     * @param Product $product
     * @return array
     */
    public static function getLargeOutput(Product $product): array
    {
        $productOutput = self::getMediumOutput($product);
        $productOutput['urls']['archive_url'] = '/product/'.$product->getId().'/archive';
        $productOutput['urls']['unarchive_url'] = '/product/'.$product->getId().'/unarchive';

        return $productOutput;
    }

    /**
     * @param Product $product
     * @param SessionPlugin $sessionPlugin
     * @param bool $is_need_session_data
     * @return array
     */
    public function getProductOutputWithSessionOrEntity(
        Product $product = null,
        SessionPlugin $sessionPlugin,
        $is_need_session_data = false
    ): array
    {
        if ($is_need_session_data) {

            return $sessionPlugin->getSessionVar(ProductManager::SESSION_PRODUCT_FLAG);
        }
        $sessionPlugin->clearSessionVar(ProductManager::SESSION_PRODUCT_FLAG);

        $productOutput = [];
        if ($product) {
            $categories = $product->getCategories();
            $subCategories = CategoryManager::filteredCollectionBySubcategory($categories);
            $category = null;
            if ($subCategories) {
                /** @var Category $category */
                $category = $subCategories->first();
            }
            $address = $product->getAddress();

            $productOutput['name'] = $product->getName();
            $productOutput['description'] = $product->getDescription();
            $productOutput['price'] = (int)$product->getPrice();

            if ($category) {
                $productOutput['category'] = [
                    'id' => $category->getId(),
                    'name' => $category->getName(),
                    'parent_id' => $category->getParent()->getId(),
                    'parent_name' => $category->getParent()->getName()
                ];
            }

            if ($address) {
                $productOutput['address'] = [
                    'country' => $address->getCountry(),
                    'region' => $address->getRegion(),
                    'city' => $address->getCity(),
                    'street' => $address->getStreet(),
                    'house' => $address->getHouse(),
                    'apartment' => $address->getApartment(),
                    'post_code' => $address->getPostcode(),
                    'lat' => $address->getLat(),
                    'lng' => $address->getLng(),
                ];
            }
            /** @var ArrayCollection|PersistentCollection $gallery */
            $gallery = $product->getGallery();
            $files = $product->getFiles();
            if ($gallery) {
                $productOutput['gallery'] = FileOutput::getMediumCollectionOutput($gallery, [
                    'fileManager' => $this->fileManager,
                    'is_need_version' => true
                ]);
            }
            if ($files) {
                $productOutput['files'] = FileOutput::getMediumCollectionOutput($files, ['fileManager' => $this->fileManager]);
            }
        }

        return $productOutput;
    }

    /**
     * @param $a_collections
     * @param $b_collections
     * @return bool
     */
    public static function intersectionCollection($a_collections, $b_collections): bool
    {
        /**
         * @var ArrayCollection|PersistentCollection $a_collections
         * @var ArrayCollection|PersistentCollection $b_collections
         */
        foreach ($a_collections as $a_collection) {
            if ($b_collections->contains($a_collection)) {

                return true;
            }
        }

        return false;
    }

    public static function getUrlForProductOrCategory(Product $product, City $city = null, Category $category = null): string
    {
        if (! $category instanceof Category) {
            $categories = $product->getCategories();
            /** @var Category $subCategory */
            $category = $categories->filter(function (Category $entity) {
                return $entity->getParent() !== null;
            })->first();
        }

        return '/'.
            UrlHelper::CATALOG_PRODUCT.
            CityManager::getPartUrlCityByProduct($product, $city).
            CategoryManager::getCategoryUrl($category);
    }
}