<?php
namespace Application\Service\Output;

/**
 * Class DatePeriodsHelper
 * @package Application\View\Helper
 */
class DatePeriodsOutput
{
    const SUBSTANTIAL_FORMS = [
        'year' => ['1' => 'год', '2' => 'года', '3' => 'лет'],
        'month' => ['1' => 'месяц', '2' => 'месяца', '3' => 'месяцев'],
        'day' => ['1' => 'день', '2' => 'дня', '3' => 'дней'],
        'hour' => ['1' => 'час', '2' => 'часа', '3' => 'часов'],
        'minute' => ['1' => 'минута', '2' => 'минуты', '3' => 'минут'],
    ];
    const LESS_THAN_A_MINUTE = 'меньше минуты';

    /**
     * @param \DateTime $dateFrom
     * @param \DateTime|null $dateTo
     * @return string
     */
    public static function differenceOutput(\DateTime $dateFrom, \DateTime $dateTo = null): string
    {
        if (null === $dateTo) {
            $dateTo = new \DateTime();
        }

        return self::convertDifferenceToString($dateTo->diff($dateFrom));
    }

    /**
     * @param string $date_from
     * @param string|null $date_to
     * @return bool|\DateInterval
     */
    public static function getDifference(string $date_from, string $date_to = null)
    {
        if (null === $date_to) {
            $dateTo = new \DateTime();
        } else {
            /** @var \DateTime $dateTo */
            $dateTo = \DateTime::createFromFormat('Y-m-d H:i:s', $date_to);
        }
        /** @var \DateTime $dateFrom */
        $dateFrom = \DateTime::createFromFormat('Y-m-d H:i:s', $date_from);

        return $dateTo->diff($dateFrom);
    }

    /**
     * @param $interval
     * @return string
     */
    public static function convertDifferenceToString($interval): string
    {
        $result = null;
        if ($interval->y >= 1) $result .= $interval->y . ' ' . self::pluralize($interval->y, 'year') . ' ';
        if ($interval->m >= 1) $result .= $interval->m . ' ' . self::pluralize($interval->m, 'month') . ' ';
        if ($interval->d >= 1) $result .= $interval->d . ' ' . self::pluralize($interval->d, 'day') . ' ';
        if ($interval->h >= 1) $result .= $interval->h . ' ' . self::pluralize($interval->h, 'hour') . ' ';
        if ($interval->i >= 1) $result .= $interval->i . ' ' . self::pluralize($interval->i, 'minute');
        if (null === $result) $result = self::LESS_THAN_A_MINUTE;

        return $result;
    }

    /**
     * @param int $n
     * @param $period_name
     * @return mixed
     */
    public static function pluralize(int $n, $period_name)
    {
        $n = abs($n) % 100;
        $n1 = $n % 10;
        if ($n > 10 && $n < 20) return self::SUBSTANTIAL_FORMS[$period_name]['3'];
        if ($n1 > 1 && $n1 < 5) return self::SUBSTANTIAL_FORMS[$period_name]['2'];
        if ($n1 == 1) return self::SUBSTANTIAL_FORMS[$period_name]['1'];

        return self::SUBSTANTIAL_FORMS[$period_name]['3'];
    }
}