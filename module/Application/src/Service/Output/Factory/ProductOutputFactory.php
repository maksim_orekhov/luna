<?php
namespace Application\Service\Output\Factory;

use Application\Service\Output\ProductOutput;
use Application\Service\ProductManager;
use Application\Service\Status\ProductStatus;
use Interop\Container\ContainerInterface;
use ModuleFileManager\Service\FileManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class ProductOutputFactory
 * @package Application\Service\Output\Factory
 */
class ProductOutputFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ProductManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $fileManager = $container->get(FileManager::class);
        $productStatus = $container->get(ProductStatus::class);

        return new ProductOutput(
            $fileManager,
            $productStatus
        );
    }
}