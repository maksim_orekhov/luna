<?php
namespace Application\Service\Output;

use Application\Entity\Email;
use Application\Entity\Phone;
use Application\Entity\User;
use Application\Service\ProductManager;
use ModuleFileManager\Entity\File;
use ModuleFileManager\Service\FileManager;
use Zend\Paginator\Paginator;

/**
 * Class FileOutput
 * @package Application\Service\Output
 */
class FileOutput
{
    /**
     * @param File $file
     * @return array
     */
    public static function getSmallOutput(File $file): array
    {
        $fileOutput = [];
        $fileOutput['id'] = $file->getId();
        $fileOutput['name'] = $file->getOriginName();

        return $fileOutput;
    }

    /**
     * @param File $file
     * @param array $params
     * @return array
     */
    public static function getMediumOutput(File $file, array $params = []): array
    {
        if (!isset($params['fileManager']) || !$params['fileManager'] instanceof FileManager) {

            throw new \RuntimeException('required parameter not given FileManager');
        }
        /** @var FileManager $fileManager */
        $fileManager = $params['fileManager'];
        $is_need_version = false;
        if (isset($params['is_need_version']) && $params['is_need_version']) {

            $is_need_version = true;
        }
        $display = $fileManager->isFileTypeDisplayable($file->getType());
        $fileOutput = self::getSmallOutput($file);

        $imagePath = $fileManager->getPathFile($file);
        $imgParts = pathinfo($imagePath);
        $basePath = $display ? '/img/' : '/file/';

        $fileOutput['extension'] = $imgParts['extension'] ?? 'jpeg';
        $fileOutput['path'] = $basePath.$file->getId().'.'.$fileOutput['extension'];

        $fileOutput['is_display'] = $display;
        $fileOutput['version'] = '';
        if ($is_need_version) {
            $changed = $file->getChanged();
            $version = $changed ? '?v='.$changed->getTimestamp() : '';
            $path = preg_replace('/\?.*/', '', $fileOutput['path']);
            $fileOutput['path'] = $path.$version;
            $fileOutput['version'] = $version;
        }

        return $fileOutput;
    }

    /**
     * @param File $file
     * @param array $params
     * @return array
     */
    public static function getLargeOutput(File $file, array $params = []): array
    {
        $fileOutput = self::getMediumOutput($file, $params);

        return $fileOutput;
    }

    /**
     * @param $files
     * @param array $params
     * @return array
     */
    public static function getMediumCollectionOutput($files, array $params = []): array
    {
        $filesOutput = [];
        /** @var File $file */
        foreach ($files as $file){

            $filesOutput[] = self::getMediumOutput($file, $params);
        }

        return $filesOutput ;
    }
}