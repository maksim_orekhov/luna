<?php
namespace Application\Service;

use Application\Entity\Interfaces\ModeratedInterface;
use Application\Entity\Moderation;
use Doctrine\ORM\EntityManager;

/**
 * Class ModerationManager
 * @package Application\Service
 */
class ModerationManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * ModerationManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param ModeratedInterface $object
     * @param $openDate
     * @return Moderation
     * @throws \Doctrine\ORM\ORMException
     * @throws \Exception
     */
    public function openModeration(ModeratedInterface $object, $openDate): Moderation
    {
        //@TODO создать проверку что преддыдущая модерация завершена
        $moderation = new Moderation();
        $moderation->setCreated($openDate);

        $this->entityManager->persist($moderation);
        $this->entityManager->flush($moderation);

        return $moderation;
    }
}