<?php
namespace Application\Service\Delegators;

use Application\Service\RouteManager;
use Zend\Mvc\Console\Router\SimpleRouteStack as ConsoleRouteStack;
use Interop\Container\ContainerInterface;
use Zend\Router\RouteStackInterface;
use Zend\ServiceManager\Factory\DelegatorFactoryInterface;

/**
 * Delegator factory for the Router service.
 *
 * If a console environment is detected, returns the ConsoleRouter service
 * instead of the default router.
 */
class ProductRouterDelegator implements DelegatorFactoryInterface
{
    /**
     * @var array
     */
    private $routes;
    /**
     * Known router names/aliases; allows auto-selection of console router.
     *
     * @var string[]
     */
    private $knownRouterNames = [
        'zend\\router\routestackinterface',
    ];

    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        $router = $callback();
        if (!$router instanceof ConsoleRouteStack &&
            \in_array(strtolower($name), $this->knownRouterNames, true)) {
            if (empty($this->categoryRoutes)) {
                $this->routes = $container->get(RouteManager::class)->getRoutes();
            }
            /** @var RouteStackInterface $router */
            $router->addRoutes($this->routes);
        }

        return $router;
    }
}
