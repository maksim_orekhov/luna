<?php
namespace Application\Service;

use Application\Entity\Category;
use Application\Entity\Fabricator;
use Application\Entity\Product;
use Application\Entity\Purchase;
use Application\Entity\Shop;
use Application\Entity\User;
use Application\Service\Output\FileOutput;
use Application\Service\Output\ShopOutput;
use Behat\Transliterator\Transliterator;
use Core\Exception\LogicException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;
use ModuleFileManager\Entity\File;
use ModuleFileManager\Service\FileManager;
use Zend\Paginator\Paginator;

class ShopManager
{
    const MAX_RECURSION_LEVEL = 100;
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var ModerationManager
     */
    private $moderationManager;
    /**
     * @var FileManager
     */
    private $fileManager;

    /**
     * ShopManager constructor.
     * @param EntityManager $entityManager
     * @param ModerationManager $moderationManager
     * @param FileManager $fileManager
     */
    public function __construct(EntityManager $entityManager,
                                ModerationManager $moderationManager,
                                FileManager $fileManager
    )
    {
        $this->entityManager = $entityManager;
        $this->moderationManager = $moderationManager;
        $this->fileManager = $fileManager;
    }

    /**
     * @param string|null $shop_slug
     * @return Shop|null
     */
    public function getShopBySlug(string $shop_slug = null)
    {
        if (!$shop_slug && !empty(trim($shop_slug))) {

            return null;
        }

        /** @var Shop|null $shop */
        $shop = $this->entityManager->getRepository(Shop::class)
            ->findOneBy(['slug' => $shop_slug]);

        return $shop ?? null;
    }

    /**
     * @param bool $city_all
     * @param null $city
     * @param Category|null $category
     * @param Category|null $subCategory
     * @return bool
     */
    public function isRouteShopCorrect($city_all = false, $city = null, $category = null, $subCategory = null):bool
    {
        if (! $city_all && $category && $subCategory) {

            return !(!$subCategory || $subCategory->getParent() !== $category);
        }

        if (! $city_all && $category && ! $subCategory) {


            return true;
        }

        if (($city_all || $city) && ! $category && ! $subCategory) {


            return true;
        }

        return false;
    }

    /**
     * @param array $route_params
     * @return Paginator
     */
    public function getPublicShopsByRouteData(array $route_params): Paginator
    {
        return $this->entityManager->getRepository(Shop::class)
            ->findPublishedShopsByRouteParams($route_params);
    }

    /**
     * @param User $user
     * @param array $data
     * @return Fabricator|mixed
     * @throws \Exception
     */
    public function createCustomShop(User $user, array $data)
    {
        $delimiters = [',',"\r\n","\n","\r"];
        $ar_phones = $this->getArrayFromStringByDelimiters($data['phones'], $delimiters);
        $ar_work_schedule = $this->getArrayFromStringByDelimiters($data['work_schedule'], $delimiters);
        $ar_web_links = $this->getArrayFromStringByDelimiters($data['web_links'], $delimiters);

        $shop = $user->getShop();
        if ($shop === null) {
            $shop = new Shop();
            // если магазин создается в первый раз то:
            // нужно все объявления и закупки отправить в архив
            $this->sendAllProductsAndPurchasesToArchive($user);
        }
        $current_date = new \DateTime();

        $shop->setName($data['name']);
        $shop->setSlug($this->generateSlugUnique($shop, $data['name']));
        $shop->setLegalName($data['legal_name']);
        $shop->setDescription($data['description']);
        $shop->setInn($data['inn']);
        $shop->setOgrn($data['ogrn']);
        $shop->setLegalAddress($data['legal_address']);
        $shop->setPostalAddress($data['postal_address']);
        $shop->setPhoneNumbers($ar_phones);
        $shop->setWebLinks($ar_web_links);
        $shop->setWorkSchedule($ar_work_schedule);
        $shop->setCreated($current_date);
        $shop->setUser($user);

        $this->entityManager->persist($shop);
        $this->entityManager->flush($shop);

        //отправляем на модерацию
        $moderation = $this->moderationManager->openModeration($shop, $current_date);
        $moderation->setApproved(true);
        $moderation->setResolutionDate($current_date);
        $this->entityManager->persist($moderation);
        $this->entityManager->flush($moderation);

        $shop->addModeration($moderation);

        $this->entityManager->persist($shop);
        $this->entityManager->flush($shop);

        return $shop;
    }

    /**
     * @param string $string
     * @param array $delimiters
     * @return array
     */
    public function getArrayFromStringByDelimiters(string $string = '', array $delimiters = []): array
    {
        $ar_result = [];
        if (empty($string) || empty($delimiters)) {

            return $ar_result;
        }
        $replaced_string = str_replace($delimiters, $delimiters[0], $string);
        $ar_result = explode($delimiters[0], $replaced_string);
        foreach ($ar_result as $key => $item){
            $ar_result[$key] = trim($item);
            if (empty($ar_result[$key])) {
                unset($ar_result[$key]);
            }
        }

        return $ar_result;
    }


    /**
     * @param User $user
     * @return bool
     * @throws \Exception
     */
    public function sendAllProductsAndPurchasesToArchive(User $user): bool
    {
        $allUserProducts = $user->getProducts();
        $allUserPurchases = $user->getPurchases();
        /** @var Product $product */
        foreach ($allUserProducts as $product) {
            $product->setRemoved(1);
        }
        /** @var Purchase $purchase */
        foreach ($allUserPurchases as $purchase) {
            $purchase->setRemoved(1);
        }

        $this->entityManager->flush();

        return true;
    }

    /**
     * @param Shop $shop
     * @param array $file_data
     * @return Shop
     * @throws LogicException
     */
    public function createShopLogo(Shop $shop, array $file_data): Shop
    {
        if ($shop->getLogo()) {

            $shop = $this->removeShopLogo($shop);
        }
        try {
            $file = $this->fileManager->write([
                'file_source' => $file_data['tmp_name'],
                'file_name' => $file_data['name'],
                'file_type' => $file_data['type'],
                'file_size' => $file_data['size'],
                'path' => Shop::PATH_LOGO_FILES,
            ]);
            $shop->setLogo($file);
            $this->entityManager->persist($shop);
            $this->entityManager->flush($shop);
        }
        catch (\Throwable $t) {

            throw new LogicException(null, LogicException::SHOP_LOGO_CREATE_FAILED);
        }

        return $shop;
    }

    /**
     * @param Shop $shop
     * @return Shop
     * @throws LogicException
     */
    public function removeShopLogo(Shop $shop): Shop
    {
        try {
            /** @var File $logo */
            $logo = $shop->getLogo();
            $this->fileManager->delete($logo->getId());

            $this->entityManager->persist($shop);
            $this->entityManager->flush($shop);
        }
        catch (\Throwable $t){

            throw new LogicException(null, LogicException::SHOP_LOGO_REMOVE_FAILED);
        }

        return $shop;
    }

    /**
     * @param Shop $currentShop
     * @param string $name
     * @param int $recursion_level
     * @return string
     * @throws LogicException
     */
    public function generateSlugUnique(Shop $currentShop, string $name, $recursion_level = 0): string
    {
        if ($recursion_level >= self::MAX_RECURSION_LEVEL) {

            throw new LogicException(null, LogicException::SHOP_GENERATION_UNIQUE_SLUG_FAILED);
        }
        $slug = $this->generateSlug($name);
        if ( $recursion_level > 0 ) {
            $slug .= '_'.$recursion_level;
        }
        $shop = $this->entityManager->getRepository(Shop::class)->findOneBy(['slug' => $slug]);

        if ($shop !== null && $shop !== $currentShop) {
            //// если Shop с таким slug существует запускаем повторную генерацию ////
            return $this->generateSlugUnique($currentShop, $name, ++$recursion_level);
        }

        return $slug;
    }

    /**
     * @param string $name
     * @return string
     */
    private function generateSlug(string $name): string
    {
        return  Transliterator::transliterate(mb_strtolower(trim($name)),'_');
    }

    /**
     * @param $shops
     * @param array $params
     * @return array|null
     */
    public function getPublicListOutput($shops, array $params = [])
    {
        $listOutput = [];
        /** @var Shop $shop */
        foreach ($shops as $shop){
            // Base Output
            $shopOutput = ShopOutput::getMediumOutput($shop, $params);

            $listOutput[] = $shopOutput;
        }

        return !empty($listOutput) ? $listOutput : null;
    }

    /**
     * @param Shop $shop
     * @param array $params
     * @return array
     */
    public function getPublicSingleOutput(Shop $shop, array $params = []): array
    {
        return ShopOutput::getLargeOutput($shop, $params);
    }
}