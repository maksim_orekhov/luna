<?php
namespace Application\Service;

use Application\Entity\Address;
use Application\Entity\Moderation;
use Application\Entity\Product;
use Application\Entity\Category;
use Application\Entity\ProductProlongation;
use Application\Entity\User;
use Application\Service\Output\AddressOutput;
use Application\Service\Output\FileOutput;
use Application\Service\Output\ModerationOutput;
use Application\Service\Output\PaginatorOutput;
use Application\Service\Output\ProductOutput;
use Application\Service\Output\UserOutput;
use Application\Service\Status\ProductStatus;
use Behat\Transliterator\Transliterator;
use Core\Controller\Plugin\SessionPlugin;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;
use ModuleFileManager\Entity\File;
use ModuleFileManager\Service\FileManager;
use ModuleImageResize\Service\ImageManager;
use Zend\Form\Form;
use Zend\Paginator\Paginator;

/**
 * Class ProductManager
 * @package Application\Service
 */
class ProductManager
{
    const SESSION_PRODUCT_FLAG = 'product_data';
    use PaginatorOutputTrait;

    const DEFAULT_PUBLICATION_PERIOD = 30;
    const MAX_RECURSION_LEVEL = 100;
    const PRODUCT_FOLDER = '/product';
    const PRODUCT_GALLERY_FOLDER = '/product/gallery';

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var FileManager
     */
    private $fileManager;

    /**
     * @var ModerationManager
     */
    private $moderationManager;

    /**
     * @var AddressManager
     */
    private $addressManager;

    /**
     * @var CityManager
     */
    private $cityManager;

    /**
     * @var CategoryManager
     */
    private $categoryManager;

    /**
     * @var ProductStatus
     */
    private $productStatus;

    /**
     * @var ImageManager
     */
    private $imageManager;
    /**
     * @var ProductModerationManager
     */
    private $productModerationManager;
    /**
     * @var ProductArchiveManager
     */
    private $productArchiveManager;

    /**
     * ProductManager constructor.
     * @param EntityManager $entityManager
     * @param FileManager $fileManager
     * @param ModerationManager $moderationManager
     * @param AddressManager $addressManager
     * @param CityManager $cityManager
     * @param CategoryManager $categoryManager
     * @param ProductStatus $productStatus
     * @param ImageManager $imageManager
     * @param ProductModerationManager $productModerationManager
     * @param ProductArchiveManager $productArchiveManager
     */
    public function __construct(EntityManager $entityManager,
                                FileManager $fileManager,
                                ModerationManager $moderationManager,
                                AddressManager $addressManager,
                                CityManager $cityManager,
                                CategoryManager $categoryManager,
                                ProductStatus $productStatus,
                                ImageManager $imageManager,
                                ProductModerationManager $productModerationManager,
                                ProductArchiveManager $productArchiveManager)
    {
        $this->entityManager = $entityManager;
        $this->fileManager = $fileManager;
        $this->moderationManager = $moderationManager;
        $this->addressManager = $addressManager;
        $this->cityManager = $cityManager;
        $this->categoryManager = $categoryManager;
        $this->productStatus = $productStatus;
        $this->imageManager = $imageManager;
        $this->productModerationManager = $productModerationManager;
        $this->productArchiveManager = $productArchiveManager;
    }

    /**
     * @param null $city
     * @param Category|null $category
     * @param Category|null $subCategory
     * @return bool
     */
    public function isRouteProductCorrect($city = null, $category = null, $subCategory = null):bool
    {
        if ($city && $category && $subCategory) {

            return !(!$subCategory || $subCategory->getParent() !== $category);
        }

        if ($city && $category && ! $subCategory) {


            return true;
        }

        if ($city && ! $category && ! $subCategory) {


            return true;
        }

        return false;
    }

    /**
     * @param array $route_params
     * @return Paginator
     */
    public function getPublicProductsByRouteData(array $route_params): Paginator
    {
        return $this->entityManager->getRepository(Product::class)
            ->findPublishedProductsByRouteParams($route_params);
    }

    /**
     * @param User $user
     * @return Paginator
     */
    public function getPublishedProductsByUser(User $user): Paginator
    {
        /** @var Paginator $products */
        $products = $this->entityManager->getRepository(Product::class)
            ->selectPublicProducts($user);

        return $products;
    }
    /**
     * @param array|null $postFileData
     * @return array
     */
    public function getFilesPostData($postFileData):array
    {
        $result = [];
        if (\count($postFileData)) {
            if (isset($postFileData['files'])) {
                foreach ($postFileData['files'] as $files) {
                    $result['files'][] = $files;
                    if ($files['error'] === 4) {
                        $result['files'] = null;
                        break;
                    }
                }
            }
            if (isset($postFileData['gallery'])) {
                foreach ($postFileData['gallery'] as $imageGallery) {
                    $result['gallery'][] = $imageGallery;
                    if ($imageGallery['error'] === 4) {
                        $result['gallery'] = null;
                        break;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param array $postData
     * @param $filesData
     * @return array
     */
    public function prepareDataToProduct(array $postData = [], $filesData): array
    {
        $data = [
            'post_data' => [
                'product' => null,
                'saved_gallery' => null,
                'saved_files' => null,
            ],
            'validation' => [
                'is_valid' => true,
                'product' => null,
                'saved_gallery' => null,
                'saved_files' => null
            ]
        ];

        if (array_key_exists('gallery', $postData) && \is_array($postData['gallery'])) {
            $saved_gallery = null;
            foreach ($postData['gallery'] as $image) {
                if (\is_array($image) && array_key_exists('id', $image)) {
                    $saved_gallery[] = $image;
                }
            }
            $data['post_data']['saved_gallery'] = $saved_gallery;
            unset($postData['gallery']);
        }
        if (array_key_exists('files', $postData) && \is_array($postData['files'])) {
            $saved_files = null;
            foreach ($postData['files'] as $file) {
                if (\is_array($file) && array_key_exists('id', $file)) {
                    $saved_files[] = $file;
                }
            }
            $data['post_data']['saved_files'] = $saved_files;
            unset($postData['files']);
        }
        if (!empty($filesData)) {
            $postData = array_merge_recursive($postData, $filesData);
        }
        $data['post_data']['product'] = $postData;

        return $data;
    }

    /**
     * @param $data
     * @param Form $form
     * @return mixed
     */
    public function getValidationProductForm($data, Form $form)
    {
        //валидация формы
        $form->setData($data['post_data']['product']);
        if (! $form->isValid()) {
            $data['validation']['is_valid'] = false;
            $data['validation']['product'] = $form->getMessages();
        }

        return $data;
    }

    /**
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function getValidationSavedGalleryAndFiles(array $data): array
    {
        $saved_gallery = $data['post_data']['saved_gallery'];
        if ($saved_gallery && \is_array($saved_gallery)) {
            $validation_messages = null;
            foreach ($saved_gallery as $image) {
                $file = $this->fileManager->selectFileById($image['id'] ?? 0);
                if (!$file) {
                    $validation_messages['idNotFound'] = 'Can not find File in database by Id';
                    break;
                }
            }
            if ($validation_messages) {
                $data['validation']['is_valid'] = false;
                $data['validation']['saved_gallery'] = $validation_messages;
            }
        } else {
            $data['post_data']['saved_gallery'] = null;
        }

        $saved_files = $data['post_data']['saved_files'];
        if ($saved_files && \is_array($saved_files)) {
            $validation_messages = null;
            foreach ($saved_files as $saved_file) {
                $file = $this->fileManager->selectFileById($saved_file['id'] ?? 0);
                if (!$file) {
                    $validation_messages['idNotFound'] = 'Can not find File in database by Id';
                    break;
                }
            }
            if ($validation_messages) {
                $data['validation']['is_valid'] = false;
                $data['validation']['saved_files'] = $validation_messages;
            }
        } else {
            $data['post_data']['saved_files'] = null;
        }

        return $data;
    }

    /**
     * @param array $image_data
     * @param string $base_path
     * @return array
     * @throws \Exception
     */
    public function saveImageTemporaryFile(array $image_data, $base_path = self::PRODUCT_FOLDER): array
    {
        $autoIncrementId = $this->entityManager->getRepository(Product::class)
            ->getNextProductFileAutoIncrementId();

        $path = $base_path.'/'.$this->fileManager::generateFolderName($autoIncrementId);

        return FileOutput::getMediumOutput($this->fileManager->saveTemporaryFile($image_data, $path), [
            'fileManager' => $this->fileManager
        ]);
    }

    /**
     * @param array $data_files
     * @param string $base_path
     * @return array
     * @throws \Exception
     */
    public function saveProductTemporaryFiles(array $data_files, $base_path = self::PRODUCT_FOLDER): array
    {
        $filesOutput = [];

        foreach ($data_files as $data_file) {
            if (!empty($data_file) && \is_array($data_file)) {
                $autoIncrementId = $this->entityManager->getRepository(Product::class)
                    ->getNextProductFileAutoIncrementId();
                $path = $base_path . '/' . $this->fileManager::generateFolderName($autoIncrementId);

                $filesOutput[] = FileOutput::getMediumOutput($this->fileManager->saveTemporaryFile($data_file, $path), [
                    'fileManager' => $this->fileManager
                ]);
            }
        }

        return $filesOutput;
    }

    /**
     * @param array $gallery
     * @return array
     * @throws \Exception
     */
    public function editSavedProductGallery(array $gallery = []): array
    {
        foreach ($gallery as $key => $image) {
            $file = $this->fileManager->selectFileById($image['id'] ?? 0);
            if ($file) {
                if (isset($image['rotate']) && (int)$image['rotate'] > 0) {
                    $rotate = (int)$image['rotate'];
                    $gallery[$key] = $this->rotateProductImage($file, $rotate);
                } else {
                    $gallery[$key] = FileOutput::getMediumOutput($file, [
                        'fileManager' => $this->fileManager,
                        'is_need_version' => true
                    ]);
                }
            }
        }

        return $gallery;
    }

    /**
     * @param File $file
     * @param int $rotate
     * @return array
     * @throws \Exception
     */
    public function rotateProductImage(File $file, int $rotate): array
    {
        $imagePath = $this->fileManager->getPathFile($file);
        if ( !file_exists($imagePath)) {

            throw new \RuntimeException('File not found');
        }

        $this->imageManager->rotateRewriteFile($rotate, $imagePath);
        //для отчистки кеша ставим время редактирования
        $file->setChanged(new \DateTime());
        $this->entityManager->flush($file);
        $image_output = FileOutput::getMediumOutput($file, [
            'fileManager' => $this->fileManager,
            'is_need_version' => true
        ]);
        $image_output['rotate'] = $rotate;

        return $image_output;
    }

    /**
     * @param User $user
     * @param $data
     * @return Product
     * @throws \Exception
     */
    public function createProduct(User $user, $data): Product
    {
        $current_date = new \DateTime();

        $product = new Product();
        $product->setUser($user);
        $product->setName($data['name']);
        $product->setDescription($data['description']);
        $product->setPrice($data['price']);
        $product->setVisible(true);
        $product->setRemoved(false);
        $product->setPublicationPeriod(self::DEFAULT_PUBLICATION_PERIOD);
        $product->setCreated($current_date);
        $product->setChanged($current_date);

        $this->entityManager->persist($product);
        $this->entityManager->flush($product);
        // генерация и добавление slug и кода
        $this->setProductSlugAndCode($product);
        //привязываем к категориям
        $sub_category = $this->categoryManager->getCategoryById($data['category']);
        if ($sub_category) {
            $product = $this->linkProductWithCategory($product, $sub_category);
        }
        //добавляем адрес
        /** @var Address $address */
        $address = $this->addressManager->createAddress($data['address']);
        $product->setAddress($address);
        //на основе введенного адреса привязываем к городам
        $this->cityManager->linkAddressWithCities($product, $address);

        //отправляем на модерацию
        $moderation = $this->moderationManager->openModeration($product, $current_date);
        $product->addModeration($moderation);

        $this->entityManager->persist($product);
        $this->entityManager->flush($product);

        return $product;
    }

    /**
     * @param Product $product
     * @param $data
     * @return Product
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function editProduct(Product $product, $data): Product
    {
        $current_date = new \DateTime();
        // первое что делаем при редактировании отправляем на модерацию
        /** @var  ArrayCollection|PersistentCollection $moderations */
        $moderations = $product->getModerations();
        $lastModeration = null;
        if ($moderations) {
            /** @var Moderation $lastModeration */
            $lastModeration = $moderations->last();
        }

        if (!$lastModeration || ($lastModeration && $lastModeration->isApproved() !== null)) {
            $moderation = $this->moderationManager->openModeration($product, $current_date);
            $product->addModeration($moderation);
        }

        $address = $product->getAddress();
        if ($address) {
            $this->addressManager->editAddress($address, $data['address']);
        }

        $product->setName($data['name']);
        $product->setDescription($data['description']);
        $product->setPrice($data['price']);
        $product->setChanged($current_date);

        // генерация и добавление slug и кода
        $this->setProductSlugAndCode($product);
        //привязываем к категориям
        $sub_category = $this->categoryManager->getCategoryById($data['category']);
        if ($sub_category) {
            $product = $this->linkProductWithCategory($product, $sub_category);
        }

        //на основе введенного адреса привязываем к городам
        $this->cityManager->linkAddressWithCities($product, $address);

        $this->entityManager->persist($product);
        $this->entityManager->flush($product);

        return $product;
    }

    /**
     * @param Product $product
     * @param array $temporary_files
     * @return Product
     * @throws \Exception
     */
    public function attachProductFiles(Product $product, array $temporary_files): Product
    {
        try {
            $files = $temporary_files['files'] ?? [];
            $images = $temporary_files['gallery'] ?? [];

            $is_clear_files = false;
            /** @var ArrayCollection|PersistentCollection $productFiles */
            $productFiles = $product->getFiles();
            if ($productFiles) {
                $productFiles->clear();
                $is_clear_files = true;
            }
            /** @var ArrayCollection|PersistentCollection $productFiles */
            $productGallery = $product->getGallery();
            if ($productGallery) {
                $productGallery->clear();
                $is_clear_files = true;
            }

            if ($is_clear_files) {
                $this->entityManager->persist($product);
                $this->entityManager->flush($product);
                $this->entityManager->refresh($product);
            }

            $filesCollection = new ArrayCollection();
            $unique_files = [];
            foreach ($files as $file) {
                if (isset($file['id'])) {
                    $unique_files[$file['id']] = $file;
                }
            }
            foreach ($unique_files as  $unique_file) {
                /** @var File $productFile */
                $productFile = $this->fileManager->getFileById($unique_file['id']);
                $filesCollection->add($productFile);
            }
            $product->setFiles($filesCollection);
            $this->entityManager->persist($product);
            $this->entityManager->flush($product);

            $imagesCollection = new ArrayCollection();
            $unique_images = [];
            foreach ($images as $image) {
                $unique_images[$image['id']] = $image;
            }
            foreach ($unique_images as $unique_image) {
                /** @var File $productImage */
                $productImage = $this->fileManager->getFileById($unique_image['id']);
                $imagesCollection->add($productImage);
            }

            $product->setGallery($imagesCollection);
            $this->entityManager->persist($product);
            $this->entityManager->flush($product);
        }
        catch (\Throwable $t ) {

            logException($t, 'add-file-errors');
        }

        return $product;
    }

    /**
     * @param Product $product
     */
    public function setProductSlugAndCode(Product $product)
    {
        $code = $this->generateProductCode($product);
        $slug = $this->generateProductSlug($product);
        $product->setCode($code);
        $product->setSlug($slug.'_'.$code);
    }

    /**
     * @param Product $product
     * @return string
     */
    public function generateProductCode(Product $product):string
    {
        return 'LN_'.str_pad($product->getId(), 4, '0', STR_PAD_LEFT);
    }

    /**
     * @param Product $product
     * @return string
     */
    public function generateProductSlug(Product $product):string
    {
        return Transliterator::transliterate(mb_strtolower($product->getName()), '_');
    }

    /**
     * @param null $product_slug
     * @param null $category_slug
     * @return Product|null
     * @throws \Exception
     */
    public function getProductByCategory($product_slug = null, $category_slug = null)
    {
        if (!$product_slug || !$category_slug) {

            return null;
        }
        /** @var Category $category */
        $category = $this->entityManager->getRepository(Category::class)
            ->findOneBy(['slug' => $category_slug]);
        if (!$category ) {

            return null;
        }
        /** @var Product|null $product */
        $product = $this->entityManager->getRepository(Product::class)
            ->getProductByCategory($product_slug, $category);

        return $product;
    }

    /**
     * @param Product $product
     * @param Category $sub_category
     * @return Product
     * @throws \Exception
     */
    public function linkProductWithCategory(Product $product, Category $sub_category): Product
    {
        /** @var ArrayCollection|PersistentCollection $categories */
        $categories = $product->getCategories();
        if ($categories) {
            $categories->clear();
            $this->entityManager->persist($product);
            $this->entityManager->flush($product);
            $this->entityManager->refresh($product);
        }

        $category = $sub_category->getParent();
        $categoryCollection = new ArrayCollection();
        $categoryCollection->add($category);
        $categoryCollection->add($sub_category);

        $product->setCategories($categoryCollection);

        $this->entityManager->persist($product);
        $this->entityManager->flush($product);

        return $product;
    }

    /**
     * @param User|null $user
     * @param array $route_params
     * @return array
     * @throws \Exception
     */
    public function getPublicProductsAndPaginatorOutput(User $user = null, array $route_params): array
    {
        $publicProducts = $this->getPublicProducts($user, $route_params);
        $paginator = $publicProducts->getPages();

        return [
            'products' => $this->getPublicListOutput($publicProducts),
            'paginator' => $paginator ? $this->getPaginatorOutput($paginator) : null
        ];
    }

    /**
     * @param $products
     * @return array|null
     */
    public function getPublicListOutput($products)
    {
        $listOutput = [];
        /** @var Product $product */
        foreach ($products as $product){
            // Base Output
            $productOutput = ProductOutput::getMediumOutput($product);
            $productOutput['owner'] = UserOutput::getMediumOutput($product->getUser());
            /** @var array|ArrayCollection|PersistentCollection $gallery */
            $gallery = $product->getGallery();
            $productOutput['gallery'] = null;
            if ($gallery) {
                $productOutput['gallery'] = FileOutput::getMediumCollectionOutput($gallery, [
                    'fileManager' => $this->fileManager,
                    'is_need_version' => true
                ]);
            }


            $listOutput[] = $productOutput;
        }

        return !empty($listOutput) ? $listOutput : null;
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return array
     * @throws \Exception
     */
    public function getModerationProductsAndPaginatorOutput(User $user = null, int $page = 1, int $per_page = null): array
    {
        $paginatedProducts = $this->productModerationManager->getProductsOnModeration($user, $page, $per_page);
        $paginator = $paginatedProducts->getPages();

        return [
            'products' => $this->getInsideListOutput($paginatedProducts),
            'paginator' => $paginator ? PaginatorOutput::getOutput($paginator) : null
        ];
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return array
     * @throws \Exception
     */
    public function getDeclineProductsAndPaginatorOutput(User $user = null, int $page = 1, int $per_page = null): array
    {
        $declineProducts = $this->productModerationManager->getDeclinedProducts($user, $page, $per_page);
        $paginator = $declineProducts->getPages();

        return [
            'products' => $this->getInsideListOutput($declineProducts),
            'paginator' => $paginator ? PaginatorOutput::getOutput($paginator) : null
        ];
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return array
     * @throws \Exception
     */
    public function getPublishedProductsAndPaginatorOutput(User $user = null, int $page = 1, int $per_page = null): array
    {
        $publicProducts = $this->getPublicProducts($user, ['page' => $page, 'per_page' => $per_page]);
        $paginator = $publicProducts->getPages();

        return [
            'products' => $this->getInsideListOutput($publicProducts),
            'paginator' => $paginator ? $this->getPaginatorOutput($paginator) : null
        ];
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return array
     * @throws \Exception
     */
    public function getInactiveProductsAndPaginatorOutput(User $user = null, int $page = 1, int $per_page = null): array
    {
        $inactiveProducts  = $this->getInactiveProducts($user, $page, $per_page);
        $paginator = $inactiveProducts->getPages();

        return [
            'products' => $this->getInsideListOutput($inactiveProducts),
            'paginator' => $paginator ? $this->getPaginatorOutput($paginator) : null
        ];
    }


    /**
     * @param User|null $user
     * @param int $page
     * @param int $per_page
     * @return array
     */
    public function getArchiveProductsAndPaginatorOutput(User $user = null, int $page = 1, int $per_page = null): array
    {
        $moderationProducts = $this->productArchiveManager->getArchiveProducts($user, $page, $per_page);
        $paginator = $moderationProducts->getPages();

        return [
            'products' => $this->getInsideListOutput($moderationProducts),
            'paginator' => $paginator ? PaginatorOutput::getOutput($paginator) : null
        ];
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return mixed
     * @throws \Exception
     */
    public function getInactiveProducts(User $user = null, int $page, int $per_page = null)
    {
        return $this->entityManager->getRepository(Product::class)
            ->selectInactiveProducts($user, $page, $per_page);
    }

    /**
     * @param User|null $user
     * @param array $route_params
     * @return mixed
     */
    public function getPublicProducts(User $user = null, array $route_params)
    {
        return $this->entityManager->getRepository(Product::class)
            ->selectPublicProducts($user, $route_params);
    }

    /**
     * @param $id
     * @return Product|null
     */
    public function getProductById($id)
    {
        if (!$id) {
            return null;
        }
        /** @var Product|null $product */
        $product = $this->entityManager->getRepository(Product::class)
            ->findOneBy(['id'=> (int) $id]);

        return $product;
    }

    /**
     * @param $products
     * @return array|null
     */
    public function getInsideListOutput($products)
    {
        $listOutput = [];
        /** @var Product $product */
        foreach ($products as $product){
            $productOutput = ProductOutput::getLargeOutput($product);
            $productOutput['owner'] = UserOutput::getLargeOutput($product->getUser());
            /** @var Address $address */
            $address = $product->getAddress();
            $productOutput['address'] = AddressOutput::getLargeOutput($address);
            $productOutput['status'] = $this->productStatus->getStatus($product);
            $productOutput['product_moderation'] = ModerationOutput::getOutputForModerationList($product->getModerations()->last());
            /** @var array|ArrayCollection|PersistentCollection $gallery */
            $gallery = $product->getGallery();
            $productOutput['gallery'] = null;
            if ($gallery) {
                $productOutput['gallery'] = FileOutput::getMediumCollectionOutput($gallery, [
                    'fileManager' => $this->fileManager,
                    'is_need_version' => true
                ]);
            }
            /** @var array|ArrayCollection|PersistentCollection $files */
            $files = $product->getFiles();
            $productOutput['files'] = null;
            if ($files) {
                $productOutput['files'] = FileOutput::getMediumCollectionOutput($files, [
                    'fileManager' => $this->fileManager
                ]);
            }

            $listOutput[] = $productOutput;
        }

        return !empty($listOutput) ? $listOutput : null;
    }

    /**
     * @param Product $product
     * @return array
     */
    public function getInsideOutput(Product $product): array
    {
        $productOutput = ProductOutput::getLargeOutput($product);
        $productOutput['owner'] = UserOutput::getLargeOutput($product->getUser());
        /** @var Address $address */
        $address = $product->getAddress();
        $productOutput['address'] = AddressOutput::getLargeOutput($address);
        $productOutput['status'] = $this->productStatus->getStatus($product);
        $productOutput['product_moderation'] = ModerationOutput::getOutputForModerationList($product->getModerations()->last());
        /** @var array|ArrayCollection|PersistentCollection $gallery */
        $gallery = $product->getGallery();
        $productOutput['gallery'] = null;
        if ($gallery) {
            $productOutput['gallery'] = FileOutput::getMediumCollectionOutput($gallery, [
                'fileManager' => $this->fileManager,
                'is_need_version' => true
            ]);
        }

        return $productOutput;
    }

    /**
     * @param null $product_slug
     * @return Product|null
     */
    public function getProductBySlug($product_slug = null)
    {
        if (!$product_slug) {
            return null;
        }
        /** @var Product|null $product */
        $product = $this->entityManager->getRepository(Product::class)
            ->findOneBy(['slug'=> $product_slug]);

        return $product;
    }

    /**
     * @param Product $product
     * @return array
     */
    public function getPublicSingleOutput(Product $product): array
    {
        /** @var array|ArrayCollection|PersistentCollection $files */
        $files = $product->getFiles();
        /** @var array|ArrayCollection|PersistentCollection $gallery */
        $gallery = $product->getGallery();
        /** @var Address $address */
        $address = $product->getAddress();
        /** @var Paginator $publishedProducts */
        $publishedProducts = $this->getPublishedProductsByUser($product->getUser());
        $paginator = $publishedProducts->getPages();

        $productOutput = ProductOutput::getMediumOutput($product);
        $productOutput['owner'] = UserOutput::getLargeOutput($product->getUser());
        $productOutput['owner']['count_published_products'] = $paginator->totalItemCount;
        $productOutput['urls']['user_products'] = UserOutput::getUserProductsUrl($product->getUser());
        $productOutput['files'] = null;
        if ($files) {
            $productOutput['files'] = FileOutput::getMediumCollectionOutput($files, [
                'fileManager' => $this->fileManager
            ]);
        }
        $productOutput['gallery'] = null;
        if ($gallery) {
            $productOutput['gallery'] = FileOutput::getMediumCollectionOutput($gallery, [
                'fileManager' => $this->fileManager,
                'is_need_version' => true
            ]);
        }
        $productOutput['address'] = null;
        if ($address) {
            $productOutput['address'] = AddressOutput::getLargeOutput($address);
        }

        return $productOutput;
    }

    /**
     * @param string $search
     * @param string $catalog_root_url
     * @param string|null $fabricator
     * @return array
     */
    public function productsSearchAutoComplete(string $search = '', string $catalog_root_url, string $fabricator = null): array
    {
        $products = $this->entityManager->getRepository(Product::class)
            ->searchPublicProducts($search, $fabricator);

        if (empty($products)) {
            return [];
        }

        $category_block = [];
        $sub_category_block = [];
        $product_block = [];
        /** @var Product $product */
        foreach ($products as $product){
            $productCategories = $product->getCategories();
            $subCategories = CategoryManager::filteredCollectionBySubcategory($productCategories);
            $categories = CategoryManager::filteredCollectionByCategory($productCategories);

            $arr_phrase = self::phraseProcessing($search, $product->getName());

            if (!empty($arr_phrase)) {
                /** @var Category $category */
                foreach ($categories as $category) {
                    $category_name = $category->getName();
                    $category_block[$category_name] = [
                        'arr_phrase' => $arr_phrase,
                        'section' => $category_name,
                        'type' => 'section',
                        'url' => $catalog_root_url . '/' . $category->getSlug(),
                    ];
                }
                /** @var Category $subCategory */
                foreach ($subCategories as $subCategory) {
                    $sub_category_name = $subCategory->getName();
                    $sub_category_block[$sub_category_name] = [
                        'arr_phrase' => $arr_phrase,
                        'section' => $sub_category_name,
                        'type' => 'section',
                        'url' => $catalog_root_url . '/' . $subCategory->getParent()->getSlug() . '/' . $subCategory->getSlug(),
                    ];
                }

                $product_block[$arr_phrase['phrase'] ?? $product->getName()] = [
                    'arr_phrase' => $arr_phrase,
                    'type' => 'string',
                    'url' => $catalog_root_url,
                ];
            }
        }
        $output = [];
        $arrCategory = [];
        $max_category = 5;
        $max_product = 5;

        $count = 0;
        foreach ($sub_category_block as $value) {
            if ($count >= $max_category){
                break;
            }
            $arrCategory[] = $value;
            $count++;
        }
        foreach ($category_block as $value) {
            if ($count >= $max_category){
                break;
            }
            $arrCategory[] = $value;
            $count++;
        }
        krsort($arrCategory);

        foreach ($arrCategory as $value) {
            $output[] = $value;
        }
        $all_count = $max_category + $max_product;
        $count_product = 0;

        foreach ($product_block as $value) {
            if ($count_product >= $max_product || $count >= $all_count){
                break;
            }
            $output[] = $value;
            $count_product++;
            $count++;
        }

        return $output;
    }

    /**
     * @param null $search
     * @param string $string
     * @return array
     */
    public static function phraseProcessing($search = null, $string = ''): array
    {
        if (empty($string)) {

            return [];
        }
        $search = trim($search);

        $search_regexp = str_ireplace(
            ['<','>','\\','!','$','(',')','*','+','.','?','[',']','^','{','|','}'],
            ['','','\\\\','\\!','\\$','\\(','\\)','\\*','\\+','\\.','\\?','\\[','\\]','\\^','\\{','\\|','\\}',],
            $search
        );

        $regexp = [
            'l1' => mb_strlen($search) >= 10 ?'':'(\S*\s*)?',
            'l2' => mb_strlen($search) >= 30 ?'':'(\S*\s*)?',
            'r1' => mb_strlen($search) >= 40 ?'':'(\s*\S*)?',
            'r2' => mb_strlen($search) >= 20 ?'':'(\s*\S*)?',
        ];

        preg_match('/'.$regexp['l1'].$regexp['l2'].'\S*'.$search_regexp.'\S*'.$regexp['r1'].$regexp['r2'].'/iu', $string, $matches);
        $arr_phrase = [];

        if (isset($matches[0])) {

            $pre_word = '';
            $post_word = '';
            $exploded_word = preg_split('/'.$search_regexp.'/iu', $matches[0], 2);

            if (isset($exploded_word[0]) && !empty($exploded_word[0])) {
                $pre_word = $exploded_word[0];
            }

            if (isset($exploded_word[1]) && !empty($exploded_word[1])) {
                $post_word = $exploded_word[1];
            }

            $phrase = $pre_word.$search.$post_word;

            $arr_phrase = [
                'pre_word' => $pre_word,
                'found_word' => $search,
                'post_word' => $post_word,
                'phrase' => trim($phrase),
            ];
        }

        return $arr_phrase;
    }

    /**
     * @param Product $product
     * @return mixed
     * @throws \Exception
     */
    public function getSimilarProducts(Product $product)
    {
        $page = 1;
        $limit = 4;
        $similarProducts = $this->entityManager->getRepository(Product::class)
            ->searchSimilarProducts($product, $page, $limit);

        return $similarProducts;
    }

    /**
     * @param array $criterion
     * @return array
     * @throws \Exception
     */
    public function getLastProductsByCriteria(array $criterion = []): array
    {
        $result = [];
        $limit = 100;
        $max_sort = 1000;
        // подготавливаем массив для сортировки
        foreach ($criterion as $k=>$v) {
            $is_sort = isset($v['sort']) && !empty($v['sort']) && (int) $v['sort'] > 0;
            $sort = $is_sort ? (int) $v['sort'] : 0;
            $sort = $sort > $max_sort ? $max_sort: $sort;
            $criterion[$k]['sort'] = $sort;
        }
        // сортировка
        usort($criterion, function ($a, $b) {
            return $a['sort'] - $b['sort'];
        });

        $exclude_product = [];
        $i = 0;
        foreach ($criterion as $key => $item) {
            if($i >= $limit){
                break;
            }
            //// выборка по категории ////
            if (array_key_exists('criterion', $item)
                && array_key_exists('criterion_ident', $item)
                && $item['criterion'] === 'category'
                && !empty($item['criterion_ident'])) {

                $category = $this->categoryManager->getCategoryBySlug($item['criterion_ident']);
                //получаем объявление по категории и исключаем уже выбранные объявления
                $product = $this->getLastProductsByCategory($category, $exclude_product);
                if ($product) {
                    $exclude_product[] = $product;
                    $result[$key] = $product;
                    $i++;
                }
            }
            //// конец выборки по категории ////
            /// возможно будет еще какой-нибудь критерий
        }

        return $result;
    }

    /**
     * @param null $category
     * @param array $exclude_product
     * @return Product|null
     * @throws \Exception
     */
    public function getLastProductsByCategory($category = null, array $exclude_product = [])
    {
        if (!$category instanceof Category) {
            return null;
        }
        /** @var Product $product */
        $product = $this->entityManager->getRepository(Product::class)
            ->getLastProductsByCategory($category, $exclude_product);

        return $product ?? null;
    }

    /**
     * @param Form $form
     * @param $session_data
     * @return Form
     */
    public function fillProductFormWithSessionData(Form $form, $session_data): Form
    {
        if (!empty($session_data)) {
            $form->setData([
                'name' => $session_data['name'] ?? '',
                'description' => $session_data['description'] ?? '',
                'category' => $session_data['category'] && $session_data['category']['id'] ? $session_data['category']['id'] :'',
                'price' => $session_data['price'] ?? '',
                'safe_deal' => $session_data['safe_deal'] ?? '',
                'address' => $session_data['address'] ?? '',
                'csrf' => $session_data['csrf'] ?? '',
            ]);
        }

        return $form;
    }

    /**
     * @param array $product_data
     * @return array
     */
    public function getFormDataWithProductData(array $product_data = []): array
    {
        if (!empty($product_data)) {
            if (array_key_exists('category', $product_data) && \is_array($product_data['category'])) {
                $product_data['category'] = $product_data['category']['id'] ?? '';
            }
            if (array_key_exists('gallery', $product_data)) {
                $product_data['gallery'] = '';
            }
            if (array_key_exists('files', $product_data)) {
                $product_data['files'] = '';
            }
        }

        return $product_data;
    }

    /**
     * @param SessionPlugin $sessionPlugin
     * @param array $preparedData
     * @return mixed
     * @throws \Exception
     */
    public function editFilesAndSaveDataInSession(SessionPlugin $sessionPlugin, array $preparedData)
    {
        $product_data = $preparedData['post_data']['product'];
        $saved_gallery = $preparedData['post_data']['saved_gallery'];
        $saved_files = $preparedData['post_data']['saved_files'];
        unset($product_data['csrf']);
        // сохранение файла в бд
        if (isset($product_data['files']) && \is_array($product_data['files'])) {
            $product_data['files'] = $this->saveProductTemporaryFiles($product_data['files']);
        } else {
            $product_data['files'] = [];
        }
        // сохранение изображения в бд
        if (isset($product_data['gallery']) && \is_array($product_data['gallery'])) {
            $product_data['gallery'] = $this->saveProductTemporaryFiles($product_data['gallery']);
        } else {
            $product_data['gallery'] = [];
        }

        //// *** edit image *** ////
        if ($saved_gallery && \is_array($saved_gallery)) {
            foreach ($saved_gallery as $images) {
                $product_data['gallery'][] = $images;
            }
            //метод редактирования поддерживает пока только *rotate*
            $product_data['gallery'] = $this->editSavedProductGallery($product_data['gallery'] ?? []);
        }
        //// *** end edit image *** ////

        //// *** edit files *** ////
        if ($saved_files && \is_array($saved_files)) {
            foreach ($saved_files as $saved_file) {
                $product_data['files'][] = $saved_file;
            }
        }
        //// *** end edit files *** ////

        $category = $this->categoryManager->getCategoryById($product_data['category']);
        if ($category) {
            $product_data['category'] = [
                'id' => $category->getId(),
                'name' => $category->getName(),
                'parent_id' => $category->getParent()->getId(),
                'parent_name' => $category->getParent()->getName()
            ];
        }

        //сохраняем результат в сессию для последующего использования или сохранения
        $sessionPlugin->addSessionVar($product_data, self::SESSION_PRODUCT_FLAG);

        return $product_data;
    }

    /**
     * post данные для debug
     *
     * @param Product|null $product
     * @return array
     */
    public function getTestingProductPostData(Product $product = null): array
    {
        require_once __DIR__.'./../fixture/products/files/DisablePhpUploadChecks.php';

        $csrf = new \Zend\Form\Element\Csrf('csrf');
        $csrf->setCsrfValidatorOptions([
            'timeout' => 3600
        ]);
        /** @var ArrayCollection|PersistentCollection $categories */
        if ($product && $product->getCategories()) {
            $categories = $product->getCategories();
        } else {
            $categories = $this->entityManager->getRepository(Category::class)
                ->findAll();
        }
        /** @var Category $category */
        $category = $categories->filter(function (Category $entity) {
            return $entity->getParent() !== null;
        })->first();
        $address = null;
        if ($product && $product->getAddress()) {
            $address = $product->getAddress();
        }

        if ($product && $product->getGallery()) {
            /** @var ArrayCollection|PersistentCollection $gallery */
            $gallery = $product->getGallery();
            /** @var File $image */
            $image = $gallery->first();
            $imagePath = $this->fileManager->getPathFile($image);
            $imgParts = pathinfo($imagePath);
            $foto = [
                'id' => $image->getId(),
                'name' => $image->getOriginName(),
                'path' => '/img/'.$image->getId().'.'.$imgParts['extension'],
                'rotate' => 90
            ];
        } else {
            $foto = [
                'name' => 'bravia_tv.jpg',
                'type' => 'image/jpeg',
                'tmp_name' => __DIR__.'./../fixture/products/files/bravia_tv.jpg',
                'size' => filesize(__DIR__.'./../fixture/products/files/bravia_tv.jpg'),
                'error' => 0
            ];
        }
        $price = $product ? (int) $product->getPrice() : '320';
        $postData = [
            'csrf' => $csrf->getValue(),
            'category' => $category->getId(),
            'name' => $product ? $product->getName() : 'Брусы, доски в ассортименте',
            'price' => (string) $price,
            'safe_deal' => '0',
            'description' => $product ? $product->getDescription() : 'Продаются брусы, доски различных размеров',
            'address' =>
                [
                    'country' => $address ? $address->getCountry() : 'Россия',
                    'apartment' => $address ? $address->getApartment() : '',
                    'post_code' => $address ? $address->getPostcode() : '',
                    'lat' => $address ? $address->getLat() : '53.24480521358528',
                    'lng' => $address ? $address->getLng() : '34.3648682566223',
                    'region' => $address ? $address->getRegion() : 'Брянская область',
                    'city' => $address ? $address->getCity() : 'Брянск',
                    'street' => $address ? $address->getStreet() : 'улица Горького',
                    'house' => $address ? $address->getHouse() : '23',
                ],
            'gallery' => [0 => $foto]
        ];

        return $postData;
    }

    /**
     * @param Product $product
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function stopPublish(Product $product)
    {
        $product->setVisible(0);

        $this->entityManager->flush();

        return true;
    }

    /**
     * @param Product $product
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function startPublish(Product $product)
    {
        $product->setVisible(1);

        $this->entityManager->flush();

        return true;
    }

    /**
     * @param Product $product
     * @throws \Exception
     */
    public function removeProduct(Product $product)
    {
        // 1 удаление связи с категорией
        /** @var ArrayCollection|PersistentCollection $categories */
        $categories = $product->getCategories();
        if ($categories) {
            $categories->clear();
        }
        // 2 удаление связи с городами
        /** @var ArrayCollection|PersistentCollection $cities */
        $cities = $product->getCities();
        if ($cities) {
            $cities->clear();
        }
        // 3 удалить запись в таблице адрес
        /** @var Address $address */
        $address = $product->getAddress();
        if ($address) {
            $product->setAddress(Null);
            $this->entityManager->remove($address);
        }
        // 4 удалием связи файлов
        /** @var ArrayCollection|PersistentCollection $gallery */
        $gallery = $product->getGallery();
        $clone_gallery = [];
        if ($gallery) {
            $clone_gallery = clone $gallery;
            $gallery->clear();
        }
        // 5 удалием связи галереи
        $files = $product->getFiles();
        $clone_files = [];
        if ($files) {
            $clone_files = clone $files;
            $files->clear();
        }
        if (!empty($clone_files) || !empty($clone_gallery)) {
            // 6 удаляем файлы из базы и с диска
            $this->entityManager->persist($product);
            $this->entityManager->flush($product);

            /** @var File $file */
            foreach ($clone_gallery as $file){
                $this->fileManager->delete($file->getId());
            }
            /** @var File $file */
            foreach ($clone_files as $file){
                $this->fileManager->delete($file->getId());
            }
        }
        // 6 удалить связь и сущности модерации
        /** @var ArrayCollection|PersistentCollection $moderations */
        $moderations = $product->getModerations();
        if (!empty($moderations)) {
            $clone_moderations = clone $moderations;
            $moderations->clear();

            /** @var Moderation $moderation */
            foreach ($clone_moderations as $moderation) {
                $this->entityManager->remove($moderation);
            }
            $this->entityManager->flush();
        }
        // 7 удалить связь и сущности prolongation
        /** @var ArrayCollection|PersistentCollection $prolongations */
        $prolongations = $product->getProductProlongations();
        if (!empty($prolongations)) {
            $clone_prolongations = clone $prolongations;
            $prolongations->clear();

            /** @var ProductProlongation $prolongation */
            foreach ($clone_prolongations as $prolongation) {
                $this->entityManager->remove($prolongation);
            }
            $this->entityManager->flush();
        }
        // 8 удалить Product
        $this->entityManager->remove($product);
        $this->entityManager->flush();
    }
}