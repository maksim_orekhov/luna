<?php
namespace Application\Service;

use Application\Entity\Category;
use Application\Entity\Fabricator;
use Application\Entity\User;
use Core\Entity\Interfaces\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;

class FabricatorManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var CategoryManager
     */
    private $categoryManager;
    /**
     * @var ModerationManager
     */
    private $moderationManager;


    /**
     * AddressManager constructor.
     * @param EntityManager $entityManager
     * @param CategoryManager $categoryManager
     * @param ModerationManager $moderationManager
     */
    public function __construct(EntityManager $entityManager,
                                CategoryManager $categoryManager,
                                ModerationManager $moderationManager)
    {
        $this->entityManager = $entityManager;
        $this->categoryManager = $categoryManager;
        $this->moderationManager = $moderationManager;
    }


    /**
     * @param User $user
     * @param array $data
     * @return Fabricator|mixed
     * @throws \Exception
     */
    public function createCustomFabricator(User $user, array $data)
    {
        $category_collection = new ArrayCollection();
        $categories = $data['category'] ?? [];
        $ar_category = [];
        $ar_sub_category = [];
        foreach ($categories as $category_id) {
            /** @var Category $subCategory */
            $subCategory = $this->categoryManager->getCategoryById((int) $category_id);
            if ($subCategory) {
                /** @var Category $category */
                $category = $subCategory->getParent();
                if ($category) {
                    $ar_category[$category->getId()] = $category;
                }
                $ar_sub_category[$subCategory->getId()] = $subCategory;
            }
        }
        foreach ($ar_category as $item) {
            $category_collection->add($item);
        }
        foreach ($ar_sub_category as $item) {
            $category_collection->add($item);
        }

        $fabricator = $user->getFabricator();
        if ($fabricator === null) {
            $fabricator = new Fabricator();
        }
        $current_date = new \DateTime();

        $fabricator->setName($data['name']);
        $fabricator->setInn($data['inn']);
        $fabricator->setKpp($data['kpp']);
        $fabricator->setLegalAddress($data['legal_address']);
        $fabricator->setCreated($current_date);
        $fabricator->setUser($user);

        $this->entityManager->persist($fabricator);
        $this->entityManager->flush($fabricator);

        //отправляем на модерацию
        $moderation = $this->moderationManager->openModeration($fabricator, $current_date);
        $moderation->setApproved(true);
        $moderation->setResolutionDate($current_date);
        $this->entityManager->persist($moderation);
        $this->entityManager->flush($moderation);

        $fabricator->addModeration($moderation);

        /** @var ArrayCollection|PersistentCollection $fabricatorCategories */
        $fabricatorCategories = $fabricator->getCategories();
        if ($fabricatorCategories) {
            $fabricatorCategories->clear();
            $this->entityManager->persist($fabricator);
            $this->entityManager->flush($fabricator);
            $this->entityManager->refresh($fabricator);
        }
        $fabricator->setCategories($category_collection);

        $this->entityManager->persist($fabricator);
        $this->entityManager->flush($fabricator);

        return $fabricator;
    }
}