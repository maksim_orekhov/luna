<?php
namespace Application\Service;

use Application\Entity\Product;
use Application\Entity\Moderation;
use Application\Entity\User;
use Doctrine\ORM\EntityManager;

/**
 * Class ProductModerationManager
 * @package Application\Service
 */
class ProductModerationManager
{
    use PaginatorOutputTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * ProductModerationManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Moderation $moderation
     * @param $formData
     * @return Moderation
     * @throws \Doctrine\ORM\ORMException
     * @throws \Exception
     */
    public function saveFormData(Moderation $moderation, $formData)
    {
        $moderation->setApproved($formData['approved']);
        $moderation->setResolutionDate(new \DateTime());
        if (isset($formData['description'])) {
            $moderation->setDescription($formData['description']);
        }

        $this->entityManager->flush();

        return $moderation;
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return mixed
     * @throws \Exception
     */
    public function getProductsOnModeration(User $user = null, int $page, int $per_page = null)
    {
        return $this->entityManager->getRepository(Product::class)
            ->selectModerationProducts($user, $page, $per_page);
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return mixed
     * @throws \Exception
     */
    public function getDeclinedProducts(User $user = null, int $page, int $per_page = null)
    {
        return $this->entityManager->getRepository(Product::class)
            ->selectDeclineProducts($user, $page, $per_page);
    }
}