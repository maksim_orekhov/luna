<?php

namespace Application\Service\Status\Factory;

use Application\Service\Status\ProductStatus\ArchiveStatus;
use Application\Service\Status\ProductStatus\DeclinedStatus;
use Application\Service\Status\ProductStatus\PublishedStatus;
use Application\Service\Status\ProductStatus\PublishStoppedStatus;
use Application\Service\Status\ProductStatus\WaitingModerationStatus;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Service\Status\ProductStatus;

/**
 * Class ProductStatusFactory
 * @package Application\Service\Status\Factory
 */
class ProductStatusFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ProductStatus|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $waitingModerationStatus = new WaitingModerationStatus();
        $declinedStatus = new DeclinedStatus();
        $publishStoppedStatus = new PublishStoppedStatus();
        $archiveStatus = new ArchiveStatus();
        $publishedStatus = new PublishedStatus();

        return new ProductStatus(
            $waitingModerationStatus,
            $declinedStatus,
            $publishStoppedStatus,
            $archiveStatus,
            $publishedStatus
        );
    }
}