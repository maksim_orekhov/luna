<?php

namespace Application\Service\Status;
use Application\Entity\Interfaces\ProductInterface;
use Application\Service\Status\ProductStatus\ArchiveStatus;
use Application\Service\Status\ProductStatus\DeclinedStatus;
use Application\Service\Status\ProductStatus\ProductStatusInterface;
use Application\Service\Status\ProductStatus\PublishedStatus;
use Application\Service\Status\ProductStatus\PublishStoppedStatus;
use Application\Service\Status\ProductStatus\WaitingModerationStatus;

/**
 * Class ProductStatus
 * @package Application\Service\Status
 */
class ProductStatus
{
    const PRODUCT_STATUSES = [ // Important! Не менять значения ключа 'status'
        '1' =>  ['code' => 1,  'status' => 'waiting_moderation',    'priority' => 1,  'is_blocker' => false, 'name' => 'Ожидает модерации'],
        '10' => ['code' => 10, 'status' => 'declined',              'priority' => 10, 'is_blocker' => false, 'name' => 'Отклонено модератором'],
        '20' => ['code' => 20, 'status' => 'publish_stopped',       'priority' => 20, 'is_blocker' => false, 'name' => 'Показ остановлен'],
        '30' => ['code' => 30, 'status' => 'archive',               'priority' => 30, 'is_blocker' => true,  'name' => 'В архиве'],
        '40' => ['code' => 40, 'status' => 'published',             'priority' => 40, 'is_blocker' => false, 'name' => 'Публикуется'],
        '99' => ['code' => 99, 'status' => 'undefined',             'priority' => 99, 'is_blocker' => true,  'name' => 'Статус не определен']
    ];

    /**
     * @var array
     */
    private $status_objects;

    public function __construct(WaitingModerationStatus $waitingModerationStatus,
                                DeclinedStatus $declinedStatus,
                                PublishStoppedStatus $publishStoppedStatus,
                                ArchiveStatus $archiveStatus,
                                PublishedStatus $publishedStatus)
    {
        $this->status_objects = [
            'waiting_moderation'    => $waitingModerationStatus,
            'declined'              => $declinedStatus,
            'publish_stopped'       => $publishStoppedStatus,
            'archive'               => $archiveStatus,
            'published'             => $publishedStatus,
            'undefined'             => null,
        ];
    }

    /**
     * @param ProductInterface $product
     * @return array
     */
    public function getStatus(ProductInterface $product): array
    {
        $status = null;
        /** @var array $product_statuses */
        $product_statuses = self::sortStatusArrayByPriority(self::PRODUCT_STATUSES);

        foreach ($product_statuses as $product_status) {

            $statusObject = $this->status_objects[$product_status['status']];

            if ($statusObject instanceof ProductStatusInterface && $statusObject->isProductInStatus($product)) {

                $status = $product_status;

                if (true === $product_status['is_blocker']) {

                    return $status;
                }
            }
        }

        if (null === $status) {

            $status = self::PRODUCT_STATUSES['99'];
        }

        return $status;
    }

    /**
     * @return array
     */
    public static function getStatuses(): array
    {
        return self::PRODUCT_STATUSES;
    }

    /**
     * Сортировка массива стутасов по приоритету
     *
     * @param array $statuses
     * @return array
     */
    private static function sortStatusArrayByPriority(array $statuses): array
    {
        usort($statuses, function($a, $b) {
            return ($a['priority'] < $b['priority']) ? -1 : 1;
        });

        return $statuses;
    }
}