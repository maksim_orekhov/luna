<?php

namespace Application\Service\Status;

use Application\Entity\Shop;
use Application\Service\Status\ShopStatus\DeclinedStatus;
use Application\Service\Status\ShopStatus\PublishedStatus;
use Application\Service\Status\ShopStatus\ShopStatusInterface;
use Application\Service\Status\ShopStatus\WaitingModerationStatus;

/**
 * Class ShopStatus
 * @package Application\Service\Status
 */
class ShopStatus
{
    const SHOP_STATUSES = [ // Important! Не менять значения ключа 'status'
        '1' =>  ['code' => 1,  'status' => 'waiting_moderation',    'priority' => 1,  'is_blocker' => false, 'name' => 'Ожидает модерации'],
        '10' => ['code' => 10, 'status' => 'declined',              'priority' => 10, 'is_blocker' => false, 'name' => 'Отклонено модератором'],
        '30' => ['code' => 40, 'status' => 'published',             'priority' => 40, 'is_blocker' => false, 'name' => 'Публикуется'],
        '99' => ['code' => 99, 'status' => 'undefined',             'priority' => 99, 'is_blocker' => true,  'name' => 'Статус не определен']
    ];

    /**
     * @var array
     */
    private $status_objects;

    public function __construct(WaitingModerationStatus $waitingModerationStatus,
                                DeclinedStatus $declinedStatus,
                                PublishedStatus $publishedStatus)
    {
        $this->status_objects = [
            'waiting_moderation'    => $waitingModerationStatus,
            'declined'              => $declinedStatus,
            'published'             => $publishedStatus,
            'undefined'             => null,
        ];
    }

    /**
     * @param Shop $shop
     * @return array
     */
    public function getStatus(Shop $shop): array
    {
        $status = null;
        /** @var array $shop_statuses */
        $shop_statuses = self::sortStatusArrayByPriority(self::SHOP_STATUSES);

        foreach ($shop_statuses as $shop_status) {
            /** @var ShopStatusInterface $statusObject */
            $statusObject = $this->status_objects[$shop_status['status']];

            if ($statusObject instanceof ShopStatusInterface && $statusObject->isShopInStatus($shop)) {

                $status = $shop_status;

                if (true === $shop_status['is_blocker']) {

                    return $status;
                }
            }
        }

        if (null === $status) {

            $status = self::SHOP_STATUSES['99'];
        }

        return $status;
    }

    /**
     * @return array
     */
    public static function getStatuses(): array
    {
        return self::SHOP_STATUSES;
    }

    /**
     * Сортировка массива стутасов по приоритету
     *
     * @param array $statuses
     * @return array
     */
    private static function sortStatusArrayByPriority(array $statuses): array
    {
        usort($statuses, function($a, $b) {
            return ($a['priority'] < $b['priority']) ? -1 : 1;
        });

        return $statuses;
    }
}