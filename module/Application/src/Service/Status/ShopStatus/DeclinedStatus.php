<?php
namespace Application\Service\Status\ShopStatus;

use Application\Entity\Moderation;
use Application\Entity\Shop;

/**
 * Class DeclinedStatus
 * @package Application\Service\Status\ShopStatus
 */
class DeclinedStatus implements ShopStatusInterface
{
    /**
     * @param Shop $shop
     * @return bool|mixed
     */
    public function isShopInStatus(Shop $shop)
    {
        /** @var Moderation $moderation */
        $moderation = $shop->getModerations()->last();

        return false === $moderation->isApproved();
    }
}