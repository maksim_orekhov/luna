<?php

namespace Application\Service\Status\ShopStatus;

use Application\Entity\Shop;

/**
 * Interface ShopStatusInterface
 * @package Application\Service\Status\ProductStatus
 */
interface ShopStatusInterface
{
    /**
     * @param Shop $shop
     * @return mixed
     */
    public function isShopInStatus(Shop $shop);
}