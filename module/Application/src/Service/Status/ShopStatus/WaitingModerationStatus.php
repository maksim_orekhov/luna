<?php
namespace Application\Service\Status\ShopStatus;

use Application\Entity\Moderation;
use Application\Entity\Shop;

/**
 * Class WaitingModerationStatus
 * @package Application\Service\Status\ShopStatus
 */
class WaitingModerationStatus implements ShopStatusInterface
{
    /**
     * @param Shop $shop
     * @return bool|mixed
     */
    public function isShopInStatus(Shop $shop)
    {
        /** @var Moderation $moderation */
        $moderation = $shop->getModerations()->last();

        return null === $moderation->isApproved();
    }
}