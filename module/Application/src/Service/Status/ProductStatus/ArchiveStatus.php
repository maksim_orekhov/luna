<?php

namespace Application\Service\Status\ProductStatus;

use Application\Entity\Interfaces\ProductInterface;

/**
 * Class ArchiveStatus
 * @package Application\Service\Status\ProductStatus
 */
class ArchiveStatus implements ProductStatusInterface
{
    /**
     * @param ProductInterface $product
     * @return bool|mixed
     */
    public function isProductInStatus(ProductInterface $product)
    {
        return $product->isRemoved();
    }
}