<?php

namespace Application\Service\Status\ProductStatus;

use Application\Entity\Interfaces\ProductInterface;


/**
 * Class PublishStoppedStatus
 * @package Application\Service\Status\ProductStatus
 */
class PublishStoppedStatus implements ProductStatusInterface
{
    /**
     * @param ProductInterface $product
     * @return bool|mixed
     */
    public function isProductInStatus(ProductInterface $product)
    {
        return !$product->isVisible();
    }
}