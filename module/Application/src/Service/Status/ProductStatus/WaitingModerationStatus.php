<?php

namespace Application\Service\Status\ProductStatus;

use Application\Entity\Interfaces\ProductInterface;
use Application\Entity\Moderation;

/**
 * Class WaitingModerationStatus
 * @package Application\Service\Status\ProductStatus
 */
class WaitingModerationStatus implements ProductStatusInterface
{
    /**
     * @param ProductInterface $product
     * @return bool|mixed
     */
    public function isProductInStatus(ProductInterface $product)
    {
        if ($product->isRemoved()) {

            return false;
        }

        /** @var Moderation $moderation */
        $moderation = $product->getModerations()->last();

        return null === $moderation->isApproved();
    }
}