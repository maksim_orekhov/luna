<?php

namespace Application\Service\Status\ProductStatus;

use Application\Entity\Interfaces\ProductInterface;

/**
 * Interface ProductStatusInterface
 * @package Application\Service\Status
 */
interface ProductStatusInterface
{
    /**
     * @param ProductInterface $product
     * @return mixed
     */
    public function isProductInStatus(ProductInterface $product);
}