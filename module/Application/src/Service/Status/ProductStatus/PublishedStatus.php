<?php

namespace Application\Service\Status\ProductStatus;

use Application\Entity\Interfaces\ProductInterface;
use Application\Entity\Moderation;

/**
 * Class PublishedStatus
 * @package Application\Service\Status\ProductStatus
 */
class PublishedStatus implements ProductStatusInterface
{
    /**
     * @param ProductInterface $product
     * @return bool|mixed
     */
    public function isProductInStatus(ProductInterface $product)
    {
        if ($product->isRemoved()) {

            return false;
        }

        if (false === $product->isVisible()) {

            return false;
        }

        /** @var Moderation $moderation */
        $moderation = $product->getModerations()->last();

        return $moderation->isApproved();
    }
}