<?php

namespace Application\Service\Status\ProductStatus;

use Application\Entity\Interfaces\ProductInterface;
use Application\Entity\Moderation;

/**
 * Class DeclinedStatus
 * @package Application\Service\Status\ProductStatus
 */
class DeclinedStatus implements ProductStatusInterface
{
    /**
     * @param ProductInterface $product
     * @return bool|mixed
     */
    public function isProductInStatus(ProductInterface $product)
    {
        if ($product->isRemoved()) {

            return false;
        }

        /** @var Moderation $moderation */
        $moderation = $product->getModerations()->last();

        return false === $moderation->isApproved();
    }
}