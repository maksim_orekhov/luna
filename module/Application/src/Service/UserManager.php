<?php
namespace Application\Service;

use Application\Entity\Email;
use Application\Entity\Phone;
use Application\Entity\Product;
use Application\Entity\Purchase;
use Application\Entity\User;
use Application\Service\Output\UserOutput;
use Behat\Transliterator\Transliterator;
use Core\Entity\Interfaces\EmailInterface;
use Core\Exception\LogicException;
use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BaseUserManager;
use Doctrine\ORM\EntityManager;
use ModuleFileManager\Service\FileManager;
use Zend\Paginator\Paginator;

/**
 * Class UserManager
 * @package Application\Service
 */
class UserManager extends BaseUserManager
{
    const MAX_RECURSION_LEVEL = 100;

    /**
     * @var BaseEmailManager
     */
    private $baseEmailManager;

    /**
     * @var FileManager
     */
    private $fileManager;

    /**
     * UserManager constructor.
     * @param EntityManager $entityManager
     * @param BaseEmailManager $baseEmailManager
     * @param FileManager $fileManager
     */
    public function __construct(EntityManager $entityManager,
                                BaseEmailManager $baseEmailManager,
                                FileManager $fileManager)
    {
        parent::__construct($entityManager);

        $this->fileManager = $fileManager;
        $this->baseEmailManager = $baseEmailManager;
    }

    /**
     * @param int $id
     * @return User
     */
    public function getUserById(int $id)
    {
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['id' => $id]);

        return $user;
    }

    /**
     * @param $slug
     * @return User|Null
     */
    public function getUserBySlug($slug)
    {
        /** @var User $user */
        $user = null;
        if ($slug && \is_string($slug)) {
            $user = $this->entityManager->getRepository(User::class)->findOneBy(['slug' => $slug]);
            if (!$user) {
                $user = $this->entityManager->getRepository(User::class)->findOneBy(['login' => $slug]);
            }
        }

       return $user;
    }

    /**
     * @param $login
     * @return User
     * @throws LogicException
     */
    public function getUserByLogin($login):User
    {
        $user = $this->selectUserByLogin($login);

        if (!$user instanceof User) {

            throw new LogicException(null, LogicException::USER_BY_LOGIN_NOT_FOUND);
        }

        return $user;
    }

    /**
     * @param $login_or_email
     * @return User|null
     */
    public function selectUserByLoginOrEmail($login_or_email)
    {
        $user = null;
        /** @var EmailInterface $email */
        $email = $this->baseEmailManager->getEmailVerifiedByValue($login_or_email);
        if ($email !== null) {
            /** @var User $user */
            $user = $this->getUserByEmail($email);
        }
        if ($user === null) {
            /** @var User $user */
            $user = $this->selectUserByLogin($login_or_email);
        }

        return $user;
    }

    /**
     * @param $login
     * @return User|null
     */
    public function selectUserByLogin($login)
    {
        /** @var User|null $user */
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['login' => $login]);

        return $user;
    }

    /**
     * @param User $user
     * @return array
     */
    public function getUserOutput(User $user): array
    {
        $userOutput = [];

        $userOutput['id'] = $user->getId();
        $userOutput['login'] = $user->getLogin();
        $userOutput['name'] = $user->getName();
        $userOutput['avatar_id'] = $user->getAvatar() ? $user->getAvatar()->getId() : null;
        /** @var Phone $phone */
        $phone = $user->getPhone();
        $userOutput['phone'] = $phone ? $phone->getPhoneNumber(): null;
        $userOutput['is_phone_verified'] = $phone ? $phone->getIsVerified(): null;
        /** @var Email $email */
        $email = $user->getEmail();
        $userOutput['email'] = $email ? $email->getEmail(): null;
        $userOutput['is_email_verified'] = $email ? $email->getIsVerified(): null;
        /** @var \DateTime $created */
        $created = $user->getCreated();
        $userOutput['created'] = $created ? $created->format('Y-m-d H:i:s') : null;
        $userOutput['deals'] = 1; // инфо о заключенных сделках
        $userOutput['subscriber'] = 2; // подписчиков
        $userOutput['subscribe'] = 3; // подписок

        return $userOutput;
    }

    public function getLargeOutput(User $user, $count_products = false, $count_purchases = false)
    {
        $userOutput = UserOutput::getLargeOutput($user);
        if ($count_products) {
            /** @var Paginator $products */
            $products = $this->entityManager->getRepository(Product::class)
                ->selectPublicProducts($user);
            $paginatorProducts = $products->getPages();
            $userOutput['count_published_products'] = $paginatorProducts->totalItemCount;
        }
        if ($count_purchases) {
            /** @var Paginator $purchases */
            $purchases = $this->entityManager->getRepository(Purchase::class)
                ->selectPublicPurchases($user);
            $paginatorPurchases = $purchases->getPages();
            $userOutput['count_published_purchases'] = $paginatorPurchases->totalItemCount;
        }

        return $userOutput;
    }

    /**
     * @param User $user
     * @param array $params
     * @return User
     * @throws LogicException
     * @throws \Exception
     */
    public function editUser(User $user, array $params = []): User
    {
        if (isset($params['avatar'])) {
            $user = $this->createUserAvatar($user, $params['avatar']);
        }
        if (isset($params['name']) && !empty($params['name'])) {
            $slug = $this->generateSlugUnique($user, $params['name']);
            $user->setName($params['name']);
            $user->setSlug($slug);
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }

        return $user;
    }

    /**
     * @param User $currentUser
     * @param string $name
     * @param int $recursion_level
     * @return string
     * @throws LogicException
     */
    public function generateSlugUnique(User $currentUser, string $name, $recursion_level = 0): string
    {
        if ($recursion_level >= self::MAX_RECURSION_LEVEL) {

            throw new LogicException(null, LogicException::USER_GENERATION_UNIQUE_SLUG_FAILED);
        }
        $slug = $this->generateSlug($name);
        if ( $recursion_level > 0 ) {
            $slug .= '_'.$recursion_level;
        }
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['slug' => $slug]);

        if ($user !== null && $user !== $currentUser) {
            //// если User с таким slug существует запускаем повторную генерацию ////
            return $this->generateSlugUnique($currentUser, $name, ++$recursion_level);
        }

        return $slug;
    }

    /**
     * @param string $name
     * @return string
     */
    private function generateSlug(string $name): string
    {
        return  Transliterator::transliterate(mb_strtolower(trim($name)),'_');
    }

    /**
     * @param User $user
     * @param array $file_data
     * @return User
     * @throws LogicException
     */
    public function createUserAvatar(User $user, array $file_data): User
    {
        if ($user->existAvatar()) {

            $user = $this->removeUserAvatar($user);
        }
        try {
            $file = $this->fileManager->write([
                'file_source' => $file_data['tmp_name'],
                'file_name' => $file_data['name'],
                'file_type' => $file_data['type'],
                'file_size' => $file_data['size'],
                'path' => User::PATH_AVATAR_FILES,
            ]);
            $user->setAvatar($file);
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }
        catch (\Throwable $t) {

            throw new LogicException(null, LogicException::USER_AVATAR_CREATE_FAILED);
        }

        return $user;
    }

    /**
     * @param User $user
     * @return User
     * @throws LogicException
     */
    public function removeUserAvatar(User $user): User
    {
        try {
            $avatar = $user->getAvatar();
            $user->removeAvatar();

            $this->fileManager->delete($avatar->getId());

            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }
        catch (\Throwable $t){

            throw new LogicException(null, LogicException::USER_AVATAR_REMOVE_FAILED);
        }

        return $user;
    }

    /**
     * @param $id
     * @return User|null
     */
    public function getUserByAvatarId($id)
    {
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->selectUsersByAvatarId((int) $id);

        return $user ?? null;
    }
}