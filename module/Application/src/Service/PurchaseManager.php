<?php
namespace Application\Service;

use Application\Entity\Address;
use Application\Entity\Moderation;
use Application\Entity\Purchase;
use Application\Entity\Category;
use Application\Entity\PurchaseProlongation;
use Application\Entity\User;
use Application\Service\Output\AddressOutput;
use Application\Service\Output\FileOutput;
use Application\Service\Output\ModerationOutput;
use Application\Service\Output\PaginatorOutput;
use Application\Service\Output\PurchaseOutput;
use Application\Service\Output\UserOutput;
use Application\Service\Status\ProductStatus;
use Behat\Transliterator\Transliterator;
use Core\Controller\Plugin\SessionPlugin;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;
use ModuleFileManager\Entity\File;
use ModuleFileManager\Service\FileManager;
use Zend\Form\Form;
use Zend\Paginator\Paginator;

/**
 * Class PurchaseManager
 * @package Application\Service
 */
class PurchaseManager
{
    use PaginatorOutputTrait;

    const SESSION_PURCHASE_FLAG = 'purchase_data';
    const DEFAULT_PUBLICATION_PERIOD = 30;
    const PURCHASE_FOLDER = '/purchase';

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var FileManager
     */
    private $fileManager;

    /**
     * @var ModerationManager
     */
    private $moderationManager;

    /**
     * @var AddressManager
     */
    private $addressManager;

    /**
     * @var CityManager
     */
    private $cityManager;

    /**
     * @var CategoryManager
     */
    private $categoryManager;

    /**
     * @var ProductStatus
     */
    private $productStatus;

    /**
     * PurchaseManager constructor.
     * @param EntityManager $entityManager
     * @param FileManager $fileManager
     * @param ModerationManager $moderationManager
     * @param AddressManager $addressManager
     * @param CityManager $cityManager
     * @param CategoryManager $categoryManager
     * @param ProductStatus $productStatus
     */
    public function __construct(EntityManager $entityManager,
                                FileManager $fileManager,
                                ModerationManager $moderationManager,
                                AddressManager $addressManager,
                                CityManager $cityManager,
                                CategoryManager $categoryManager,
                                ProductStatus $productStatus)
    {
        $this->entityManager = $entityManager;
        $this->fileManager = $fileManager;
        $this->moderationManager = $moderationManager;
        $this->addressManager = $addressManager;
        $this->cityManager = $cityManager;
        $this->categoryManager = $categoryManager;
        $this->productStatus = $productStatus;
    }

    /**
     * @param $id
     * @return Purchase|null
     */
    public function getPurchaseById($id)
    {
        if (!$id) {
            return null;
        }
        /** @var Purchase|null $purchase */
        $purchase = $this->entityManager->getRepository(Purchase::class)
            ->findOneBy(['id' => (int) $id]);

        return $purchase;
    }

    /**
     * @param null $city
     * @param Category|null $category
     * @param Category|null $subCategory
     * @return bool
     */
    public function isRoutePurchaseCorrect($city = null, $category = null, $subCategory = null):bool
    {
        if ($city && $category && $subCategory) {

            return !(!$subCategory || $subCategory->getParent() !== $category);
        }

        if ($city && $category && ! $subCategory) {


            return true;
        }

        if ($city && ! $category && ! $subCategory) {


            return true;
        }

        return false;
    }

    /**
     * @param array $data_files
     * @param string $base_path
     * @return array
     * @throws \Exception
     */
    public function saveImageTemporaryFile(array $data_files, $base_path = self::PURCHASE_FOLDER): array
    {
        $autoIncrementId = $this->entityManager->getRepository(Purchase::class)
            ->getNextPurchaseFileAutoIncrementId();

        $path = $base_path.'/'.$this->fileManager::generateFolderName($autoIncrementId);

        return FileOutput::getMediumOutput($this->fileManager->saveTemporaryFile($data_files, $path), [
            'fileManager' => $this->fileManager
        ]);
    }

    /**
     * @param array $data_files
     * @param string $base_path
     * @return array
     * @throws \Exception
     */
    public function savePurchaseTemporaryFiles(array $data_files, $base_path = self::PURCHASE_FOLDER): array
    {
        $filesOutput = [];

        foreach ($data_files as $data_file) {
            if (!empty($data_file) && \is_array($data_file)) {
                $autoIncrementId = $this->entityManager->getRepository(Purchase::class)
                    ->getNextPurchaseFileAutoIncrementId();
                $path = $base_path . '/' . $this->fileManager::generateFolderName($autoIncrementId);

                $filesOutput[] = FileOutput::getMediumOutput($this->fileManager->saveTemporaryFile($data_file, $path), [
                    'fileManager' => $this->fileManager
                ]);
            }
        }

        return $filesOutput;
    }

    /**
     * @param User $user
     * @param $data
     * @return Purchase
     * @throws \Exception
     */
    public function createPurchase(User $user, $data): Purchase
    {
        $current_date = new \DateTime();

        $purchase = new Purchase();
        $purchase->setUser($user);
        $purchase->setName($data['name']);
        $purchase->setDescription($data['description']);
        $purchase->setPrice(!empty($data['price']) ? $data['price'] : null);
        $purchase->setQuantity(!empty($data['quantity']) ? $data['quantity'] : null);
        $purchase->setMeasure($data['measure']);
        $purchase->setVisible(true);
        $purchase->setRemoved(false);
        $purchase->setPublicationPeriod(self::DEFAULT_PUBLICATION_PERIOD);
        $purchase->setCreated($current_date);
        $purchase->setChanged($current_date);

        $this->entityManager->persist($purchase);
        $this->entityManager->flush($purchase);
        // генерация и добавление slug и кода
        $this->setPurchaseSlugAndCode($purchase);
        //привязываем к категориям
        $sub_category = $this->categoryManager->getCategoryById($data['category']);
        if ($sub_category) {
            $purchase = $this->linkPurchaseWithCategory($purchase, $sub_category);
        }
        //добавляем адрес
        /** @var Address $address */
        $address = $this->addressManager->createAddress($data['address']);
        $purchase->setAddress($address);
        //на основе введенного адреса привязываем к городам
        $this->cityManager->linkAddressWithCities($purchase, $address);

        //отправляем на модерацию
        $moderation = $this->moderationManager->openModeration($purchase, $current_date);
        $purchase->addModeration($moderation);

        $this->entityManager->persist($purchase);
        $this->entityManager->flush($purchase);

        return $purchase;
    }

    /**
     * @param Purchase $purchase
     * @param $data
     * @return Purchase
     * @throws \Exception
     */
    public function editPurchase(Purchase $purchase, $data): Purchase
    {
        $current_date = new \DateTime();
        // первое что делаем при редактировании отправляем на модерацию
        /** @var  ArrayCollection|PersistentCollection $moderations */
        $moderations = $purchase->getModerations();
        $lastModeration = null;
        if ($moderations) {
            /** @var Moderation $lastModeration */
            $lastModeration = $moderations->last();
        }

        if (!$lastModeration || ($lastModeration && $lastModeration->isApproved() !== null)) {
            $moderation = $this->moderationManager->openModeration($purchase, $current_date);
            $purchase->addModeration($moderation);
        }

        $address = $purchase->getAddress();
        if ($address) {
            $this->addressManager->editAddress($address, $data['address']);
        }

        $purchase->setName($data['name']);
        $purchase->setDescription($data['description']);
        $purchase->setPrice(!empty($data['price']) ? $data['price'] : null);
        $purchase->setQuantity(!empty($data['quantity']) ? $data['quantity'] : null);
        $purchase->setMeasure($data['measure']);
        $purchase->setChanged($current_date);

        // генерация и добавление slug и кода
        $this->setPurchaseSlugAndCode($purchase);
        //привязываем к категориям
        $sub_category = $this->categoryManager->getCategoryById($data['category']);
        if ($sub_category) {
            $purchase = $this->linkPurchaseWithCategory($purchase, $sub_category);
        }

        //на основе введенного адреса привязываем к городам
        $this->cityManager->linkAddressWithCities($purchase, $address);

        $this->entityManager->persist($purchase);
        $this->entityManager->flush($purchase);

        return $purchase;
    }

    /**
     * @param Purchase $purchase
     */
    public function setPurchaseSlugAndCode(Purchase $purchase)
    {
        $code = $this->generatePurchaseCode($purchase);
        $slug = $this->generatePurchaseSlug($purchase);
        $purchase->setCode($code);
        $purchase->setSlug($slug.'_'.$code);
    }

    /**
     * @param Purchase $purchase
     * @param Category $sub_category
     * @return Purchase
     * @throws \Exception
     */
    public function linkPurchaseWithCategory(Purchase $purchase, Category $sub_category): Purchase
    {
        /** @var ArrayCollection|PersistentCollection $categories */
        $categories = $purchase->getCategories();
        if ($categories) {
            $categories->clear();
            $this->entityManager->persist($purchase);
            $this->entityManager->flush($purchase);
            $this->entityManager->refresh($purchase);
        }

        $category = $sub_category->getParent();
        $categoryCollection = new ArrayCollection();
        $categoryCollection->add($category);
        $categoryCollection->add($sub_category);

        $purchase->setCategories($categoryCollection);

        #$this->entityManager->persist($purchase);
        $this->entityManager->flush($purchase);

        return $purchase;
    }

    /**
     * @param Purchase $purchase
     * @param array $temporary_files
     * @return Purchase
     */
    public function attachPurchaseFiles(Purchase $purchase, array $temporary_files): Purchase
    {
        try {
            $files = $temporary_files['files'] ?? [];

            $is_clear_files = false;
            /** @var ArrayCollection|PersistentCollection $purchaseFiles */
            $purchaseFiles = $purchase->getFiles();
            if ($purchaseFiles) {
                $purchaseFiles->clear();
                $is_clear_files = true;
            }

            if ($is_clear_files) {
                $this->entityManager->persist($purchase);
                $this->entityManager->flush($purchase);
                $this->entityManager->refresh($purchase);
            }

            $filesCollection = new ArrayCollection();
            $unique_files = [];
            foreach ($files as $file) {
                if (isset($file['id'])) {
                    $unique_files[$file['id']] = $file;
                }
            }
            foreach ($unique_files as $unique_file) {
                /** @var File $purchaseFile */
                $purchaseFile = $this->fileManager->getFileById($unique_file['id']);
                $filesCollection->add($purchaseFile);
            }

            $purchase->setFiles($filesCollection);
            $this->entityManager->persist($purchase);
            $this->entityManager->flush($purchase);
        }
        catch (\Throwable $t ) {

            logException($t, 'add-file-errors');
        }

        return $purchase;
    }

    /**
     * @param Purchase $purchase
     * @return string
     */
    public function generatePurchaseCode(Purchase $purchase):string
    {
        return 'LNZ_'.str_pad($purchase->getId(), 4, '0', STR_PAD_LEFT);
    }

    /**
     * @param Purchase $purchase
     * @return string
     */
    public function generatePurchaseSlug(Purchase $purchase):string
    {
        return Transliterator::transliterate(mb_strtolower($purchase->getName()), '_');
    }

    /**
     * @param User $user
     * @return Paginator
     * @throws \Exception
     */
    public function getPublishedPurchasesByUser(User $user): Paginator
    {
        /** @var Paginator $products */
        $products = $this->entityManager->getRepository(Purchase::class)
            ->selectPublicPurchases($user);

        return $products;
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return array
     */
    public function getModerationPurchasesAndPaginatorOutput(User $user = null, int $page = 1, int $per_page = null): array
    {
        /** @var Paginator $paginatedPurchases */
        $paginatedPurchases = $this->getPurchasesOnModeration($user, $page, $per_page);
        $paginator = $paginatedPurchases->getPages();

        return [
            'purchases' => $this->getInsideListOutput($paginatedPurchases),
            'paginator' => $paginator ? PaginatorOutput::getOutput($paginator) : null
        ];
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return mixed
     */
    private function getPurchasesOnModeration(User $user = null, int $page, int $per_page = null)
    {
        return $this->entityManager->getRepository(Purchase::class)
            ->selectModerationPurchases($user, $page, $per_page);
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return array
     */
    public function getDeclinePurchasesAndPaginatorOutput(User $user = null, int $page = 1, int $per_page = null): array
    {
        /** @var Paginator $declinePurchases */
        $declinePurchases = $this->getDeclinedPurchases($user, $page, $per_page);
        $paginator = $declinePurchases->getPages();

        return [
            'purchases' => $this->getInsideListOutput($declinePurchases),
            'paginator' => $paginator ? PaginatorOutput::getOutput($paginator) : null
        ];
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return mixed
     */
    private function getDeclinedPurchases(User $user = null, int $page, int $per_page = null)
    {
        return $this->entityManager->getRepository(Purchase::class)
            ->selectDeclinePurchases($user, $page, $per_page);
    }

    /**
     * @param User|null $user
     * @param array $route_params
     * @return array
     */
    public function getPublicPurchasesAndPaginatorOutput(User $user = null, array $route_params): array
    {
        $publicPurchases = $this->getPublicPurchases($user, $route_params);
        $paginator = $publicPurchases->getPages();

        return [
            'purchases' => $this->getPublicListOutput($publicPurchases),
            'paginator' => $paginator ? $this->getPaginatorOutput($paginator) : null,
        ];
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return array
     */
    public function getPublishedPurchasesAndPaginatorOutput(User $user = null, int $page = 1, int $per_page = null): array
    {
        $publicPurchases = $this->getPublicPurchases($user, ['page' => $page, 'per_page' => $per_page]);
        $paginator = $publicPurchases->getPages();

        return [
            'purchases' => $this->getInsideListOutput($publicPurchases),
            'paginator' => $paginator ? $this->getPaginatorOutput($paginator) : null,
        ];
    }

    /**
     * @param User|null $user
     * @param array $route_params
     * @return mixed
     */
    public function getPublicPurchases(User $user = null, array $route_params)
    {
        return $this->entityManager->getRepository(Purchase::class)
            ->selectPublicPurchases($user, $route_params);
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return array
     */
    public function getArchivePurchasesAndPaginatorOutput(User $user = null, int $page = 1, int $per_page = null): array
    {
        $archivePurchases = $this->getArchivePurchases($user, $page, $per_page);
        $paginator = $archivePurchases->getPages();

        return [
            'purchases' => $this->getInsideListOutput($archivePurchases),
            'paginator' => $paginator ? PaginatorOutput::getOutput($paginator) : null
        ];
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return mixed
     */
    public function getArchivePurchases(User $user = null, int $page, int $per_page = null)
    {
        return $this->entityManager->getRepository(Purchase::class)
            ->selectArchivePurchases($user, $page, $per_page);
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return array
     */
    public function getInactivePurchasesAndPaginatorOutput(User $user = null, int $page = 1, int $per_page = null): array
    {
        $inactivePurchases = $this->getInactivePurchases($user, $page, $per_page);
        $paginator = $inactivePurchases->getPages();

        return [
            'purchases' => $this->getInsideListOutput($inactivePurchases),
            'paginator' => $paginator ? $this->getPaginatorOutput($paginator) : null
        ];
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return mixed
     */
    public function getInactivePurchases(User $user = null, int $page, int $per_page = null)
    {
        return $this->entityManager->getRepository(Purchase::class)
            ->selectInactivePurchases($user, $page, $per_page);
    }

    /**
     * @param $purchases
     * @return array|null
     */
    public function getPublicListOutput($purchases)
    {
        $listOutput = [];
        /** @var Purchase $purchase */
        foreach ($purchases as $purchase){
            $purchaseOutput = PurchaseOutput::getMediumOutput($purchase);
            $purchaseOutput['owner'] = UserOutput::getMediumOutput($purchase->getUser());
            /** @var Address $address */
            $address = $purchase->getAddress();
            $purchaseOutput['address'] = AddressOutput::getLargeOutput($address);

            $listOutput[] = $purchaseOutput;
        }

        return !empty($listOutput) ? $listOutput : null;
    }

    /**
     * @param $purchases
     * @return array|null
     */
    public function getInsideListOutput($purchases)
    {
        $listOutput = [];
        /** @var Purchase $purchase */
        foreach ($purchases as $purchase){
            $purchaseOutput = PurchaseOutput::getLargeOutput($purchase);
            $purchaseOutput['owner'] = UserOutput::getMediumOutput($purchase->getUser());
            /** @var Address $address */
            $address = $purchase->getAddress();
            $purchaseOutput['address'] = AddressOutput::getLargeOutput($address);
            $purchaseOutput['status'] = $this->productStatus->getStatus($purchase);
            $purchaseOutput['moderation'] = ModerationOutput::getOutputForModerationList($purchase->getModerations()->last());

            $listOutput[] = $purchaseOutput;
        }

        return !empty($listOutput) ? $listOutput : null;
    }

    /**
     * @param Purchase $purchase
     * @return array|null
     */
    public function getInsideOutput(Purchase $purchase)
    {
        $purchaseOutput = PurchaseOutput::getLargeOutput($purchase);
        $purchaseOutput['owner'] = UserOutput::getLargeOutput($purchase->getUser());
        /** @var Address $address */
        $address = $purchase->getAddress();
        $purchaseOutput['address'] = AddressOutput::getLargeOutput($address);
        $purchaseOutput['status'] = $this->productStatus->getStatus($purchase);
        $purchaseOutput['moderation'] = ModerationOutput::getOutputForModerationList($purchase->getModerations()->last());
        /** @var array|ArrayCollection|PersistentCollection $files */
        $files = $purchase->getFiles();
        $purchaseOutput['files'] = null;
        if ($files) {
            $purchaseOutput['files'] = FileOutput::getMediumCollectionOutput($files, [
                'fileManager' => $this->fileManager
            ]);
        }

        return $purchaseOutput;
    }

    /**
     * @param Purchase $purchase
     * @return bool
     * @throws \Exception
     */
    public function stopPublish(Purchase $purchase): bool
    {
        $purchase->setVisible(0);

        $this->entityManager->flush();

        return true;
    }

    /**
     * @param Purchase $purchase
     * @return bool
     * @throws \Exception
     */
    public function startPublish(Purchase $purchase): bool
    {
        $purchase->setVisible(1);

        $this->entityManager->flush();

        return true;
    }

    /**
     * @param array $route_params
     * @return mixed
     */
    public function getPublishedPurchasesByRouteData(array $route_params): Paginator
    {
        return $this->entityManager->getRepository(Purchase::class)
            ->findPublishedPurchasesByRouteParams($route_params);
    }

    /**
     * @param Purchase $purchase
     * @return array
     */
    public function getPublicSingleOutput(Purchase $purchase): array
    {
        $owner = $purchase->getUser();
        /** @var Paginator $publishedPurchases */
        $publishedPurchases = $this->getPurchasesProductsByUser($owner);
        $paginator = $publishedPurchases->getPages();

        $purchasesOutput = PurchaseOutput::getMediumOutput($purchase);
        $purchasesOutput['owner'] = UserOutput::getMediumOutput($purchase->getUser());
        $purchasesOutput['owner']['phone'] = $purchasesOutput['owner']['is_phone_verified'] ? $owner->getPhone()->getPhoneNumber(): null;
        $purchasesOutput['owner']['count_published_purchases'] = $paginator->totalItemCount;
        $purchasesOutput['urls']['user_purchases'] = UserOutput::getUserPurchasesUrl($owner);
        //обезопасил публичные данные
        unset($purchasesOutput['id']);

        return $purchasesOutput;
    }

    /**
     * @param null $purchase_slug
     * @param null $category_slug
     * @return Purchase|null
     */
    public function getPurchaseByCategory($purchase_slug = null, $category_slug = null)
    {
        if (!$purchase_slug || !$category_slug) {

            return null;
        }
        /** @var Category $category */
        $category = $this->entityManager->getRepository(Category::class)
            ->findOneBy(['slug' => $category_slug]);
        if (!$category ) {

            return null;
        }
        /** @var Purchase|null $purchase */
        $purchase = $this->entityManager->getRepository(Purchase::class)
            ->getPurchaseByCategory($purchase_slug, $category);

        return $purchase;
    }

    /**
     * @param User $user
     * @return Paginator
     */
    public function getPurchasesProductsByUser(User $user): Paginator
    {
        /** @var Paginator $purchases */
        $purchases = $this->entityManager->getRepository(Purchase::class)
            ->selectPublicPurchases($user);

        return $purchases;
    }

    /**
     * @param Purchase $purchase
     * @param SessionPlugin $sessionPlugin
     * @param bool $is_need_session_data
     * @return array
     */
    public function getPurchaseOutputWithSessionOrEntity(
        Purchase $purchase = null,
        SessionPlugin $sessionPlugin,
        $is_need_session_data = false
    ): array
    {
        if ($is_need_session_data) {

            return $sessionPlugin->getSessionVar(self::SESSION_PURCHASE_FLAG);
        }
        $sessionPlugin->clearSessionVar(self::SESSION_PURCHASE_FLAG);

        $productOutput = [];
        if ($purchase) {
            $categories = $purchase->getCategories();
            $subCategories = CategoryManager::filteredCollectionBySubcategory($categories);
            $category = null;
            if ($subCategories) {
                /** @var Category $category */
                $category = $subCategories->first();
            }
            $address = $purchase->getAddress();

            $productOutput['name'] = $purchase->getName();
            $productOutput['description'] = $purchase->getDescription();
            $productOutput['price'] = (int)$purchase->getPrice();
            $productOutput['quantity'] = $purchase->getQuantity();
            $productOutput['measure'] = $purchase->getMeasure();

            if ($category) {
                $productOutput['category'] = [
                    'id' => $category->getId(),
                    'name' => $category->getName(),
                    'parent_id' => $category->getParent()->getId(),
                    'parent_name' => $category->getParent()->getName()
                ];
            }

            if ($address) {
                $productOutput['address'] = [
                    'country' => $address->getCountry(),
                    'region' => $address->getRegion(),
                    'city' => $address->getCity(),
                    'street' => $address->getStreet(),
                    'house' => $address->getHouse(),
                    'apartment' => $address->getApartment(),
                    'post_code' => $address->getPostcode(),
                    'lat' => $address->getLat(),
                    'lng' => $address->getLng(),
                ];
            }
            /** @var ArrayCollection|PersistentCollection $files */
            $files = $purchase->getFiles();
            if ($files) {
                $productOutput['files'] = FileOutput::getMediumCollectionOutput($files, ['fileManager' => $this->fileManager]);
            }
        }

        return $productOutput;
    }

    /**
     * @param array $purchase_data
     * @return array
     */
    public function getFormDataWithPurchaseData(array $purchase_data = []): array
    {
        if (!empty($purchase_data)) {
            if (array_key_exists('category', $purchase_data) && \is_array($purchase_data['category'])) {
                $purchase_data['category'] = $purchase_data['category']['id'] ?? '';
            }
            if (array_key_exists('files', $purchase_data)) {
                $purchase_data['files'] = '';
            }
        }

        return $purchase_data;
    }

    /**
     * @param array|null $postFileData
     * @return array
     */
    public function getFilesPostData($postFileData):array
    {
        $result = [];
        if (\count($postFileData) && isset($postFileData['files'])) {
            if (isset($postFileData['files'])) {
                foreach ($postFileData['files'] as $files) {
                    $result['files'][] = $files;
                    if ($files['error'] === 4) {
                        $result['files'] = null;
                        break;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param array $postData
     * @param $filesData
     * @return array
     */
    public function prepareDataToPurchase(array $postData = [], $filesData): array
    {
        $data = [
            'post_data' => [
                'purchase' => null,
                'saved_files' => null,
            ],
            'validation' => [
                'is_valid' => true,
                'purchase' => [],
                'saved_files' => null
            ]
        ];

        if (array_key_exists('files', $postData) && \is_array($postData['files'])) {
            $saved_files = null;
            foreach ($postData['files'] as $file) {
                if (\is_array($file) && array_key_exists('id', $file)) {
                    $saved_files[] = $file;
                }
            }
            $data['post_data']['saved_files'] = $saved_files;
            unset($postData['files']);
        }
        if (!empty($filesData)) {
            $postData = array_merge_recursive($postData, $filesData);
        }
        $data['post_data']['purchase'] = $postData;

        return $data;
    }

    /**
     * @param $data
     * @param Form $form
     * @return mixed
     */
    public function getValidationPurchaseForm($data, Form $form)
    {
        //валидация формы
        $form->setData($data['post_data']['purchase']);
        if (! $form->isValid()) {
            $data['validation']['is_valid'] = false;
            $data['validation']['purchase'] = $form->getMessages();
        }

        return $data;
    }

    /**
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function getValidationSavedFiles(array $data): array
    {
        $saved_files = $data['post_data']['saved_files'];
        if ($saved_files && \is_array($saved_files)) {
            $validation_messages = null;
            foreach ($saved_files as $saved_file) {
                $file = $this->fileManager->selectFileById($saved_file['id'] ?? 0);
                if (!$file) {
                    $validation_messages['idNotFound'] = 'Can not find File in database by Id';
                    break;
                }
            }
            if ($validation_messages) {
                $data['validation']['is_valid'] = false;
                $data['validation']['saved_files'] = $validation_messages;
            }
        } else {
            $data['post_data']['saved_files'] = null;
        }

        return $data;
    }

    /**
     * @param SessionPlugin $sessionPlugin
     * @param array $preparedData
     * @return mixed
     * @throws \Exception
     */
    public function editFilesAndSaveDataInSession(SessionPlugin $sessionPlugin, array $preparedData)
    {
        $purchase_data = $preparedData['post_data']['purchase'];
        $saved_files = $preparedData['post_data']['saved_files'];
        unset($purchase_data['csrf']);
        // сохранение файла в бд
        if (isset($purchase_data['files']) && \is_array($purchase_data['files'])) {
            $purchase_data['files'] = $this->savePurchaseTemporaryFiles($purchase_data['files']);
        } else {
            $purchase_data['files'] = [];
        }

        //// *** edit files *** ////
        if ($saved_files && \is_array($saved_files)) {
            foreach ($saved_files as $saved_file) {
                $purchase_data['files'][] = $saved_file;
            }
        }
        //// *** end edit files *** ////

        $category = $this->categoryManager->getCategoryById($purchase_data['category']);
        if ($category) {
            $purchase_data['category'] = [
                'id' => $category->getId(),
                'name' => $category->getName(),
                'parent_id' => $category->getParent()->getId(),
                'parent_name' => $category->getParent()->getName()
            ];
        }

        //сохраняем результат в сессию для последующего использования или сохранения
        $sessionPlugin->addSessionVar($purchase_data, self::SESSION_PURCHASE_FLAG);

        return $purchase_data;
    }

    /**
     * @param Purchase $purchase
     * @param bool $archive
     * @return bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setArchive(Purchase $purchase, bool $archive)
    {
        $purchase->setRemoved($archive);

        $this->entityManager->flush();

        return true;
    }

    /**
     * @param Form $form
     * @param $session_data
     * @return Form
     */
    public function fillPurchaseFormWithSessionData(Form $form, $session_data): Form
    {
        if (!empty($session_data)) {
            $form->setData([
                'name' => $session_data['name'] ?? '',
                'description' => $session_data['description'] ?? '',
                'category' => isset($session_data['category']) && $session_data['category']['id'] ? $session_data['category']['id'] :'',
                'price' => $session_data['price'] ?? '',
                'quantity' => $session_data['quantity'] ?? '',
                'measure' => $session_data['measure'] ?? '',
                'safe_deal' => $session_data['safe_deal'] ?? '',
                'address' => $session_data['address'] ?? '',
                'csrf' => $session_data['csrf'] ?? '',
            ]);
        }

        return $form;
    }

    /**
     * @param string $search
     * @param string $catalog_root_url
     * @return array
     */
    public function purchaseSearchAutoComplete(string $search = '', string $catalog_root_url): array
    {
        $purchases = $this->entityManager->getRepository(Purchase::class)
            ->searchPublicPurchases($search);

        if (empty($purchases)) {
            return [];
        }

        $category_block = [];
        $sub_category_block = [];
        $product_block = [];
        /** @var Purchase $purchase */
        foreach ($purchases as $purchase){
            $purchaseCategories = $purchase->getCategories();
            $subCategories = CategoryManager::filteredCollectionBySubcategory($purchaseCategories);
            $categories = CategoryManager::filteredCollectionByCategory($purchaseCategories);

            $arr_phrase = ProductManager::phraseProcessing($search, $purchase->getName());

            if (!empty($arr_phrase)) {
                /** @var Category $category */
                foreach ($categories as $category) {
                    $category_name = $category->getName();
                    $category_block[$category_name] = [
                        'arr_phrase' => $arr_phrase,
                        'section' => $category_name,
                        'type' => 'section',
                        'url' => $catalog_root_url . '/' . $category->getSlug(),
                    ];
                }
                /** @var Category $subCategory */
                foreach ($subCategories as $subCategory) {
                    $sub_category_name = $subCategory->getName();
                    $sub_category_block[$sub_category_name] = [
                        'arr_phrase' => $arr_phrase,
                        'section' => $sub_category_name,
                        'type' => 'section',
                        'url' => $catalog_root_url . '/' . $subCategory->getParent()->getSlug() . '/' . $subCategory->getSlug(),
                    ];
                }

                $product_block[$arr_phrase['phrase'] ?? $purchase->getName()] = [
                    'arr_phrase' => $arr_phrase,
                    'type' => 'string',
                    'url' => $catalog_root_url,
                ];
            }
        }
        $output = [];
        $arrCategory = [];
        $max_category = 5;
        $max_product = 5;

        $count = 0;
        foreach ($sub_category_block as $value) {
            if ($count >= $max_category){
                break;
            }
            $arrCategory[] = $value;
            $count++;
        }
        foreach ($category_block as $value) {
            if ($count >= $max_category){
                break;
            }
            $arrCategory[] = $value;
            $count++;
        }
        krsort($arrCategory);

        foreach ($arrCategory as $value) {
            $output[] = $value;
        }
        $all_count = $max_category + $max_product;
        $count_product = 0;

        foreach ($product_block as $value) {
            if ($count_product >= $max_product || $count >= $all_count){
                break;
            }
            $output[] = $value;
            $count_product++;
            $count++;
        }

        return $output;
    }

    /**
     * @param Purchase $purchase
     * @throws \Exception
     */
    public function removePurchase(Purchase $purchase)
    {
        // 1 удаление связи с категорией
        /** @var ArrayCollection|PersistentCollection $categories */
        $categories = $purchase->getCategories();
        if ($categories) {
            $categories->clear();
        }
        // 2 удаление связи с городами
        /** @var ArrayCollection|PersistentCollection $cities */
        $cities = $purchase->getCities();
        if ($cities) {
            $cities->clear();
        }
        // 3 удалить запись в таблице адрес
        /** @var Address $address */
        $address = $purchase->getAddress();
        if ($address) {
            $purchase->setAddress(Null);
            $this->entityManager->remove($address);
        }
        // 5 удалием связи файлов
        $files = $purchase->getFiles();
        $clone_files = [];
        if ($files) {
            $clone_files = clone $files;
            $files->clear();
        }
        if (!empty($clone_files)) {
            // 6 удаляем файлы из базы и с диска
            $this->entityManager->persist($purchase);
            $this->entityManager->flush($purchase);
            /** @var File $file */
            foreach ($clone_files as $file){
                $this->fileManager->delete($file->getId());
            }
        }
        // 6 удалить связь и сущности модерации
        /** @var ArrayCollection|PersistentCollection $moderations */
        $moderations = $purchase->getModerations();
        if (!empty($moderations)) {
            $clone_moderations = clone $moderations;
            $moderations->clear();

            /** @var Moderation $moderation */
            foreach ($clone_moderations as $moderation) {
                $this->entityManager->remove($moderation);
            }
            $this->entityManager->flush();
        }
        // 7 удалить связь и сущности prolongation
        /** @var ArrayCollection|PersistentCollection $prolongations */
        $prolongations = $purchase->getPurchaseProlongations();
        if (!empty($prolongations)) {
            $clone_prolongations = clone $prolongations;
            $prolongations->clear();

            /** @var PurchaseProlongation $prolongation */
            foreach ($clone_prolongations as $prolongation) {
                $this->entityManager->remove($prolongation);
            }
            $this->entityManager->flush();
        }
        // 8 удалить Purchase
        $this->entityManager->remove($purchase);
        $this->entityManager->flush();
    }
}