<?php
namespace Application\Service;

use Application\Entity\Address;
use Application\Entity\Product;
use Doctrine\ORM\EntityManager;


class AddressManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;


    /**
     * AddressManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $data
     * @return Address
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createAddress($data): Address
    {
        $address = new Address();
        #$address->setProduct($advert);
        $address->setCountry($data['country']);
        $address->setRegion($data['region']);
        $address->setCity($data['city']);
        $address->setStreet($data['street']);
        $address->setHouse($data['house']);
        $address->setApartment($data['apartment']);
        $address->setPostcode(null);
        $address->setLat($data['lat']);
        $address->setLng($data['lng']);

        $this->entityManager->persist($address);
        $this->entityManager->flush($address);

        return $address;
    }

    /**
     * @param Address $address
     * @param $data
     * @return Address
     * @throws \Exception
     */
    public function editAddress(Address $address, $data): Address
    {
        $address->setCountry($data['country']);
        $address->setRegion($data['region']);
        $address->setCity($data['city']);
        $address->setStreet($data['street']);
        $address->setHouse($data['house']);
        $address->setApartment($data['apartment']);
        $address->setPostcode(null);
        $address->setLat($data['lat']);
        $address->setLng($data['lng']);

        $this->entityManager->persist($address);
        $this->entityManager->flush($address);

        return $address;
    }
}