<?php
namespace Application\Service;

use Application\Entity\Moderation;
use Doctrine\ORM\EntityManager;

/**
 * Class PurchaseModerationManager
 * @package Application\Service
 */
class PurchaseModerationManager
{
    use PaginatorOutputTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * PurchaseModerationManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * @param Moderation $moderation
     * @param $formData
     * @return Moderation
     * @throws \Doctrine\ORM\ORMException
     * @throws \Exception
     */
    public function saveFormData(Moderation $moderation, $formData)
    {
        $moderation->setApproved($formData['approved']);
        $moderation->setResolutionDate(new \DateTime());
        if (isset($formData['description'])) {
            $moderation->setDescription($formData['description']);
        }

        $this->entityManager->flush();

        return $moderation;
    }
}