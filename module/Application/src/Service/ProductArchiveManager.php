<?php
namespace Application\Service;

use Application\Entity\Product;
use Application\Entity\User;
use Doctrine\ORM\EntityManager;

/**
 * Class ProductArchiveManager
 * @package Application\Service
 */
class ProductArchiveManager
{
    use PaginatorOutputTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * ProductModerationManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return mixed
     */
    public function getArchiveProducts(User $user = null, int $page, int $per_page = null)
    {
        return $this->entityManager->getRepository(Product::class)
            ->selectArchiveProducts($user, $page, $per_page);
    }

    /**
     * @param Product $product
     * @return bool
     * @throws \Exception
     */
    public function sendProductToArchive(Product $product): bool
    {
        $product->setRemoved(1);

        $this->entityManager->flush();

        return true;
    }

    /**
     * @param Product $product
     * @return bool
     * @throws \Exception
     */
    public function returnProductFromArchive(Product $product): bool
    {
        $product->setRemoved(0);

        $this->entityManager->flush();

        return true;
    }
}