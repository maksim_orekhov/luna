<?php
namespace Application\Service;

/**
 * Class ProfileManager
 * @package Application\Service
 */
class ProfileManager
{
    /**
     * @param array $data
     * @return array
     */
    public function formProductsCollections(array $data): array
    {
        $products_collections = [];

        if (isset($data['public']['products'])) {
            $products_collections['public'] = [];
            $products_collections['public']['products'] = $data['public']['products'];
            $products_collections['public']['productsPaginator'] = $data['public']['paginator'];
        }
        if (isset($data['waiting_moderation']['products'])) {
            $products_collections['waiting_moderation'] = [];
            $products_collections['waiting_moderation']['products'] = $data['waiting_moderation']['products'];
            $products_collections['waiting_moderation']['productsPaginator'] = $data['waiting_moderation']['paginator'];
        }
        if (isset($data['declined']['products'])) {
            $products_collections['declined'] = [];
            $products_collections['declined']['products'] = $data['declined']['products'];
            $products_collections['declined']['productsPaginator'] = $data['declined']['paginator'];
        }
        if (isset($data['inactive']['products'])) {
            $products_collections['inactive'] = [];
            $products_collections['inactive']['products'] = $data['inactive']['products'];
            $products_collections['inactive']['productsPaginator'] = $data['inactive']['paginator'];
        }
        if (isset($data['archival']['products'])) {
            $products_collections['archival'] = [];
            $products_collections['archival']['products'] = $data['archival']['products'];
            $products_collections['archival']['productsPaginator'] = $data['archival']['paginator'];
        }

        return $products_collections;
    }

    /**
     * @param array $data
     * @return array
     */
    public function formPurchasesCollections(array $data): array
    {
        $purchases_collections = [];

        if (isset($data['public']['purchases'])) {
            $purchases_collections['public'] = [];
            $purchases_collections['public']['purchases'] = $data['public']['purchases'];
            $purchases_collections['public']['collection_url'] = isset($data['purchases_collection_urls']['public']) ? $data['purchases_collection_urls']['public'] : null;
            $purchases_collections['public']['purchasesPaginator'] = $data['public']['paginator'];
            // @TODO Убрать!
            foreach ($purchases_collections['public']['purchases'] as &$public_purchase) {
                if (isset($data['purchases_collection_urls']['archive'])) {
                    $public_purchase['urls']['archive_url'] = $data['purchases_collection_urls']['archive'] . '/' . $public_purchase['id'];
                }
            }
        }
        if (isset($data['waiting_moderation']['purchases'])) {
            $purchases_collections['waiting_moderation'] = [];
            $purchases_collections['waiting_moderation']['purchases'] = $data['waiting_moderation']['purchases'];
            $purchases_collections['waiting_moderation']['purchasesPaginator'] = $data['waiting_moderation']['paginator'];
        }
        if (isset($data['declined']['purchases'])) {
            $purchases_collections['declined'] = [];
            $purchases_collections['declined']['purchases'] = $data['declined']['purchases'];
            $purchases_collections['declined']['purchasesPaginator'] = $data['declined']['paginator'];
        }
        if (isset($data['inactive']['purchases'])) {
            $purchases_collections['inactive'] = [];
            $purchases_collections['inactive']['purchases'] = $data['inactive']['purchases'];
            $purchases_collections['inactive']['collection_url'] = isset($data['purchases_collection_urls']['inactive']) ? $data['purchases_collection_urls']['inactive'] : null;
            $purchases_collections['inactive']['purchasesPaginator'] = $data['inactive']['paginator'];
            // @TODO Убрать!
            foreach ($purchases_collections['inactive']['purchases'] as &$inactive_purchase) {
                if (isset($data['purchases_collection_urls']['archive'])) {
                    $inactive_purchase['urls']['archive_url'] = $data['purchases_collection_urls']['archive'] . '/' . $inactive_purchase['id'];
                }
            }
        }
        if (isset($data['archival']['purchases'])) {
            $purchases_collections['archival'] = [];
            $purchases_collections['archival']['purchases'] = $data['archival']['purchases'];
            $purchases_collections['archival']['collection_url'] = isset($data['purchases_collection_urls']['archive']) ? $data['purchases_collection_urls']['archive'] : null;
            $purchases_collections['archival']['purchasesPaginator'] = $data['archival']['paginator'];
            // @TODO Убрать!
            foreach ($purchases_collections['archival']['purchases'] as &$archival_purchase) {
                if (isset($data['purchases_collection_urls']['archive'])) {
                    $archival_purchase['urls']['archive_url'] = $data['purchases_collection_urls']['archive'] . '/' . $archival_purchase['id'];
                }
            }
        }

        return $purchases_collections;
    }
}