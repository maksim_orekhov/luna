<?php
namespace Application\Service;

use Application\Entity\City;
use Application\Entity\Interfaces\ProductInterface;
use Application\Entity\Product;
use Application\Entity\Category;
use Application\Entity\Purchase;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;

/**
 * Class CategoryManager
 * @package Application\Service
 */
class CategoryManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var array
     */
    private $categories;

    /**
     * UserManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $parent_id
     * @return array
     */
    public function getCategories($parent_id = null):array
    {
        if ( $this->categories ) {
            return $this->categories;
        }
        /** @var array $categories */
        $this->categories = $this->entityManager->getRepository(Category::class)
            ->findBy(['active' => true, 'parent' => $parent_id ? (int) $parent_id: null]);

        return $this->categories;
    }

    /**
     * @return array
     */
    public function getSubCategories():array
    {
        /** @var array $categories */
        $this->categories = $this->entityManager->getRepository(Category::class)
            ->getSubCategories();

        return $this->categories;
    }

    /**
     * @param $id_category
     * @return Category|null
     */
    public function getCategoryById($id_category)
    {
        /** @var Category|null $category */
        $category = $this->entityManager->getRepository(Category::class)
            ->findOneBy(['active' => true, 'id'=> (int) $id_category]);

        return $category;
    }

    /**
     * @param $slug
     * @return Category|null
     */
    public function getCategoryBySlug($slug)
    {
        /** @var Category|null $category */
        $category = $this->entityManager->getRepository(Category::class)
            ->findOneBy(['active' => true, 'slug'=> $slug]);

        return $category;
    }

    /**
     * @param array|ArrayCollection|PersistentCollection $categories
     * @param bool $sub_category
     * @return array|null
     */
    public function getCategoriesOutput($categories, $sub_category = false)
    {
        $categoriesOutput = [];
        /** @var Category $category */
        foreach ($categories as $category){
            $categoriesOutput[] = $this->getCategoryOutput($category, $sub_category);
        }

        return !empty($categoriesOutput) ? $categoriesOutput : null ;
    }

    /**
     * @param Category $category
     * @param bool $sub_category
     * @return array
     */
    public function getCategoryOutput(Category $category, $sub_category = false): array
    {
        $categoryOutput = [];
        $categoryOutput['id'] = $category->getId();
        $categoryOutput['name'] = $category->getName();
        $categoryOutput['slug'] = $category->getSlug();
        /** @var Category $parentCategory */
        $parentCategory = $category->getParent();
        if ($parentCategory) {
            $categoryOutput['url'] = '/'.$parentCategory->getSlug().'/'.$category->getSlug();
        } else {
            $categoryOutput['url'] = '/'.$category->getSlug();
        }

        $subCategories = $category->getChildren();
        if ($sub_category) {
            $categoryOutput['sub_category'] = null;
            if ($subCategories) {
                $categoryOutput['sub_category'] = $this->getCategoriesOutput($subCategories);
            }
        }

        return $categoryOutput;
    }

    /**
     * @param Product $product
     * @return null|string
     */
    public function getCategoryUrlByProduct(Product $product)
    {
        $subCategories = self::filteredCollectionBySubcategory($product->getCategories());

        /** @var Category $subCategory */
        $subCategory = $subCategories->first();
        if ($subCategory) {
            /** @var Category $category */
            $category = $subCategory->getParent();
            if ($category) {

                return '/'.$category->getSlug().'/'.$subCategory->getSlug().'/'.$product->getSlug();
            }
        }

        return null;
    }

    /**
     * @param Category|null $category
     * @return string
     */
    public static function getCategoryUrl(Category $category = null): string
    {
        if ($category instanceof Category && $category->getParent()) {

            return '/'.$category->getParent()->getSlug().'/'.$category->getSlug();
        }

        return $category instanceof Category ? '/'.$category->getSlug() : '';
    }

    /**
     * @param ArrayCollection|PersistentCollection $collections
     * @return mixed
     */
    public static function filteredCollectionBySubcategory($collections)
    {
        return $collections->filter(function(Category $entity){
            return $entity->getParent() !== null;
        });
    }

    /**
     * @param ArrayCollection|PersistentCollection $collections
     * @return mixed
     */
    public static function filteredCollectionByCategory($collections)
    {
        return $collections->filter(function(Category $entity){
            return $entity->getParent() === null;
        });
    }
}