<?php
namespace Application\Service\Factory;

use Application\Service\CityManager;
use Application\Service\ProductManager;
use Application\Service\CategoryManager;
use Application\Service\RouteManager;
use Interop\Container\ContainerInterface;
use Zend\Router\RouteStackInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class ProductRouteManagerFactory
 * @package Application\Service\Factory
 */
class RouteManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ProductManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $categoryManager = $container->get(CategoryManager::class);
        $cityManager = $container->get(CityManager::class);
        $cache = $container->get('FilesystemCache');
        $config = $container->get('config');

        return new RouteManager(
            $entityManager,
            $categoryManager,
            $cityManager,
            $cache,
            $config
        );
    }
}