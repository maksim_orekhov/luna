<?php
namespace Application\Service\Factory;

use Application\Service\ProductModerationManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class ProductModerationManagerFactory
 * @package Application\Service\Factory
 */
class ProductModerationManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ProductModerationManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new ProductModerationManager(
            $entityManager
        );
    }
}