<?php
namespace Application\Service\Factory;

use Application\Service\CategoryManager;
use Application\Service\FabricatorManager;
use Application\Service\ModerationManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class FabricatorManagerFactory
 * @package Application\Service\Factory
 */
class FabricatorManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return FabricatorManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $categoryManager = $container->get(CategoryManager::class);
        $moderationManager = $container->get(ModerationManager::class);

        return new FabricatorManager(
            $entityManager,
            $categoryManager,
            $moderationManager
        );
    }
}