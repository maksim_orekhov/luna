<?php
namespace Application\Service\Factory;

use Application\Service\CategoryManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class CategoryManagerFactory
 * @package Application\Service\Factory
 */
class CategoryManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return CategoryManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new CategoryManager(
            $entityManager
        );
    }
}