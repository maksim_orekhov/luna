<?php
namespace Application\Service\Factory;

use Application\Service\PurchaseModerationManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class PurchaseModerationManagerFactory
 * @package Application\Service\Factory
 */
class PurchaseModerationManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return PurchaseModerationManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new PurchaseModerationManager(
            $entityManager
        );
    }
}