<?php
namespace Application\Service\Factory;

use Application\Service\Output\ProductOutput;
use Application\Service\ProductArchiveManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class ProductArchiveManagerFactory
 * @package Application\Service\Factory
 */
class ProductArchiveManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ProductArchiveManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $productOutput = $container->get(ProductOutput::class);

        return new ProductArchiveManager(
            $entityManager,
            $productOutput
        );
    }
}