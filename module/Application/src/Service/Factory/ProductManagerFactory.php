<?php
namespace Application\Service\Factory;

use Application\Service\AddressManager;
use Application\Service\Output\ProductOutput;
use Application\Service\ProductArchiveManager;
use Application\Service\ProductManager;
use Application\Service\CategoryManager;
use Application\Service\CityManager;
use Application\Service\ModerationManager;
use Application\Service\ProductModerationManager;
use Application\Service\Status\ProductStatus;
use Interop\Container\ContainerInterface;
use ModuleFileManager\Service\FileManager;
use ModuleImageResize\Service\ImageManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\View\Helper\Url;

/**
 * Class AdvertManagerFactory
 * @package Application\Service\Factory
 */
class ProductManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ProductManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $fileManager = $container->get(FileManager::class);
        $moderationManager = $container->get(ModerationManager::class);
        $addressManager = $container->get(AddressManager::class);
        $cityManager = $container->get(CityManager::class);
        $categoryManager = $container->get(CategoryManager::class);
        $productStatus = $container->get(ProductStatus::class);
        $imageManager = $container->get(ImageManager::class);
        $productModerationManager = $container->get(ProductModerationManager::class);
        $productArchiveManager = $container->get(ProductArchiveManager::class);

        return new ProductManager(
            $entityManager,
            $fileManager,
            $moderationManager,
            $addressManager,
            $cityManager,
            $categoryManager,
            $productStatus,
            $imageManager,
            $productModerationManager,
            $productArchiveManager
        );
    }
}