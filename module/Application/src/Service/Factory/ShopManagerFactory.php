<?php
namespace Application\Service\Factory;

use Application\Service\ModerationManager;
use Application\Service\ShopManager;
use Interop\Container\ContainerInterface;
use ModuleFileManager\Service\FileManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class ShopManagerFactory
 * @package Application\Service\Factory
 */
class ShopManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ShopManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $moderationManager = $container->get(ModerationManager::class);
        $fileManager = $container->get(FileManager::class);

        return new ShopManager(
            $entityManager,
            $moderationManager,
            $fileManager
        );
    }
}