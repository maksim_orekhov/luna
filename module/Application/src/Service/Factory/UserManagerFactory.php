<?php
namespace Application\Service\Factory;

use Application\Service\UserManager;
use Core\Service\Base\BaseEmailManager;
use Interop\Container\ContainerInterface;
use ModuleFileManager\Service\FileManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class UserManagerFactory
 * @package Application\Service\Factory
 */
class UserManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return UserManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $baseEmailManager = $container->get(BaseEmailManager::class);
        $fileManager = $container->get(FileManager::class);

        return new UserManager(
            $entityManager,
            $baseEmailManager,
            $fileManager
        );
    }
}