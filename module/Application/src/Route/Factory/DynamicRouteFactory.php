<?php
namespace Application\Route\Factory;

use Application\Route\DynamicRoute;
use Application\Service\AddressManager;
use Interop\Container\ContainerInterface;
use Traversable;
use Zend\Router\Exception;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\Stdlib\ArrayUtils;

/**
 * Class DynamicRouteFactory
 * @package Application\Route\Factory
 */
class DynamicRouteFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $routeName
     * @param array|null $options
     * @return AddressManager|object
     */
    public function __invoke(ContainerInterface $container, $routeName, array $options = null)
    {
        $options = $options ?: [];
        /** @var DynamicRoute $dynamicRoute */
        $dynamicRoute = $routeName;
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        if ($options instanceof Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        } elseif (! \is_array($options)) {
            throw new Exception\InvalidArgumentException(sprintf(
                '%s expects an array or Traversable set of options',
                __METHOD__
            ));
        }

        if (! isset($options['route'])) {
            throw new Exception\InvalidArgumentException('Missing "route" in options array');
        }

        if (! isset($options['constraints'])) {
            throw new Exception\InvalidArgumentException('Missing "constraints" in options array');
        }

        if (! isset($options['database'])) {
            throw new Exception\InvalidArgumentException('Missing "database" in options array');
        }

        if (! isset($options['defaults'])) {
            $options['defaults'] = [];
        }

        return new $dynamicRoute($entityManager, $options['route'], $options['constraints'], $options['defaults'], $options['database']);
    }
}