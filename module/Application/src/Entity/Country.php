<?php
namespace Application\Entity;

use Core\Entity\AbstractCountry;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Country
 * @package Application\Entity
 *
 * @ORM\Table(name="country")
 * @ORM\Entity
 */
class Country extends AbstractCountry
{
}

