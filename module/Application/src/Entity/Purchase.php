<?php
namespace Application\Entity;

use Application\Entity\Interfaces\ProductInterface;
use Application\Entity\Interfaces\ModeratedInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use ModuleFileManager\Entity\File;

/**
 * Class Product
 * @package Application\Entity
 *
 * @ORM\Table(name="purchase", indexes={@ORM\Index(name="purchase_slug", columns={"slug"}), @ORM\Index(name="purchase_created", columns={"created"}), @ORM\Index(name="purchase_visible", columns={"visible"})})
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\PurchaseRepository");
 */
class Purchase implements ModeratedInterface, ProductInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=64, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="publication_period", type="smallint", nullable=false)
     */
    private $publicationPeriod;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @var string
     *
     * @ORM\Column(name="measure", type="string", length=125, nullable=true)
     */
    private $measure;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="changed", type="datetime", nullable=false)
     */
    private $changed;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadline", type="datetime", nullable=true)
     */
    private $deadline;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean", nullable=true)
     */
    private $visible;

    /**
     * @var boolean
     *
     * @ORM\Column(name="removed", type="boolean", nullable=true)
     */
    private $removed;

    /**
     * @var \Application\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\User", inversedBy="purchases")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * One Purchase has Many Files.
     * @ORM\ManyToMany(targetEntity="ModuleFileManager\Entity\File")
     * @ORM\JoinTable(name="purchases_files",
     *      joinColumns={@ORM\JoinColumn(name="purchase_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $files;

    /**
     * Many Purchases have Many Categories.
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="purchases")
     * @ORM\JoinTable(name="purchases_categories")
     */
    private $categories;

    /**
     * Many Purchases have Many Cities.
     * @ORM\ManyToMany(targetEntity="City", inversedBy="purchases")
     * @ORM\JoinTable(name="purchase_cities")
     */
    private $cities;

    /**
     * Many Purchases has Many Moderations.
     * @ORM\ManyToMany(targetEntity="Application\Entity\Moderation")
     * @ORM\JoinTable(name="purchases_moderations",
     *      joinColumns={@ORM\JoinColumn(name="purchase_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="moderation_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $moderations;

    /**
     * @ORM\OneToMany(targetEntity="PurchaseProlongation", mappedBy="purchase")
     */
    private $purchaseProlongations;

    /**
     * @var Address
     *
     * One Purchase has One Address.
     * @ORM\OneToOne(targetEntity="Address", inversedBy="purchase")
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id")
     */
    private $address;

    /**
     * Product constructor.
     */
    public function __construct()
    {
        $this->categories   = new ArrayCollection();
        $this->files        = new ArrayCollection();
        $this->cities       = new ArrayCollection();
        $this->moderations  = new ArrayCollection();
        $this->purchaseProlongations = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string|null $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string|null $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getPublicationPeriod(): int
    {
        return $this->publicationPeriod;
    }

    /**
     * @param int $publicationPeriod
     */
    public function setPublicationPeriod(int $publicationPeriod)
    {
        $this->publicationPeriod = $publicationPeriod;
    }

    /**
     * @return float|null
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float|null $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return int|null
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int|null $quantity
     */
    public function setQuantity(int $quantity = null)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return string|null
     */
    public function getMeasure()
    {
        return $this->measure;
    }

    /**
     * @param string|null $measure
     */
    public function setMeasure(string $measure = null)
    {
        $this->measure = $measure;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getChanged(): \DateTime
    {
        return $this->changed;
    }

    /**
     * @param \DateTime $changed
     */
    public function setChanged(\DateTime $changed)
    {
        $this->changed = $changed;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * @param \DateTime|null $deadline
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;
    }

    /**
     * @return bool
     */
    public function isVisible(): bool
    {
        return $this->visible;
    }

    /**
     * @param bool $visible
     */
    public function setVisible(bool $visible)
    {
        $this->visible = $visible;
    }

    /**
     * @return bool
     */
    public function isRemoved(): bool
    {
        return $this->removed;
    }

    /**
     * @param bool $removed
     */
    public function setRemoved(bool $removed)
    {
        $this->removed = $removed;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param mixed $files
     */
    public function setFiles($files)
    {
        $this->files = $files;
    }

    /**
     * @param File $file
     */
    public function addFile(File $file)
    {
        $this->files->add($file);
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param mixed $categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    /**
     * @param Category $category
     */
    public function addCategory(Category $category)
    {
        $this->categories->add($category);
    }

    /**
     * @inheritdoc
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @inheritdoc
     */
    public function setCities($cities)
    {
        $this->cities = $cities;
    }

    /**
     * @inheritdoc
     */
    public function addCity(City $city)
    {
        $this->cities->add($city);
    }

    /**
     * @return mixed
     */
    public function getPurchaseProlongations()
    {
        return $this->purchaseProlongations;
    }

    /**
     * @param PurchaseProlongation $purchaseProlongation
     */
    public function addPurchaseProlongation(PurchaseProlongation $purchaseProlongation)
    {
        $this->purchaseProlongations->add($purchaseProlongation);
    }

    /**
     * @inheritdoc
     */
    public function getAddress(): Address
    {
        return $this->address;
    }

    /**
     * @inheritdoc
     */
    public function setAddress(Address $address = null)
    {
        $this->address = $address;
    }

    /**
     * @inheritdoc
     */
    public function getModerations()
    {
        return $this->moderations;
    }

    /**
     * @inheritdoc
     */
    public function setModerations($moderations)
    {
        $this->moderations = $moderations;
    }

    /**
     * @inheritdoc
     */
    public function addModeration(Moderation $moderation)
    {
        $this->moderations->add($moderation);
    }
}