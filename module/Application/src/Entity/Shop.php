<?php
namespace Application\Entity;

use Application\Entity\Interfaces\ModeratedInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use ModuleFileManager\Entity\File;

/**
 * Class Shop
 * @package Application\Entity
 *
 * @ORM\Table(name="shop")
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\ShopRepository");
 */
class Shop implements ModeratedInterface
{
    const PATH_LOGO_FILES = '/shop-logos';
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="legal_name", type="string", length=255, nullable=false)
     */
    private $legalName;

    /**
     * @var string
     *
     * @ORM\Column(name="inn", type="string", length=12, nullable=false)
     */
    private $inn;

    /**
     * @var string
     *
     * @ORM\Column(name="ogrn", type="string", length=15, nullable=true)
     */
    private $ogrn;

    /**
     * @var string
     *
     * @ORM\Column(name="postal_address", type="string", length=255, nullable=false)
     */
    private $postalAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="legal_address", type="string", length=255, nullable=false)
     */
    private $legalAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @var boolean
     *
     * @ORM\Column(name="unique_design", type="boolean", nullable=true)
     */
    private $uniqueDesign;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var @ORM\Column(type="array", name="phone_numbers")
     */
    private $phoneNumbers;

    /**
     * @var @ORM\Column(type="array", name="web_links")
     */
    private $webLinks;

    /**
     * @var @ORM\Column(type="array", name="work_schedule")
     */
    private $workSchedule;

    /**
     * One Shop has One User.
     * @ORM\OneToOne(targetEntity="Application\Entity\User", inversedBy="shop")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var File
     *
     * One Shop has One File (logo).
     * @ORM\OneToOne(targetEntity="ModuleFileManager\Entity\File")
     * @ORM\JoinColumn(name="logo_file_id", referencedColumnName="id")
     */
    private $logo;

    /**
     * Many Shops has Many Moderations.
     * @ORM\ManyToMany(targetEntity="Application\Entity\Moderation")
     * @ORM\JoinTable(name="shops_moderations",
     *      joinColumns={@ORM\JoinColumn(name="shop_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="moderation_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $moderations;


    /**
     * Fabricator constructor.
     */
    public function __construct()
    {
        $this->moderations  = new ArrayCollection();
        $this->uniqueDesign = false;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLegalName(): string
    {
        return $this->legalName;
    }

    /**
     * @param string $legalName
     */
    public function setLegalName(string $legalName)
    {
        $this->legalName = $legalName;
    }

    /**
     * @return string
     */
    public function getInn(): string
    {
        return $this->inn;
    }

    /**
     * @param string $inn
     */
    public function setInn(string $inn)
    {
        $this->inn = $inn;
    }

    /**
     * @return string|null
     */
    public function getOgrn()
    {
        return $this->ogrn;
    }

    /**
     * @param string|null $ogrn
     */
    public function setOgrn($ogrn)
    {
        $this->ogrn = $ogrn;
    }

    /**
     * @return string
     */
    public function getPostalAddress(): string
    {
        return $this->postalAddress;
    }

    /**
     * @param string $postalAddress
     */
    public function setPostalAddress(string $postalAddress)
    {
        $this->postalAddress = $postalAddress;
    }

    /**
     * @return string
     */
    public function getLegalAddress(): string
    {
        return $this->legalAddress;
    }

    /**
     * @param string $legalAddress
     */
    public function setLegalAddress(string $legalAddress)
    {
        $this->legalAddress = $legalAddress;
    }

    /**
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(string $description = null)
    {
        $this->description = $description;
    }

    /**
     * @return string|null
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string|null $slug
     */
    public function setSlug(string $slug = null)
    {
        $this->slug = $slug;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumbers()
    {
        return $this->phoneNumbers ? unserialize($this->phoneNumbers, array('allowed_classes' => false)) : [];
    }

    /**
     * @param mixed $phoneNumbers
     */
    public function setPhoneNumbers(array $phoneNumbers = [])
    {
        $this->phoneNumbers = serialize($phoneNumbers);
    }

    /**
     * @return mixed
     */
    public function getWebLinks()
    {
        return $this->webLinks ? unserialize($this->webLinks, array('allowed_classes' => false)) : [];
    }

    /**
     * @param mixed $webLinks
     */
    public function setWebLinks(array $webLinks = [])
    {

        $this->webLinks = serialize($webLinks);
    }

    /**
     * @return mixed
     */
    public function getWorkSchedule()
    {
        return $this->workSchedule ? unserialize($this->workSchedule, array('allowed_classes' => false)) : [];
    }

    /**
     * @param mixed $workSchedule
     */
    public function setWorkSchedule(array $workSchedule = [])
    {
        $this->workSchedule = serialize($workSchedule);
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param mixed $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * @return bool
     */
    public function isUniqueDesign(): bool
    {
        return $this->uniqueDesign;
    }

    /**
     * @param bool $uniqueDesign
     */
    public function setUniqueDesign(bool $uniqueDesign)
    {
        $this->uniqueDesign = $uniqueDesign;
    }

    /**
     * @inheritdoc
     */
    public function getModerations()
    {
        return $this->moderations;
    }

    /**
     * @inheritdoc
     */
    public function setModerations($moderations)
    {
        $this->moderations = $moderations;
    }

    /**
     * @inheritdoc
     */
    public function addModeration(Moderation $moderation)
    {
        $this->moderations->add($moderation);
    }
}

