<?php
namespace Application\Entity;

use Application\Entity\Interfaces\ProductInterface;
use Application\Entity\Interfaces\ModeratedInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use ModuleFileManager\Entity\File;

/**
 * Class Product
 * @package Application\Entity
 *
 * @ORM\Table(name="product", indexes={@ORM\Index(name="product_slug", columns={"slug"}), @ORM\Index(name="product_created", columns={"created"}), @ORM\Index(name="product_visible", columns={"visible"})})
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\ProductRepository");
 */
class Product implements ModeratedInterface, ProductInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=64, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="publication_period", type="smallint", nullable=false)
     */
    private $publicationPeriod;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $price;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="changed", type="datetime", nullable=false)
     */
    private $changed;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadline", type="datetime", nullable=true)
     */
    private $deadline;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean", nullable=true)
     */
    private $visible;

    /**
     * @var boolean
     *
     * @ORM\Column(name="removed", type="boolean", nullable=true)
     */
    private $removed;

    /**
     * @var \Application\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\User", inversedBy="products")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * One Product has Many Files.
     * @ORM\ManyToMany(targetEntity="ModuleFileManager\Entity\File")
     * @ORM\JoinTable(name="products_files",
     *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $files;

    /**
     * One Product has Many Files.
     * @ORM\ManyToMany(targetEntity="ModuleFileManager\Entity\File")
     * @ORM\JoinTable(name="products_galleries",
     *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $gallery;

    /**
     * Many Products have Many Categories.
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="products")
     * @ORM\JoinTable(name="products_categories")
     */
    private $categories;

    /**
     * Many Products have Many Cities.
     * @ORM\ManyToMany(targetEntity="City", inversedBy="products")
     * @ORM\JoinTable(name="products_cities")
     */
    private $cities;

    /**
     * Many Products has Many Moderations.
     * @ORM\ManyToMany(targetEntity="Application\Entity\Moderation")
     * @ORM\JoinTable(name="products_moderations",
     *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="moderation_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $moderations;

    /**
     * @ORM\OneToMany(targetEntity="ProductProlongation", mappedBy="product")
     */
    private $productProlongations;

    /**
     * @var Address
     *
     * One Product has One Address.
     * @ORM\OneToOne(targetEntity="Address", inversedBy="product")
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id")
     */
    private $address;

    /**
     * Product constructor.
     */
    public function __construct()
    {
        $this->categories   = new ArrayCollection();
        $this->files        = new ArrayCollection();
        $this->gallery      = new ArrayCollection();
        $this->cities       = new ArrayCollection();
        $this->moderations  = new ArrayCollection();
        $this->productProlongations = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string|null $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string|null $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getPublicationPeriod(): int
    {
        return $this->publicationPeriod;
    }

    /**
     * @param int $publicationPeriod
     */
    public function setPublicationPeriod(int $publicationPeriod)
    {
        $this->publicationPeriod = $publicationPeriod;
    }

    /**
     * @return float|null
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float|null $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getChanged(): \DateTime
    {
        return $this->changed;
    }

    /**
     * @param \DateTime $changed
     */
    public function setChanged(\DateTime $changed)
    {
        $this->changed = $changed;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * @param \DateTime|null $deadline
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;
    }

    /**
     * @return bool
     */
    public function isVisible(): bool
    {
        return $this->visible;
    }

    /**
     * @param bool $visible
     */
    public function setVisible(bool $visible)
    {
        $this->visible = $visible;
    }

    /**
     * @return bool
     */
    public function isRemoved(): bool
    {
        return $this->removed;
    }

    /**
     * @param bool $removed
     */
    public function setRemoved(bool $removed)
    {
        $this->removed = $removed;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param mixed $files
     */
    public function setFiles($files)
    {
        $this->files = $files;
    }

    /**
     * @param File $file
     */
    public function addFile(File $file)
    {
        $this->files->add($file);
    }

    /**
     * @return mixed
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * @param mixed $gallery
     */
    public function setGallery($gallery)
    {
        $this->gallery = $gallery;
    }

    /**
     * @param File $gallery
     */
    public function addGallery(File $gallery)
    {
        $this->gallery->add($gallery);
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param mixed $categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    /**
     * @param Category $category
     */
    public function addCategory(Category $category)
    {
        $this->categories->add($category);
    }

    /**
     * @return mixed
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @param mixed $cities
     */
    public function setCities($cities)
    {
        $this->cities = $cities;
    }

    /**
     * @param City $city
     */
    public function addCity(City $city)
    {
        $this->cities->add($city);
    }

    /**
     * @return mixed
     */
    public function getProductProlongations()
    {
        return $this->productProlongations;
    }

    /**
     * @param ProductProlongation $productProlongation
     */
    public function addProductProlongation(ProductProlongation $productProlongation)
    {
        $this->productProlongations->add($productProlongation);
    }

    /**
     * @return Address
     */
    public function getAddress(): Address
    {
        return $this->address;
    }

    /**
     * @param Address|null $address
     */
    public function setAddress(Address $address = null)
    {
        $this->address = $address;
    }

    /**
     * @inheritdoc
     */
    public function getModerations()
    {
        return $this->moderations;
    }

    /**
     * @inheritdoc
     */
    public function setModerations($moderations)
    {
        $this->moderations = $moderations;
    }

    /**
     * @inheritdoc
     */
    public function addModeration(Moderation $moderation)
    {
        $this->moderations->add($moderation);
    }
}

