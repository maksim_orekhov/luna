<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Moderation
 * @package Application\Entity
 *
 * @ORM\Table(name="moderation", indexes={@ORM\Index(name="moderation_created", columns={"created"})})
 * @ORM\Entity();
 */
class Moderation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="resolution_date", type="datetime", nullable=true)
     */
    private $resolution_date;

    /**
     * @var boolean
     *
     * @ORM\Column(name="approved", type="boolean", nullable=true)
     */
    private $approved;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime|null
     */
    public function getResolutionDate()
    {
        return $this->resolution_date;
    }

    /**
     * @param \DateTime|null $resolution_date
     */
    public function setResolutionDate($resolution_date)
    {
        $this->resolution_date = $resolution_date;
    }

    /**
     * @return bool|null
     */
    public function isApproved()
    {
        return $this->approved;
    }

    /**
     * @param bool|null $approved
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;
    }

    /**
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
}

