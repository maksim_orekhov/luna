<?php
namespace Application\Entity\Repository;

use Doctrine\ORM\EntityManager;

/**
 * Trait EntityUtilTrait
 * @package Application\Entity
 */
trait EntityUtilTrait
{
    /**
     * @param EntityManager $entityManager
     * @param string $entity_class
     * @return string
     */
    public function getEntityTableName(EntityManager $entityManager, string $entity_class): string
    {
        $classMetadata = $entityManager->getClassMetadata($entity_class);

        return $classMetadata->getTableName();
    }

    /**
     * @param EntityManager $entityManager
     * @param string $entity_class
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    public function nextAutoIncrementId(EntityManager $entityManager, string $entity_class)
    {
        $table_name = $this->getEntityTableName($entityManager, $entity_class);
        $connect = $entityManager->getConnection()->prepare("SELECT auto_increment FROM information_schema.TABLES WHERE table_name='$table_name'");
        $connect->execute();

        return $connect->fetchColumn();
    }
}