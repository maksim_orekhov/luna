<?php
namespace Application\Entity\Repository;
use Application\Entity\City;

/**
 * Class ShopRepository
 * @package Application\Entity\Repository
 */
class ShopRepository extends BaseRepository
{
    const ITEMS_PER_PAGE = 8;

    /**
     * @param array $route_params
     * @return array|\Zend\Paginator\Paginator
     */
    public function findPublishedShopsByRouteParams(array $route_params = [
        'search' => null,
        'city_slug' => null,
        'category_slug' => null,
        'sub_category_slug' => null,
        'page' => 1,
    ])
    {
        if (! $route_params['city_slug'] ||
            ($route_params['city_slug'] && ! $route_params['category_slug'] && $route_params['sub_category_slug'])) {
            return [];
        }

        $city_slug = '';
        if ($route_params['city_slug']) {
            $city_slug = $route_params['city_slug'];
        }
        $category_slug = '';
        if ($route_params['category_slug']) {
            $category_slug = $route_params['category_slug'];
        }
        $sub_category_slug = '';
        if ($route_params['sub_category_slug']) {
            $sub_category_slug = $route_params['sub_category_slug'];
        }

        $qb = $this->createQueryBuilder('s')
            ->addSelect('user')
            ->innerJoin('s.user', 'user')
            ->addSelect('p')
            ->innerJoin('user.products', 'p')
            ->addSelect('city')
            ->leftJoin('p.cities', 'city')
            ->addSelect('region')
            ->leftJoin('p.cities', 'region')
            ->addSelect('country')
            ->leftJoin('p.cities', 'country')
            ->addSelect('category')
            ->innerJoin('p.categories', 'category')
            ->addSelect('sub_category')
            ->innerJoin('p.categories', 'sub_category')
            ->addSelect('sm')
            ->innerJoin('s.moderations', 'sm')
            ->andWhere('sm.created = (SELECT MAX(sm2.created) FROM \Application\Entity\Moderation sm2 WHERE sm2 MEMBER OF s.moderations) AND
                        sm.approved = true AND
                        p.removed = false AND
                        p.visible = true AND
                        category.parent is Null AND
                        sub_category.parent is not Null AND 
                        city.type = :city_type AND
                        region.type = :region_type AND
                        country.type = :country_type')
            ->setParameter('city_type', City::CITY_TYPE)
            ->setParameter('region_type', City::REGION_TYPE)
            ->setParameter('country_type', City::COUNTRY_TYPE)
        ;

        if (! empty($city_slug)) {
            $qb->andWhere('(city.slug = :city_slug OR region.slug = :city_slug OR country.slug = :city_slug)')
                ->setParameter('city_slug', $city_slug);
        }
        if (! empty($category_slug)) {
            $qb->andWhere('category.slug = :category_slug')
                ->setParameter('category_slug', $category_slug);
        }
        if (! empty($sub_category_slug)) {
            $qb->andWhere('sub_category.slug = :sub_category_slug')
                ->setParameter('sub_category_slug', $sub_category_slug);
        }

        //сортировка
        $qb->orderBy('s.created', 'desc');

        return $this->paginate($qb, $route_params['page'], self::ITEMS_PER_PAGE);
    }
}