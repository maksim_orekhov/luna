<?php
namespace Application\Entity\Repository;

use Application\Entity\City;

/**
 * Class CityRepository
 * @package Application\Entity\Repository
 */
class CityRepository extends BaseRepository
{
    const ADDRESS_EXCEPTION = [
        'Республика'
    ];

    use EntityUtilTrait;

    /**
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getNextCityAutoIncrementId()
    {
        return $this->nextAutoIncrementId($this->getEntityManager(), City::class);
    }

    public function findRegionOneByNameAndCountry(City $country, string $addressRegion = '')
    {
        if (empty($addressRegion) || !$country) {
            return null;
        }
        foreach (self::ADDRESS_EXCEPTION as $exception) {
            $addressRegion = trim(str_replace($exception, '', $addressRegion));
        }

        $qb = $this->createQueryBuilder('c')
            ->andWhere('c.parent = :parent AND
                        c.type = :type AND
                        c.name LIKE :addressRegion')
            ->setParameter('parent', $country)
            ->setParameter('addressRegion', '%'.$addressRegion)
            ->setParameter('type', City::REGION_TYPE)
            ->setMaxResults(1)
        ;

        try {

            return $qb->getQuery()->getSingleResult();
        }
        catch (\Throwable $t){

            return null;
        }
    }

    public function findCityOneByNameAndRegion(City $region, string $addressCity = '')
    {
        if (empty($addressCity) || !$region) {
            return null;
        }


        $qb = $this->createQueryBuilder('c')
            ->andWhere('c.parent = :parent AND
                        c.type = :type AND
                        c.name = :addressCity')
            ->setParameter('parent', $region)
            ->setParameter('addressCity', $addressCity)
            ->setParameter('type', City::CITY_TYPE)
            ->setMaxResults(1)
        ;

        try {

            return $qb->getQuery()->getSingleResult();
        }
        catch (\Throwable $t){

            return null;
        }
    }
}