<?php
namespace Application\Entity\Repository;

use Application\Entity\Category;

/**
 * Class CategoryRepository
 * @package Application\Entity\Repository
 */
class CategoryRepository extends BaseRepository
{
    use EntityUtilTrait;

    /**
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getNextCategoryAutoIncrementId()
    {
        return $this->nextAutoIncrementId($this->getEntityManager(), Category::class);
    }

    /**
     * @return array
     */
    public function getSubCategories()
    {
        $qb = $this->createQueryBuilder('c')
            ->andWhere('c.parent is not NULL')
            ->andWhere('c.active = true')
        ;

        return $qb->getQuery()->getResult();
    }
}