<?php
namespace Application\Entity\Repository;

use Application\Entity\Category;
use Application\Entity\City;
use Application\Entity\Product;
use Application\Entity\User;
use Doctrine\ORM\Query\Expr;
use ModuleFileManager\Entity\File;
use Zend\Paginator\Paginator;

/**
 * Class ProductRepository
 * @package Application\Entity\Repository
 */
class ProductRepository extends BaseRepository
{
    use EntityUtilTrait;

    const ITEMS_PER_PAGE = 8;

    /**
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getNextProductFileAutoIncrementId()
    {
        return $this->nextAutoIncrementId($this->getEntityManager(), File::class);
    }

    public function getProductByCategory($product_slug, Category $category)
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('c')
            ->innerJoin('p.categories', 'c')
            ->andWhere(':product_slug = p.slug')
            ->andWhere(':category MEMBER OF p.categories')
            ->setParameter('product_slug', $product_slug)
            ->setParameter('category', $category)
        ;

        try {

            return $qb->getQuery()->getSingleResult();
        }
        catch (\Throwable $t){

            return null;
        }
    }

    /**
     * @param array $route_params
     * @return array|\Zend\Paginator\Paginator
     */
    public function findPublishedProductsByRouteParams(array $route_params = [
        'search' => null,
        'city_slug' => null,
        'category_slug' => null,
        'sub_category_slug' => null,
        'fabricator' => false,
        'shop_slug' => false,
        'page' => 1,
    ])
    {
        if (! $route_params['city_slug'] ||
            ($route_params['city_slug'] && ! $route_params['category_slug'] && $route_params['sub_category_slug'])) {
            return [];
        }

        $city_slug = '';
        if (isset($route_params['city_slug'])) {
            $city_slug = $route_params['city_slug'];
        }
        $category_slug = '';
        if (isset($route_params['category_slug'])) {
            $category_slug = $route_params['category_slug'];
        }
        $sub_category_slug = '';
        if (isset($route_params['sub_category_slug'])) {
            $sub_category_slug = $route_params['sub_category_slug'];
        }
        $shop_slug = '';
        if (isset($route_params['shop_slug'])) {
            $shop_slug = $route_params['shop_slug'];
        }
        $only_fabricator = false;
        if (isset($route_params['fabricator'])) {
            $only_fabricator = true;
        }

        $qb = $this->createQueryBuilder('p')
            ->addSelect('user')
            ->innerJoin('p.user', 'user')
            ->addSelect('city')
            ->leftJoin('p.cities', 'city')
            ->addSelect('region')
            ->leftJoin('p.cities', 'region')
            ->addSelect('country')
            ->leftJoin('p.cities', 'country')
            ->addSelect('category')
            ->innerJoin('p.categories', 'category')
            ->addSelect('sub_category')
            ->innerJoin('p.categories', 'sub_category')
            ->addSelect('m')
            ->innerJoin('p.moderations', 'm')
            ->andWhere('m.created = (SELECT MAX(pm.created) FROM \Application\Entity\Moderation pm WHERE pm MEMBER OF p.moderations) AND
                        m.approved = true AND
                        p.removed = false AND
                        p.visible = true AND
                        category.parent is Null AND
                        sub_category.parent is not Null AND 
                        city.type = :city_type AND
                        region.type = :region_type AND
                        country.type = :country_type')
            ->setParameter('city_type', City::CITY_TYPE)
            ->setParameter('region_type', City::REGION_TYPE)
            ->setParameter('country_type', City::COUNTRY_TYPE)
        ;

        //выборка только для фабрикатора
        if ($only_fabricator === true) {
            $qb->addSelect('fabricator')
                ->innerJoin('user.fabricator', 'fabricator', Expr\Join::WITH, 'fabricator.user = user')
                ->addSelect('f_category')
                ->innerJoin('fabricator.categories', 'f_category')
                ->addSelect('fm')
                ->innerJoin('fabricator.moderations', 'fm')
                ->andWhere('(f_category.id = sub_category.id OR f_category.id = category.id)')
                ->andWhere('fm.created = (SELECT MAX(fm2.created) FROM \Application\Entity\Moderation fm2 WHERE fm2 MEMBER OF fabricator.moderations)')
                ->andWhere('fm.approved = true');
        }

        //выборка для магазинов
        if (! empty($shop_slug)) {
            $qb->addSelect('shop')
                ->innerJoin('user.shop', 'shop')
                ->addSelect('sm')
                ->innerJoin('shop.moderations', 'sm')
                ->andWhere('sm.created = (SELECT MAX(sm2.created) FROM \Application\Entity\Moderation sm2 WHERE sm2 MEMBER OF shop.moderations)')
                ->andWhere('sm.approved = true')
                ->andWhere('shop.slug = :shop_slug')
                ->setParameter('shop_slug', $shop_slug);
        }

        if (! empty($city_slug)) {
            $qb->andWhere('(city.slug = :city_slug OR region.slug = :city_slug OR country.slug = :city_slug)')
                ->setParameter('city_slug', $city_slug);
        }
        if (! empty($category_slug)) {
            $qb->andWhere('category.slug = :category_slug')
                ->setParameter('category_slug', $category_slug);
        }
        if (! empty($sub_category_slug)) {
            $qb->andWhere('sub_category.slug = :sub_category_slug')
                ->setParameter('sub_category_slug', $sub_category_slug);
        }
        if (! empty($route_params['search'])) {
            $qb->andWhere('p.name LIKE :search')
                ->setParameter('search', '%'.$route_params['search'].'%');
        }

        //сортировка
        $qb->orderBy('p.created', 'desc');

        return $this->paginate($qb, $route_params['page'], self::ITEMS_PER_PAGE);
    }

    /**
     * @param string $search
     * @param string|null $fabricator
     * @return array
     */
    public function searchPublicProducts(string $search = '', string $fabricator = null)
    {
        if (empty($search)) {
            return [];
        }
        $only_fabricator = false;
        if ($fabricator) {
            $only_fabricator = true;
        }
        $qb = $this->createQueryBuilder('p')
            ->addSelect('user')
            ->innerJoin('p.user', 'user')
            ->addSelect('category')
            ->innerJoin('p.categories', 'category')
            ->addSelect('sub_category')
            ->innerJoin('p.categories', 'sub_category')
            ->addSelect('m')
            ->innerJoin('p.moderations', 'm')
            ->andWhere('m.created = (SELECT MAX(pm.created) FROM \Application\Entity\Moderation pm WHERE pm MEMBER OF p.moderations) AND
                        p.removed = false AND
                        p.visible = true AND
                        m.approved = true AND
                        category.parent is Null AND
                        sub_category.parent is not Null AND 
                        (p.name LIKE :search)')
            //сортировка
            ->orderBy('p.created', 'desc')
            ->setParameter('search', '%'.$search.'%')
            ->setMaxResults(20)
        ;
        //выборка только для фабрикатора
        if ($only_fabricator === true) {
            $qb->addSelect('fabricator')
                ->innerJoin('user.fabricator', 'fabricator', Expr\Join::WITH, 'fabricator.user = user')
                ->addSelect('f_category')
                ->innerJoin('fabricator.categories', 'f_category')
                ->addSelect('fm')
                ->innerJoin('fabricator.moderations', 'fm')
                ->andWhere('(f_category.id = sub_category.id OR f_category.id = category.id)')
                ->andWhere('fm.created = (SELECT MAX(fm2.created) FROM \Application\Entity\Moderation fm2 WHERE fm2 MEMBER OF fabricator.moderations)')
                ->andWhere('fm.approved = true');
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * Similar Products
     *
     * @param Product $product
     * @param int $page
     * @param int $limit
     * @return array|\Zend\Paginator\Paginator
     */
    public function searchSimilarProducts(Product $product, $page = 1, $limit = 5)
    {
        $collections = $product->getCategories();
        $category = $collections->filter(function(Category $entity){
            return $entity->getParent() !== null;
        })->first();
        if (! $category) {
            $category = $collections->filter(function(Category $entity){
                return $entity->getParent() === null;
            })->first();
        }
        if (! $category) {

            return [];
        }

        $qb = $this->createQueryBuilder('p')
            ->addSelect('m')
            ->innerJoin('p.moderations', 'm')
            ->andWhere('m.created = (SELECT MAX(pm.created) FROM \Application\Entity\Moderation pm WHERE pm MEMBER OF p.moderations) AND
                        p.removed = false AND
                        p.visible = true AND
                        m.approved = true AND
                        p.id <> :product_id AND
                        :category MEMBER OF p.categories')
            ->setParameter('category', $category)
            ->setParameter('product_id', $product->getId())
            ->orderBy('p.created', 'desc')
            ->groupBy('p.id')
        ;

        return $this->paginate($qb, $page, $limit);
    }

    /**
     * @param null $category
     * @param array $exclude_product
     * @return mixed|null
     */
    public function getLastProductsByCategory($category = null, array $exclude_product = [])
    {
        $exclude = null;
        if (!empty($exclude_product)) {
            foreach ($exclude_product as $product) {
                if ($product instanceof Product) {
                    $exclude[] = $product->getId();
                }
            }
        }

        $qb = $this->createQueryBuilder('p')
            ->addSelect('m')
            ->innerJoin('p.moderations', 'm')
            ->andWhere('m.created = (SELECT MAX(pm.created) FROM \Application\Entity\Moderation pm WHERE pm MEMBER OF p.moderations) AND
                        p.removed = false AND
                        p.visible = true AND
                        m.approved = true AND
                        :category MEMBER OF p.categories')
            ->setParameter('category', $category)
            ->orderBy('p.created', 'desc')
            ->groupBy('p.id')
            ->setMaxResults(1)
        ;
        if ($exclude) {
            $qb->andWhere('p.id not in (:exclude)')
                ->setParameter('exclude', $exclude);
        }

        try {
            return $qb->getQuery()->getSingleResult();
        }
        catch (\Throwable $t){

            return null;
        }
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return Paginator
     */
    public function selectModerationProducts(User $user = null, int $page = 1, int $per_page = null): Paginator
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('c')
            ->innerJoin('p.categories', 'c')
            ->addSelect('m')
            ->innerJoin('p.moderations', 'm')
            ->andWhere('
                m.created = (SELECT MAX(pm.created) FROM \Application\Entity\moderation pm WHERE pm MEMBER OF p.moderations) AND
                p.removed = false AND
                p.visible = true AND
                m.approved is Null')
        ;

        if ($user) {
            $qb->andWhere(':user = p.user')
                ->setParameter('user', $user);
        }

        if (null === $per_page) {
            $per_page = self::ITEMS_PER_PAGE;
        }
        //сортировка
        $qb->orderBy('p.created', 'desc');

        return $this->paginate($qb, $page, $per_page);
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return Paginator
     */
    public function selectDeclineProducts(User $user = null, int $page = 1, int $per_page = null): Paginator
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('c')
            ->innerJoin('p.categories', 'c')
            ->addSelect('m')
            ->innerJoin('p.moderations', 'm')
            ->andWhere('
                m.created = (SELECT MAX(pm.created) FROM \Application\Entity\Moderation pm WHERE pm MEMBER OF p.moderations) AND
                p.removed = false AND
                p.visible = true AND
                m.approved = false
            ')
        ;

        if ($user) {
            $qb->andWhere(':user = p.user')
                ->setParameter('user', $user);
        }

        if (null === $per_page) {
            $per_page = self::ITEMS_PER_PAGE;
        }
        //сортировка
        $qb->orderBy('p.created', 'desc');

        return $this->paginate($qb, $page, $per_page);
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return Paginator
     */
    public function selectInactiveProducts(User $user = null, int $page = 1, int $per_page = null): Paginator
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('c')
            ->innerJoin('p.categories', 'c')
            ->addSelect('m')
            ->innerJoin('p.moderations', 'm')
            ->andWhere('
                m.created = (SELECT MAX(pm.created) FROM \Application\Entity\Moderation pm WHERE pm MEMBER OF p.moderations) AND
                (p.removed = false OR p.removed is Null) AND
                (p.visible = false OR m.approved = false OR m.approved is Null)
            ')
        ;

        if ($user) {
            $qb->andWhere(':user = p.user')
                ->setParameter('user', $user);
        }

        if (null === $per_page) {
            $per_page = self::ITEMS_PER_PAGE;
        }
        //сортировка
        $qb->orderBy('p.created', 'desc');

        return $this->paginate($qb, $page, $per_page);
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return Paginator
     */
    public function selectArchiveProducts(User $user = null, int $page = 1, int $per_page = null): Paginator
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('c')
            ->innerJoin('p.categories', 'c')
            ->addSelect('m')
            ->innerJoin('p.moderations', 'm')
            ->andWhere('p.removed = true')
        ;

        if ($user) {
            $qb->andWhere(':user = p.user')
                ->setParameter('user', $user);
        }

        if (null === $per_page) {
            $per_page = self::ITEMS_PER_PAGE;
        }
        //сортировка
        $qb->orderBy('p.created', 'desc');

        return $this->paginate($qb, $page, $per_page);
    }

    /**
     * @param User|null $user
     * @param array $route_params
     * @return Paginator
     */
    public function selectPublicProducts(User $user = null, array $route_params = [
        'search' => null,
        'per_page' => self::ITEMS_PER_PAGE,
        'page' => 1,
    ]): Paginator
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('c')
            ->innerJoin('p.categories', 'c')
            ->addSelect('m')
            ->innerJoin('p.moderations', 'm')
            ->andWhere('
                m.created = (SELECT MAX(pm.created) FROM \Application\Entity\Moderation pm WHERE pm MEMBER OF p.moderations) AND
                p.removed = false AND
                p.visible = true AND
                m.approved = true
            ')
        ;

        if ($user) {
            $qb->andWhere(':user = p.user')
                ->setParameter('user', $user);
        }
        if (! empty($route_params['search'])) {
            $qb->andWhere('p.name LIKE :search')
                ->setParameter('search', '%'.$route_params['search'].'%');
        }
        //сортировка
        $qb->orderBy('p.created', 'desc');

        return $this->paginate($qb, $route_params['page'], $route_params['per_page']);
    }
}