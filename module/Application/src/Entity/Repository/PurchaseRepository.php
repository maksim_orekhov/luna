<?php
namespace Application\Entity\Repository;

use Application\Entity\Category;
use Application\Entity\City;
use Application\Entity\User;
use ModuleFileManager\Entity\File;
use Zend\Paginator\Paginator;

/**
 * Class PurchaseRepository
 * @package Application\Entity\Repository
 */
class PurchaseRepository extends BaseRepository
{
    use EntityUtilTrait;

    const ITEMS_PER_PAGE = 8;

    /**
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getNextPurchaseFileAutoIncrementId()
    {
        return $this->nextAutoIncrementId($this->getEntityManager(), File::class);
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return Paginator
     */
    public function selectModerationPurchases(User $user = null, int $page = 1, int $per_page = null): Paginator
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('c')
            ->innerJoin('p.categories', 'c')
            ->addSelect('m')
            ->innerJoin('p.moderations', 'm')
            ->andWhere('
                m.created = (SELECT MAX(pm.created) FROM \Application\Entity\moderation pm WHERE pm MEMBER OF p.moderations) AND
                p.removed = false AND
                p.visible = true AND
                m.approved is Null')
        ;

        if ($user) {
            $qb->andWhere(':user = p.user')
                ->setParameter('user', $user);
        }

        if (null === $per_page) {
            $per_page = self::ITEMS_PER_PAGE;
        }
        //сортировка
        $qb->orderBy('p.created', 'desc');

        return $this->paginate($qb, $page, $per_page);
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return Paginator
     */
    public function selectDeclinePurchases(User $user = null, int $page = 1, int $per_page = null): Paginator
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('c')
            ->innerJoin('p.categories', 'c')
            ->addSelect('m')
            ->innerJoin('p.moderations', 'm')
            ->andWhere('
                m.created = (SELECT MAX(pm.created) FROM \Application\Entity\Moderation pm WHERE pm MEMBER OF p.moderations) AND
                p.removed = false AND
                p.visible = true AND
                m.approved = false
            ');

        if ($user) {
            $qb->andWhere(':user = p.user')
                ->setParameter('user', $user);
        }

        if (null === $per_page) {
            $per_page = self::ITEMS_PER_PAGE;
        }
        //сортировка
        $qb->orderBy('p.created', 'desc');

        return $this->paginate($qb, $page, $per_page);
    }

    /**
     * @param User|null $user
     * @param array $route_params
     * @return Paginator
     */
    public function selectPublicPurchases(User $user = null, array $route_params = [
        'search' => null,
        'per_page' => self::ITEMS_PER_PAGE,
        'page' => 1,
    ]): Paginator
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('c')
            ->innerJoin('p.categories', 'c')
            ->addSelect('m')
            ->innerJoin('p.moderations', 'm')
            ->andWhere('
                m.created = (SELECT MAX(pm.created) FROM \Application\Entity\Moderation pm WHERE pm MEMBER OF p.moderations) AND
                p.removed = false AND
                p.visible = true AND
                m.approved = true
            ')
        ;

        if ($user) {
            $qb->andWhere(':user = p.user')
                ->setParameter('user', $user);
        }
        if (! empty($route_params['search'])) {
            $qb->andWhere('p.name LIKE :search')
                ->setParameter('search', '%'.$route_params['search'].'%');
        }
        //сортировка
        $qb->orderBy('p.created', 'desc');

        return $this->paginate($qb, $route_params['page'], $route_params['per_page']);
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return Paginator
     */
    public function selectArchivePurchases(User $user = null, int $page = 1, int $per_page = null): Paginator
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('c')
            ->innerJoin('p.categories', 'c')
            ->addSelect('m')
            ->innerJoin('p.moderations', 'm')
            ->andWhere('p.removed = true')
        ;

        if ($user) {
            $qb->andWhere(':user = p.user')
                ->setParameter('user', $user);
        }

        if (null === $per_page) {
            $per_page = self::ITEMS_PER_PAGE;
        }
        //сортировка
        $qb->orderBy('p.created', 'desc');

        return $this->paginate($qb, $page, $per_page);
    }

    /**
     * @param User|null $user
     * @param int $page
     * @param int|null $per_page
     * @return Paginator
     */
    public function selectInactivePurchases(User $user = null, int $page = 1, int $per_page = null): Paginator
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('c')
            ->innerJoin('p.categories', 'c')
            ->addSelect('m')
            ->innerJoin('p.moderations', 'm')
            ->andWhere('
                m.created = (SELECT MAX(pm.created) FROM \Application\Entity\Moderation pm WHERE pm MEMBER OF p.moderations) AND
                (p.removed = false OR p.removed is Null) AND
                (p.visible = false OR m.approved = false OR m.approved is Null)
            ')
        ;

        if ($user) {
            $qb->andWhere(':user = p.user')
                ->setParameter('user', $user);
        }

        if (null === $per_page) {
            $per_page = self::ITEMS_PER_PAGE;
        }
        //сортировка
        $qb->orderBy('p.created', 'desc');

        return $this->paginate($qb, $page, $per_page);
    }

    /**
     * @param string $search
     * @return array|\Zend\Paginator\Paginator
     */
    public function searchPublicPurchases(string $search = '')
    {
        if (empty($search)) {
            return [];
        }
        $qb = $this->createQueryBuilder('p')
            ->addSelect('m')
            ->innerJoin('p.moderations', 'm')
            ->andWhere('m.created = (SELECT MAX(pm.created) FROM \Application\Entity\Moderation pm WHERE pm MEMBER OF p.moderations) AND
                        p.removed = false AND
                        p.visible = true AND
                        m.approved = true AND
                        (p.name LIKE :search)')
            //сортировка
            ->orderBy('p.created', 'desc')
            ->setParameter('search', '%'.$search.'%')
            ->setMaxResults(20)
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @param array $route_params
     * @return array|\Zend\Paginator\Paginator
     */
    public function findPublishedPurchasesByRouteParams(array $route_params = [
        'search' => null,
        'city_slug' => null,
        'category_slug' => null,
        'sub_category_slug' => null,
        'page' => 1,
    ])
    {
        if (! $route_params['city_slug'] ||
            ($route_params['city_slug'] && ! $route_params['category_slug'] && $route_params['sub_category_slug'])) {
            return [];
        }

        $city_slug = '';
        if ($route_params['city_slug']) {
            $city_slug = $route_params['city_slug'];
        }
        $category_slug = '';
        if ($route_params['category_slug']) {
            $category_slug = $route_params['category_slug'];
        }
        $sub_category_slug = '';
        if ($route_params['sub_category_slug']) {
            $sub_category_slug = $route_params['sub_category_slug'];
        }

        $qb = $this->createQueryBuilder('p')
            ->addSelect('city')
            ->leftJoin('p.cities', 'city')
            ->addSelect('region')
            ->leftJoin('p.cities', 'region')
            ->addSelect('country')
            ->leftJoin('p.cities', 'country')
            ->addSelect('category')
            ->innerJoin('p.categories', 'category')
            ->addSelect('sub_category')
            ->innerJoin('p.categories', 'sub_category')
            ->addSelect('m')
            ->innerJoin('p.moderations', 'm')
            ->andWhere('m.created = (SELECT MAX(pm.created) FROM \Application\Entity\Moderation pm WHERE pm MEMBER OF p.moderations) AND
                        m.approved = true AND
                        p.removed = false AND
                        p.visible = true AND
                        category.parent is Null AND
                        sub_category.parent is not Null AND 
                        city.type = :city_type AND
                        region.type = :region_type AND
                        country.type = :country_type')
            ->setParameter('city_type', City::CITY_TYPE)
            ->setParameter('region_type', City::REGION_TYPE)
            ->setParameter('country_type', City::COUNTRY_TYPE)
        ;

        if (! empty($city_slug)) {
            $qb->andWhere('(city.slug = :city_slug OR region.slug = :city_slug OR country.slug = :city_slug)')
                ->setParameter('city_slug', $city_slug);
        }
        if (! empty($category_slug)) {
            $qb->andWhere('category.slug = :category_slug')
                ->setParameter('category_slug', $category_slug);
        }
        if (! empty($sub_category_slug)) {
            $qb->andWhere('sub_category.slug = :sub_category_slug')
                ->setParameter('sub_category_slug', $sub_category_slug);
        }
        if (! empty($route_params['search'])) {
            $qb->andWhere('p.name LIKE :search')
                ->setParameter('search', '%'.$route_params['search'].'%');
        }
        //сортировка
        $qb->orderBy('p.created', 'desc');
        return $this->paginate($qb, $route_params['page'], self::ITEMS_PER_PAGE);
    }

    /**
     * @param $purchase_slug
     * @param Category $category
     * @return mixed|null
     */
    public function getPurchaseByCategory($purchase_slug, Category $category)
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('c')
            ->innerJoin('p.categories', 'c')
            ->andWhere(':purchase_slug = p.slug')
            ->andWhere(':category MEMBER OF p.categories')
            ->setParameter('purchase_slug', $purchase_slug)
            ->setParameter('category', $category)
        ;

        try {

            return $qb->getQuery()->getSingleResult();
        }
        catch (\Throwable $t){

            return null;
        }
    }
}