<?php
namespace Application\Entity\Repository;

use Application\Entity\User;
use Doctrine\ORM\QueryBuilder;

/**
 * Class UserRepository
 * @package Application\Entity\Repository
 */
class UserRepository extends BaseRepository
{
    const PER_PAGE_DEFAULT = 10;

    const ALIASES = [
        'user_id' => 'u.id',
        'user_login' => 'u.login',
        'user_created_from' => 'u.created',
        'user_created_till' => 'u.created',
    ];

    /**
     * @param $phone_value
     * @return array
     */
    public function selectUsersByPhoneValue($phone_value)
    {
        $qb = $this->createQueryBuilder('u')
            //->addSelect('u')
            //->addSelect('p')
            ->leftJoin('u.phone', 'p')
            //->andWhere('u.isBanned = false')
            ->andWhere('p.phoneNumber = :phone_value')
            ->setParameter('phone_value', $phone_value);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $email_value
     * @return array
     */
    public function findAllUnverifiedUserByEmail($email_value)
    {
        $qb = $this->createQueryBuilder('u')
            ->leftJoin('u.email', 'e')
            ->where('e.email = :email_value and (e.isVerified = 0 or e.isVerified is null)')
            ->setParameter('email_value', $email_value);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $avatar_id
     * @return User|null
     */
    public function selectUsersByAvatarId($avatar_id)
    {
        $qb = $this->createQueryBuilder('u')
            ->innerJoin('u.avatar', 'ua')
            ->andWhere('ua.id = :avatar_id')
            ->setParameter('avatar_id', $avatar_id);

        try {
            $result = $qb->getQuery()->getSingleResult();
        } catch (\Throwable $t){
            $result = null;
        }
        return $result;
    }

    /**
     * @param $params
     * @return array|\Zend\Paginator\Paginator
     */
    public function selectSortedUser($params)
    {
        // QueryBuilderQuery
        $result = $this->executeQueryBuilderQuery($params);

        return $result;
    }

    /**
     * @param array $params
     * @return array|\Zend\Paginator\Paginator
     */
    private function executeQueryBuilderQuery(array $params)
    {
        $qb = $this->createQueryBuilder('u')
            ->addSelect('phone') // u.phone
            ->leftJoin('u.phone', 'phone')
            ->addSelect('email') // u.email
            ->leftJoin('u.email', 'email')
            ->addSelect('roles') // u.roles
            ->leftJoin('u.roles', 'roles')
        ;

        // Добавляем к запросу параметры фильтрации
        if (key_exists('filter', $params)) {
            $this->addQueryBuilderFilterParams($qb, $params['filter']);
        }

        #var_dump($params);
        #die();

        // Добавляем к запросу параметры сортировки
        if (key_exists('order', $params)) {
            $this->addQueryBuilderSortParams($qb, $params['order']);
        }

        if (isset($params['per_page']) && isset($params['page'])) {
            $userCollection = $this->paginate($qb, $params['page'], $params['per_page']);
        } else {
            $userCollection = $qb->getQuery()->getResult();
        }

        return $userCollection;
    }

    /**
     * @param QueryBuilder $qb
     * @param array $params
     */
    private function addQueryBuilderFilterParams(QueryBuilder $qb, array $params)
    {
        foreach ($params as $item) {
            // common cases
            $qb->andWhere(
                self::ALIASES[$item['property']] . ' ' . self::OPERATORS_FOR_FILTER_RULES[$item['rule']]['operator'] . ' :' . $item['property'])
                ->setParameter($item['property'],
                    self::OPERATORS_FOR_FILTER_RULES[$item['rule']]['pre_item'] . $item['value'] . self::OPERATORS_FOR_FILTER_RULES[$item['rule']]['post_item']
                );
        }
    }

    /**
     * @param QueryBuilder $qb
     * @param array $params
     */
    private function addQueryBuilderSortParams(QueryBuilder $qb, array $params)
    {
        foreach ($params as $key=>$value) {
            switch ($key) {
                case 'id':
                    $qb->addOrderBy('u.id', $value);
                    break;
                case 'login':
                    $qb->addOrderBy('u.login', $value);
                    break;
                case 'created':
                    $qb->addOrderBy('u.created', $value);
                    break;
            }
        }
    }
}