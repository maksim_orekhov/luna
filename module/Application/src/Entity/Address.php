<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Address
 * @package Application\Entity
 *
 * @ORM\Table(name="address")
 * @ORM\Entity();
 */
class Address
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=255, nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="house", type="string", length=16, nullable=true)
     */
    private $house;

    /**
     * @var string
     *
     * @ORM\Column(name="apartment", type="string", length=16, nullable=true)
     */
    private $apartment;

    /**
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", length=64, nullable=true)
     */
    private $postcode;

    /**
     * @var string
     *
     * @ORM\Column(name="lat", type="string", length=128, nullable=true)
     */
    private $lat;

    /**
     * @var string
     *
     * @ORM\Column(name="lng", type="string", length=128, nullable=true)
     */
    private $lng;

    /**
     * @var Product
     *
     * One Address has One Product.
     * @ORM\OneToOne(targetEntity="Product", mappedBy="address")
     */
    private $product;

    /**
     * @var Product
     *
     * One Address has One Purchase.
     * @ORM\OneToOne(targetEntity="Purchase", mappedBy="address")
     */
    private $purchase;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string|null
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string|null $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return string|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string|null
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string|null $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string|null
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * @param string|null $house
     */
    public function setHouse($house)
    {
        $this->house = $house;
    }

    /**
     * @return string|null
     */
    public function getApartment()
    {
        return $this->apartment;
    }

    /**
     * @param string|null $apartment
     */
    public function setApartment($apartment)
    {
        $this->apartment = $apartment;
    }

    /**
     * @return string|null
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param string|null $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @return string|null
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param string|null $lat
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    }

    /**
     * @return string|null
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @param string|null $lng
     */
    public function setLng($lng)
    {
        $this->lng = $lng;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @return Product
     */
    public function getPurchase(): Product
    {
        return $this->purchase;
    }

    /**
     * @param Product $purchase
     */
    public function setPurchase(Product $purchase)
    {
        $this->purchase = $purchase;
    }
}

