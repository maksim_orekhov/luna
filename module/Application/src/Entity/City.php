<?php
namespace Application\Entity;

use Core\Entity\AbstractCountry;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class City
 * @package Application\Entity
 *
 * @ORM\Table(name="city")
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\CityRepository");
 */
class City
{
    const COUNTRY_TYPE = 'COUNTRY';
    const REGION_TYPE = 'REGION';
    const CITY_TYPE = 'CITY';
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="name_prepositional", type="string", length=128)
     */
    private $namePrepositional;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=128, nullable=false)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=128, nullable=false)
     */
    private $type;

    /**
     * One City has Many Cities.
     * @ORM\OneToMany(targetEntity="City", mappedBy="parent")
     */
    private $children;

    /**
     * Many Cities have One City.
     * @ORM\ManyToOne(targetEntity="City", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * Many Cities have Many Products.
     * @ORM\ManyToMany(targetEntity="Product", mappedBy="cities")
     */
    private $products;

    /**
     * Many Cities have Many Purchases.
     * @ORM\ManyToMany(targetEntity="Purchase", mappedBy="cities")
     */
    private $purchases;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->purchases = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getNamePrepositional()
    {
        return $this->namePrepositional;
    }

    /**
     * @param string|null $namePrepositional
     */
    public function setNamePrepositional($namePrepositional)
    {
        $this->namePrepositional = $namePrepositional;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param Product $product
     */
    public function addProduct(Product $product)
    {
        $this->products->add($product);
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param City $children
     */
    public function addChildren(City $children)
    {
        $this->children->add($children);
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getPurchases()
    {
        return $this->purchases;
    }

    /**
     * @param mixed $purchases
     */
    public function setPurchases($purchases)
    {
        $this->purchases = $purchases;
    }

    /**
     * @param Purchase $purchase
     */
    public function addPurchase(Purchase $purchase)
    {
        $this->products->add($purchase);
    }
}

