<?php
namespace Application\Entity\Interfaces;

use Application\Entity\Address;
use Application\Entity\City;

/**
 * Interface ProductInterface
 * @package Application\Entity\Interfaces
 */
interface ProductInterface
{
    // @TODO Добавить другие методы, обязательные для типа Объявление

    /**
     * @return mixed
     */
    public function getCities();

    /**
     * @param mixed $cities
     */
    public function setCities($cities);

    /**
     * @param City $city
     */
    public function addCity(City $city);

    /**
     * @return Address
     */
    public function getAddress(): Address;

    /**
     * @param Address|null $address
     */
    public function setAddress(Address $address = null);

    /**
     * @return mixed
     */
    public function getModerations();

    /**
     * @return bool
     */
    public function isRemoved(): bool;
    /**
     * @return bool
     */
    public function isVisible(): bool;
}