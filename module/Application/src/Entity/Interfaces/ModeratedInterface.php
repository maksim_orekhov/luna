<?php
namespace Application\Entity\Interfaces;

use Application\Entity\Moderation;

/**
 * Interface ModeratedInterface
 * @package Application\Entity\Interfaces
 */
interface ModeratedInterface
{
    /**
     * @return mixed
     */
    public function getModerations();

    /**
     * @param mixed $moderations
     */
    public function setModerations($moderations);

    /**
     * @param Moderation $moderation
     */
    public function addModeration(Moderation $moderation);
}