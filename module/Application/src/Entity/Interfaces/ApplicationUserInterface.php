<?php
namespace Application\Entity\Interfaces;

use Application\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use ModuleFileManager\Entity\File;
use ModuleCode\Entity\Code;

/**
 * Interface ApplicationUserInterface
 * @package Application\Entity\Interfaces
 */
interface ApplicationUserInterface
{
    /**
     * @return mixed
     */
    public function getCodes();
    /**
     * @param mixed $codes
     */
    public function setCodes($codes);
    /**
     * Add code
     *
     * @param Code $code
     *
     * @return User
     */
    public function addCode(Code $code);
    /**
     * Remove code
     *
     * @param Code $code
     */
    public function removeCode(Code $code);
    /**
     * @return bool
     */
    public function getIsBanned();
    /**
     * @param bool $isBanned
     */
    public function setIsBanned($isBanned);
    /**
     * @return File
     */
    public function getAvatar();
    /**
     * @return bool
     */
    public function existAvatar();
    /**
     * @param File $file
     * @return $this
     */
    public function setAvatar(File $file);
    /**
     * @return $this
     */
    public function removeAvatar();
}