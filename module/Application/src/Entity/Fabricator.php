<?php
namespace Application\Entity;

use Application\Entity\Interfaces\ModeratedInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use ModuleFileManager\Entity\File;

/**
 * Class Fabricator
 * @package Application\Entity
 *
 * @ORM\Table(name="fabricator")
 * @ORM\Entity();
 */
class Fabricator implements ModeratedInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="inn", type="string", length=12, nullable=false)
     */
    private $inn;

    /**
     * @var string
     *
     * @ORM\Column(name="kpp", type="string", length=9, nullable=false)
     */
    private $kpp;

    /**
     * @var string
     *
     * @ORM\Column(name="legal_address", type="string", length=255, nullable=false)
     */
    private $legalAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * One Fabricator has One User.
     * @ORM\OneToOne(targetEntity="Application\Entity\User", inversedBy="fabricator")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * One Fabricator has Many Files.
     * @ORM\ManyToMany(targetEntity="ModuleFileManager\Entity\File")
     * @ORM\JoinTable(name="fabricators_files",
     *      joinColumns={@ORM\JoinColumn(name="fabricator_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $files;

    /**
     * Many Fabricators have Many Categories.
     * @ORM\ManyToMany(targetEntity="Category")
     * @ORM\JoinTable(name="fabricators_categories",
     *      joinColumns={@ORM\JoinColumn(name="fabricator_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     *      )
     */
    private $categories;

    /**
     * Many Fabricators has Many Moderations.
     * @ORM\ManyToMany(targetEntity="Application\Entity\Moderation")
     * @ORM\JoinTable(name="fabricators_moderations",
     *      joinColumns={@ORM\JoinColumn(name="fabricator_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="moderation_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $moderations;

    /**
     * Fabricator constructor.
     */
    public function __construct()
    {
        $this->categories   = new ArrayCollection();
        $this->files        = new ArrayCollection();
        $this->moderations  = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getInn(): string
    {
        return $this->inn;
    }

    /**
     * @param string $inn
     */
    public function setInn(string $inn)
    {
        $this->inn = $inn;
    }

    /**
     * @return string
     */
    public function getKpp(): string
    {
        return $this->kpp;
    }

    /**
     * @param string $kpp
     */
    public function setKpp(string $kpp)
    {
        $this->kpp = $kpp;
    }

    /**
     * @return string
     */
    public function getLegalAddress(): string
    {
        return $this->legalAddress;
    }

    /**
     * @param string $legalAddress
     */
    public function setLegalAddress(string $legalAddress)
    {
        $this->legalAddress = $legalAddress;
    }

    /**
     * @return string|null
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string|null $slug
     */
    public function setSlug(string $slug = null)
    {
        $this->slug = $slug;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param mixed $files
     */
    public function setFiles($files)
    {
        $this->files = $files;
    }

    /**
     * @param File $file
     */
    public function addFile(File $file)
    {
        $this->files->add($file);
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param mixed $categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    /**
     * @param Category $category
     */
    public function addCategory(Category $category)
    {
        $this->categories->add($category);
    }

    /**
     * @inheritdoc
     */
    public function getModerations()
    {
        return $this->moderations;
    }

    /**
     * @inheritdoc
     */
    public function setModerations($moderations)
    {
        $this->moderations = $moderations;
    }

    /**
     * @inheritdoc
     */
    public function addModeration(Moderation $moderation)
    {
        $this->moderations->add($moderation);
    }
}

