<?php
namespace Application\Entity;

use Application\Entity\Interfaces\ApplicationUserInterface;
use Core\Entity\AbstractUser;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use ModuleAuthV2\Entity\Interfaces\AuthUserInterface;
use ModuleFileManager\Entity\File;
use ModuleCode\Entity\Code;

/**
 * Class User
 * @package Application\Entity
 *
 * @ORM\Table(name="user", indexes={@ORM\Index(name="fk_user_phone1_idx", columns={"phone_id"}), @ORM\Index(name="fk_user_email1_idx", columns={"email_id"})})
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\UserRepository");
 */
class User extends AbstractUser implements ApplicationUserInterface, AuthUserInterface
{
    const PATH_AVATAR_FILES = '/avatar';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128, nullable=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="ModuleCode\Entity\Code", mappedBy="user")
     */
    private $codes;

    /**
     * @ORM\ManyToMany(targetEntity="ModuleFileManager\Entity\File", fetch="EAGER")
     * @ORM\JoinTable(name="user_avatar_file",
     *   joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id")}
     * )
     */
    private $avatar;

    /**
     * @ORM\OneToMany(targetEntity="Application\Entity\Product", mappedBy="user")
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity="Application\Entity\Purchase", mappedBy="user")
     */
    private $purchases;

    /**
     * One Use has One Fabricator.
     * @ORM\OneToOne(targetEntity="Application\Entity\Fabricator", mappedBy="user")
     */
    private $fabricator;

    /**
     * One Use has One Shop.
     * @ORM\OneToOne(targetEntity="Application\Entity\Shop", mappedBy="user")
     */
    private $shop;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->codes    = new ArrayCollection();
        $this->avatar   = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->purchases = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCodes()
    {
        return $this->codes;
    }

    /**
     * @param mixed $codes
     */
    public function setCodes($codes)
    {
        $this->codes = $codes;
    }

    /**
     * Add code
     *
     * @param Code $code
     *
     * @return User
     */
    public function addCode(Code $code)
    {
        $this->codes[] = $code;

        return $this;
    }

    /**
     * Remove code
     *
     * @param Code $code
     */
    public function removeCode(Code $code)
    {
        $this->codes->removeElement($code);
    }

    /**
     * @return bool
     */
    public function getIsBanned()
    {
        return $this->isBanned;
    }

    /**
     * @param bool $isBanned
     */
    public function setIsBanned($isBanned)
    {
        $this->isBanned = $isBanned;
    }

    /**
     * @return File
     */
    public function getAvatar()
    {
        return $this->avatar->last();
    }

    /**
     * @return bool
     */
    public function existAvatar()
    {
        return $this->getAvatar() !== false;
    }

    /**
     * @param File $file
     * @return $this
     */
    public function setAvatar(File $file)
    {
        if (!$this->avatar->contains($file)) {
            $this->avatar->add($file);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeAvatar()
    {
        foreach ($this->avatar as $file) {
            $this->avatar->removeElement($file);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param Collection $products
     */
    public function setProducts(Collection $products)
    {
        $this->products = $products;
    }

    /**
     * @param Product $product
     */
    public function addProduct(Product $product)
    {
        $this->products->add($product);
    }

    /**
     * @return mixed
     */
    public function getFabricator()
    {
        return $this->fabricator;
    }

    /**
     * @param mixed $fabricator
     */
    public function setFabricator($fabricator)
    {
        $this->fabricator = $fabricator;
    }

    /**
     * @return mixed
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param mixed $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return mixed
     */
    public function getPurchases()
    {
        return $this->purchases;
    }

    /**
     * @param mixed $purchases
     */
    public function setPurchases($purchases)
    {
        $this->purchases = $purchases;
    }

    /**
     * @param Purchase $purchase
     */
    public function addPurchase(Purchase $purchase)
    {
        $this->purchases->add($purchase);
    }
    /**
     * @return string|null
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string|null $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }
}

