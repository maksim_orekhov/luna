<?php
namespace Application;

use Application\Entity\Category;
use Application\Service\CategoryManager;
use Zend\Mvc\MvcEvent;
use Zend\Router\Http\Segment;
use Zend\Router\Http\TreeRouteStack;

/**
 * Class Module
 * @package Application
 */
class Module
{
    const VERSION = '3.0.3-dev';

    public function getConfig(): array
    {
        return array_replace_recursive(
            require __DIR__ . '/../config/module.config.php',
            require __DIR__ . '/../config/router.config.php',
            require __DIR__ . '/../config/api.router.config.php',
            require __DIR__ . '/../config/assets.config.php',
            require __DIR__ . '/../config/doctrine.config.php',
            require __DIR__ . '/../config/controllers.config.php',
            require __DIR__ . '/../config/service.config.php',
            require __DIR__ . '/../config/rbac.config.php',
            require __DIR__ . '/../config/views.config.php'
        );
    }

    /**
     * @param MvcEvent $event
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function onBootstrap(MvcEvent $event)
    {
        $application = $event->getApplication();
        // Get service manager
        $serviceManager = $application->getServiceManager();
        //Set session
        $this->setInitiatorVarToSession($serviceManager);
    }

    /**
     * @param $serviceManager
     * @throws \Exception
     */
    public function setInitiatorVarToSession($serviceManager)
    {
        try {
            $sessionContainer = $serviceManager->get('ContainerNamespace');
            // Check if not exists and set var initiator in session
            if (!$sessionContainer->initiator) {

                $sessionContainer->initiator = array();
            }
        }
        catch (\Throwable $t) {
        }
    }
}
