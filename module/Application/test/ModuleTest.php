<?php

namespace ApplicationTest;

use Application\Module;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;

class ModuleTest extends AbstractHttpControllerTestCase
{
    const CONFIG_FILES_NAMES = [
        'module.config.php',
        'router.config.php',
        'assets.config.php',
        'doctrine.config.php',
        'controllers.config.php',
        'service.config.php',
        'views.config.php'
    ];

    private $moduleRoot = null;

    protected function setUp()
    {
        $this->moduleRoot = realpath(__DIR__ . '/../');

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../config/application.config.php',
            include __DIR__ . '/../../../config/autoload/global.php'
        ));
    }

    /**
     * @group module
     */
    public function testGetConfig()
    {
        $expectedConfig = [];
        foreach(self::CONFIG_FILES_NAMES as $file_name) {
            /** @noinspection PhpIncludeInspection */
            $config = include $this->moduleRoot . '/config/' . $file_name;
            /** @noinspection SlowArrayOperationsInLoopInspection */
            $expectedConfig = array_replace_recursive($expectedConfig, $config);
        }

        $module = new Module();
        $configData = $module->getConfig();

        $this->assertEquals($expectedConfig, $configData);
    }
}