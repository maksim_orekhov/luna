<?php
namespace Application;

use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'controllers' => [
        'factories' => [
            Controller\TestController::class => Controller\Factory\TestControllerFactory::class,
            Controller\LandingController::class => Controller\Factory\LandingControllerFactory::class,
            Controller\ProfileController::class => Controller\Factory\ProfileControllerFactory::class,
            Controller\FabricatorController::class => Controller\Factory\FabricatorControllerFactory::class,
            Controller\Shops\ShopController::class => Controller\Shops\Factory\ShopControllerFactory::class,
            Controller\FeedbackController::class => InvokableFactory::class,
            // Products
            Controller\Products\ProductController::class => Controller\Products\Factory\ProductControllerFactory::class,
            Controller\Products\ProductImageController::class => Controller\Products\Factory\ProductImageControllerFactory::class,
            Controller\Products\PublicProductController::class => Controller\Products\Factory\PublicProductControllerFactory::class,
            Controller\Products\ProductModerationController::class => Controller\Products\Factory\ProductModerationControllerFactory::class,
            Controller\Products\ProductArchiveController::class => Controller\Products\Factory\ProductArchiveControllerFactory::class,
            // Purchase
            Controller\Purchases\PublicPurchaseController::class => Controller\Purchases\Factory\PublicPurchaseControllerFactory::class,
            Controller\Purchases\PurchaseController::class => Controller\Purchases\Factory\PurchaseControllerFactory::class,
            // Shops
            Controller\Shops\PublicShopController::class => Controller\Shops\Factory\PublicShopControllerFactory::class,
        ],
    ],
];
