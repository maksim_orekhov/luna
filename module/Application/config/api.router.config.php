<?php
namespace Application;

use Application\View\Helper\UrlHelper;
use Zend\Router\Http\Literal;

return [
    'router' => [
        'routes' => [
            // **** Api route **** //
            UrlHelper::ROUTE_PRODUCT_SEARCH => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/product/search',
                    'defaults' => [
                        'controller' => Controller\Products\ProductController::class,
                        'action' => 'productSearch',
                        UrlHelper::CATALOG_KEY => UrlHelper::CATALOG_PRODUCT,
                    ]
                ],
            ],
            UrlHelper::ROUTE_PURCHASE_SEARCH => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/purchase/search',
                    'defaults' => [
                        'controller' => Controller\Purchases\PurchaseController::class,
                        'action' => 'purchaseSearch',
                        UrlHelper::CATALOG_KEY => UrlHelper::CATALOG_PURCHASE,
                    ]
                ],
            ],
            UrlHelper::ROUTE_SHOP_SEARCH => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/shops/search',
                    'defaults' => [
                        'controller' => Controller\Shops\ShopController::class,
                        'action' => 'shopSearch',
                        UrlHelper::CATALOG_KEY => UrlHelper::CATALOG_SHOP,
                    ]
                ],
            ],
            // **** End api route **** //
        ]
    ],
];
