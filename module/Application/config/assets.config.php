<?php
namespace Application;

return [
    // The 'access_filter' key is used by the Application module to restrict or permit
    // access to certain controller actions for unauthorized visitors.
    'access_filter' => [
        'options' => [
            'mode' => 'permissive',     // все, что не запрещено, разрешено
            //'mode' => 'restrictive',  // все, что не разрешено, запрещено
        ],
        'controllers' => [
            Controller\ProfileController::class => [
                [
                    'actions' => [
                        'profile',
                        'purchase',
                    ],
                    'allows' => ['@']
                ],
            ],
            Controller\Purchases\PurchaseController::class => [
                [
                    'actions' => [
                        'get',
                        'createPurchase',
                        'editPurchase',
                        'edit',
                        'create',
                        'stopPublish',
                        'startPublish',
                    ],
                    'allows' => ['#Verified'],
                ],
            ],
            Controller\Products\ProductController::class => [
                [
                    'actions' => [
                        'get',
                        'createProduct',
                        'editProduct',
                        'edit',
                        'create',
                        'getInactiveList',
                        'stopPublish',
                        'startPublish'
                    ],
                    'allows' => ['#Verified'],
                ],
            ],
            Controller\Products\ProductModerationController::class => [
                [
                    'actions' => [
                        'getWaitingModerationList',
                        'getDeclinedList',
                        'get',
                        'update',
                        'editForm',
                        'formInit',
                    ],
                    'allows' => ['#Operator'],
                ],
            ],
            Controller\Products\ProductArchiveController::class => [
                [
                    'actions' => [
                        'getList',
                        'archive'
                    ],
                    'allows' => ['#Verified'],
                ]
            ],
            Controller\FabricatorController::class => [
                [
                    'actions' => [
                        'fabricatorCustomCreate',
                    ],
                    'allows' => ['#Operator'],
                ]
            ],
            Controller\Shops\ShopController::class => [
                [
                    'actions' => [
                        'shopCustomCreate',
                    ],
                    'allows' => ['#Operator'],
                ]
            ],
        ]
    ],
];
