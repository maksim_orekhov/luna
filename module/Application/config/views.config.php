<?php
namespace Application;

use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'view_manager' => [
        'display_not_found_reason' => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            // layout
            'twig/layout/landing' => __DIR__ . '/../viewTwig/layout/landing.twig',
            'twig/layout/application' => __DIR__ . '/../viewTwig/layout/application.twig',
            'twig/layout/authentication' => __DIR__ . '/../viewTwig/layout/authentication.twig',
            'twig/layout/error-layout' => __DIR__ . '/../viewTwig/layout/error-layout.twig',
            // error
            'twig/error/404' => __DIR__ . '/../viewTwig/error/404.twig',
            'twig/error/500' => __DIR__ . '/../viewTwig/error/500.twig',
            // message
            'twig/message/error' => __DIR__ . '/../viewTwig/message/error.twig',
            'twig/message/success' => __DIR__ . '/../viewTwig/message/success.twig',
            'twig/message/under-construction' => __DIR__ . '/../viewTwig/message/under-construction.twig',
            // email
            'twig/email_confirm' => __DIR__ . '/../viewTwig/email/email_confirm.twig',
            'twig/password_reset' => __DIR__ . '/../viewTwig/email/password_reset.twig',
            'twig/confirmation_code' => __DIR__ . '/../viewTwig/email/confirmation_code.twig',
            'twig/error_report' => __DIR__ . '/../viewTwig/email/error_report.twig',
            //для теста верстки писем
            'twig/test-email' => __DIR__ . '/../viewTwig/email/TestEmail.twig',
            // common
            'twig/macro/component' => __DIR__ . '/../viewTwig/macro/component.twig',
        ],
        'template_path_stack' => [
            __DIR__ . '/../viewTwig',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'view_helpers' => [
        'factories' => [
            View\Helper\VersionHelper::class => View\Helper\Factory\VersionHelperFactory::class,
            View\Helper\QueryHelper::class => View\Helper\Factory\QueryHelperFactory::class,
            View\Helper\SortHelper::class => View\Helper\Factory\SortHelperFactory::class,
            View\Helper\PhoneHelper::class => InvokableFactory::class,
            View\Helper\DateHelper::class => InvokableFactory::class,
            View\Helper\ViewModeHelper::class => InvokableFactory::class,
            View\Helper\JsonUtilHelper::class => InvokableFactory::class,
            View\Helper\BreadcrumbsHelper::class => View\Helper\Factory\BreadcrumbsHelperFactory::class,
            View\Helper\ProductsHelper::class => View\Helper\Factory\ProductsHelperFactory::class,
            View\Helper\UrlHelper::class => View\Helper\Factory\UrlHelperFactory::class,
        ],
        'aliases' => [
            'versionHelper' => View\Helper\VersionHelper::class,
            'queryHelper' => View\Helper\QueryHelper::class,
            'sortHelper' => View\Helper\SortHelper::class,
            'phoneHelper' => View\Helper\PhoneHelper::class,
            'dateHelper' => View\Helper\DateHelper::class,
            'viewModeHelper' => View\Helper\ViewModeHelper::class,
            'Breadcrumbs' => View\Helper\BreadcrumbsHelper::class,
            'breadcrumbs' => View\Helper\BreadcrumbsHelper::class,
            'ProductsHelper' => View\Helper\ProductsHelper::class,
            'productsHelper' => View\Helper\ProductsHelper::class,
            'JsonUtilHelper' => View\Helper\JsonUtilHelper::class,
            'JsonUtil' => View\Helper\JsonUtilHelper::class,
            'jsonUtil' => View\Helper\JsonUtilHelper::class,
            'json_util' => View\Helper\JsonUtilHelper::class,
            'UrlHelper' => View\Helper\UrlHelper::class,
            'urlHelper' => View\Helper\UrlHelper::class,
            'url_helper' => View\Helper\UrlHelper::class,
        ],
    ],
];
