<?php
namespace Application;

use Application\Entity\Category;
use Application\Entity\City;
use Application\Entity\Shop;
use Application\Route\DynamicRoute;
use Application\View\Helper\UrlHelper;

return [
    'router' => [
        'routes' => [
            /// *** shops dynamic route *** ///
            UrlHelper::ROUTE_SHOP_LIST => [
                'type' => DynamicRoute::class,
                'priority' => 0,
                'options' => [
                    'route' => '/'.UrlHelper::CATALOG_SHOPS.'/:city_slug[/:category_slug][/:sub_category_slug]',
                    'constraints' => [
                        'city_all' => 'all',
                        'city_slug' => '[\w0-9_-]+',
                        'category_slug' => '[\w0-9_-]+',
                        'sub_category_slug' => '[\w0-9_-]+',
                    ],
                    'database' => [
                        'city_slug' => [
                            'entity' => City::class,
                            'field' => 'slug'
                        ],
                        'category_slug' => [
                            'entity' => Category::class,
                            'field' => 'slug'
                        ],
                        'sub_category_slug' => [
                            'entity' => Category::class,
                            'field' => 'slug'
                        ],
                    ],
                    'defaults' => [
                        'controller' => Controller\Shops\PublicShopController::class,
                        'action'     => 'shopList',
                        UrlHelper::CATALOG_KEY => UrlHelper::CATALOG_SHOP,
                    ],
                ],
            ],
            UrlHelper::ROUTE_SHOP_SINGLE => [
                'type' => DynamicRoute::class,
                'priority' => 0,
                'options' => [
                    'route' => '/'.UrlHelper::CATALOG_SHOP.'/:shop_slug/:city_slug[/:category_slug][/:sub_category_slug]',
                    'constraints' => [
                        'shop_slug' => '[\w0-9_-]+',
                        'city_all' => 'all',
                        'city_slug' => '[\w0-9_-]+',
                        'category_slug' => '[\w0-9_-]+',
                        'sub_category_slug' => '[\w0-9_-]+',
                    ],
                    'database' => [
                        'shop_slug' => [
                            'entity' => Shop::class,
                            'field' => 'slug'
                        ],
                        'city_slug' => [
                            'entity' => City::class,
                            'field' => 'slug'
                        ],
                        'category_slug' => [
                            'entity' => Category::class,
                            'field' => 'slug'
                        ],
                        'sub_category_slug' => [
                            'entity' => Category::class,
                            'field' => 'slug'
                        ],
                    ],
                    'defaults' => [
                        'controller' => Controller\Shops\PublicShopController::class,
                        'action' => 'shopSingle',
                        UrlHelper::CATALOG_KEY => UrlHelper::CATALOG_SHOP,
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => []
            ],
            /// end purchase dynamic route ///
        ]
    ],
];
