<?php
namespace Application;

use Application\Entity\Product;
use Application\Entity\Category;
use Application\Entity\City;
use Application\Route\DynamicRoute;
use Application\View\Helper\UrlHelper;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Regex;
use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            ////////////////////////////////////
            ///          PUBLIC ROUTE        ///
            ////////////////////////////////////
            /// *** product dynamic route *** ///
            UrlHelper::ROUTE_PRODUCT_LIST => [
                'type' => DynamicRoute::class,
                'priority' => 0,
                'options' => [
                    'route' => '/'.UrlHelper::CATALOG_PRODUCT.'/:city_slug[/:category_slug][/:sub_category_slug]',
                    'constraints' => [
                        'city_slug' => '[\w0-9_-]+',
                        'category_slug' => '[\w0-9_-]+',
                        'sub_category_slug' => '[\w0-9_-]+',
                    ],
                    'database' => [
                        'city_slug' => [
                            'entity' => City::class,
                            'field' => 'slug'
                        ],
                        'category_slug' => [
                            'entity' => Category::class,
                            'field' => 'slug'
                        ],
                        'sub_category_slug' => [
                            'entity' => Category::class,
                            'field' => 'slug'
                        ],
                    ],
                    'defaults' => [
                        'controller' => Controller\Products\PublicProductController::class,
                        'action'     => 'productList',
                        UrlHelper::CATALOG_KEY => UrlHelper::CATALOG_PRODUCT,
                    ],
                ],
            ],
            UrlHelper::ROUTE_PRODUCT_SINGLE => [
                'type' => DynamicRoute::class,
                'priority' => 0,
                'options' => [
                    'route' => '/'.UrlHelper::CATALOG_PRODUCT.'/:city_slug/:category_slug/:sub_category_slug/:product_slug',
                    'constraints' => [
                        'city_slug' => '[\w0-9_-]+',
                        'category_slug' => '[\w0-9_-]+',
                        'sub_category_slug' => '[\w0-9_-]+',
                        'product_slug' => '[\w0-9_-]+',
                    ],
                    'database' => [
                        'city_slug' => [
                            'entity' => City::class,
                            'field' => 'slug'
                        ],
                        'category_slug' => [
                            'entity' => Category::class,
                            'field' => 'slug'
                        ],
                        'sub_category_slug' => [
                            'entity' => Category::class,
                            'field' => 'slug'
                        ],
                        'product_slug' => [
                            'entity' => Product::class,
                            'field' => 'slug'
                        ],
                    ],
                    'defaults' => [
                        'controller' => Controller\Products\PublicProductController::class,
                        'action' => 'productSingle',
                        UrlHelper::CATALOG_KEY => UrlHelper::CATALOG_PRODUCT,
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => []
            ],
            /// end product dynamic route ///
            'user-product' => [
                'type' => Regex::class,
                'options' => [
                    'regex'    => '/(?<code>[a-zA-Z0-9\._-]+)/products',
                    'defaults' => [
                        'controller' => Controller\Products\PublicProductController::class,
                        'action' => 'userProductList',
                        UrlHelper::CATALOG_KEY => UrlHelper::CATALOG_PRODUCT,
                    ],
                    'spec' => '/%code%/products',
                ],
                'may_terminate' => true,
                'child_routes' => []
            ],
            ////////////////////////////////////
            ///        END PUBLIC ROUTE      ///
            ////////////////////////////////////
            //// create and edit ////
            'product' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/product',
                    'defaults' => [
                        'controller' => Controller\Products\ProductController::class,
                    ],
                ],
            ],
            'product-single' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/product/:id',
                    'constraints' => [
                        'id'     => '\d+'
                    ],
                    'defaults' => [
                        'controller' => Controller\Products\ProductController::class,
                    ],
                ],
            ],
            'product-create' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/product/create',
                    'defaults' => [
                        'controller' => Controller\Products\ProductController::class,
                        'action' => 'createProduct',
                    ],
                ],
            ],
            'product-edit' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/product/:idProduct/edit',
                    'constraints' => [
                        'idProduct' => '\d+'
                    ],
                    'defaults' => [
                        'controller' => Controller\Products\ProductController::class,
                        'action' => 'editProduct',
                    ],
                ],
            ],
            //// end create and edit ////
            'product-image-show' => [
                'type' => Regex::class,
                'options' => [
                    'regex'    => '/img/(?<image>[1-9][0-9]*?)\.(?<extension>[a-zA-Z]+)',
                    'defaults' => [
                        'controller' => Controller\Products\ProductImageController::class,
                        'action' => 'productImageShow',
                    ],
                    'spec' => '/img/%image%.%extension%',
                ],
            ],
            'product-image-upload' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/product/image/upload',
                    'defaults' => [
                        'controller' => Controller\Products\ProductImageController::class,
                        'action' => 'imageUpload',
                    ]
                ],
            ],
            //
            // Product
            //
            'published-products' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/published-products',
                    'defaults' => [
                        'controller' => Controller\Products\ProductController::class,
                        'action' => 'getPublishedList'
                    ],
                ]
            ],
            'product-inactive' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/product/inactive',
                    'defaults' => [
                        'controller' => Controller\Products\ProductController::class,
                        'action' => 'getInactiveList'
                    ],
                ]
            ],
            'product-stop-publish' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/product/:idProduct/stop-publish',
                    'constraints' => [
                        'idProduct' => '\d+',
                    ],
                    'defaults' => [
                        'controller' => Controller\Products\ProductController::class,
                        'action' => 'stopPublish'
                    ],
                ]
            ],
            'product-start-publish' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/product/:idProduct/start-publish',
                    'constraints' => [
                        'idProduct' => '\d+',
                    ],
                    'defaults' => [
                        'controller' => Controller\Products\ProductController::class,
                        'action' => 'startPublish'
                    ],
                ]
            ],
            //
            // Waiting Moderation Product
            //
            'product-waiting-moderation' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/product/moderation/waiting',
                    'defaults' => [
                        'controller' => Controller\Products\ProductModerationController::class,
                        'action' => 'getWaitingModerationList'
                    ],
                ]
            ],
            'product-declined' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/product/moderation/declined',
                    'defaults' => [
                        'controller' => Controller\Products\ProductModerationController::class,
                        'action' => 'getDeclinedList'
                    ],
                ]
            ],
            'product-moderation-single' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/product/moderation/:id',
                    'constraints' => [
                        'id'     => '\d+'
                    ],
                    'defaults' => [
                        'controller' => Controller\Products\ProductModerationController::class,
                    ],
                ],
            ],
            'product-moderation-form-init' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/product/moderation/form-init',
                    'defaults' => [
                        'controller' => Controller\Products\ProductModerationController::class,
                        'action' => 'formInit',
                    ],
                    'strategies' => [
                        'ViewJsonStrategy',
                    ],
                ]
            ],
            //
            // Archival Product
            //
            'product-archive' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/product/archive',
                    'defaults' => [
                        'controller' => Controller\Products\ProductArchiveController::class,
                    ],
                ]
            ],
            'product-archiving' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/product/:idProduct/archive', // @TODO получать id из POST формы
                    'constraints' => [
                        'idProduct' => '\d+',
                    ],
                    'defaults' => [
                        'controller' => Controller\Products\ProductArchiveController::class,
                        'action' => 'archive'
                    ],
                ]
            ],
            'product-unarchiving' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/product/:idProduct/unarchive', // @TODO получать id из POST формы
                    'constraints' => [
                        'idProduct' => '\d+',
                    ],
                    'defaults' => [
                        'controller' => Controller\Products\ProductArchiveController::class,
                        'action' => 'unarchive'
                    ],
                ]
            ],
        ]
    ],
];
