<?php
namespace Application;

use Application\Entity\Category;
use Application\Entity\City;
use Application\Entity\Purchase;
use Application\Route\DynamicRoute;
use Application\View\Helper\UrlHelper;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Regex;
use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            /// *** purchase dynamic route *** ///
            UrlHelper::ROUTE_PURCHASE_LIST => [
                'type' => DynamicRoute::class,
                'priority' => 0,
                'options' => [
                    'route' => '/'.UrlHelper::CATALOG_PURCHASE.'/:city_slug[/:category_slug][/:sub_category_slug]',
                    'constraints' => [
                        'city_slug' => '[\w0-9_-]+',
                        'category_slug' => '[\w0-9_-]+',
                        'sub_category_slug' => '[\w0-9_-]+',
                    ],
                    'database' => [
                        'city_slug' => [
                            'entity' => City::class,
                            'field' => 'slug'
                        ],
                        'category_slug' => [
                            'entity' => Category::class,
                            'field' => 'slug'
                        ],
                        'sub_category_slug' => [
                            'entity' => Category::class,
                            'field' => 'slug'
                        ],
                    ],
                    'defaults' => [
                        'controller' => Controller\Purchases\PublicPurchaseController::class,
                        'action'     => 'purchaseList',
                        UrlHelper::CATALOG_KEY => UrlHelper::CATALOG_PURCHASE,
                    ],
                ],
            ],
            UrlHelper::ROUTE_PURCHASE_SINGLE => [
                'type' => DynamicRoute::class,
                'priority' => 0,
                'options' => [
                    'route' => '/'.UrlHelper::CATALOG_PURCHASE.'/:city_slug/:category_slug/:sub_category_slug/:purchase_slug',
                    'constraints' => [
                        'city_slug' => '[\w0-9_-]+',
                        'category_slug' => '[\w0-9_-]+',
                        'sub_category_slug' => '[\w0-9_-]+',
                        'purchase_slug' => '[\w0-9_-]+',
                    ],
                    'database' => [
                        'city_slug' => [
                            'entity' => City::class,
                            'field' => 'slug'
                        ],
                        'category_slug' => [
                            'entity' => Category::class,
                            'field' => 'slug'
                        ],
                        'sub_category_slug' => [
                            'entity' => Category::class,
                            'field' => 'slug'
                        ],
                        'product_slug' => [
                            'entity' => Purchase::class,
                            'field' => 'slug'
                        ],
                    ],
                    'defaults' => [
                        'controller' => Controller\Purchases\PublicPurchaseController::class,
                        'action' => 'purchaseSingle',
                        UrlHelper::CATALOG_KEY => UrlHelper::CATALOG_PURCHASE,
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => []
            ],
            /// end purchase dynamic route ///
            'user-purchase' => [
                'type' => Regex::class,
                'options' => [
                    'regex'    => '/(?<code>[a-zA-Z0-9\._-]+)/purchases',
                    'defaults' => [
                        'controller' => Controller\Purchases\PublicPurchaseController::class,
                        'action' => 'userPurchaseList',
                        UrlHelper::CATALOG_KEY => UrlHelper::CATALOG_PURCHASE,
                    ],
                    'spec' => '/%code%/purchase',
                ],
                'may_terminate' => true,
                'child_routes' => []
            ],
            'purchase'  => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/purchase',
                    'defaults' => [
                        'controller' => Controller\Purchases\PurchaseController::class,
                    ],
                ],
            ],
            'purchase-single' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/purchase/:id',
                    'constraints' => [
                        'id'     => '\d+'
                    ],
                    'defaults' => [
                        'controller' => Controller\Purchases\PurchaseController::class,
                    ],
                ],
            ],
            'purchase-create'  => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/purchase/create',
                    'defaults' => [
                        'controller' => Controller\Purchases\PurchaseController::class,
                        'action' => 'createPurchase',
                    ],
                ],
            ],
            'purchase-edit' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/purchase/:idPurchase/edit',
                    'constraints' => [
                        'idPurchase' => '\d+'
                    ],
                    'defaults' => [
                        'controller' => Controller\Purchases\PurchaseController::class,
                        'action' => 'editPurchase',
                    ],
                ],
            ],

            'purchase-stop-publish' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/purchase/:idPurchase/stop-publish',
                    'constraints' => [
                        'idPurchase' => '\d+',
                    ],
                    'defaults' => [
                        'controller' => Controller\Purchases\PurchaseController::class,
                        'action' => 'stopPublish'
                    ],
                ]
            ],
            'purchase-start-publish' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/purchase/:idPurchase/start-publish',
                    'constraints' => [
                        'idPurchase' => '\d+',
                    ],
                    'defaults' => [
                        'controller' => Controller\Purchases\PurchaseController::class,
                        'action' => 'startPublish'
                    ],
                ]
            ],
        ]
    ],
];
