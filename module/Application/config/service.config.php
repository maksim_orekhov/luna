<?php
namespace Application;

use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'service_manager' => [
        'factories' => [
            // Listeners
            Listener\AuthListenerAggregate::class => Listener\Factory\AuthListenerAggregateFactory::class,
            Listener\ProductListenerAggregate::class => Listener\Factory\ProductListenerAggregateFactory::class,
            // Services
            Service\UserManager::class => Service\Factory\UserManagerFactory::class,
            Service\ProductManager::class => Service\Factory\ProductManagerFactory::class,
            Service\ModerationManager::class => Service\Factory\ModerationManagerFactory::class,
            Service\CategoryManager::class => Service\Factory\CategoryManagerFactory::class,
            Service\AddressManager::class => Service\Factory\AddressManagerFactory::class,
            Service\CityManager::class => Service\Factory\CityManagerFactory::class,
            Service\RouteManager::class => Service\Factory\RouteManagerFactory::class,
            Service\FabricatorManager::class => Service\Factory\FabricatorManagerFactory::class,
            Service\ShopManager::class => Service\Factory\ShopManagerFactory::class,
            Service\ProfileManager::class => InvokableFactory::class,
            Service\ProductModerationManager::class => Service\Factory\ProductModerationManagerFactory::class,
            Service\ProductArchiveManager::class => Service\Factory\ProductArchiveManagerFactory::class,
            Service\PurchaseManager::class => Service\Factory\PurchaseManagerFactory::class,
            Service\PurchaseModerationManager::class => Service\Factory\PurchaseModerationManagerFactory::class,
            // EventManagers
            EventManager\ProductEventProvider::class => InvokableFactory::class,
            // Rbac
            Service\Rbac\RbacProfileAssertionManager::class => InvokableFactory::class,
            Service\Rbac\RbacProductAssertionManager::class => InvokableFactory::class,
            Service\Rbac\RbacPurchaseAssertionManager::class => InvokableFactory::class,
            //route
            Route\DynamicRoute::class => Route\Factory\DynamicRouteFactory::class,
            // Output
            Service\Output\ProductOutput::class => Service\Output\Factory\ProductOutputFactory::class,
            Service\Output\PurchaseOutput::class => InvokableFactory::class,
            // Status
            Service\Status\ProductStatus::class => Service\Status\Factory\ProductStatusFactory::class,

        ],
        'delegators' => [
//            RouteStackInterface::class => [ ProductRouterDelegator::class ],
        ],
    ],
    'route_manager' => [
        'factories' => [
            Route\DynamicRoute::class => Route\Factory\DynamicRouteFactory::class
        ]
    ]
];
