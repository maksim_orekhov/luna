<?php
namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;

$routes = [
    'router' => [
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/',
                    'defaults' => [
                        'controller' => Controller\LandingController::class,
                        'action' => 'landing',
                    ],
                ],
            ],
            'profile' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/profile',
                    'defaults' => [
                        'controller' => Controller\ProfileController::class,
                        'action' => 'profile',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'purchase' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/purchase',
                            'defaults' => [
                                'controller' => Controller\ProfileController::class,
                                'action' => 'purchase',
                            ],
                            'strategies' => [
                                'ViewJsonStrategy',
                            ],
                        ]
                    ],
                    'form-init' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/form-init',
                            'defaults' => [
                                'controller' => Controller\ProfileController::class,
                                'action' => 'formInit',
                            ],
                            'strategies' => [
                                'ViewJsonStrategy',
                            ],
                        ]
                    ],
                    'avatar' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/avatar/:fileId',
                            'constraints' => [
                                'fileId' => '[1-9][0-9]*',
                            ],
                            'defaults' => [
                                'controller' => Controller\ProfileController::class,
                                'action' => 'showAvatar',
                            ],
                        ],
                    ],
                ]
            ],
            'complain-request' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/complain-request',
                    'defaults' => [
                        'controller' => Controller\FeedbackController::class,
                        'action' => 'complainRequest',
                    ],
                ],
                'strategies' => [
                    'ViewJsonStrategy',
                ],
            ],
            'safe-deal-request' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/safe-deal-request',
                    'defaults' => [
                        'controller' => Controller\FeedbackController::class,
                        'action' => 'safeDealRequest',
                    ],
                ],
                'strategies' => [
                    'ViewJsonStrategy',
                ],
            ],
            'fabricator-request' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/fabricator-request',
                    'defaults' => [
                        'controller' => Controller\FeedbackController::class,
                        'action' => 'fabricatorRequest',
                    ],
                ],
                'strategies' => [
                    'ViewJsonStrategy',
                ],
            ],
            'shop-request' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shop-request',
                    'defaults' => [
                        'controller' => Controller\FeedbackController::class,
                        'action' => 'shopRequest',
                    ],
                ],
                'strategies' => [
                    'ViewJsonStrategy',
                ],
            ],
            'fabricator-custom-create' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/fabricator-custom-create',
                    'defaults' => [
                        'controller' => Controller\FabricatorController::class,
                        'action' => 'fabricatorCustomCreate',
                    ],
                ],
                'strategies' => [
                    'ViewJsonStrategy',
                ],
            ],
            'shop-custom-create' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shop-custom-create',
                    'defaults' => [
                        'controller' => Controller\Shops\ShopController::class,
                        'action' => 'shopCustomCreate',
                    ],
                ],
                'strategies' => [
                    'ViewJsonStrategy',
                ],
            ],
            //
            // Test
            //
            'test' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/test',
                    'defaults' => [
                        'controller' => Controller\TestController::class,
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                /* ---===============  Child for file ===============--- */
                'child_routes' => [
                    'mail-template' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/mail-template',
                            'defaults' => [
                                'controller' => Controller\TestController::class,
                                'action' => 'mailTemplate',
                            ],
                        ],
                    ],
                ]
            ],
        ]
    ],
];

return array_replace_recursive(
    $routes,
    require __DIR__ . '/../config/routes/products.routes.php',
    require __DIR__ . '/../config/routes/purchases.routes.php',
    require __DIR__ . '/../config/routes/shops.routes.php'
);
