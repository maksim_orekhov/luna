<?php
namespace Application;

return [
    'rbac_manager' => [
        'assertions' => [
            Service\Rbac\RbacProfileAssertionManager::class,
            Service\Rbac\RbacProductAssertionManager::class,
            Service\Rbac\RbacPurchaseAssertionManager::class,
        ],
    ],
];
