<?php
namespace Application;

use Application\EventManager\ProductEventProvider;
use Application\Listener\ProductListenerAggregate;
use Application\Listener\AuthListenerAggregate;
use Core\EventManager\AuthEventProvider;

return [
    'route_cache_max_age' => 60*60*24*1, //1 день
    // Navigation
    'navigation' => [
        'default' => [
            [
                'label' => 'Home',
                'route' => 'home',
            ]
        ],
    ],
    'event_provider' => [
        //provider class => listeners
        AuthEventProvider::class => [
            //listener class => priority
            AuthListenerAggregate::class => 1,
        ],
        //provider class => listeners
        ProductEventProvider::class => [
            //listener class => priority
            ProductListenerAggregate::class => 1,
        ],
    ],
];
