<?php
namespace ModuleRbac\Controller\Plugin\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use ModuleRbac\Service\RbacManager;
use ModuleRbac\Controller\Plugin\AccessPlugin;

/**
 * This is the factory for AccessPlugin
 */
class AccessPluginFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $rbacManager = $container->get(RbacManager::class);

        return new AccessPlugin($rbacManager);
    }
}

