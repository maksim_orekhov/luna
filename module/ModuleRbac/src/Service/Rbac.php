<?php
namespace ModuleRbac\Service;

use Application\Entity\User;
use Zend\Permissions\Rbac\Rbac as BaseRbac;

class Rbac extends BaseRbac
{
    private $user;

    /**
     * @param User|null $user
     */
    public function addUser(User $user = null)
    {
        $this->user = $user;
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        return $this->user ?? null;
    }
}