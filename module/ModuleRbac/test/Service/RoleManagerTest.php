<?php

namespace ModuleRbac\Service;

use Application\Entity\Role;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use \Exception;

class RoleManagerTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \ModuleRbac\Service\RbacManager
     */
    private $rbacManager;

    /**
     * @var \ModuleRbac\Service\RoleManager
     */
    private $roleManager;

    /**
     * @var \ModuleRbac\Service\PermissionManager
     */
    private $permissionManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        $this->setApplicationConfig(include __DIR__ . '/../../../../config/application.config.php');

        parent::setUp();

        $serviceManager = $this->getApplicationServiceLocator();

        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->roleManager = $serviceManager->get(\ModuleRbac\Service\RoleManager::class);
        $this->permissionManager = $serviceManager->get(\ModuleRbac\Service\PermissionManager::class);
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->rbacManager = $serviceManager->get(\ModuleRbac\Service\RbacManager::class);

        // Перед каждым тестом обновляем кэш
        $this->rbacManager->init(true);
    }

    public function tearDown() {

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
    }

    /**
     * Добавление новой роли
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testAddRole()
    {
        $role_name = 'TEST_ROLE';
        // Test data
        $data = [
            'name'          => $role_name,
            'description'   => 'new test role'
        ];
        // Add new role to DB
        $this->roleManager->addRole($data);
        // Get new permission from DB
        $role = $this->roleManager->getRoleByName($role_name);

        $this->assertEquals($role_name, $role->getName());

        return $role;
    }

    /**
     * Попытка добавить уже существующую роль
     *
     * @depends testAddRole
     * @expectedException Exception
     *
     * @param Role $role
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testAddRoleIfAlreadyExists(Role $role)
    {
        // Test data
        // То же имя и измененное описание
        $data = [
            'name'          => $role->getName(),
            'description'   => $role->getDescription() . " 2"
        ];
        // Add new permission to DB
        $this->roleManager->addRole($data);
    }

    /**
     * Изменение роли
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testUpdateRole()
    {
        $role_name = 'TEST_ROLE';
        $role_new_name = 'TEST_ROLE_UPDATE';

        $role = $this->roleManager->getRoleByName($role_name);

        $data = [
            'name'          => $role_new_name,
            'description'   => $role->getDescription() . " updated"
        ];

        $this->roleManager->updateRole($role, $data);

        $updatedPermission = $this->roleManager->getRoleByName($role_new_name);

        $this->assertEquals($role->getId(), $updatedPermission->getId());
        $this->assertEquals($role_new_name, $updatedPermission->getName());

        return $role_new_name;
    }

    /**
     * Попытка изменить имя существующей роли на имя тоже существующей роли
     *
     * @depends testUpdateRole
     * @expectedException Exception
     *
     * @param $role_new_name
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testUpdateWithNameOfExistentRole($role_new_name)
    {
        $oldRole = $this->roleManager->getRoleByName($role_new_name);

        $new_role_name = 'NEW_TEST_ROLE';

        // Data for new role
        $data = [
            'name'          => $new_role_name,
            'description'   => 'absolutely new test role'
        ];
        // Add new role to DB
        $this->roleManager->addRole($data);

        $newRole = $this->roleManager->getRoleByName($new_role_name);

        $this->assertEquals($new_role_name, $newRole->getName());
        $this->assertNotEquals($oldRole->getId(), $newRole->getId());

        // Data for update oldRole
        $data = [
            'name'          => $new_role_name, // имя совпадает с именем новой, только что добавленной роли
            'description'   => $oldRole->getDescription() . " updated"
        ];

        $this->roleManager->updateRole($oldRole, $data);
    }

    /**
     * Удаление ролей
     *
     * @depends testUpdateRole
     *
     * @param string $role_new_name
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testDeleteRole(string $role_new_name)
    {
        $new_role_name = 'NEW_TEST_ROLE';

        $oldRole = $this->roleManager->getRoleByName($role_new_name);
        $newRole = $this->roleManager->getRoleByName($new_role_name);

        $this->roleManager->deleteRole($oldRole);
        $this->roleManager->deleteRole($newRole);

        // Get permissions from DB
        $deletedOldRole = $this->roleManager->getRoleByName($role_new_name);
        $deletedNewRole = $this->roleManager->getRoleByName($new_role_name);

        $this->assertEquals(null, $deletedOldRole);
        $this->assertEquals(null, $deletedNewRole);
    }

    /**
     * Создаем иерархию ролей (parent - child - grandchild с привилегиями).
     * Получаем и проверяем все привилегии parent роли (с дочерними).
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGetEffectivePermissions()
    {
        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();

        // Array of test role names
        $role_names = [
            'PARENT_ROLE',
            'CHILD_ROLE',
            'GRANDCHILD_ROLE'
        ];
        // Array of test permission names
        $permission_names = [
            'permission.parent',
            'permission.child',
            'permission.grandchild',
        ];

        // Create all roles
        foreach ($role_names as $name) {
            $data = [
                'name'          => $name,
                'description'   => 'Description of ' . $name
            ];
            $this->roleManager->addRole($data);
        }

        // Create all permissions
        foreach ($permission_names as $name) {
            $data = [
                'name'          => $name,
                'description'   => 'Description of ' . $name
            ];
            $this->permissionManager->addPermission($data);
        }

        // Assignment of privileges to roles
        $parentRole = $this->roleManager->getRoleByName('PARENT_ROLE');
        $parentPermission = $this->permissionManager->getPermissionByName('permission.parent');
        $parentRole->addPermission($parentPermission);

        $childRole = $this->roleManager->getRoleByName('CHILD_ROLE');
        $childPermission = $this->permissionManager->getPermissionByName('permission.child');
        $childRole->addPermission($childPermission);

        $grandchildRole = $this->roleManager->getRoleByName('GRANDCHILD_ROLE');
        $grandchildPermission = $this->permissionManager->getPermissionByName('permission.grandchild');
        $grandchildRole->addPermission($grandchildPermission);

        // Get parent role permissions. Must contain only one 'permission.parent'.
        $parentRolePermissions = $this->roleManager->getEffectivePermissions($parentRole);

        // Assignment child roles (hierarchy: $parentRole has $childRole has $grandchildRole)
        $childRole->addChildRole($grandchildRole);
        $parentRole->addChildRole($childRole);

        // Save to DB
        $this->entityManager->flush();

        // Get parent role
        $updatedParentRole = $this->roleManager->getRoleByName('PARENT_ROLE');

        // Get all parent role permissions (with permissions of inherited roles)
        // Must contain all permission from array $permission_names
        $updatedParentRolePermissions = $this->roleManager->getEffectivePermissions($updatedParentRole);

        // Testing
        $this->assertCount(1, $parentRolePermissions);
        $this->assertTrue(in_array('own', $parentRolePermissions));
        $this->assertTrue(array_key_exists('permission.parent', $parentRolePermissions));
        $this->assertCount(3, $updatedParentRolePermissions);
        $this->assertTrue(in_array('own', $updatedParentRolePermissions));
        $this->assertTrue(in_array('inherited', $updatedParentRolePermissions));
        foreach ($permission_names as $name) {
            $this->assertTrue(array_key_exists($name, $updatedParentRolePermissions));
        }

        // Transaction rollback
        $this->entityManager->getConnection()->rollback();
    }
}