<?php

namespace ModuleRbac\Service;

use Application\Entity\Permission;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use \Exception;

class PermissionManagerTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \ModuleRbac\Service\RbacManager
     */
    private $rbacManager;

    /**
     * @var \ModuleRbac\Service\PermissionManager
     */
    private $permissionManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        $this->setApplicationConfig(include __DIR__ . '/../../../../config/application.config.php');

        parent::setUp();

        $serviceManager = $this->getApplicationServiceLocator();

        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->permissionManager = $serviceManager->get(\ModuleRbac\Service\PermissionManager::class);
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->rbacManager = $serviceManager->get(\ModuleRbac\Service\RbacManager::class);

        // Перед каждым тестом обновляем кэш
        $this->rbacManager->init(true);
    }

    protected function tearDown()
    {
        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
    }

    /**
     * Добавление новой привилегии
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testAddPermission()
    {
        $permission_name = 'permission.name';
        // Test data
        $data = [
            'name'          => $permission_name,
            'description'   => 'new test permission'
        ];
        // Add new permission to DB
        $this->permissionManager->addPermission($data);
        // Get new permission from DB
        $permission = $this->permissionManager->getPermissionByName($permission_name);

        $this->assertEquals($permission_name, $permission->getName());

        return $permission;
    }

    /**
     * Попытка добавить уже существующую привилегию
     *
     * @depends testAddPermission
     * @expectedException Exception
     *
     * @param \Application\Entity\Permission $permission
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testAddPermissionIfAlreadyExists(Permission $permission)
    {
        // Test data
        // То же имя и измененное описание
        $data = [
            'name'          => $permission->getName(),
            'description'   => $permission->getDescription() . " 2"
        ];
        // Add new permission to DB
        $this->permissionManager->addPermission($data);
    }

    /**
     * Изменение привилегии
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testUpdatePermission()
    {
        $permission_name = 'permission.name';
        $permission_new_name = 'permission.name.view';

        $permission = $this->permissionManager->getPermissionByName($permission_name);

        $data = [
            'name'          => $permission_new_name,
            'description'   => $permission->getDescription() . " updated"
        ];

        $this->permissionManager->updatePermission($permission, $data);

        $updatedPermission = $this->permissionManager->getPermissionByName($permission_new_name);

        $this->assertEquals($permission->getId(), $updatedPermission->getId());
        $this->assertEquals($permission_new_name, $updatedPermission->getName());

        return $permission_new_name;
    }

    /**
     * Попытка изменить имя существующей привилегии на имя тоже существующей привилегии
     *
     * @depends testUpdatePermission
     * @expectedException Exception
     *
     * @param $permission_new_name
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testUpdateWithNameOfExistentPermission($permission_new_name)
    {
        $oldPermission = $this->permissionManager->getPermissionByName($permission_new_name);

        $new_permission_name = 'new.permission.name';

        // Data for new permission
        $data = [
            'name'          => $new_permission_name,
            'description'   => 'absolutely new test permission'
        ];
        // Add new permission to DB
        $this->permissionManager->addPermission($data);

        $newPermission = $this->permissionManager->getPermissionByName($new_permission_name);

        $this->assertEquals($new_permission_name, $newPermission->getName());
        $this->assertNotEquals($oldPermission->getId(), $newPermission->getId());

        // Data for update oldPermission
        $data = [
            'name'          => $new_permission_name, // имя совпадает с именем новой только что добавленной привилегии
            'description'   => $oldPermission->getDescription() . " updated"
        ];

        $this->permissionManager->updatePermission($oldPermission, $data);
    }

    /**
     * Удаление привилегий
     *
     * @depends testUpdatePermission
     *
     * @param string $permission_new_name
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testDeletePermission(string $permission_new_name)
    {
        $new_permission_name = 'new.permission.name';

        $oldPermission = $this->permissionManager->getPermissionByName($permission_new_name);
        $newPermission = $this->permissionManager->getPermissionByName($new_permission_name);

        $this->permissionManager->deletePermission($oldPermission);
        $this->permissionManager->deletePermission($newPermission);

        // Get permissions from DB
        $deletedOldPermission = $this->permissionManager->getPermissionByName($permission_new_name);
        $deletedNewPermission = $this->permissionManager->getPermissionByName($new_permission_name);

        $this->assertEquals(null, $deletedOldPermission);
        $this->assertEquals(null, $deletedNewPermission);
    }
}