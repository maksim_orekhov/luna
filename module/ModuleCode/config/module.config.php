<?php
namespace ModuleCode;

use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
        ]
    ],
    // The 'access_filter' key is used by the Sms module to restrict or permit
    // access to certain controller actions for unauthorized visitors.
    'access_filter' => [
        'options' => [
            'mode' => 'permissive',     // все, что не запрещено, разрешено
            //'mode' => 'restrictive',  // все, что не разрешено, запрещено
        ],
    ],
    'controllers' => [
        'factories' => [
        ],
    ],
    'controller_plugins' => [
    ],
    'service_manager' => [
        //...
        'factories' => [
            // SMS
            Api\CodeController::class => Api\Factory\CodeControllerFactory::class,
            Service\CodeManager::class => Service\Factory\CodeManagerFactory::class,
            Service\UserSmsInspector::class => Service\Factory\UserSmsInspectorFactory::class,
            Service\UserSmsInspectorStrategy::class => Service\Factory\UserSmsInspectorStrategyFactory::class,
            // Listeners
            Listener\OperationConfirmCodeDemandListener::class =>
                Listener\Factory\OperationConfirmCodeDemandListenerFactory::class,
            Listener\OperationConfirmCodeReceptionListener::class =>
                Listener\Factory\OperationConfirmCodeReceptionListenerFactory::class,
            Service\SmsAllowedNotificationPolitic::class => InvokableFactory::class,
        ],
    ],
    'view_manager' => [
        'template_map' => [

        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/Entity')
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' =>  __NAMESPACE__ . '_driver',
                ],
            ],
        ],
        'fixture' => array(
            __NAMESPACE__ => __DIR__ . '/../src/Fixture',
        )
    ],
    // Sms providers gateways configurations
    'sms_providers' => [
        'prostor-sms' => [
            'gateway_host'   => 'http://api.prostor-sms.ru',
            'login'         => 't89103306544',
            'pass'          => '708306',
            'sender'        => null,
            'wapurl'        => null,
            'port'          => 80 // for sockets
        ],
        'twill' => [
            'login' => '',
            'pass' => '',
        ],
    ],
];
