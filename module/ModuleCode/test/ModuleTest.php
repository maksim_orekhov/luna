<?php

namespace ModuleCodeTest;

use ModuleCode\Module;
use PHPUnit\Framework\TestCase;

class ModuleTest extends TestCase
{
    protected $traceError = true;

    private $moduleRoot = null;

    protected function setUp()
    {
        $this->moduleRoot = realpath(__DIR__ . '/../');
    }

    /**
     * @group module
     */
    public function testGetConfig()
    {
        $expectedConfig = include $this->moduleRoot
            . '/config/module.config.php';

        $module = new Module();
        $configData = $module->getConfig();

        $this->assertEquals($expectedConfig, $configData);
    }
}