<?php
namespace ModuleCodeTest\Provider;

use ModuleCode\Provider\SmsCodeGeneratorTrait;

/**
 * Class SmsCodeGeneratorTraitImpl
 * @package CodeTest\Provider
 */
class SmsCodeGeneratorTraitImpl
{
    use SmsCodeGeneratorTrait;
}