<?php

namespace ModuleCodeTest\Provider;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use ModuleCode\Provider\ProstorSmsProvider;

/**
 * Class ProstorSmsProviderTest
 * @package CodeTest\Provider
 *
 * Please note that final, private and static methods cannot be stubbed or mocked.
 * They are ignored by PHPUnit's test double functionality and retain their original behavior
 */
class ProstorSmsProviderTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    /**
     * @var \ModuleCode\Provider\ProstorSmsProvider
     */
    private $prostorSmsProvider;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php and Code/config/module.config.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
            ),
            include __DIR__ . '/../../../ModuleCode/config/module.config.php'
        ));

        $config = $this->getApplicationConfig();

        // Assign real ProstorSmsProvider object
        $this->prostorSmsProvider = new ProstorSmsProvider($config['sms_providers']['prostor-sms']);
    }

    public function testGetName()
    {
        $provider_name = $this->prostorSmsProvider->getName();

        self::assertEquals(ProstorSmsProvider::PROVIDER_NAME, $provider_name);
    }

    public function testGetBalanceLimit()
    {
        $balance_limit = $this->prostorSmsProvider->getBalanceLimit();

        self::assertEquals(ProstorSmsProvider::BALANCE_LIMIT, $balance_limit);
    }

    /**
     * Проверка баланса при превышении лимита
     */
    public function testIsBalanceEnough()
    {
        // Баланс больше лимита
        $enough_balance = ProstorSmsProvider::BALANCE_LIMIT + 0.1;
        // Mocking ProstorSmsProvider
        $prostorSmsProviderMock = $this->getMockBuilder(ProstorSmsProvider::class)
            ->setMethods(['providerBalanceRequest'])
            ->disableOriginalConstructor()
            ->getMock();
        // Configure the providerBalanceRequest method
        $prostorSmsProviderMock->method('providerBalanceRequest')
            ->willReturn($enough_balance);

        self::assertTrue($prostorSmsProviderMock->isBalanceEnough());
    }

    /**
     * Проверка баланса при балансе меньше лимита
     */
    public function testIsBalanceEnoughFails()
    {
        // Баланс меньше лимита
        $low_balance = ProstorSmsProvider::BALANCE_LIMIT - 0.1;
        // Mocking ProstorSmsProvider
        $prostorSmsProviderMock = $this->getMockBuilder(ProstorSmsProvider::class)
            ->setMethods(['providerBalanceRequest'])
            ->disableOriginalConstructor()
            ->getMock();
        // Configure the providerBalanceRequest method
        $prostorSmsProviderMock->method('providerBalanceRequest')
            ->willReturn($low_balance);

        self::assertFalse($prostorSmsProviderMock->isBalanceEnough());
    }

    // Variant of invalid numbers for tests
    public function providerAnswers()
    {
        return [
            ['accepted;2397708808', '2397708808'],
            [ProstorSmsProvider::INVALID_MOBILE_PHONE, 'Prostor-SMS returned answer: invalid mobile phone'],
            [ProstorSmsProvider::TEXT_IS_EMPTY, 'Prostor-SMS returned answer: text is empty'],
            [ProstorSmsProvider::SENDER_ADDRESS_INVALID, 'Prostor-SMS returned answer: sender address invalid'],
            [ProstorSmsProvider::NOT_ENOUGH_BALANCE, 'Prostor-SMS returned answer: not enough balance'],
        ];
    }

    /**
     * Вызываваем метом smsSend с разными ответамии от API провайдера.
     * Ответы провайдера предоставляет mocked метод doHttpRequest.
     * @dataProvider providerAnswers
     */
    public function testSmsSend($given, $expected)
    {
        // Mocking ProstorSmsProvider
        $prostorSmsProviderMock = $this->getMockBuilder(ProstorSmsProvider::class)
            ->setMethods(['doHttpRequest'])
            ->disableOriginalConstructor()
            ->getMock();
        // Configure the smsSendHttpRequest method
        $prostorSmsProviderMock->method('doHttpRequest')
            ->willReturn($given);

        try {
            $result = $prostorSmsProviderMock->smsSend('79991112233','Test message');
        } catch (\Throwable $t) {
            $result = $t->getMessage();
        }

        // Здесь номер телефона и сообщение не важны, т.к. ответы возвращаются из mocked метода doHttpRequest
        self::assertEquals($expected, $result);
    }
}