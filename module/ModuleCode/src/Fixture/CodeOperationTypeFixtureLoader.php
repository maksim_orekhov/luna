<?php

namespace ModuleCode\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use ModuleCode\Entity\CodeOperationType;

/**
 * Class CodeOperationTypeFixtureLoader
 * @package ModuleCode\Fixture
 */
class CodeOperationTypeFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        $codeOperationType = new CodeOperationType();
        $codeOperationType->setName('PHONE_CONFIRMATION');
        $entityManager->persist($codeOperationType);

        $codeOperationType2 = new CodeOperationType();
        $codeOperationType2->setName('AUTHORIZATION_CONFIRMATION');
        $entityManager->persist($codeOperationType2);

        $codeOperationType3 = new CodeOperationType();
        $codeOperationType3->setName('PASSWORD_RESET_CONFIRMATION');
        $entityManager->persist($codeOperationType3);

        $codeOperationType4 = new CodeOperationType();
        $codeOperationType4->setName('PHONE_CHANGE');
        $entityManager->persist($codeOperationType4);

        $entityManager->flush();
    }

    public function getOrder()
    {
        return 10;
    }
}