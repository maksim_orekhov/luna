<?php

namespace ModuleCode\Entity;

use Application\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * Code
 *
 * @ORM\Table(name="code")
 * @ORM\Entity(repositoryClass="ModuleCode\Entity\Repository\CodeRepository");
 */
class Code
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=10, precision=0, scale=0, nullable=false, unique=false)
     */
    private $value;

    /**
     * @var integer
     *
     * @ORM\Column(name="count", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $count;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \ModuleCode\Entity\CodeOperationType
     *
     * @ORM\ManyToOne(targetEntity="CodeOperationType", inversedBy="codes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="code_type_id", referencedColumnName="id")
     * })
     */
    private $codeOperationType;

    /**
     * @var \Application\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\User", inversedBy="codes")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Code
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set count
     *
     * @param integer $count
     *
     * @return Code
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return integer
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Code
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set codeOperationType
     *
     * @param \ModuleCode\Entity\CodeOperationType $codeOperationType
     *
     * @return Code
     */
    public function setCodeOperationType(\ModuleCode\Entity\CodeOperationType $codeOperationType = null)
    {
        $this->codeOperationType = $codeOperationType;

        return $this;
    }

    /**
     * Get codeOperationType
     *
     * @return \ModuleCode\Entity\CodeOperationType
     */
    public function getCodeOperationType()
    {
        return $this->codeOperationType;
    }

    /**
     * Set user
     *
     * @param \Application\Entity\User $user
     *
     * @return Code
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}

