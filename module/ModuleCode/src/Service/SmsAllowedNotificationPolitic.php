<?php

namespace ModuleCode\Service;

use Application\Entity\Phone;

/**
 * Class SmsAllowedNotificationPolitic
 * @package Application\Service
 */
class SmsAllowedNotificationPolitic
{
    public function isAllowed(Phone $userPhone)
    {
        return $userPhone->getIsVerified();
    }
}