<?php
namespace ModuleCode\Service;

use Application\Entity\User;
use ModuleCode\Entity\CodeOperationType;

/**
 * User verification to execute sms operation
 *
 * Class UserSmsInspectorStrategy
 * @package ModuleCode\Service
 */
class UserSmsInspectorStrategy
{
    /**
     * @var CodeManager
     */
    private $codeManager;

    /**
     * @var array
     */
    private $config_tokens;

    /**
     * UserSmsInspectorStrategy constructor.
     * @param CodeManager $codeManager
     * @param array $config_tokens
     */
    public function __construct(CodeManager $codeManager, $config_tokens)
    {
        $this->codeManager      = $codeManager;
        $this->config_tokens    = $config_tokens;
    }

    /**
     * @param User              $user
     * @param CodeOperationType $codeOperationType
     * @return bool
     */
    public function isUserViolate(User $user, CodeOperationType $codeOperationType)
    {
        return $this->userVerificationByStrategy(
            array(
                new UserSmsLegitimacyViolation(null, null),
                new UserSmsLimitByCodeTypeViolation($this->config_tokens, $this->codeManager),
                new UserSmsMinIntervalViolation($this->config_tokens, $this->codeManager),
            ),
            $user,
            $codeOperationType
        );
    }

    /**
     * @param array             $strategyCollection
     * @param User              $user
     * @param CodeOperationType $codeOperationType
     * @return bool
     */
    private function userVerificationByStrategy($strategyCollection, User $user, CodeOperationType $codeOperationType)
    {
        foreach($strategyCollection as $strategy) {
            $violation = $strategy->hasViolation($user, $codeOperationType);
            if ($strategy instanceof UserSmsViolationInterface && $violation) {

                return $violation;
            }
        }

        return false;
    }
}