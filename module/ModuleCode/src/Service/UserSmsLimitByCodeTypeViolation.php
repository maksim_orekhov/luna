<?php

namespace ModuleCode\Service;

use Application\Entity\User;
use ModuleCode\Entity\CodeOperationType;

/**
 * Class UserSmsLimitByCodeTypeViolation
 * @package ModuleCode\Service
 */
class UserSmsLimitByCodeTypeViolation extends UserSmsViolation
{
    /**
     * {@inheritdoc}
     */
    public function hasViolation(User $user, CodeOperationType $codeOperationType=null)
    {
        // Get number of codes by User and Operation Type
        $codes_count = $this->codeManager->getCountUserCodesByOperationType($user, $codeOperationType);

        // If it exceeds the limit
        if ($codes_count >= $this->config_tokens['max_number_of_specific_code_for_user']) {

            return [ // Violation detected!
                'violation_type'        => 'limit_by_code_type',
                'violation_message'     => 'The number of messages limit reached',
                'codes_count'           => $codes_count,
                'max_number_of_code'    => $this->config_tokens['max_number_of_specific_code_for_user']
            ];
        }

        return false; // No violations found
    }
}