<?php

namespace ModuleCode\Service;

/**
 * Abstract class UserSmsViolation
 * @package ModuleCode\Service
 */
abstract class UserSmsViolation implements UserSmsViolationInterface
{
    /**
     * @var CodeManager
     */
    protected $codeManager;

    /**
     * @var array
     */
    protected $config_tokens;


    public function __construct($config_tokens, $codeManager=null)
    {
        $this->config_tokens    = $config_tokens;
        $this->codeManager      = $codeManager;
    }
}