<?php

namespace ModuleCode\Service;

use Application\Entity\User;
use ModuleCode\Entity\CodeOperationType;

/**
 * Interface UserSmsViolationInterface
 * @package ModuleCode\Service
 */
interface UserSmsViolationInterface
{
    /**
     * @param User $user
     * @param CodeOperationType|null $codeOperationType
     * @return mixed
     */
    function hasViolation(User $user, CodeOperationType $codeOperationType=null);
}