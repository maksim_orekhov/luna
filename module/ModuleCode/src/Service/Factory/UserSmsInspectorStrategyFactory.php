<?php
namespace ModuleCode\Service\Factory;

use Interop\Container\ContainerInterface;
use ModuleCode\Service\CodeManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use ModuleCode\Service\UserSmsInspectorStrategy;

/**
 * Class UserSmsInspectorStrategyFactory
 * @package ModuleCode\Service\Factory
 */
class UserSmsInspectorStrategyFactory implements FactoryInterface
{
    /**
     * This method creates the UserSmsInspectorStrategyFactory service and returns its instance.
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $codeManager = $container->get(CodeManager::class);

        $config = $container->get('Config');
        if (isset($config['tokens']))
            $config_tokens = $config['tokens'];
        else
            $config_tokens = [];

        return new UserSmsInspectorStrategy($codeManager, $config_tokens);
    }
}