<?php
namespace ModuleCode\Api;

use Core\Controller\Plugin\Message\MessageJsonModelTrait;
use Core\Exception\LogicException;
use Core\Service\Base\BaseUserManager;
use Core\Service\CodeOperationInterface;
use Core\Service\SessionContainerManager;
use Zend\View\Model\JsonModel;
use Application\Entity\User;
use ModuleCode\Entity\CodeOperationType;
use ModuleCode\Service\SmsStrategyContext;
use ModuleCode\Service\CodeManager;
use ModuleCode\Service\UserSmsInspector;

/**
 * Class CodeController
 * @package ModuleCode\Api
 */
class CodeController implements CodeOperationInterface
{
    use \ModuleCode\Provider\SmsCodeGeneratorTrait;
    use MessageJsonModelTrait;

    const SMS_CODE_LENGTH = 6;

    const ERROR_INVALID_DATA_PROVIDED           = "Invalid data provided";
    const ERROR_NO_SUCH_CODE_TYPE_FOUND         = "No such code type found";
    const ERROR_USER_NOT_ALLOWED_REQUEST_CODE   = "User not allowed request code";
    const SUCCESS_CODE_IS_GENERATED             = "Code is generated";

    /**
     * Entity manager
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var BaseUserManager
     */
    private $baseUserManager;

    /**
     * Code manager.
     * @var CodeManager
     */
    private $codeManager;

    /**
     * @var UserSmsInspector
     */
    private $userSmsInspector;

    /**
     * Contents of the 'sms_providers' config key
     * @var array
     */
    private $config_sms_providers;

    /**
     * Contents of the 'email_providers' config key
     * @var array
     */
    private $config_email_providers;
    /**
     * @var SessionContainerManager
     */
    private $sessionContainerManager;

    /**
     * CodeController constructor.
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param BaseUserManager $baseUserManager
     * @param CodeManager $codeManager
     * @param UserSmsInspector $userSmsInspector
     * @param SessionContainerManager $sessionContainerManager
     * @param array $config_sms_providers
     * @param array $config_email_providers
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager,
                                BaseUserManager $baseUserManager,
                                CodeManager $codeManager,
                                UserSmsInspector $userSmsInspector,
                                SessionContainerManager $sessionContainerManager,
                                array $config_sms_providers,
                                array $config_email_providers)
    {
        $this->entityManager            = $entityManager;
        $this->baseUserManager          = $baseUserManager;
        $this->codeManager              = $codeManager;
        $this->userSmsInspector         = $userSmsInspector;
        $this->config_sms_providers     = $config_sms_providers;
        $this->config_email_providers   = $config_email_providers;
        $this->sessionContainerManager = $sessionContainerManager;
    }

    /**
     * @param array $params
     * @return JsonModel
     * @throws \Throwable
     */
    public function sendCode(array $params)
    {
        // Get parameters from incoming $params
        $user_login                 = $params['user_login'];
        $code_operation_type_name   = $params['code_operation_type_name'];
        /** @var \Core\Provider\Mail\MailSenderInterface $emailSender */
        $emailSender                = $params['emailSender'];

        try {
            // Check for the presence of necessary incoming data
            if ( !$user_login || !$code_operation_type_name ) {
                throw new \Exception(self::ERROR_INVALID_DATA_PROVIDED);
            }

            /** @var User $user */ // Get User by login
            $user = $this->baseUserManager->getUserByLogin($user_login);
            if (!$user) {

                throw new LogicException(null, LogicException::USER_NOT_FOUND);
            }
            /** @var CodeOperationType $codeOperationType */ // Get CodeType by name
            $codeOperationType = $this->codeManager->getCodeOperationTypeByName($code_operation_type_name);
            // Remove user's old codes (over $this->config_tokens['operation_confirmation_code_expiration_time'])
            $this->codeManager->removeOldUserCodes($user);

            // If user does not allowed to request(generate) code
            $code_permission = $this->userSmsInspector->isUserAllowedToRequestCode($user, $codeOperationType);

            if(!$code_permission['allowed']) {
                #throw new \Exception(self::ERROR_USER_NOT_ALLOWED_REQUEST_CODE);
                return $this->JsonErrorModel(self::ERROR_USER_NOT_ALLOWED_REQUEST_CODE, null, $code_permission);
            }

            // Create (generate and save) confirmation code
            $confirmation_code = $this->createConfirmationCode($user, $codeOperationType);
            // Try send confirmation code
            if(!$emailSender) {
                if(!$this->config_sms_providers['message_send_simulation'] ) {
                    $this->sendConfirmationCodeBySms($user, $confirmation_code);
                }
            } else {
                if(!$this->config_email_providers['message_send_simulation'] ) {
                    $emailSender->sendMail(
                        $user->getEmail()->getEmail(),
                        ['code' => $confirmation_code, 'user_login' => $user_login]
                    );
                }
            }

            return $this->JsonSuccessModel(self::SUCCESS_CODE_IS_GENERATED);

        } catch (\Throwable $t) {

            throw $t;
        }
    }

    /**
     * @param array $params
     * @return JsonModel
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function checkCode(array $params)
    {
        // Get parameters from incoming $params
        $user_login                 = $params['user_login'];
        $code_value                 = $params['code_value'];
        $code_operation_type_name   = $params['code_operation_type_name'];

        if($user_login && $code_value) {

            // Get User by $user_login
            $user = $this->entityManager->getRepository(User::class)
                ->findOneBy(array('login'=> $user_login ));

            // Get codeOperationTyp object by provided code type name
            $codeOperationType = $this->entityManager->getRepository(CodeOperationType::class)
                ->findOneBy(
                    array('name' => $code_operation_type_name)
                );

            if(!$user || !$user instanceof User || !$codeOperationType || !$codeOperationType instanceof CodeOperationType) {
                // Return Json with ERROR status
                return $this->JsonErrorModel('Confirmation failed');
            }

            // Confirm code by user login and code value
            if ($this->codeManager->checkUserCodeExists($user, $codeOperationType, $code_value)) {
                // Delete all user's codes of specific operation type from DB
                $this->codeManager->removeAllUserCodesByCodeType($user, $codeOperationType);
                // Return Json with SUCCESS status
                return $this->JsonSuccessModel('Successfully confirmed');

            } else {
                // Return Json with ERROR status
                return $this->JsonErrorModel('Confirmation failed');
            }

        } else {
            // Return Json with ERROR status
            return $this->JsonErrorModel('Confirmation failed');
        }
    }

    /**
     * @param array $params
     * @return array|bool|JsonModel
     */
    public function userAllowedRequestCodeStatus(array $params)
    {
        if (isset($params['user_login'], $params['code_operation_type_name'])) {

            // Get User by $user_login
            $user = $this->entityManager->getRepository(User::class)
                ->findOneBy(array('login'=> $params['user_login']));

            // Get codeOperationTyp object by provided code type name
            $codeOperationType = $this->entityManager->getRepository(CodeOperationType::class)
                ->findOneBy(
                    array('name' => $params['code_operation_type_name'])
                );

            if(!$user || !$user instanceof User || !$codeOperationType || !$codeOperationType instanceof CodeOperationType) {
                // Return Json with ERROR status
                return $this->JsonErrorModel('User does not found');
            }

            return $this->userSmsInspector->isUserAllowedToRequestCode($user, $codeOperationType);

        } else {
            // Return Json with ERROR status
            return $this->JsonErrorModel('Insufficient data provided');
        }
    }

    /**
     * @param User $user
     * @param CodeOperationType $codeType
     * @return bool
     * @throws \Exception
     */
    private function createConfirmationCode(User $user, CodeOperationType $codeType)
    {
        try {
            // Code generate
            $confirmation_code = $this->codeGenerate(self::SMS_CODE_LENGTH);
            // Save confirmationCode // If fail returns Exception
            $this->codeManager->saveCode($user, $codeType, $confirmation_code);

            return $confirmation_code;
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }
    }

    /**
     * @param User $user
     * @param $confirmation_code
     * @return bool
     * @throws \Exception
     */
    private function sendConfirmationCodeBySms(User $user, $confirmation_code)
    {
        // Get user phone object
        $userPhone = $user->getPhone();
        // Choose sms provider
        // @TODO Exception
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config_sms_providers);

        $is_initiator = $this->sessionContainerManager
            ->isProcessInitiatorExists(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION)
            || $this->sessionContainerManager
                ->isProcessInitiatorExists(CodeOperationInterface::CODE_OPERATION_TYPE_NAME);

        if ($is_initiator || $smsStrategy->isAllowedSmsNotify()) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            // @TODO Вынести в отдельный метод
            //$smsMessage = "Код для подтверждения операции: " . $confirmationCode . ".";
            $smsMessage = 'Confirmation code: ' . $confirmation_code . '.';
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);

        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
        return true;
    }
}