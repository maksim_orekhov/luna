<?php

namespace ModuleCode\Provider;

/**
 * Interface SmsProviderInterface
 * @package ModuleCode\Provider
 */
interface SmsProviderInterface
{
    /**
     * @param string $phone_number
     * @param string $message
     * @return mixed
     */
    public function smsSend($phone_number, $message);

    public function smsStatusCheck();

    /**
     * @return boolean
     */
    public function isBalanceEnough();

    /**
     * @return string
     */
    public function getName();
}