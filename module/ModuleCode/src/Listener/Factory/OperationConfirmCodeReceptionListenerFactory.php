<?php
namespace ModuleCode\Listener\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use ModuleCode\Listener\OperationConfirmCodeReceptionListener;
use Zend\EventManager\EventManager;
use ModuleCode\Api\CodeController;

/**
 * Class OperationConfirmCodeReceptionListenerFactory
 * @package ModuleCode\Listener\Factory
 */
class OperationConfirmCodeReceptionListenerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $confirmationCode = $container->get(CodeController::class);
        $events = new EventManager();

        return new OperationConfirmCodeReceptionListener($confirmationCode, $events);
    }
}