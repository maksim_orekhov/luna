<?php
namespace ModuleImageResize\Controller;

use ModuleFileManager\Service\FileManager;
use Zend\Cache\Storage\Adapter\FilesystemOptions;
use Zend\Mvc\Controller\AbstractActionController;
use ModuleImageResize\Service\ImageProcessing;
use Zend\Cache\Storage\Adapter\Filesystem;

/**
 * Class ResizeController
 * @package ModuleImageResize\Controller
 */
class ResizeController extends AbstractActionController
{
    const CACHE_DIR = './data/cache/images';
    const CACHE_NAMESPACE = 'img';
    const PUBLIC_DIR = './public';
    /**
     * @var FileManager
     */
    private $fileManager;
    /**
     * @var ImageProcessing
     */
    private $imageProcessing;
    /**
     * @var Filesystem
     */
    private $cache;
    /**
     * @var array
     */
    private $config;

    /**
     * ResizeController constructor.
     * @param ImageProcessing $imageProcessing
     * @param FileManager $fileManager
     * @param Filesystem $cache
     * @param array $config
     */
    public function __construct(ImageProcessing $imageProcessing,
                                FileManager $fileManager,
                                Filesystem $cache,
                                array $config)
    {
        error_reporting(E_ALL ^ E_WARNING);

        $this->fileManager = $fileManager;
        $this->imageProcessing = $imageProcessing;
        $this->cache = $cache;
        $this->config = $config;
    }

    public function initCache()
    {
        if(!is_dir(self::CACHE_DIR) && !mkdir($concurrentDirectory = self::CACHE_DIR, 0755, true) && !is_dir($concurrentDirectory)) {
            throw new \RuntimeException('error create cache folder');
        }
        $optionsCache = new FilesystemOptions([
            'cache_dir' => self::CACHE_DIR,
            'namespace' => self::CACHE_NAMESPACE,
            'ttl' => $this->config['image_cache_max_age']
        ]);
        $this->cache->setOptions($optionsCache);
        $this->cache->clearExpired();
    }

    /**
     * @throws \Exception
     */
    public function resizeAction()
    {
        $dirname = $this->params('dirname');
        $image = $this->params('image');
        $command = $this->params('command');
        $extension = $this->params('extension');
        $version = $this->params()->fromQuery('v', '');
        $version = !empty($version) ? '?v='.$version : '';

        $file_name = $image.'.'.$command.'.'.$extension.$version;

        try {

            $file = $this->fileManager->selectFileById($image);
            if ( $file ) {
                if ( !$this->fileManager->isFileReadable($file) || !$this->fileManager->isFileTypeDisplayable($file->getType())) {

                    throw new \RuntimeException('File not found');
                }

                $imagePath = $this->fileManager->getPathFile($file);
            } else {
                //если файла нет в базе то ищем в public
                $imagePath = self::PUBLIC_DIR.'/'.$dirname.'/'.$image.'.'.$extension;
                if ( !file_exists($imagePath)) {

                    throw new \RuntimeException('File not found');
                }
            }

            $expires = $this->config['image_cache_max_age'];
            $mod_gmt = gmdate('D, d M Y H:i:', filemtime($imagePath)) . ' GMT';
            $exp_gmt = gmdate('D, d M Y H:i:', time() + $expires) . ' GMT';

            //проверяем дату изменения
            if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) &&
                $_SERVER['HTTP_IF_MODIFIED_SINCE'] === $mod_gmt)
            {
                header('HTTP/1.1 304 Not Modified');
                exit();
            }

            $this->initCache();
            // Image has changed or browser has no cache
            $cache_id = preg_replace('/[^a-z0-9_\+\-]/', '', $file_name);
            // Save image in Zend Framework's server side Cache or get it from there
            if (null === ($image = $this->cache->getItem($cache_id))) {
                $image = $this->imageProcessing->process($imagePath, $command, $extension);
                $this->cache->setItem($cache_id, $image);
            }

            //set response
            $response = $this->getResponse();
            $response->setContent($image);
            $response
                ->getHeaders()
                ->addHeaderLine('Content-Transfer-Encoding', 'binary')
                ->addHeaderLine('Content-Disposition', 'filename="' . $file_name . '"')
                ->addHeaderLine('Content-Type', $this->imageProcessing->getMimeType($extension))
                ->addHeaderLine('Expires', $exp_gmt, true)
                ->addHeaderLine('Pragma', 'cache', true)
                ->addHeaderLine('ETag', md5($image), true)
                ->addHeaderLine('Cache-Control', 'public, max-age=' . $expires, true)
                ->addHeaderLine('Last-Modified', $mod_gmt, true);

            return $response;
        }
        catch (\Throwable $t) {
            $image = $this->imageProcessing->process404($command, 'jpeg');

            $response = $this->getResponse();
            $response->setContent($image);
            $response
                ->getHeaders()
                ->addHeaderLine('Content-Transfer-Encoding', 'binary')
                ->addHeaderLine('Content-Disposition', 'filename="' . $file_name . '"')
                ->addHeaderLine('Content-Type', $this->imageProcessing->getMimeType('jpeg'));

            return $response;
        }
    }
}
