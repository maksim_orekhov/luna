<?php
namespace ModuleImageResize\Controller;

use Interop\Container\ContainerInterface;
use ModuleFileManager\Service\FileManager;
use ModuleImageResize\Service\ImageProcessing;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class ResizeControllerFactory
 * @package ModuleImageResize\Controller
 */
class ResizeControllerFactory implements FactoryInterface
{
    /**
     * Create an IndexController object
     *
     * @param  ContainerInterface $container
     * @param  string             $requestedName
     * @param  null|array         $options
     * @return ResizeController
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $imageProcessing = $container->get(ImageProcessing::class);
        $fileManager = $container->get(FileManager::class);
        $cache = $container->get('FilesystemCache');
        $config = $container->get('config');

        return new ResizeController(
            $imageProcessing,
            $fileManager,
            $cache,
            $config
        );
    }
}
