<?php
namespace ModuleImageResize\View\Helper;

use Zend\View\Helper\AbstractHelper;

class ResizeHelper extends AbstractHelper
{
    /** @var  array */
    protected $imgParts;
    /** @var  string*/
    protected $commands;
    /** @var  string*/
    protected $version;

    /**
     * @param $imgPath
     * @return $this
     */
    public function __invoke($imgPath)
    {
        $arr_url = explode('?', $imgPath, 2);
        $this->version = $arr_url[1] ?? '';

        $this->imgParts = mb_pathinfo($imgPath);
        $this->commands = '';

        return $this;
    }

    /**
     * @param $width
     * @param $height
     * @return $this
     */
    public function thumb($width, $height): self
    {
        $pref = '';
        if (!empty($this->commands)){
            $pref = '-';
        }
        $this->commands .= $pref.'thumb_' . $width . 'x' . $height;
        
        return $this;
    }

    /**
     * @param $width
     * @param $height
     * @return $this
     */
    public function resize($width, $height): self
    {
        $pref = '';
        if (!empty($this->commands)){
            $pref = '-';
        }
        $this->commands .= $pref.'resize_' . $width . 'x' . $height;

        return $this;
    }

    /**
     * @param $degree
     * @return $this
     */
    public function rotate($degree): self
    {
        $pref = '';
        if (!empty($this->commands)){
            $pref = '-';
        }
        $this->commands .= $pref.'rotate_' . (int) $degree;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $dirname = ltrim($this->imgParts['dirname'], '.');
        $filename = $this->imgParts['filename'];
        $commands = $this->commands;
        $extension = $this->imgParts['extension'] ?? 'jpeg';
        $version = $this->version ? '?'.$this->version : '';

        $base_path = $this->getView()->basePath(
              $dirname . '/'
            . $filename . '.'
            . $commands . '.'
            . $extension
            . $version
        );

        return '/resize'.$base_path;
    }
}
