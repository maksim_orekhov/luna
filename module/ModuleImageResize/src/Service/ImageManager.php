<?php
namespace ModuleImageResize\Service;

/**
 * Class ImageManager
 * @package ModuleImageResize\Service
 */
class ImageManager
{
    /**
     * @var ImageProcessing
     */
    private $imageProcessing;

    /**
     * ImageManager constructor.
     * @param ImageProcessing $imageProcessing
     */
    public function __construct(ImageProcessing $imageProcessing)
    {
        $this->imageProcessing = $imageProcessing;
    }

    /**
     * @param $rotate
     * @param $imagePath
     * @return bool
     */
    public function rotateRewriteFile($rotate, $imagePath): bool
    {
        if (!file_exists($imagePath) || !file_get_contents($imagePath, FILE_USE_INCLUDE_PATH)) {

            throw new \RuntimeException('File not found');
        }

        $imgParts = pathinfo($imagePath);
        $command = 'rotate_'.$rotate;
        $this->imageProcessing->process($imagePath, $command, $imgParts['extension']);
        $this->imageProcessing->save($imagePath);

        return true;
    }
}
