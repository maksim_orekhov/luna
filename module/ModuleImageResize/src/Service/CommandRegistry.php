<?php
namespace ModuleImageResize\Service;

/**
 * Class CommandRegistry
 * @package ModuleImageResize\Service
 */
class CommandRegistry
{
    /**
     * singleton
     */
    private static $instance;
    
    /**
     * the actual commands
     *
     * @var array
     */
    protected $commands = array();
    
    /**
     * private constructor
     */
    private function __construct()
    {
    }
    
    /**
     * get singleton instance
     *
     * @return self
     */
    public static function getInstance(): self
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * clear the singleton
     *
     * @return void
     */
    public static function destroy()
    {
        self::$instance = null;
    }
    
    /**
     * get command registry list
     *
     * @return array
     */
    public static function getCommands(): array
    {
        return self::getInstance()->commands;
    }
    
    /**
     * command registry has command
     *
     * @param string $command
     *
     * @return boolean
     */
    public static function hasCommand($command): bool
    {
        if (!\is_string($command) || '' === $command) {
            throw new \RuntimeException(
                'Parameter command is not a valid string'
            );
        }
        $instance = self::getInstance();

        return isset($instance->commands[$command]);
    }
    
    /**
     * get command callback
     *
     * @param string $command
     *
     * @return array
     */
    public static function getCommand($command): array
    {
        if (!\is_string($command) || '' === $command) {
            throw new \RuntimeException(
                'Parameter command is not a valid string'
            );
        }
        $instance = self::getInstance();
        
        if (!isset($instance->commands[$command])) {
            throw new \RuntimeException(
                'Command "' . $command . '" is not a registered command'
            );

        }
        
        return $instance->commands[$command];
    }

    /**
     * register a image command
     *
     * @param  string $command
     * @param  callable $callback
     * @return self
     */
    public static function register($command, $callback): self
    {
        if (!\is_string($command) || '' === $command) {
            throw new \RuntimeException(
                'Parameter command is not a valid string'
            );
        }
        if (!\is_callable($callback)) {
            throw new \RuntimeException(
                'Parameter callback is not a valid callback'
            );
        }
        $instance = self::getInstance();
        $instance->commands[$command] = $callback;
        
        return $instance;
    }
}
