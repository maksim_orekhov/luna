<?php
namespace ModuleImageResize\Service;

use Imagine\Gd\Imagine;
use Imagine\Image\ImageInterface;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Image\Palette\RGB;


/**
 * Class ImageProcessing
 * @package ModuleImageResize\Service
 */
class ImageProcessing
{
    // font for error (Roboto-Regular.ttf|PTSerif-Regular.ttf|Big-River.ttf)
    const DEFAULT_FONT = 'Big-River.ttf';
    /**
     * @var Imagine
     */
    protected $imagine;
    
    /**
     * @var ImageInterface
     */
    protected $image;

    /**
     * ImageProcessing constructor.
     * @param Imagine $imagine
     */
    public function __construct(Imagine $imagine)
    {
        $this->setImagineService($imagine);
    }

    /**
     * @param Imagine $imagine
     * @return $this
     */
    public function setImagineService(Imagine $imagine): self
    {
        $this->imagine = $imagine;

        return $this;
    }
    
    /**
     * Get the imagine service
     * 
     * @return Imagine
     */
    public function getImagineService(): Imagine
    {
        return $this->imagine;
    }

    /**
     * Process command to show
     *
     * @param $source
     * @param $commands
     * @param $extension
     * @return string
     */
    public function process($source, $commands, $extension): string
    {
        $this->image = $this->getImagineService()->open($source);
        foreach ($this->analyseCommands($commands) as $command) {
            if ($this->runCommand($command)) {
                continue;
            }
            $this->runCustomCommand($command);
        }
        
        return $this->image->get($this->normalizeFormat($extension));
    }

    /**
     * @param $target
     * @return ImageInterface
     */
    public function save($target): ImageInterface
    {
        return $this->image->save($target);
    }

    /**
     * Process command to show 404 image and save to target
     *
     * @param string $commands
     *
     * @param $extension
     * @return string
     */
    public function process404($commands, $extension): string
    {
        $text = 'Not found';
        $backgroundColor = null;
        $color = null;
        $width = null;
        $height = null;
        foreach ($this->analyseCommands($commands) as $command) {
            list($width, $height) = $command['params'];
        }
        $this->image404($text, $backgroundColor, $color, $width, $height);

        return $this->image->get($this->normalizeFormat($extension));
    }

    /**
     * Normalize size parameters
     *
     * @param int $width
     * @param int $height
     * @return array
     */
    public function normalizeSizeParams($width = 0, $height = 0): array
    {
        $size = [
            'width' => (int) $width,
            'height' => (int) $height
        ];
        if ($size['width'] <= 0 && $size['height'] <= 0) {

            throw new \RuntimeException('Invalid parameter width for command "thumb"');
        }
        if ($size['width'] <= 0) {
            $size['width'] = $size['height'];
        }
        if ($size['height'] <= 0) {
            $size['height'] = $size['width'];
        }

        return $size;
    }
    
    /**
     * Analyse commands string and returns array with command/params keys
     * 
     * @param string $commands
     * 
     * @return array
     */
    protected function analyseCommands($commands): array
    {
        $commandList = array();
        foreach (explode('-', $commands) as $commandLine) {
            $params = explode('_', $commandLine);
            $command = array_shift($params);
            $params = explode('x', $params[0]);

            $commandList[] = array(
                'command' => $command,
                'params' => $params,
            );
        }

        return $commandList;
    }
    
    /**
     * Run command if exists
     * 
     * @param array $command
     * 
     * @return boolean
     */
    protected function runCommand($command): bool
    {
        $method = 'image' . ucfirst(strtolower($command['command']));
        if (!method_exists($this, $method)) {
            return false;
        }
        \call_user_func_array(array($this, $method), $command['params']);
        
        return true;
    }
    
    /**
     * Run custom command if exists
     * 
     * @param array $command
     * 
     * @return boolean
     */
    protected function runCustomCommand($command): bool
    {
        if (!CommandRegistry::hasCommand($command['command'])) {
            throw new \RuntimeException(
                'Command "' . $command['command'] . '" not found'
            );
        }
        $customCommand = CommandRegistry::getCommand($command['command']);
        
        array_unshift($command['params'], $this->image);
        \call_user_func_array($customCommand, $command['params']);
        
        return true;
    }
    
    /**
     * Command image thumb
     * 
     * @param int $width
     * @param int $height
     * 
     * @return void
     */
    protected function imageThumb($width, $height)
    {
        $size = $this->normalizeSizeParams($width, $height);

        $sizeSource = $this->image->getSize();
        $sourceWidth = $sizeSource->getWidth();
        $sourceHeight = $sizeSource->getHeight();

        $x_ratio = $size['width'] / $sourceWidth;
        $y_ratio = $size['height'] / $sourceHeight;

        if (($sourceWidth <= $size['width']) && ($sourceHeight <= $size['height'])){
            $tn_width = $sourceWidth;
            $tn_height = $sourceHeight;
        } else {
            if (($x_ratio * $sourceHeight) < $size['height']) {
                $tn_height = ceil($x_ratio * $sourceHeight);
                $tn_width = $size['width'];
            } else {
                $tn_width = ceil($y_ratio * $sourceWidth);
                $tn_height = $size['height'];
            }
        }

        $this->image->resize(new Box($tn_width, $tn_height));
    }

    /**
     * Command image resize
     *
     * @param int $width
     * @param int $height
     *
     * @return void
     */
    protected function imageResize($width, $height)
    {
        $size = $this->normalizeSizeParams($width, $height);
        if ($size['width'] !== $size['height']) {
            $sizeSource = $this->image->getSize();
            $sourceWidth = $sizeSource->getWidth();
            $sourceHeight = $sizeSource->getHeight();

            $src_aspect = $sourceWidth / $sourceHeight; //отношение ширины к высоте исходника
            $thumb_aspect = $size['width'] / $size['height']; //отношение ширины к высоте аватарки

            if ($src_aspect < $thumb_aspect) {
                //узкий вариант (фиксированная ширина)
                $before_height = $sourceWidth / $thumb_aspect;
                $new_size = array($sourceWidth, $before_height);
                $src_pos = array(0, ($sourceHeight - $before_height) / 2);

            } else if ($src_aspect > $thumb_aspect) {
                //широкий вариант (фиксированная высота)
                $before_width = $sourceHeight * $thumb_aspect;
                $new_size = array($before_width, $sourceHeight);
                $src_pos = array(($sourceWidth - $before_width) / 2, 0);
            } else {
                $new_size = array($size['width'], $size['height']);
                $src_pos = array(0, 0);
            }

            $new_size[0] = max($new_size[0], 1);
            $new_size[1] = max($new_size[1], 1);

            $this->image->crop(new Point($src_pos[0], $src_pos[1]), new Box($new_size[0], $new_size[1]));
            $this->image->resize(new Box($size['width'], $size['height']));
        } else {
            $this->imageThumb($width, $height);
        }
    }

    /**
     * Command image rotate
     *
     * @param int $degree
     *
     * @return void
     */
    protected function imageRotate($degree)
    {
        $this->image->rotate((int) $degree);
    }

    /**
     * Command image resize
     * 
     * @param string $text
     * @param string $backgroundColor
     * @param string $color
     * @param int $width
     * @param int $height
     * 
     * @return void
     */
    protected function image404($text, $backgroundColor, $color, $width, $height)
    {
        $text = (string) $text;
        $backgroundColor = (string) $backgroundColor;
        $color = (string) $color;
        $width = (int) $width;
        $height = (int) $height;
        
        if (\strlen($backgroundColor) !== 6 || !preg_match('![0-9abcdef]!i', $backgroundColor)) {
            $backgroundColor = 'F8F8F8';
        }
        if (\strlen($color) !== 6 || !preg_match('![0-9abcdef]!i', $color)) {
            $color = '4B4C6E';
        }
        if ($width < 110) {
            $width = 110;
        }
        if ($height <= 40) {
            $height = 40;
        }
        
        $palette = new RGB();
        $size  = new Box($width, $height);
        $this->image = $this->getImagineService()->create($size, $palette->color('#' . $backgroundColor, 0));

        if ($text) {
            $this->drawCenteredText($text, $color);
        }
    }
    
    /**
     * Draw centered text in current image
     * 
     * @param string $text
     * @param string $color
     * 
     * @return void
     */
    protected function drawCenteredText($text, $color)
    {
        $width = $this->image->getSize()->getWidth();
        $height = $this->image->getSize()->getHeight();
        $fontColor = $this->image->palette()->color('#' . $color);
        $fontSize = 22;
        $widthFactor = $width > 160 ? 0.8 : 0.9;
        $heightFactor = $height > 160 ? 0.8 : 0.9;
        do {
            $font = $this->getImagineService()
              ->font(__DIR__ . '/../font/'.self::DEFAULT_FONT, $fontSize, $fontColor);
            $fontBox = $font->box($text);
            $fontSize = round($fontSize * 0.8);
            
        } while ($fontSize > 5
          && ($width * $widthFactor < $fontBox->getWidth() || $height * $heightFactor < $fontBox->getHeight()));

        $pointX = max(0, floor(($width - $fontBox->getWidth()) / 2));
        $pointY = max(0, floor(($height - $fontBox->getHeight()) / 2));
        
        $this->image->draw()->text($text, $font, new Point($pointX, $pointY));
    }

    /**
     * @param $format
     * @return mixed
     */
    public function getMimeType($format)
    {
        $format = $this->normalizeFormat($format);

        if (!$this->supported($format)) {

            throw new \RuntimeException('Invalid format');
        }

        static $mimeTypes = array(
            'jpeg' => 'image/jpeg',
            'gif'  => 'image/gif',
            'png'  => 'image/png',
            'wbmp' => 'image/vnd.wap.wbmp',
            'xbm'  => 'image/xbm',
        );

        return $mimeTypes[$format];
    }

    /**
     * @param null $format
     * @return array|bool
     */
    private function supported($format = null)
    {
        $formats = array('gif', 'jpeg', 'png', 'wbmp', 'xbm');

        if (null === $format) {
            return $formats;
        }

        return \in_array($format, $formats, true);
    }

    /**
     * @param $format
     * @return string
     */
    private function normalizeFormat($format): string
    {
        $format = strtolower($format);

        if ('jpg' === $format || 'pjpeg' === $format) {
            $format = 'jpeg';
        }

        return $format;
    }
}
