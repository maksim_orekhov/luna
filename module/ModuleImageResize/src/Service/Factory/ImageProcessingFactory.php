<?php
namespace ModuleImageResize\Service\Factory;

use Imagine\Gd\Imagine;
use Interop\Container\ContainerInterface;
use ModuleImageResize\Service\ImageProcessing;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class ImageProcessingFactory
 * @package ModuleImageResize\Service
 */
class ImageProcessingFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ImageProcessing|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $imagine = $container->get(Imagine::class);

        return new ImageProcessing(
            $imagine
        );
    }
}
