<?php
namespace ModuleImageResize\Service\Factory;

use Interop\Container\ContainerInterface;
use ModuleImageResize\Service\ImageManager;
use ModuleImageResize\Service\ImageProcessing;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class ImageManagerFactory
 * @package ModuleImageResize\Service
 */
class ImageManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ImageManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $imageProcessing = $container->get(ImageProcessing::class);

        return new ImageManager(
            $imageProcessing
        );
    }
}
