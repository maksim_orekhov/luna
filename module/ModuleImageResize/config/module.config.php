<?php
namespace ModuleImageResize;

use Imagine\Gd\Imagine;
use Zend\Router\Http\Regex;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'image-resize' => [
                'type' => Regex::class,
                'options' => [
                    'regex'    => '/resize/(?<dirname>.*)/(?<image>.*?)\.(?<command>.*)\.(?<extension>[a-zA-Z]+)',
                    'defaults' => [
                        'controller' => Controller\ResizeController::class,
                        'action' => 'resize',
                    ],
                    'spec' => '/resize/%dirname%/%image%.%command%.%extension%',
                ],
            ],
        ],
    ],
    'service_manager' => [
        'aliases' => [
            'ModuleImageResizeImagine' => Imagine::class,
        ],
        'factories' => [
            Imagine::class => InvokableFactory::class,
            Service\ImageProcessing::class => Service\Factory\ImageProcessingFactory::class,
            Service\ImageManager::class => Service\Factory\ImageManagerFactory::class,
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\ResizeController::class => Controller\ResizeControllerFactory::class,
        ],
    ],
    'view_helpers' => [
        'invokables' => [
            'resizer' => View\Helper\ResizeHelper::class,
            'Resizer' => View\Helper\ResizeHelper::class,
            'resizeHelper' => View\Helper\ResizeHelper::class,
            'ResizeHelper' => View\Helper\ResizeHelper::class,
            'resizehelper' => View\Helper\ResizeHelper::class,
        ],
    ],
    'image_cache_max_age' => 60*60*24*1 //1 день
];
