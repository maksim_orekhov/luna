<?php
namespace Api;

class Module
{
    public function getConfig(): array
    {
        return array_replace_recursive(
            require __DIR__ . '/../config/module.config.php',
            require __DIR__ . '/../config/router.config.php',
            require __DIR__ . '/../config/assets.config.php'
        );
    }
}
