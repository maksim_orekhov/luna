<?php
namespace Api\Purchase\Factory;

use Api\Purchase\ArchivePurchaseController;
use Application\Service\PurchaseManager;
use Application\Service\UserManager;
use Core\Service\Base\BaseAuthManager;
use Interop\Container\ContainerInterface;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class UserArchivePurchaseControllerFactory
 * @package Api\Controller\Purchase\Factory
 */
class ArchivePurchaseControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ArchivePurchaseController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $userManager = $container->get(UserManager::class);
        $purchaseManager = $container->get(PurchaseManager::class);
        $rbacManager = $container->get(RbacManager::class);

        return new ArchivePurchaseController(
            $baseAuthManager,
            $userManager,
            $purchaseManager,
            $rbacManager
        );
    }
}