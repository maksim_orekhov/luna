<?php
namespace Api\Purchase\Factory;

use Api\Purchase\UserArchivePurchaseController;
use Application\Service\PurchaseManager;
use Application\Service\UserManager;
use Core\Service\Base\BaseAuthManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class UserArchivePurchaseControllerFactory
 * @package Api\Controller\Purchase\Factory
 */
class UserArchivePurchaseControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return UserArchivePurchaseController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $userManager = $container->get(UserManager::class);
        $purchaseManager = $container->get(PurchaseManager::class);

        return new UserArchivePurchaseController(
            $baseAuthManager,
            $userManager,
            $purchaseManager
        );
    }
}