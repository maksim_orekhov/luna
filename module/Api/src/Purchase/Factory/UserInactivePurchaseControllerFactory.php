<?php
namespace Api\Purchase\Factory;

use Api\Purchase\UserInactivePurchaseController;
use Application\Service\PurchaseManager;
use Application\Service\UserManager;
use Core\Service\Base\BaseAuthManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class UserInactivePurchaseControllerFactory
 * @package Api\Controller\Purchase\Factory
 */
class UserInactivePurchaseControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return UserInactivePurchaseController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $userManager = $container->get(UserManager::class);
        $purchaseManager = $container->get(PurchaseManager::class);

        return new UserInactivePurchaseController(
            $baseAuthManager,
            $userManager,
            $purchaseManager
        );
    }
}