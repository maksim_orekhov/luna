<?php
namespace Api\Purchase\Factory;

use Api\Purchase\ModerationPurchaseController;
use Application\Service\PurchaseManager;
use Application\Service\PurchaseModerationManager;
use Application\Service\UserManager;
use Core\Service\Base\BaseAuthManager;
use Interop\Container\ContainerInterface;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class ModerationPurchaseControllerFactory
 * @package Api\Controller\Purchase\Factory
 */
class ModerationPurchaseControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ModerationPurchaseController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $userManager = $container->get(UserManager::class);
        $purchaseModerationManager = $container->get(PurchaseModerationManager::class);
        $purchaseManager = $container->get(PurchaseManager::class);
        $rbacManager = $container->get(RbacManager::class);

        return new ModerationPurchaseController(
            $baseAuthManager,
            $userManager,
            $purchaseModerationManager,
            $purchaseManager,
            $rbacManager
        );
    }
}