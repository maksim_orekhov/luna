<?php
namespace Api\Purchase;

use Application\Entity\User;
use Application\Service\PurchaseManager;
use Application\Service\UserManager;
use Core\Controller\AbstractRestfulController;
use Core\Controller\Plugin\Message\MessageJsonModelTrait;
use Core\Service\Base\BaseAuthManager;
use ModuleRbac\Service\RbacManager;
use Zend\Paginator\Paginator;

/**
 * Class UserInactivePurchaseController
 * @package Api\Controller\Purchase
 */
class UserInactivePurchaseController extends AbstractRestfulController
{
    use MessageJsonModelTrait;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * User manager
     * @var UserManager
     */
    private $userManager;

    /**
     * @var PurchaseManager
     */
    private $purchaseManager;

    /**
     * ArchivePurchaseController constructor.
     * @param BaseAuthManager $baseAuthManager
     * @param UserManager $userManager
     * @param PurchaseManager $purchaseManager
     */
    public function __construct(BaseAuthManager $baseAuthManager, UserManager $userManager,
                                PurchaseManager $purchaseManager)
    {
        $this->baseAuthManager = $baseAuthManager;
        $this->userManager = $userManager;
        $this->purchaseManager = $purchaseManager;
    }

    /**
     * @return \Core\Service\TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     */
    public function getList()
    {
        $this->setOnlyAjax();
        $this->message()->setJsonStrategy(true);
        $page = (int)$this->params()->fromQuery('page', 1);
        $per_page = (int)$this->params()->fromQuery('per_page', 20);
        $user_id = (int)$this->params()->fromRoute('idUser', null);

        try {
            $user = null;
            if (null !== $this->baseAuthManager->getIdentity()) {
                /** @var User $user */
                $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            }
            if (null === $user || $user->getId() !== $user_id) {

                return $this->message()->error('Access is denied');
            }

            /** @var Paginator $paginatedPurchases */
            $paginatedPurchases = $this->purchaseManager->getInactivePurchasesAndPaginatorOutput($user, $page, $per_page);

            if (!$paginatedPurchases['purchases']) {

                return $this->message()->error('No purchases found');
            }

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $data = [
            'purchases' => $paginatedPurchases['purchases'] ?? null,
            'purchasesPaginator' => $paginatedPurchases['paginator'] ?? null
        ];

        return $this->message()->success('Inactive purchases for user', $data);
    }


    /**
     * @return array
     */
    private function setOnlyAjax()
    {
        // only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }
    }
}
