<?php
namespace Api\Purchase;

use Application\Entity\Purchase;
use Application\Entity\User;
use Application\Form\ArchivingForm;
use Application\Service\Output\PurchaseOutput;
use Application\Service\PurchaseManager;
use Application\Service\UserManager;
use Core\Controller\AbstractRestfulController;
use Core\Controller\Plugin\Message\MessageJsonModelTrait;
use Core\Service\Base\BaseAuthManager;
use ModuleRbac\Service\RbacManager;
use Zend\Paginator\Paginator;

/**
 * Class ArchivePurchaseController
 * @package Api\Controller\Purchase
 */
class ArchivePurchaseController extends AbstractRestfulController
{
    use MessageJsonModelTrait;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * User manager
     * @var UserManager
     */
    private $userManager;

    /**
     * @var PurchaseManager
     */
    private $purchaseManager;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * ArchivePurchaseController constructor.
     * @param BaseAuthManager $baseAuthManager
     * @param UserManager $userManager
     * @param PurchaseManager $purchaseManager
     * @param RbacManager $rbacManager
     */
    public function __construct(BaseAuthManager $baseAuthManager, UserManager $userManager,
                                PurchaseManager $purchaseManager, RbacManager $rbacManager)
    {
        $this->baseAuthManager = $baseAuthManager;
        $this->userManager = $userManager;
        $this->purchaseManager = $purchaseManager;
        $this->rbacManager = $rbacManager;
    }

    /**
     * @return \Core\Service\TwigViewModel|mixed|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     */
    public function getList()
    {
        $this->setOnlyAjax();
        $this->message()->setJsonStrategy(true);
        $page = (int)$this->params()->fromQuery('page', 1);
        $per_page = (int)$this->params()->fromQuery('per_page', 20);

        try {
            $user = null;
            if (null !== $this->baseAuthManager->getIdentity()) {
                // Get current user // Can throw Exception
                $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            }
            if (null === $user || !$this->rbacManager->hasRole($user, 'Operator')) {

                return $this->message()->accessDenied();
            }

            /** @var Paginator $paginatedPurchases */
            $paginatedPurchases = $this->purchaseManager->getArchivePurchasesAndPaginatorOutput(null, $page, $per_page);

            if (!$paginatedPurchases['purchases']) {

                return $this->message()->error('No purchases found');
            }
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $data = [
            'purchases' => $paginatedPurchases['purchases'] ?? null,
            'purchasesPaginator' => $paginatedPurchases['paginator'] ?? null,
        ];

        return $this->message()->success('Archival purchases', $data);
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return bool|\Core\Service\TwigViewModel|mixed|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function update($id, $data)
    {
        $this->setOnlyPostAndAjax();
        $this->message()->setJsonStrategy(true);

        try {
            $incomingData = $this->collectIncomingData();
            $id = (int)$incomingData['id'];

            $form = new ArchivingForm();
            $postData = $incomingData['postData'];
            // set data to form
            $form->setData($postData);
            // validate form
            if ($form->isValid()) {

                /** @var Purchase $purchase */
                $purchase = $this->purchaseManager->getPurchaseById($id);

                $user = null;
                if (null !== $this->baseAuthManager->getIdentity()) {
                    /** @var User $user */
                    $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
                }
                if (null === $user || $user !== $purchase->getUser()) {

                    return $this->message()->accessDenied();
                }

                $formData = $form->getData();

                // При попытке отправить в архив архивное объявление (которое уже в архиве)
                if (1 === $formData['archive'] && true === $purchase->isRemoved()) {

                    return $this->message()->error('The purchase is already in the archive');
                }
                // При попытке вернуть из архива не архивное объявление
                if (0 === $formData['archive'] && false === $purchase->isRemoved()) {

                    return $this->message()->error('The purchase is not in the archive');
                }

                // Отправка в архив / возврат из архива
                $this->purchaseManager->setArchive($purchase, $formData['archive']);
                // Get Output
                $purchaseOutput = $this->purchaseManager->getInsideOutput($purchase);

                $message = (true === $purchaseOutput['archive']) ? 'The purchase sent to archive' : 'The purchase returned from the archive';

                return $this->message()->success($message, ['purchase' => $purchaseOutput]);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @return array
     */
    private function setOnlyAjax()
    {
        // only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }
    }


    /**
     * @return array
     */
    private function setOnlyPostAndAjax()
    {
        // only ajax
        if (!$this->getRequest()->isXmlHttpRequest() || !$this->getRequest()->isPost()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }
    }
}
