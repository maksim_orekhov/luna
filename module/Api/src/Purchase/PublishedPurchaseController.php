<?php
namespace Api\Purchase;

use Application\Service\PurchaseManager;
use Application\Service\UserManager;
use Core\Controller\AbstractRestfulController;
use Core\Controller\Plugin\Message\MessageJsonModelTrait;
use Core\Service\Base\BaseAuthManager;
use Core\Service\TwigViewModel;
use ModuleRbac\Service\RbacManager;

/**
 * Class PublishedPurchaseController
 * @package Api\Controller\Purchase
 */
class PublishedPurchaseController extends AbstractRestfulController
{
    use MessageJsonModelTrait;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * User manager
     * @var UserManager
     */
    private $userManager;

    /**
     * @var PurchaseManager
     */
    private $purchaseManager;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * PublishedPurchaseController constructor.
     * @param BaseAuthManager $baseAuthManager
     * @param UserManager $userManager
     * @param PurchaseManager $purchaseManager
     * @param RbacManager $rbacManager
     */
    public function __construct(BaseAuthManager $baseAuthManager, UserManager $userManager, PurchaseManager $purchaseManager,
                                RbacManager $rbacManager)
    {
        $this->baseAuthManager = $baseAuthManager;
        $this->userManager = $userManager;
        $this->purchaseManager = $purchaseManager;
        $this->rbacManager = $rbacManager;
    }

    /**
     * @return TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     */
    public function getList()
    {
        $this->setOnlyAjax();
        $this->message()->setJsonStrategy(true);
        $page = (int)$this->params()->fromQuery('page', 1);
        $per_page = (int)$this->params()->fromQuery('per_page', 8);

        try {
            $user = null;
            if (null !== $this->baseAuthManager->getIdentity()) {
                // Get current user // Can throw Exception
                $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            }
            if (null === $user) {

                return $this->message()->accessDenied();
            }
            $is_operator = !$this->rbacManager->hasRole($user, 'Operator');

            $paginatedPurchases = $this->purchaseManager->getPublishedPurchasesAndPaginatorOutput($is_operator ? null : $user, $page, $per_page);

            if (!$paginatedPurchases['purchases']) {

                return $this->message()->error('No purchases found');
            }
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $data = [
            'purchases' => $paginatedPurchases['purchases'] ?? null,
            'purchasesPaginator' => $paginatedPurchases['paginator'] ?? null
        ];

        return $this->message()->success('Published purchase', $data);
    }

    /**
     * @return array
     */
    private function setOnlyAjax()
    {
        // only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }
    }
}
