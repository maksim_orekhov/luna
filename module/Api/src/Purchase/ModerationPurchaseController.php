<?php
namespace Api\Purchase;

use Application\Entity\Purchase;
use Application\Entity\Moderation;
use Application\Form\PurchaseModerationForm;
use Application\Service\Output\PurchaseOutput;
use Application\Service\PurchaseManager;
use Application\Service\PurchaseModerationManager;
use Application\Service\UserManager;
use Core\Controller\AbstractRestfulController;
use Core\Controller\Plugin\Message\MessageJsonModelTrait;
use Core\Exception\LogicException;
use Core\Service\Base\BaseAuthManager;
use Core\Service\TwigViewModel;
use ModuleRbac\Service\RbacManager;

/**
 * Class ModerationPurchaseController
 * @package Api\Controller\Purchase
 */
class ModerationPurchaseController extends AbstractRestfulController
{
    use MessageJsonModelTrait;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * User manager
     * @var UserManager
     */
    private $userManager;

    /**
     * @var PurchaseModerationManager
     */
    private $purchaseModerationManager;

    /**
     * @var PurchaseManager
     */
    private $purchaseManager;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * ModerationPurchaseController constructor.
     * @param BaseAuthManager $baseAuthManager
     * @param UserManager $userManager
     * @param PurchaseModerationManager $purchaseModerationManager
     * @param PurchaseManager $purchaseManager
     * @param RbacManager $rbacManager
     */
    public function __construct(BaseAuthManager $baseAuthManager, UserManager $userManager,
                                PurchaseModerationManager $purchaseModerationManager, PurchaseManager $purchaseManager,
                                RbacManager $rbacManager)
    {
        $this->baseAuthManager = $baseAuthManager;
        $this->userManager = $userManager;
        $this->purchaseModerationManager = $purchaseModerationManager;
        $this->purchaseManager = $purchaseManager;
        $this->rbacManager = $rbacManager;
    }

    /**
     * @return TwigViewModel|mixed|\Zend\Http\Response|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     */
    public function getList()
    {
        $this->setOnlyAjax();
        $this->message()->setJsonStrategy(true);
        $page = (int)$this->params()->fromQuery('page', 1);
        $per_page = (int)$this->params()->fromQuery('per_page', 20);

        try {
            $user = null;
            if (null !== $this->baseAuthManager->getIdentity()) {
                // Get current user // Can throw Exception
                $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            }
            if (null === $user || !$this->rbacManager->hasRole($user, 'Operator')) {

                return $this->message()->accessDenied();
            }

            $products = $this->purchaseManager->getModerationPurchasesAndPaginatorOutput(null, $page, $per_page);

            if (!$products['purchases']) {

                return $this->message()->error('No purchases found');
            }

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $data = [
            'purchases' => $products['purchases'] ?? null,
            'purchasesPaginator' => $products['paginator'] ?? null
        ];

        return $this->message()->success('Waiting moderation purchases', $data);
    }

    /**
     * @param mixed $id
     * @return TwigViewModel|mixed|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     */
    public function get($id)
    {
        $this->setOnlyAjax();
        $this->message()->setJsonStrategy(true);

        try {
            $user = null;
            if (null !== $this->baseAuthManager->getIdentity()) {
                // Get current user // Can throw Exception
                $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            }
            if (null === $user || !$this->rbacManager->hasRole($user, 'Operator')) {

                return $this->message()->error('Access is denied');
            }

            /** @var Purchase $purchase */
            $purchase = $this->purchaseManager->getPurchaseById((int)$id);
            // Get Output
            $purchaseOutput = $this->purchaseManager->getInsideOutput($purchase);
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $data = [
            'purchase' => $purchaseOutput
        ];

        return $this->message()->success('Purchase single', $data);
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return array|bool|TwigViewModel|mixed|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function update($id, $data)
    {
        $this->setOnlyPostAndAjax();
        $this->message()->setJsonStrategy(true);

        $user = null;
        if (null !== $this->baseAuthManager->getIdentity()) {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        }
        if (null === $user || !$this->rbacManager->hasRole($user, 'Operator')) {

            return $this->message()->error('Access is denied');
        }

        try {
            $formDataInit = $this->processFormDataInit();

            /** @var Purchase $purchase */
            $purchase = $formDataInit['purchase'];
            /** @var Moderation $moderation */
            $moderation = $purchase->getModerations()->last();
            if (null !== $moderation->getResolutionDate()) {

                return $this->message()->error('Moderation already has resolution');
            }

            if (isset($data['approved']) && $data['approved'] === 1) {
                /** @var PurchaseModerationForm $form */
                $form = new PurchaseModerationForm();
            } else {
                /** @var PurchaseModerationForm $form */
                $form = new PurchaseModerationForm('decline');
            }

            // set data to form
            $form->setData($data);
            // validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                // Save Form Data
                $moderation = $this->purchaseModerationManager->saveFormData($moderation, $formData);
                // Get Output
                $purchaseOutput = $this->purchaseManager->getInsideOutput($purchase);

                return $this->message()->success('Purchase', ['purchase' => $purchaseOutput]);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @return array|bool|TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function formInitAction()
    {
        $this->setOnlyAjax();
        $this->message()->setJsonStrategy(true);

        try {
            $user = null;
            if (null !== $this->baseAuthManager->getIdentity()) {
                // Get current user // Can throw Exception
                $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            }
            if (null === $user || !$this->rbacManager->hasRole($user, 'Operator')) {

                return $this->message()->error('Access is denied');
            }

            $formDataInit = $this->processFormDataInit();

            if (isset($formDataInit['purchase'])) {
                /** @var Purchase $purchase */
                $purchase = $formDataInit['purchase'];
                $purchaseOutput = $this->purchaseManager->getInsideOutput($purchase);

                $formDataInit['purchase'] = $purchaseOutput;
            }
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        return $this->message()->success('formInit', $formDataInit);
    }

    /**
     * @return array
     * @throws LogicException
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        $product = null;
        if (isset($incomingData['id']) || isset($incomingData['idPurchase'])) {
            $id = $incomingData['id'] ?? $incomingData['idPurchase'];
            /** @var Purchase $purchase */
            $purchase = $this->purchaseManager->getPurchaseById((int)$id);
        }

        if (null === $purchase) {

            throw new LogicException(null, 'PURCHASE_NOT_FOUND');
        }

        return [
            'purchase' => $purchase
        ];
    }

    /**
     * @return array
     */
    private function setOnlyAjax()
    {
        // only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }
    }

    /**
     * @return array
     */
    private function setOnlyPostAndAjax()
    {
        // only ajax
        if (!$this->getRequest()->isXmlHttpRequest() || !$this->getRequest()->isPost()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }
    }
}
