<?php
namespace Api\Category;

use Application\Service\CategoryManager;
use Core\Controller\AbstractRestfulController;
use Core\Controller\Plugin\Message\MessageJsonModelTrait;
use Core\Service\TwigViewModel;

/**
 * Class CategoryController
 * @package Api\Category
 */
class CategoryController extends AbstractRestfulController
{
    use MessageJsonModelTrait;
    const TYPE_EXTENDED = 'extended';
    const TYPE_NORMAL = 'normal';
    /**
     * @var CategoryManager
     */
    private $categoryManager;

    /**
     * CategoryController constructor.
     * @param CategoryManager $categoryManager
     */
    public function __construct(CategoryManager $categoryManager)
    {
        $this->categoryManager = $categoryManager;
    }

    /**
     * @return bool|TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function getList()
    {
        $this->message()->setJsonStrategy(true);
        try {
            $parent_id = $this->params()->fromQuery('parent', null);
            $type = $this->params()->fromQuery('type', self::TYPE_NORMAL);

            $categories = $this->categoryManager->getCategories($parent_id);
            $sub_category = $type === self::TYPE_EXTENDED;
            $categoriesOutput = $this->categoryManager->getCategoriesOutput($categories, $sub_category);

            return $this->message()->success('category list', $categoriesOutput);
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }
}
