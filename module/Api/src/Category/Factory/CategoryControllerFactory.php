<?php
namespace Api\Category\Factory;

use Api\Category\CategoryController;
use Application\Service\CategoryManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class CategoryControllerFactory
 * @package Api\Category\Factory
 */
class CategoryControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return \Api\Category\CategoryController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $categoryManager = $container->get(CategoryManager::class);

        return new CategoryController(
            $categoryManager
        );
    }
}