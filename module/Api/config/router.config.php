<?php
namespace Api;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            //
            // Purchase
            //
            'moderation-purchases' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/purchase/moderation',
                    'defaults' => [
                        'controller' => Purchase\ModerationPurchaseController::class,
                    ],
                ]
            ],
            'moderation-purchase-single' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/purchase/moderation/:id',
                    'constraints' => [
                        'id'     => '\d+'
                    ],
                    'defaults' => [
                        'controller' => Purchase\ModerationPurchaseController::class,
                    ],
                ],
            ],
            'declined-purchases' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/purchase/declined',
                    'defaults' => [
                        'controller' => Purchase\DeclinedPurchaseController::class,
                    ],
                ]
            ],
            'published-purchases' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/published-purchase',
                    'defaults' => [
                        'controller' => Purchase\PublishedPurchaseController::class,
                    ],
                ]
            ],
            'archive-purchases' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/archive-purchase',
                    'defaults' => [
                        'controller' => Purchase\ArchivePurchaseController::class,
                    ],
                ]
            ],
            'archive-purchases-single' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/archive-purchase/:id',
                    'constraints' => [
                        'id'     => '\d+'
                    ],
                    'defaults' => [
                        'controller' => Purchase\ArchivePurchaseController::class,
                    ],
                ],
            ],
            'archive-purchases-for-user' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/user/:idUser/archive-purchase',
                    'constraints' => [
                        'idUser' => '\d+',
                    ],
                    'defaults' => [
                        'controller' => Purchase\UserArchivePurchaseController::class,
                    ],
                ]
            ],
            'inactive-purchases-for-user' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/user/:idUser/inactive-purchase',
                    'constraints' => [
                        'idUser' => '\d+',
                    ],
                    'defaults' => [
                        'controller' => Purchase\UserInactivePurchaseController::class,
                    ],
                ]
            ],

//            'moderation-purchase-form-init' => [
//                'type' => Segment::class,
//                'options' => [
//                    'route'    => '/purchase/moderation/form-init',
//                    'defaults' => [
//                        'controller' => Controller\Purchase\ModerationPurchaseController::class,
//                        'action' => 'formInit',
//                    ],
//                    'strategies' => [
//                        'ViewJsonStrategy',
//                    ],
//                ]
//            ],
            //
            // category
            //
            'api-categories' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/api/categories',
                    'defaults' => [
                        'controller' => Category\CategoryController::class,
                    ]
                ],
            ],
        ],
    ],
];
