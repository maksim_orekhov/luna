<?php
namespace Api;

return [
    'controllers' => [
        'factories' => [
            // Purchase
            Purchase\ModerationPurchaseController::class => Purchase\Factory\ModerationPurchaseControllerFactory::class,
            Purchase\DeclinedPurchaseController::class => Purchase\Factory\DeclinedPurchaseControllerFactory::class,
            Purchase\PublishedPurchaseController::class => Purchase\Factory\PublishedPurchaseControllerFactory::class,
            Purchase\ArchivePurchaseController::class => Purchase\Factory\ArchivePurchaseControllerFactory::class,
            Purchase\UserArchivePurchaseController::class => Purchase\Factory\UserArchivePurchaseControllerFactory::class,
            Purchase\UserInactivePurchaseController::class => Purchase\Factory\UserInactivePurchaseControllerFactory::class,
            // Category
            Category\CategoryController::class => Category\Factory\CategoryControllerFactory::class,
        ],
    ],
];
