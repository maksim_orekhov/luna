<?php
namespace Api;

return [
    // The 'access_filter' key is used by the Application module to restrict or permit
    // access to certain controller actions for unauthorized visitors.
    'access_filter' => [
        'options' => [
            'mode' => 'permissive',     // все, что не запрещено, разрешено
            //'mode' => 'restrictive',  // все, что не разрешено, запрещено
        ],
        'controllers' => [
            Purchase\ArchivePurchaseController::class => [
                [
                    'actions' => [
                        'getList'
                    ],
                    'allows' => ['#Operator'],
                ],
                [
                    'actions' => [
                        'update'
                    ],
                    'allows' => ['#Verified'],
                ],
            ],
            Purchase\DeclinedPurchaseController::class => [
                [
                    'actions' => [
                        'getList'
                    ],
                    'allows' => ['#Operator'],
                ],
            ],
            Purchase\ModerationPurchaseController::class => [
                [
                    'actions' => [
                        'getList',
                        'get',
                        'update',
                        'formInit',
                    ],
                    'allows' => ['#Operator'],
                ],
            ],
            Purchase\ModerationPurchaseController::class => [
                [
                    'actions' => [
                        'getList',
                        'get',
                        'update',
                        'formInit',
                    ],
                    'allows' => ['#Operator'],
                ],
            ],
            Purchase\PublishedPurchaseController::class => [
                [
                    'actions' => [
                        'getList',
                    ],
                    'allows' => ['#Verified'],
                ],
            ],
            Purchase\UserArchivePurchaseController::class => [
                [
                    'actions' => [
                        'getList',
                    ],
                    'allows' => ['#Verified'],
                ],
            ],
            Purchase\UserInactivePurchaseController::class => [
                [
                    'actions' => [
                        'getList',
                    ],
                    'allows' => ['#Verified'],
                ],
            ],
        ]
    ],
];
