<?php

namespace ModuleFileManagerTest\Service;

use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use \Exception;
use ModuleFileManager\Service\FileManager;
use ModuleFileManager\Entity\File;

class FileManagerTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    private $main_project_url;
    private $main_upload_folder;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var FileManager
     */
    private $fileManager;

    public function setUp()
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        //$configOverrides = [];

        $this->setApplicationConfig(ArrayUtils::merge(
            ArrayUtils::merge(
                include __DIR__ . '/../../../../config/application.config.php',
                include __DIR__ . '/../../../../config/autoload/global.php'
            ),
            include __DIR__ . '/../../../../config/autoload/local.php'
        ));

        parent::setUp();

        $config = $this->getApplicationConfig();

        $_SERVER['DOCUMENT_ROOT'] = $config['server_document_root'];

        $this->main_project_url = $config['main_project_url'];
        $this->main_upload_folder = $config['file-management']['main_upload_folder'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->fileManager = $serviceManager->get(FileManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollback();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * Не доступность загрузки файла с не валидными данными
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testWriteWithInvalidParams()
    {
        $file_name = 'text_file_for_test.txt';
        $path = "/test";

        // Invalid params
        $params = [
            'file_source'   => null,
            'file_name'     => $file_name,
            'file_type'     => 'text/plain',
            'file_size'     => 100,
            'path'          => $path
        ];

        try {
            $this->fileManager->write($params);
            $exception = false;
        } catch (\Throwable $t) {
            $exception = true;
        }
        $this->assertTrue($exception);

        /** @var File $file */
        $file = $this->entityManager->getRepository(File::class)
            ->findOneBy(['originName' => $file_name]);
        $this->assertEquals(null, $file);
    }

    /**
     * Доступность загрузки файла с валидными данными
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testWriteWithValidParams()
    {
        $file_name = 'text_file_for_test.txt';
        $path = "/test";

        // Invalid params
        $params = [
            'file_source'   => "./module/ModuleFileManager/test/files/".$file_name,
            'file_name'     => $file_name,
            'file_type'     => 'text/plain',
            'file_size'     => filesize("./module/ModuleFileManager/test/files/".$file_name),
            'path'          => $path
        ];

        $this->fileManager->write($params);
        /** @var File $file */
        $file = $this->entityManager->getRepository(File::class)
            ->findOneBy(['originName' => $file_name]);

        $this->assertNotNull($file);
        $this->assertEquals($file_name, $file->getOriginName());
        $this->assertFileExists("./".$this->main_upload_folder.$path."/".$file->getName());

        return $file->getId();
    }

    /**
     * Удаление файла
     *
     * @param int $file_id
     * @depends testWriteWithValidParams
     * @expectedException Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testDelete(int $file_id)
    {
        $file = $this->fileManager->getFileById($file_id);
        $this->assertInstanceOf('File', $file);

        $this->fileManager->delete($file_id);

        $this->assertFileNotExists("./".$this->main_upload_folder.$file->getPath()."/".$file->getName());
        // Must throw Exception
        try {
            $this->fileManager->getFileById($file_id);
            $exception = false;
        } catch (\Throwable $t) {
            $exception = true;
        }
        $this->assertFalse($exception);
    }

    /**
     * Попытка удалени несуществующего обеъкта файла
     * Должно вернуть Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testDeleteNonExistingFile()
    {        // Must throw Exception
        try {
            $this->fileManager->delete(9999999999);
            $exception = false;
            $exceptionMessage = null;
        } catch (\Throwable $t) {
            $exception = true;
            $exceptionMessage = $t->getMessage();
        }
        $this->assertTrue($exception);
        $this->assertEquals('Can not find File in database by Id', $exceptionMessage);
    }

    /**
     * Чтение существующего файла по entity File
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testReadToEntityFile()
    {
        $file_name = 'text_file_for_test.txt';
        //file for pass to the method read
        $file_content = "Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test ";
        $path = "/test";

        // Invalid params
        $params = [
            'file_source'   => "./module/ModuleFileManager/test/files/".$file_name,
            'file_name'     => $file_name,
            'file_type'     => 'text/plain',
            'file_size'     => filesize("./module/ModuleFileManager/test/files/".$file_name),
            'path'          => $path
        ];

        $this->fileManager->write($params);
        /** @var File $file */
        $file = $this->entityManager->getRepository(File::class)
            ->findOneBy(['originName' => $file_name]);

        $file_data = $this->fileManager->readToEntityFile($file);

        $this->assertEquals($file_content, $file_data);
        $this->assertFileExists("./".$this->main_upload_folder.$path."/".$file->getName());
    }


    /**
     * Чтение файла
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testRead()
    {
        $file_content = "Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test ";
        $file_name = 'text_file_for_test.txt';

        // Set of data for pass to write method
        $file_path = "./module/ModuleFileManager/test/files/".$file_name;

        $this->assertFileExists($file_path);

        $file = $this->fileManager->read($file_path);
        $this->assertEquals($file_content, $file);
    }

    /**
     * Попытка чтения несуществующего файла
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testReadNotFileExists()
    {
        //NotFileExist
        $file_name = 'NotFileExist.txt';

        // Set of data for pass to write method
        $file_path = "./module/ModuleFileManager/test/files/".$file_name;

        $this->assertFileNotExists($file_path);
        try {
            $this->fileManager->read($file_path);
            $exception = false;
        } catch (\Throwable $t) {
            $exception = true;
        }
        $this->assertTrue($exception);
    }
}
