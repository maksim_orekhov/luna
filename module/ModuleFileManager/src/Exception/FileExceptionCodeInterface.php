<?php
namespace ModuleFileManager\Exception;

interface FileExceptionCodeInterface
{
    /**
     * префикс FILE, для сообшений суфикс _MESSAGE
     */

    const FILE_NOT_READABLE_OR_NOT_EXIST = 'FILE_NOT_READABLE_OR_NOT_EXIST';
    const FILE_NOT_READABLE_OR_NOT_EXIST_MESSAGE = 'The file is not readable or does not exist.';

    const FILE_COULD_NOT_BE_DISPLAYED = 'FILE_COULD_NOT_BE_DISPLAYED';
    const FILE_COULD_NOT_BE_DISPLAYED_MESSAGE = 'File could not be displayed.';

    const FILE_NAME_NOT_FOUND_FROM_ROUTE = 'FILE_NAME_NOT_FOUND_FROM_ROUTE';
    const FILE_NAME_NOT_FOUND_FROM_ROUTE_MESSAGE = 'Not found file name from route.';

    const FILE_NOT_FOUND_IN_COMMON_DIRECTORY = 'FILE_NOT_FOUND_IN_COMMON_DIRECTORY';
    const FILE_NOT_FOUND_IN_COMMON_DIRECTORY_MESSAGE = 'Not found file in common directory.';
}
