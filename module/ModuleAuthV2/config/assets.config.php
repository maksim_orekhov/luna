<?php
namespace ModuleAuthV2;

return [
    // The 'access_filter' key is used by the Application module to restrict or permit
    // access to certain controller actions for unauthorized visitors.
    'access_filter' => [
        'options' => [
            'mode' => 'permissive',     // все, что не запрещено, разрешено
            //'mode' => 'restrictive',  // все, что не разрешено, запрещено
        ],
        'controllers' => [
            Controller\AuthController::class => [
                [
                    'actions' => [
                        'editLogin',
                    ],
                    'allows' => ['@']
                ],
            ],
            Controller\PhoneController::class => [
                [
                    'actions' => [
                        'create',
                        'edit',
                        'confirmationEditCode',
                        'confirmationEditCodeResend'
                    ],
                    'allows' => ['@']
                ],
            ],
//            Controller\UserRegistrationController::class => [
//                [
//                    'actions' => [
//                        'delete',
//                    ],
//                    'allows' => ['#Administrator']
//                ],
//            ],
        ],
    ],
];