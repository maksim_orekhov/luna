<?php
namespace ModuleAuthV2;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'auth-csrf-token' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/auth/csrf-token',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'csrfToken',
                    ],
                ],
            ],
            'login' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/login',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'login',
                    ],
                ],
//                'may_terminate' => true,
//                'child_routes' => [
//                    'edit' => [
//                        'type' => Literal::class,
//                        'options' => [
//                            'route' => '/edit',
//                            'defaults' => [
//                                'controller' => Controller\AuthController::class,
//                                'action' => 'editLogin',
//                            ],
//                        ],
//                    ],
//                ]
            ],
            'logout' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/logout',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'logout',
                    ],
                ],
            ],
            'not-authorized' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/not-authorized',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'notAuthorized',
                    ],
                ],
            ],
            'access-denied' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/access-denied',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'accessDenied',
                    ],
                ],
            ],
            ///////////////////////////////////////
            /////////////// forgot ////////////////
            ///////////////////////////////////////
            'forgot' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/forgot',
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'password-reset' => [
                        'type' => Literal::class,
                        'options' => [
                            'route'    => '/password/reset',
                            'defaults' => [
                                'controller' => Controller\PasswordResetController::class,
                                'action'     => 'resetPassword',
                            ],
                        ],
                    ],
                    'reset-confirm' => [
                        'type' => Literal::class,
                        'options' => [
                            'route'    => '/password/reset/confirm',
                            'defaults' => [
                                'controller' => Controller\PasswordResetController::class,
                                'action'     => 'confirmationCode',
                            ],
                        ],
                    ],
                    'reset-code-resend' => [
                        'type' => Literal::class,
                        'options' => [
                            'route'    => '/password/reset/code-resend',
                            'defaults' => [
                                'controller' => Controller\PasswordResetController::class,
                                'action'     => 'confirmationCodeResend',
                            ],
                        ],
                    ],
                    'password-set' => [
                        'type' => Literal::class,
                        'options' => [
                            'route'    => '/password/set',
                            'defaults' => [
                                'controller' => Controller\PasswordResetController::class,
                                'action'     => 'setPassword',
                            ],
                        ],
                    ],
                ],
            ],
            ///////////////////////////////////////
            //////////// registration /////////////
            ///////////////////////////////////////
            'register' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/register',
                    'defaults' => [
                        'controller' => Controller\UserRegistrationController::class,
                        'action' => 'create',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'check' => [
                        'type'    => Segment::class,
                        'options' => [
                            'route'    => '/check',
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'email' => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'    => '/email',
                                    'defaults' => [
                                        'controller' => Controller\UserRegistrationCheckedController::class,
                                        'action'     => 'checkIsEmailUnique',
                                    ],
                                    'strategies' => [
                                        'ViewJsonStrategy',
                                    ],
                                ],
                            ],
                        ]
                    ],
                ]
            ],
            ///////////////////////////////////////
            //////////////// email ////////////////
            ///////////////////////////////////////
            'email' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/email',
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'confirm' => array(
                        'type' => Segment::class,
                        'options' => array(
                            'route'    => '/confirm',
                            'defaults' => array(
                                'controller' => Controller\EmailController::class,
                                'action' => 'confirm',
                            ),
                        ),
                        'strategies' => [
                            'ViewJsonStrategy',
                        ],
                    ),
                    'edit' => array(
                        'type' => Segment::class,
                        'options' => array(
                            'route'    => '/edit',
                            'defaults' => array(
                                'controller' => Controller\EmailController::class,
                                'action' => 'edit',
                            ),
                        ),
                        'strategies' => [
                            'ViewJsonStrategy',
                        ],
                    ),
                    'resend' => array(
                        'type' => Segment::class,
                        'options' => array(
                            'route'    => '/resend',
                            'defaults' => array(
                                'controller' => Controller\EmailController::class,
                                'action' => 'resend',
                            ),
                        ),
                    ),
                ]
            ],
            ///////////////////////////////////////
            //////////////// phone ////////////////
            ///////////////////////////////////////
            'phone' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/phone',
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'create' => [
                        'type'    => Literal::class,
                        'options' => [
                            'route'    => '/create',
                            'defaults' => [
                                'controller' => Controller\PhoneController::class,
                                'action' => 'create',
                            ],
                        ],
                    ],
                    'edit' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/edit',
                            'defaults' => [
                                'controller' => Controller\PhoneController::class,
                                'action' => 'edit',
                            ],
                        ],
                    ],
                    'confirm' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/confirm',
                            'defaults' => [
                                'controller' => Controller\PhoneController::class,
                                'action' => 'confirm',
                            ],
                        ],
                    ],
                    'edit-confirm' => [
                        'type'    => Literal::class,
                        'options' => [
                            'route' => '/edit-confirm',
                            'defaults' => [
                                'controller' => Controller\PhoneController::class,
                                'action' => 'confirmationEditCode',
                            ],
                            'strategies' => [
                                'ViewJsonStrategy',
                            ],
                        ],
                    ],
                    'code-resend' => [
                        'type'    => Literal::class,
                        'options' => [
                            'route' => '/confirm/code-resend',
                            'defaults' => [
                                'controller' => Controller\PhoneController::class,
                                'action' => 'confirmationCodeResend',
                            ],
                        ],
                    ],
                    'edit-code-resend' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/confirm/edit-code-resend',
                            'defaults' => [
                                'controller' => Controller\PhoneController::class,
                                'action' => 'confirmationEditCodeResend',
                            ],
                        ],
                    ],
                ]
            ],
        ],
    ],
];