<?php
namespace ModuleAuthV2;

return [
    'service_manager' => [
        'factories' => [
            //provider
            Provider\AuthServiceProvider::class => Provider\Factory\AuthServiceProviderFactory::class,
            // Adapters
            Adapter\AuthAdapter::class => Adapter\Factory\AuthAdapterFactory::class,
            Service\RegistrationManager::class => Service\Factory\RegistrationManagerFactory::class,
        ],
    ],
];