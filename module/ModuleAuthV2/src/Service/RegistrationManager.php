<?php
namespace ModuleAuthV2\Service;

use Application\Entity\User;
use Core\EventManager\AuthEventProvider as AuthEvent;
use Core\Exception\LogicException;
use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BaseUserManager;
use DateTime;
use Doctrine\ORM\EntityManager;
use Zend\EventManager\EventManager;
use Zend\Math\Rand;

class RegistrationManager extends BaseUserManager
{
    const MAX_RECURSION_LEVEL = 100;
    /**
     * @var EventManager
     */
    private $eventManager;
    /**
     * @var BaseEmailManager
     */
    private $baseEmailManager;

    /**
     * RegistrationManager constructor.
     * @param EntityManager $entityManager
     * @param BaseEmailManager $baseEmailManager
     * @param EventManager $eventManager
     */
    public function __construct(EntityManager $entityManager,
                                EventManager $eventManager,
                                BaseEmailManager $baseEmailManager)
    {
        parent::__construct($entityManager);

        $this->eventManager = $eventManager;
        $this->baseEmailManager = $baseEmailManager;
    }

    /**
     * @param $data
     * @return User
     * @throws LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addNewUser($data): User
    {
        // Проверка уникальности email на стороне сервера
        if(!$this->baseEmailManager->checkEmailUnique($data['email'])){

            throw new LogicException(null, LogicException::EMAIL_ALREADY_EXISTS);
        }
        // Генерация уникального login c проверкой на не совпадение с базой
        $login = $this->generateLoginUnique();
        $user = new User();
        $user->setLogin($login);
        $user->setName($login);
        $user->setPassword($this->createPassword($data['password']));
        $user->setCreated(new DateTime('now'));
        $user->setIsBanned(false);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        if (!$user->getId()) {
            throw new LogicException(null, LogicException::USER_CREATED_FAILED);
        }

        // create new email and set to user
        $email = $this->baseEmailManager->addNewEmail($data['email']);
        $this->setEmailToUser($user, $email);


        $this->eventManager->trigger(AuthEvent::EVENT_SET_ROLE_TO_USER, $this, [
            'user' => $user,
            'role' => 'Unverified'
        ]);

        return $user;
    }

    /**
     * @param int $recursion_level
     * @return string
     * @throws LogicException
     */
    private function generateLoginUnique($recursion_level = 0): string
    {

        if ($recursion_level >= self::MAX_RECURSION_LEVEL) {

            throw new LogicException(null, LogicException::USER_GENERATION_UNIQUE_LOGIN_FAILED);
        }

        $login = $this->generateRandLogin();
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['login' => $login]);

        if ($user !== null) {
            //// если User с таким логином существует запускаем повторную генерацию ////
            return $this->generateLoginUnique(++$recursion_level);
        }

        return $login;
    }

    /**
     * @return string
     */
    private function generateRandLogin(): string
    {
        return uniqid('user_', false);
    }
}