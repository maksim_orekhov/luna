<?php
namespace ModuleAuthV2\Controller;

use Core\Controller\AbstractRestfulController;
use Core\Entity\Interfaces\EmailInterface;
use Core\Entity\Interfaces\UserInterface;
use Core\Exception\LogicException;
use Core\Provider\Token\EncodedTokenProviderInterface;
use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BaseTokenManager;
use Core\Service\Base\BaseUserManager;
use Core\Service\CodeOperationInterface;
use Core\Service\SessionContainerManager;
use Core\Service\TwigViewModel;
use ModuleAuthV2\Form\EmailEditForm;
use Core\Form\OperationConfirmForm;
use Zend\EventManager\EventManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\Json\Json;
use Core\Service\Base\BaseAuthManager;
use Core\EventManager\AuthEventProvider as AuthEvent;

/**
 * Class EmailController
 * @package ModuleAuthV2\Controller
 */
class EmailController extends AbstractRestfulController implements CodeOperationInterface
{
    const ERROR_NO_VALID_EMAIL_PROVIDED = 'No valid email provided';
    const SUCCESS_EMAIL_CHANGE          = 'Change email sent';
    const SUCCESS_EMAIL_RESENT          = 'Resending email sent';
    const SUCCESS_CSRF_TOKEN            = 'CSRF token return';
    const SUCCESS_EMAIL_CONFIRM         = 'Email has been confirmed';
    const SUCCESS_CHECK_EMAIL_EXISTS    = 'Email exists';
    const ERROR_CHECK_EMAIL_EXISTS      = 'Email does not exists';
    const ERROR_NO_VALID_DATA_PROVIDED  = 'No valid data provided';

    /**
     * @var BaseUserManager
     */
    private $baseUserManager;
    /**
     * @var SessionContainerManager
     */
    private $sessionContainerManager;
    /**
     * @var EncodedTokenProviderInterface
     */
    private $encodedTokenProvider;
    /**
     * @var BaseAuthManager;
     */
    private $baseAuthManager;
    /**
     * @var BaseEmailManager
     */
    private $baseEmailManager;
    /**
     * @var BaseTokenManager
     */
    private $baseTokenManager;
    /**
     * @var EventManager
     */
    private $eventManager;
    /**
     * @var array
     */
    private $config;

    /**
     * EmailController constructor.
     * @param BaseUserManager $baseUserManager
     * @param SessionContainerManager $sessionContainerManager
     * @param EncodedTokenProviderInterface $encodedTokenProvider
     * @param BaseAuthManager $baseAuthManager
     * @param BaseEmailManager $baseEmailManager
     * @param BaseTokenManager $baseTokenManager
     * @param EventManager $eventManager
     * @param array $config
     */
    public function __construct(BaseUserManager $baseUserManager,
                                SessionContainerManager $sessionContainerManager,
                                EncodedTokenProviderInterface $encodedTokenProvider,
                                BaseAuthManager $baseAuthManager,
                                BaseEmailManager $baseEmailManager,
                                BaseTokenManager $baseTokenManager,
                                EventManager $eventManager,
                                array $config)
    {
        $this->baseUserManager          = $baseUserManager;
        $this->sessionContainerManager  = $sessionContainerManager;
        $this->encodedTokenProvider     = $encodedTokenProvider;
        $this->baseAuthManager          = $baseAuthManager;
        $this->baseEmailManager         = $baseEmailManager;
        $this->baseTokenManager         = $baseTokenManager;
        $this->eventManager             = $eventManager;
        $this->config                   = $config;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Zend\Form\Exception\DomainException
     * @throws \Zend\Form\Exception\InvalidArgumentException
     * @throws \Exception
     */
    public function editAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        $isPost = $this->getRequest()->isPost();

        // Rbac настроен так, что не пустит в этот экшен неавторизованного пользователя.
        try {
            /** @var UserInterface $user */ // Can throw Exception
            $user = $this->baseUserManager->getUserByLogin($this->baseAuthManager->getIdentity());
            if (!$user) {

                throw new LogicException(null, LogicException::USER_NOT_FOUND);
            }
            /** @var \DateTime $tokenCreatedDate */
            $tokenCreatedDate = $user->getEmail()->getConfirmationTokenCreationDate();
        }
        catch (\Throwable $t) {
            // Ajax -
            if($isAjax) {
                return $this->message()->error($t->getMessage());
            }
            // Http -
            return $this->redirect()->toRoute('not-authorized');
        }
        // Code checking
        try {
            // Form creation
            $form = new EmailEditForm();
            $form->setData([
                'current_email' => strtolower($user->getEmail()->getEmail()),
            ]);

            // Check if user has submitted the form
            if ($isPost) {
                // Get data from request
                if ($isAjax) {
                    // Ajax -
                    $content = $this->getRequest()->getContent();
                    $data = Json::decode($content, Json::TYPE_ARRAY);
                } else {
                    // Http -
                    $data = $this->params()->fromPost();
                }
                // К нижнему регистру
                if(array_key_exists('new_email', $data)) {
                    $data['new_email'] = strtolower($data['new_email']);
                }
                if(array_key_exists('current_email', $data)) {
                    $data['current_email'] = strtolower($data['current_email']);
                }

                if ($data['current_email'] === $data['new_email']) {

                    throw new LogicException(null, LogicException::EMAIL_IS_SAME_AS_EMAIL_IN_PROFILE);
                }

                $form->setData($data);
                if($form->isValid()) {
                    $data = $form->getData();
                    /** @var EmailInterface $currentEmail */
                    $currentEmail = $this->baseEmailManager->getEmailByValue($data['current_email']);
                    // Check whether user provide correct email
                    if($user->getEmail() !== $currentEmail) {

                        throw new LogicException(null, LogicException::EMAIL_ENTERED_NOT_BELONG_USER);
                    }
                    // If email is not unique
                    if( !$this->baseEmailManager->checkEmailUnique($data['new_email']) ) {

                        throw new LogicException(null, LogicException::EMAIL_NOT_UNIQUE);
                    }

                    $this->baseEmailManager->setNewValueToEmail($currentEmail, $data['new_email']);
                    $this->eventManager->trigger(AuthEvent::EVENT_VERIFY_USER_CONFIRMS, $this, [
                        'identity' => $user->getLogin()
                    ]);
                    // Provide encoded token (Create token, create mail and send mail)
                    $this->encodedTokenProvider->provideToken($user);
                    // Ajax -
                    if ($isAjax) {
                        return $this->message()->success(self::SUCCESS_EMAIL_CHANGE );
                    }
                    // Http -
                    return $this->redirect()->toRoute('email/confirm');
                }
                // $form->isValid failed
                if($isAjax){

                    return $this->message()->invalidFormData($form->getMessages());
                }
            }
        }
        catch(\Throwable $t) {
            return $this->message()->exception($t);
        }
        // Ajax - csrf token
        if($isAjax){
            return $this->message()->success('CSRF token return', [
                'csrf' => $form->get('csrf')->getValue(),
                'token_created_date' => $tokenCreatedDate ? $tokenCreatedDate->getTimestamp() : null,
                'email_confirmation_token_request_period' => $this->config['tokens']['email_confirmation_token_request_period']
            ]);
        }
        $form->prepare();
        // Html -
        $view = new TwigViewModel([
            'formEmail' => $form,
            'token_created_date' => $tokenCreatedDate ? $tokenCreatedDate->getTimestamp() : null,
            'email_confirmation_token_request_period' => $this->config['tokens']['email_confirmation_token_request_period']
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * Email confirmation
     *
     * @return \Zend\Http\Response|JsonModel|ViewModel
     * @throws \Exception
     */
    public function confirmAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        // Trying to confirm email
        try {
            $data = $this->collectIncomingData();
            if (\is_array($data['queryData']) && array_key_exists('token', $data['queryData']) ) {
                $encoded_token = $data['queryData']['token'];
            } elseif(\is_array($data['postData']) && array_key_exists('confirmation_token', $data['postData'])) {
                $encoded_token = $data['postData']['confirmation_token'];
            } else {
                $encoded_token = null;
            }

            if($encoded_token) {
                $email = $this->baseTokenManager->validateEmailConfirmationToken($encoded_token);
                $user = $this->baseUserManager->getUserByEmail($email);
                if (!$user) {

                    throw new LogicException(null, LogicException::USER_NOT_FOUND);
                }
                $this->eventManager->trigger(AuthEvent::EVENT_VERIFY_USER_CONFIRMS, $this, [
                    'identity' => $user->getLogin()
                ]);
//                if ( $email->getIsVerified() ) {
//                    //set trigger verify email
//                    $this->eventManager->trigger(AuthEvent::EVENT_EMAIL_CONFIRMED, $this, [
//                        'email_confirmed_params' => ['user' => $user]
//                    ]);
//                }
                if (! $this->baseAuthManager->userAutoLogin($user) ) {

                    throw new LogicException(null, LogicException::USER_AUTHORIZATION_FAILED);
                }
                // If Ajax
                if ($isAjax){
                    // Return Json with SUCCESS status
                    return $this->message()->success(self::SUCCESS_EMAIL_CONFIRM);
                }
                // Redirect to Profile
                return $this->redirect()->toRoute('profile');
            }
        } catch (\Throwable $t) {

            return $this->message()->exception($t);
        }
        // If no $encoded_token passed
        if ($this->baseAuthManager->getIdentity()) {
            // Если авторизован
            $user_login = $this->baseAuthManager->getIdentity();
        } else {
            $user_login = $this->sessionContainerManager->getSessionVar('unconfirmed_user');
        }

        try {
            /** @var UserInterface $user */
            $user = $this->baseUserManager->getUserByLogin($user_login);
            /** @var \DateTime $tokenCreatedDate */
            $tokenCreatedDate = $user->getEmail()->getConfirmationTokenCreationDate();
        }
        catch (\Throwable $t) {
            $user = null;
            $tokenCreatedDate = null;
        }

        $is_email_token_expired = null;

        if ($user && $user->getEmail()->getConfirmationToken()) {
            /** @var \DateTime $confirmationTokenCreationDate */
            $confirmationTokenCreationDate = $user->getEmail()->getConfirmationTokenCreationDate();
            $tokenCreationDate = clone $confirmationTokenCreationDate;
            $tokenExpiredDate = $tokenCreationDate->modify('+'.$this->config['tokens']['email_confirmation_token_expiration_time'].' seconds');
            $is_email_token_expired = $tokenExpiredDate < new \DateTime();
        }

        $form = new OperationConfirmForm();

        // Для Ajax отдаем csrf токен
        // Success response if requested by Ajax
        if($isAjax){
            // Return Json with SUCCESS status (in this case also with $data array, which contains csrf token)
            return $this->message()->success(self::SUCCESS_CSRF_TOKEN, [
                'csrf' => $form->get('csrf')->getValue(),
                'token_created_date' => $tokenCreatedDate ? $tokenCreatedDate->getTimestamp() : null,
                'email_confirmation_token_request_period' => $this->config['tokens']['email_confirmation_token_request_period'],
                'is_email_token_expired' => $is_email_token_expired
            ]);
        }

        // HTML-
        $form->prepare();
        $view = new TwigViewModel([
            'confirmForm' => $form,
            'token_created_date' => $tokenCreatedDate ? $tokenCreatedDate->getTimestamp() : null,
            'email_confirmation_token_request_period' => $this->config['tokens']['email_confirmation_token_request_period'],
            'is_email_token_expired' => $is_email_token_expired
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        if ($this->baseAuthManager->getIdentity()) {
            $view->setTemplate('module-auth-v2/email/confirm');
        } else {
            $view->setTemplate('module-auth-v2/email/auth-confirm');
        }

        return $view;
    }

    /**
     * Resending email (for registration)
     * @throws \Exception
     */
    public function resendAction()
    {
        // Если только что зарегистрировался
        $user_login = $this->sessionContainerManager->getSessionVar('unconfirmed_user');

        if (!$user_login) {
            // Если авторизован
            $user_login = $this->baseAuthManager->getIdentity();
        }

        if ($user_login) {
            try {
                /** @var UserInterface $user */
                $user = $this->baseUserManager->getUserByLogin($user_login);
                // Provide encoded token (Create token, create mail and send mail)
                $this->encodedTokenProvider->provideToken($user);
                // SUCCESS Response
                // If Ajax-
                if($this->getRequest()->isXmlHttpRequest()){
                    // Return Json with SUCCESS status
                    return $this->message()->success(self::SUCCESS_EMAIL_RESENT);
                }
                // Html -
                return $this->redirect()->toRoute('email/confirm');

            } catch (\Throwable $t) {
                // ERROR Response
                return $this->message()->exception($t);
            }
        }
        // If no Email provided
        // ERROR Response
        return $this->message()->error(self::ERROR_NO_VALID_EMAIL_PROVIDED);
    }
}
