<?php
namespace ModuleAuthV2\Controller\Factory;

use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BasePhoneManager;
use Core\Service\Base\BaseUserManager;
use Interop\Container\ContainerInterface;
use ModuleAuthV2\Controller\UserRegistrationCheckedController;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class UserRegistrationCheckedControllerFactory
 * @package ModuleAuth\Controller\Factory
 */
class UserRegistrationCheckedControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return UserRegistrationCheckedController
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $baseUserManager = $container->get(BaseUserManager::class);
        $basePhoneManager = $container->get(BasePhoneManager::class);
        $baseEmailManager = $container->get(BaseEmailManager::class);

        return new UserRegistrationCheckedController(
            $baseUserManager,
            $basePhoneManager,
            $baseEmailManager
        );
    }
}
