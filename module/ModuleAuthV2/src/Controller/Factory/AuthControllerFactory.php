<?php
namespace ModuleAuthV2\Controller\Factory;

use Core\EventManager\AuthEventProvider;
use Core\Service\Base\BaseUserManager;
use Core\Service\SessionContainerManager;
use Interop\Container\ContainerInterface;
use ModuleAuthV2\Controller\AuthController;
use Zend\ServiceManager\Factory\FactoryInterface;
use Core\Service\Base\BaseAuthManager;

/**
 * This is the factory for AuthController
 */
class AuthControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $baseUserManager = $container->get(BaseUserManager::class);
        $sessionContainerManager = $container->get(SessionContainerManager::class);
        $authEventProvider = $container->get(AuthEventProvider::class);
        $config = $container->get('config');

        return new AuthController(
            $baseAuthManager,
            $baseUserManager,
            $sessionContainerManager,
            $authEventProvider,
            $config
        );
    }
}