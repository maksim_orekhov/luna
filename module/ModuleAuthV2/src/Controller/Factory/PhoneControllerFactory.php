<?php
namespace ModuleAuthV2\Controller\Factory;

use Core\EventManager\AuthEventProvider;
use Core\Service\Base\BasePhoneManager;
use Core\Service\Base\BaseUserManager;
use Core\Service\OperationConfirmManager;
use Core\Service\SessionContainerManager;
use Core\Service\Base\BaseAuthManager;
use Interop\Container\ContainerInterface;
use ModuleAuthV2\Controller\PhoneController;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class PhoneControllerFactory
 * @package ModuleAuthV2\Controller\Factory
 */
class PhoneControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $baseUserManager = $container->get(BaseUserManager::class);
        $basePhoneManager = $container->get(BasePhoneManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $sessionContainerManager = $container->get(SessionContainerManager::class);
        $operationConfirmManager = $container->get(OperationConfirmManager::class);
        $authEventProvider = $container->get(AuthEventProvider::class);

        return new PhoneController(
            $baseUserManager,
            $basePhoneManager,
            $baseAuthManager,
            $sessionContainerManager,
            $operationConfirmManager,
            $authEventProvider->getEventManager()
        );
    }
}