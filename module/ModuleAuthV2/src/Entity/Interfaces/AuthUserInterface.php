<?php
namespace ModuleAuthV2\Entity\Interfaces;

use Application\Entity\Email;
use Application\Entity\Phone;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Interface AuthUserInterface
 * @package Core\Entity
 */
interface AuthUserInterface
{
    /**
     * Get id
     *
     * @return integer
     */
    public function getId();
    /**
     * Set login
     *
     * @param string $login
     */
    public function setLogin($login);
    /**
     * Get login
     *
     * @return string
     */
    public function getLogin();
    /**
     * Set password
     *
     * @param string $password
     */
    public function setPassword($password);
    /**
     * Get password
     *
     * @return string
     */
    public function getPassword();
    /**
     * Set created
     *
     * @param \DateTime $created
     */
    public function setCreated($created);
    /**
     * Get created
     *
     * @return string
     */
    public function getCreated();
    /**
     * @return string
     */
    public function getPasswordResetToken();
    /**
     * @param string $token
     */
    public function setPasswordResetToken($token);
    /**
     * @return string
     */
    public function getPasswordResetTokenCreationDate();
    /**
     * @param string $date
     */
    public function setPasswordResetTokenCreationDate($date);
    /**
     * Set email
     *
     * @param Email $email
     */
    public function setEmail(Email $email = null);
    /**
     * Get email
     *
     * @return Email
     */
    public function getEmail();
    /**
     * Set phone
     *
     * @param Phone $phone
     */
    public function setPhone(Phone $phone = null);
    /**
     * Get phone
     *
     * @return Phone
     */
    public function getPhone();
    /**
     * @return ArrayCollection
     */
    public function getRoles();
    /**
     * @param $role
     */
    public function addRole($role);
}