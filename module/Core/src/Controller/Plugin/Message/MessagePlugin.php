<?php
namespace Core\Controller\Plugin\Message;

use Application\Form\ErrorReportForm;
use Core\Controller\AbstractRestfulController;
use Core\Exception\CriticalException;
use Core\Exception\LogicException;
use Core\Provider\Mail\MessagePluginSender;
use Core\Service\TwigViewModel;
use Exception;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Class MessagePlugin
 * @package Core\Controller\Plugin\Message
 */
class MessagePlugin extends AbstractPlugin implements BaseMessageCodeInterface
{
    use MessageJsonModelTrait;

    const TWIG_MODE = true;

    const TEMPLATE_ERROR = 'template_error';
    const TEMPLATE_SUCCESS = 'template_success';
    const TEMPLATE_UNDER_CONSTRUCTION = 'template_under_construction';
    /**
     * @var array
     */
    public static $view_model_templates = [
        self::TEMPLATE_ERROR => 'message/error',
        self::TEMPLATE_SUCCESS => 'message/success',
        self::TEMPLATE_UNDER_CONSTRUCTION => 'message/under-construction'
    ];
    /**
     * @var array
     */
    public static $twig_model_templates = [
        self::TEMPLATE_ERROR => 'twig/message/error',
        self::TEMPLATE_SUCCESS => 'twig/message/success',
        self::TEMPLATE_UNDER_CONSTRUCTION => 'twig/message/under-construction'
    ];
    /**
     * @var bool
     */
    private $isAjax;
    /**
     * @var bool
     */
    private $jsonStrategy;
    /**
     * @var MessagePluginSender
     */
    private $messagePluginSender;
    /**
     * @var array
     */
    private $config;

    public function __construct(\Zend\Http\Request $request,
                                MessagePluginSender $messagePluginSender,
                                array $config)
    {
        error_reporting(E_ALL & ~E_WARNING);
        $this->isAjax = $request->isXmlHttpRequest();
        $this->messagePluginSender = $messagePluginSender;
        $this->config = $config;
    }

    /**
     * @param bool $isJsonStrategy
     * @return bool
     */
    public function setJsonStrategy(bool $isJsonStrategy): bool
    {
        $this->jsonStrategy = $isJsonStrategy;

        return $this->jsonStrategy;
    }

    /**
     * @return bool
     */
    public function isJsonStrategy(): bool
    {
        if ($this->jsonStrategy) {
            return $this->jsonStrategy;
        }

        return $this->setJsonStrategy($this->isAjax);
    }

    /**
     * @param null $message
     * @param array $data
     * @param null $code
     * @return TwigViewModel|JsonModel|ViewModel
     * @throws \RuntimeException
     */
    public function error($message = null, $data = null, $code = null)
    {
        if ($this->isAjax || $this->isJsonStrategy()){

            return $this->JsonErrorModel($this->getMessageText($message, $code), $this->getMessageCode($code), $data);
        }

        return $this->getView($this->getMessageText($message, $code), self::TEMPLATE_ERROR);
    }

    /**
     * @param null $message
     * @param array $data
     * @param string $code
     * @return TwigViewModel|JsonModel|ViewModel
     * @throws \RuntimeException
     */
    public function success($message = null, $data = null, $code = null)
    {
        if ($this->isAjax || $this->isJsonStrategy()){

            return $this->JsonSuccessModel($this->getMessageText($message, $code), $this->getMessageCode($code), $data);
        }

        return $this->getView($this->getMessageText($message, $code), self::TEMPLATE_SUCCESS);
    }

    /**
     * @param null $message
     * @param $data
     * @param string $code
     * @return TwigViewModel|JsonModel|ViewModel
     * @throws \RuntimeException
     */
    public function underConstruction($message = null, $data = null, $code = null)
    {
        if ($this->isAjax || $this->isJsonStrategy()){

            return $this->JsonSuccessModel($this->getMessageText($message, $code), $this->getMessageCode($code), $data);
        }

        return $this->getView($this->getMessageText($message, $code), self::TEMPLATE_UNDER_CONSTRUCTION);
    }

    /**
     * @param null $data
     * @return TwigViewModel|JsonModel|ViewModel
     * @throws \RuntimeException
     */
    public function invalidFormData($data = null)
    {
        return $this->error(null, $data, self::ERROR_INVALID_FORM_DATA);
    }

    /**
     * @param Exception|LogicException|CriticalException $exception
     * @return bool|TwigViewModel|JsonModel|ViewModel
     * @throws Exception
     */
    public function exception($exception)
    {
        switch (\get_class($exception)){
            case LogicException::class:
                if ($this->isAjax || $this->isJsonStrategy()) {
                    return $this->JsonErrorModel(
                        $exception->getExceptionMessage(), // message
                        $exception->getExceptionCode(), // code
                        APP_ENV_DEV === true
                            ? [
                                'message' => $exception->getMessage(),
                                'file'    => $exception->getFile(),
                                'line'    => $exception->getLine(),
                                'trace'   => $exception->getTrace()
                            ]
                            :null
                    );
                }

                return $this->getView($exception->getMessage(), self::TEMPLATE_ERROR);

            case CriticalException::class:
                //отправка уведомления об ошибке
                try {
                    $this->sendCriticalExceptionAlert($exception);
                }
                catch (\Throwable $t) {

                    logException($t, 'email-error');
                }
                // запись в лог
                logException($exception,'critical-errors',\Zend\Log\Logger::CRIT);

                if ($this->isAjax || $this->isJsonStrategy()) {
                    return $this->JsonErrorModel(
                        $exception->getExceptionMessage(), // message
                        $exception->getExceptionCode(), // code
                        APP_ENV_DEV === true
                            ? [
                                'message' => $exception->getMessage(),
                                'file'    => $exception->getFile(),
                                'line'    => $exception->getLine(),
                                'trace'   => $exception->getTrace()
                            ]
                            :null
                    );
                }

                return $this->getView($exception->getMessage(), self::TEMPLATE_ERROR);

                break;

            default:
                throw $exception;
        }
    }

    /**
     * @return JsonModel
     */
    public function accessDenied()
    {
        $code = BaseMessageCodeInterface::ERROR_ACCESS_DENIED;
        if ($this->isAjax || $this->isJsonStrategy()){

            return $this->JsonSuccessModel($this->getMessageText(null, $code), $this->getMessageCode($code), null);
        }

        return $this->getController()->redirect()->toRoute('access-denied');
    }

    /**
     * @param null $message
     * @param null $email
     * @param null $subject
     * @throws Exception
     */
    public function sendSpecialMail($message, $email = null, $subject = null)
    {
        if ( !$email && array_key_exists('admin_email',$this->config)) {
            $email = $this->config['admin_email'];
        }

        if (!$email || !$message) {

            throw new \Exception('sendSpecialMail error: Invalid data provided');
        }

        $this->messagePluginSender->sendMail($email, [
            'message' => $message,
            'subject' => $subject,
            MessagePluginSender::TYPE_NOTIFY_KEY => MessagePluginSender::SPECIAL_MAIL,
        ]);
    }

    /**
     * @param Exception|LogicException|CriticalException $exception
     * @param null $email
     * @throws Exception
     */
    public function sendCriticalExceptionAlert($exception, $email = null)
    {
        if ( !$email && array_key_exists('admin_email',$this->config) ) {
            $email = $this->config['admin_email'];
        }

        if (!$email || !$exception) {

            throw new \Exception('sendExceptionAlert error: Invalid data provided');
        }

        $this->messagePluginSender->sendMail($email, [
            'message' => 'CriticalException: "'.$exception->getMessage().'"',
            'line' => $exception->getLine(),
            'file' => $exception->getFile(),
            'trace' => $exception->getTraceAsString(),
            'xdebug_message' => $exception->xdebug_message ?? null,
            MessagePluginSender::TYPE_NOTIFY_KEY => MessagePluginSender::EXCEPTION_ALERT,
        ]);
    }

    /**
     * @param null $message
     * @param $template_name
     * @return TwigViewModel|ViewModel
     */
    private function getView($message = null, $template_name)
    {
        if (self::TWIG_MODE){
            $template = self::$twig_model_templates[$template_name];
            $errorReportForm = new ErrorReportForm();
            $errorReportForm->get('error_message')->setValue($message);
            $errorReportForm->prepare();
            $view = new TwigViewModel([
                'message' => $message,
                'errorReportForm' => $errorReportForm
            ]);
            /** @var AbstractRestfulController $controller */
            $controller = $this->controller;
            $controller->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        } else {
            $template = self::$view_model_templates[$template_name];
            $view = new ViewModel([
                'message' => $message
            ]);
        }

        $view->setTemplate($template);

        return $view;
    }

    /**
     * @param string $messageCode
     * @return mixed|null
     * @throws \RuntimeException
     */
    private function getMessageCode($messageCode)
    {
        if (empty($messageCode) || $messageCode === null) {
            //return code
            return null;
        }

        try {
            $code = @\constant('self::'.$messageCode);
        }
        catch (\Throwable $t) {

            $code = null;
        }

        if ($code) {
            //return code
            return $code;
        }

        throw new \RuntimeException("constant \"$messageCode\" not found");
    }

    /**
     * @param $messageText
     * @param $messageCode
     * @return mixed|string
     */
    private function getMessageText($messageText, $messageCode)
    {
        if (!empty($messageText) || $messageText !== null) {

            try {
                $is_message_constant = @\constant('self::'.$messageText);
            }
            catch (\Throwable $t) {

                $is_message_constant = false;
            }

            if ($is_message_constant) {

                throw new \RuntimeException('Message "'.$messageText.'" matching with constant for code');
            }

            //return message
            return $messageText;
        }

        if (empty($messageCode) || $messageCode === null) {
            //return message
            return null;
        }

        try {
            $message = @\constant('self::'.$messageCode.'_MESSAGE');
        }
        catch (\Throwable $t) {

            $message = null;
        }

        if ($message) {
            //return message
            return $message;
        }

        throw new \RuntimeException("Message to code \"$messageCode\" not found");
    }
}

