<?php
namespace Core\Controller\Plugin;

use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Session\ManagerInterface as Manager;

class SessionPlugin extends AbstractPlugin
{
    const NAMESPACE_DEFAULT = 'default';
    /**
     * Instance namespace, default is 'default'
     *
     * @var string
     */
    protected $namespace = self::NAMESPACE_DEFAULT;
    /**
     * @var Container
     */
    protected $container;
    /**
     * Messages from previous request
     * @var array
     */
    protected $messages = [];
    /**
     * @var Manager
     */
    protected $session;

    /**
     * @var bool
     */
    protected $messageAdded = false;

    /**
     * Set the session manager
     *
     * @param  Manager $manager
     * @return SessionPlugin
     */
    public function setSessionManager(Manager $manager): SessionPlugin
    {
        $this->session = $manager;
        return $this;
    }

    /**
     * Retrieve the session manager
     *
     * If none composed, lazy-loads a SessionManager instance
     *
     * @return Manager
     */
    public function getSessionManager(): Manager
    {
        if (! $this->session instanceof Manager) {
            $this->setSessionManager(Container::getDefaultManager());
        }

        return $this->session;
    }

    /**
     * Get session container for flash messages
     *
     * @return Container
     */
    public function getContainer(): Container
    {
        if ($this->container instanceof Container) {
            return $this->container;
        }

        return $this->setContainer();
    }

    /**
     * @param string $container_name
     * @return Container
     */
    public function setContainer($container_name = 'Luna_Container'): Container
    {
        $manager = $this->getSessionManager();
        $this->container = new Container($container_name, $manager);

        return $this->container;
    }

    /**
     * @param string $namespace
     * @return $this
     */
    public function setNamespace($namespace = 'default'): self
    {
        $this->namespace = $namespace;
        return $this;
    }

    /**
     * @return string
     */
    public function getNamespace(): string
    {
        return $this->namespace;
    }

    /**
     * Add a message
     *
     * @param  string|array         $message
     * @param  null|string    $namespace
     * @return SessionPlugin Provides a fluent interface
     */
    public function addSessionVar($message, $namespace = null): SessionPlugin
    {
        $container = $this->getContainer();

        if (null === $namespace) {
            $namespace = $this->getNamespace();
        }

        if (! $this->messageAdded) {
            $this->getMessagesFromContainer();
        }

        $container->{$namespace} = $message;

        $this->messageAdded = true;

        return $this;
    }

    /**
     * Get messages from a specific namespace
     *
     * @param  string         $namespace
     * @return array
     */
    public function getSessionVar($namespace = null): array
    {
        if (null === $namespace) {
            $namespace = $this->getNamespace();
        }

        if ($this->hasSessionVar($namespace)) {

            return $this->messages[$namespace];
        }

        return [];
    }

    /**
     * @param null $namespace
     * @return bool
     */
    public function hasSessionVar($namespace = null): bool
    {
        if (null === $namespace) {
            $namespace = $this->getNamespace();
        }

        $this->getMessagesFromContainer();

        return isset($this->messages[$namespace]);
    }

    /**
     * @param null $namespace
     * @return bool
     */
    public function clearSessionVar($namespace = null): bool
    {
        if (null === $namespace) {
            $namespace = $this->getNamespace();
        }

        if ($this->hasSessionVar($namespace)) {
            $container = $this->getContainer();
            unset($container->{$namespace});
            return true;
        }

        return false;
    }

    protected function getMessagesFromContainer()
    {
        if (! empty($this->messages) || $this->messageAdded) {
            return;
        }

        $container = $this->getContainer();

        foreach ($container as $namespace => $messages) {
            $this->messages[$namespace] = $messages;
        }
    }
}

