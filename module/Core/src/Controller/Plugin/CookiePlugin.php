<?php
namespace Core\Controller\Plugin;

use GuzzleHttp\Cookie\CookieJar;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Http\Header\SetCookie;

/**
 * Class CookiePlugin
 * @package Core\Controller\Plugin
 */
class CookiePlugin extends AbstractPlugin
{
    /**
     * @var \Zend\Http\Response
     */
    private $response;

    private $request;

    /**
     * CookiePlugin constructor.
     * @param \Zend\Http\Response $response
     */
    public function __construct(\Zend\Http\Response $response,
                                \Zend\Http\Request $request)
    {
        $this->response = $response;
        $this->request = $request;
    }

    /**
     * @return $this
     */
    public function __invoke()
    {
        return $this;
    }

    /**
     * @param $name
     * @param $value
     * @param int $expire_min
     * @param null $path
     * @return SetCookie
     */
    public function set($name, $value, int $expire_min = 0, $path = null): SetCookie
    {
        if ( \is_object($value) ) {
            throw new \RuntimeException('The object cannot be set as a cookie');
        }

        if (\is_array($value)) {

            $value = str_replace(' ','%20', json_encode($value));
        }

        if ( empty($value) ||  $value === null) {
            //clear cookie if empty
            $expire_time = 1;
        } else {
            $expire_time = time() + (60 * $expire_min);
        }

        $cookie = new SetCookie(
            $name,
            $value,
            $expire_time,
            $path
        );

        $this->response->getHeaders()->addHeader($cookie);

        return $cookie;
    }

    /**
     * @param null $name
     * @return bool|mixed|null|\Zend\Http\Header\Cookie
     */
    public function get($name = null)
    {
        $cookie = $this->request->getCookie();
        if ($name) {
            if (!empty($cookie) && array_key_exists($name, $cookie)) {
                return $cookie[$name];
            }
        } else {
            return $cookie;
        }
        return null;
    }
}

