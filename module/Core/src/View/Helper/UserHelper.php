<?php
namespace Core\View\Helper;

use Core\Entity\Interfaces\UserInterface;
use Core\Service\Base\BaseUserManager;
use Core\Service\Base\BaseAuthManager;
use ModuleAuthV2\Controller\AuthController;
use ModuleAuthV2\Form\LoginForm;
use ModuleAuthV2\Form\UserRegistrationForm;
use Zend\Http\Request;
use Zend\Form\Element\Captcha;

class UserHelper
{
    /**
     * @var BaseUserManager
     */
    private $baseUserManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var array
     */
    private $config;

    /**
     * @var UserInterface
     */
    private $user = null;

    /**
     * @var Request
     */
    private $request;

    /**
     * UserHelper constructor.
     * @param BaseUserManager $baseUserManager
     * @param BaseAuthManager $baseAuthManager
     * @param Request $request
     * @param $config
     * @throws \Exception
     */
    public function __construct(BaseUserManager $baseUserManager,
                                BaseAuthManager $baseAuthManager,
                                Request $request,
                                $config)
    {
        $this->baseUserManager = $baseUserManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->request = $request;
        $this->config = $config;

        if ($this->user === null || ($this->user !== null
                && strtolower($this->user->getLogin()) !== strtolower($baseAuthManager->getIdentity()))){
            $this->setCurrentUser($baseAuthManager->getIdentity());
        }
    }

    public function __invoke()
    {
        return $this;
    }

    /**
     * @param $login
     * @return bool
     * @throws \Exception
     */
    public function setCurrentUser($login){
        try{
            $user = $this->baseUserManager->getUserByLogin($login);
            $this->user = $user;
        }catch(\Throwable $t){
            $this->user = null;
        }
        return true;
    }

    /**
     * @return UserInterface|null
     */
    public function getCurrentUser(){
        return $this->user;
    }

    /**
     * @return string|null
     */
    public function getLogin(){
        return $this->getCurrentUser() ? $this->getCurrentUser()->getLogin() : null;
    }

    /**
     * @return string|null
     */
    public function getName(){
        return $this->getCurrentUser() ? $this->getCurrentUser()->getName() : null;
    }

    /**
     * @return string|null
     */
    public function getCreated(){
        $created = null;
        if ($this->getCurrentUser()) {
            /** @var \DateTime $created */
            $created = $this->getCurrentUser()->getCreated();
        }

        return $created ? $created->format('Y-m-d H:i:s') : null;
    }

    /**
     * //TODO надобы дописать
     * @return null
     */
    public function getAvatar(){
        return null;
    }

    /**
     * Get user Phone indicator
     *
     * @return null|string
     */
    public function unconfirmedPhoneIndicator()
    {
        if($this->isUnconfirmedPhone()) {

            $code_expiration_time_in_hours = $this->config['tokens']['confirmation_code_expiration_time']/3600;

            $phoneIndicator = '<div class="alert alert-warning">
                                <span class="glyphicon glyphicon-phone"></span>
                                На ваш номер телефона (' . $this->user->getPhone()->getPhoneNumber(). ') отправлен код подтверждения.
                                Код действителен в течение ' . $code_expiration_time_in_hours . ' часов.
                                <a href="/phone/confirm">Подтвердить</a>.
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                </div>';

            return $phoneIndicator;
        }

        return null;
    }

    /**
     * @return bool
     */
    public function isUnconfirmedPhone()
    {
        if ($this->user && !$this->user->getPhone()) {

            return true;
        }
        if ($this->user && $this->user->getPhone() && !$this->user->getPhone()->getIsVerified()) {

            return true;
        }

        return false;
    }

    /**
     * Get user Email indicator
     *
     * @return null|string
     */
    public function unconfirmedEmailIndicator()
    {
        if($this->isUnconfirmedEmail()) {

            $token_expiration_time_in_hours = $this->config['tokens']['email_confirmation_token_expiration_time']/3600;

            $emailIndicator = '<div class="alert alert-warning">
                                <span class="glyphicon glyphicon-envelope"></span>
                                На ваш email (' . $this->user->getEmail()->getEmail() . ') отправлена ссылка для подтверждения.
                                Ссылка действительна в течение ' . $token_expiration_time_in_hours . ' часов. 
                                <a href="/email/confirm">Подтвердить</a>.
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                </div>';

            return $emailIndicator;
        }

        return null;
    }

    /**
     * @return bool
     */
    public function isUnconfirmedEmail()
    {
        return $this->user && !$this->user->getEmail()->getIsVerified();
    }

    /**
     * @return bool
     */
    public function isPhoneExists()
    {
        return $this->user && $this->user->getPhone();
    }

    /**
     * @return LoginForm
     */
    public function getLoginForm(): LoginForm
    {
        $count_failed_login_attempts = $this->getCookie('count_failed_login_attempts') ?? 0;
        $captcha_enable = $this->getCookie('login_captcha_enable') ?? false;
        if (!$captcha_enable) {
            $captcha_enable = (int) $count_failed_login_attempts === 2;
        }
        return new LoginForm($this->baseAuthManager, $this->config, $captcha_enable);
    }

    /**
     * @return UserRegistrationForm
     */
    public function getRegistrationForm(): UserRegistrationForm
    {
        return new UserRegistrationForm(null, $this->config);
    }

    /**
     * @param null $name
     * @return bool|mixed|null|\Zend\Http\Header\Cookie
     */
    private function getCookie($name = null)
    {
        $cookie = $this->request->getCookie();
        if ($name) {
            if (!empty($cookie) && array_key_exists($name, $cookie)) {
                return $cookie[$name];
            }
        } else {
            return $cookie;
        }
        return null;
    }
}