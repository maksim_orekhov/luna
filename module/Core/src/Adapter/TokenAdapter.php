<?php
namespace Core\Adapter;

use Core\Exception\LogicException;
use \Firebase\JWT\JWT;

/**
 * Class TokenAdapter
 * @package Core\Adapter
 */
class TokenAdapter
{
    /**
     * @var array
     */
    private $config;

    /**
     * TokenAdapter constructor.
     * @param $config
     */
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * Decode data from token
     * @param $encoded_token
     * @return object
     * @throws \Exception
     *
     * Возвращает:
     * object(stdClass)
     *  public 'email' => string 'r.akhetv@sim-t.org'
     *  public 'simple_token' => string 's6JaYjaC'
     */
    public function decodeToken($encoded_token)
    {
        try {
            // Decode encoded token
            $decoded_token = JWT::decode($encoded_token, $this->config['tokens']['token_encryption_key'], array('HS256'));

        } catch (\Exception $e){

            throw new LogicException(null, LogicException::TOKEN_NOT_VALID);
        }

        return $decoded_token;
    }


    /**
     * Create token
     *
     * @param string $email_value
     * @param string $simple_token
     * @return string
     */
    public function createToken($email_value, $simple_token)
    {
        // Get encoded Token
        $encoded_token = $this->encodeTokenForConfirmationEmail($email_value, $simple_token);

        return $encoded_token;
    }

    /**
     * Encode token
     * Используем для подтверждения email
     *
     * @param string $email_value
     * @param string $simple_token
     * @return string
     */
    private function encodeTokenForConfirmationEmail($email_value, $simple_token)
    {
        $token = array(
            'email'         => $email_value,
            'simple_token'  => $simple_token
        );

        return JWT::encode($token, $this->config['tokens']['token_encryption_key']);
    }

    /**
     * @param $email_value
     * @param $deal_id
     * @param $deal_agent_id
     * @return string
     */
    public function createTokenForDealAgent($email_value, $deal_id, $deal_agent_id)
    {
        $token = array(
            'email' => $email_value,
            'dealId' => $deal_id,
            'dealAgentId' => $deal_agent_id
        );
        return JWT::encode($token, $this->config['tokens']['token_encryption_key']);
    }

    /**
     * @param $data
     * @return string
     */
    public function createTokenForMandarinOrderId($data) :string
    {
        return JWT::encode($data, $this->config['tokens']['token_encryption_key']);
    }

    /**
     * @param $encoded_token
     * @return object
     * @throws \Exception
     *
     * object(stdClass)
     *  public 'deal_id'
     *  public 'timestamp'
     *  public 'payment_purpose'
     *
     * @TODO Повторяет decodeToken
     */
    public function decodeTokenForMandarinOrderId($encoded_token)
    {
        try {
            // Decode encoded token
            $decoded_token = JWT::decode($encoded_token, $this->config['tokens']['token_encryption_key'], array('HS256'));

        } catch(\Exception $e){

            throw new \Exception('Invalid encoded token');
        }

        return $decoded_token;
    }

    /**
     * @param $data
     * @return string
     *
     * @TODO Временно. Требует рефакторигна!
     */
    public function createTokenForDeal($data) :string
    {
        return JWT::encode($data, $this->config['tokens']['token_encryption_key']);
    }
}