<?php
namespace Core\Entity;

use Application\Entity\Email;
use Core\Entity\Interfaces\EmailInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Email
 * @package Core\Entity
 *
 * @ORM\MappedSuperclass
 */
abstract class AbstractEmail implements EmailInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    protected $email;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_verified", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    protected $isVerified;

    /**
     * @ORM\Column(name="confirmation_token", length=32, nullable=true)
     */
    protected $confirmationToken;

    /**
     * @ORM\Column(name="confirmation_token_creation_date", type="datetime", nullable=true)
     */
    protected $confirmationTokenCreationDate;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Email
     */
    public function setEmail($email)
    {
        $this->email = strtolower($email);

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set isVerified
     *
     * @param boolean $isVerified
     *
     * @return Email
     */
    public function setIsVerified($isVerified)
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * Get isVerified
     *
     * @return boolean
     */
    public function getIsVerified()
    {
        return $this->isVerified;
    }

    /**
     * @return string
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * @param string $token
     */
    public function setConfirmationToken($token)
    {
        $this->confirmationToken = $token;
    }

    /**
     * @return string
     */
    public function getConfirmationTokenCreationDate()
    {
        return $this->confirmationTokenCreationDate;
    }

    /**
     * @param string $date
     */
    public function setConfirmationTokenCreationDate($date)
    {
        $this->confirmationTokenCreationDate = $date;
    }
}

