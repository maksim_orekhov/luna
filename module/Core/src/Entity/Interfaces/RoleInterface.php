<?php
namespace Core\Entity\Interfaces;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Interface BaseRoleInterface
 * @package Core\Entity
 */
interface RoleInterface
{
    public function __construct();
    /**
     * Returns role ID.
     * @return integer
     */
    public function getId();
    /**
     * @return string
     */
    public function getName();
    /**
     * @param string $name
     */
    public function setName($name);
    /**
     * @return string mixed
     */
    public function getDescription();
    /**
     * @param string $description
     */
    public function setDescription($description);
    /**
     * @return ArrayCollection
     */
    public function getParentRoles();
    /**
     * Assigns a parent Role to role
     * @param $parentRole
     */
    public function addParentRole($parentRole);
    /**
     * @return ArrayCollection
     */
    public function getChildRoles();
    /**
     * Assigns a child Role to role
     * @param $childRole
     */
    public function addChildRole($childRole);
    /**
     * @return ArrayCollection
     */
    public function getPermissions();
    /**
     * Assigns a permission to role
     * @param $permission
     */
    public function addPermission($permission);
}