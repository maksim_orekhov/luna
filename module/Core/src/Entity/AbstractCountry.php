<?php
namespace Core\Entity;

use Core\Entity\Interfaces\CountryInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AbstractCountry
 * @package Core\Entity
 *
 * @ORM\MappedSuperclass
 */
abstract class AbstractCountry implements CountryInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=70, nullable=false, unique=false)
     */
    protected $name;
    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=2, nullable=false, unique=false)
     */
    protected $code;
    /**
     * @var string
     *
     * @ORM\Column(name="code3", type="string", length=3, nullable=false, unique=false)
     */
    protected $code3;
    /**
     * @var integer
     *
     * @ORM\Column(name="phone_code", type="integer", length=7, nullable=false, unique=false)
     */
    protected $phoneCode;
    /**
     * @var boolean
     *
     * @ORM\Column(name="postcode_required", type="boolean", nullable=false, options={"default" : 0})
     */
    protected $postcodeRequired;
    /**
     * @var boolean
     *
     * @ORM\Column(name="is_eu", type="boolean", nullable=false, unique=false,  options={"default" : 0})
     */
    protected $isEu;
    /**
     * @var integer
     *
     * @ORM\Column(name="weight", type="integer", length=11, nullable=false, unique=false)
     */
    protected $weight;
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @param int $id
     * For fixtures creation
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }
    /**
     * @return string
     */
    public function getCode3()
    {
        return $this->code3;
    }
    /**
     * @param string $code3
     */
    public function setCode3($code3)
    {
        $this->code3 = $code3;
    }
    /**
     * @return int
     */
    public function getPhoneCode()
    {
        return $this->phoneCode;
    }
    /**
     * @param int $phoneCode
     */
    public function setPhoneCode($phoneCode)
    {
        $this->phoneCode = $phoneCode;
    }
    /**
     * @return bool
     */
    public function isPostcodeRequired()
    {
        return $this->postcodeRequired;
    }
    /**
     * @param bool $postcodeRequired
     */
    public function setPostcodeRequired($postcodeRequired)
    {
        $this->postcodeRequired = $postcodeRequired;
    }
    /**
     * @return bool
     */
    public function isEu()
    {
        return $this->isEu;
    }
    /**
     * @param bool $isEu
     */
    public function setIsEu($isEu)
    {
        $this->isEu = $isEu;
    }
    /**
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }
    /**
     * @param int $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }
}

