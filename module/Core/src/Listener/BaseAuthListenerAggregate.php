<?php
namespace Core\Listener;

use Core\Entity\Interfaces\UserInterface;
use Core\EventManager\AuthEventProvider as AuthEvent;
use Core\Exception\LogicException;
use Core\Service\Base\BaseUserManager;
use Core\Service\CodeOperationInterface;
use Core\Service\SessionContainerManager;
use Core\Provider\Token\EmailConfirmationTokenProvider;
use ModuleAuthV2\Service\RegistrationManager;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;

class BaseAuthListenerAggregate implements ListenerAggregateInterface, CodeOperationInterface
{
    use ListenerAggregateTrait;
    /**
     * @var SessionContainerManager
     */
    private $sessionContainerManager;
    /**
     * @var EmailConfirmationTokenProvider
     */
    private $encodedTokenProvider;
    /**
     * @var BaseUserManager
     */
    private $baseUserManager;
    /**
     * @var RegistrationManager
     */
    private $registrationManager;
    /**
     * @var EventManager
     */
    private $notifyEventManager;
    /**
     * @var EventManager
     */
    private $authEventManager;

    /**
     * RegistrationListenerAggregate constructor.
     * @param SessionContainerManager $sessionContainerManager
     * @param EmailConfirmationTokenProvider $encodedTokenProvider
     * @param BaseUserManager $baseUserManager
     * @param RegistrationManager $registrationManager
     * @param EventManager $notifyEventManager
     * @param EventManager $authEventManager
     */
    public function __construct(SessionContainerManager $sessionContainerManager,
                                EmailConfirmationTokenProvider $encodedTokenProvider,
                                BaseUserManager $baseUserManager,
                                RegistrationManager $registrationManager,
                                EventManager $notifyEventManager,
                                EventManager $authEventManager)
    {
        $this->sessionContainerManager = $sessionContainerManager;
        $this->encodedTokenProvider = $encodedTokenProvider;
        $this->baseUserManager = $baseUserManager;
        $this->registrationManager = $registrationManager;
        $this->notifyEventManager = $notifyEventManager;
        $this->authEventManager = $authEventManager;
    }

    /**
     * @inheritdoc
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach(AuthEvent::EVENT_BEFORE_REGISTRATION, [$this, 'beforeUserRegistration'], $priority);
        $this->listeners[] = $events->attach(AuthEvent::EVENT_SUCCESS_REGISTRATION, [$this, 'successUserRegistration'], $priority);
        $this->listeners[] = $events->attach(AuthEvent::EVENT_USER_AUTO_REGISTRATION, [$this, 'userAutoRegistration'], $priority);

        $this->listeners[] = $events->attach(AuthEvent::EVENT_VERIFY_USER_CONFIRMS, [$this, 'verifyUserConfirms'], $priority);
        $this->listeners[] = $events->attach(AuthEvent::EVENT_VERIFY_USER, [$this, 'verifyUser'], $priority);
        $this->listeners[] = $events->attach(AuthEvent::EVENT_VERIFY_PHONE, [$this, 'verifyPhone'], $priority);
        $this->listeners[] = $events->attach(AuthEvent::EVENT_VERIFY_EMAIL, [$this, 'verifyEmail'], $priority);
    }

    /**
     * @param EventInterface $event
     * @return void
     */
    public function beforeUserRegistration(EventInterface $event)
    {
        // Set to session need to send confirmation email
        $this->sessionContainerManager->setSessionVar('send_confirmation_email', true);
    }

    /**
     * @param EventInterface $event
     * @throws \Exception
     */
    public function successUserRegistration(EventInterface $event)
    {
        /** @var UserInterface $user */
        $user = $event->getParam('user');

        $this->baseUserManager->verifyUserConfirms($user, $this->authEventManager);
        if ( $this->sessionContainerManager->getSessionVar('send_confirmation_email') ) {
            // Create token, and send mail
            $this->encodedTokenProvider->provideToken($user);
            //removed after registration unnecessary cookies
            $this->sessionContainerManager->deleteSessionVar('send_confirmation_email');
        }
    }

    /**
     * @param EventInterface $event
     * @return void
     * @throws \Exception
     */
    public function verifyUserConfirms(EventInterface $event)
    {
        $identity = $event->getParam('identity');
        if ($identity) {
            $user = $this->baseUserManager->getUserByLogin($identity);

            if ($user !== null) {
                $this->baseUserManager->verifyUserConfirms($user, $this->authEventManager);
            }
        }
    }

    /**
     * @param EventInterface $event
     */
    public function verifyUser(EventInterface $event)
    {
        $params = $event->getParam('verify_user_params', null);
        $event->setParam('verify_user_params', null);
        if (!\is_array($params) ||
            !array_key_exists('user', $params) ||
            !array_key_exists('is_user_verified', $params)) {

            return;
        }
        $user = $params['user'];
        $is_user_verified = $params['is_user_verified'];

        if ( ! $is_user_verified ) {
            $this->sessionContainerManager->setSessionVar('unconfirmed_user', $user->getLogin());
        } else if ($this->sessionContainerManager->getSessionVar('unconfirmed_user')) {
            $this->sessionContainerManager->deleteSessionVar('unconfirmed_user');
            //set trigger verify user
            $this->authEventManager->trigger(AuthEvent::EVENT_USER_CONFIRMED, $this, [
                'user_confirmed_params' => ['user' => $user]
            ]);
        }
    }

    /**
     * @param EventInterface $event
     */
    public function verifyPhone(EventInterface $event)
    {
        $params = $event->getParam('verify_phone_params', null);
        $event->setParam('verify_phone_params', null);
        if (!\is_array($params) ||
            !array_key_exists('user', $params) ||
            !array_key_exists('is_phone_verified', $params)) {

            return;
        }
        $user = $params['user'];
        $is_phone_verified = $params['is_phone_verified'];

        if (!$is_phone_verified && $user->getPhone()) {
            $this->sessionContainerManager->setSessionVar('unconfirmed_phone', $user->getPhone()->getPhoneNumber());
        }else if ($this->sessionContainerManager->getSessionVar('unconfirmed_phone')) {
            $this->sessionContainerManager->deleteSessionVar('unconfirmed_phone');
            //set trigger verify phone
            $this->authEventManager->trigger(AuthEvent::EVENT_PHONE_CONFIRMED, $this, [
                'phone_confirmed_params' => ['user' => $user]
            ]);
        }
    }

    /**
     * @param EventInterface $event
     */
    public function verifyEmail(EventInterface $event)
    {
        $params = $event->getParam('verify_email_params', null);
        $event->setParam('verify_email_params', null);
        if (!\is_array($params) ||
            !array_key_exists('user', $params) ||
            !array_key_exists('is_email_verified', $params)) {

            return;
        }
        $user = $params['user'];
        $is_email_verified = $params['is_email_verified'];
        if (!$is_email_verified && $user->getEmail()) {
            $this->sessionContainerManager->setSessionVar('unconfirmed_email', $user->getEmail()->getEmail());
        } else if ($this->sessionContainerManager->getSessionVar('unconfirmed_email')) {
            $this->sessionContainerManager->deleteSessionVar('unconfirmed_email');
            //set trigger verify email
            $this->authEventManager->trigger(AuthEvent::EVENT_EMAIL_CONFIRMED, $this, [
                'email_confirmed_params' => ['user' => $user]
            ]);
        }
    }

    /**
     * @param EventInterface $event
     * @throws \Exception
     */
    public function userAutoRegistration(EventInterface $event)
    {
        $params = $event->getParam('user_auto_registration_params', null);
        $event->setParam('user_auto_registration_params', null);
        if (!\is_array($params) ||
            !array_key_exists('email', $params) ||
            $params['email'] === null) {

            throw new LogicException(null, LogicException::EMAIL_VALUE_NOT_PROVIDED);
        }
        $password = $this->baseUserManager->randomPassword(6);
        $data = [
            'email' => $params['email'],
            'password' => $password,
        ];
        $user = $this->registrationManager->addNewUser($data);
        $encoded_token = $this->encodedTokenProvider->encodedToken($user);

        $notification = $event->getParam('notification', null);
        if ($notification) {
            if (!\is_array($notification) ||
                !array_key_exists('event', $notification) ||
                !array_key_exists('params', $notification)) {

                throw new LogicException(
                    sprintf(LogicException::INCORRECT_DATA_PROVIDED_TO_LISTENER_MESSAGE.': "%s"',self::class),
                    LogicException::INCORRECT_DATA_PROVIDED_TO_LISTENER);
            }
            $notification['params']['user'] = $user;
            $notification['params']['password'] = $password;
            $notification['params']['encoded_token'] = $encoded_token;

            $this->notifyEventManager->trigger($notification['event'], $this, ['params' => $notification['params']]);
        }
    }
}