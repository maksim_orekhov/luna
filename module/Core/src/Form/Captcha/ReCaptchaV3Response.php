<?php
namespace Core\Form\Captcha;

use Zend\Http\Response as HTTPResponse;

/**
 * Model responses from the ReCaptcha and Mailhide APIs.
 */
class ReCaptchaV3Response
{
    const HUMAN_COEF = 0.5;

    /**
     * Status
     * @var boolean
     */
    protected $status = null;

    /**
     * Error codes
     * The error codes if the status is false. The different error codes can be found in the
     * recaptcha API docs.
     *
     * @var array
     */
    protected $errorCodes = [];

    /**
     * @var float
     */
    private $score = 0.0;

    /**
     * ReCaptchaV3Response constructor.
     * @param null $status
     * @param $errorCodes
     * @param HTTPResponse|null $httpResponse
     */
    public function __construct($status = null, $errorCodes = [], HTTPResponse $httpResponse = null)
    {
        if ($status !== null) {
            $this->setStatus($status);
        }

        if (! empty($errorCodes)) {
            $this->setErrorCodes($errorCodes);
        }

        if ($httpResponse !== null) {
            $this->setFromHttpResponse($httpResponse);
        }
    }

    /**
     * Set the status
     *
     * @param bool $status
     * @return self
     */
    public function setStatus($status):self
    {
        $this->status = (bool) $status;

        return $this;
    }

    /**
     * Get the status
     *
     * @return bool
     */
    public function getStatus():bool
    {
        return $this->status;
    }

    /**
     * @param $score
     * @return self
     */
    public function setScore($score):self
    {
        $this->score = (float) $score;

        return $this;
    }

    /**
     * @return float
     */
    public function getScore():float
    {
        return $this->score;
    }

    /**
     * @return bool
     */
    public function isValid():bool
    {
        return $this->getStatus() && $this->getScore() >= self::HUMAN_COEF;
    }

    /**
     * Set the error codes
     *
     * @param $errorCodes
     * @return self
     */
    public function setErrorCodes($errorCodes): self
    {
        if (\is_string($errorCodes)) {
            $errorCodes = [$errorCodes];
        }

        $this->errorCodes = $errorCodes;

        return $this;
    }

    /**
     * @return array
     */
    public function getErrorCodes(): array
    {
        return $this->errorCodes;
    }

    /**
     * @param HTTPResponse $response
     * @return ReCaptchaV3Response
     */
    public function setFromHttpResponse(HTTPResponse $response): self
    {
        $body = $response->getBody();

        $parts = json_decode($body, true);
        $status = false;
        $score = 0.0;
        $errorCodes = [];

        if (\is_array($parts) && array_key_exists('success', $parts) && array_key_exists('score', $parts)) {
            $status = $parts['success'];
            $score = (float) $parts['score'];
            if (array_key_exists('error-codes', $parts)) {
                $errorCodes = $parts['error-codes'];
            }
        }

        $this->setStatus($status);
        $this->setScore($score);
        $this->setErrorCodes($errorCodes);

        return $this;
    }
}
