<?php
namespace Core\Provider\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Core\Provider\QrCodeProvider;

/**
 * Class QrCodeProviderFactory
 * @package Core\Provider\Factory
 */
class QrCodeProviderFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');

        return new QrCodeProvider($config);
    }
}