<?php
namespace Core\Provider;

use Endroid\QrCode\QrCode;

/**
 * Class QrCodeProvider
 * @package Core\Provider
 */
class QrCodeProvider
{
    const DEFAULT_SIZE = 200;
    const DEFAULT_ENCODING = 'UTF-8';
    const DEFAULT_FORMAT = 'png';

    /**
     * Пока не используется, но может понадобится.
     * @var array
     */
    private $config;


    /**
     * QRcodeProvider constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config   = $config;
    }

    /**
     * @param string $text
     * @param int|null $size
     * @param string|null $encoding
     * @param string|null $format
     * @return string
     * @throws \Endroid\QrCode\Exception\InvalidWriterException
     */
    public function createDataUri(string $text, int $size=null, string $encoding=null, string $format=null)
    {
        if (!$size) {
            $size = self::DEFAULT_SIZE;
        }
        if (!$encoding) {
            $encoding = self::DEFAULT_ENCODING;
        }
        if (!$format) {
            $format = self::DEFAULT_FORMAT;
        }

        $qrCode = new QrCode($text);
        $qrCode->setSize($size);
        $qrCode->setEncoding($encoding);
        $qrCode->setWriterByName($format);
        #$qrCode->setValidateResult(true);

        return $qrCode->writeDataUri();
    }

    /**
     * @param string $text
     * @param string $path
     * @param int|null $size
     * @param string|null $encoding
     * @param string|null $format
     * @return string
     */
    public function createFile(string $text, string $path, int $size=null, string $encoding=null, string $format=null)
    {
        if (!$size) {
            $size = self::DEFAULT_SIZE;
        }
        if (!$encoding) {
            $encoding = self::DEFAULT_ENCODING;
        }
        if (!$format) {
            $format = self::DEFAULT_FORMAT;
        }

        $qrCode = new QrCode($text);
        $qrCode->setSize($size);
        $qrCode->setEncoding($encoding);
        $qrCode->setWriterByName($format);
        #$qrCode->setValidateResult(true);

        // Save it to a file
        $qrCode->writeFile($path);

        return true;
    }
}