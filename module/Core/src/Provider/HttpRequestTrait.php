<?php
namespace Core\Provider;

use GuzzleHttp\Client as GuzzleClient;
use Zend\Http\Client as ZendClient;
use Zend\Soap\Client as ZendSoapClient;

/**
 * Trait HttpRequestTrait
 * @package Core\Provider
 */
trait HttpRequestTrait
{
    /**
     * Http request with GuzzleClient
     *
     * @param $url
     * @param null $responseFormat
     * @return mixed|\Psr\Http\Message\StreamInterface
     * @see https://github.com/guzzle/guzzle
     */
    protected function guzzleHttpRequest($url, $responseFormat=null)
    {
        $client = new GuzzleClient();
        $response = $client->get($url); // или так $response = $client->request('GET', $url);

        if($responseFormat === 'json') {
            $result = json_decode($response->getBody(), true);
        } else {
            $result = $response->getBody();
        }

        return $result;
    }

    /**
     * Http request with ZendClient
     *
     * @param string $url
     * @param string $method OPTIONS|GET|HEAD|POST|PUT|DELETE|TRACE|CONNECT|PATCH|PROPFIND
     * @param array|string $data
     * @param array $options [
     *  'isAjax' => true|false, (default false)
     *  'headers' => [...] (example ['X-Requested-With: XMLHttpRequest', 'X-Powered-By: Zend Framework'])
     * ]
     * @return mixed
     * @see https://docs.zendframework.com/zend-http/
     */
    protected function zendHttpRequest($url, $method = 'GET', $data = [], $options = [])
    {
        $client = new ZendClient($url);
        $client->setMethod($method);

        // Set parameters
        if ($method === 'GET' && \is_array($data)) {
            $client->setParameterGet($data);
        }

        if ($method === 'POST' && \is_array($data)) {
            $client->setParameterPost($data);
        }

        if ($method === 'POST' && \is_string($data)) {
            $client->setRawBody($data);
        }

        // Set headers
        $headers = $client->getRequest()->getHeaders();

        if (isset($options['isAjax']) && $options['isAjax'] === true) {
            $headers->addHeaderLine('X-Requested-With: XMLHttpRequest');
        }

        if (isset($options['headers']) && \is_array($options['headers'])) {
            $headers->addHeaders($options['headers']);
        }

        $client->setHeaders($headers);

        #dump($client->getHeader('X-Auth'));

        //Get response
        $response = $client->send();

        return $response->getContent();
    }

    /**
     * @param string $url
     * @param string $method
     * @param array  $arguments
     * @return mixed
     * @see https://docs.zendframework.com/zend-soap/
     */
    protected function zendSoapRequest($url, $method, $arguments)
    {
        $client = new ZendSoapClient($url);
        $response = $client->$method($arguments);

        $result = json_decode(json_encode($response), true);  //stdClass to array
        if (\is_array($result) && \count($result) === 1) {
            $result = current($result);
        }

        return $result;
    }

    /**
     * @param $url
     * @return mixed
     * @TODO Почему-то не работает. Разобраться!
     */
    protected function curlHttpRequest($url)
    {
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1' );
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        #curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec( $ch );

        //$response = curl_getinfo( $ch );
        curl_close ( $ch );

        return $result;
    }

    /**
     * @param $url_transaction
     * @param $json_content
     * @param $headers
     * @return mixed
     * @throws \Exception
     */
    protected function curlMandarinRequest($url_transaction, $json_content, $headers)
    {
        $ch = curl_init($url_transaction);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_content);
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        // @TODO Only for DEVELOP MODE
        /** @noinspection CurlSslServerSpoofingInspection */
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);

        if (curl_errno($ch)) {

            throw new \Exception(curl_error($ch));
        }

        return json_decode($result);
    }
}