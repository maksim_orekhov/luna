<?php

namespace Core\Provider\Mail;

use Core\Service\TwigRenderer;

/**
 * Class MessagePluginSender
 * @package Core\Provider\Mail
 */
class MessagePluginSender extends BaseMailSender
{
    const INVALID_DATA = 'Trying send notification email with invalid data';

    const TYPE_NOTIFY_KEY = 'type_notify';
    const SPECIAL_MAIL = 'special_mail';
    const EXCEPTION_ALERT = 'exception_alert';

    /**
     * @var TwigRenderer $twigRenderer
     */
    private $twigRenderer;


    /**
     * MessagePluginSender constructor.
     * @param \Core\Service\TwigRenderer $twigRenderer
     * @param array $config
     */
    public function __construct(TwigRenderer $twigRenderer, array $config)
    {
        parent::__construct($config);

        $this->twigRenderer  = $twigRenderer;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function sendMail($email_value, $data)
    {
        $this->validateData($data);

        switch ($data[self::TYPE_NOTIFY_KEY]) {
            case self::SPECIAL_MAIL:
                $this->sendSpecialMail($email_value, $data);
                break;
            case self::EXCEPTION_ALERT:
                $this->sendExceptionAlert($email_value, $data);
                break;
        }
    }

    /**
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendSpecialMail($email_value, $data)
    {
        $subject = self::SUBJECT_TITLE.( $data['subject'] ?: 'Special message' );
        // Get main_project_url from config
        $main_project_url = $this->config['main_project_url'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'subject'           => $subject,
            'main_project_url'  => $main_project_url,
            'message'           => $data['message'],
        ]);
        $twigRenderer->setTemplate('twig/special_mail');
        $html = $twigRenderer->getHtml();

        $this->send($email_value, $subject, $html, null);
    }

    /**
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendExceptionAlert($email_value, $data)
    {
        $subject = self::SUBJECT_TITLE.'Exception alert';
        // Get main_project_url from config
        $main_project_url = $this->config['main_project_url'];

        $trace = explode ('#', $data['trace']);

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'subject'           => $subject,
            'main_project_url'  => $main_project_url,
            'message'           => $data['message'],
            'line'              => $data['line'],
            'file'              => $data['file'],
            'trace'             => $trace,
            'xdebug_message'    => $data['xdebug_message'],
        ]);

        $twigRenderer->setTemplate('twig/exception_alert');
        $html = $twigRenderer->getHtml();

        $this->send($email_value, $subject, $html, null);
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    private function validateData($data)
    {
        if(!array_key_exists(self::TYPE_NOTIFY_KEY, $data) || empty($data[self::TYPE_NOTIFY_KEY])) {

            throw new \Exception(self::INVALID_DATA);
        }
        return true;
    }
}