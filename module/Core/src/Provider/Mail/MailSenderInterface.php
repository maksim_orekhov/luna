<?php
namespace Core\Provider\Mail;

/**
 * Interface MailSenderInterface
 * @package Core\Provider\Mail
 */
interface MailSenderInterface
{
    /**
     * @param string    $email_value
     * @param array     $data
     * @return mixed
     */
    public function sendMail($email_value, $data);
}