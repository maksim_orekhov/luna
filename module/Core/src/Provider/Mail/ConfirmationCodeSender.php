<?php
namespace Core\Provider\Mail;

use Core\Service\TwigRenderer;
use Core\Provider\Mail\BaseMailSender;

/**
 * Class ConfirmationCodeSender
 * @package Core\Provider\Mail
 */
class ConfirmationCodeSender extends BaseMailSender
{
    const CONFIRMATION_CODE_IS_ABSENT = 'Trying send confirmation email without code data provided';

    private $twigRenderer;

    /**
     * ConfirmationCodeSender constructor.
     * @param TwigRenderer $twigRenderer
     * @param array $config
     */
    public function __construct(
        TwigRenderer $twigRenderer,
        array $config)
    {
        parent::__construct($config);

        $this->twigRenderer = $twigRenderer;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function sendMail($email_value, $data)
    {
        //checking data
        $this->checkValidityData($data);

        $code = $data['code'];
        $subject = self::SUBJECT_TITLE.'Код подтверждения';
        // Get main_project_url from config
        $main_project_url = $this->config['main_project_url'];

        // Html body
        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'user_login' => $data['user_login'],
            'subject' => $subject,
            'main_project_url' => $main_project_url,
            'code' => $code
        ]);
        $twigRenderer->setTemplate('twig/confirmation_code');
        $html = $twigRenderer->getHtml();

        $this->send($email_value, $subject, $html, null);

        return true;
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    private function checkValidityData($data): bool
    {
        if (!array_key_exists('code', $data)) {

            throw new \Exception(self::CONFIRMATION_CODE_IS_ABSENT);
        }

        return true;
    }
}