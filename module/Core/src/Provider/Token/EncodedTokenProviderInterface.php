<?php
namespace Core\Provider\Token;

use Application\Entity\User;
use Core\Entity\Interfaces\UserInterface;

/**
 * Interface EncodedTokenProviderInterface
 * @package Core\Provider\Token
 */
interface EncodedTokenProviderInterface
{
    /**
     * @param UserInterface|User|null $user
     * @param array $param
     * @return mixed
     */
    public function provideToken($user, array $param = []);

    /**
     * @param UserInterface|User|null $user
     * @param array $param
     * @return mixed
     */
    public function encodedToken($user, array $param = []);

    /**
     * @param string $email
     * @param array $param
     * @return mixed
     */
    public function send(string $email, array $param = []);
}