<?php
namespace Core\Provider\Token\Factory;

use Core\Provider\Mail\EmailConfirmationTokensSender;
use Core\Provider\Token\EmailConfirmationTokenProvider;
use Core\Service\Base\BaseTokenManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class ConfirmationCodeSenderFactory
 * @package Core\Provider\Mail\Factory
 */
class EmailConfirmationTokenProviderFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $baseTokenManager = $container->get(BaseTokenManager::class);
        $mailSender = $container->get(EmailConfirmationTokensSender::class);

        return new EmailConfirmationTokenProvider(
            $mailSender,
            $baseTokenManager
        );
    }
}