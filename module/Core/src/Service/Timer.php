<?php
namespace Core\Service;

/**
 * Класс для измерения времени выполнения скрипта или операций
 */
class Timer
{
    /**
     * @var float время начала выполнения скрипта
     */
    private static $start = .0;

    /**
     * Начало выполнения
     */
    public static function start()
    {
        self::$start = microtime(true);
    }

    /**
     * Разница между текущей меткой времени и меткой self::$start
     * @param bool $print
     * @param bool $round
     * @return float
     */
    public static function finish($print = true, $round = true)
    {
        if ($round) {
            $time = round(microtime(true) - self::$start, 4, PHP_ROUND_HALF_EVEN);
        } else {
            $time = microtime(true) - self::$start;
        }

        if ($print) {
            echo  '<pre>'.$time.' сек.</pre>';
        } else {
            return $time;
        }
    }
}