<?php
namespace Core\Service;

use Doctrine\ORM\EntityManager;
use Zend\Json\Decoder;

class JsonUtil
{
    public static function mb_encode($data): string
    {
        return Decoder::decodeUnicodeString(json_encode(\is_array($data) ? $data : []));
    }
}
