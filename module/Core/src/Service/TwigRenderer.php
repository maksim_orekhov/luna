<?php
namespace Core\Service;

use Twig_Environment;
use Zend\View\Model\ViewModel;
use ZendTwig\Renderer\TwigRenderer as BaseTwigRenderer;

use Zend\View\Resolver\ResolverInterface;
use Zend\View\View;


/**
 * Class TwigMailRenderer
 * @package Core\Service
 */
class TwigRenderer extends BaseTwigRenderer
{
    const ERROR_TWIG_MODEL_INIT = 'The twig view model is not declared';
    /**
     * @var TwigViewModel|null
     */
    private $twigViewModel = null;

    public function __construct(View $view, Twig_Environment $env = null, ResolverInterface $resolver = null)
    {
        parent::__construct($view, $env, $resolver);
    }

    /**
     * @param null $variables
     * @param null $options
     * @return TwigViewModel
     */
    public function initTwigModel($variables = null, $options = null)
    {
        $this->twigViewModel = new TwigViewModel($variables, $options);

        return $this->twigViewModel;
    }

    /**
     * @param $template
     * @return TwigViewModel|null
     * @throws \Exception
     */
    public function setTemplate($template)
    {
        if (! $this->twigViewModel) {
            throw new \Exception(self::ERROR_TWIG_MODEL_INIT);
        }
        $this->twigViewModel->setTemplate((string) $template);

        return $this->twigViewModel;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getHtml()
    {
        if (! $this->twigViewModel) {
            throw new \Exception(self::ERROR_TWIG_MODEL_INIT);
        }
        return $this->render($this->twigViewModel);
    }
}
