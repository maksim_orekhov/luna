<?php
namespace Core\Service\Auth\Factory;

use Core\Service\Auth\AuthService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\AbstractFactoryInterface;
use Zend\Session\SessionManager;
use Zend\Authentication\Storage\Session as SessionStorage;

class AbstractAuthServiceFactory implements AbstractFactoryInterface
{
    /**
     * @var array
     */
    protected $config;

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return mixed|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $sessionManager = $container->get(SessionManager::class);
        $authStorage = new SessionStorage('Zend_Auth', 'session', $sessionManager);

        $config = $this->getConfig($container);
        $authProvider = $container->get($config['provider']);
        $authAdapter = $authProvider->getAdapter();

        return new AuthService(
            $authStorage,
            $authAdapter
        );
    }

    /**
     * Can we create an adapter by the requested name?
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @return bool
     */
    public function canCreate(ContainerInterface $container, $requestedName)
    {
        $config = $this->getConfig($container);

        if (empty($config)) {
            return false;
        }

        return (
            array_key_exists('provider', $config)
            && ! empty($config['provider'])
            && \is_string($config['provider'])
        );
    }

    /**
     * Get auth configuration, if any
     *
     * @param  ContainerInterface $container
     * @return array
     */
    protected function getConfig(ContainerInterface $container)
    {
        if ($this->config !== null) {
            return $this->config;
        }

        if (! $container->has('config')) {
            $this->config = [];
            return $this->config;
        }

        $config = $container->get('config');
        if (! isset($config['auth_service'])
            || ! \is_array($config['auth_service'])
        ) {
            $this->config = [];
            return $this->config;
        }

        $config = $config['auth_service'];
        if (! isset($config['provider']) ) {
            $this->config = [];
            return $this->config;
        }

        $this->config = $config;
        return $this->config;
    }
}