<?php
namespace Core\Service;

use DateTimeZone;

class TimeZone
{
    /**
     * @var DateTimeZone
     */
    protected $timeZone;

    /**
     * TimeZone constructor.
     * @param $timeZone
     */
    public function __construct($timeZone)
    {
        $this->setTimeZone($timeZone);
    }

    /**
     * @param $timeZone
     * @return $this
     */
    public function setTimeZone($timeZone)
    {
        if (!$timeZone instanceof DateTimeZone) {
            $timeZone = new DateTimeZone($timeZone);
        }
        $this->timeZone = $timeZone;

        return $this;
    }

    /**
     * @return DateTimeZone
     */
    public function getTimeZone()
    {
        return $this->timeZone;
    }

    /**
     * @return $this
     */
    public function apply()
    {
        date_default_timezone_set($this->timeZone->getName());

        return $this;
    }
}