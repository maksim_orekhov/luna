<?php
namespace Core\Service\Base\Factory;

use Core\Adapter\PhoneNumberUtilAdapter;
use Core\Service\Base\BasePhoneManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class BasePhoneManagerFactory
 * @package Core\Service
 */
class BasePhoneManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return BasePhoneManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $phoneNumberUtilAdapter = new PhoneNumberUtilAdapter();

        return new BasePhoneManager(
            $entityManager,
            $phoneNumberUtilAdapter
        );
    }
}
