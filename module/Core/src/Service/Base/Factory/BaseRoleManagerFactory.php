<?php
namespace Core\Service\Base\Factory;

use Core\Service\Base\BaseRoleManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class BaseRoleManagerFactory
 * @package Core\Service
 */
class BaseRoleManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return BaseRoleManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new BaseRoleManager(
            $entityManager
        );
    }
}
