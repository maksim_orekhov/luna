<?php
namespace Core\Service\Base;

use Core\Adapter\TokenAdapter;
use Core\Entity\Interfaces\EmailInterface;
use Core\Entity\Interfaces\UserInterface;
use Application\Entity\Email;
use Core\Exception\LogicException;
use Core\Service\Base\BaseUserManager;
use Core\Service\Base\BaseEmailManager;
use Core\Service\SessionContainerManager;
use Doctrine\ORM\EntityManager;

class BaseTokenManager
{
    use \Core\Provider\Token\SimpleTokenGeneratorTrait;
    /**
     * @var EntityManager
     */
    protected $entityManager;
    /**
     * @var TokenAdapter
     */
    protected $tokenAdapter;
    /**
     * @var BaseEmailManager
     */
    protected $baseEmailManager;
    /**
     * @var BaseUserManager
     */
    protected $baseUserManager;
    /**
     * @var array
     */
    protected $config;
    /**
     * @var SessionContainerManager
     */
    protected $sessionContainerManager;


    /**
     * TokenManager constructor.
     * @param EntityManager $entityManager
     * @param TokenAdapter $tokenAdapter
     * @param BaseEmailManager $baseEmailManager
     * @param BaseUserManager $baseUserManager
     * @param SessionContainerManager $sessionContainerManager
     * @param array $config
     */
    public function __construct(EntityManager $entityManager,
                                TokenAdapter $tokenAdapter,
                                BaseEmailManager $baseEmailManager,
                                BaseUserManager $baseUserManager,
                                SessionContainerManager $sessionContainerManager,
                                array $config)
    {
        $this->entityManager = $entityManager;
        $this->tokenAdapter = $tokenAdapter;
        $this->baseEmailManager = $baseEmailManager;
        $this->baseUserManager = $baseUserManager;
        $this->sessionContainerManager = $sessionContainerManager;
        $this->config = $config;
    }

    /**
     * @param $encoded_token
     * @return array
     * @throws LogicException
     */
    private function decodeBaseToken($encoded_token)
    {
        // Validate token
        if (!\is_string($encoded_token)) {

            throw new LogicException(null, LogicException::TOKEN_NOT_VALID);
        }

        try {
            // Decode token
            $decodedToken = $this->tokenAdapter->decodeToken($encoded_token);

            return [
                'email_value' => strtolower($decodedToken->email),
                'simple_token' => $decodedToken->simple_token,
            ];
        }
        catch(\Throwable $t) {

            throw new LogicException(null, LogicException::TOKEN_NOT_VALID);
        }
    }

    /**
     * @param $encoded_token
     * @return array
     * @throws LogicException
     */
    private function decodeEmailConfirmationToken($encoded_token)
    {
        return $this->decodeBaseToken($encoded_token);
    }

    /**
     * @param $encoded_token
     * @return array
     * @throws LogicException
     */
    private function decodePasswordResetToken($encoded_token)
    {
        return $this->decodeBaseToken($encoded_token);
    }

    /**
     * Generate and return jwToken
     *
     * @param \Application\Entity\Email $email
     * @return string
     * @throws \Exception
     */
    public function createEmailConfirmationToken(Email $email)
    {
        // Check if confirmation token request period has passed
        if($email->getConfirmationTokenCreationDate()) {
            // If enough time of the last request of the token has not passed
            $request_period = $this->config['tokens']['email_confirmation_token_request_period'];
            /** @var \DateTime $confirmationTokenCreationDate */
            $confirmationTokenCreationDate = $email->getConfirmationTokenCreationDate();
            $tokenCreationDate = clone $confirmationTokenCreationDate;

            if ($tokenCreationDate->modify('+'.$request_period.' seconds') > new \DateTime()) {

                throw new LogicException(null, LogicException::TOKEN_NOT_GENERATED_NOT_ENOUGH_TIMER_PASSED);
            }
        }
        // Create simple token
        $simple_token = $this->tokenGenerate($this->config['tokens']['email_confirmation_token_length']);
        // Get Confirmation Token
        $encodedToken = $this->tokenAdapter->createToken($email->getEmail(), $simple_token);
        // Set Token to Email object
        $email->setConfirmationToken($simple_token);
        // Set Token Creation Date to Email object
        $email->setConfirmationTokenCreationDate($this->tokenCreationDate());
        // Save to DB
        $this->entityManager->flush();

        return $encodedToken;
    }

    /**
     * @param $encoded_token
     * @return EmailInterface
     * @throws LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function validateEmailConfirmationToken($encoded_token)
    {
        $decoded_token = $this->decodeEmailConfirmationToken($encoded_token);
        if (!$this->baseEmailManager->checkEmailUnique($decoded_token['email_value']) ) {

            throw new LogicException(null, LogicException::EMAIL_IS_ALREADY_CONFIRMED);
        }
        $email = $this->baseEmailManager->getEmailByValueAndSimpleToken($decoded_token['email_value'], $decoded_token['simple_token']);
        if ($email === null) {

            throw new LogicException(null, LogicException::EMAIL_NOT_MATCHING_SIMPLE_TOKEN);
        }
        // Validation by time (expiration)
        $currentDate = new \DateTime();
        $expiration_time = $this->config['tokens']['email_confirmation_token_expiration_time'];

        if ($email->getConfirmationTokenCreationDate() < $currentDate->modify('-'.$expiration_time.' seconds')) {

            throw new LogicException(null, LogicException::TOKEN_IS_EXPIRED);
        }

        // If all verifications successfully passed
        $email = $this->baseEmailManager->setEmailConfirmed($email);

        return $email;
    }

    /**
     * @param UserInterface $user
     * @return null|string
     * @throws \Exception
     */
    public function createPasswordResetToken(UserInterface $user)
    {
        $currentDate = new \DateTime();
        // Get previous token creation date
        $tokenCreationDate = $user->getPasswordResetTokenCreationDate();
        $request_period = $this->config['tokens']['password_resetting_token_request_period'];
        // Check if previous token creation date not expired
        if ($tokenCreationDate && $tokenCreationDate > $currentDate->modify('-' . $request_period . ' seconds')) {

            throw new LogicException(null, LogicException::TOKEN_NOT_GENERATED_NOT_ENOUGH_TIMER_PASSED);
        }
        // If no Email object
        if( !$user->getEmail() ) {

            throw new LogicException(null, LogicException::EMAIL_ENTITY_EXPECTED_BUT_ACTUAL_NULL);
        }
        try {
            // Generate Simple Token
            $simple_token = $this->tokenGenerate($this->config['tokens']['password_resetting_token_length']);
            // Generate Confirmation Token
            $encodedToken = $this->tokenAdapter->createToken($user->getEmail()->getEmail(), $simple_token);
        }
        catch(\Throwable $t) {

            throw new LogicException(null, LogicException::TOKEN_GENERATING_FAILED);
        }
        // If no Throwable thrown
        // Set Token
        $user->setPasswordResetToken($simple_token);
        // Set Token Creation Date
        $user->setPasswordResetTokenCreationDate($this->tokenCreationDate());

        $this->entityManager->flush();

        return $encodedToken;
    }

    /**
     * @param $encoded_token
     * @return UserInterface
     * @throws LogicException
     */
    public function validatePasswordResetToken($encoded_token)
    {
        $decoded_token = $this->decodePasswordResetToken($encoded_token);
        /** @var UserInterface $user */
        $user = $this->baseUserManager->getUserByPasswordResetToken($decoded_token['simple_token']);
        if ($user === null || $user->getEmail()->getEmail() !== $decoded_token['email_value']) {

            throw new LogicException(null, LogicException::USER_NOT_MATCHING_SIMPLE_TOKEN);
        }
        // Validation by time (expiration)
        $currentDate = new \DateTime();
        $expiration_time = $this->config['tokens']['password_resetting_token_expiration_time'];
        // If more than specified in the configuration period have passed
        if ($user->getPasswordResetTokenCreationDate() < $currentDate->modify('-' . $expiration_time . ' seconds')) {

            throw new LogicException(null, LogicException::TOKEN_IS_EXPIRED);
        }

        return $user;
    }
}