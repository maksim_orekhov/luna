<?php
namespace Core\Service\Base;

use Core\Adapter\PhoneNumberUtilAdapter;
use Core\Entity\Interfaces\PhoneInterface;
use Core\Entity\Interfaces\UserInterface;
use Application\Entity\Country;
use Application\Entity\E164;
use Application\Entity\Phone;
use Core\Exception\LogicException;
use Doctrine\ORM\EntityManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class BasePhoneManager
{
    /**
     * @var EntityManager
     */
    protected $entityManager;
    /**
     * @var PhoneNumberUtilAdapter
     */
    private $phoneNumberUtilAdapter;

    /**
     * BaseEmailManager constructor.
     * @param EntityManager $entityManager
     * @param PhoneNumberUtilAdapter $phoneNumberUtilAdapter
     */
    public function __construct(EntityManager $entityManager,
                                PhoneNumberUtilAdapter $phoneNumberUtilAdapter)
    {
        $this->entityManager = $entityManager;
        $this->phoneNumberUtilAdapter = $phoneNumberUtilAdapter;
    }

    /**
     * @param $phone_value
     * @return \Application\Entity\Phone
     * @throws LogicException
     */
    public function addNewPhone($phone_value)
    {
        try {
            // $phone_value всегда должен быть с плюсом(+)
            $phoneNumberObj = $this->phoneNumberUtilAdapter->getPhoneNumberObj($phone_value);
            // Get country code (например, 'RU' для России)
            $countryCode = $this->phoneNumberUtilAdapter->getCountryCode($phoneNumberObj);

            // Create new Phone object
            $phone = new Phone();
            $phone->setCountry($this->selectCountryByCountryCode($countryCode));
            $phone_value = $this->removePlusFromPhoneNumber($phone_value);
            $phone->setUserEnter($phone_value);
            $phone->setPhoneNumber($phoneNumberObj->getCountryCode().$phoneNumberObj->getNationalNumber());

            //@TODO Сделать с PhoneNumberAdapter
            $e164_array = $this->makeE164($phone_value);

            $e164 = new E164();
            $hydrator = new DoctrineHydrator($this->entityManager, $e164);
            $e164 = $hydrator->hydrate($e164_array, $e164);

            $this->entityManager->persist($e164);
            $phone->setE164($e164);
            $this->entityManager->persist($phone);

            $this->entityManager->flush();

            return $phone;

        } catch (\Throwable $throwable) {

            throw new LogicException(null, LogicException::PHONE_CREATED_FAILED);
        }
    }

    /**
     * Редактирование номера существущего телефона
     *
     * @param $current_phone_value
     * @param $new_phone_value
     * @return Phone
     * @throws LogicException
     * @throws \Throwable
     * @throws \libphonenumber\NumberParseException
     */
    public function editUsersPhone($current_phone_value, $new_phone_value)
    {
        $phoneNumberObj = $this->phoneNumberUtilAdapter->getPhoneNumberObj($new_phone_value);

        if(!$phoneNumberObj) {

            throw new LogicException(null, LogicException::PHONE_NUMBER_ERROR_PARSING);
        }
        try {
            // Get country code (например, 'RU' для России)
            $countryCode = $this->phoneNumberUtilAdapter->getCountryCode($phoneNumberObj);

            // Get Country by country code
            $country = $this->selectCountryByCountryCode($countryCode);

            if (!$country || !$country instanceof Country) {

                throw new LogicException(null, LogicException::PHONE_COUNTRY_NOT_DEFINED);
            }
            /** @var \Application\Entity\Phone $phone */
            $phone = $this->getPhoneByNumber($current_phone_value);
            // Set Country object to Phone object
            $phone->setCountry($country);

            $phone_value = $this->removePlusFromPhoneNumber($new_phone_value);
            $phone->setUserEnter($phone_value);
            $phone->setPhoneNumber($phoneNumberObj->getCountryCode().$phoneNumberObj->getNationalNumber());
            $phone->setIsVerified(false);

            $e164_array = $this->makeE164($phone_value);
            $e164 = new E164();
            $hydrator = new DoctrineHydrator($this->entityManager, $e164);
            $e164 = $hydrator->hydrate($e164_array, $e164);
            // Set E164 object to Phone object
            $phone->setE164($e164);

            return $phone;

        } catch (\Throwable $throwable){

            throw new LogicException(null, LogicException::PHONE_NUMBER_ERROR_PARSING);
        }
    }

    /**
     * @param $countryCode
     * @return Country|null
     */
    private function selectCountryByCountryCode($countryCode)
    {
        /** @var Country $country */
        $country = $this->entityManager->getRepository(Country::class)->findOneBy(['code' => $countryCode]);

        return $country ?? null;
    }

    /**
     * @param $phone_value
     * @return bool|string
     */
    private function removePlusFromPhoneNumber($phone_value)
    {
        if(preg_match('/^[\+]/', $phone_value, $matches)){
            $phone_value = substr($phone_value, 1, \strlen($phone_value));
        }
        return $phone_value;
    }

    /**
     * @param $user_enter
     * @return array
     */
    protected function makeE164($user_enter){
        //2 do: переделать
        $country_code = $user_enter[0];
        $region_code = substr($user_enter, 1, 3);
        $phone_number = substr($user_enter, 4, \strlen($user_enter));

        return [
            'country_code'  =>  $country_code,
            'region_code'   =>  $region_code,
            'phone_number'  =>  $phone_number
        ];
    }

    /**
     * @param $phone_number
     * @return \Application\Entity\Phone|null
     */
    public function getPhoneByNumber($phone_number)
    {
        $phone_number = $this->removePlusFromPhoneNumber($phone_number);

        /** @var \Application\Entity\Phone $phone */
        $phone = $this->entityManager->getRepository(Phone::class)
            ->findOneBy(['phoneNumber' => $phone_number]);

        return $phone;
    }

    /**
     * @param $phone_number
     * @return bool
     */
    public function checkPhoneUnique($phone_number)
    {
        /** @var PhoneInterface $phone */
        $phone = $this->getPhoneVerifiedByNumber($phone_number);
        //если найден телефон то не унекально
        return $phone ? false : true;
    }

    /**
     * @param $phone_number
     * @return PhoneInterface|null
     */
    public function getPhoneVerifiedByNumber($phone_number)
    {
        $phone_num = $this->removePlusFromPhoneNumber($phone_number);

        /** @var PhoneInterface $phone */
        $phone = $this->entityManager->getRepository(Phone::class)->findOneBy(['phoneNumber' => $phone_num, 'isVerified' => true]);
        return $phone ?? null;
    }

    /**
     * @param PhoneInterface $phone
     * @return PhoneInterface
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setPhoneConfirmed(PhoneInterface $phone)
    {
        $phone->setIsVerified(1);
        $this->entityManager->flush();

        return $phone;
    }
}