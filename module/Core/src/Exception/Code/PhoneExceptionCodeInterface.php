<?php
namespace Core\Exception\Code;

/**
 * Interface PhoneExceptionCodeInterface
 * @package Core\Exception
 */
interface PhoneExceptionCodeInterface
{
    /**
     * префикс PHONE, для сообшений суфикс _MESSAGE
     */
    const PHONE_NOT_FOUND = 'PHONE_NOT_FOUND';
    const PHONE_NOT_FOUND_MESSAGE = 'Phone not found.';

    const PHONE_CREATED_FAILED = 'PHONE_CREATED_FAILED';
    const PHONE_CREATED_FAILED_MESSAGE = 'Phone created failed.';

    const PHONE_EDIT_FAILED = 'PHONE_EDIT_FAILED';
    const PHONE_EDIT_FAILED_MESSAGE = 'Phone edit failed.';

    const PHONE_NUMBER_ERROR_PARSING = 'PHONE_NUMBER_ERROR_PARSING';
    const PHONE_NUMBER_ERROR_PARSING_MESSAGE = 'Phone number error parsing.';

    const PHONE_IS_ALREADY_VERIFIED = 'PHONE_IS_ALREADY_VERIFIED';
    const PHONE_IS_ALREADY_VERIFIED_MESSAGE = 'Phone is already verified.';

    const PHONE_NOT_UNIQUE = 'PHONE_NOT_UNIQUE';
    const PHONE_NOT_UNIQUE_MESSAGE = 'Такой номер телефона уже используется.';

    const PHONE_ALREADY_EXISTS = 'PHONE_ALREADY_EXISTS';
    const PHONE_ALREADY_EXISTS_MESSAGE = 'Телефон уже существует.';

    const PHONE_ENTERED_NOT_BELONG_USER = 'PHONE_ENTERED_NOT_BELONG_USER';
    const PHONE_ENTERED_NOT_BELONG_USER_MESSAGE = 'Введенный текущий телефон не соответствует телефону, указанному в профайле.';

    const PHONE_NUMBER_IS_SAME_AS_PHONE_IN_PROFILE = 'PHONE_NUMBER_IS_SAME_AS_PHONE_IN_PROFILE';
    const PHONE_NUMBER_IS_SAME_AS_PHONE_IN_PROFILE_MESSAGE = 'Введенный телефон совпадает с телефоном, указанным в профайле.';

    const PHONE_INVALID_CODE = 'PHONE_INVALID_CODE';
    const PHONE_INVALID_CODE_MESSAGE = 'Invalid code entered';

    const PHONE_COUNTRY_NOT_DEFINED = 'PHONE_COUNTRY_NOT_DEFINED';
    const PHONE_COUNTRY_NOT_DEFINED_MESSAGE = 'Can not determine the country';

    const PHONE_PLUS_SYMBOL_ABSENT = 'PHONE_PLUS_SYMBOL_ABSENT';
    const PHONE_PLUS_SYMBOL_ABSENT_MESSAGE = 'Plus symbol absent';
}