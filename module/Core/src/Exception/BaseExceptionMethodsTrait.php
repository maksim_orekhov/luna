<?php
namespace Core\Exception;

/**
 * Trait BaseExceptionMethodsTrait
 * @package Core\Exception
 */
trait BaseExceptionMethodsTrait
{
    private $exceptionType;
    private $exceptionCode;
    private $exceptionMessage;
    /**
     * @return mixed|string
     */
    public function getExceptionType()
    {
        return $this->exceptionType;
    }
    /**
     * @return string
     */
    public function getExceptionCode()
    {
        return $this->exceptionCode;
    }
    /**
     * @return string
     */
    public function getExceptionMessage()
    {
        return $this->exceptionMessage;
    }
    /**
     * @return mixed
     */
    public function setExceptionType()
    {
        $explode = explode('\\', \get_class($this));
        $this->exceptionType = end($explode);

        return $this->exceptionType;
    }
    /**
     * @param string $exceptionCode
     * @return mixed|null
     * @throws \RuntimeException
     */
    public function setExceptionCode($exceptionCode)
    {
        if ( empty($exceptionCode) || $exceptionCode === null) {

            $this->exceptionCode = null;
            //return code
            return $this->exceptionCode;
        }

        try {
            $code = @\constant('self::'.$exceptionCode);
        }
        catch (\Throwable $t) {

            $code = null;
        }

        if($code){
            $this->exceptionCode = $code;
            //return code
            return $this->exceptionCode;
        }

        throw new \RuntimeException("constant \"$exceptionCode\" not found");
    }
    /**
     * @param string $exceptionMessage
     * @return mixed|string
     * @throws \RuntimeException
     */
    public function setExceptionMessage($exceptionMessage)
    {
        if ( !empty($exceptionMessage) ) {

            try {
                $code = @\constant('self::'.$exceptionMessage);
            }
            catch (\Throwable $t) {

                $code = null;
            }

            if($code){

                throw new \RuntimeException('Message "'.$exceptionMessage.'" matching with constant for code');
            }

            $this->exceptionMessage = $exceptionMessage;
            //return message
            return $this->exceptionMessage;
        }

        if ( $this->exceptionCode === null) {

            $this->exceptionMessage = null;
            //return message
            return $this->exceptionMessage;
        }

        try {
            $message = @\constant('self::'.$this->exceptionCode.'_MESSAGE');
        }
        catch (\Throwable $t) {

            $message = null;
        }

        if ($message) {

            $this->exceptionMessage = $message;
            //return message
            return $this->exceptionMessage;
        }

        throw new \RuntimeException('Message to code "'.$this->exceptionCode.'" not found');
    }
}