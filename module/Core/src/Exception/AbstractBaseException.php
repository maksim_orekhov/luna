<?php
namespace Core\Exception;


/**
 * Class AbstractBaseException
 * @package Core\Exception
 */
abstract class AbstractBaseException extends \Exception
{
    /**
     * @return string
     */
    abstract public function getExceptionType();
    /**
     * @return string
     */
    abstract public function getExceptionCode();
    /**
     * @return string
     */
    abstract public function getExceptionMessage();
    /**
     * @param $exceptionCode
     * @return mixed|string
     */
    abstract protected  function setExceptionCode($exceptionCode);
    /**
     * @param string $exceptionCode
     * @return mixed|string
     */
    abstract protected function setExceptionMessage($exceptionCode);
}