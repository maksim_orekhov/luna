<?php
namespace Core\Exception;

use Application\Exception\ApplicationExceptionCodeInterface;

/**
 * Class LogicException
 * @package Core\Exception
 */
class LogicException extends AbstractBaseException
    implements
    BaseExceptionCodeInterface,
    ApplicationExceptionCodeInterface,
    ModuleExceptionCodeInterface
{
    use BaseExceptionMethodsTrait;
    /**
     * LogicException constructor.
     * @param null $exceptionMessage
     * @param null $exceptionCode
     * @throws \RuntimeException
     */
    public function __construct($exceptionMessage = null, $exceptionCode = null)
    {
        error_reporting(E_ALL & ~E_WARNING);

        $this->setExceptionCode($exceptionCode);
        $this->setExceptionMessage($exceptionMessage);
        $this->setExceptionType();

        parent::__construct($this->exceptionMessage, 0, null);
    }
}