<?php
namespace Core\Exception;

use ModuleFileManager\Exception\FileExceptionCodeInterface;

/**
 * Interface ModuleExceptionCodeInterface
 * @package Core\Exception
 */
interface ModuleExceptionCodeInterface extends
    FileExceptionCodeInterface
{}