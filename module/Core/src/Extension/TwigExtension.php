<?php
namespace Core\Extension;

use Twig_Extension;
use Twig_SimpleFunction;
use Twig_SimpleTest;

class TwigExtension extends Twig_Extension
{
    /**
     * @var array
     */
    private $config;

    /**
     * TwigExtension constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'extwig-class-extension';
    }

    /**
     * {@inheritDoc}
     */
    public function getFunctions()
    {
        return [
            'get_class' => new Twig_SimpleFunction('get_class', [$this, 'getClassName']),
            'sort_by_field' => new Twig_SimpleFunction('sort_by_field', [$this, 'sortByField']),
            'getApplicationEnv' => new Twig_SimpleFunction('getApplicationEnv', [$this, 'getApplicationEnv']),
            'first' => new Twig_SimpleFunction('first', [$this, 'first']),
            'last' => new Twig_SimpleFunction('last', [$this, 'last']),
            'generateCsrf' => new Twig_SimpleFunction('generateCsrf', [$this, 'generateCsrf']),
            'getReCaptchaV2SiteKey' => new Twig_SimpleFunction('getReCaptchaV2SiteKey', [$this, 'getReCaptchaV2SiteKey']),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getTests()
    {
        return [
            'instanceof' => new Twig_SimpleTest('instanceof', [$this, 'isInstanceOf']),
        ];
    }

    /**
     * @param object $object
     * @return string
     */
    public function getClassName($object)
    {
        return \get_class($object);
    }

    /**
     * @param object|string $actualClass
     * @param object|string $expectClass
     * @return boolean
     */
    public function isInstanceOf($actualClass, $expectClass)
    {
        return $actualClass instanceof $expectClass;
    }

    /**
     * @param $content
     * @param null $sort_by
     * @param string $direction
     * @param string $type_value
     * @return array
     */
    public function sortByField($content, $sort_by = null, $direction = 'asc', $type_value = 'int') {
        if (is_a($content, 'Doctrine\Common\Collections\Collection')) {
            $content = $content->toArray();
        }
        if (!is_array($content)) {
            throw new \InvalidArgumentException('Variable passed to the sortByField filter is not an array');
        } elseif (count($content) < 1) {
            return $content;
        } elseif ($sort_by === null) {
            throw new Exception('No sort by parameter passed to the sortByField filter');
        } elseif (!self::isSortable(current($content), $sort_by)) {
            throw new Exception('Entries passed to the sortByField filter do not have the field "' . $sort_by . '"');
        } else {
            // Unfortunately have to suppress warnings here due to __get function
            // causing usort to think that the array has been modified:
            // usort(): Array was modified by the user comparison function
            @usort($content, function ($a, $b) use ($sort_by, $direction, $type_value) {
                $flip = ($direction === 'desc') ? -1 : 1;
                if (is_array($a))
                    $a_sort_value = $a[$sort_by];
                else if (method_exists($a, 'get' . ucfirst($sort_by)))
                    $a_sort_value = $a->{'get' . ucfirst($sort_by)}();
                else
                    $a_sort_value = $a->$sort_by;
                if (is_array($b))
                    $b_sort_value = $b[$sort_by];
                else if (method_exists($b, 'get' . ucfirst($sort_by)))
                    $b_sort_value = $b->{'get' . ucfirst($sort_by)}();
                else
                    $b_sort_value = $b->$sort_by;
                if ($type_value === 'date') {
                    $a_sort_value = strtotime($a_sort_value);
                    $b_sort_value = strtotime($b_sort_value);
                }
                if ($a_sort_value == $b_sort_value) {
                    return 0;
                } else if ($a_sort_value > $b_sort_value) {
                    return (1 * $flip);
                } else {
                    return (-1 * $flip);
                }
            });
        }
        return $content;
    }

    /**
     * Validate the passed $item to check if it can be sorted
     * @param $item mixed Collection item to be sorted
     * @param $field string
     * @return bool If collection item can be sorted
     */
    private static function isSortable($item, $field) {
        if (is_array($item))
            return array_key_exists($field, $item);
        elseif (is_object($item))
            return isset($item->$field) || property_exists($item, $field);
        else
            return false;
    }

    /**
     * Application Environment
     * @return string
     */
    public function getApplicationEnv()
    {
        return APP_ENV;
    }

    /**
     * @param $array
     * @return array|mixed
     */
    function first($array)
    {
        if ( !is_array($array) ) {
            return $array;
        }
        if ( !count($array) ) {
            return [];
        };
        reset($array);

        return $array[key($array)];
    }

    /**
     * @param $array
     * @return array|mixed
     */
    function last($array)
    {
        if (!is_array($array)) {
            return $array;
        }
        if (!count($array)) {
            return [];
        }
        end($array);

        return $array[key($array)];
    }

    public function generateCsrf()
    {
        $csrf = new \Zend\Form\Element\Csrf('csrf');
        $csrf->setCsrfValidatorOptions([
            'timeout' => 3600
        ]);
        return $csrf->getValue();
    }

    public function getReCaptchaV2SiteKey()
    {
        return $this->config['recaptcha_v2']['site_key'];
    }
}
