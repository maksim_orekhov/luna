<?php
namespace Core;

use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'controller_plugins' => [
        'factories' => [
            Controller\Plugin\Message\MessagePlugin::class => Controller\Plugin\Message\Factory\MessagePluginFactory::class,
            Controller\Plugin\CookiePlugin::class => Controller\Plugin\Factory\CookiePluginFactory::class,
            Controller\Plugin\SessionPlugin::class => InvokableFactory::class,
        ],
        'aliases' => [
            'message' => Controller\Plugin\Message\MessagePlugin::class,
            'cookie' => Controller\Plugin\CookiePlugin::class,
            'sessionPlugin' => Controller\Plugin\SessionPlugin::class,
        ],
    ],
];
