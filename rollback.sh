#!/bin/bash

color_output() {
  echo -e "\033[$2m$1\033[0m"
}

title_label() {
  color_output "$1" "$START_END_SCRIPT_COLOR"
}

START_END_SCRIPT_COLOR=37
START_TASK_COLOR=33
END_COLOR=36
INTERACTION=32

################################
# script execution starts here #
################################

color_output "-> This runs fixtures installation (db will be purged) and clears product upload folders. Continue (y/N)?" "$INTERACTION"
read CONTINUE

if [ "$CONTINUE" == "y" ]; then

    # product folder clearing
	echo ""
	color_output "-> product folder clearing..." "$START_TASK_COLOR"

	if [ "$(ls -A upload/product)" ]; then
	     rm -rf upload/product/*
	else
		echo ""
	    echo "product folder is empty"
	fi

	# Fixtures
	echo ""
	rm -rf data/cache/*
	touch data/cache/.gitkeep
	color_output "-> Fixtures..." "$START_TASK_COLOR"
	./vendor/bin/doctrine-module --no-interaction orm:fixtures:load
	echo ""



	echo ""
	
	echo ""
	title_label "System state rollback finished at: $(date)"
	echo ""

else
	echo ""
	color_output "Exit" "$END_COLOR"
fi
