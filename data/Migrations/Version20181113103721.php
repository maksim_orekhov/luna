<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181113103721 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE purchase (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, address_id INT DEFAULT NULL, name VARCHAR(128) NOT NULL, slug VARCHAR(255) DEFAULT NULL, code VARCHAR(64) DEFAULT NULL, description LONGTEXT DEFAULT NULL, publication_period SMALLINT NOT NULL, price NUMERIC(10, 2) DEFAULT NULL, created DATETIME NOT NULL, changed DATETIME NOT NULL, deadline DATETIME DEFAULT NULL, visible TINYINT(1) DEFAULT NULL, removed TINYINT(1) DEFAULT NULL, INDEX IDX_6117D13BA76ED395 (user_id), UNIQUE INDEX UNIQ_6117D13BF5B7AF75 (address_id), INDEX purchase_slug (slug), INDEX purchase_created (created), INDEX purchase_visible (visible), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE purchases_files (purchase_id INT NOT NULL, file_id INT NOT NULL, INDEX IDX_5F7FFD78558FBEB9 (purchase_id), UNIQUE INDEX UNIQ_5F7FFD7893CB796C (file_id), PRIMARY KEY(purchase_id, file_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE purchases_galleries (purchase_id INT NOT NULL, file_id INT NOT NULL, INDEX IDX_3A278570558FBEB9 (purchase_id), UNIQUE INDEX UNIQ_3A27857093CB796C (file_id), PRIMARY KEY(purchase_id, file_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE purchases_categories (purchase_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_3F3E3890558FBEB9 (purchase_id), INDEX IDX_3F3E389012469DE2 (category_id), PRIMARY KEY(purchase_id, category_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE purchase_cities (purchase_id INT NOT NULL, city_id INT NOT NULL, INDEX IDX_232B33E558FBEB9 (purchase_id), INDEX IDX_232B33E8BAC62AF (city_id), PRIMARY KEY(purchase_id, city_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE purchases_moderations (purchase_id INT NOT NULL, moderation_id INT NOT NULL, INDEX IDX_7916F5C0558FBEB9 (purchase_id), UNIQUE INDEX UNIQ_7916F5C067FDB807 (moderation_id), PRIMARY KEY(purchase_id, moderation_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE purchase_prolongation (id INT AUTO_INCREMENT NOT NULL, purchase_id INT DEFAULT NULL, days SMALLINT NOT NULL, created DATETIME NOT NULL, INDEX IDX_2FA56EA7558FBEB9 (purchase_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE purchase ADD CONSTRAINT FK_6117D13BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE purchase ADD CONSTRAINT FK_6117D13BF5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE purchases_files ADD CONSTRAINT FK_5F7FFD78558FBEB9 FOREIGN KEY (purchase_id) REFERENCES purchase (id)');
        $this->addSql('ALTER TABLE purchases_files ADD CONSTRAINT FK_5F7FFD7893CB796C FOREIGN KEY (file_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE purchases_galleries ADD CONSTRAINT FK_3A278570558FBEB9 FOREIGN KEY (purchase_id) REFERENCES purchase (id)');
        $this->addSql('ALTER TABLE purchases_galleries ADD CONSTRAINT FK_3A27857093CB796C FOREIGN KEY (file_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE purchases_categories ADD CONSTRAINT FK_3F3E3890558FBEB9 FOREIGN KEY (purchase_id) REFERENCES purchase (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE purchases_categories ADD CONSTRAINT FK_3F3E389012469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE purchase_cities ADD CONSTRAINT FK_232B33E558FBEB9 FOREIGN KEY (purchase_id) REFERENCES purchase (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE purchase_cities ADD CONSTRAINT FK_232B33E8BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE purchases_moderations ADD CONSTRAINT FK_7916F5C0558FBEB9 FOREIGN KEY (purchase_id) REFERENCES purchase (id)');
        $this->addSql('ALTER TABLE purchases_moderations ADD CONSTRAINT FK_7916F5C067FDB807 FOREIGN KEY (moderation_id) REFERENCES moderation (id)');
        $this->addSql('ALTER TABLE purchase_prolongation ADD CONSTRAINT FK_2FA56EA7558FBEB9 FOREIGN KEY (purchase_id) REFERENCES purchase (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE purchases_files DROP FOREIGN KEY FK_5F7FFD78558FBEB9');
        $this->addSql('ALTER TABLE purchases_galleries DROP FOREIGN KEY FK_3A278570558FBEB9');
        $this->addSql('ALTER TABLE purchases_categories DROP FOREIGN KEY FK_3F3E3890558FBEB9');
        $this->addSql('ALTER TABLE purchase_cities DROP FOREIGN KEY FK_232B33E558FBEB9');
        $this->addSql('ALTER TABLE purchases_moderations DROP FOREIGN KEY FK_7916F5C0558FBEB9');
        $this->addSql('ALTER TABLE purchase_prolongation DROP FOREIGN KEY FK_2FA56EA7558FBEB9');
        $this->addSql('DROP TABLE purchase');
        $this->addSql('DROP TABLE purchases_files');
        $this->addSql('DROP TABLE purchases_galleries');
        $this->addSql('DROP TABLE purchases_categories');
        $this->addSql('DROP TABLE purchase_cities');
        $this->addSql('DROP TABLE purchases_moderations');
        $this->addSql('DROP TABLE purchase_prolongation');
    }
}
