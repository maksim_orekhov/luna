<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181109155248 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE products_shops');
        $this->addSql('ALTER TABLE fabricator DROP INDEX IDX_E0378BFFA76ED395, ADD UNIQUE INDEX UNIQ_E0378BFFA76ED395 (user_id)');
        $this->addSql('ALTER TABLE shop DROP INDEX IDX_AC6A4CA2A76ED395, ADD UNIQUE INDEX UNIQ_AC6A4CA2A76ED395 (user_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE products_shops (product_id INT NOT NULL, shop_id INT NOT NULL, INDEX IDX_C32399534584665A (product_id), INDEX IDX_C32399534D16C4DD (shop_id), PRIMARY KEY(product_id, shop_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE products_shops ADD CONSTRAINT FK_C32399534584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE products_shops ADD CONSTRAINT FK_C32399534D16C4DD FOREIGN KEY (shop_id) REFERENCES shop (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fabricator DROP INDEX UNIQ_E0378BFFA76ED395, ADD INDEX IDX_E0378BFFA76ED395 (user_id)');
        $this->addSql('ALTER TABLE shop DROP INDEX UNIQ_AC6A4CA2A76ED395, ADD INDEX IDX_AC6A4CA2A76ED395 (user_id)');
    }
}
