<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181026123441 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product ADD changed DATETIME NOT NULL, ADD removed TINYINT(1) DEFAULT NULL');
        $this->addSql('CREATE INDEX product_slug ON product (slug)');
        $this->addSql('CREATE INDEX product_created ON product (created)');
        $this->addSql('CREATE INDEX product_visible ON product (visible)');
        $this->addSql('ALTER TABLE product_moderation CHANGE closed resolution_date DATETIME DEFAULT NULL');
        $this->addSql('CREATE INDEX moderation_created ON product_moderation (created)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX product_slug ON product');
        $this->addSql('DROP INDEX product_created ON product');
        $this->addSql('DROP INDEX product_visible ON product');
        $this->addSql('ALTER TABLE product DROP changed, DROP removed');
        $this->addSql('DROP INDEX moderation_created ON product_moderation');
        $this->addSql('ALTER TABLE product_moderation CHANGE resolution_date closed DATETIME DEFAULT NULL');
    }
}
