<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181107142136 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD6BF700BD');
        $this->addSql('CREATE TABLE fabricator (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, name VARCHAR(128) NOT NULL, inn VARCHAR(12) NOT NULL, kpp VARCHAR(9) NOT NULL, legal_address VARCHAR(255) NOT NULL, created DATETIME NOT NULL, INDEX IDX_E0378BFFA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fabricators_files (fabricator_id INT NOT NULL, file_id INT NOT NULL, INDEX IDX_5E1C932BB81315B2 (fabricator_id), UNIQUE INDEX UNIQ_5E1C932B93CB796C (file_id), PRIMARY KEY(fabricator_id, file_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fabricators_categories (fabricator_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_C8FE4983B81315B2 (fabricator_id), INDEX IDX_C8FE498312469DE2 (category_id), PRIMARY KEY(fabricator_id, category_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE products_shops (product_id INT NOT NULL, shop_id INT NOT NULL, INDEX IDX_C32399534584665A (product_id), INDEX IDX_C32399534D16C4DD (shop_id), PRIMARY KEY(product_id, shop_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shop (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, logo_file_id INT DEFAULT NULL, name VARCHAR(128) NOT NULL, legal_name VARCHAR(255) NOT NULL, inn VARCHAR(12) NOT NULL, ogrn VARCHAR(15) DEFAULT NULL, postal_address VARCHAR(255) NOT NULL, legal_address VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, unique_design TINYINT(1) DEFAULT NULL, created DATETIME NOT NULL, phone_numbers LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', web_links LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', work_schedule LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_AC6A4CA2A76ED395 (user_id), UNIQUE INDEX UNIQ_AC6A4CA2A7E1931C (logo_file_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fabricator ADD CONSTRAINT FK_E0378BFFA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE fabricators_files ADD CONSTRAINT FK_5E1C932BB81315B2 FOREIGN KEY (fabricator_id) REFERENCES fabricator (id)');
        $this->addSql('ALTER TABLE fabricators_files ADD CONSTRAINT FK_5E1C932B93CB796C FOREIGN KEY (file_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE fabricators_categories ADD CONSTRAINT FK_C8FE4983B81315B2 FOREIGN KEY (fabricator_id) REFERENCES fabricator (id)');
        $this->addSql('ALTER TABLE fabricators_categories ADD CONSTRAINT FK_C8FE498312469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE products_shops ADD CONSTRAINT FK_C32399534584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE products_shops ADD CONSTRAINT FK_C32399534D16C4DD FOREIGN KEY (shop_id) REFERENCES shop (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE shop ADD CONSTRAINT FK_AC6A4CA2A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE shop ADD CONSTRAINT FK_AC6A4CA2A7E1931C FOREIGN KEY (logo_file_id) REFERENCES file (id)');
        $this->addSql('DROP TABLE product_status');
        $this->addSql('DROP INDEX IDX_D34A04AD6BF700BD ON product');
        $this->addSql('ALTER TABLE product DROP status_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fabricators_files DROP FOREIGN KEY FK_5E1C932BB81315B2');
        $this->addSql('ALTER TABLE fabricators_categories DROP FOREIGN KEY FK_C8FE4983B81315B2');
        $this->addSql('ALTER TABLE products_shops DROP FOREIGN KEY FK_C32399534D16C4DD');
        $this->addSql('CREATE TABLE product_status (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(20) NOT NULL COLLATE utf8_unicode_ci, code VARCHAR(20) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE fabricator');
        $this->addSql('DROP TABLE fabricators_files');
        $this->addSql('DROP TABLE fabricators_categories');
        $this->addSql('DROP TABLE products_shops');
        $this->addSql('DROP TABLE shop');
        $this->addSql('ALTER TABLE product ADD status_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD6BF700BD FOREIGN KEY (status_id) REFERENCES product_status (id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD6BF700BD ON product (status_id)');
    }
}
