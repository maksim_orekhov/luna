<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181107144233 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE fabricators_moderations (fabricator_id INT NOT NULL, moderation_id INT NOT NULL, INDEX IDX_FD5F746FB81315B2 (fabricator_id), UNIQUE INDEX UNIQ_FD5F746F67FDB807 (moderation_id), PRIMARY KEY(fabricator_id, moderation_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE moderation (id INT AUTO_INCREMENT NOT NULL, created DATETIME NOT NULL, resolution_date DATETIME DEFAULT NULL, approved TINYINT(1) DEFAULT NULL, description LONGTEXT DEFAULT NULL, INDEX moderation_created (created), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE products_moderations (product_id INT NOT NULL, moderation_id INT NOT NULL, INDEX IDX_30A8200B4584665A (product_id), UNIQUE INDEX UNIQ_30A8200B67FDB807 (moderation_id), PRIMARY KEY(product_id, moderation_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shops_moderations (shop_id INT NOT NULL, moderation_id INT NOT NULL, INDEX IDX_A06965F24D16C4DD (shop_id), UNIQUE INDEX UNIQ_A06965F267FDB807 (moderation_id), PRIMARY KEY(shop_id, moderation_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fabricators_moderations ADD CONSTRAINT FK_FD5F746FB81315B2 FOREIGN KEY (fabricator_id) REFERENCES fabricator (id)');
        $this->addSql('ALTER TABLE fabricators_moderations ADD CONSTRAINT FK_FD5F746F67FDB807 FOREIGN KEY (moderation_id) REFERENCES moderation (id)');
        $this->addSql('ALTER TABLE products_moderations ADD CONSTRAINT FK_30A8200B4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE products_moderations ADD CONSTRAINT FK_30A8200B67FDB807 FOREIGN KEY (moderation_id) REFERENCES moderation (id)');
        $this->addSql('ALTER TABLE shops_moderations ADD CONSTRAINT FK_A06965F24D16C4DD FOREIGN KEY (shop_id) REFERENCES shop (id)');
        $this->addSql('ALTER TABLE shops_moderations ADD CONSTRAINT FK_A06965F267FDB807 FOREIGN KEY (moderation_id) REFERENCES moderation (id)');
        $this->addSql('DROP TABLE product_moderation');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fabricators_moderations DROP FOREIGN KEY FK_FD5F746F67FDB807');
        $this->addSql('ALTER TABLE products_moderations DROP FOREIGN KEY FK_30A8200B67FDB807');
        $this->addSql('ALTER TABLE shops_moderations DROP FOREIGN KEY FK_A06965F267FDB807');
        $this->addSql('CREATE TABLE product_moderation (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, created DATETIME NOT NULL, resolution_date DATETIME DEFAULT NULL, approved TINYINT(1) DEFAULT NULL, description LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, INDEX IDX_538D358F4584665A (product_id), INDEX moderation_created (created), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_moderation ADD CONSTRAINT FK_538D358F4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('DROP TABLE fabricators_moderations');
        $this->addSql('DROP TABLE moderation');
        $this->addSql('DROP TABLE products_moderations');
        $this->addSql('DROP TABLE shops_moderations');
    }
}
