<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180927073250 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE address (id INT AUTO_INCREMENT NOT NULL, country VARCHAR(255) DEFAULT NULL, region VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, street VARCHAR(255) DEFAULT NULL, house VARCHAR(16) DEFAULT NULL, apartment VARCHAR(16) DEFAULT NULL, postcode VARCHAR(64) DEFAULT NULL, lat VARCHAR(128) DEFAULT NULL, lng VARCHAR(128) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, name VARCHAR(128) NOT NULL, slug VARCHAR(128) NOT NULL, description LONGTEXT DEFAULT NULL, active TINYINT(1) DEFAULT NULL, INDEX IDX_64C19C1727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE city (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, name VARCHAR(128) NOT NULL, name_prepositional VARCHAR(128) NOT NULL, slug VARCHAR(128) NOT NULL, type VARCHAR(128) NOT NULL, INDEX IDX_2D5B0234727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE country (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(70) NOT NULL, code VARCHAR(2) NOT NULL, code3 VARCHAR(3) NOT NULL, phone_code INT NOT NULL, postcode_required TINYINT(1) DEFAULT \'0\' NOT NULL, is_eu TINYINT(1) DEFAULT \'0\' NOT NULL, weight INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE e164 (id INT AUTO_INCREMENT NOT NULL, country_code SMALLINT NOT NULL, region_code SMALLINT NOT NULL, phone_number BIGINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE email (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(255) NOT NULL, is_verified TINYINT(1) DEFAULT NULL, confirmation_token VARCHAR(32) DEFAULT NULL, confirmation_token_creation_date DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE global_setting (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, value VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_F4F078795E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE permission (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE phone (id INT AUTO_INCREMENT NOT NULL, e164_id INT DEFAULT NULL, country_id INT DEFAULT NULL, user_enter VARCHAR(50) NOT NULL, phone_number VARCHAR(45) DEFAULT NULL, comment TINYTEXT DEFAULT NULL, is_verified TINYINT(1) DEFAULT NULL, INDEX IDX_444F97DDF92F3E70 (country_id), INDEX fk_phone_e1641_idx (e164_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, status_id INT DEFAULT NULL, address_id INT DEFAULT NULL, name VARCHAR(128) NOT NULL, slug VARCHAR(255) DEFAULT NULL, code VARCHAR(64) DEFAULT NULL, description LONGTEXT DEFAULT NULL, publication_period SMALLINT NOT NULL, price NUMERIC(10, 2) DEFAULT NULL, created DATETIME NOT NULL, deadline DATETIME DEFAULT NULL, visible TINYINT(1) DEFAULT NULL, INDEX IDX_D34A04ADA76ED395 (user_id), INDEX IDX_D34A04AD6BF700BD (status_id), UNIQUE INDEX UNIQ_D34A04ADF5B7AF75 (address_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE products_files (product_id INT NOT NULL, file_id INT NOT NULL, INDEX IDX_E66CBE894584665A (product_id), UNIQUE INDEX UNIQ_E66CBE8993CB796C (file_id), PRIMARY KEY(product_id, file_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE products_galleries (product_id INT NOT NULL, file_id INT NOT NULL, INDEX IDX_A5738D914584665A (product_id), UNIQUE INDEX UNIQ_A5738D9193CB796C (file_id), PRIMARY KEY(product_id, file_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE products_categories (product_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_E8ACBE764584665A (product_id), INDEX IDX_E8ACBE7612469DE2 (category_id), PRIMARY KEY(product_id, category_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE products_cities (product_id INT NOT NULL, city_id INT NOT NULL, INDEX IDX_5F6E3A414584665A (product_id), INDEX IDX_5F6E3A418BAC62AF (city_id), PRIMARY KEY(product_id, city_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_moderation (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, created DATETIME NOT NULL, closed DATETIME DEFAULT NULL, approved TINYINT(1) DEFAULT NULL, description LONGTEXT DEFAULT NULL, INDEX IDX_538D358F4584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_prolongation (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, days SMALLINT NOT NULL, created DATETIME NOT NULL, INDEX IDX_661BBB6C4584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_status (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(20) NOT NULL, code VARCHAR(20) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role_hierarchy (role_source INT NOT NULL, role_target INT NOT NULL, INDEX IDX_AB8EFB72F4AC9EC2 (role_source), INDEX IDX_AB8EFB72ED49CE4D (role_target), PRIMARY KEY(role_source, role_target)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role_permission (role_id INT NOT NULL, permission_id INT NOT NULL, INDEX IDX_6F7DF886D60322AC (role_id), INDEX IDX_6F7DF886FED90CCA (permission_id), PRIMARY KEY(role_id, permission_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email_id INT DEFAULT NULL, phone_id INT DEFAULT NULL, name VARCHAR(128) DEFAULT NULL, login VARCHAR(45) NOT NULL, password MEDIUMTEXT NOT NULL, created DATETIME NOT NULL, password_reset_token VARCHAR(32) DEFAULT NULL, password_reset_token_creation_date DATETIME DEFAULT NULL, is_banned TINYINT(1) DEFAULT NULL, INDEX fk_user_phone1_idx (phone_id), INDEX fk_user_email1_idx (email_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_avatar_file (user_id INT NOT NULL, file_id INT NOT NULL, INDEX IDX_B720945EA76ED395 (user_id), INDEX IDX_B720945E93CB796C (file_id), PRIMARY KEY(user_id, file_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_role (user_id INT NOT NULL, role_id INT NOT NULL, INDEX IDX_2DE8C6A3A76ED395 (user_id), INDEX IDX_2DE8C6A3D60322AC (role_id), PRIMARY KEY(user_id, role_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE code (id INT AUTO_INCREMENT NOT NULL, code_type_id INT DEFAULT NULL, user_id INT DEFAULT NULL, value VARCHAR(10) NOT NULL, count INT DEFAULT NULL, created DATETIME NOT NULL, INDEX IDX_77153098FDEBC80F (code_type_id), INDEX IDX_77153098A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE code_operation_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(45) DEFAULT NULL, description LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE file (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, origin_name VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, path VARCHAR(255) NOT NULL, size INT NOT NULL, created DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C1727ACA70 FOREIGN KEY (parent_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE city ADD CONSTRAINT FK_2D5B0234727ACA70 FOREIGN KEY (parent_id) REFERENCES city (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE phone ADD CONSTRAINT FK_444F97DD43EC56C6 FOREIGN KEY (e164_id) REFERENCES e164 (id)');
        $this->addSql('ALTER TABLE phone ADD CONSTRAINT FK_444F97DDF92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD6BF700BD FOREIGN KEY (status_id) REFERENCES product_status (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADF5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE products_files ADD CONSTRAINT FK_E66CBE894584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE products_files ADD CONSTRAINT FK_E66CBE8993CB796C FOREIGN KEY (file_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE products_galleries ADD CONSTRAINT FK_A5738D914584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE products_galleries ADD CONSTRAINT FK_A5738D9193CB796C FOREIGN KEY (file_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE products_categories ADD CONSTRAINT FK_E8ACBE764584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE products_categories ADD CONSTRAINT FK_E8ACBE7612469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE products_cities ADD CONSTRAINT FK_5F6E3A414584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE products_cities ADD CONSTRAINT FK_5F6E3A418BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_moderation ADD CONSTRAINT FK_538D358F4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE product_prolongation ADD CONSTRAINT FK_661BBB6C4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE role_hierarchy ADD CONSTRAINT FK_AB8EFB72F4AC9EC2 FOREIGN KEY (role_source) REFERENCES role (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE role_hierarchy ADD CONSTRAINT FK_AB8EFB72ED49CE4D FOREIGN KEY (role_target) REFERENCES role (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE role_permission ADD CONSTRAINT FK_6F7DF886D60322AC FOREIGN KEY (role_id) REFERENCES role (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE role_permission ADD CONSTRAINT FK_6F7DF886FED90CCA FOREIGN KEY (permission_id) REFERENCES permission (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649A832C1C9 FOREIGN KEY (email_id) REFERENCES email (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6493B7323CB FOREIGN KEY (phone_id) REFERENCES phone (id)');
        $this->addSql('ALTER TABLE user_avatar_file ADD CONSTRAINT FK_B720945EA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_avatar_file ADD CONSTRAINT FK_B720945E93CB796C FOREIGN KEY (file_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE user_role ADD CONSTRAINT FK_2DE8C6A3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_role ADD CONSTRAINT FK_2DE8C6A3D60322AC FOREIGN KEY (role_id) REFERENCES role (id)');
        $this->addSql('ALTER TABLE code ADD CONSTRAINT FK_77153098FDEBC80F FOREIGN KEY (code_type_id) REFERENCES code_operation_type (id)');
        $this->addSql('ALTER TABLE code ADD CONSTRAINT FK_77153098A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADF5B7AF75');
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C1727ACA70');
        $this->addSql('ALTER TABLE products_categories DROP FOREIGN KEY FK_E8ACBE7612469DE2');
        $this->addSql('ALTER TABLE city DROP FOREIGN KEY FK_2D5B0234727ACA70');
        $this->addSql('ALTER TABLE products_cities DROP FOREIGN KEY FK_5F6E3A418BAC62AF');
        $this->addSql('ALTER TABLE phone DROP FOREIGN KEY FK_444F97DDF92F3E70');
        $this->addSql('ALTER TABLE phone DROP FOREIGN KEY FK_444F97DD43EC56C6');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649A832C1C9');
        $this->addSql('ALTER TABLE role_permission DROP FOREIGN KEY FK_6F7DF886FED90CCA');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6493B7323CB');
        $this->addSql('ALTER TABLE products_files DROP FOREIGN KEY FK_E66CBE894584665A');
        $this->addSql('ALTER TABLE products_galleries DROP FOREIGN KEY FK_A5738D914584665A');
        $this->addSql('ALTER TABLE products_categories DROP FOREIGN KEY FK_E8ACBE764584665A');
        $this->addSql('ALTER TABLE products_cities DROP FOREIGN KEY FK_5F6E3A414584665A');
        $this->addSql('ALTER TABLE product_moderation DROP FOREIGN KEY FK_538D358F4584665A');
        $this->addSql('ALTER TABLE product_prolongation DROP FOREIGN KEY FK_661BBB6C4584665A');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD6BF700BD');
        $this->addSql('ALTER TABLE role_hierarchy DROP FOREIGN KEY FK_AB8EFB72F4AC9EC2');
        $this->addSql('ALTER TABLE role_hierarchy DROP FOREIGN KEY FK_AB8EFB72ED49CE4D');
        $this->addSql('ALTER TABLE role_permission DROP FOREIGN KEY FK_6F7DF886D60322AC');
        $this->addSql('ALTER TABLE user_role DROP FOREIGN KEY FK_2DE8C6A3D60322AC');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADA76ED395');
        $this->addSql('ALTER TABLE user_avatar_file DROP FOREIGN KEY FK_B720945EA76ED395');
        $this->addSql('ALTER TABLE user_role DROP FOREIGN KEY FK_2DE8C6A3A76ED395');
        $this->addSql('ALTER TABLE code DROP FOREIGN KEY FK_77153098A76ED395');
        $this->addSql('ALTER TABLE code DROP FOREIGN KEY FK_77153098FDEBC80F');
        $this->addSql('ALTER TABLE products_files DROP FOREIGN KEY FK_E66CBE8993CB796C');
        $this->addSql('ALTER TABLE products_galleries DROP FOREIGN KEY FK_A5738D9193CB796C');
        $this->addSql('ALTER TABLE user_avatar_file DROP FOREIGN KEY FK_B720945E93CB796C');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE city');
        $this->addSql('DROP TABLE country');
        $this->addSql('DROP TABLE e164');
        $this->addSql('DROP TABLE email');
        $this->addSql('DROP TABLE global_setting');
        $this->addSql('DROP TABLE permission');
        $this->addSql('DROP TABLE phone');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE products_files');
        $this->addSql('DROP TABLE products_galleries');
        $this->addSql('DROP TABLE products_categories');
        $this->addSql('DROP TABLE products_cities');
        $this->addSql('DROP TABLE product_moderation');
        $this->addSql('DROP TABLE product_prolongation');
        $this->addSql('DROP TABLE product_status');
        $this->addSql('DROP TABLE role');
        $this->addSql('DROP TABLE role_hierarchy');
        $this->addSql('DROP TABLE role_permission');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_avatar_file');
        $this->addSql('DROP TABLE user_role');
        $this->addSql('DROP TABLE code');
        $this->addSql('DROP TABLE code_operation_type');
        $this->addSql('DROP TABLE file');
    }
}
